USE [SPEX_TR_DB]
GO
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-01', N'Monitoring', N'', N'', N'Y', 1)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-02', N'Production Master', N'', N'', N'Y', 8)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-03', N'Delivery Arrangement', N'', N'', N'N', 3)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-04', N'Monthly Order Planning (MOP)', N'', N'', N'N', 4)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-05', N'Production Control', N'', N'', N'Y', 2)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-06', N'Manifest Receiving', N'', N'', N'N', 6)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-07', N'Part Receiving', N'', N'', N'N', 7)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-08', N'Packing Vaning', N'', N'', N'Y', 5)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-09', N'Export Master', N'', N'', N'Y', 7)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-10', N'Invoice and Shipment', N'', N'', N'Y', 6)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-11', N'Stock Management', N'', N'', N'Y', 4)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'menu-12', N'Partial Payment', N'', N'', N'Y', 3)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-01.01', N'File Log Monitoring', N'FileLogMonitoring', N'menu-01', N'Y', 1)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-01.02', N'Log Monitoring', N'LogMonitoring', N'menu-01', N'Y', 2)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-01.03', N'DynamicQuery', N'DynamicQuery', N'menu-01', N'N', 3)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.01', N'Buyer PD', N'BuyerPD', N'menu-02', N'Y', 1)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.02', N'Distribution', N'DistributionMaster', N'menu-02', N'Y', 2)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.03', N'Export Order Header', N'ExportOrderHeaderMaster', N'menu-02', N'Y', 3)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.04', N'Order Type Conversion', N'OrderTypeConversionMaster', N'menu-02', N'Y', 4)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.05', N'Part Master', N'PartMaster', N'menu-02', N'Y', 5)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.06', N'Payment Term', N'PaymentTermMaster', N'menu-02', N'Y', 6)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.07', N'Price Term', N'PriceTerm', N'menu-02', N'Y', 7)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.08', N'PD Code', N'PDCode', N'menu-02', N'Y', 8)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.09', N'TMMIN Packing Service Part', N'TMMINPackingServicePartExportMaintence', N'menu-02', N'Y', 9)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.09.01', N'Calendar Master', N'CalendarMaster', N'menu-02', N'Y', 10)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.09.02', N'Shutter Master', N'ShutterMaster', N'menu-02', N'Y', 11)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.09.03', N'Level Case Master', N'LevelCaseMaster', N'menu-02', N'Y', 12)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.10', N'Case Master', N'CaseMaster', N'menu-02', N'Y', 13)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.11', N'Device User Master', N'DeviceUserMaster', N'menu-02', N'Y', 14)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-02.12', N'Sub Supplier Master', N'SubSupplier', N'menu-02', N'Y', 15)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-03.01', N'Upload TLMS Result', N'UploadTLMS', N'menu-05', N'Y', 4)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-04.01', N'Monthly Order Planning Inquiry', N'MonthlyOrderPlanningInquiry', N'menu-05', N'Y', 5)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-05.01', N'Order Inquiry', N'OrderInquiry', N'menu-05', N'Y', 1)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-05.02', N'POSS Confirmation', N'POSSConfirmation', N'menu-05', N'Y', 2)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-05.03', N'Order Background Report Inquiry', N'OBR', N'menu-05', N'Y', 3)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-05.04', N'POSS Inquiry', N'POSSInquiry', N'menu-05', N'Y', 4)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-05.05', N'Pipeline Inquiry', N'PipelineInquiry', N'menu-05', N'y', 5)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-06.01', N'Manifest Receiving Inquiry', N'ManifestRecivingInquiry', N'menu-08', N'N', 1)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-07.01', N'Part Receiving (Sorting) Input', N'PartReceiving', N'menu-08', N'Y', 3)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-07.02', N'Part Receiving (Sorting) Inquiry', N'PartReceivingInquiry', N'menu-08', N'Y', 4)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-08.02', N'Packing Order', N'PackingOrder', N'menu-08', N'Y', 7)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-08.03', N'Packing Completion', N'PackingCompletion', N'menu-08', N'N', 7)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-08.04', N'Vanning Completion', N'VanningCompletion', N'menu-08', N'N', 8)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-08.05', N'Revise Packing', N'RevisePacking', N'menu-08', N'Y', 8)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-08.06', N'Packing Completion Inquiry', N'PackingCompInquiry', N'menu-08', N'Y', 5)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-08.07', N'Vanning Inquiry', N'VanningInquire', N'menu-08', N'Y', 6)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-08.08', N'Part Label Printing', N'PartLabelPrinting', N'menu-08', N'Y', 9)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-08.09', N'Case Grouping Maintenance', N'CaseGroupingMaintenance', N'menu-08', N'Y', 10)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.01', N'Buyer Master', N'Buyer', N'menu-09', N'Y', 1)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.02', N'Shipping Agent Master', N'SHIPPING_AGENT', N'menu-09', N'Y', 2)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.03', N'Forwarding Agent', N'FORWARDING_AGENT', N'menu-09', N'Y', 3)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.04', N'Forwarding Agent Air Assignment [By Air]', N'ForwardingAgentAirAssignment', N'menu-09', N'Y', 4)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.05', N'Port Loading Master', N'PORT_LOADING', N'menu-09', N'Y', 5)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.06', N'Vessel Schedule Master', N'VESSEL_SCHEDULE', N'menu-09', N'Y', 6)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.07', N'Part Price Master', N'PartPriceMaster', N'menu-09', N'Y', 7)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.08', N'RVC Master', N'RVCMaster', N'menu-09', N'Y', 8)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.09', N'HS Master', N'HSMaster', N'menu-09', N'Y', 9)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.10', N'Marks Number Master [By Air]', N'MarksNumberMaster', N'menu-09', N'Y', 10)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.11', N'Port Discharge', N'PORT_DISCHARGE', N'menu-09', N'Y', 11)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.12', N'Document Master', N'Document', N'menu-01', N'Y', 3)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.13', N'Vendor Service Mapping Master', N'ServiceMappingMaster', N'menu-09', N'Y', 13)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-09.14', N'Service Price Master', N'ServicePriceMaster', N'menu-09', N'Y', 14)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.01', N'Ready Cargo [By Sea]', N'ReadyCargoInquiry', N'menu-10', N'Y', 1)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.02', N'Ready Cargo [By Air]', N'ReadyCargoByAirInquiry', N'menu-10', N'Y', 2)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.03', N'Invoice, PEB, FormD, Shipment, Finance', N'Invoice', N'menu-10', N'Y', 3)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.04', N'Tentative Shipping Instruction', N'ShippingInstructionTentative', N'menu-10', N'Y', 4)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.05', N'Final Shipping Instruction', N'ShippingInstruction', N'menu-10', N'Y', 5)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.06', N'PEB Process', N'PEB', N'menu-10', N'Y', 6)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.07', N'PEB Process (TAM Packing)', N'PEBTAM', N'menu-10', N'N', 7)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.08', N'Shipping & Vanning Instruction [By Air]', N'VanningInstructionByAir', N'menu-10', N'Y', 8)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.09', N'Good Issue', N'GoodIssue', N'menu-10', N'Y', 9)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.09.01', N'Form D Data Upload', N'FormD_Data_Upload', N'menu-10', N'N', 10)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.09.02', N'Ready Cargo Download', N'ReadyCargoDownload', N'menu-10', N'Y', 11)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-10.09.03', N'Shipment Inquiry', N'ShipmentInquiry', N'menu-10', N'Y', 12)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-11.01', N'Rack Address Master', N'RackAddressMaster', N'menu-11', N'Y', 1)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-11.02', N'Maintenance Stock Master', N'MaintenanceStockMaster', N'menu-11', N'Y', 2)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-11.03', N'Stock Inquiry', N'StockInquiry', N'menu-11', N'Y', 8)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-11.04', N'Picking List Inquiry', N'PickingListInquiry', N'menu-11', N'Y', 4)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-11.05', N'Warehouse Inquiry', N'WarehouseInquiry', N'menu-11', N'Y', 3)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-11.06', N'Picking List Operation', N'PickingListOperation', N'menu-11', N'Y', 5)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-11.07', N'Part Stock Receiving (Binning) Inquiry', N'PartStockReceivingInquiry', N'menu-08', N'Y', 2)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-11.08', N'Sub Manifest Receiving Inquiry', N'SubManifestReceivingInquiry', N'menu-08', N'Y', 1)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-11.09', N'Stock Transaction Inquiry', N'StockPipelineInquiry', N'menu-11', N'Y', 9)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-12.01', N'Manual Order Creation', N'ManualOrderCreation', N'menu-12', N'Y', 1)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-12.02', N'Cancellation And Reorder', N'CancellationAndReorder', N'menu-12', N'Y', 2)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-12.03', N'Problem Report Creation', N'ProblemReportCreation', N'menu-12', N'Y', 3)
INSERT [spex].[TB_M_MENU_SPEX] ([MENU_ID], [MENU_TEXT], [NAVIGATE_URL], [PARENT_ID], [VISIBLE], [SHOW_NO]) VALUES (N'submenu-12.04', N'Problem Report Inquiry', N'ProblemReportInquiry', N'menu-12', N'Y', 4)
