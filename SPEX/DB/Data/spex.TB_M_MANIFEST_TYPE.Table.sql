USE [SPEX_TR_DB]
GO
INSERT [spex].[TB_M_MANIFEST_TYPE] ([ORDER_TYPE], [TRANSP_CD], [MANIFEST_TYPE], [REMARK], [REMARK_FINISHCASE], [PO_TYPE], [MANIFEST_NAME]) VALUES (N'V', N'1', N'P', N'PRIORITY', N'PRIOR-LC', N'D3EV', N'PRIORITY')
INSERT [spex].[TB_M_MANIFEST_TYPE] ([ORDER_TYPE], [TRANSP_CD], [MANIFEST_TYPE], [REMARK], [REMARK_FINISHCASE], [PO_TYPE], [MANIFEST_NAME]) VALUES (N'9', N'%', N'R', NULL, N'LC', N'D3ER', N'REGULAR')
INSERT [spex].[TB_M_MANIFEST_TYPE] ([ORDER_TYPE], [TRANSP_CD], [MANIFEST_TYPE], [REMARK], [REMARK_FINISHCASE], [PO_TYPE], [MANIFEST_NAME]) VALUES (N'2', N'%', N'I', N'INITIAL', N'INITIAL-LC', N'D3EE', N'INITIAL')
INSERT [spex].[TB_M_MANIFEST_TYPE] ([ORDER_TYPE], [TRANSP_CD], [MANIFEST_TYPE], [REMARK], [REMARK_FINISHCASE], [PO_TYPE], [MANIFEST_NAME]) VALUES (N'%', N'%', N'S', N'SPECIAL', N'SPECIAL-LC', N'D3EE', N'SPECIAL')
INSERT [spex].[TB_M_MANIFEST_TYPE] ([ORDER_TYPE], [TRANSP_CD], [MANIFEST_TYPE], [REMARK], [REMARK_FINISHCASE], [PO_TYPE], [MANIFEST_NAME]) VALUES (N'5', N'%', N'R', NULL, N'LC', N'D3ER', N'REGULAR')
