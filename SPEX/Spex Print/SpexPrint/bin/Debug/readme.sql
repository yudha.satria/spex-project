--1 Remote Desktop ke 10.16.20.98 username=Toyota\istd.app2 Password toyota@123
--2 jalankan query dibawah untuk database spex_tr_db (trial) 


DROP SERVICE spex_SendService
DROP SERVICE spex_ReceiveService
DROP QUEUE spex_SendQueue
DROP QUEUE spex_ReceiveQueue
DROP CONTRACT spex_QueueContract 
DROP MESSAGE TYPE spex_QueueMessage

GO

CREATE MESSAGE TYPE spex_QueueMessage VALIDATION = NONE
CREATE CONTRACT spex_QueueContract (spex_QueueMessage SENT BY INITIATOR)
CREATE QUEUE spex_SendQueue
CREATE QUEUE spex_ReceiveQueue
CREATE SERVICE spex_SendService ON QUEUE spex_SendQueue (spex_QueueContract)
CREATE SERVICE spex_ReceiveService ON QUEUE spex_ReceiveQueue (spex_QueueContract)

GO

GRANT SEND ON SERVICE::spex_SendService TO SP3X_DB_QAS;
GO
GRANT SEND ON SERVICE::spex_ReceiveService TO SP3X_DB_QAS;
GO
GRANT RECEIVE ON spex_ReceiveQueue TO SP3X_DB_QAS;
GO
GRANT RECEIVE ON spex_SendQueue TO SP3X_DB_QAS;
GO

exec spex.spQueueClear

--3 remote ke print server cari folder E:/spex_db_tr 10.33.4.14
--gunakan username=Toyota\istd.app2 Password toyota@123
--replace SpexPrint.exe , apabila tidak bisa di replace, kill spexprint.exe from task manager