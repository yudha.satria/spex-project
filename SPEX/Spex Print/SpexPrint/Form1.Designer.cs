﻿namespace SpexPrint
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.serviceNameLbl = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnViewQueue = new System.Windows.Forms.Button();
            this.clearLogBtn = new System.Windows.Forms.Button();
            this.cbPrinter = new System.Windows.Forms.ComboBox();
            this.btnRefreshPrinter = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(55, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 36);
            this.button1.TabIndex = 0;
            this.button1.Text = "Test Print";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(55, 195);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(144, 36);
            this.button2.TabIndex = 1;
            this.button2.Text = "Test DB";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(55, 71);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(144, 36);
            this.button3.TabIndex = 2;
            this.button3.Text = "Print Service";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // serviceNameLbl
            // 
            this.serviceNameLbl.AutoSize = true;
            this.serviceNameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serviceNameLbl.ForeColor = System.Drawing.Color.Red;
            this.serviceNameLbl.Location = new System.Drawing.Point(217, 78);
            this.serviceNameLbl.Name = "serviceNameLbl";
            this.serviceNameLbl.Size = new System.Drawing.Size(70, 20);
            this.serviceNameLbl.TabIndex = 3;
            this.serviceNameLbl.Text = "Stopped";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(55, 279);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(144, 36);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnViewQueue
            // 
            this.btnViewQueue.Location = new System.Drawing.Point(55, 111);
            this.btnViewQueue.Name = "btnViewQueue";
            this.btnViewQueue.Size = new System.Drawing.Size(144, 36);
            this.btnViewQueue.TabIndex = 5;
            this.btnViewQueue.Text = "View Queue";
            this.btnViewQueue.UseVisualStyleBackColor = true;
            this.btnViewQueue.Click += new System.EventHandler(this.btnViewQueue_Click);
            // 
            // clearLogBtn
            // 
            this.clearLogBtn.Location = new System.Drawing.Point(55, 153);
            this.clearLogBtn.Name = "clearLogBtn";
            this.clearLogBtn.Size = new System.Drawing.Size(144, 36);
            this.clearLogBtn.TabIndex = 6;
            this.clearLogBtn.Text = "Clear Log And PDF";
            this.clearLogBtn.UseVisualStyleBackColor = true;
            this.clearLogBtn.Click += new System.EventHandler(this.clearLogBtn_Click);
            // 
            // cbPrinter
            // 
            this.cbPrinter.FormattingEnabled = true;
            this.cbPrinter.Location = new System.Drawing.Point(218, 38);
            this.cbPrinter.Name = "cbPrinter";
            this.cbPrinter.Size = new System.Drawing.Size(188, 21);
            this.cbPrinter.TabIndex = 7;
            // 
            // btnRefreshPrinter
            // 
            this.btnRefreshPrinter.Location = new System.Drawing.Point(55, 237);
            this.btnRefreshPrinter.Name = "btnRefreshPrinter";
            this.btnRefreshPrinter.Size = new System.Drawing.Size(144, 36);
            this.btnRefreshPrinter.TabIndex = 8;
            this.btnRefreshPrinter.Text = "Refresh Printer";
            this.btnRefreshPrinter.UseVisualStyleBackColor = true;
            this.btnRefreshPrinter.Click += new System.EventHandler(this.btnRefreshPrinter_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(327, 291);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "version 1.0";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "Spex Print";
            this.notifyIcon1.BalloonTipTitle = "Spex Print";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Spex Print";
            this.notifyIcon1.Click += new System.EventHandler(this.notifyIcon1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 334);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnRefreshPrinter);
            this.Controls.Add(this.cbPrinter);
            this.Controls.Add(this.clearLogBtn);
            this.Controls.Add(this.btnViewQueue);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.serviceNameLbl);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Printer Service";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label serviceNameLbl;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnViewQueue;
        private System.Windows.Forms.Button clearLogBtn;
        private System.Windows.Forms.ComboBox cbPrinter;
        private System.Windows.Forms.Button btnRefreshPrinter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

