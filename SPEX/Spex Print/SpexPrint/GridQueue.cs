﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SpexPrint
{
    public partial class GridQueue : Form
    {
        public GridQueue()
        {
            InitializeComponent();
        }

        private void GridQueue_Load(object sender, EventArgs e)
        {
            RefreshQueue();
        }

        private void RefreshQueue()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());

            SqlCommand cmd = new SqlCommand("[spex].[spQueueView]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable o = new DataTable();
            conn.Open();
            da.Fill(o);
            conn.Close();

            dataGridView1.DataSource = o;
            dataGridView1.Refresh();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshQueue();
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());

            SqlCommand cmd = new SqlCommand("[spex].[spQueueClear]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            cmd.CommandTimeout = 0;
            cmd.ExecuteScalar();
            conn.Close();

            RefreshQueue();
        }

        private void btnSendTestQueue_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());

            SqlCommand cmd = new SqlCommand("[spex].[spQueueSend]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Message", "Test");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            cmd.CommandTimeout = 0;
            cmd.ExecuteScalar();
            conn.Close();
        }

    }
}
