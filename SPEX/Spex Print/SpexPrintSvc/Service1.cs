﻿using Seagull.BarTender.Print;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Configuration;
using System.Threading;
using System.Transactions;
using SpexPrint.AppCode;

namespace SpexPrintSvc
{
    public partial class Service1 : ServiceBase
    {
        static List<QueueListenerConfig> QueueSettings = new List<QueueListenerConfig>();
        static List<Thread> Listeners = new List<Thread>();
        private static Engine engine = null;
        static bool stopping = false;
        private static LabelFormatDocument format = null;
        static String AppName = "SpexPrintSvc";
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            string path = GetPath("log.txt");
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                Log("Start Service", writer);

                var l = new QueueListenerConfig();
                l.QueueName = "spex_ReceiveQueue";  //The name of the service broker queue 
                l.Threads = 1;
                l.EnlistMessageProcessor = false;
                l.MessageProcessor = InboundMessageProcessor.ProcessMessage;  //Wire up the message processors 
                l.FailedMessageProcessor = InboundMessageProcessor.SaveFailedMessage;
                l.ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ToString();
                //l.log = listBox1;
                QueueSettings.Add(l);

                //l = new QueueListenerConfig(); 
                //l.QueueName = "Outbound"; 
                //l.Threads = 2; 
                //l.EnlistMessageProcessor = false; 
                //l.MessageProcessor = OutboundMessageProcessor.ProcessMessage; 
                //l.FailedMessageProcessor = OutboundMessageProcessor.SaveFailedMessage; 
                //l.ConnectionString = "Data Source=(local);Initial Catalog=test;Integrated Security=true"; 


                foreach (var q in QueueSettings)
                {
                    for (int i = 0; i < q.Threads; i++)
                    {
                        Thread listenerThread = new Thread(ListenerThreadProc);
                        listenerThread.Name = "Listener Thread " + i.ToString() + " for " + q.QueueName;
                        listenerThread.IsBackground = false;
                        Listeners.Add(listenerThread);

                        listenerThread.Start(q);
                        Trace.WriteLine("Started thread " + listenerThread.Name);
                    }
                }
                
            }
        }

        protected override void OnStop()
        {
            string path = GetPath("log.txt");
            stopping = true;
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                Log("Stop Service", writer);
            }
        }


        public static void Log(string logMessage, TextWriter w)
        {
            w.WriteLine("");
            w.WriteLine("{0} : {1}", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"), logMessage);
        }

        public static void ListenerThreadProc(object queueListenerConfig)
        {

            QueueListenerConfig config = (QueueListenerConfig)queueListenerConfig;
            while (!stopping)
            {
                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                to.Timeout = TimeSpan.MaxValue;

                CommittableTransaction tran = new CommittableTransaction(to);

                try
                {

                    using (var con = new SqlConnection(config.ConnectionString))
                    {
                        con.Open();
                        con.EnlistTransaction(tran);
                        string message = ServiceBrokerUtils.GetMessageStr(config.QueueName, con, TimeSpan.FromSeconds(10));


                        if (string.IsNullOrEmpty(message)) //no message available 
                        {
                            tran.Commit();
                            con.Close();
                            continue;
                        }

                        try
                        {
                            if (config.EnlistMessageProcessor)
                            {
                                using (var ts = new TransactionScope(tran))
                                {
                                    config.MessageProcessor(message);
                                    ts.Complete();
                                }
                            }
                            else
                            {
                                config.MessageProcessor(message);
                            }

                            process_message(message);
                            /*
                            string m = "";
                            for (int i = 0; i <= message.Length; i = i + 2)
                            {
                                m=m+((char)Int32.Parse(message.GetValue(i).ToString())).ToString();
                            }*/
                            //MessageBox.Show(message.Length.ToString());

                        }
                        catch (SqlException ex) //catch selected exceptions thrown by the MessageProcessor 
                        {
                            //config.FailedMessageProcessor(message, con, ex);
                        }

                        tran.Commit(); // the message processing succeeded or the FailedMessageProcessor ran so commit the RECEIVE 
                        con.Close();

                    }
                }
                catch (SqlException ex)
                {
                    System.Diagnostics.Trace.Write("Error processing message from " + config.QueueName + ": " + ex.Message);
                    tran.Rollback();
                    tran.Dispose();
                    Thread.Sleep(1000);
                }
                ///catch any other non-fatal exceptions that should not stop the listener loop. 
                catch (Exception ex)
                {
                    Trace.WriteLine("Unexpected Exception in Thread Proc for " + config.QueueName + ".  Thread Proc is exiting: " + ex.Message);
                    tran.Rollback();
                    tran.Dispose();
                    return;
                }

            }


        }
        private static void process_message(string message)
        {
            string path = GetPath("log.txt");
            using (StreamWriter w = new StreamWriter(path, true))
            {
                Log("Receive Message: " + message, w);
                if (message == "CLEARLOG")
                {
                    DirectoryInfo di = new DirectoryInfo(GetPath(""));
                    FileInfo[] files = di.GetFiles("*.pdf")
                                         .Where(p => p.Extension == ".pdf").ToArray();
                    try
                    {
                        stopping = true;
                        Thread.Sleep(2000);
                        foreach (FileInfo file in files)
                        {
                            file.Attributes = FileAttributes.Normal;
                            File.Delete(file.FullName);
                        }
                        string pt = GetPath("log.txt");
                        w.Close();
                        File.WriteAllText(pt, "Log Clear At " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));

                        stopping = false;

                    }
                    catch (Exception exc)
                    {
                        Log("Error:" + exc.Message, w);
                    }
                }
                else if (message.StartsWith("TESTPRINT|"))
                {
                    try
                    {
                        string printer_name = message.Replace("TESTPRINT|", "");
                        print_case_label("TEST", 1, "DEST", "TMT12345", "12345", "TMT", "AIR", printer_name, w);
                    }
                    catch (Exception exc)
                    {
                        Log("Error Print:" + exc.Message, w);
                    }
                }
                else if (message == "RENEWPRINT")
                {
                    RefreshPrinter(w);
                }
                else if (message.StartsWith("CASELABEL|"))
                {
                    string batch = message.Replace("CASELABEL|", "");
                    try
                    {
                        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());

                        SqlCommand cmd = new SqlCommand("[spex].[SP_DEVICE_PRINT_CASE_LABEL]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@BATCH_ID", batch);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable o = new DataTable();
                        conn.Open();
                        da.Fill(o);
                        conn.Close();

                        if (o.Rows.Count > 0)
                        {
                            int i = 1;
                            foreach (DataRow dr in o.Rows)
                            {
                                print_case_label(batch, i, dr["DESTINATION_CODE"].ToString(), dr["CASE_NO_A"].ToString(), dr["CASE_NO_B"].ToString(), dr["CASE_NO_C"].ToString(), dr["TRANSPORT"].ToString(), dr["PRINTER"].ToString(), w);
                                i++;
                            }
                        }

                        else
                        {
                            Log("Error Batch Id :" + batch + " Not Found", w);
                        }
                    }
                    catch (Exception exc)
                    {
                        Log("Error Print Case Label:" + exc.Message, w);
                    }
                }
                else if (message.StartsWith("VANNINGLABEL|"))
                {
                    string batch = message.Replace("VANNINGLABEL|", "");
                    try
                    {
                        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());

                        SqlCommand cmd = new SqlCommand("[spex].[SP_DEVICE_PRINT_VANNING_LABEL]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@BATCH_ID", batch);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable o = new DataTable();
                        conn.Open();
                        da.Fill(o);
                        conn.Close();

                        if (o.Rows.Count > 0)
                        {
                            int i = 1;
                            foreach (DataRow dr in o.Rows)
                            {
                                print_vanning_label(batch, i, dr["VANNING_LABEL_NO"].ToString(), dr["CONTAINER_NO"].ToString(), dr["DESTINATION"].ToString(), dr["SEAL_NO"].ToString(), dr["PRINTER"].ToString(), w);
                                i++;
                            }
                        }

                        else
                        {
                            Log("Error Batch Id :" + batch + " Not Found", w);
                        }
                    }
                    catch (Exception exc)
                    {
                        Log("Error Print Vanning Label:" + exc.Message, w);
                    }
                }

                else if (message.StartsWith("PARTLABEL|"))
                {
                    string batch = message.Replace("PARTLABEL|", "");
                    try
                    {
                        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());

                        SqlCommand cmd = new SqlCommand("[spex].[SP_DEVICE_PRINT_PART_LABEL]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@BATCH_ID", batch);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable o = new DataTable();
                        conn.Open();
                        da.Fill(o);
                        conn.Close();
                        int qty = 1;
                        if (o.Rows.Count > 0)
                        {
                            int i = 1;
                            foreach (DataRow dr in o.Rows)
                            {
                                qty = Int32.Parse(dr["QTY"].ToString());
                                for (int j = 0; j < qty; j++)
                                {
                                    print_part_label(batch, i, (j + 1), dr["PART_NAME"].ToString(), dr["PART_NO"].ToString(), dr["PRINTER"].ToString(), w);
                                }
                                i++;
                            }
                        }

                        else
                        {
                            Log("Error Batch Id :" + batch + " Not Found", w);
                        }
                    }
                    catch (Exception exc)
                    {
                        Log("Error Print Part Label:" + exc.Message, w);
                    }
                }

                else if (message.StartsWith("DELIVERYNOTE|"))
                {
                    string batch = message.Replace("DELIVERYNOTE|", "");
                    try
                    {
                        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());

                        SqlCommand cmd = new SqlCommand("[spex].[SP_DEVICE_PRINT_DELIVERY_NOTE]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@BATCH_ID", batch);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable o = new DataTable();
                        conn.Open();
                        da.Fill(o);
                        conn.Close();

                        if (o.Rows.Count > 0)
                        {
                            int i = 1;
                            foreach (DataRow dr in o.Rows)
                            {
                                print_delivery_note(batch, i,
                                dr["DELIVERY_NOTE"].ToString(), dr["PENGIRIM"].ToString(), dr["KEPADA"].ToString(), dr["NO_URUT"].ToString(),
                                dr["NAMA_BARANG"].ToString(), dr["TRANSPORTATION_DESC"].ToString(), dr["CASE_NO"].ToString(), dr["CASE_QTY"].ToString()
                               , dr["CONT_NO"].ToString(), dr["VANN_LABEL"].ToString(), dr["SEAL_NO"].ToString()
                               , dr["TARE"].ToString(), dr["PRINTER"].ToString(), w);
                                i++;
                            }
                        }

                        else
                        {
                            Log("Error Batch Id :" + batch + " Not Found", w);
                        }
                    }
                    catch (Exception exc)
                    {
                        Log("Error Print Vanning Label:" + exc.Message, w);
                    }
                }
            }
        }

        private static void RefreshPrinter(StreamWriter w)
        {
            try
            {
                RefreshPrinterProc();
            }
            catch (Exception exc)
            {
                Log("Error Refresh Printer:" + exc.Message, w);
            }
        }

        private static void RefreshPrinterProc()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());

            SqlCommand cmd = new SqlCommand("INSERT INTO [spex].[TB_P_PRINTER](PRINTER_NAME,STATUS) VALUES ('','')", conn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            conn.Open();
            conn.Close();
            ExecuteSql("DELETE FROM [spex].[TB_P_PRINTER]");
            ExecuteSql("INSERT INTO [spex].[TB_P_PRINTER](PRINTER_NAME,STATUS) VALUES ('pdf','Online')");

            Printers printers = new Printers();

            string listP = GetStringFromSQL("SELECT SYSTEM_VALUE from TB_M_SYSTEM where SYSTEM_TYPE='DEVICE' AND SYSTEM_CD='FILTER_PRINTER_NAME'");
            string[] t1 = listP.Split(';');

            /*
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                string status = "Online";
                ExecuteSql("INSERT INTO [spex].[TB_P_PRINTER](PRINTER_NAME,STATUS) VALUES ('" + printer + "','" + status + "')");
            }
             * */
            foreach (Printer printer in printers)
            {
                string status = "Online";
                ExecuteSql("INSERT INTO [spex].[TB_P_PRINTER](PRINTER_NAME,STATUS) VALUES ('" + printer.PrinterName + "','" + status + "')");
            }
            /*
            foreach (Printer printer in printers)
            {
                string status = "Online";

                if (printer.Status.Offline)
                {
                    status = "Offline";
                }

                ExecuteSql("INSERT INTO [spex].[TB_P_PRINTER](PRINTER_NAME,STATUS) VALUES ('" + printer.PrinterName + "','" + status + "')");
                break;
                
                foreach (string x in t1)
                {
                   // ExecuteSql("INSERT INTO [spex].[TB_P_PRINTER](PRINTER_NAME,STATUS) VALUES ('" + x + "','" + printer.PrinterName + "')");
                    if (listP == "'ALL'" ||  printer.PrinterName.ToLower().Contains(x.ToLower()))
                    {
                        ExecuteSql("INSERT INTO [spex].[TB_P_PRINTER](PRINTER_NAME,STATUS) VALUES ('" + printer.PrinterName + "','" + status + "')");
                        break;
                    }
                }
            }*/
        }

        private static string GetPath(string fn)
        {
            return AppDomain.CurrentDomain.BaseDirectory + fn;
        }

        public static void ExecuteSql(String sql)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            con.Open();
            cmd.CommandTimeout = 0;
            cmd.ExecuteScalar();
            con.Close();
        }

        public static string GetStringFromSQL(String sql)
        {
            string o = "";
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();

            if (dt.Rows.Count > 0)
            {
                o = dt.Rows[0][0].ToString();
            }
            return o;
        }

        private static void print_case_label(string batchid, int urutan, string dist, string casenoa, string casenob, string casenoc, string transdesc, string printer_name, StreamWriter w)
        {
            string fn = GetPath("CaseLabel.btw");
            engine = new Engine(true);
            bool isPdf = false;
            if (string.IsNullOrEmpty(printer_name) || printer_name == "pdf")
            {
                isPdf = true;
                printer_name = "Foxit Reader PDF Printer";
            }
            format = engine.Documents.Open(fn);

            format.PrintSetup.PrinterName = printer_name;

            //format.Print();
            format.Prompts["DistTxt"].Value = dist;
            format.Prompts["BarcodeTxt"].Value = casenoa;
            format.Prompts["CaseNoATxt"].Value = casenoa;
            format.Prompts["CaseNoBTxt"].Value = casenob;

            format.Prompts["CaseNoCTxt"].Value = casenoc;
            format.Prompts["TransDescTxt"].Value = transdesc;

            if (isPdf)
            {
                format.ExportImageToFile("CL" + batchid + urutan.ToString() + ".pdf", ImageType.PDF, Seagull.BarTender.Print.ColorDepth.ColorDepth24bit, new Resolution(300), OverwriteOptions.Overwrite);
            }
            else
            {
                int waitForCompletionTimeout = 10000;

                Messages messages;
                Result result = format.Print(AppName, waitForCompletionTimeout, out messages);

                string messageString = "\n\nMessages:";

                foreach (Seagull.BarTender.Print.Message message in messages)
                {
                    messageString += "\n\n" + message.Text;
                }

                if (result == Result.Failure)
                    Log(messageString, w);
                else
                    Log("Success Print Case Label -" + batchid + "-" + urutan.ToString(), w);
            }

            if (engine.IsAlive)
            {
                engine.Stop();
            }
        }

        private static void print_vanning_label(string batchid, int urutan, string vanningLabel, string ContainerNo, string Destination, string SealNo, string printer_name, StreamWriter w)
        {
            string fn = GetPath("VanningLabel.btw");
            engine = new Engine(true);
            bool isPdf = false;
            if (string.IsNullOrEmpty(printer_name) || printer_name == "pdf")
            {
                isPdf = true;
                printer_name = "Foxit Reader PDF Printer";
            }
            //printer_name = @"TEC B-SX5T (305 dpi)";
            format = engine.Documents.Open(fn);

            format.PrintSetup.PrinterName = printer_name;

            //format.Print();
            format.Prompts["VanningLabelTxt"].Value = vanningLabel;
            format.Prompts["ContainerNoTxt"].Value = ContainerNo;
            format.Prompts["DestinationTxt"].Value = Destination;
            format.Prompts["SealNoTxt"].Value = SealNo;

            if (isPdf)
            {
                format.ExportImageToFile("VL" + batchid + urutan.ToString() + ".pdf", ImageType.PDF, Seagull.BarTender.Print.ColorDepth.ColorDepth24bit, new Resolution(300), OverwriteOptions.Overwrite);
            }
            else
            {
                int waitForCompletionTimeout = 10000;

                Messages messages;
                Result result = format.Print(AppName, waitForCompletionTimeout, out messages);

                string messageString = "\n\nMessages:";

                foreach (Seagull.BarTender.Print.Message message in messages)
                {
                    messageString += "\n\n" + message.Text;
                }

                if (result == Result.Failure)
                    Log(messageString, w);
                else
                    Log("Success Print Vanning Label -" + batchid + "-" + urutan.ToString(), w);
            }

            if (engine.IsAlive)
            {
                engine.Stop();
            }
        }

        private static void print_part_label(string batchid, int urutan, int counter, string PartName, string PartNo, string printer_name, StreamWriter w)
        {
            string fn = GetPath("PartLabel.btw");
            engine = new Engine(true);
            bool isPdf = false;
            if (string.IsNullOrEmpty(printer_name) || printer_name == "pdf")
            {
                isPdf = true;
                printer_name = "Foxit Reader PDF Printer";
            }
            //printer_name = @"TEC B-SX5T (305 dpi)";
            format = engine.Documents.Open(fn);

            format.PrintSetup.PrinterName = printer_name;

            //format.Print();
            format.Prompts["PartNoTxt"].Value = PartNo;
            format.Prompts["PartNameTxt"].Value = PartName;

            if (isPdf)
            {
                format.ExportImageToFile("PL" + batchid + "-" + counter.ToString() + "-" + urutan.ToString() + ".pdf", ImageType.PDF, Seagull.BarTender.Print.ColorDepth.ColorDepth24bit, new Resolution(300), OverwriteOptions.Overwrite);
            }
            else
            {
                int waitForCompletionTimeout = 10000;

                Messages messages;
                Result result = format.Print(AppName, waitForCompletionTimeout, out messages);

                string messageString = "\n\nMessages:";

                foreach (Seagull.BarTender.Print.Message message in messages)
                {
                    messageString += "\n\n" + message.Text;
                }

                if (result == Result.Failure)
                    Log(messageString, w);
                else
                    Log("Success Print Part Label -" + batchid + "-" + counter.ToString() + "-" + urutan.ToString(), w);
            }

            if (engine.IsAlive)
            {
                engine.Stop();
            }
        }


        private static void print_delivery_note(string batchid, int urutan, string No, string Pengirim
            ,
            string Kepada, string NoUrut, string NamaBarang, string TransDesc, string Satuan, string CaseQty, string ContNo,
            string VanningLabel, string SealNo, string Tare,
            string printer_name, StreamWriter w)
        {

            string fn = GetPath("DeliveryNote.btw");
            engine = new Engine(true);
            bool isPdf = false;
            if (string.IsNullOrEmpty(printer_name) || printer_name == "pdf")
            {
                isPdf = true;
                printer_name = "Foxit Reader PDF Printer";
            }
            //printer_name = @"TEC B-SX5T (305 dpi)";
            format = engine.Documents.Open(fn);

            format.PrintSetup.PrinterName = printer_name;

            //format.Print();
            format.Prompts["NoTxt"].Value = No;
            format.Prompts["PengirimTxt"].Value = Pengirim;

            format.Prompts["KepadaTxt"].Value = Kepada;
            format.Prompts["NoUrutTxt"].Value = NoUrut;
            format.Prompts["NamaBarangTxt"].Value = NamaBarang;
            format.Prompts["TransDescTxt"].Value = TransDesc;
            format.Prompts["SatuanTxt"].Value = Satuan;
            format.Prompts["CaseQtyTxt"].Value = CaseQty;
            format.Prompts["ContNoTxt"].Value = ContNo;
            format.Prompts["VanningLabelTxt"].Value = VanningLabel;
            format.Prompts["SealNoTxt"].Value = SealNo;
            format.Prompts["TareTxt"].Value = Tare;



            if (isPdf)
            {
                format.ExportImageToFile("DN" + batchid + urutan.ToString() + ".pdf", ImageType.PDF, Seagull.BarTender.Print.ColorDepth.ColorDepth24bit, new Resolution(300), OverwriteOptions.Overwrite);
            }
            else
            {
                int waitForCompletionTimeout = 10000;

                Messages messages;
                Result result = format.Print(AppName, waitForCompletionTimeout, out messages);

                string messageString = "\n\nMessages:";

                foreach (Seagull.BarTender.Print.Message message in messages)
                {
                    messageString += "\n\n" + message.Text;
                }

                if (result == Result.Failure)
                    Log(messageString, w);
                else
                    Log("Success Print Delivery Note -" + batchid + "-" + urutan.ToString(), w);
            }

            if (engine.IsAlive)
            {
                engine.Stop();
            }
        }
    }

    class QueueListenerConfig
    {
        public string QueueName { get; set; }
        public int Threads { get; set; }
        public bool EnlistMessageProcessor { get; set; }
        public Action<string> MessageProcessor { get; set; }
        public Action<string, SqlConnection, Exception> FailedMessageProcessor { get; set; }
        public string ConnectionString { get; set; }
    }

    class InboundMessageProcessor
    {
        public static void ProcessMessage(string message)
        {
            Trace.WriteLine("InboundMessageProcessor Recieved Message");
            return;
        }

        public static void SaveFailedMessage(string message, SqlConnection con, Exception errorInfo)
        {
            Trace.WriteLine("InboundMessageProcessor Recieved Failed Message");
            return;
        }
    }

    class OutboundMessageProcessor
    {
        public static void ProcessMessage(byte[] message)
        {
            Trace.WriteLine("OutboundMessageProcessor Recieved Message");
            return;
        }

        public static void SaveFailedMessage(byte[] message, SqlConnection con, Exception errorInfo)
        {
            Trace.WriteLine("OutboundMessageProcessor Recieved Failed Message");
            return;
        }
    }
}
