/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function($) {
    $.fn.tdkWebdeskLayout = function(options) {
        var CLASS_MARKER = "tdk-ui-layout-webdesk";
        var CLASS_CONTAINER = "container";
        var CLASS_TEXT = "text";
        var CLASS_BUTTON = "button";
        var CLASS_SEPARATOR = "separator";
        var CLASS_DROP_DOWN = "dropdown";
        var CLASS_PRESSED = "pressed";
        var CLASS_DISABLED = "disabled";
        
        var MENUBAR_HEIGHT = 30;         
        var MENUBAR_PADDING_LEFT = 2;
        var MENUBAR_PADDING_RIGHT = 2;
        var MENUBAR_SEPARATOR_HEIGHT = MENUBAR_HEIGHT - (MENUBAR_HEIGHT/3);
        var MENUBAR_BUTTON_ICON_SIZE = 16;

        var menuIsScrolling = false;
        var menuUpScrollingHandler;
        var menuDownScrollingHandler;

        var settings = $.extend({
            ApplicationName: "Application Name Here",
            ApplicationAbbreviation: "APP",
            ImageBasePath: "images",
            Actions: "",
            Menus: "",
            Title: "Title Here",
            DisableEyeCandy: false,
            UserProfile: {
                Username: "username",
                Name: "Full Name",
                Email: "user@email.com",
                Phone: [ "08137771625", "08568897253" ]
            },
            DateTime: {
                Time: "16:40",
                Date: "Fri, 4 November 1983"
            },
            EnableDateTimePooling: false,
            DateTimePoolingInterval: 30 /* Seconds */,
            OnDateTimeRequested: function() {
                return {
                    Time: "17:30",
                    Date: "Tue, 24 October 2035"
                };
            },
            OnHomeClicked: function() { alert("Go to main page"); },
            OnLogoutClicked: function() { alert("Exit"); },
            OnSearchClicked: function(key) { 
                var result = [
                    { Id: "result-1", Description: "Result description 1", Url: "http://www.oracle.com", Target: "_newtab" },
                    { Id: "result-2", Description: "Result description 2", Url: "http://www.oracle.com", Target: "_newtab" },
                    { Id: "result-3", Description: "Result description 3", Url: "http://www.oracle.com", Callback: function() { alert("Callback search result 3"); }, Target: "_newtab" },
                    { Id: "result-4", Description: "Result description 4", Url: "http://www.oracle.com", Target: "_newtab" },
                    { Id: "result-5", Description: "Result description 5", Url: "http://www.oracle.com", Target: "_newtab" }
                ];  
                return result;
            }
        }, options);
        
        var initContainer = function(container) {
            container.addClass(createClass(CLASS_CONTAINER));   
            container.css("margin", 0);
            container.css("padding-top", (MENUBAR_HEIGHT) + "px");
            
            return container;
        };
        var createClass = function(className) {
            return CLASS_MARKER + " " + className;
        };        
        var getElement = function(parent, className) {
            return $(parent).find("." + CLASS_MARKER + "." + className);
        };
        var appendSection = function(paramMap) {
            var params = $.extend({}, paramMap);
            params.parent.append("<div class='" + createClass(params.className) + "'/>");
            return getElement(params.parent, params.className).first();
        };
        
        var initElementWidthCalculator = function() {
            var body = $("body");
            body.append("<div class='tdk-webdesk-width-calculator'/>");
            var widthCalculator = body.children(".tdk-webdesk-width-calculator").first();
            widthCalculator.css("display", "inline-block");
            widthCalculator.hide();  
        };
        var calculateElementWidth = function(component) {
            var widthCalculator = $("body").children(".tdk-webdesk-width-calculator").first(); 
            widthCalculator.empty();
            var maxWidth = widthCalculator.append(component.clone()).outerWidth(true);            
            
            return maxWidth + 2;
        };
        var calculateElementHeight = function(component) {
            var widthCalculator = $("body").children(".tdk-webdesk-width-calculator").first();
            widthCalculator.empty();
            var maxHeight = widthCalculator.append(component.clone()).outerHeight(true);

            return maxHeight + 2;
        };
        var disposeElementWidthCalculator = function() {            
            var widthCalculator = $("body").children(".tdk-webdesk-width-calculator").first();  
            widthCalculator.remove();
        };
        
        var createMenubar = function(parent) {
            var menubar = appendSection({
                parent: parent,
                className: "menubar"
            });
            
            var browserWindow = $(window);
            
            menubar.css("position", "fixed");
            menubar.css("top", 0);
            menubar.css("left", 0);
            menubar.css("padding-top", 0);
            menubar.css("padding-bottom", 0);
            menubar.css("padding-left", MENUBAR_PADDING_LEFT);
            menubar.css("padding-right", MENUBAR_PADDING_RIGHT);
            menubar.css("height", MENUBAR_HEIGHT + "px");            
            menubar.css("width", browserWindow.width() - MENUBAR_PADDING_LEFT - MENUBAR_PADDING_RIGHT + "px");            
            menubar.css("line-height", MENUBAR_HEIGHT + "px");
            if(settings.DisableEyeCandy) {
                menubar.addClass("no-eyecandy");
            } else {
                menubar.addClass("eyecandy");
            }
            
            return menubar;
        };

        var mouseInsideDropdown = false;
        var createMenubarButton = function(paramMap) {
            var params = $.extend({
                className: "button",
                side: "left",
                displayIcon: true,
                displayText: true,
                icon: null,
                text: "Button",
                enableDropDownPanel: false,
                dropDownPanelSlideTrigger: "hover",
                dropDownWidth: -1,
                dropDownHeight: -1,
                enableTooltip: false,
                tooltipText: "Tooltip Here",
                tooltipAlignment: "center",
                OnClicked: function() { alert("Clicked"); }
            }, paramMap);
            var button = appendSection({
                parent: params.menubar,
                className: params.className
            });
            button.addClass(CLASS_BUTTON);
            button.addClass(CLASS_TEXT);
            button.css("display", "inline-block");
            button.css("position", "relative");
            if((params.side === undefined) || (params.side === "left")) {
                button.css("float", "left");
            } else {
                button.css("float", "right");
            }
            
            var displayIcon = params.displayIcon !== undefined ? params.displayIcon : false;
            var displayText = params.displayText !== undefined ? params.displayText : false;
            
            var image = null;
            if(displayIcon && (params.icon !== null)) {
                button.append("<img alt='' width='" + MENUBAR_BUTTON_ICON_SIZE + "' src='" + params.icon + "'/>");
                image = button.find("img");
                image.css("position", "relative");
                image.css("top", (MENUBAR_HEIGHT/2) - (MENUBAR_BUTTON_ICON_SIZE/2) - 4); // darimana nilai koreksi 4 ??
            }
            
            var textSpan = null;
            if(displayText) {
                button.append("<span>" + params.text + "</span>");
                textSpan = button.find("span");
                textSpan.css("position", "relative");
                textSpan.css("margin-left", "5px");
            }
            
            var dropdown = null;
            if(params.enableDropDownPanel) {
                button.append("<div class='" + CLASS_DROP_DOWN + "'/>");
                dropdown = button.find("." + CLASS_DROP_DOWN).first();
                dropdown.hide();
                dropdown.css("position", "absolute");

                var dropdownTopCorrection = 0;
                if(settings.DisableEyeCandy) {
                    dropdownTopCorrection = 1;
                }
                dropdown.css("top", button.outerHeight(true) - parseInt(dropdown.css("border-top-width"), 10) + dropdownTopCorrection);

                if(params.dropDownWidth > 0) {
                    dropdown.css("width", params.dropDownWidth + "px");
                }
                if(params.dropDownHeight > 0) {
                    dropdown.css("height", params.dropDownHeight + "px");
                }
                
                if((params.side === undefined) || (params.side === "left")) {
                    dropdown.css("left", 0);
                } else {
                    dropdown.css("right", 0);
                }
                
                dropdown.mouseenter(function() {
                    mouseInsideDropdown = true;
                });
                dropdown.mouseleave(function() {
                    mouseInsideDropdown = false;
                });
            }
            
            var openDropDownPanel = function() {
                if(!settings.DisableEyeCandy) {
                    if(displayIcon && (image !== null)) {
                        image.css("top", parseInt(image.css("top"), 10) + 1);
                    }
                    if(displayText) {
                        textSpan.css("top", parseInt(textSpan.css("top"), 10) + 1);
                    }
                }

                if(params.enableDropDownPanel) {
                    if(settings.DisableEyeCandy) {
                        dropdown.show();
                    } else {
                        dropdown.fadeIn("fast", "swing", function() {});
                    }
                }
            };
            var closeDropDownPanel = function() {
                if(!settings.DisableEyeCandy) {
                    if(displayIcon && (image !== null)) {
                        image.css("top", parseInt(image.css("top"), 10) - 1);
                    }
                    if(displayText) {
                        textSpan.css("top", parseInt(textSpan.css("top"), 10) - 1);
                    }
                }
                if(params.enableDropDownPanel) {
                    if(settings.DisableEyeCandy) {
                        dropdown.hide();
                    } else {
                        dropdown.fadeOut("fast", "swing", function() {});
                    }
                }
            };
            
            if(params.dropDownPanelSlideTrigger === "hover") {
                button.hover(function(e) {
                    e.preventDefault();
                    openDropDownPanel();
                }, function(e) {
                    e.preventDefault();
                    closeDropDownPanel();
                });
            } else {
                button.click(function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    if(!$(this).hasClass(CLASS_PRESSED)) {
                        if(!mouseInsideDropdown) {
                            $(this).addClass(CLASS_PRESSED);
                            openDropDownPanel();
                        }                        
                    } else {
                        if(!mouseInsideDropdown) {
                            closeDropDownPanel();
                            $(this).removeClass(CLASS_PRESSED);
                        }
                    }
                });
            }
            
            if(params.enableTooltip) {
                var tooltip = $(document.createElement("div"));
                    tooltip.addClass("tooltip");
                    var tooltipText = $(document.createElement("span"));
                    tooltipText.addClass("text");
                    tooltipText.text(params.tooltipText);
                    tooltip.append(tooltipText);
                    tooltip.hide();
                    tooltip.css("position", "absolute");
                    tooltip.css("top", button.outerHeight() + 5);

                    if(params.tooltipAlignment === "center") {
                        tooltip.css("left", -((Math.abs(tooltip.width() - button.width()) / 2) + 4));
                    } else if(params.tooltipAlignment === "left") {
                        tooltip.css("left", button.width() / 2);
                        tooltip.addClass("left");
                    } else if(params.tooltipAlignment === "right") {
                        tooltip.css("right", button.width() / 2);
                        tooltip.addClass("right");
                    }                    
                    
                    tooltip.width(calculateElementWidth(tooltipText));
                    button.append(tooltip);
                    
                    var tooltipConnector = $(document.createElement("div"));
                    tooltipConnector.addClass("connector");
                    tooltipConnector.css("position", "absolute");
                    tooltipConnector.css("top", -8); // mesti dicari cara supaya fleksible

                    if(params.tooltipAlignment === "center") {
                        tooltipConnector.css("left", (tooltip.width() / 2) + 4);
                    } else if(params.tooltipAlignment === "left") {
                        tooltipConnector.css("left", 0);
                    } else if(params.tooltipAlignment === "right") {
                        tooltipConnector.css("right", 0);
                    }

                    tooltipConnector.hide();
                    tooltip.append(tooltipConnector);
                    
                    button.hover(function(e) {
                        e.stopPropagation();
                        var box = $(this).find(".tooltip").first();
                        var connector = box.find(".connector").first();
                        if(settings.DisableEyeCandy) {
                            connector.show();
                            box.show();
                        } else {
                            connector.fadeIn("fast");
                            box.fadeIn("fast");
                        }
                    }, function(e) {
                        e.stopPropagation();
                        var box = $(this).find(".tooltip").first();
                        var connector = box.find(".connector").first();
                        if(settings.DisableEyeCandy) {
                            connector.hide();
                            box.hide();
                        } else {
                            connector.fadeOut("fast");
                            box.fadeOut("fast");
                        }
                    });
            }
            
            button.click(function() {
                if(!params.enableDropDownPanel) {
                    params.OnClicked();
                } 
            });
            
            return button;
        };   
        var decorateMenu = function(menuItems, parent, recalculateParentWidth, asSubmenu) {
            var rowCounter = 0;
            var listWrapper = $(document.createElement("div"));
            listWrapper.addClass("list-wrapper");
            listWrapper.css("position", "relative");
            listWrapper.css("width", "100%");
            var list = $(document.createElement("ol"));   
            list.css("position", "relative");
            list.addClass("list");
            list.css("width", "100%");
            list.css("position", "relative");
            list.css("top", 0);
            listWrapper.append(list);
            parent.append(listWrapper);

            var listItem;
            var listItemTable;
            var listItemTableRow;
            var listItemTableColumn;
            var icon;
            var hasSubmenu;
            var submenuMarker;
            $.each(menuItems, function(index, value) {                             
                listItem = $(document.createElement("li"));
                listItem.attr("id", value.Id);

                hasSubmenu = (value.Menus !== null) && (value.Menus !== undefined);
                if(hasSubmenu) {
                    listItem.addClass("has-submenu");
                }
                if(asSubmenu) {
                    listItem.addClass("children");
                }

                listItem.addClass("item");
                listItemTable = $(document.createElement("table"));
                listItemTable.css("position", "relative");
                listItemTable.css("width", "100%");
                listItem.append(listItemTable);
                listItemTableRow = $(document.createElement("tr"));
                listItemTable.append(listItemTableRow);

                listItemTableColumn = $(document.createElement("td"));
                listItemTableColumn.css("vertical-align", "middle");                    
                listItemTableColumn.addClass("icon");                    
                icon = $(document.createElement("img"));
                icon.attr("alt", "");
                icon.attr("src", settings.ImageBasePath + "/menu-item.png");
                icon.attr("width", "24");
                listItemTableColumn.css("width", icon.outerWidth(true));
                listItemTableColumn.append(icon);

                listItemTableRow.append(listItemTableColumn);
                listItemTableColumn = $(document.createElement("td"));
                listItemTableColumn.addClass("text");
                listItemTableColumn.html(
                        "<div class='title' style='display: block;'>" + value.Text + "</div>" + 
                        "<div class='description' style='display: block;'>" + value.Description + "</div>"
                );
                listItemTableRow.append(listItemTableColumn);                    
                
                if(hasSubmenu) {
                    listItemTableColumn = $(document.createElement("td"));
                    submenuMarker = $(document.createElement("img"));
                    submenuMarker.addClass("submenu-marker");                    
                    if(asSubmenu) {
                        submenuMarker.attr("src", settings.ImageBasePath +"/submenu-collapsed-hover.png");   
                    } else {
                        submenuMarker.attr("src", settings.ImageBasePath +"/submenu-collapsed.png");   
                    }                    
                    submenuMarker.css("vertical-align", "middle");
                    listItemTableColumn.append(submenuMarker); 
                    listItemTableRow.append(listItemTableColumn);                    
                }                
                
                list.append(listItem);                    
                listItem.hover(function(e) {                        
                    e.stopPropagation();
                    $(this).find(".icon img").attr("src", settings.ImageBasePath +"/menu-item-hover.png");
                    if($(this).hasClass("has-submenu")) {
                        if($(this).hasClass("expanded")) {
                            $(this).children("table").find(".submenu-marker").attr("src", settings.ImageBasePath + "/submenu-expanded.png");
                        }  else {
                            $(this).children("table").find(".submenu-marker").attr("src", settings.ImageBasePath + "/submenu-collapsed-hover.png");
                        }                        
                    }
                }, function(e) {      
                    e.stopPropagation();
                    if(!$(this).hasClass("expanded")) {
                        $(this).find(".icon img").attr("src", settings.ImageBasePath +"/menu-item.png");                        
                    }                                        
                    if($(this).hasClass("has-submenu")) {                        
                        if($(this).hasClass("expanded")) {
                            $(this).children("table").find(".submenu-marker").attr("src", settings.ImageBasePath + "/submenu-expanded.png");
                        } else {
                            if($(this).hasClass("children")) {
                                $(this).children("table").find(".submenu-marker").attr("src", settings.ImageBasePath + "/submenu-collapsed-hover.png");
                            } else {
                                $(this).children("table").find(".submenu-marker").attr("src", settings.ImageBasePath + "/submenu-collapsed.png");
                            }                            
                        }                        
                    }
                });  
                
                if(!listItem.hasClass("has-submenu")) {
                   if(value.Callback !== undefined) {
                       listItem.attr("onclick", "(" + value.Callback +")()");
                   } else if(value.Url !== "") {                       
                       if(value.Target !== undefined) {                           
                           listItem.attr("onclick", "(function() { window.open('" + value.Url + "', '" + value.Target + "'); })()");
                       } else {
                           listItem.attr("onclick", "(function() { window.location.href = '" + value.Url + "'; })()");
                       }                       
                   }
                }
                
                if((value.Menus !== null) && (value.Menus !== undefined)) {
                    var submenuWrapper = $(document.createElement("div"));
                    submenuWrapper.addClass("submenu container " + value.Id);
                    submenuWrapper.css("position", "relative");
                    submenuWrapper.css("width", "100%");
                    submenuWrapper.hide();
                    decorateMenu(value.Menus, submenuWrapper, false, true);
                    submenuWrapper.find(".list .item .icon img").hide();
                    listItem.append(submenuWrapper);
                    listItem.click(function(e) {
                        e.stopPropagation();                        
                        var sublistItem = $(this);
                        var subwrapper = parent.find(".submenu.container." + value.Id);         
                        subwrapper.css("position", "relative");
                        if(!sublistItem.hasClass("expanded")) {    
                            sublistItem.addClass("expanded");                            
                            listWrapper.children(".list:first").children(".item.expanded").each(function() {                                
                                var expandedList = $(this);                                
                                if((expandedList.attr("id") !== value.Id) && expandedList.hasClass("expanded")) {                                    
                                    var expandedSubmenuContainer = expandedList.children(".submenu.container");
                                    expandedSubmenuContainer.removeClass("expanded");
                                    if(settings.DisableEyeCandy) {
                                        expandedSubmenuContainer.hide();
                                        expandedList.removeClass("expanded");
                                        expandedList.find(".icon img").attr("src", settings.ImageBasePath +"/menu-item.png");
                                        expandedList.children("table:first").find(".submenu-marker").attr("src", settings.ImageBasePath + "/submenu-collapsed.png");
                                    } else {
                                        expandedSubmenuContainer.slideUp(150, function() {
                                            expandedList.removeClass("expanded");
                                            expandedList.find(".icon img").attr("src", settings.ImageBasePath +"/menu-item.png");
                                            expandedList.children("table:first").find(".submenu-marker").attr("src", settings.ImageBasePath + "/submenu-collapsed.png");
                                        });
                                    }
                                } 
                            });

                            if(settings.DisableEyeCandy) {
                                subwrapper.show();
                                $(this).addClass("expanded");
                                sublistItem.children("table:first").find(".submenu-marker").attr("src", settings.ImageBasePath + "/submenu-expanded.png");
                            } else {
                                subwrapper.slideDown(150, function() {
                                    $(this).addClass("expanded");
                                    sublistItem.children("table:first").find(".submenu-marker").attr("src", settings.ImageBasePath + "/submenu-expanded.png");
                                });
                            }
                        } else {
                            if(settings.DisableEyeCandy) {
                                subwrapper.hide();
                                sublistItem.children("table:first").find(".submenu-marker").attr("src", settings.ImageBasePath + "/submenu-collapsed-hover.png");
                                $(this).removeClass("expanded");
                                sublistItem.removeClass("expanded");
                            } else {
                                subwrapper.slideUp(150, function() {
                                    sublistItem.children("table:first").find(".submenu-marker").attr("src", settings.ImageBasePath + "/submenu-collapsed-hover.png");
                                    $(this).removeClass("expanded");
                                    sublistItem.removeClass("expanded");
                                });
                            }
                        }
                    });
                }

                rowCounter++;
            });
            if((listItem !== undefined) && (listItem !== null)) {
                listItem.addClass("last");
            }
            var listItems = list.children("li");                
            listItems.first().addClass("first first-displayed"); 
            
            if(!asSubmenu) {
                var scrollMenuUp = function() {
                    var top = parseInt(listWrapper.css("top"), 10);
                    var maxTop = -(listWrapper.height() - parent.height());
                    if(top > maxTop) {
                        if(Math.abs(maxTop-top) > SCROLLING_STEP) {
                            listWrapper.animate({
                                top: "-=" + SCROLLING_STEP + "px"
                            }, "fast", "swing", function() {});
                        } else {
                            listWrapper.animate({
                                top: (maxTop + 1) + "px"
                            }, "fast", "swing", function() {});
                        }
                    }
                };
                var scrollMenuDown = function() {
                    var top = parseInt(listWrapper.css("top"), 10);
                    if(top < 0) {
                        if(Math.abs(top) < SCROLLING_STEP) {
                            listWrapper.animate({
                                top: 0
                            }, "fast", "swing", function() {});
                        } else {
                            listWrapper.animate({
                                top: "+=" + SCROLLING_STEP + "px"
                            }, "fast", "swing", function() {});
                        }
                    }
                };

                var SCROLLING_STEP = 100;
                var AUTO_SCROLLING_PERIOD = 70;
                var scrollerWrapper = $(document.createElement("div"));
                scrollerWrapper.addClass("scroller");
                scrollerWrapper.css("position", "absolute");
                scrollerWrapper.css("right", 0);
                parent.append(scrollerWrapper);
                var scroller = $(document.createElement("div"));
                scroller.addClass("up");
                scrollerWrapper.append(scroller);
                var scrollerImage = $(document.createElement("img"));
                scrollerImage.attr("src", settings.ImageBasePath + "/scroller-up.png");
                scrollerImage.attr("width", 24);
                scrollerImage.css("vertical-align", "middle");
                scroller.append(scrollerImage);
                var continuousMenuUpScrolling = function() {
                    if(menuIsScrolling) {
                        scrollMenuUp();
                        menuUpScrollingHandler = setTimeout(function() {
                            continuousMenuUpScrolling();
                        }, AUTO_SCROLLING_PERIOD);
                    }
                };
                scroller.hover(function(e) {
                    if(!menuIsScrolling) {
                        menuIsScrolling = true;
                        continuousMenuUpScrolling();
                    }
                }, function(e) {
                    menuIsScrolling = false;
                    clearTimeout(menuUpScrollingHandler);
                });

                scroller = $(document.createElement("div"));
                scroller.addClass("down");
                scrollerWrapper.append(scroller);
                scrollerImage = $(document.createElement("img"));
                scrollerImage.attr("src", settings.ImageBasePath + "/scroller-down.png");
                scrollerImage.attr("width", 24);
                scrollerImage.css("vertical-align", "middle");
                scroller.append(scrollerImage)
                var continuousMenuDownScrolling = function() {
                    if(menuIsScrolling) {
                        scrollMenuDown();
                        menuDownScrollingHandler = setTimeout(function() {
                            continuousMenuDownScrolling();
                        }, AUTO_SCROLLING_PERIOD);
                    }
                };
                scroller.hover(function(e) {
                    if(!menuIsScrolling) {
                        menuIsScrolling = true;
                        continuousMenuDownScrolling();
                    }
                }, function(e) {
                    menuIsScrolling = false;
                    clearTimeout(menuDownScrollingHandler);
                });

                var menuWheelUpScrolling = function() {
                    menuUpScrollingHandler = setTimeout(function() {
                        scrollMenuUp();
                        clearTimeout(menuUpScrollingHandler);
                        menuIsScrolling = false;
                    }, AUTO_SCROLLING_PERIOD);
                };
                var menuWheelDownScrolling = function() {
                    menuUpScrollingHandler = setTimeout(function() {
                        scrollMenuDown();
                        clearTimeout(menuDownScrollingHandler);
                        menuIsScrolling = false;
                    }, AUTO_SCROLLING_PERIOD);
                };

                listWrapper.mousewheel(function(event, delta) {
                    event.stopImmediatePropagation();
                    if(delta > 0) {
                        if(!menuIsScrolling) {
                            menuIsScrolling = true;
                            menuWheelUpScrolling();
                        }
                        if(!menuIsScrolling) {
                            menuWheelUpScrolling();
                        }
                    } else {
                        if(!menuIsScrolling) {
                            menuIsScrolling = true;
                            menuWheelDownScrolling();
                        }
                        if(!menuIsScrolling) {
                            menuWheelDownScrolling();
                        }
                    }

                    return false;
                });
            }
            
            if(recalculateParentWidth) {
                parent.width(calculateElementWidth(listWrapper));
            }
        };
        var createMenu = function(menubar) { 
            var menus = settings.Menus;
            if((menus !== null) && (menus.Items !== undefined) && (menus.Items !== null)) {
                var button = createMenubarButton({
                    menubar: menubar,
                    className: "menu",
                    text: "Menu",
                    side: "right",
                    displayIcon: true,
                    displayText: true,
                    enableDropDownPanel: true,
                    dropDownPanelSlideTrigger: "click",
                    icon: settings.ImageBasePath + "/menu-down.png"
                });

                button.click(function() {
                    var image = button.children("img");
                    if($(this).hasClass(CLASS_PRESSED)) {                        
                        image.attr("src", settings.ImageBasePath + "/menu-up.png");
                    } else {
                        image.attr("src", settings.ImageBasePath + "/menu-down.png");
                    }
                });
                
                var dropdown = button.find("." + CLASS_DROP_DOWN).first();                    
                decorateMenu($(menus.Items), dropdown, true, false);

                var listWrapper = dropdown.children(".list-wrapper:first");
                var listWrapperHeight = calculateElementHeight(listWrapper);
                var targetHeight = $(window).height() * 0.75;
                if(listWrapperHeight <= targetHeight) {
                    targetHeight = listWrapperHeight;
                }
                dropdown.height(targetHeight);

                var scroller = dropdown.children(".scroller:first");
                scroller.css("top", (dropdown.height() / 2) - 48); // 48: asumsi fixed image 24x24
            }
            
            return button;
        };
        var createMenubarSeparator = function(paramMap) {
            var params = $.extend({}, paramMap);
            var separator = appendSection({
                parent: params.menubar,
                className: CLASS_SEPARATOR
            });            
            
            separator.css("height", MENUBAR_SEPARATOR_HEIGHT + "px");
            separator.css("width", "1px");
            separator.css("display", "inline-block");
            
            return separator;
        };
        var createMenubarSearch = function(menubar) {
            var button = createMenubarButton({
                menubar: menubar,
                className: "search",
                text: "Search",
                side: "right",
                displayText: false,
                enableDropDownPanel: true,
                icon: settings.ImageBasePath + "/search.png"
            });  
            
            var dropdown = button.find("." + CLASS_DROP_DOWN).first();
            dropdown.append("<div class='input'/>");
            var inputWrapper = dropdown.find(".input").first();
            inputWrapper.append("<table class='layout'/>");
            var inputLayout = inputWrapper.children("table").first();
            inputLayout.append("<tr>");
            inputLayout.append("<td class='label'>Search</td>");
            inputLayout.append("<td class='text'><input type='text'/></td>");
            inputLayout.append("<td class='trigger'><input type='button' value='GO'/></td>");
            inputLayout.append("</tr>");
            
            var inputText = inputLayout.find(".text:first input:first");
            var btGo = inputLayout.find(".trigger:first input:first");
            btGo.click(function() {
                if((settings.OnSearchClicked !== undefined) && (settings.OnSearchClicked !== null)) {
                    var results = settings.OnSearchClicked($.trim(inputText.val()));
                    if((results !== undefined) && (results !== null)) {
                        var resultList = dropdown.find(".result-list:first");
                        resultList.empty();
                        var resultItem;
                        var qResults = $(results);
                        qResults.each(function(index, value) {
                            resultItem = $(document.createElement("li"));
                            resultItem.addClass("result-item");
                            resultItem.attr("id", value.Id);
                            resultItem.text(value.Description);
                            resultList.append(resultItem);
                            
                            if((value.Callback !== undefined) && (value.Callback !== null)) {
                                resultItem.attr("onclick", "(" + value.Callback + ")()");
                            } else if((value.Url !== undefined) && (value.Url !== null)) {
                                resultItem.attr("onclick", "javascript:window.open('" + value.Url + "','" + value.Target + "')");
                            }
                        });
                        resultList.fadeIn("fast", function() {
                            if(qResults.size() > 0) {
                                dropdown.addClass("has-result");
                            }
                        });
                    }
                }
            });
            btGo.mousedown(function() {
                $(this).addClass("pressed");
            });
            btGo.mouseup(function() {
                $(this).removeClass("pressed");
            });
            
            var resultList = $(document.createElement("ul"));
            resultList.addClass("result-list");
            resultList.css("width", "100%");
            resultList.hide();
            dropdown.append(resultList);
            
            inputText.keypress(function(e) {
                e.stopPropagation();
                var textValue = inputText.val();
                if(textValue === "") {
                    resultList.empty();
                    resultList.fadeOut("fast");
                }
            });
            
            return button;
        };
        var createMenubarTimestamp = function(menubar) {
            var button = createMenubarButton({
                menubar: menubar,
                className: "timestamp",
                text: (settings.DateTime !== undefined) && (settings.DateTime !== null) ? ((settings.DateTime.Time !== undefined) && (settings.DateTime.Time !== null) ? settings.DateTime.Time : "Unknown") : "Unknown",
                side: "right",
                enableDropDownPanel: true,
                dropDownWidth: 180,
                icon: settings.ImageBasePath + "/timestamp.png"
            });  
            
            var dropdown = button.find("." + CLASS_DROP_DOWN).first();
            dropdown.append("<div class='description'/>");
            var description = dropdown.children(".description").first();
            description.text("Unknown");
            if((settings.DateTime !== undefined) && (settings.DateTime !== null) && (settings.DateTime.Date !== undefined) && (settings.DateTime.Date !== null)) {
                description.text(settings.DateTime.Date);
            }
            description.css("text-align", "center");
            
            if(settings.EnableDateTimePooling && (settings.DateTimePoolingInterval !== undefined) && (settings.OnDateTimeRequested !== undefined)) {                
                var timeReload = function() {
                    var dateTime = settings.OnDateTimeRequested();
                    if((dateTime !== undefined) && (dateTime !== null)) {
                        if((dateTime.Time !== undefined) && (dateTime.Time !== null)) {
                            button.children("span").text(dateTime.Time);
                        } else {
                            button.children("span").text("Unknown");
                        }
                        if((dateTime.Date !== undefined) && (dateTime.Date !== null)) {
                            description.text(dateTime.Date);
                        } else {
                            description.text("Unknown");
                        }
                    }
                    
                    setTimeout(function() {
                        timeReload();
                    }, settings.DateTimePoolingInterval * 1000);
                };
                
                timeReload();
            }
            
            return button;
        };
        var createMenubarUserProfile = function(menubar) {
            var button = createMenubarButton({
                menubar: menubar,
                className: "user",
                text: (settings.UserProfile !== undefined) && (settings.UserProfile !== null) ? settings.UserProfile.Username : "Anonymous",
                side: "right",
                enableDropDownPanel: true,
                icon: settings.ImageBasePath + "/user.png"
            });
            var dropdown = button.find("." + CLASS_DROP_DOWN).first();
            
            var applicationInfo = $(document.createElement("div"));
            applicationInfo.addClass("application-info");
            applicationInfo.css("width", "100%");
            applicationInfo.css("text-align", "center");
            dropdown.append(applicationInfo);
            
            var applicationName = $(document.createElement("div"));
            applicationName.addClass("name");
            applicationInfo.append(applicationName);
            applicationName.text(settings.ApplicationName);
            
            if((settings.ApplicationAbbreviation !== undefined) && (settings.ApplicationAbbreviation !== null) && (settings.ApplicationAbbreviation !== "")) {
                var applicationAbbreviation = $(document.createElement("div"));
                applicationAbbreviation.addClass("abbreviation");
                applicationAbbreviation.text(settings.ApplicationAbbreviation);
                applicationInfo.append(applicationAbbreviation);
            }
            
            dropdown.append("<div class='picture'/>");
            var picture = dropdown.children(".picture").first();
            picture.append("<img alt='' src='" + settings.ImageBasePath + "/user-picture.jpg'/>");
            
            dropdown.append("<div class='profile'/>");           
            var profile = dropdown.children(".profile");
            profile.append("<table class='layout'/>");
            var profileLayout = profile.children(".layout").first();
            profileLayout.css("width", "100%");
            
            if((settings.UserProfile !== undefined) && (settings.UserProfile !== null)) {
                var profileData = settings.UserProfile;
                if((profileData.Name !== undefined) && (profileData.Name !== null)) {
                    profileLayout.append(
                        "<tr class='row'>" +
                        "   <td class='key'>Name</td>" +
                        "   <td class='value'>" + profileData.Name + "</td>" +
                        "</tr>"
                    );
                }
                if((profileData.Email !== undefined) && (profileData.Email !== null)) {
                    profileLayout.append(
                        "<tr class='row'>" +
                        "   <td class='key'>Email</td>" +
                        "   <td class='value'>" + profileData.Email + "</td>" +
                        "</tr>"
                    );
                }
                if((profileData.Phone !== undefined) && (profileData.Phone !== null)) {
                    profileLayout.append(
                        "<tr class='row'>" +
                        "   <td class='key'>Phone</td>" +
                        "   <td class='value'><ul></ul></td>" +
                        "</tr>"
                    );
                    var phoneList = profileLayout.find(".value:last ul:first");
                    $.each(profileData.Phone, function(index, value) {
                        phoneList.append("<li style=''>" + value + "</li>");
                    });
                }
            }
            
            var keys = profileLayout.find(".key");
            keys.css("text-align", "right");
            keys.css("padding-right", "5px");
            var values = profileLayout.find(".value");
            values.css("padding-left", "5px");
            
            return button;
        };
        
        var createActionBar = function(menubar) {
            var button = null;            
            var actionItems = settings.Actions;
            if(actionItems !== null) {
                button = createMenubarButton({
                    menubar: menubar,
                    className: "actions",
                    text: "Actions",
                    side: "right",
                    enableDropDownPanel: true,
                    displayText: false,
                    icon: settings.ImageBasePath + "/actions.png"
                });                
                
                var MAX_ROW = 10;
                var row;
                var list = $(document.createElement("table"));
                list.addClass("list");
                list.css("width", "100%");
                list.css("margin", 0);
                list.css("padding", 0);      
                for(var i = 0; i < MAX_ROW; i++) {
                    list.append("<tr id='" + i + "'/>");
                }
                
                var rows = list.find("tr");
                var rowCounter;
                var renderQuickAccess = (actionItems.QuickAccess !== null) && (actionItems.QuickAccess !== undefined);
                var quickAccessCount = renderQuickAccess ? actionItems.QuickAccess.length : 0;
                var quickAccess = [];
                $(actionItems.Items).each(function(index, value) {    
                    if((row === null) || (row === undefined)) {
                        row = rows.first();
                    } else {
                        if(rowCounter >= MAX_ROW) {
                            row = rows.first();
                            rowCounter = 0;
                        } else {
                            row = row.next();
                        }     
                    }
                    
                    row.append(
                        "<td class='action' align='center'>" +
                        "   <img style='position: relative; vertical-align: middle; padding-right: 5px;' class='icon' alt='' width='24' src='" + settings.ImageBasePath + "/actions/" + value.Icon + ".png'/>" +
                        "   <a style='position: relative;' href='" + value.Url + "' class='text'>" + value.Text + "</a>" +
                        "</td>"
                    );                    
                    
                    if(renderQuickAccess) {
                        for(var i = 0; i < quickAccessCount; i++) {   
                            if(actionItems.QuickAccess[i] === value.Id) {                                
                                quickAccess.push(value);                 
                                break;
                            }
                        }
                    }           
                             
                    rowCounter++;
                });               
                
                var actionImage;
                var actionText;
                list.find(".action").each(function() {
                    $(this).hover(function() {
                        actionImage = $(this).children("img").first();                        
                        actionImage.css("top", parseInt(actionImage.css("top"), 10) + 1);
                        
                        actionText = $(this).children("a").first();
                        actionText.css("top", parseInt(actionText.css("top"), 10) + 1);
                    }, function() {
                        actionImage = $(this).children("img").first();                        
                        actionImage.css("top", parseInt(actionImage.css("top"), 10) - 1);
                        
                        actionText = $(this).children("a").first();
                        actionText.css("top", parseInt(actionText.css("top"), 10) - 1);
                    });
                });                
                
                var maxWidth = calculateElementWidth(list);                
                var dropdown = button.find("." + CLASS_DROP_DOWN).first();                                  
                dropdown.width(maxWidth + (maxWidth/3));
                dropdown.append(list);
                
                var quickAccessButton;
                var quickAccessDropdown;                
                $.each(quickAccess, function(index, value) { 
                    quickAccessButton = createMenubarButton({
                        menubar: menubar,
                        className: value.Id,
                        text: value.Text,
                        side: "right",
                        displayText: false,
                        enableDropDownPanel: false,
                        icon: settings.ImageBasePath + "/actions/" + value.Icon + ".png",
                        enableTooltip: true,
                        tooltipText: value.Description,
                        OnClicked: value.Callback !== undefined ? function() { value.Callback(); } : function() { window.location.href = value.Url; }
                    });
                    
                    quickAccessButton.addClass("quick-action");
                    quickAccessDropdown = quickAccessButton.find("." + CLASS_DROP_DOWN).first();                            
                    quickAccessButton.mousedown(function() {
                        $(this).addClass(CLASS_PRESSED);
                    });
                    quickAccessButton.mouseup(function() {
                        $(this).removeClass(CLASS_PRESSED);
                    });
                });
            }            
            
            return button;
        };
        
        return this.each(function() {
            var container = initContainer($(this));            
            initElementWidthCalculator();
            
            var menubar = createMenubar(container);
            var home = createMenubarButton({
                menubar: menubar,
                className: "home",
                text: "Home",
                displayText: false,
                enableTooltip: true,
                tooltipText: "Back to main page",
                tooltipAlignment: "left",
                icon: settings.ImageBasePath + "/home.png",
                OnClicked: settings.OnHomeClicked
            });          
            createMenubarSeparator({ menubar: menubar, className: "home-separator" }).insertAfter(home);            
            
            var pageTitle = appendSection({
                parent: menubar,
                className: "page-title"
            });
            
            pageTitle.css("display", "inline");
            pageTitle.addClass(CLASS_TEXT);
            pageTitle.text(settings.Title);
            
            createMenubarButton({
                menubar: menubar,
                className: "logout",
                text: "Logout",
                side: "right",
                displayText: false,
                icon: settings.ImageBasePath + "/logout.png",
                enableTooltip: true,
                tooltipText: "Logout",
                tooltipAlignment: "right",
                OnClicked: settings.OnLogoutClicked
            }); 
            createMenubarSearch(menubar);
            createMenubarTimestamp(menubar);
            createMenubarUserProfile(menubar);
            createMenu(menubar);                      
            createActionBar(menubar);       
            
            disposeElementWidthCalculator();

            $(window).resize(function(e) {
                menubar.width($(document).width());
            });
        });
    };
})(jQuery);

