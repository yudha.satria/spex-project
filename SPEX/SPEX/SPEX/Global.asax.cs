using System.Web.Mvc;
using DevExpress.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Credential.TMMIN;
using Toyota.Common.Web.Platform;
//added by kevin
//using Toyota.Common.Credential.SingleSignOn;


namespace SPEX
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801


   /*
    * Write your basic settings here. 
    * Most of application's basic settings can be defined at Startup(), 
    * but some of them may need to be defined at the constructor.
    * So shortly, just put any basic settings here.
    */ 
    public class MvcApplication : WebApplication
    {
        public MvcApplication()
            : base("Web Starter Project")
        {
            /*
             * Turn this setting on to enable authentication mode.
             * This will enable login screen redirection.
             *
            ApplicationSettings.Instance.Security.EnableAuthentication = true; */

            /*
             * Enable menu management.
             */
            ApplicationSettings.Instance.Security.SimulateAuthenticatedSession = false;//false
            ApplicationSettings.Instance.Security.SimulatedAuthenticatedUser = new User() { Username = "spex.user" };
            //ApplicationSettings.Instance.Security.SimulatedAuthenticatedUser = new User() { Username = "Simulated.User.New" };
            //ApplicationSettings.Instance.Security.SimulatedAuthenticatedUser.Roles.Add(new Role() { Id = "R-SPEX-AD" });
            
            //added by kevin
            ApplicationSettings.Instance.Security.IgnoreAuthorization = false;
            ApplicationSettings.Instance.Security.EnableAuthentication = true;
            ApplicationSettings.Instance.Menu.Enabled = true;
            ApplicationSettings.Instance.Security.EnableSingleSignOn = false;

            //buat trial
            //ApplicationSettings.Instance.Security.IgnoreAuthorization = true;
            //ApplicationSettings.Instance.Security.EnableAuthentication = false;
            //ApplicationSettings.Instance.Menu.Enabled = true;

            //buat trial agi
            //ApplicationSettings.Instance.Security.IgnoreAuthorization = true;
            //ApplicationSettings.Instance.Security.EnableAuthentication = false;
            //ApplicationSettings.Instance.Menu.Enabled = true;

            //ApplicationSettings.Instance.Security.SimulateAuthenticatedSession = true;//block
            //ApplicationSettings.Instance.Security.SimulatedAuthenticatedUser = new User() { Username = "alira.agi" };//block
            //ApplicationSettings.Instance.Security.SimulatedAuthenticatedUser = new User() { Username = "spex.admin" };//block
            //ApplicationSettings.Instance.Security.SimulatedAuthenticatedUser.Roles.Add(new Role() { Id = "R-SPEX-AD" });//block
            
            //ApplicationSettings.Instance.Security.EnableSingleSignOn = true;            
            //ApplicationSettings.Instance.Security.UnauthorizedController = "CustomUnauthorizedAccess";

        }

        /*
         * Write your startup codes here
         */ 
        protected override void Startup()
        {
            //added by kevin
            //ProviderRegistry.Instance.Register<IUserProvider>(typeof(UserProvider));
            //ProviderRegistry.Instance.Register<IUserProvider>(typeof(SingleSignOnUserProvider), "http://localhost/TmminSSO/TmminSSOService.svc?wsdl");
            //ProviderRegistry.Instance.Register<ISessionProvider>(typeof(SingleSignOnSessionProvider), "http://localhost/TmminSSO/TmminSSOService.svc?wsdl");
            //ProviderRegistry.Instance.Register<IUserProvider>(typeof(SingleSignOnUserProvider), Configurations.Instance.SingleSignOn_Url);
            //ProviderRegistry.Instance.Register<ISessionProvider>(typeof(SingleSignOnSessionProvider), Configurations.Instance.SingleSignOn_Url);
            
            ProviderRegistry.Instance.Register<IUserProvider>(typeof(TmminUserProvider), DatabaseManager.Instance, "SecurityCenter");
            ModelBinders.Binders.DefaultBinder = new DevExpressEditorsBinder();
        }
    }
}