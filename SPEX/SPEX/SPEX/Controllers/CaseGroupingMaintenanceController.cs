﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class CaseGroupingMaintenanceController : PageController
    {
        MessagesString msgError = new MessagesString();
        public string moduleID = "SPX11";
        public string functionCaseGroupingID = "OD012";
        public string functionFinishCaseID = "OD013";
        public const string UploadDirectory = "Content\\FileUploadResult";
        mCaseGroupingMaintenance CancelOrder = new mCaseGroupingMaintenance();

        public const string tb_t_case_grouping_revise = "spex.TB_T_CASE_GROUPING_REVISE";
        public const string tb_t_finish_case_revise = "spex.TB_T_FINISH_CASE_REVISE";
        public const string tb_t_cancel_order_revise = "spex.TB_T_CANCEL_MANIFEST";

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public CaseGroupingMaintenanceController()
        {
            Settings.Title = "Case Grouping Maintenance";
        }

        protected override void Startup()
        {
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Case Grouping Maintenance");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }
        
        public class UploadControlHelperRevise
        {
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".csv" },
                MaxFileSize = 1120971520
            };
        }

        public class UploadControlHelper
        {
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xlsx" },
                MaxFileSize = 1120971520
            };
        }

        public ActionResult CallbackUploadCaseGrouping()
        {
            UploadControlExtension.GetUploadedFiles("UploadCaseGrouping", UploadControlHelperRevise.ValidationSettings, uc_FileUploadCaseGroupingComplete);
            return null;
        }

        public void uc_FileUploadCaseGroupingComplete(object sender, FileUploadCompleteEventArgs e)
        {
            mCaseGroupingMaintenance CGM = new mCaseGroupingMaintenance();
            string rExtract = string.Empty;
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            long pid = 0;
            
            Lock L = new Lock();
            string functionIDUpload = functionCaseGroupingID;
            int IsLock = L.is_lock(functionIDUpload, out pid);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(pid.ToString());
            }

            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = CGM.DeleteCaseGroupingDataTBT();
                if (rExtract == "SUCCESS")
                {
                    string resultFile = fileNameUpload + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, resultFile);

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);
                    
                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_case_grouping_revise, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    rExtract = resultFile;
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        public ActionResult UploadCaseGroupingBackgroundProsess(String filename)
        {
            mCaseGroupingMaintenance CGM = new mCaseGroupingMaintenance();
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;

            string resultMessage = CGM.UploadCaseGroupingBackgroundProcess(UserID);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                //msg = msgError.getMSPXCG02INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                msg = msgError.getMSPXCG02INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                //msg = msgError.getMSPXCG01INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                msg = msgError.getMSPXCG01INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CallbackUploadFinishCase()
        {
            UploadControlExtension.GetUploadedFiles("UploadFinishCase", UploadControlHelperRevise.ValidationSettings, uc_FileUploadFinishCaseComplete);
            return null;
        }

        public ActionResult CallbackUploadCancelOrder() //add fid.mario 28/02/2018
        {
            UploadControlExtension.GetUploadedFiles("UploadCancelOrder", UploadControlHelperRevise.ValidationSettings, uc_FileUploadCancelOrderComplete);
            //UploadControlExtension.GetUploadedFiles("UploadCancelOrder", UploadControlHelperRevise.ValidationSettings, uc_FileUploadCancelOrderComplete);
            //UploadControlExtension.GetUploadedFiles("UploadCancelOrder", UploadControlHelper.ValidationSettings, FileUploadComplete);
            return null;
        }

        private string FunctionID = "OD014";
        private string ModuleID = "SPX11";
        public void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            //mCaseGroupingMaintenance CaseGrouping = new mCaseGroupingMaintenance();
            //CaseGrouping.CREATED_BY = getpE_UserId.Username;

            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_CANCEL_MANIFEST";
            #region createlog
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);

            string templateFileName = "TemplateUploadCancelOrder";
            string UploadDirectory = Server.MapPath("~/Content/FileUploadResult");
            long ProcessID = 0;
            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock lockTable = new Lock();

            int IsLock = lockTable.is_lock(FunctionID, out ProcessID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(ProcessID.ToString());
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                //rExtract = packingCompletion.DeleteTempData();
                rExtract = CancelOrder.DeleteTempCancelOrder();
                if (rExtract == "SUCCESS")
                {
                    string resultFilePath = UploadDirectory + templateFileName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, templateFileName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);
                    rExtract = ReadAndInsertExcelData(pathfile);
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .xls OR xlsx";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        private string ReadAndInsertExcelData(string FileName)
        {
            try
            {
                string UserID = getpE_UserId.Username;
                //mCaseGroupingMaintenance CaseGrouping = new mCaseGroupingMaintenance();
                XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                ISheet sheet = xssfworkbook.GetSheetAt(0);
                int lastrow = sheet.LastRowNum;
                List<mCaseGroupingMaintenance.mCancelOrder> batalOrder = new List<mCaseGroupingMaintenance.mCancelOrder>();
                int index = 1;

                DataFormatter dataFormatter = new DataFormatter(CultureInfo.CurrentCulture);

                mCaseGroupingMaintenance.mCancelOrder CancelOrder;
                for (int i = index; i <= lastrow; i++)
                {
                    CancelOrder = new mCaseGroupingMaintenance.mCancelOrder();
                    IRow row = sheet.GetRow(i);

                    //if (row == null) continue;
                    //if (row.GetCell(0) == null && row.GetCell(1) == null && row.GetCell(2) == null) continue;

                    //DATAID
                    //if (row.GetCell(0) == null) CancelOrder.DATAID = "";
                    //else if (row.GetCell(0).CellType == CellType.Blank || (row.GetCell(0).CellType == CellType.String && (row.GetCell(0).StringCellValue == null || row.GetCell(0).StringCellValue == "")))
                    //    CancelOrder.DATAID = "";
                    //else if (row.GetCell(0).CellType == CellType.Numeric && (row.GetCell(0).NumericCellValue.ToString() == ""))
                    //    CancelOrder.DATAID = "";
                    //else if (row.GetCell(0).CellType == CellType.String)
                    //    CancelOrder.DATAID = row.GetCell(0).StringCellValue;
                    //else if (row.GetCell(0).CellType == CellType.Numeric)
                    //{
                    //    ICell cell = row.GetCell(0);
                    //    string formatString = row.GetCell(0).CellStyle.GetDataFormatString();

                    //    if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                    //        CancelOrder.DATAID = row.GetCell(0).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                    //    else
                    //        CancelOrder.DATAID = row.GetCell(0).NumericCellValue.ToString();
                    //}
                    //else
                        CancelOrder.DATAID = row.GetCell(0).ToString();

                    //if (CancelOrder.DATAID.ToLower() != "d")
                    //    continue;

                    //MANIFEST_NO
                    //if (row.GetCell(1) == null) CancelOrder.MANIFEST_NO = "";
                    //else if (row.GetCell(1).CellType == CellType.Blank || (row.GetCell(1).CellType == CellType.String && (row.GetCell(1).StringCellValue == null || row.GetCell(1).StringCellValue == "")))
                    //    CancelOrder.MANIFEST_NO = "";
                    //else if (row.GetCell(1).CellType == CellType.Numeric && (row.GetCell(1).NumericCellValue.ToString() == ""))
                    //    CancelOrder.MANIFEST_NO = "";
                    //else if (row.GetCell(1).CellType == CellType.String)
                    //    CancelOrder.MANIFEST_NO = row.GetCell(1).StringCellValue;
                    //else if (row.GetCell(1).CellType == CellType.Numeric)
                    //{
                    //    ICell cell = row.GetCell(1);
                    //    string formatString = row.GetCell(1).CellStyle.GetDataFormatString();

                    //    //string value = dataFormatter.FormatCellValue(cell);
                    //    if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                    //        CancelOrder.MANIFEST_NO = row.GetCell(1).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                    //    else
                    //        CancelOrder.MANIFEST_NO = row.GetCell(1).NumericCellValue.ToString();
                    //}
                    //else
                        CancelOrder.MANIFEST_NO = row.GetCell(1).ToString();

                    //TMAP_ORDER_NO
                    //if (row.GetCell(2) == null) CancelOrder.TMAP_ORDER_NO = "";
                    //else if (row.GetCell(2).CellType == CellType.Blank || (row.GetCell(2).CellType == CellType.String && (row.GetCell(2).StringCellValue == null || row.GetCell(2).StringCellValue == "")))
                    //    CancelOrder.TMAP_ORDER_NO = "";
                    //else if (row.GetCell(2).CellType == CellType.Numeric && (row.GetCell(2).NumericCellValue.ToString() == ""))
                    //    CancelOrder.TMAP_ORDER_NO = "";
                    //else if (row.GetCell(2).CellType == CellType.String)
                    //    CancelOrder.TMAP_ORDER_NO = row.GetCell(2).StringCellValue;
                    //else if (row.GetCell(2).CellType == CellType.Numeric)
                    //{
                    //    ICell cell = row.GetCell(2);
                    //    string formatString = row.GetCell(2).CellStyle.GetDataFormatString();

                    //    //string value = dataFormatter.FormatCellValue(cell);
                    //    if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                    //        CancelOrder.TMAP_ORDER_NO = row.GetCell(2).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                    //    else
                    //        CancelOrder.TMAP_ORDER_NO = row.GetCell(2).NumericCellValue.ToString();
                    //}
                    //else
                        CancelOrder.TMAP_ORDER_NO = row.GetCell(2).ToString();

                    //TMAP_ITEM_NO
                    //if (row.GetCell(3) == null) CancelOrder.TMAP_ITEM_NO = "";
                    //else if (row.GetCell(3).CellType == CellType.Blank || (row.GetCell(3).CellType == CellType.String && (row.GetCell(3).StringCellValue == null || row.GetCell(3).StringCellValue == "")))
                    //    CancelOrder.TMAP_ITEM_NO = "";
                    //else if (row.GetCell(3).CellType == CellType.Numeric && (row.GetCell(3).NumericCellValue.ToString() == ""))
                    //    CancelOrder.TMAP_ITEM_NO = "";
                    //else if (row.GetCell(3).CellType == CellType.String)
                    //    CancelOrder.TMAP_ITEM_NO = row.GetCell(3).StringCellValue;
                    //else if (row.GetCell(3).CellType == CellType.Numeric)
                    //{
                    //    ICell cell = row.GetCell(3);
                    //    string formatString = row.GetCell(3).CellStyle.GetDataFormatString();

                    //    //string value = dataFormatter.FormatCellValue(cell);
                    //    if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                    //        CancelOrder.TMAP_ITEM_NO = row.GetCell(3).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                    //    else
                    //        CancelOrder.TMAP_ITEM_NO = row.GetCell(3).NumericCellValue.ToString();
                    //}
                    //else
                        CancelOrder.TMAP_ITEM_NO = row.GetCell(3).ToString();

                    //TMAP_ITEM_NO
                    //if (row.GetCell(4) == null) CancelOrder.TMAP_PART_NO = "";
                    //else if (row.GetCell(4).CellType == CellType.Blank || (row.GetCell(4).CellType == CellType.String && (row.GetCell(4).StringCellValue == null || row.GetCell(4).StringCellValue == "")))
                    //    CancelOrder.TMAP_PART_NO = "";
                    //else if (row.GetCell(4).CellType == CellType.Numeric && (row.GetCell(4).NumericCellValue.ToString() == ""))
                    //    CancelOrder.TMAP_PART_NO = "";
                    //else if (row.GetCell(4).CellType == CellType.String)
                    //    CancelOrder.TMAP_PART_NO = row.GetCell(4).StringCellValue;
                    //else if (row.GetCell(4).CellType == CellType.Numeric)
                    //{
                    //    ICell cell = row.GetCell(4);
                    //    string formatString = row.GetCell(4).CellStyle.GetDataFormatString();

                    //    //string value = dataFormatter.FormatCellValue(cell);
                    //    if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                    //        CancelOrder.TMAP_PART_NO = row.GetCell(4).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                    //    else
                    //        CancelOrder.TMAP_PART_NO = row.GetCell(4).NumericCellValue.ToString();
                    //}
                    //else
                        CancelOrder.TMAP_PART_NO = row.GetCell(4).ToString();

                    batalOrder.Add(CancelOrder);
                }

                foreach (mCaseGroupingMaintenance.mCancelOrder data in batalOrder)
                {
                    string insertResult = (new mCaseGroupingMaintenance()).InsertCancelOrderTemp(data, UserID);
                }

                return "0|success";
            }
            catch (Exception ex)
            {
                return "1|" + ex.Message;
            }
        }

        public void uc_FileUploadFinishCaseComplete(object sender, FileUploadCompleteEventArgs e)
        {
            mCaseGroupingMaintenance CGM = new mCaseGroupingMaintenance();
            string rExtract = string.Empty;
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            long pid = 0;
            
            Lock L = new Lock();
            string functionIDUpload = functionFinishCaseID;
            int IsLock = L.is_lock(functionIDUpload, out pid);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(pid.ToString());
            }

            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = CGM.DeleteFinishCaseDataTBT();
                if (rExtract == "SUCCESS")
                {
                    string resultFile = fileNameUpload + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, resultFile);

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);
                    
                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_finish_case_revise, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    rExtract = resultFile;
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        public void uc_FileUploadCancelOrderComplete(object sender, FileUploadCompleteEventArgs e) //add fid.mario 28/02/2018
        {
            mCaseGroupingMaintenance CGM = new mCaseGroupingMaintenance();
            string rExtract = string.Empty;
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            long pid = 0;

            Lock L = new Lock();
            string functionIDUpload = functionFinishCaseID;
            int IsLock = L.is_lock(functionIDUpload, out pid);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(pid.ToString());
            }

            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = CGM.DeleteCancelOrderDataTBT();
                if (rExtract == "SUCCESS")
                {
                    string resultFile = fileNameUpload + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, resultFile);

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_cancel_order_revise, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    rExtract = resultFile;
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        public ActionResult UploadFinishCaseBackgroundProsess( String filename)
        {
            mCaseGroupingMaintenance CGM = new mCaseGroupingMaintenance();
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;

            string resultMessage = CGM.UploadFinishCaseBackgroundProcess(UserID);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                //msg = msgError.getMSPXCG02INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                msg = msgError.getMSPXCG02INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                //msg = msgError.getMSPXCG01INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                msg = msgError.getMSPXCG01INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UploadCancelOrderBackgroundProsess(String filename) //add fid.mario 28/02/2018
        {
            mCaseGroupingMaintenance CGM = new mCaseGroupingMaintenance();
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;

            //long pid = 0;
            //string functionIDUpload = "OD017";
            //string processName = "CaseGroupingMaintenance_CancelOrder";
            //Lock L = new Lock();

            //Log lg = new Log();
            //msg = "MSPX00092INF|INF|" + new MessagesString().getMsgText("MSPX00092INF").Replace("{0}", processName);
            //pid = lg.createLog(msg, UserID, "CaseGroupingMaintenance_CancelOrder", pid, "OD", functionIDUpload);
            //L.CREATED_BY = UserID;
            //L.CREATED_DT = DateTime.Now;
            //L.PROCESS_ID = pid;
            //L.FUNCTION_ID = functionIDUpload;
            //L.LOCK_REF = functionIDUpload;
            //L.LockFunction(L);

            string resultMessage = CGM.UploadCancelOrderBackgroundProcess(UserID);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                //msg = msgError.getMSPXCG02INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                msg = msgError.getMSPXCG02INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                //msg = msgError.getMSPXCG01INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                msg = msgError.getMSPXCG01INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}