﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Linq;

namespace SPEX.Controllers
{
    public class PartLabelPrintingController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mCaseMaster caseMaster = new mCaseMaster();
        PartLabelPrinting plp = new PartLabelPrinting();
        Printer mPrinter = new Printer();
        mPLPSupp plpSupp = new mPLPSupp();

        public PartLabelPrintingController()
        {
            Settings.Title = "Part Label Printing";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            ViewData["SearchBy"] = plp.getSearchBy();
            ViewData["PrinterCmb"] = mPrinter.getList();
            var b = DateTime.Now.ToString("yyyyMMddhhmmss") + RandomString(5);
            ViewData["BatchId"] = b;
            Session["BatchIdPartLabel"] = b;
            getSupplierComboBox();
        }

        public void getSupplierComboBox()
        {
            List<mPLPSupp> model = new List<mPLPSupp>();
            mPLPSupp item = new mPLPSupp();
            item.SUPP_CD = "";
            item.SUPP_DESC = "";

            model = plpSupp.GetSuppList();
            model.Insert(0, item);
            ViewData["SUPPLIER"] = model;
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (string.IsNullOrEmpty(dt))
            {
                result = "";
            }
            else if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult checkMAD(string pMad)
        {
            string[] r = null;
            string msg = "";

            string message = plp.checkMAD(pMad);
            r = message.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DoPrint(string ids, string printer, string batch_session)
        {
            string[] r = null;
            string msg = "";

            string message = plp.DoPrint(ids, printer, batch_session);
            return Json(new { success = "true", messages = "ok" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PLPGridCallback(string pMad, string pSupplier_Cd, string pSupplierPlant_Cd, string pPart_No, string pQty_Print, string pSearch_Mode, string pBatch_Id)
        {
            List<PartLabelPrinting> model = new List<PartLabelPrinting>();

            model = plp.getList(pMad, pSupplier_Cd, pSupplierPlant_Cd, pPart_No, pQty_Print, pSearch_Mode, pBatch_Id);

            return PartialView("PartLabelPrintingGrid", model);
          
        }

       
 

        #region Upload function when user click button upload
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, FileUploadComplete);
            return null;
        }
        #endregion

        #region cek format file to Upload  Price
        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplateVanningCompletion";
            public const string BatchId = "";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xlsx" },
                MaxFileSize = 20971520
            };
        }
        #endregion

        private string FunctionID = "D01-006";
        private string ModuleID = "D01";
        public void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            //Session["BatchIdPartLabel"]
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_PART_LABEL_UPLOAD";
            #region createlog
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);

            string templateFileName = "PartLabelPrinting";
            string UploadDirectory = Server.MapPath("~/Content/FileUploadResult");
            long ProcessID = 0;
            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock lockTable = new Lock();

            int IsLock = lockTable.is_lock(FunctionID, out ProcessID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(ProcessID.ToString());
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = plp.DeleteTempData();
                if (rExtract == "SUCCESS")
                {
                    string resultFilePath = UploadDirectory + templateFileName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, templateFileName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);
                    rExtract = ReadAndInsertExcelData(pathfile);
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .xls OR xlsx";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        private string ReadAndInsertExcelData(string FileName)
        {
            try
            {
                XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                ISheet sheet = xssfworkbook.GetSheetAt(0);
                int lastrow = sheet.LastRowNum;
                List<PartLabelPrinting.mPartLabelUpload> listUpload = new List<PartLabelPrinting.mPartLabelUpload>();
                int index = 1;
                string b_id = Session["BatchIdPartLabel"].ToString();
                DataFormatter dataFormatter = new DataFormatter(CultureInfo.CurrentCulture);
                plp.ClearTempData(b_id);
                PartLabelPrinting.mPartLabelUpload uploadData;
                for (int i = index; i <= lastrow; i++)
                {
                    uploadData = new PartLabelPrinting.mPartLabelUpload();
                    IRow row = sheet.GetRow(i);

                    if (row == null) continue;
                    if (row.GetCell(0) == null && row.GetCell(1) == null && row.GetCell(2) == null) continue;


                    //data id
                    if (row.GetCell(0) == null) uploadData.DATA_ID = "";
                    else
                        uploadData.DATA_ID = row.GetCell(0).ToString();

                    if (uploadData.DATA_ID.ToLower() != "d")
                        continue;

                    uploadData.PART_NO = row.GetCell(1) != null ? row.GetCell(1).ToString() : "";
                    int q = 0;
                    bool successfullyParsed = Int32.TryParse(row.GetCell(2).ToString(),out q);
                    if (!successfullyParsed)
                    {
                        q = -1;
                    }
                    uploadData.QTY = q;
                    uploadData.PART_NAME = "";
                    uploadData.BATCH_ID = b_id;

                    plp.AddTempData(uploadData);
                }

                var m = plp.ValidateUploadTemp(b_id);
                if (m.Contains("Error"))
                {
                    return "1|" + m.Replace("Error|", "");
                }
                else
                {
                    return "0|success";
                }
            }
            catch (Exception ex)
            {
                return "1|" + ex.Message;
            }
        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        private string GetDateSQL(string dt)
        {
            if (dt == "01.01.0100")
                return "";

            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime date = new DateTime(year, month, day);

            return date.ToString("yyyy-MM-dd");
        }

        public void DownloadData_ByParameter(object sender, EventArgs e, string pMad, string pSupplier_Cd, string pSupplierPlant_Cd, string pPart_No, string pQty_Print)
        {/*
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/OBR_Download_Report.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            //styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            //styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;
            IFont font = workbook.CreateFont();
            font.FontHeight = 8;
            styleContent.SetFont(font);

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            //styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("OBR");
            string date = DateTime.Now.ToString("yyyyMMddHHmmss");
            filename = "SPEX_OBR_" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(2).GetCell(7).SetCellValue("");
            sheet.GetRow(2).GetCell(31).SetCellValue("");
            sheet.GetRow(2).GetCell(55).SetCellValue(("" == "01.01.0100" ? "" : ""));
            sheet.GetRow(2).GetCell(67).SetCellValue(("" == "01.01.0100" ? "" : ""));

            sheet.GetRow(3).GetCell(7).SetCellValue("");
            sheet.GetRow(3).GetCell(31).SetCellValue("");
            sheet.GetRow(3).GetCell(55).SetCellValue("");

            List<PartLabelPrinting> model = new List<PartLabelPrinting>();

            model = plp.getList(pMad, pSupplier_Cd, pSupplierPlant_Cd, pPart_No, pQty_Print);

            int row = 7;
            int rowNum = 1;
            IRow Hrow;
            int RowTemplate = 3;

            foreach (PartLabelPrinting d in model)
            {
                Hrow = sheet.CreateRow(row);
                for (int i = 0; i <= 79; i++)
                {
                    Hrow.CreateCell(i).SetCellValue("");
                }
                Hrow.CreateCell(0).SetCellValue(d.OBR_NO);
                Hrow.CreateCell(7).SetCellValue("");
                Hrow.CreateCell(15).SetCellValue(d.TMAP_ORDER_NO);

                Hrow.CreateCell(21).SetCellValue(d.TMAP_ITEM_NO);
                Hrow.CreateCell(27).SetCellValue(d.PART_NO);
                Hrow.CreateCell(33).SetCellValue("");
                Hrow.CreateCell(40).SetCellValue(d.ORDER_QTY.ToString());
                Hrow.CreateCell(45).SetCellValue(d.CANCEL_QTY.ToString());
                Hrow.CreateCell(50).SetCellValue(d.ACCEPT_QTY.ToString());
                Hrow.CreateCell(55).SetCellValue(d.OBR_STATUS_NAME);
                
                for (int i = 0; i <= 79; i++)
                {
                    if (i == 79 || i == 74 || i == 6 || i==14 || i == 20 || i == 26 || i == 32 || i == 39 || i == 44 || i == 49 || i == 54 || i == 59 || i == 64 || i == 69)
                    {
                        Hrow.GetCell(i).CellStyle = styleContent2;
                    }
                    else
                    {
                        Hrow.GetCell(i).CellStyle = styleContent;
                    }
                }

                row++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
          * */
        }

        public void DownloadData_ByTicked(object sender, EventArgs e, string pCASE_KEY, string pCASE_TYPE, string pDESCRIPTION)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/CaseMasterDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Case Master");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "CaseMaster" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(pCASE_TYPE);
            sheet.GetRow(9).GetCell(2).SetCellValue(pDESCRIPTION);

            int row = 16;
            int rowNum = 1;
            IRow Hrow;

            List<mCaseMaster> model = new List<mCaseMaster>();

            model = caseMaster.getDownloadList(pCASE_KEY);


            foreach (mCaseMaster result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.CASE_TYPE);
                Hrow.CreateCell(3).SetCellValue(result.DESCRIPTION);
                if (result.CASE_NET_WEIGHT != null)
                    Hrow.CreateCell(4).SetCellValue((double)result.CASE_NET_WEIGHT);
                else
                    Hrow.CreateCell(4).SetCellValue("");
                if (result.CASE_LENGTH != null)
                    Hrow.CreateCell(5).SetCellValue((double)result.CASE_LENGTH);
                else
                    Hrow.CreateCell(5).SetCellValue("");
                if (result.CASE_WIDTH != null)
                    Hrow.CreateCell(6).SetCellValue((double)result.CASE_WIDTH);
                else
                    Hrow.CreateCell(6).SetCellValue("");
                if (result.CASE_HEIGHT != null)
                    Hrow.CreateCell(7).SetCellValue((double)result.CASE_HEIGHT);
                else
                    Hrow.CreateCell(7).SetCellValue("");
                if (result.MEASUREMENT != null)
                    Hrow.CreateCell(8).SetCellValue((double)result.MEASUREMENT);
                else
                    Hrow.CreateCell(8).SetCellValue("");
                Hrow.CreateCell(9).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(10).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(11).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(12).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent;
                Hrow.GetCell(12).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
    }
}

