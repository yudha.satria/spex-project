﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class LogMonitoringController : PageController
    {
        //panggil isi model
        mLogMonitoring LogMonitoring = new mLogMonitoring();
        mMasterFunction MasterFunction = new mMasterFunction();

        //public string getpE_ProcessId
        //{
        //    get
        //    {
        //        if (Session["getpE_ProcessId"] != null)
        //        {
        //            return Session["getpE_ProcessId"].ToString();
        //        }
        //        else return null;
        //    }
        //    set
        //    {
        //        Session["getpE_ProcessId"] = value;
        //    }
        //}

        public LogMonitoringController()
        {
                       
            Settings.Title = "Log Monitoring";
            
        }

        //public ActionResult _PartialAddMaster(string p_UPLOAD)
        //{
        //    ViewBag.UPLOAD = p_UPLOAD;
        //    return View();
        //}
       

        public ActionResult _PopUpLog(string p_Process_ID)
        {
            ViewData["SetProcessId"] = p_Process_ID;
            List<mLogMonitoring> model = LogMonitoring.getListLogMontoringDetail(p_Process_ID);
            ViewData["LogMonitoring"] = model;

            List<mLogMonitoring> modelH = LogMonitoring.getListLogMontoringHeader(p_Process_ID);
            ViewData["D_ProcessId"] = modelH[0].PROCESS_ID;
            ViewData["D_ModuleName"] = modelH[0].MODULE_NAME;
            ViewData["D_FunctionName"] = modelH[0].FUNCTION_NAME;
            ViewData["D_ProcessStatus"] = modelH[0].PROCESS_STATUS;
            //ViewData["D_ProcessStatus"] = "";
            ViewData["D_UserId"] = modelH[0].CREATED_BY;
            DateTime dt = Convert.ToDateTime(modelH[0].START_DATE);
            ViewData["D_StartDate"] = dt.ToString("dd-MM-yyyy hh:mm:ss");

            return PartialView("~/Views/Shared/_PartialAddMaster.cshtml");

        }

        protected override void Startup()
        {
            ComboBoxPartial();
            //add agi 2016-12-14 migarsi from prod download excel
            getpE_UserId = (User)ViewData["User"];
        }

        public ActionResult GridViewLogMonitoring( string p_PROCESS_ID, string p_START_DATE, string p_END_DATE, string p_FUNCTION_ID, string p_CREATED_BY)
        {
            List<mLogMonitoring> model = new List<mLogMonitoring>();

       
            {
                if (p_START_DATE != "") p_START_DATE = p_START_DATE.Substring(6, 4) + "-" + p_START_DATE.Substring(3, 2) + "-" + p_START_DATE.Substring(0, 2);
                if (p_END_DATE != "") p_END_DATE = p_END_DATE.Substring(6, 4) + "-" + p_END_DATE.Substring(3, 2) + "-" + p_END_DATE.Substring(0, 2);
                model = LogMonitoring.getListLogMontoring
                (p_PROCESS_ID, p_START_DATE, p_END_DATE, p_FUNCTION_ID, p_CREATED_BY);
            }
            return PartialView("GridViewLogMonitoring", model);
        }


        public ActionResult GridViewLogMonitoringDetail(string p_PROCESS_ID)
        {
            List<mLogMonitoring> model = new List<mLogMonitoring>();

            {
                model = LogMonitoring.getListLogMontoringDetail
                (p_PROCESS_ID);
            }
            return PartialView("GridViewLogMonitoringDetail", model);
        }

        //public void ComboBoxPartial()
        //{
        //    List<mMasterFunction> ModelMasterFunction = MasterFunction.getListMasterFunction();

        //    ViewData["DataMasterFunction"] = ModelMasterFunction;
        //}
        

        //untuk look up master function
        public void ComboBoxPartial()
        {
            List<mLogMonitoring> model = new List<mLogMonitoring>();

            model = LogMonitoring.GetListMasterFunction();
            ViewData["FUNCTION_NAME"] = model;
        }        

        //add agi 2016-12-14 hasil compare prod
        public void DownloadData(string pProcessId)
        {
            try
            {
                string filename = "";
                string filesTmp = HttpContext.Request.MapPath("~/Template/LogMonitoringDownload.xlsx");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

                IDataFormat format = workbook.CreateDataFormat();

                ICellStyle styleDecimal = workbook.CreateCellStyle();
                styleDecimal.DataFormat = HSSFDataFormat.GetBuiltinFormat("##,##0.0####");
                styleDecimal.Alignment = HorizontalAlignment.Center;
                styleDecimal.BorderBottom = BorderStyle.Thin;
                styleDecimal.BorderTop = BorderStyle.Thin;
                styleDecimal.BorderLeft = BorderStyle.Thin;
                styleDecimal.BorderRight = BorderStyle.Thin;

                ICellStyle styleDate1 = workbook.CreateCellStyle();
                styleDate1.DataFormat = HSSFDataFormat.GetBuiltinFormat("dd/MM/yyyy HH:mm:ss");
                styleDate1.Alignment = HorizontalAlignment.Center;
                styleDate1.BorderBottom = BorderStyle.Thin;
                styleDate1.BorderTop = BorderStyle.Thin;
                styleDate1.BorderLeft = BorderStyle.Thin;
                styleDate1.BorderRight = BorderStyle.Thin;

                ICellStyle style1 = workbook.CreateCellStyle();
                style1.VerticalAlignment = VerticalAlignment.Center;
                style1.Alignment = HorizontalAlignment.Center;
                style1.BorderBottom = BorderStyle.Thin;
                style1.BorderTop = BorderStyle.Thin;
                style1.BorderLeft = BorderStyle.Thin;
                style1.BorderRight = BorderStyle.Thin;

                ICellStyle style2 = workbook.CreateCellStyle();
                style2.VerticalAlignment = VerticalAlignment.Center;
                style2.Alignment = HorizontalAlignment.Left;
                style2.BorderBottom = BorderStyle.Thin;
                style2.BorderRight = BorderStyle.Thin;
                style2.BorderLeft = BorderStyle.Thin;
                style2.BorderRight = BorderStyle.None;


                ISheet sheetMain = workbook.GetSheet("LogMonitoring");
                string date = DateTime.Now.ToString("ddMMyyyyHHmmss");
                filename = "LogMonitoring_" + date + ".xlsx";

                string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

                List<mLogMonitoring> models = LogMonitoring.getListLogMontoringDetail(pProcessId);
                int startRow = 12;
                IRow Hrow;

                Hrow = sheetMain.GetRow(5);
                Hrow.CreateCell(2).SetCellValue(DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                Hrow.GetCell(2).CellStyle = style2;
                Hrow = sheetMain.GetRow(6);
                Hrow.CreateCell(2).SetCellValue(getpE_UserId.Username);
                Hrow.GetCell(2).CellStyle = style2;

                foreach (mLogMonitoring mn in models)
                {
                    Hrow = sheetMain.CreateRow(startRow);
                    //Hrow.CreateCell(0).SetCellValue("");
                    Hrow.CreateCell(1).SetCellValue((startRow - 11).ToString());
                    Hrow.GetCell(1).CellStyle = style1;
                    Hrow.CreateCell(2).SetCellValue(mn.PROCESS_ID);
                    Hrow.GetCell(2).CellStyle = style1;
                    Hrow.CreateCell(3).SetCellValue(mn.SEQUENCE_NUMBER);
                    Hrow.GetCell(3).CellStyle = style1;
                    Hrow.CreateCell(4).SetCellValue(mn.LOCATION);
                    Hrow.GetCell(4).CellStyle = style2;
                    Hrow.CreateCell(5).SetCellValue(mn.MSG_TYPE);
                    Hrow.GetCell(5).CellStyle = style1;
                    Hrow.CreateCell(6).SetCellValue(mn.MESSAGE);
                    Hrow.GetCell(6).CellStyle = style2;


                    if (mn.START_DATE != null)
                    {
                        Hrow.CreateCell(7).SetCellValue(mn.START_DATE);
                        Hrow.GetCell(7).CellStyle = style1;
                    }
                    else
                    {
                        Hrow.CreateCell(7).SetCellValue(" ");
                        Hrow.GetCell(7).CellStyle = style1;
                    }

                    if (mn.END_DATE != null)
                    {
                        Hrow.CreateCell(8).SetCellValue(mn.END_DATE);
                        Hrow.GetCell(8).CellStyle = style1;
                    }
                    else
                    {
                        Hrow.CreateCell(8).SetCellValue(" ");
                        Hrow.GetCell(8).CellStyle = style1;
                    }

                    startRow++;
                }

                ExcelInsertHeaderImage(workbook, sheetMain, HttpContext.Request.MapPath("~/Content/images/excel_header.png"), 0);

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                Response.BinaryWrite(ms.ToArray());
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            }
            catch (Exception ex)
            {

            }
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        private void ExcelInsertHeaderImage(XSSFWorkbook workbook, ISheet sheet1, string path, int rowDest)
        {
            XSSFDrawing patriarch = (XSSFDrawing)sheet1.CreateDrawingPatriarch();
            XSSFClientAnchor anchor;
            anchor = new XSSFClientAnchor(0, 0, 0, 0, 1, rowDest, 2, rowDest);
            anchor.AnchorType = (AnchorType)2;
            //load the picture and get the picture index in the workbook
            XSSFPicture picture = (XSSFPicture)patriarch.CreatePicture(anchor, ExcelLoadImage(path, workbook));
            //Reset the image to the original size.
            picture.Resize();
            //picture.LineStyle = HSSFPicture.LINESTYLE_DASHDOTGEL;
        }

        public int ExcelLoadImage(string path, XSSFWorkbook workbook)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, (int)file.Length);
            return workbook.AddPicture(buffer, PictureType.JPEG);

        }

    }
}

