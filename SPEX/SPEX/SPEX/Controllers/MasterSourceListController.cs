﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;
using Toyota.Common.Web.Platform.Starter.Models;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using NPOI.POIFS.FileSystem;
using NPOI.HPSF;
using System.IO;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxGridView;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class MasterSourceListController : PageController
    {
        mMasterLine MasterLine = new mMasterLine();
        mMasterPlant MasterPlant = new mMasterPlant();
        mSourceList SourceList = new mSourceList();

        public string getpE_PartNo
        {
            get
            {
                if (Session["getpE_PartNo"] != null)
                {
                    return Session["getpE_PartNo"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_PartNo"] = value;
            }
        }

        public DateTime getpE_ValidFrom
        {
            get
            {
                if (Session["getpE_ValidFrom"] != null)
                {
                    return Convert.ToDateTime(Session["getpE_ValidFrom"]);
                }
                else return Convert.ToDateTime("01-01-1753");
            }
            set
            {
                Session["getpE_ValidFrom"] = value;
            }
        }

        public string getpE_LineCd
        {
            get
            {
                if (Session["getpE_LineCd"] != null)
                {
                    return Session["getpE_LineCd"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_LineCd"] = value;
            }
        }

        public string getpE_SlocCd
        {
            get
            {
                if (Session["getpE_SlocCd"] != null)
                {
                    return Session["getpE_SlocCd"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_SlocCd"] = value;
            }
        }

        public DateTime getpE_ValidTo
        {
            get
            {
                if (Session["getpE_ValidTo"] != null)
                {
                    return Convert.ToDateTime(Session["getpE_ValidTo"]);
                }
                else return Convert.ToDateTime("01-01-1753");
            }
            set
            {
                Session["getpE_ValidTo"] = value;
            }
        }

        public string getpE_PlantCd
        {
            get
            {
                if (Session["getpE_PlantCd"] != null)
                {
                    return Session["getpE_PlantCd"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_PlantCd"] = value;
            }
        }

        public string get_Message_Upload
        {
            get
            {
                if (Session["get_Message_Upload"] != null)
                {
                    return Session["get_Message_Upload"].ToString();
                }
                else return null;
            }
            set
            {
                Session["get_Message_Upload"] = value;
            }
        }

        public MasterSourceListController()
        {

        }

        protected override void Startup()
        {
            SPN113GetMasterLine();
            SPN113GetMasterPlant();

        }

        public void SPN113GetMasterLine()
        {
            List<mMasterLine> modelMasterLine = MasterLine.getListMasterLine();
            ViewData["DataMasterLine"] = modelMasterLine;
        }

        public void SPN113GetMasterPlant()
        {
            List<mMasterPlant> modelMasterPlant = MasterPlant.getListMasterPlant();
            ViewData["DataMasterPlant"] = modelMasterPlant;
        }

        public ActionResult SPN113GridViewPartialView(string ps_PartNo, string ps_PlantCode, string ps_LineCode, string ps_ValidFrom, string ps_ValidTo)
        {
            List<mSourceList> model = new List<mSourceList>();
            model = SourceList.SPN113MasterSourceListSearch(ps_PartNo, ps_PlantCode, ps_LineCode, ps_ValidFrom, ps_ValidTo);          

            return PartialView("GridViewPartialView", model);
        }

        public ActionResult _SPN113PartialAddSourceList()
        {
            return PartialView("_PartialAddSourceList");
        }

        public void _SPN113EditSourceList(string pE_PartNo, string pE_ValidFrom, string pE_LineCd, string pE_SLocCd, string pE_ValidTo, string pE_PlantCd)
        {
            DateTime dtPValidFrom = Convert.ToDateTime("01-01-1753");
            DateTime dtPValidTo = Convert.ToDateTime("01-01-1753");

            if (!String.IsNullOrEmpty(pE_ValidFrom))
            {
                string strValidFrom = pE_ValidFrom.Substring(5, 2) + "-" + pE_ValidFrom.Substring(8, 2) + "-" + pE_ValidFrom.Substring(0, 4);
                dtPValidFrom = Convert.ToDateTime(strValidFrom);
            }

            if (!String.IsNullOrEmpty(pE_ValidTo))
            {
                string strValidTo = pE_ValidTo.Substring(5, 2) + "-" + pE_ValidTo.Substring(8, 2) + "-" + pE_ValidTo.Substring(0, 4);
                dtPValidTo = Convert.ToDateTime(strValidTo);
            }
                   
            getpE_PartNo = pE_PartNo;
            getpE_ValidFrom = dtPValidFrom;
            getpE_LineCd = pE_LineCd;
            getpE_SlocCd = pE_SLocCd;
            getpE_ValidTo = dtPValidTo;
            getpE_PlantCd = pE_PlantCd;
        }

        public ActionResult _SPN113PartialEditSourceList()
        {
            ViewData["SettxtPartNo_E"] = getpE_PartNo;
            ViewData["SettxtValidFrom_E"] = getpE_ValidFrom != Convert.ToDateTime("01-01-1753") ? getpE_ValidFrom : DateTime.MinValue;
            ViewData["SettxtLineCode_E"] = getpE_LineCd;
            ViewData["SettxtSLocCode_E"] = getpE_SlocCd;
            ViewData["SettxtValidTo_E"] = getpE_ValidTo != Convert.ToDateTime("01-01-1753") ? getpE_ValidTo : DateTime.MinValue; ;
            ViewData["SettxtPlantCode_E"] = getpE_PlantCd;

            return PartialView("_PartialEditSourceList");
        }

        public string SPN113SaveData(string pi_PartNo, string pi_PlantCode, string pi_LineCode, string pi_SlocCode, string pi_ValidFrom, string pi_ValidTo)
        {
            string resultMessage = SourceList.SPN113MasterSourceListSaveData(pi_PartNo, pi_PlantCode, pi_LineCode, pi_SlocCode, pi_ValidFrom, pi_ValidTo);

            return resultMessage;
        }

        public string SPN113EditData(string pE_PartNo, string pE_PlantCode, string pE_LineCode, string pE_SlocCode, string pE_ValidFrom, string pE_ValidTo)
        {
            string resultMessage = SourceList.SPN113MasterSourceListEditData(pE_PartNo, pE_PlantCode, pE_LineCode, pE_SlocCode, pE_ValidFrom, pE_ValidTo);

            return resultMessage;
        }

        public string SPN113DeleteData(string pd_Part_List)
        {            
            string resultMessage = SourceList.SPN113MasterSourceListDeleteData(pd_Part_List);

            return resultMessage;
        }

        public void _SPN113DownloadSourceList(object sender, EventArgs e, string pD_PartNo, string pD_PlantCode, string pD_LineCode, string pD_ValidFrom, string pD_ValidTo)
        {
            DateTime dtPValidFrom = Convert.ToDateTime("01-01-1753");
            DateTime dtPValidTo = Convert.ToDateTime("01-01-1753");

            if (!String.IsNullOrEmpty(pD_ValidFrom))
            {

                string strValidFrom = pD_ValidFrom.Substring(3, 2) + "-" + pD_ValidFrom.Substring(0, 2) + "-" + pD_ValidFrom.Substring(6, 4);
                dtPValidFrom = Convert.ToDateTime(strValidFrom);
            }

            if (!String.IsNullOrEmpty(pD_ValidTo))
            {

                string strValidTo = pD_ValidTo.Substring(3, 2) + "-" + pD_ValidTo.Substring(0, 2) + "-" + pD_ValidTo.Substring(6, 4);
                dtPValidTo = Convert.ToDateTime(strValidTo);
            }

            List<mSourceList> model = new List<mSourceList>();
            model = SourceList.SPN113MasterSourceListSearch(pD_PartNo, pD_PlantCode, pD_LineCode, pD_ValidFrom, pD_ValidTo);


            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Source_List_Master_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.TOP;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.TOP;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.Alignment = HorizontalAlignment.CENTER;

            ISheet sheet = workbook.GetSheet("Source List Master");
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPIN113SourceListMaster_" + date + ".xls";
            sheet.ForceFormulaRecalculation = true;

            string GetdtPValidFrom = dtPValidFrom.ToString("dd-MM-yyyy");
            string GetdtPValidTo = dtPValidTo.ToString("dd-MM-yyyy");

            string dateNow = DateTime.Now.ToString("dd-MM-yyyy");
            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(3).SetCellValue("Sys");
            sheet.GetRow(9).GetCell(4).SetCellValue(pD_PartNo);
            sheet.GetRow(10).GetCell(4).SetCellValue(pD_PlantCode);
            sheet.GetRow(11).GetCell(4).SetCellValue(pD_LineCode);
            if (GetdtPValidFrom != "01-01-1753")
            {
                sheet.GetRow(12).GetCell(4).SetCellValue(GetdtPValidFrom);
            }
            else
            {
                sheet.GetRow(12).GetCell(4).SetCellValue("");            
            }
            if (GetdtPValidTo != "01-01-1753")
            {
                sheet.GetRow(12).GetCell(7).SetCellValue(GetdtPValidTo);
            }
            else
            {
                sheet.GetRow(12).GetCell(7).SetCellValue("");
            }
            

            int row = 17;
            string GetPartNo = string.Empty;
            string GetLineCode = string.Empty;
            string GetPlantCode = string.Empty;
            string GetSlocCode = string.Empty;
            string GetValidFrom = string.Empty;
            string GetValidTO = string.Empty;
            IRow Hrow;

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                GetPartNo = result.PART_NO;
                GetLineCode = result.LINE_CD;
                GetPlantCode = result.PLANT_CD;
                GetSlocCode = result.SLOC_CD;
                GetValidFrom = result.VALID_FROM.ToString("dd-MM-yyyy");
                GetValidTO = result.VALID_TO.ToString("dd-MM-yyyy");

                //sheet.GetRow(row).GetCell(1).SetCellValue(GetPartNo);
                //sheet.GetRow(row).GetCell(2).SetCellValue(GetLineCode);
                //sheet.GetRow(row).GetCell(3).SetCellValue(GetPlantCode);
                //sheet.GetRow(row).GetCell(4).SetCellValue(GetSlocCode);
                //sheet.GetRow(row).GetCell(5).SetCellValue(GetValidFrom);
                //sheet.GetRow(row).GetCell(6).SetCellValue(GetValidTO);

                //sheet.GetRow(row).GetCell(1).CellStyle = styleContent;
                //sheet.GetRow(row).GetCell(2).CellStyle = styleContent2;
                //sheet.GetRow(row).GetCell(3).CellStyle = styleContent2;
                //sheet.GetRow(row).GetCell(4).CellStyle = styleContent2;
                //sheet.GetRow(row).GetCell(5).CellStyle = styleContent2;
                //sheet.GetRow(row).GetCell(6).CellStyle = styleContent2;

                Hrow.CreateCell(1).SetCellValue(GetPartNo);
                Hrow.CreateCell(2).SetCellValue(GetLineCode);
                Hrow.CreateCell(3).SetCellValue(GetPlantCode);
                Hrow.CreateCell(4).SetCellValue(GetSlocCode);
                Hrow.CreateCell(5).SetCellValue(GetValidFrom);
                Hrow.CreateCell(6).SetCellValue(GetValidTO);

                Hrow.GetCell(1).CellStyle = styleContent;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                
                row++;
            }

            //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
            //    sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));  
        }

        public ActionResult SPN113CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadSourceList", UploadControlHelper.ValidationSettings, SPN113CallbackUploaduc_FileUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public const string ThumbnailFormat = "Thumbnail{0}{1}";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xls" },
                MaxFileSize = 209751520
            };
        }

        public void SPN113CallbackUploaduc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                var resultFilePath = HttpContext.Request.MapPath("~/Content/FileUploadResult/SourceListMaster_"  + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xls");
                e.UploadedFile.SaveAs(resultFilePath);
                IUrlResolutionService urlResolver = sender as IUrlResolutionService;
                if (urlResolver != null)
                {
                    e.CallbackData = urlResolver.ResolveClientUrl(resultFilePath);
                }

                string result = SPN113CallbackUploadExcelToSQL(resultFilePath);
            }
        }

        public string SPN113CallbackUploadExcelToSQL(string resultFilePath)
        {
            try{
                FileStream ftmp = new FileStream(resultFilePath, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
                ISheet sheet = workbook.GetSheet("Source List Master");

                string CreatedDT = DateTime.Now.ToString("MM-dd-yyyy");
                string CreatedBy = "Sys";

                int row = 1;
                mProcessID ModProcessID = new mProcessID();
                string ProcessId = ModProcessID.GetProcessID();

                for (int i = 0; i < sheet.LastRowNum; i++)
                {
                    string PartNo = string.Empty;
                    string LineCd = string.Empty;
                    string PlantCd = string.Empty;
                    string SlocCd = string.Empty;
                    string ValidFrom = string.Empty;
                    string ValidTo = string.Empty;

                    ICell PartNoCell = sheet.GetRow(row).GetCell(0);
                    ICell LineCdCell = sheet.GetRow(row).GetCell(1);
                    ICell PlantCdCell = sheet.GetRow(row).GetCell(2);
                    ICell SlocCdCell = sheet.GetRow(row).GetCell(3);
                    ICell ValidFromCell = sheet.GetRow(row).GetCell(4);
                    ICell ValidToCell = sheet.GetRow(row).GetCell(5);

                    if (PartNoCell != null)
                    {
                        if (PartNoCell.CellType != CellType.BLANK)
                        {
                            PartNo = sheet.GetRow(row).GetCell(0).StringCellValue;
                        }
                    }

                    if (LineCdCell != null)
                    {
                        if (LineCdCell.CellType != CellType.BLANK)
                        {
                            LineCd = sheet.GetRow(row).GetCell(1).StringCellValue;
                        }
                    }

                    if (PlantCdCell != null)
                    {
                        if (PlantCdCell.CellType != CellType.BLANK)
                        {
                            PlantCd = sheet.GetRow(row).GetCell(2).StringCellValue;
                        }
                    }

                    if (SlocCdCell != null)
                    {
                        if (SlocCdCell.CellType != CellType.BLANK)
                        {
                            SlocCd = sheet.GetRow(row).GetCell(3).StringCellValue;
                        }
                    }

                    if (ValidFromCell != null)
                    {
                        if (ValidFromCell.CellType != CellType.BLANK)
                        {
                            ValidFrom = sheet.GetRow(row).GetCell(4).StringCellValue;
                        }
                    }

                    if (ValidToCell != null)
                    {
                        if (ValidToCell.CellType != CellType.BLANK)
                        {
                            ValidTo = sheet.GetRow(row).GetCell(5).StringCellValue;
                        }
                    }               

                    if (String.IsNullOrEmpty(PartNo) && String.IsNullOrEmpty(LineCd) && String.IsNullOrEmpty(PlantCd) && String.IsNullOrEmpty(SlocCd) && String.IsNullOrEmpty(ValidFrom) && String.IsNullOrEmpty(ValidTo))
                    {
                        break;
                    }

                    SourceList.SPN113CallbackUploadExcelToSQL(PartNo, ValidFrom, LineCd, SlocCd, PlantCd, ValidTo, CreatedBy, CreatedDT, ProcessId);
                    row++;
                }

                get_Message_Upload = SourceList.SPN113SaveUploadExcel(CreatedBy, ProcessId);

                if (get_Message_Upload == "Upload Successfully")
                {
                    return get_Message_Upload;
                }
                else
                {
                    return get_Message_Upload = "Error : please check Process ID " + ProcessId;
                }
            }
            catch(Exception err)
            {
                return get_Message_Upload = "Error | " + Convert.ToString(err.Message);
            }
        }

        public string SPN113GetInfo()
        {

            return get_Message_Upload;
        }
    }
}
