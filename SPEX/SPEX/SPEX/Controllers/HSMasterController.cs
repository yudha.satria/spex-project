﻿using System.Collections.Generic;
using System.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using System.Web.UI;
using System.IO;
using System;

namespace SPEX.Controllers
{
    public class HSMasterController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mHSMaster hs = new mHSMaster();

        public HSMasterController()
        {
            Settings.Title = "HS Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult HSGridCallback(string pPART_NO)
        {
            List<mHSMaster> model = new List<mHSMaster>();

            model = hs.GetList(pPART_NO);

            return PartialView("HSGrid", model);
        }

        public ActionResult EditHS(string pPART_NO, string pHS_INDONESIA, string pHS_PEB)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = hs.UpdateData(pPART_NO, pHS_INDONESIA, pHS_PEB, getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddHS(string pPART_NO, string pHS_INDONESIA, string pHS_PEB)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = hs.AddData(pPART_NO, pHS_INDONESIA, pHS_PEB, getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteHS(string pPART_NO)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = hs.DeleteData(pPART_NO, getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PopUpMaintenance(string p_PART_NO)
        {
            if (p_PART_NO == null)
                p_PART_NO = "";

            if (p_PART_NO == "")
                ViewData["IsNewData"] = true;
            else
                ViewData["IsNewData"] = false;

            mHSMaster model = hs.GetHS(p_PART_NO);

            return PartialView("HSMaintenance", model);
        }

        public ActionResult GetPartName(string pPART_NO)
        {
            string[] r = null;
            string msg = "";

            mHSMaster model = hs.GetPartName(pPART_NO);

            if (model.PART_NAME == null)
                return Json(new { success = "false", PART_NAME = "" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", PART_NAME = model.PART_NAME }, JsonRequestBehavior.AllowGet);
        }

        //add agi 2017-06-07 Request Mrs Hesty upload hs master
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, FileUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplatePartPriceMaster";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".csv" },
                //MaxFileSize = 20971520
                MaxFileSize = 30000000
            };
        }

        private string FunctionID = "F15-051";
        private string ModuleID = "F15";
        public void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_HS";
            #region createlog
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);

            string templateFileName = "TemplateHSMaster";
            string UploadDirectory = Server.MapPath("~/Content/FileUploadResult");
            long ProcessID = 0;
            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock lockTable = new Lock();

            int IsLock = lockTable.is_lock(FunctionID, out ProcessID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(ProcessID.ToString());
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = hs.DeleteTempData(); //partPrice.DeleteTempData();
                if (rExtract == "SUCCESS")
                {
                    string resultFilePath = UploadDirectory + templateFileName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, templateFileName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);
                    //ReadAndInsertExcelData(pathfile);
                    //tb_t_name = "spex.TB_T_PART_PRICE_CSV";

                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    //rExtract = partPrice.BulkInsertTempCSVToTempPartPrice();
                    
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        public ActionResult UploadBackgroundProsess()
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;

            string resultMessage = hs.UploadHsBackgroundProcess(UserID);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = msgError.getMSPX00034INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00045INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }


    }
}

