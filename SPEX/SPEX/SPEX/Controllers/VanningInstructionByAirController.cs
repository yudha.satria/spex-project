﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class VanningInstructionByAirController : PageController
    {
        //
        // GET: /VanningInstructionByAir/

        public VanningInstructionByAirController()
        {
            Settings.Title = "Vanning and Shipping Instruction [TRANS By Air]";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            MessagesString msgError = new MessagesString();
            ViewData["Packing_Company"] = GetPackingCompany();
            ViewData["Original"] = GetOriginal();
            List<string> InvoiceNos = new List<string>();
            List<string> Vanning_Nos = new List<string>();
            List<string> SI_NOs = new List<string>();
            GetINVOICE(out InvoiceNos, out Vanning_Nos, out SI_NOs);
            ViewData["INVOICE_NO"] = InvoiceNos;
            ViewData["Vanning_No"] = Vanning_Nos;
            ViewData["SI_NO"] = SI_NOs;
            ViewData["FORWARDING_AGENT"] = GetFA();
            ViewData["CASE_NO"] = GetCASE();
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Vanning Instruction");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
            //add agi 2017-04-07 request mr yoni
            ViewBag.MSPXVNG01ERR = msgError.getMsgText("MSPXVNG01ERR");
            ViewBag.MSPXVNG02ERR = msgError.getMsgText("MSPXVNG02ERR");
        }
        public List<string> GetPackingCompany()
        {
            return new VanningInstructionByAir().GetPackingCompany();
        }

        public List<string> GetOriginal()
        {
            return new VanningInstructionByAir().GetOriginal();
        }

        public void GetINVOICE(out List<string> InvoiceNos, out List<string> Vanning_Nos, out List<string> SI_NOs)
        {
            List<mInvoice> Invoices = new List<mInvoice>();
            Invoices = new VanningInstructionByAir().GetINVOICE();
            InvoiceNos = (from a in Invoices where a.INVOICE_NO != null && a.INVOICE_NO != string.Empty select a.INVOICE_NO).Distinct().ToList();
            Vanning_Nos = (from a in Invoices where a.VANNING_NO != null && a.VANNING_NO != string.Empty select a.VANNING_NO).Distinct().ToList();
            SI_NOs = (from a in Invoices where a.SHIP_INSTRUCTION_NO != null && a.SHIP_INSTRUCTION_NO != string.Empty select a.SHIP_INSTRUCTION_NO).Distinct().ToList();

        }

        public List<string> GetFA()
        {
            return new VanningInstructionByAir().Get_FA();
        }

        public List<string> GetCASE()
        {
            return new VanningInstructionByAir().Get_READY_CARGO_CASE();
        }

        public ActionResult GetVanningInstructionByAir(string Original, string Vanning_No, string VANNING_DT_FROM, string VANNING_DT_TO, string Complete_Data, string CASE_NO, string INVOICE_NO, string SI_NO, string ETD_FROM, string ETD_TO, string FORWARDING_AGENT, string PACKING_COMPANY)
        {
            VanningInstructionByAir model = new VanningInstructionByAir();
            List<VanningInstructionByAir> models = new List<VanningInstructionByAir>();
            models = model.GetVanningInstructionByAir(Original, Vanning_No, VANNING_DT_FROM, VANNING_DT_TO, Complete_Data, CASE_NO, INVOICE_NO, SI_NO, ETD_FROM, ETD_TO, FORWARDING_AGENT, PACKING_COMPANY);
            return PartialView("_PartialGrid", models);
        }

        public ActionResult GetVanningInstructionDetailByAir(string Original, string Vanning_No, string VANNING_DT_FROM, string VANNING_DT_TO, string Complete_Data, string CASE_NO, string INVOICE_NO, string SI_NO, string ETD_FROM, string ETD_TO, string FORWARDING_AGENT, string PACKING_COMPANY)
        {
            VanningInstructionByAir model = new VanningInstructionByAir();
            List<VanningInstructionByAir> models = new List<VanningInstructionByAir>();
            models = model.GetVanningInstructionByAirDetail(Original, Vanning_No, VANNING_DT_FROM, VANNING_DT_TO, Complete_Data, CASE_NO, INVOICE_NO, SI_NO, ETD_FROM, ETD_TO, FORWARDING_AGENT, PACKING_COMPANY);
            return PartialView("PartialGridDetail", models);
        }

        public void GenerateExcelMainSI(string INV_NO, string FLIGHT, string DEPARTURE_ABOUT, string CONNECTING_FLIGHT, string CONNECTING_DEPARTURE_ABOUT, string ORIGINAL)
        {
            mInvoice invoice = new mInvoice();
            invoice.INVOICE_NO = INV_NO;
            invoice.BYAIR_FLIGHT = FLIGHT;
            invoice.BYAIR_CONNECTING_FLIGHT = CONNECTING_FLIGHT;
            invoice.DEPARTURE_ABOUT_1 = GetDate(DEPARTURE_ABOUT);
            if (CONNECTING_DEPARTURE_ABOUT != "" && CONNECTING_DEPARTURE_ABOUT != "01.01.0100")
                invoice.DEPARTURE_ABOUT_2 = GetDate(CONNECTING_DEPARTURE_ABOUT);
            else
                invoice.DEPARTURE_ABOUT_2 = new DateTime(1900, 1, 1);

            string res = invoice.UpdateInvoiceByAirHeader(invoice);

            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Vanning_Instruction_SI_Download.xlsx");

            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            IDataFormat format = workbook.CreateDataFormat();


            ICellStyle styleDate1 = workbook.CreateCellStyle();
            styleDate1.DataFormat = HSSFDataFormat.GetBuiltinFormat("mmmm d, yyyy");
            styleDate1.Alignment = HorizontalAlignment.Center;

            ICellStyle styleLeft = workbook.CreateCellStyle();
            styleLeft.VerticalAlignment = VerticalAlignment.Center;
            styleLeft.Alignment = HorizontalAlignment.Left;
            styleLeft.WrapText = true;


            ICellStyle styleCenter = workbook.CreateCellStyle();
            styleCenter.VerticalAlignment = VerticalAlignment.Center;
            styleCenter.Alignment = HorizontalAlignment.Center;
            styleCenter.WrapText = true;


            ISheet sheetMain = workbook.GetSheet("SI");
            ISheet sheetAttachment = workbook.GetSheet("Attachment");
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            filename = "SI_" + date + ".xlsx";

            mShippingInstruction modelSI = new mShippingInstruction();
            VanningInstructionByAir download = new VanningInstructionByAir();
            string pVERSION = "F";

            modelSI = download.getShippingInstructionDownload(INV_NO, ORIGINAL);
            GenerateExcelSI(styleDate1, styleCenter, styleLeft, sheetMain, modelSI);
            ExcelInsertApprovedImage(workbook, sheetMain, HttpContext.Request.MapPath(modelSI.APPROVED_BY), 52, 16);
            GenerateExcelAttachment(workbook, sheetAttachment, modelSI);
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            //return filename;
        }

        private static void GenerateExcelSI(ICellStyle styleDate1, ICellStyle styleCenter, ICellStyle styleLeft, ISheet sheetMain, mShippingInstruction model)
        {
            //var a=sheetMain.GetRow(0);
            sheetMain.GetRow(1).GetCell(0).SetCellValue(model.BYAIR_COVER_SI1);
            sheetMain.GetRow(2).GetCell(0).SetCellValue(model.BYAIR_COVER_SI2);
            sheetMain.GetRow(3).GetCell(0).SetCellValue(model.BYAIR_COVER_SI3);
            sheetMain.GetRow(5).GetCell(0).SetCellValue(model.SHIPPER);
            sheetMain.GetRow(10).GetCell(0).SetCellValue(model.CONSIGNEE);
            sheetMain.GetRow(17).GetCell(0).SetCellValue(model.NOTIFY_PARTY);
            sheetMain.GetRow(5).GetCell(13).SetCellValue(model.BYAIR_MESSRS_SI1);
            sheetMain.GetRow(6).GetCell(13).SetCellValue(model.BYAIR_MESSRS_SI2);
            sheetMain.GetRow(7).GetCell(13).SetCellValue(model.BYAIR_MESSRS_SI3);
            sheetMain.GetRow(16).GetCell(12).SetCellValue(model.BYAIR_DGCARGO == "D" || model.BYAIR_DGCARGO == "d" ? "√" : " ");
            sheetMain.GetRow(17).GetCell(12).SetCellValue(model.BYAIR_DGCARGO == "R" || model.BYAIR_DGCARGO == "r" ? "√" : " ");
            sheetMain.GetRow(24).GetCell(0).SetCellValue(model.BYAIR_FLIGHT_SI != "" ? model.BYAIR_FLIGHT_SI : " ");
            if (model.DEPARTURE_ABOUT == null)
                sheetMain.GetRow(24).GetCell(6).SetCellType(CellType.Blank);
            else
                sheetMain.GetRow(24).GetCell(6).SetCellValue(model.DEPARTURE_ABOUT.Value);

            sheetMain.GetRow(26).GetCell(0).SetCellValue(model.BYAIR_CONNECTING_FLIGHT_SI != "" ? model.BYAIR_CONNECTING_FLIGHT_SI : " ");
            if (model.DEPARTURE_ABOUT2 == null)
                sheetMain.GetRow(26).GetCell(6).SetCellType(CellType.Blank);
            else
                sheetMain.GetRow(26).GetCell(6).SetCellValue(model.DEPARTURE_ABOUT2.Value);

            sheetMain.GetRow(28).GetCell(0).SetCellValue(model.PORT_LOADING);
            sheetMain.GetRow(28).GetCell(6).SetCellValue(model.PORT_DISCHARGE);
            sheetMain.GetRow(27).GetCell(13).SetCellValue(model.SHIP_INSTRUCTION_NO);
            sheetMain.GetRow(32).GetCell(6).SetCellValue(model.BYAIR_TITLE_SI);
            sheetMain.GetRow(32).GetCell(1).SetCellValue(model.BYAIR_MARKS_NUMBER);
            sheetMain.GetRow(33).GetCell(1).SetCellValue(model.BYAIR_COUNTRY);
            sheetMain.GetRow(33).GetCell(8).SetCellValue(model.INVOICE_NO);
            sheetMain.GetRow(32).GetCell(14).SetCellValue(model.GROSS_WEIGHT);
            sheetMain.GetRow(33).GetCell(14).SetCellValue(model.NET_WEIGHT);
            sheetMain.GetRow(32).GetCell(17).SetCellValue(model.MEASUREMENT);
            sheetMain.GetRow(37).GetCell(4).SetCellValue(model.TOTAL_CASE);
            sheetMain.GetRow(39).GetCell(1).SetCellValue(model.COMPANY);
            sheetMain.GetRow(49).GetCell(0).SetCellValue(model.FREIGHT_PAYABLE);
            sheetMain.GetRow(49).GetCell(6).SetCellValue(model.PREPAID);
            sheetMain.GetRow(49).GetCell(7).SetCellValue(model.COLLECT);
            sheetMain.GetRow(49).GetCell(8).SetCellValue(model.BL_RELEASE);
            sheetMain.GetRow(49).GetCell(14).SetCellValue((DateTime)model.ISSUE_DT);
            sheetMain.GetRow(51).GetCell(2).SetCellValue(model.BYAIR_NOTED_SI1);
            sheetMain.GetRow(52).GetCell(3).SetCellValue(model.BYAIR_NOTED_SI2);
            sheetMain.GetRow(53).GetCell(3).SetCellValue(model.BYAIR_NOTED_SI3);
            sheetMain.GetRow(54).GetCell(3).SetCellValue(model.BYAIR_NOTED_SI4);
            sheetMain.GetRow(55).GetCell(3).SetCellValue(model.BYAIR_NOTED_SI5);
            sheetMain.GetRow(56).GetCell(3).SetCellValue(model.BYAIR_NOTED_SI6);
            sheetMain.GetRow(56).GetCell(11).SetCellValue(model.VANNING_NO);
        }

        private void ExcelInsertApprovedImage(XSSFWorkbook workbook, ISheet sheet1, string path, int RowDest, int coldest)
        {
            XSSFDrawing patriarch = (XSSFDrawing)sheet1.CreateDrawingPatriarch();
            XSSFClientAnchor anchor;
            anchor = new XSSFClientAnchor(0, 0, 3, 3, coldest, RowDest, coldest, RowDest);
            anchor.AnchorType = (AnchorType)2;
            //load the picture and get the picture index in the workbook
            XSSFPicture picture = (XSSFPicture)patriarch.CreatePicture(anchor, ExcelLoadImage(path, workbook));
            //Reset the image to the original size.
            picture.Resize();
        }

        public int ExcelLoadImage(string path, XSSFWorkbook workbook)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, (int)file.Length);
            return workbook.AddPicture(buffer, PictureType.JPEG);

        }

        private static void GenerateExcelAttachment(XSSFWorkbook workbook, ISheet sheetAttachment, mShippingInstruction model)
        {
            int row = 2;
            IRow Hrow;
            ICellStyle styleLeft = workbook.CreateCellStyle();
            styleLeft.BorderLeft = BorderStyle.Thin;
            styleLeft.BorderBottom = BorderStyle.None;

            ICellStyle styleLeftbottom = workbook.CreateCellStyle();
            styleLeftbottom.BorderLeft = BorderStyle.Thin;
            styleLeftbottom.BorderBottom = BorderStyle.Thin;

            ICellStyle styleright = workbook.CreateCellStyle();
            styleright.BorderRight = BorderStyle.Thin;
            styleright.BorderBottom = BorderStyle.None;

            ICellStyle stylerightbottom = workbook.CreateCellStyle();
            stylerightbottom.BorderRight = BorderStyle.Thin;
            stylerightbottom.BorderBottom = BorderStyle.Thin;

            ICellStyle stylebottom = workbook.CreateCellStyle();
            stylebottom.BorderBottom = BorderStyle.Thin;

            foreach (ReadyCargoCase rc in model.AttachmentReadyCargoCase)
            {

                Hrow = sheetAttachment.CreateRow(row);
                Hrow.CreateCell(0).SetCellValue(rc.No);
                Hrow.CreateCell(1).SetCellValue(rc.CASE_NO);
                Hrow.CreateCell(2).SetCellValue(rc.CASE_GROSS_WEIGHT);
                Hrow.CreateCell(3).SetCellValue(rc.CASE_NET_WEIGHT);
                Hrow.CreateCell(4).SetCellValue(rc.CASE_MEASUREMENT);
                Hrow.CreateCell(5).SetCellValue(rc.CASE_LENGTH);
                Hrow.CreateCell(6).SetCellValue(rc.CASE_WIDTH);
                Hrow.CreateCell(7).SetCellValue(rc.CASE_HEIGHT);
                //create style
                if (rc.No == model.AttachmentReadyCargoCase.Max(C => C.No))
                {
                    Hrow.GetCell(0).CellStyle = styleLeftbottom;
                    Hrow.GetCell(1).CellStyle = stylebottom;
                    Hrow.GetCell(2).CellStyle = stylebottom;
                    Hrow.GetCell(3).CellStyle = stylebottom;
                    Hrow.GetCell(4).CellStyle = stylebottom;
                    Hrow.GetCell(5).CellStyle = stylebottom;
                    Hrow.GetCell(6).CellStyle = stylebottom;
                    Hrow.GetCell(7).CellStyle = stylerightbottom;

                }
                else
                {
                    Hrow.GetCell(0).CellStyle = styleLeft;
                    Hrow.GetCell(7).CellStyle = styleright;

                }
                row++;
            }
            Hrow = sheetAttachment.CreateRow(row);
            Hrow.CreateCell(0).SetCellType(CellType.Blank);
            Hrow.CreateCell(1).SetCellType(CellType.Blank);
            Hrow.CreateCell(2).SetCellValue(model.AttachmentReadyCargoCase.Sum(c => c.CASE_GROSS_WEIGHT));
            Hrow.CreateCell(3).SetCellValue(model.AttachmentReadyCargoCase.Sum(c => c.CASE_NET_WEIGHT));
            Hrow.CreateCell(4).SetCellValue(model.AttachmentReadyCargoCase.Sum(c => c.CASE_MEASUREMENT));
            Hrow.CreateCell(5).SetCellType(CellType.Blank);
            Hrow.CreateCell(6).SetCellType(CellType.Blank);
            Hrow.CreateCell(7).SetCellType(CellType.Blank);
            //create style
            Hrow.GetCell(0).CellStyle = styleLeftbottom;
            Hrow.GetCell(1).CellStyle = stylebottom;
            Hrow.GetCell(2).CellStyle = stylebottom;
            Hrow.GetCell(3).CellStyle = stylebottom;
            Hrow.GetCell(4).CellStyle = stylebottom;
            Hrow.GetCell(5).CellStyle = stylebottom;
            Hrow.GetCell(6).CellStyle = stylebottom;
            Hrow.GetCell(7).CellStyle = stylerightbottom;
        }



        private string GeneratePDFSI(string INV_NO, mShippingInstruction model, VanningInstructionByAir download, string ORIGINAL)
        {
            string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            mTelerik report;
            model.APPROVED_BY = HttpContext.Request.MapPath(model.APPROVED_BY);
            String source = Server.MapPath("~/Report/Vanning_Instruction_SI_Layout.trdx");
            FileInfo fileInfo = new FileInfo(source);

            pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
            Directory.CreateDirectory(pdfFolderPath);

            pdfFilename = "SI_" + date;
            if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                System.IO.File.Delete(pdfFolderPath + pdfFilename);

            report = new mTelerik(source, pdfFilename);

            report.AddParameters("inv_no", INV_NO);
            report.AddParameters("ORIGINAL", ORIGINAL);
            report.SetConnectionString(Common.GetTelerikConnectionString());
            Dictionary<string, string> uriParam = new Dictionary<string, string>();


            report.SetSubConnectionString(uriParam, Common.GetTelerikConnectionString());

            using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
            {
                byte[] info = report.GeneratePDF();
                fs.Write(info, 0, info.Length);
            }

            return report.ResultName;
        }

        private string GeneratePDFAttachment(string INV_NO, VanningInstructionByAir download)
        {
            string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            mTelerik report;
            //model.APPROVED_BY = HttpContext.Request.MapPath(model.APPROVED_BY);
            String source = Server.MapPath("~/Report/Vanning_Instruction_SI_Attachment.trdx");
            FileInfo fileInfo = new FileInfo(source);

            pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
            Directory.CreateDirectory(pdfFolderPath);

            //DeleteOlderFile(pdfFolderPath);

            pdfFilename = "SI_Attachment" + date;
            if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                System.IO.File.Delete(pdfFolderPath + pdfFilename);

            report = new mTelerik(source, pdfFilename);

            report.AddParameters("inv_no", INV_NO);
            report.SetConnectionString(Common.GetTelerikConnectionString());
            
            using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
            {
                byte[] info = report.GeneratePDF();
                fs.Write(info, 0, info.Length);
            }

            return report.ResultName;
        }
        
        public ActionResult _PopUpSI(string INV_NO)
        {
            //mInvoice model = new mInvoice().getInvoice(INV_NO);
            mInvoice model = new VanningInstructionByAir().getInvoice(INV_NO);
            return PartialView("_PartialLoadDocument", model);
        }

        public string GenerateMainPDFSI(string INVOICE_NO, string FLIGHT, string DEPARTURE_ABOUT, string CONNECTING_FLIGHT, string CONNECTING_DEPARTURE_ABOUT, string ORIGINAL)
        {
            string result = "";
            mInvoice invoice = new mInvoice();
            invoice.INVOICE_NO = INVOICE_NO;
            invoice.BYAIR_FLIGHT = FLIGHT;
            invoice.BYAIR_CONNECTING_FLIGHT = CONNECTING_FLIGHT;
            invoice.DEPARTURE_ABOUT_1 = GetDate(DEPARTURE_ABOUT);
            if (CONNECTING_DEPARTURE_ABOUT != "" && CONNECTING_DEPARTURE_ABOUT != "01.01.0100")
                invoice.DEPARTURE_ABOUT_2 = GetDate(CONNECTING_DEPARTURE_ABOUT);
            else
                invoice.DEPARTURE_ABOUT_2 = new DateTime(1900, 1, 1);

            string res = invoice.UpdateInvoiceByAirHeader(invoice);

            mShippingInstruction modelSI = new mShippingInstruction();
            VanningInstructionByAir download = new VanningInstructionByAir();
            modelSI = download.getShippingInstructionDownload(INVOICE_NO, ORIGINAL);
            string Pdffilename = GeneratePDFSI(INVOICE_NO, modelSI, download, ORIGINAL);
            string Attachment = GeneratePDFAttachment(INVOICE_NO, download);

            result += Pdffilename + "|" + Attachment;
            return result;
        }

        public ActionResult GenaretePDFVanning(string VANNING_NO, string PICKUP_DATE)
        {
            try
            {
                VanningInstructionByAir download = new VanningInstructionByAir();
                download.getShippingInstructionAttachment(VANNING_NO, PICKUP_DATE);
                string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                mTelerik report;
                //model.APPROVED_BY = HttpContext.Request.MapPath(model.APPROVED_BY);
                String source = Server.MapPath("~/Report/Vanning_Instruction.trdx");
                FileInfo fileInfo = new FileInfo(source);

                pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
                Directory.CreateDirectory(pdfFolderPath);

                //DeleteOlderFile(pdfFolderPath);

                pdfFilename = "VANNING_" + date;
                if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                    System.IO.File.Delete(pdfFolderPath + pdfFilename);

                report = new mTelerik(source, pdfFilename);

                report.AddParameters("VANNING_NO", VANNING_NO);
                report.SetConnectionString(Common.GetTelerikConnectionString());

                using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
                {
                    byte[] info = report.GeneratePDF();
                    fs.Write(info, 0, info.Length);
                }
                return Json(new { success = "true", messages = "sukses", FILE_NAME = report.ResultName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message, FILE_NAME = "" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public ActionResult GenareteEXCELVanning(string VANNING_NO, string PICKUP_DATE)
        {
            try
            {
                VanningInstructionByAir download = new VanningInstructionByAir();
                download.getShippingInstructionAttachment(VANNING_NO, PICKUP_DATE);
                string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                mTelerik report;
                //model.APPROVED_BY = HttpContext.Request.MapPath(model.APPROVED_BY);
                String source = Server.MapPath("~/Report/Vanning_Instruction.trdx");
                FileInfo fileInfo = new FileInfo(source);

                pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
                Directory.CreateDirectory(pdfFolderPath);

                //DeleteOlderFile(pdfFolderPath);

                pdfFilename = "VANNING_" + date;
                if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                    System.IO.File.Delete(pdfFolderPath + pdfFilename);

                report = new mTelerik(source, pdfFilename, true);

                report.AddParameters("VANNING_NO", VANNING_NO);
                report.SetConnectionString(Common.GetTelerikConnectionString());


                using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
                {
                    byte[] info = report.GenerateXLS();
                    fs.Write(info, 0, info.Length);
                }
                return Json(new { success = "true", messages = "sukses", FILE_NAME = report.ResultName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message, FILE_NAME = "" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public void GenerateVanningInstructionDetail(string Original, string Vanning_No, string VANNING_DT_FROM, string VANNING_DT_TO, string Complete_Data, string CASE_NO, string INVOICE_NO, string SI_NO, string ETD_FROM, string ETD_TO, string FORWARDING_AGENT, string PACKING_COMPANY)
        {
            string filename = string.Empty;
            string filesTmp = HttpContext.Request.MapPath("~/Template/Vanning_Instruction_Inquiry.xlsx");

            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            IDataFormat format = workbook.CreateDataFormat();

            ISheet sheetMain = workbook.GetSheet("Inquiry");
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            filename = "Invoice_Inquiry_" + date + "_" + getpE_UserId.Username + ".xlsx";

            List<VanningInstructionByAir> download = new List<VanningInstructionByAir>();
            VanningInstructionByAir model = new VanningInstructionByAir();
            
            download = model.GetVanningInstructionByAirDetail(Original, Vanning_No, VANNING_DT_FROM, VANNING_DT_TO, Complete_Data, CASE_NO, INVOICE_NO, SI_NO, ETD_FROM, ETD_TO, FORWARDING_AGENT, PACKING_COMPANY);
            GenerateExcelVanningInstructionDetail(download, sheetMain, workbook, getpE_UserId.Name);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            var att = String.Format("attachment;filename={0}", filename);
            Response.AddHeader("content-disposition", att);
        }

        public static void GenerateExcelVanningInstructionDetail(List<VanningInstructionByAir> download, ISheet sheetMain, XSSFWorkbook workbook, string user)
        {
            int row = 9;
            IRow Hrow;
            ICellStyle styleBorder = workbook.CreateCellStyle();
            styleBorder.BorderLeft = BorderStyle.Thin;
            styleBorder.BorderBottom = BorderStyle.Thin;
            styleBorder.BorderRight = BorderStyle.Thin;
            styleBorder.BorderTop = BorderStyle.Thin;


            sheetMain.GetRow(4).GetCell(2).SetCellValue(DateTime.Now.ToString("yyyy-MM-dd"));
            sheetMain.GetRow(5).GetCell(2).SetCellValue(user);
            foreach (VanningInstructionByAir vanning in download)
            {
                Hrow = sheetMain.CreateRow(row);
                Hrow.CreateCell(0).SetCellValue(vanning.BUYER_CD);
                Hrow.CreateCell(1).SetCellValue(vanning.PD_CD);
                Hrow.CreateCell(2).SetCellValue(vanning.ORIGINAL);
                Hrow.CreateCell(3).SetCellValue(vanning.COUNTRY);
                Hrow.CreateCell(4).SetCellValue(vanning.INVOICE_NO);
                Hrow.CreateCell(5).SetCellValue(vanning.VANNING_NO);
                Hrow.CreateCell(6).SetCellValue(vanning.SI_NO);
                Hrow.CreateCell(7).SetCellValue(vanning.ETD);
                Hrow.CreateCell(8).SetCellValue(vanning.VANNING_DT);
                Hrow.CreateCell(9).SetCellValue(vanning.FA);
                Hrow.CreateCell(10).SetCellValue(vanning.CASE);
                Hrow.CreateCell(11).SetCellValue(vanning.GROSS_WEIGHT);
                Hrow.CreateCell(12).SetCellValue(vanning.NET_WEIGHT);
                Hrow.CreateCell(13).SetCellValue(vanning.MEASUREMENT);
                Hrow.CreateCell(14).SetCellValue(vanning.LENGTH);
                Hrow.CreateCell(15).SetCellValue(vanning.WIDTH);
                Hrow.CreateCell(16).SetCellValue(vanning.HEIGHT);
                Hrow.CreateCell(17).SetCellValue(vanning.AMOUNT);
                Hrow.CreateCell(18).SetCellValue(vanning.PACKAGE_DT);
                Hrow.CreateCell(19).SetCellValue(vanning.CREATED_BY);
                Hrow.CreateCell(20).SetCellValue(vanning.CREATED_DT);
                Hrow.GetCell(0).CellStyle = styleBorder;
                Hrow.GetCell(1).CellStyle = styleBorder;
                Hrow.GetCell(2).CellStyle = styleBorder;
                Hrow.GetCell(3).CellStyle = styleBorder;
                Hrow.GetCell(4).CellStyle = styleBorder;
                Hrow.GetCell(5).CellStyle = styleBorder;
                Hrow.GetCell(6).CellStyle = styleBorder;
                Hrow.GetCell(7).CellStyle = styleBorder;
                Hrow.GetCell(8).CellStyle = styleBorder;
                Hrow.GetCell(9).CellStyle = styleBorder;
                Hrow.GetCell(10).CellStyle = styleBorder;
                Hrow.GetCell(11).CellStyle = styleBorder;
                Hrow.GetCell(12).CellStyle = styleBorder;
                Hrow.GetCell(13).CellStyle = styleBorder;
                Hrow.GetCell(14).CellStyle = styleBorder;
                Hrow.GetCell(15).CellStyle = styleBorder;
                Hrow.GetCell(16).CellStyle = styleBorder;
                Hrow.GetCell(17).CellStyle = styleBorder;
                Hrow.GetCell(18).CellStyle = styleBorder;
                Hrow.GetCell(19).CellStyle = styleBorder;
                Hrow.GetCell(20).CellStyle = styleBorder;
                row++;
            }
        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            ar = dt.Split('.');
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;
            success = int.TryParse(ar[0].ToString(), out day);
            success = int.TryParse(ar[1].ToString(), out month);
            success = int.TryParse(ar[2].ToString(), out year);

            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        //Add agi 2017-09-07 untuk etd 
        public ActionResult _PopUpVanning(string INV_NO)
        {
            //mInvoice model = new mInvoice().getInvoice(INV_NO);
            mInvoice model = new VanningInstructionByAir().getInvoiceForVanning(INV_NO);

            return PartialView("_partial_load_vanning", model);
        }
    }
}
