﻿using System.Collections.Generic;
using System.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class SHIPPING_AGENTController : PageController
    {
        //
        // GET: /BuyerPD/
        //mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();

        public SHIPPING_AGENTController()
        {
            Settings.Title = "Shipping Agent";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //CallbackComboPriceTerm();

            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("SHIPPING AGENT Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        public ActionResult SHIPPING_AGENTCallBack(string SHIP_AGENT_CD, string SHIP_AGENT_NAME, string PIC_NAME)
        {
            if (SHIP_AGENT_CD != null)
            {
                if (SHIP_AGENT_CD.Contains("\""))
                {
                    SHIP_AGENT_CD = "";
                    SHIP_AGENT_NAME = "";
                    PIC_NAME = "";
                }
            }
            else
            {
                SHIP_AGENT_CD = "XXX/XXXX";
                SHIP_AGENT_NAME = "XXX/XXXX";
                PIC_NAME = "XXX/XXXX";
            }

            SHIPPING_AGENT ship = new SHIPPING_AGENT();
            ship.SHIP_AGENT_CD = SHIP_AGENT_CD;
            ship.SHIP_AGENT_NAME = SHIP_AGENT_NAME;
            ship.PIC_NAME = PIC_NAME;
            List<SHIPPING_AGENT> model = new List<SHIPPING_AGENT>();
            model = ship.getSHIPPING_AGENT(ship);

            return PartialView("_SHIPPING_AGENTGrid", model);
        }

        // //function download      
        // public void DownloadBuyerPD(object sender, EventArgs e, string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION)
        // {
        //     string filename = "";
        //     string filesTmp = HttpContext.Request.MapPath("~/Template/BuyerPD_Code_Download.xls");
        //     FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

        //     HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

        //     ICellStyle styleContent = workbook.CreateCellStyle();
        //     styleContent.VerticalAlignment = VerticalAlignment.Top;
        //     styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent.Alignment = HorizontalAlignment.Left;

        //     ICellStyle styleContent2 = workbook.CreateCellStyle();
        //     styleContent2.VerticalAlignment = VerticalAlignment.Top;
        //     styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent2.Alignment = HorizontalAlignment.Center;

        //     ICellStyle styleContent3 = workbook.CreateCellStyle();
        //     styleContent3.VerticalAlignment = VerticalAlignment.Top;
        //     styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent3.Alignment = HorizontalAlignment.Right;

        //     ISheet sheet = workbook.GetSheet("BuyerPD Code");
        //     string date = DateTime.Now.ToString("ddMMyyyy");
        //     filename = "BuyerPD Code Master" + date + ".xls";

        //     string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

        //     sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
        //     sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);
        //     sheet.GetRow(8).GetCell(2).SetCellValue(pBUYER_PD_CD);
        //     sheet.GetRow(9).GetCell(2).SetCellValue(pCUSTOMER_NO);
        //     sheet.GetRow(10).GetCell(2).SetCellValue(pDESCRIPTION);


        //     int row = 13;
        //     int rowNum = 1;
        //     IRow Hrow;

        //     List<mBuyerPD> model = new List<mBuyerPD>();

        //     model = BuyerPD.getListBuyerPD
        //         (pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION);

        //     foreach (var result in model)
        //     {
        //         Hrow = sheet.CreateRow(row);

        //         Hrow.CreateCell(1).SetCellValue(rowNum);
        //         Hrow.CreateCell(2).SetCellValue(result.BUYER_PD_CD);
        //         Hrow.CreateCell(3).SetCellValue(result.CUSTOMER_NO);
        //         Hrow.CreateCell(4).SetCellValue(result.DESCRIPTION);
        //         Hrow.CreateCell(5).SetCellValue(result.DELETION_FLAG);
        //         Hrow.CreateCell(6).SetCellValue(result.CREATED_BY);
        //         Hrow.CreateCell(7).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
        //         Hrow.CreateCell(8).SetCellValue(result.CHANGED_BY);
        //         Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

        //         Hrow.GetCell(1).CellStyle = styleContent3;
        //         Hrow.GetCell(2).CellStyle = styleContent2;
        //         Hrow.GetCell(3).CellStyle = styleContent2;
        //         Hrow.GetCell(4).CellStyle = styleContent;
        //         Hrow.GetCell(5).CellStyle = styleContent2;
        //         Hrow.GetCell(6).CellStyle = styleContent2;
        //         Hrow.GetCell(7).CellStyle = styleContent2;
        //         Hrow.GetCell(8).CellStyle = styleContent2;
        //         Hrow.GetCell(9).CellStyle = styleContent2;

        //         row++;
        //         rowNum++;
        //     }

        //     MemoryStream ms = new MemoryStream();
        //     workbook.Write(ms);
        //     ftmp.Close();
        //     Response.BinaryWrite(ms.ToArray());
        //     Response.ContentType = "application/vnd.ms-excel";
        //     Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        // }

        // #region Fucntion Delete Data
        public ActionResult DeleteSHIPPING_AGENT(string SHIP_AGENT_CD)
        {

            SHIPPING_AGENT ship = new SHIPPING_AGENT();
            string[] r = null;
            string resultMessage = ship.DeleteData(SHIP_AGENT_CD);

            //r = resultMessage.Split('|');
            if (resultMessage == "Error ")
            {
                return Json(new { success = "false", messages = resultMessage }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = resultMessage }, JsonRequestBehavior.AllowGet);
            }

            return null;
        }
        //#endregion 

        // #region Function Add
        public ActionResult AddNewSHIPPING_AGENT(string SHIP_AGENT_CD, string SHIP_AGENT_NAME, string PIC_NAME, string CONTACT_NO, string DESCR1, string DESCR2, string DESCR3)
        {
            //p_CREATED_BY = getpE_UserId.Username;
            //p_CREATED_BY = "SYSTEM";
            string[] r = null;
            SHIPPING_AGENT ship = new SHIPPING_AGENT();
            ship.SHIP_AGENT_CD = SHIP_AGENT_CD;
            ship.SHIP_AGENT_NAME = SHIP_AGENT_NAME;
            ship.PIC_NAME = PIC_NAME;
            ship.CONTACT_NO = CONTACT_NO;
            ship.DESCR1 = DESCR1;
            ship.DESCR2 = DESCR2;
            ship.DESCR3 = DESCR3;
            ship.CREATED_BY = getpE_UserId.Username;
            string resultMessage = ship.SaveData(ship);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion

        //#region Function Edit -
        public ActionResult UpdateSHIPPING_AGENT(string SHIP_AGENT_CD, string SHIP_AGENT_NAME, string PIC_NAME, string CONTACT_NO, string DESCR1, string DESCR2, string DESCR3)
        {
            //string p_CHANGED_BY = getpE_UserId.Username;
            //pCHANGED_BY = "SYSTEM";
            string[] r = null;
            SHIPPING_AGENT ship = new SHIPPING_AGENT();
            ship.SHIP_AGENT_CD = SHIP_AGENT_CD;
            ship.SHIP_AGENT_NAME = SHIP_AGENT_NAME;
            ship.PIC_NAME = PIC_NAME;
            ship.CONTACT_NO = CONTACT_NO;
            ship.DESCR1 = DESCR1;
            ship.DESCR2 = DESCR2;
            ship.DESCR3 = DESCR3;
            ship.CHANGED_BY = getpE_UserId.Username;
            string resultMessage = ship.UpdateData(ship);

            //string resultMessage = BuyerPD.UpdateData(pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION, pDELETION_FLAG, pCHANGED_BY);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion
    }
}
