﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class ReadyCargoByAirInquiryController : PageController
    {
        //
        // GET: /ReadyCargoByAirInquiry/

        MessagesString msg = new MessagesString();

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public ReadyCargoByAirInquiryController()
        {
            Settings.Title = "Ready Cargo By Air Inquiry";
        }

        protected override void Startup()
        {
            ViewBag.MSPX00001ERR = msg.getMSPX00001ERR("Ready Cargo By Air Inquiry");
            ViewBag.MSPX00006ERR = msg.getMSPX00006ERR();
            ViewBag.MSPx0006ERR2 = msg.getMsgText("MSPx0006ERR2");
            ViewData["Invoice_Status"] = GetAllInvoiceReadyCargoByAirStatus();
            ViewData["BUYER_PD"] = GetBuyer();
            ViewData["Packing_Company"] = GetPackingCompany();
            ViewData["DG_Cargo"] = GetDGCargo();
            ViewData["Completenes"] = GetCompletness();
            ViewData["I_VANNING_DT_FROM"] = GetLoadVaningDateFrom(); //GetCompletness();
            ViewData["I_VANNING_DT_TO"] = DateTime.Now.Date;
            getpE_UserId = (User)ViewData["User"];
            //add agi 2017-10-25
            //"ReadyCargoByAirInqury","VanDtFrom"
            //string range = new mSystemMaster().GetSystemValueList("ready cargo by air inquiry", "range day", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            //change agi 2017-10-26 request mr yoni
            string range = new mSystemMaster().GetSystemValueList("ReadyCargoByAirInqury", "VanDtFrom", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            ViewBag.MSPXC0001ERR = msg.getMsgText("MSPXC0001ERR").Replace("{0}", range.Replace("-",""));
            ViewBag.range = Convert.ToInt32(range) < 0 ? Convert.ToInt32(range) * -1 : Convert.ToInt32(range);
        }
        public List<string> GetPackingCompany()
        {
            return new ReadyCargoByAirInquiry().GetPackingCompany();
        }

        public List<mBuyerPD> GetBuyer()
        {
            return new ReadyCargoByAirInquiry().GetAllBuyerPD();
        }
        public List<string> GetAllInvoiceReadyCargoByAirStatus()
        {
            return new ReadyCargoByAirInquiry().GetAllInvoiceReadyCargoByAirStatus();
        }

        public List<string> GetDGCargo()
        {
            return new ReadyCargoByAirInquiry().GetDGCargo();
        }

        public ActionResult get_Data_GridReadyCargoBySeaCallBack()
        {
            string BUYER_PD_CD = string.Empty;
            string PACKING_COMPANY = string.Empty;
            string DG_CARGO = string.Empty;
            string INVOICE_STATUS = string.Empty;
            string VANNING_DT_FROM=string.Empty;
            string VANNING_DT_TO=string.Empty;
            string COMPLATE_STATUS=string.Empty;
            //ADD AGI 2017-09-07 CR MRS HESTI 
            string CASE_NO=string.Empty;

            BUYER_PD_CD = Request.Params["BUYER_PD_CD"];
            PACKING_COMPANY = Request.Params["Packing_Company"];
            DG_CARGO = Request.Params["DG_Cargo"];
            INVOICE_STATUS = Request.Params["Invoice_Status"];
            VANNING_DT_FROM = Request.Params["VANNING_DT_FROM"];
            VANNING_DT_TO = Request.Params["VANNING_DT_TO"];
            COMPLATE_STATUS = Request.Params["COMPLATE_STATUS"];
            //ADD AGI 2017-09-07 CR MRS HESTI 
            CASE_NO =  Request.Params["CASE_NO"];
            List<ReadyCargoByAirInquiry> model = new List<ReadyCargoByAirInquiry>();
            ReadyCargoByAirInquiry readyCargo = new ReadyCargoByAirInquiry();
            //remkas agi 2016-12-07 add parameter search
            //model = readyCargo.ReadyCargoInquirySearch(BUYER_PD_CD, PACKING_COMPANY, DG_CARGO, INVOICE_STATUS);
            
            //model = readyCargo.ReadyCargoInquirySearch(BUYER_PD_CD, PACKING_COMPANY, DG_CARGO, INVOICE_STATUS, VANNING_DT_FROM, VANNING_DT_TO, COMPLATE_STATUS);
            //CHANGE AGI 2017-09-07 CR MRS HESTI 
            model = readyCargo.ReadyCargoInquirySearch(BUYER_PD_CD, PACKING_COMPANY, DG_CARGO, INVOICE_STATUS, VANNING_DT_FROM, VANNING_DT_TO, COMPLATE_STATUS,CASE_NO);
 
            return PartialView("_partial_Grid", model);
        }

        //REMAKS AGI 2016-12-08 ADD PARAMETR MOVE TO REQUEST PARAMETER
        //public ActionResult _PopUp(string BUYER_CD, string PD_CD, string Packing_Company, string DG_Cargo, string INVOICE_NO, string COMPLETE_STATUS)
        public ActionResult _PopUp()
        {

            string BUYER_CD = string.Empty;
            string PD_CD = string.Empty;
            string PACKING_COMPANY = string.Empty;
            string DG_CARGO = string.Empty;
            string INVOICE_NO = string.Empty;
            string VANNING_DT_FROM=string.Empty;
            string VANNING_DT_TO=string.Empty;
            string COMPLETE_STATUS = string.Empty;

            BUYER_CD = Request.Params["BUYER_CD"];
            PD_CD = Request.Params["PD_CD"];
            PACKING_COMPANY = Request.Params["Packing_Company"];
            DG_CARGO = Request.Params["DG_Cargo"];
            INVOICE_NO = Request.Params["INVOICE_NO"];
            VANNING_DT_FROM = Request.Params["VANNING_DT_FROM"];
            VANNING_DT_TO = Request.Params["VANNING_DT_TO"];
            COMPLETE_STATUS = Request.Params["COMPLETE_STATUS"];
            ViewData["SetBUYER_CD"] = BUYER_CD;
            ViewData["SetPD_CD"] = PD_CD;
            ViewData["SetPacking_Company"] = PACKING_COMPANY;
            ViewData["SetDG_Cargo"] = DG_CARGO;
            ViewData["SetCOMPLETE_STATUS"] = COMPLETE_STATUS;
            ViewData["SetINVOICE_NO"] = INVOICE_NO;
            ViewData["SetVANNING_DT_FROM"] = VANNING_DT_FROM;
            ViewData["SetVANNING_DT_TO"] = VANNING_DT_TO;

            List<ReadyCargoByAirDetailInquiry> model = new List<ReadyCargoByAirDetailInquiry>();
            ReadyCargoByAirInquiry readyCargo = new ReadyCargoByAirInquiry();
            
            //REMAKS AGI 2016-12-08 ADD PARAMETER REQUEST MR YONI
            //model = readyCargo.ReadyCargoDetailInquirySearch(BUYER_CD, PD_CD, Packing_Company, DG_Cargo, INVOICE_NO, COMPLETE_STATUS);
            model = readyCargo.ReadyCargoDetailInquirySearch(BUYER_CD, PD_CD, PACKING_COMPANY, DG_CARGO, INVOICE_NO, COMPLETE_STATUS, VANNING_DT_FROM,VANNING_DT_TO);

            return PartialView("_partial_Master_PopUp", model);
            //return View();
        }
        //public ActionResult Get_Data_Detail_ReadyCargoByAir(string BUYER_CD, string PD_CD, string Packing_Company, string DG_Cargo, string INVOICE_NO, string COMPLETE_STATUS)
        public ActionResult Get_Data_Detail_ReadyCargoByAir()
        {
            //add agi 2016-12-08
            string BUYER_CD = string.Empty;
            string PD_CD = string.Empty;
            string PACKING_COMPANY = string.Empty;
            string DG_CARGO = string.Empty;
            string INVOICE_NO = string.Empty;
            string VANNING_DT_FROM = string.Empty;
            string VANNING_DT_TO = string.Empty;
            string COMPLETE_STATUS = string.Empty;

            BUYER_CD = Request.Params["BUYER_CD"];
            PD_CD = Request.Params["PD_CD"];
            PACKING_COMPANY = Request.Params["Packing_Company"];
            DG_CARGO = Request.Params["DG_Cargo"];
            INVOICE_NO = Request.Params["INVOICE_NO"];
            VANNING_DT_FROM = Request.Params["VANNING_DT_FROM"];
            VANNING_DT_TO = Request.Params["VANNING_DT_TO"];
            COMPLETE_STATUS = Request.Params["COMPLETE_STATUS"];

            List<ReadyCargoByAirDetailInquiry> model = new List<ReadyCargoByAirDetailInquiry>();
            ReadyCargoByAirInquiry readyCargo = new ReadyCargoByAirInquiry();
            //remaks agi 2016-12-08 add parameter
            //model = readyCargo.ReadyCargoDetailInquirySearch(BUYER_CD, PD_CD, Packing_Company, DG_Cargo, INVOICE_NO, COMPLETE_STATUS);
            model = readyCargo.ReadyCargoDetailInquirySearch(BUYER_CD, PD_CD, PACKING_COMPANY, DG_CARGO, INVOICE_NO, COMPLETE_STATUS,VANNING_DT_FROM,VANNING_DT_TO);
            ViewData["ReadyCargoCheck"] = model;
            return PartialView("_partial_Grid_PopUp", model);
        }

        public ActionResult CreateInvoice(List<string> KeyList)
        {
            var key = KeyList.First().ToString().Split('|');
            string message = string.Empty;
            ReadyCargoByAirInquiry model = new ReadyCargoByAirInquiry();
            model.BUYER_CD = key[1].ToString();
            model.PD_CD = key[2].ToString();
            model.CASE_DANGER_FLAG = key[3].ToString();
            model.PACKING_COMPANY = key[5].ToString();
            model.User_Name = getpE_UserId.Username;

            //add parameter vaning date from to
            model.VANNING_DT_FROM = key[6].ToString();
            model.VANNING_DT_TO = key[7].ToString();

            string session_name = "ReadyCargoByAirInquiry" + getpE_UserId.Username;
            Session[session_name] = model;
            message = model.getUrlCreateInvoice();
            return Json(new { success = "true", messages = message }, JsonRequestBehavior.AllowGet);
            return null;
        }

        //add by ria 20160719
        //remaks agi 2016-12-08 add parameter and move parameter to request param
        //public void DownloadInquiry(string BUYER_PD_CD, string PACKING_COMPANY, string DG_CARGO, string INVOICE_STATUS)
        public void DownloadInquiry()
        {
            try
            {
                
                string BUYER_PD_CD = string.Empty;
                string PACKING_COMPANY = string.Empty;
                string DG_CARGO = string.Empty;
                string INVOICE_STATUS = string.Empty;
                string VANNING_DT_FROM = string.Empty;
                string VANNING_DT_TO = string.Empty;
                string COMPLATE_STATUS = string.Empty;

                //add agi 2017-09-07 request mrs hesti. crf
                string CASE_NO = string.Empty;


                BUYER_PD_CD = Request.Params["BUYER_PD_CD"];
                PACKING_COMPANY = Request.Params["Packing_Company"];
                DG_CARGO = Request.Params["DG_Cargo"];
                INVOICE_STATUS = Request.Params["Invoice_Status"];
                VANNING_DT_FROM = Request.Params["VANNING_DT_FROM"];
                VANNING_DT_TO = Request.Params["VANNING_DT_TO"];
                COMPLATE_STATUS = Request.Params["COMPLATE_STATUS"];

                //add agi 2017-09-07 request mrs hesti. crf
                CASE_NO = Request.Params["CASE_NO"];

                string filename = "";
                string filesTmp = HttpContext.Request.MapPath("~/Template/ReadyCargoByAirInquiryDownload.xlsx");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

                IDataFormat format = workbook.CreateDataFormat();

                ICellStyle styleDecimal = workbook.CreateCellStyle();
                styleDecimal.DataFormat = HSSFDataFormat.GetBuiltinFormat("##,##0.0####");
                styleDecimal.Alignment = HorizontalAlignment.Center;
                styleDecimal.BorderBottom = BorderStyle.Thin;
                styleDecimal.BorderTop = BorderStyle.Thin;
                styleDecimal.BorderLeft = BorderStyle.Thin;
                styleDecimal.BorderRight = BorderStyle.Thin;

                ICellStyle styleDate1 = workbook.CreateCellStyle();
                styleDate1.DataFormat = HSSFDataFormat.GetBuiltinFormat("dd/MM/yyyy HH:mm:ss");
                styleDate1.Alignment = HorizontalAlignment.Center;
                styleDate1.BorderBottom = BorderStyle.Thin;
                styleDate1.BorderTop = BorderStyle.Thin;
                styleDate1.BorderLeft = BorderStyle.Thin;
                styleDate1.BorderRight = BorderStyle.Thin;

                ICellStyle style1 = workbook.CreateCellStyle();
                style1.VerticalAlignment = VerticalAlignment.Center;
                style1.Alignment = HorizontalAlignment.Center;
                style1.BorderBottom = BorderStyle.Thin;
                style1.BorderTop = BorderStyle.Thin;
                style1.BorderLeft = BorderStyle.Thin;
                style1.BorderRight = BorderStyle.Thin;

                ICellStyle style2 = workbook.CreateCellStyle();
                style2.VerticalAlignment = VerticalAlignment.Center;
                style2.Alignment = HorizontalAlignment.Left;
                style2.BorderBottom = BorderStyle.Thin;
                style2.BorderRight = BorderStyle.Thin;
                style2.BorderLeft = BorderStyle.Thin;
                style2.BorderRight = BorderStyle.None;

                ReadyCargoByAirInquiry readyCargo = new ReadyCargoByAirInquiry();

                ISheet sheetMain = workbook.GetSheet("PriceMaster");
                string date = DateTime.Now.ToString("ddMMyyyyHHmmss");

                filename = "Ready_Cargo_Inquiry_" + date + "_" + getpE_UserId.Username + ".xlsx";

                string dateNow = DateTime.Now.ToString("dd.MM.yyyy");


                List<ReadyCargoByAirInquiry> models = new List<ReadyCargoByAirInquiry>();
                //remaks agi 2016-12-08
                //models=readyCargo.ReadyCargoInquirySearch(BUYER_PD_CD, PACKING_COMPANY, DG_CARGO, INVOICE_STATUS);
                //models = readyCargo.ReadyCargoInquirySearch(BUYER_PD_CD, PACKING_COMPANY, DG_CARGO, INVOICE_STATUS, VANNING_DT_FROM,VANNING_DT_TO,COMPLATE_STATUS);
                //CHANGE AGI 2017-09-07 REQUEST MRS HESTI
                models = readyCargo.ReadyCargoInquirySearch(BUYER_PD_CD, PACKING_COMPANY, DG_CARGO, INVOICE_STATUS, VANNING_DT_FROM, VANNING_DT_TO, COMPLATE_STATUS, CASE_NO);

                int startRow = 12;
                IRow Hrow;

                Hrow = sheetMain.GetRow(5);
                Hrow.CreateCell(2).SetCellValue(DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                Hrow.GetCell(2).CellStyle = style2;
                Hrow = sheetMain.GetRow(6);
                Hrow.CreateCell(2).SetCellValue(getpE_UserId.Username);
                Hrow.GetCell(2).CellStyle = style2;

                foreach (ReadyCargoByAirInquiry model in models)
                {
                    Hrow = sheetMain.CreateRow(startRow);
                    //Hrow.CreateCell(0).SetCellValue("");
                    Hrow.CreateCell(1).SetCellValue((startRow - 11).ToString());
                    Hrow.GetCell(1).CellStyle = style1;
                    Hrow.CreateCell(2).SetCellValue(model.BUYER_CD);
                    Hrow.GetCell(2).CellStyle = style1;
                    Hrow.CreateCell(3).SetCellValue(model.PD_CD);
                    Hrow.GetCell(3).CellStyle = style1;
                    Hrow.CreateCell(4).SetCellValue(model.CASE_DANGER_FLAG );
                    Hrow.GetCell(4).CellStyle = style1;
                    Hrow.CreateCell(5).SetCellValue(model.PACKING_COMPANY);
                    Hrow.GetCell(5).CellStyle = style1;
                    Hrow.CreateCell(6).SetCellValue(model.INVOICE_NO);
                    Hrow.GetCell(6).CellStyle = style1;
                    Hrow.CreateCell(7).SetCellValue(model.COMPLETE);
                    Hrow.GetCell(7).CellStyle = style1;
                    Hrow.CreateCell(8).SetCellValue(model.INCOMPLETE);
                    Hrow.GetCell(8).CellStyle = style1;
                    //add agi 2017-09-07 request mrs hesti
                    Hrow.CreateCell(9).SetCellValue(model.VANNING_NO);
                    Hrow.GetCell(9).CellStyle = style1;
                    Hrow.CreateCell(10).SetCellValue(model.FORWARDER);
                    Hrow.GetCell(10).CellStyle = style1;

                    startRow++;
                }

                ExcelInsertHeaderImage(workbook, sheetMain, HttpContext.Request.MapPath("~/Content/images/excel_header.png"), 0);

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                Response.BinaryWrite(ms.ToArray());
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            }
            catch (Exception ex)
            {

            }
        }

        //public void DownloadDetail(string BUYER_CD, string BUYER_PD, string PACKING_COMPANY, string DG_CARGO, string INVOICE_STATUS, string COMPLETE_STATUS)
        //{
        //    try
        //    {
        //        string filename = "";
        //        string filesTmp = HttpContext.Request.MapPath("~/Template/ReadyCargoByAirInquiryDownload.xlsx");

        //        FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

        //        XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

        //        IDataFormat format = workbook.CreateDataFormat();

        //        ICellStyle styleDecimal = workbook.CreateCellStyle();
        //        styleDecimal.DataFormat = HSSFDataFormat.GetBuiltinFormat("##,##0.0####");
        //        styleDecimal.Alignment = HorizontalAlignment.Center;
        //        styleDecimal.BorderBottom = BorderStyle.Thin;
        //        styleDecimal.BorderTop = BorderStyle.Thin;
        //        styleDecimal.BorderLeft = BorderStyle.Thin;
        //        styleDecimal.BorderRight = BorderStyle.Thin;

        //        ICellStyle styleDate1 = workbook.CreateCellStyle();
        //        styleDate1.DataFormat = HSSFDataFormat.GetBuiltinFormat("dd/MM/yyyy HH:mm:ss");
        //        styleDate1.Alignment = HorizontalAlignment.Center;
        //        styleDate1.BorderBottom = BorderStyle.Thin;
        //        styleDate1.BorderTop = BorderStyle.Thin;
        //        styleDate1.BorderLeft = BorderStyle.Thin;
        //        styleDate1.BorderRight = BorderStyle.Thin;

        //        ICellStyle style1 = workbook.CreateCellStyle();
        //        style1.VerticalAlignment = VerticalAlignment.Center;
        //        style1.Alignment = HorizontalAlignment.Center;
        //        style1.BorderBottom = BorderStyle.Thin;
        //        style1.BorderTop = BorderStyle.Thin;
        //        style1.BorderLeft = BorderStyle.Thin;
        //        style1.BorderRight = BorderStyle.Thin;

        //        ICellStyle style2 = workbook.CreateCellStyle();
        //        style2.VerticalAlignment = VerticalAlignment.Center;
        //        style2.Alignment = HorizontalAlignment.Left;
        //        style2.BorderBottom = BorderStyle.Thin;
        //        style2.BorderRight = BorderStyle.Thin;
        //        style2.BorderLeft = BorderStyle.Thin;
        //        style2.BorderRight = BorderStyle.None;

        //        ReadyCargoByAirInquiry readyCargo = new ReadyCargoByAirInquiry();

        //        ISheet sheetMain = workbook.GetSheet("PriceMaster");
        //        string date = DateTime.Now.ToString("ddMMyyyyHHmmss");

        //        filename = "Ready_Cargo_Inquiry_" + date + "_" + getpE_UserId.Username + ".xlsx";

        //        string dateNow = DateTime.Now.ToString("dd.MM.yyyy");


        //        List<ReadyCargoByAirInquiry> models = readyCargo.ReadyCargoInquirySearch(BUYER_PD_CD, PACKING_COMPANY, DG_CARGO, INVOICE_STATUS);
        //        int startRow = 12;
        //        IRow Hrow;

        //        Hrow = sheetMain.GetRow(5);
        //        Hrow.CreateCell(2).SetCellValue(DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
        //        Hrow.GetCell(2).CellStyle = style2;
        //        Hrow = sheetMain.GetRow(6);
        //        Hrow.CreateCell(2).SetCellValue(getpE_UserId.Username);
        //        Hrow.GetCell(2).CellStyle = style2;

        //        foreach (ReadyCargoByAirInquiry model in models)
        //        {
        //            Hrow = sheetMain.CreateRow(startRow);
        //            //Hrow.CreateCell(0).SetCellValue("");
        //            Hrow.CreateCell(1).SetCellValue((startRow - 11).ToString());
        //            Hrow.GetCell(1).CellStyle = style1;
        //            Hrow.CreateCell(2).SetCellValue(model.BUYER_CD);
        //            Hrow.GetCell(2).CellStyle = style1;
        //            Hrow.CreateCell(3).SetCellValue(model.PD_CD);
        //            Hrow.GetCell(3).CellStyle = style1;
        //            Hrow.CreateCell(4).SetCellValue(model.CASE_DANGER_FLAG);
        //            Hrow.GetCell(4).CellStyle = style1;
        //            Hrow.CreateCell(5).SetCellValue(model.PACKING_COMPANY);
        //            Hrow.GetCell(5).CellStyle = style1;
        //            Hrow.CreateCell(6).SetCellValue(model.INVOICE_NO);
        //            Hrow.GetCell(6).CellStyle = style1;
        //            Hrow.CreateCell(7).SetCellValue(model.COMPLETE);
        //            Hrow.GetCell(7).CellStyle = style1;
        //            Hrow.CreateCell(8).SetCellValue(model.INCOMPLETE);
        //            Hrow.GetCell(8).CellStyle = style1;
        //            startRow++;
        //        }

        //        ExcelInsertHeaderImage(workbook, sheetMain, HttpContext.Request.MapPath("~/Content/images/excel_header.png"), 0);

        //        MemoryStream ms = new MemoryStream();
        //        workbook.Write(ms);
        //        ftmp.Close();
        //        Response.BinaryWrite(ms.ToArray());
        //        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //        Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        private void ExcelInsertHeaderImage(XSSFWorkbook workbook, ISheet sheet1, string path, int rowDest)
        {
            XSSFDrawing patriarch = (XSSFDrawing)sheet1.CreateDrawingPatriarch();
            XSSFClientAnchor anchor;
            anchor = new XSSFClientAnchor(0, 0, 0, 0, 1, rowDest, 2, rowDest);
            anchor.AnchorType = (AnchorType)2;
            //load the picture and get the picture index in the workbook
            XSSFPicture picture = (XSSFPicture)patriarch.CreatePicture(anchor, ExcelLoadImage(path, workbook));
            //Reset the image to the original size.
            picture.Resize();
            //picture.LineStyle = HSSFPicture.LINESTYLE_DASHDOTGEL;
        }

        public int ExcelLoadImage(string path, XSSFWorkbook workbook)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, (int)file.Length);
            return workbook.AddPicture(buffer, PictureType.JPEG);

        }

        //add by agi 2016-12-07 Request Mr Yoni
        public List<string> GetCompletness()
        {
            return new ReadyCargoByAirInquiry().GetCompletnesStatus();
        }

        public DateTime GetLoadVaningDateFrom()
        {
            DateTime VaningDateFrom=DateTime.Now.AddMonths(Convert.ToInt32(Common.GetDataMasterSystem("ReadyCargoByAirInqury","VanDtFrom")));
            return VaningDateFrom;
        }
    }
}
