﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class PDCodeController : PageController
    {
        //
        // GET: /PDCode/
        mPDCode PDCode = new mPDCode();
        MessagesString msgError = new MessagesString();

        public PDCodeController()
        {
            Settings.Title = "PD Code Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("PD Code Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        public ActionResult PDCodeCallBack(string pPD_CD, string pDESCRIPTION)
        {
            List<mPDCode> model = new List<mPDCode>();

            {
                model = PDCode.getListPDCode
                (pPD_CD, pDESCRIPTION);
            }
            return PartialView("PDCodeGrid", model);
        }

        //function download      
        public void DownloadPDCode(object sender, EventArgs e, string pPD_CD, string pDESCRIPTION)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/PD_Code_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("PD Code");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "PD Code Master" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(8).GetCell(2).SetCellValue(pPD_CD);
            sheet.GetRow(9).GetCell(2).SetCellValue(pDESCRIPTION);


            int row = 12;
            int rowNum = 1;
            IRow Hrow;

            List<mPDCode> model = new List<mPDCode>();

            model = PDCode.getListPDCode(pPD_CD, pDESCRIPTION);
            
            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PD_CD);
                Hrow.CreateCell(3).SetCellValue(result.DESCRIPTION);
                Hrow.CreateCell(4).SetCellValue(result.SHIPMENT_FLAG);
                Hrow.CreateCell(5).SetCellValue(result.ECLUSIVE_FLAG);
                Hrow.CreateCell(6).SetCellValue(result.USED_FLAG);
                Hrow.CreateCell(7).SetCellValue(result.DELETION_FLAG);
                Hrow.CreateCell(8).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(10).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(11).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent;
                Hrow.GetCell(5).CellStyle = styleContent;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent;

                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        #region Fucntion Delete Data
        public ActionResult DeletePDCode(string p_PD_CD, string p_CHANGED_BY)
        {
            if (getpE_UserId!=null)
            {
                p_CHANGED_BY = getpE_UserId.Username;
                //p_CHANGED_BY = "system";
                string[] r = null;
                string resultMessage = PDCode.DeleteData(p_PD_CD, p_CHANGED_BY);

                r = resultMessage.Split('|');
                if (r[0] == "Error ")
                {
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = "false", messages = "session expired, please login" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        #endregion

        #region Function Add
        public ActionResult AddNewPDCode(string p_PD_CD, string p_DESCRIPTION, string p_SHIPMENT_FLAG, string p_EXCLUSIVE_FLAG, string p_CREATED_BY)
        {
            if (getpE_UserId!=null)
            {
                p_CREATED_BY = getpE_UserId.Username;
                //p_CREATED_BY = "SYSTEM";
                string[] r = null;
                string resultMessage = PDCode.SaveData(p_PD_CD, p_DESCRIPTION, p_SHIPMENT_FLAG, p_EXCLUSIVE_FLAG, p_CREATED_BY);

                r = resultMessage.Split('|');
                if (r[0] == "Error ")
                {
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { success = "false", messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
            }
          return null;
        }
        #endregion

        #region Function Edit - Update
        public ActionResult UpdatePDCode(string p_PD_CD, string p_DESCRIPTION, string p_SHIPMENT_FLAG, string p_EXCLUSIVE_FLAG, string p_DELETION_FLAG, string p_CHANGED_BY)
        {
            if (getpE_UserId!=null)
            {
                p_CHANGED_BY = getpE_UserId.Username;
                //p_CHANGED_BY = "system";
                string[] r = null;
                string resultMessage = PDCode.UpdateData(p_PD_CD, p_DESCRIPTION, p_SHIPMENT_FLAG, p_EXCLUSIVE_FLAG, p_DELETION_FLAG, p_CHANGED_BY);

                r = resultMessage.Split('|');
                if (r[0] == "Error ")
                {
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = "false", messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        #endregion

    }
}
