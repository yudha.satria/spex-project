﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using NPOI.XSSF.UserModel;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Web;
using System.Text;
using System.Linq;
namespace SPEX.Controllers
{
    public class DynamicQueryController : PageController
    {

        Log log = new Log();
        MessagesString msgError = new MessagesString();
        public DynamicQueryController()
        {
            Settings.Title = "Dynamic Query";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            ViewBag.MSPX000018ERR = msgError.getMSPX00018ERR();
            getpE_UserId = (User)ViewData["User"];
            DynamicQuery report = new DynamicQuery();
            //ViewData["SQL_Report"] = report.GetAllReportByName();

        }


        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
                //result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));
            }

            return result;
        }

        [HttpGet]
        public ActionResult GetReportParameter(string ReportName)
        {
            DynamicQuery report = new DynamicQuery();
            List<DynamicQuery> Model = new List<DynamicQuery>();
            Model = report.GetParameterScr(ReportName);
            return PartialView(Model);
        }

        public ActionResult GetAllQueryName(string pROCESS)
        {
            DynamicQuery Query = new DynamicQuery();
            List<DynamicQuery> Querys = new List<DynamicQuery>();
            if(getpE_UserId!=null)
            {   
                Querys = Query.GetAllQueryByName(pROCESS, getpE_UserId.Roles.Where(a => a._SystemId == "SPEX").First().Id);
            }
            else
            {
                ViewBag.sessionexpired = "session expired, please login";
            }
            return PartialView("_PartialGrid", Querys);
        }

        public ActionResult GenerateForm(string ProcesName)
        {
            DynamicQuery report = new DynamicQuery();
            List<DynamicQuery> Reports = report.GetParameterScr(ProcesName);
            return PartialView("_PartialScr", Reports);
        }

        public void Download(string Values)
        {
            
            var Param = System.Web.Helpers.Json.Decode<List<DynamicQuery>>(Values);
            //ExportToExcel(Param);
            DynamicQuery DQ=new DynamicQuery();
            DataTable DT = DQ.GET_DATA_FOR_DOWNLOAD(Param);
            string ext = DQ.GetExtReport(Param.FirstOrDefault().PROCESS_NAME);
            if (ext.ToLower().Contains("xls"))
            {
                WriteExcelWithNPOI("xlsx", DT);
            }
            else if (ext.ToLower().Contains("pdf"))
            {
                ExportToPdf(DT);
            }
            else if (ext.ToLower().Contains("csv"))
            {
                ExportToCSV(DT);
            }
            else if (ext.ToLower().Contains("doc"))
            {
                ExportToWord(DT);
            }
        }

        public void ExportToExcel(List<DynamicQuery> ColumnParameter)
        {
            var grid = new GridView();
            grid.DataSource = new DynamicQuery().GET_DATA_FOR_DOWNLOAD(ColumnParameter);
            grid.DataBind();

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=MyExcelFile.xls");
            Response.ContentType = "application/ms-excel";

            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }


        public void WriteExcelWithNPOI(String extension, DataTable dt)
        {
            // dll refered NPOI.dll and NPOI.OOXML  

            IWorkbook workbook;

            if (extension == "xlsx")
            {
                workbook = new XSSFWorkbook();
            }
            else if (extension == "xls")
            {
                workbook = new HSSFWorkbook();
            }
            else
            {
                throw new Exception("This format is not supported");
            }

            ISheet sheet1 = workbook.CreateSheet("Sheet 1");

            //make a header row  
            IRow row1 = sheet1.CreateRow(0);

            for (int j = 0; j < dt.Columns.Count; j++)
            {

                ICell cell = row1.CreateCell(j);

                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(columnName);
            }

            //loops through data  
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                IRow row = sheet1.CreateRow(i + 1);
                for (int j = 0; j < dt.Columns.Count; j++)
                {

                    ICell cell = row.CreateCell(j);
                    String columnName = dt.Columns[j].ToString();
                    cell.SetCellValue(dt.Rows[i][columnName].ToString());
                }
            }

            using (var exportData = new MemoryStream())
            {
                Response.Clear();
                workbook.Write(exportData);
                if (extension == "xlsx") //xlsx file format  
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", "tpms_Dict.xlsx"));
                    Response.BinaryWrite(exportData.ToArray());
                }
                else if (extension == "xls")  //xls file format  
                {
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", "tpms_dict.xls"));
                    Response.BinaryWrite(exportData.GetBuffer());
                }
                Response.End();
            }
        }



        public void ExportToPdf(DataTable myDataTable)
        {
            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 10, 10, 10, 10);
            try
            {
                PdfWriter.GetInstance(pdfDoc, System.Web.HttpContext.Current.Response.OutputStream);
                pdfDoc.Open();
                Chunk c = new Chunk("" + System.Web.HttpContext.Current.Session["CompanyName"] + "", FontFactory.GetFont("Verdana", 11));
                Paragraph p = new Paragraph();
                p.Alignment = Element.ALIGN_CENTER;
                p.Add(c);
                pdfDoc.Add(p);
                
                Font font8 = FontFactory.GetFont("ARIAL", 10);
                DataTable dt = myDataTable;
                if (dt != null)
                {
                    //Craete instance of the pdf table and set the number of column in that table  
                    PdfPTable PdfTable = new PdfPTable(dt.Columns.Count);
                    PdfPCell PdfPCell = null;
                    foreach (DataColumn column in dt.Columns)
                    {
                        PdfPCell = new PdfPCell(new Phrase(new Chunk(column.ToString(), font8)));
                        PdfTable.AddCell(PdfPCell);
                    }
                    for (int rows = 0; rows < dt.Rows.Count; rows++)
                    {
                        for (int column = 0; column < dt.Columns.Count; column++)
                        {
                            PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[rows][column].ToString(), font8)));
                            PdfTable.AddCell(PdfPCell);
                        }
                    }
                    //PdfTable.SpacingBefore = 15f; // Give some space after the text or it may overlap the table            
                    pdfDoc.Add(PdfTable); // add pdf table to the document   
                }
                pdfDoc.Close();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename= SampleExport.pdf");
                System.Web.HttpContext.Current.Response.Write(pdfDoc);
                Response.Flush();
                Response.End();
                //HttpContext.Current.ApplicationInstance.CompleteRequest();  
            }
            catch (DocumentException de)
            {
                System.Web.HttpContext.Current.Response.Write(de.Message);
            }
            catch (IOException ioEx)
            {
                System.Web.HttpContext.Current.Response.Write(ioEx.Message);
            }
            catch (Exception ex)
            {
                System.Web.HttpContext.Current.Response.Write(ex.Message);
            }
        }

        public void ExportToCSV(DataTable dt)
        {
            Response.Clear();
            Response.Buffer = true;

            Response.AddHeader("content-disposition", "attachment;filename=FileName.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(DataTableToCSV(dt));
            Response.Flush();
            Response.End();
        }

        public string DataTableToCSV(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            if (dt == null)
                return "";

            try
            {
                // Create the header row
                for (int i = 0; i <= dt.Columns.Count - 1; i++)
                {
                    // Append column name in quotes
                    sb.Append("\"" + dt.Columns[i].ColumnName + "\"");
                    // Add carriage return and linefeed if last column, else add comma
                    sb.Append(i == dt.Columns.Count - 1 ? "\n" : ",");
                }


                foreach (DataRow row in dt.Rows)
                {
                    for (int i = 0; i <= dt.Columns.Count - 1; i++)
                    {
                        // Append value in quotes
                        //sb.Append("""" & row.Item(i) & """")

                        // OR only quote items that that are equivilant to strings
                        sb.Append(object.ReferenceEquals(dt.Columns[i].DataType, typeof(string)) || object.ReferenceEquals(dt.Columns[i].DataType, typeof(char)) ? "\"" + row[i] + "\"" : row[i]);

                        // Append CR+LF if last field, else add Comma
                        sb.Append(i == dt.Columns.Count - 1 ? "\n" : ",");
                    }
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                // Handle the exception however you want
                return "";
            }

        }

        private void ExportToWord(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                string filename = "DownloadReport.doc";
                System.IO.StringWriter tw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                DataGrid dgGrid = new DataGrid();
                dgGrid.DataSource = dt;
                dgGrid.DataBind();

                //Get the HTML for the control.
                dgGrid.RenderControl(hw);
                //Write the HTML back to the browser.
                Response.ContentType = "application/msword";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
                //this.EnableViewState = false;
                Response.Write(tw.ToString());
                Response.End();
            }
        }

        public ActionResult Delete (string PN)
        {
            try
            {
                string[] msg = new DynamicQuery().Delete(PN).Split('|');

                return Json(new { success = msg[0], messages = msg[1] }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { success = "true", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }

        public ActionResult GenerateFormDelete(string ProcesName)
        {
            DynamicQuery report = new DynamicQuery();
            List<DynamicQuery> Reports = report.GetParameterScr(ProcesName);
            return PartialView("_PartialScrDelete", Reports);
        }

        public ActionResult DeleteQuery(string Values)
        {
            try
            {
                if(getpE_UserId!=null)
                {
                    DynamicQuery DQ = new DynamicQuery();
                    List<DynamicQuery> Param = System.Web.Helpers.Json.Decode<List<DynamicQuery>>(Values);
                    DynamicQuery dq = new DynamicQuery();
                    dq.ColumnName = "USER_ID";
                    dq.Value = getpE_UserId.Username;
                    Param.Add(dq);
                    string msg = DQ.DeleteQuery(Param);
                    return Json(new { success = "true", messages = msg }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = "flase", messages = "session anda telah habis silahkan login ulang" }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {
                return Json(new { success = "flase", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GenerateFormAdd(string ProcesName)
        {
            DynamicQuery report = new DynamicQuery();
            List<DynamicQuery> Reports = report.GetParameterScr(ProcesName);
            return PartialView("_PartialScrAdd", Reports);
        }

        public ActionResult AddQuery(string Values)
        {
            try
            {
                DynamicQuery DQ = new DynamicQuery();
                List<DynamicQuery> Param = System.Web.Helpers.Json.Decode<List<DynamicQuery>>(Values);
                string msg = DQ.AddQuery(Param);
                return Json(new { success = "true", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "true", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GenerateFormUpdate(string ProcesName)
        {
            DynamicQuery report = new DynamicQuery();
            DynamicUpdate Reports = report.GetParameterScrUpdate(ProcesName);
            return PartialView("_PartialScrUpdate", Reports);
        }

        public ActionResult UpdateQuery(string Values)
        {
            try
            {
                DynamicQuery DQ = new DynamicQuery();
                List<DynamicQuery> Param = System.Web.Helpers.Json.Decode<List<DynamicQuery>>(Values);
                DynamicQuery dq = new DynamicQuery();
                dq.ColumnName = "USER_ID";
                dq.Value = getpE_UserId.Username;
                Param.Add(dq);
                string msg = DQ.UpdateQuery(Param);
                return Json(new { success = "true", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "true", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult AddDynamicQueryFrom(string Process_name, string Mode)
        {
            DynamicQueryMaster DQM = new DynamicQueryMaster();
            DynamicQuery DQ = new DynamicQuery();
            if (!string.IsNullOrEmpty(Process_name) && Mode == "Edit")
            {
                
                DQM = DQ.GetDataDynamicQueryMaster(Process_name);
            }
            mSystemMaster Master = new  mSystemMaster();
            List<mSystemMaster> CategoryKey = Master.GetSystemValueList("DYNAMIC_QUERY", "CategoryCode", "4", ";").ToList();
            List<mSystemMaster> Category = Master.GetComboBoxList("DYNAMIC_QUERY").ToList();
            var cat= (from a in CategoryKey
                      join b in Category on  a.SYSTEM_VALUE  equals  b.SYSTEM_CD 
                      select new { b.SYSTEM_CD, b.SYSTEM_TYPE, b.SYSTEM_VALUE }).ToList();
            ViewData["Category"] = cat;
            ViewData["ExtDownloadFile"] = Master.GetSystemValueList("DYNAMIC_QUERY", "Ext", "4", ";").ToList();
            List<string>Roles=new List<string>();
            Roles.Add("Select All");
            Roles.AddRange(DQ.GetAllRole());
            ViewData["Roles"] = Roles;
            return PartialView("_PartialScrDynamicQuery", DQM);
        }

        //[HttpPost]
        public ActionResult AddDynamicQueryMaster(string Process_name, string Category, string SQL_QUERY1, string EXT_DOWNLOAD_FILE, string PROCESS_DESC,string Roles)
        {
            DynamicQueryMaster DQM = new DynamicQueryMaster();
            try
            {
                DQM.PROCESS_NAME = Process_name;
                DQM.SQL_QUERY1 = SQL_QUERY1;
                DQM.CATEGORY = Category;
                DQM.EXT_DOWNLOAD_FILE = EXT_DOWNLOAD_FILE;
                DQM.PROCESS_DESC = PROCESS_DESC;
                DQM.ROLES = Roles;
                DynamicQuery DQ = new DynamicQuery();
                string Message = string.Empty;
                Message = DQ.SaveDynamicDocumentMaster(DQM);
                if (Message.ToLower().Contains("error"))
                {
                    return Json(new { success = "false", messages = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = "true", messages = Message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }            
        }

        public ActionResult UpdateDynamicQueryMaster(string Process_name, string Category, string SQL_QUERY1, string EXT_DOWNLOAD_FILE, string PROCESS_DESC,string Roles)
        {
            DynamicQueryMaster DQM = new DynamicQueryMaster();
            try
            {
                DQM.PROCESS_NAME = Process_name;
                DQM.SQL_QUERY1 = SQL_QUERY1;
                DQM.CATEGORY = Category;
                DQM.EXT_DOWNLOAD_FILE = EXT_DOWNLOAD_FILE;
                DQM.PROCESS_DESC = PROCESS_DESC;
                DQM.ROLES = Roles;
                DynamicQuery DQ = new DynamicQuery();
                string Message = string.Empty;
                Message = DQ.UpdateDynamicDocumentMaster(DQM);
                if (Message.ToLower().Contains("error"))
                {
                    return Json(new { success = "false", messages = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = "true", messages = Message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RoleBackDelete(string PID)
        {
            DynamicQuery DQ= new DynamicQuery();
            string msg = DQ.RolebackDelete(PID);
            var Message = msg.Split('|');
            if (Message[0].ToLower().Contains("false"))
            {
                return Json(new { success = "false", messages = Message[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = Message[1] }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RoleBackUpdate(string PID)
        {
            DynamicQuery DQ = new DynamicQuery();
            string msg = DQ.RolebackUpdate(PID);
            var Message = msg.Split('|');
            if (Message[0].ToLower().Contains("false"))
            {
                return Json(new { success = "false", messages = Message[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = Message[1] }, JsonRequestBehavior.AllowGet);
            }
        }
   }
}
