﻿using System.Collections.Generic;
using System.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class FileLogMonitoringController : PageController
    {

        mFileLogMonitoring FileLog = new mFileLogMonitoring();
        MessagesString msgError = new MessagesString();
        Log log = new Log();

        public string getpE_ProcessId
        {
            get
            {
                if (Session["getpE_ProcessId"] != null)
                {
                    return Session["getpE_ProcessId"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_ProcessId"] = value;
            }
        }

        public FileLogMonitoringController()
        {
            Settings.Title = "File Log Monitoring";
        }

        protected override void Startup()
        {
            ViewBag.MSPX00003ERR = msgError.getMSPX00003ERR("Order Date From and Order Date To");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("File Log Monitoring");
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            ViewBag.MSPX00009INB = msgError.getMSPX00009INB("");
            ViewBag.MSPX00010INB = msgError.getMSPX00010INB("");
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
                //result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));
            }

            return result;
        }

        public ActionResult FileLogMonitoringCallBack(string p_DATE_FROM, string p_DATE_TO)
        {
            List<mFileLogMonitoring> model = new List<mFileLogMonitoring>();

            p_DATE_FROM = reFormatDate(p_DATE_FROM);
            p_DATE_TO = reFormatDate(p_DATE_TO);

            {
                model = FileLog.getListFileLogMonitoring
                (p_DATE_FROM, p_DATE_TO);
            }
            return PartialView("FileLogMonitoringGrid", model);
        }

       
        public ActionResult _PopUpDetail(string pPROCESS_ID)
        {            
            List<mFileLogMonitoring> model = FileLog.getListFileLogMontoringDetail(pPROCESS_ID);
            ViewData["FileLogMonitoring"] = model;
            return PartialView("_PartialFileLogMonitoring");
        }

    }
}
