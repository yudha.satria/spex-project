﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class InvoiceController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mInvoice Invoice = new mInvoice();

        public InvoiceController()
        {
            Settings.Title = "Invoice";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            ViewData["PackingCompanyList"] = Invoice.GetPackingCompany();
            ViewBag.FORMD_PDCD = Invoice.GetSystemValue("FORMD_PARAM", "PDCODE_FORMD");
            ViewBag.GTOPAS_USAGE = Invoice.GetSystemValue("GENERAL", "GTOPAS_USAGE");
            ViewData["StatusList"] = (new Common()).GetStatusList();
            ViewData["TRANSPORTATIONs"] = (new Transportation()).GetAllTransportation();
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        //public ActionResult InvoiceCallBack(string p_INVOICE_NO, string p_INVOICE_NO_TO, string p_INVOICE_DT, string p_INVOICE_DT_TO,
        //    string p_PACKING_COMPANY, string p_SEND_SHIP_TMAP_STATUS, string p_SEND_INV_TMAP_STATUS, string p_SEND_INV_TAM_STATUS, string p_PEB_STATUS)
        //change agi 2017-09-11
            public ActionResult InvoiceCallBack(string p_INVOICE_NO, string p_INVOICE_NO_TO, string p_INVOICE_DT, string p_INVOICE_DT_TO,
            string p_PACKING_COMPANY, string p_SEND_SHIP_TMAP_STATUS, string p_SEND_INV_TMAP_STATUS, string p_SEND_INV_TAM_STATUS, string p_PEB_STATUS, string p_TRANSPORTATION_CD)

        {
            List<mInvoice> model = new List<mInvoice>();

            p_INVOICE_DT = p_INVOICE_DT == "01.01.0100" ? "" : reFormatDate(p_INVOICE_DT);
            p_INVOICE_DT_TO = p_INVOICE_DT_TO == "01.01.0100" ? "" : reFormatDate(p_INVOICE_DT_TO);
            {
                //model = Invoice.getListInvoice(p_INVOICE_NO, p_INVOICE_NO_TO, p_INVOICE_DT, p_INVOICE_DT_TO, p_PACKING_COMPANY,
                //    p_SEND_SHIP_TMAP_STATUS, p_SEND_INV_TMAP_STATUS, p_SEND_INV_TAM_STATUS, p_PEB_STATUS);
                //change agi 2017-09-11
                model = Invoice.getListInvoice(p_INVOICE_NO, p_INVOICE_NO_TO, p_INVOICE_DT, p_INVOICE_DT_TO, p_PACKING_COMPANY,
                  p_SEND_SHIP_TMAP_STATUS, p_SEND_INV_TMAP_STATUS, p_SEND_INV_TAM_STATUS, p_PEB_STATUS,p_TRANSPORTATION_CD);
            }
            return PartialView("InvoiceGrid", model);
        }

        public String PrintInvoice(string pInvoiceNos)
        {
            string result = "";
            string[] splitInvoice = pInvoiceNos.Split('|');
            for (int i = 0; i < splitInvoice.Length; i++)
            {
                string invoiceNo = splitInvoice[i];
                if (invoiceNo.Trim() != "")
                    result += PrintInvoiceHeader(invoiceNo) + "|" + PrintInvoiceDetail(invoiceNo) + "|";
            }
            return result.Remove(result.Length - 1, 1);
        }

        private string PrintInvoiceHeader(string pInvoiceNos)
        {
            string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            mTelerik report;

            String source = Server.MapPath("~/Report/Invoice.trdx");
            FileInfo fileInfo = new FileInfo(source);

            pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
            Directory.CreateDirectory(pdfFolderPath);

            DeleteOlderFile(pdfFolderPath);

            pdfFilename = pInvoiceNos + "_HEADER";
            if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                System.IO.File.Delete(pdfFolderPath + pdfFilename);

            report = new mTelerik(source, pdfFilename);
            report.AddParameters("INVOICE_NO", pInvoiceNos);
            report.SetConnectionString(Common.GetTelerikConnectionString());
            using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
            {
                byte[] info = report.GeneratePDF();
                fs.Write(info, 0, info.Length);
            }

            return report.ResultName;
        }

        private void DeleteOlderFile(string pdfFolderPath)
        {
            DeleteOlderHeaderFile(pdfFolderPath);
            DeleteOlderDetailFile(pdfFolderPath);
        }

        private static void DeleteOlderDetailFile(string pdfFolderPath)
        {
            IEnumerable<string> detailFiles = new List<string>();
            detailFiles = System.IO.Directory.EnumerateFiles(pdfFolderPath, "*_DETAIL.pdf");
            foreach (string detail in detailFiles)
            {
                FileInfo info = new FileInfo(detail);
                if (info.CreationTime < DateTime.Now.AddDays(-1))
                    System.IO.File.Delete(detail);
            }
        }

        private static void DeleteOlderHeaderFile(string pdfFolderPath)
        {
            IEnumerable<string> headerFiles = new List<string>();
            headerFiles = System.IO.Directory.EnumerateFiles(pdfFolderPath, "*_HEADER.pdf");
            foreach (string header in headerFiles)
            {
                FileInfo info = new FileInfo(header);
                if (info.CreationTime < DateTime.Now.AddDays(-1))
                    System.IO.File.Delete(header);
            }
        }

        public String PrintInvoiceDetail(string pInvoiceNos)
        {
            string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            mTelerik report;

            String source = Server.MapPath("~/Report/Invoice_Detail.trdx");
            FileInfo fileInfo = new FileInfo(source);

            pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
            Directory.CreateDirectory(pdfFolderPath);

            pdfFilename = pInvoiceNos + "_DETAIL";

            if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                System.IO.File.Delete(pdfFolderPath + pdfFilename);

            report = new mTelerik(source, pdfFilename);
            report.AddParameters("INVOICE_NO", pInvoiceNos);
            report.SetConnectionString(Common.GetTelerikConnectionString());
            using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
            {
                byte[] info = report.GeneratePDF();
                fs.Write(info, 0, info.Length);
            }

            return report.ResultName;
        }

        public ActionResult GetInvoiceDetailGrid(string I_Parameters)
        {
            List<mInvoice> model = Invoice.getListInvoiceDetail(I_Parameters);
            return PartialView("InvoiceDetailGrid", model);
        }

        public ActionResult DeleteData(string pINVOICE_NO)
        {
            string[] r = null;
            string msg = "";
            if (getpE_UserId!=null)
            { 
                //REMAKS AGI 2017-03-03
                //string resultMessage = Invoice.DeleteData(pINVOICE_NO);
                //ADD AGI 2017-03-03
                string resultMessage = Invoice.DeleteDataByUserId(pINVOICE_NO,getpE_UserId.Username);
                r = resultMessage.Split('|');
                if (r[0].ToString().ToLower().Contains("error"))
                    return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "false", messages = "session anda telah habis silahkan login ulang" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SendLockCheck(string pFUNCTION_ID)
        {
            string[] r = null;
            string msg = "";

            string resultMessage = Invoice.SendLockCheck(pFUNCTION_ID);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendValidation(string pFUNCTION_ID, string pINVOICE_NO)
        {
            string[] r = null;
            string msg = "";

            string resultMessage = Invoice.SendValidation(pFUNCTION_ID, pINVOICE_NO);
            r = resultMessage.Split('|');
            return Json(new { status = r[0].ToString().ToLower(), messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PEBValidation(string pINVOICE_NO)
        {
            string[] r = null;
            string msg = "";

            string resultMessage = Invoice.PEBValidation(pINVOICE_NO);
            r = resultMessage.Split('|');
            if (r[0] == "error")

                r[1] = "0|" + msgError.getMSG_SpexHref("MSPXINV035ER", "<a href='#' onclick='MessageShow(" + r[2] + "); return false;'>" + r[2] + "</a>").Split('|')[2] + "|" + r[2].ToString();

            return Json(new { status = r[0].ToString().ToLower(), messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendInvoiceTMAP(string pINVOICE_NO)
        {
            string[] r = null;
            string msg = "";

            string resultMessage = Invoice.SendInvoiceTMAP(pINVOICE_NO, getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = r[1];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00036INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SendShipmentTMAP(string pINVOICE_NO)
        {
            string[] r = null;
            string msg = "";

            string resultMessage = Invoice.SendShipmentTMAP(pINVOICE_NO, getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = r[1];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00037INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendInvoiceTAM()
        {
            string[] r = null;
            string msg = "";

            string resultMessage = Invoice.SendDummyInvoiceTAM(getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreatePEB(string pINVOICE_NO)
        {
            string[] r = null;
            string msg = "";

            string resultMessage = Invoice.CreatePEB(pINVOICE_NO, getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateFormD(string pINVOICE_NO)
        {
            try
            {
                if (getpE_UserId == null)
                    return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

                string[] r = null;
                string msg = "";
                string success = "true";
                bool isLock = false;
                bool isSuccess = true;
                bool isWarning = false;
                string pID = "";
                string invoice_no_success = "";
                string[] InvoiceNos = pINVOICE_NO.Split(',');
                string param_msg = "";
                foreach (string InvoiceNo in InvoiceNos)
                {
                    if (InvoiceNo.Trim() == "")
                        continue;

                    string resultMessage = Invoice.CreateFormD(InvoiceNo, getpE_UserId.Username);

                    r = resultMessage.Split('|');


                    if (r[0].ToString().ToLower().Contains("exception"))
                    {
                        param_msg += "Process Invoice No [" + InvoiceNo + "] end with exception,";
                        isSuccess = false;
                    }
                    else if (r[0].ToString().ToLower().Contains("error"))
                    {
                        pID = r[2].ToString();
                        param_msg += " [<a href='#' onclick='OnHyperLinkClick(" + pID + "); return false;'>" + pID + "</a>] error,";
                        isSuccess = false;
                    }
                    else if (r[0].ToString().ToLower().Contains("lock"))
                    {
                        msg = r[1];
                        isLock = true;
                        break;
                    }
                    else if (r[0].ToString().ToLower().Contains("warning"))
                    {
                        pID = r[2].ToString();
                        param_msg += " [<a href='#' onclick='OnHyperLinkClick(" + pID + "); return false;'>" + pID + "</a>] warning,";
                        isWarning = true;
                    }
                    else
                    {
                        invoice_no_success += CreateFormDFile(InvoiceNo) + "|";
                        pID = r[2].ToString();
                        param_msg += " [<a href='#' onclick='OnHyperLinkClick(" + pID + "); return false;'>" + pID + "</a>] success,";
                    }
                }

                if (!isLock)
                {
                    if (!isSuccess)
                        success = "false";

                    if (param_msg != "")
                        param_msg = param_msg.Substring(param_msg.Length - 1, 1) == "," ? param_msg.Remove(param_msg.Length - 1, 1) : param_msg;

                    msg = msgError.getMSPX00139ERR(param_msg);
                }
                else
                    success = "false";

                if (invoice_no_success != "")
                    invoice_no_success = invoice_no_success.Substring(invoice_no_success.Length - 1, 1) == "|" ? invoice_no_success.Remove(invoice_no_success.Length - 1, 1) : invoice_no_success;


                return Json(new { success = success, messages = msg, process_id = pID, InvoiceNos = invoice_no_success }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Lock L = new Lock();
                L.UnlockFunction("F15-013");
                return Json(new { success = "false", messages = ex.Message, process_id = "", InvoiceNos = "" }, JsonRequestBehavior.AllowGet);
            }

        }

        public string GetFormDFiles(string pINVOICE_NO)
        {
            string ResultPaths = "";
            string[] InvoiceNos = pINVOICE_NO.Split(',');
            foreach (string InvoiceNo in InvoiceNos)
            {
                if (InvoiceNo == "")
                    continue;

                string fileName = "FormD_" + InvoiceNo + "_" + getpE_UserId.Username + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                string resultPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
                List<mInvoiceFormD> formDList = Invoice.GetFormD(InvoiceNo, getpE_UserId.Username);

                StreamWriter writer = new StreamWriter(resultPath + fileName);

                string csvText = "";
                foreach (mInvoiceFormD formD in formDList)
                {
                    csvText = string.Format(
                        "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31}",
                        formD.FIELD_A, formD.FIELD_B, formD.FIELD_C, formD.FIELD_D, formD.FIELD_E,
                        formD.FIELD_F, formD.FIELD_G, formD.FIELD_H, formD.FIELD_I, formD.FIELD_J,
                        formD.FIELD_K, formD.FIELD_L, formD.FIELD_M, formD.FIELD_N, formD.FIELD_O,
                        formD.FIELD_P, formD.FIELD_Q, formD.FIELD_R, formD.FIELD_S, formD.FIELD_T,
                        formD.FIELD_U, formD.FIELD_V, formD.FIELD_W, formD.FIELD_X, formD.FIELD_Y,
                        formD.FIELD_Z, formD.FIELD_AA, formD.FIELD_AB, formD.FIELD_AC, formD.FIELD_AD,
                        formD.FIELD_AE, formD.FIELD_AF
                    );
                    writer.WriteLine(csvText);
                }

                ResultPaths += fileName + "|";
                writer.Dispose();
            }

            return ResultPaths.Remove(ResultPaths.Length - 1, 1);
        }

        public string CreateFormDFile(string InvoiceNo)
        {
            string fileName = "FormD_" + InvoiceNo + ".csv";
            string resultPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
            List<mInvoiceFormD> formDList = Invoice.GetFormD(InvoiceNo, getpE_UserId.Username);

            if (System.IO.File.Exists(resultPath + fileName))
                System.IO.File.Delete(resultPath + fileName);

            StreamWriter writer = new StreamWriter(resultPath + fileName);

            string csvText = "";
            foreach (mInvoiceFormD formD in formDList)
            {
                csvText = string.Format(
                    "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31}",
                    formD.FIELD_A, formD.FIELD_B, formD.FIELD_C, formD.FIELD_D, formD.FIELD_E,
                    formD.FIELD_F, formD.FIELD_G, formD.FIELD_H, formD.FIELD_I, formD.FIELD_J,
                    formD.FIELD_K, formD.FIELD_L, formD.FIELD_M, formD.FIELD_N, formD.FIELD_O,
                    formD.FIELD_P, formD.FIELD_Q, formD.FIELD_R, formD.FIELD_S, formD.FIELD_T,
                    formD.FIELD_U, formD.FIELD_V, formD.FIELD_W, formD.FIELD_X, formD.FIELD_Y,
                    formD.FIELD_Z, formD.FIELD_AA, formD.FIELD_AB, formD.FIELD_AC, formD.FIELD_AD,
                    formD.FIELD_AE, formD.FIELD_AF
                );
                writer.WriteLine(csvText);
            }

            writer.Dispose();

            return fileName;
        }

        public void CreateFinanceReport(string Data)
        {
            mInvoice Model = System.Web.Helpers.Json.Decode<mInvoice>(Data);
            Model.P_INVOICE_DT = Model.P_INVOICE_DT == "01.01.0100" ? "" : reFormatDate(Model.P_INVOICE_DT);
            Model.INVOICE_DT_TO = Model.INVOICE_DT_TO == "01.01.0100" ? "" : reFormatDate(Model.INVOICE_DT_TO);

            //List<mInvoice> inv = Model.getListInvoice(Model.INVOICE_NO, Model.INVOICE_NO_TO, Model.P_INVOICE_DT, Model.INVOICE_DT_TO, Model.PACKING_COMPANY,
            //     Model.SEND_SHIP_TMAP_STATUS, Model.SEND_INV_TMAP_STATUS, Model.SEND_INV_TAM_STATUS, Model.PEB_STATUS);
            //change agi 2017-09-11
            List<mInvoice> inv = Model.getListInvoice(Model.INVOICE_NO, Model.INVOICE_NO_TO, Model.P_INVOICE_DT, Model.INVOICE_DT_TO, Model.PACKING_COMPANY,
                 Model.SEND_SHIP_TMAP_STATUS, Model.SEND_INV_TMAP_STATUS, Model.SEND_INV_TAM_STATUS, Model.PEB_STATUS, Model.TRANSPORTATION_CD);
            GenerateFinanceReport(inv);
        }

        public void GenerateFinanceReport(List<mInvoice> INVs)
        {
            //SP
            FINANCE_REPORT FR = new FINANCE_REPORT();
            List<FINANCE_REPORT> FRs = FR.CreateFinanceReport(FR.ConvertListToDatatebleInvoice(INVs));

            //EXCEL CONFIG
            string filename = string.Empty;
            string filesTmp = HttpContext.Request.MapPath("~/Template/Finance_Report.xlsx");

            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            IDataFormat format = workbook.CreateDataFormat();

            ISheet sheetMain = workbook.GetSheet("FR");
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            filename = "FINANCE_REPORT" + date + "_" + getpE_UserId.Username + ".xlsx";

            //ADD ROW
            GenerateExcelFinance_Report(FRs, sheetMain, workbook);
            //SAVE XLSX
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            var att = String.Format("attachment;filename={0}", filename);
            Response.AddHeader("content-disposition", att);

        }

        public static void GenerateExcelFinance_Report(List<FINANCE_REPORT> FRs, ISheet sheetMain, XSSFWorkbook workbook)
        {
            int row = 5;
            //style table
            IRow Hrow;

            ICellStyle StyleBorderDouble = workbook.CreateCellStyle();
            StyleBorderDouble.BorderLeft = BorderStyle.Double;
            StyleBorderDouble.BorderBottom = BorderStyle.Double;
            StyleBorderDouble.BorderRight = BorderStyle.Double;
            StyleBorderDouble.BorderTop = BorderStyle.Double;
            StyleBorderDouble.Alignment = HorizontalAlignment.Center;

            ICellStyle StyleBorderDouble1 = workbook.CreateCellStyle();
            StyleBorderDouble1.BorderLeft = BorderStyle.Thin;
            StyleBorderDouble1.BorderBottom = BorderStyle.Hair;
            StyleBorderDouble1.BorderRight = BorderStyle.Thin;
            StyleBorderDouble1.Alignment = HorizontalAlignment.Center;

            ICellStyle StyleBorderDouble1Left = workbook.CreateCellStyle();
            StyleBorderDouble1Left.BorderLeft = BorderStyle.Thin;
            StyleBorderDouble1Left.BorderBottom = BorderStyle.Hair;
            StyleBorderDouble1Left.BorderRight = BorderStyle.Thin;
            StyleBorderDouble1Left.Alignment = HorizontalAlignment.Left;

            ICellStyle StyleBorderDouble1Right = workbook.CreateCellStyle();
            StyleBorderDouble1Right.BorderLeft = BorderStyle.Thin;
            StyleBorderDouble1Right.BorderBottom = BorderStyle.Hair;
            StyleBorderDouble1Right.BorderRight = BorderStyle.Thin;
            StyleBorderDouble1Right.Alignment = HorizontalAlignment.Right;

            ICellStyle StyleBorderDouble2 = workbook.CreateCellStyle();
            StyleBorderDouble2.BorderLeft = BorderStyle.Double;
            StyleBorderDouble2.BorderBottom = BorderStyle.Hair;
            StyleBorderDouble2.BorderRight = BorderStyle.Double;
            StyleBorderDouble2.Alignment = HorizontalAlignment.Center;

            IDataFormat dataFormatCustom = workbook.CreateDataFormat();

            //region insert table
            foreach (FINANCE_REPORT FR in FRs)
            {
                Hrow = sheetMain.CreateRow(row);
                Hrow.CreateCell(0).SetCellValue(FR.ID);
                Hrow.GetCell(0).CellStyle = StyleBorderDouble1;

                Hrow.CreateCell(1).SetCellValue(FR.INVOICE_NO);
                Hrow.GetCell(1).CellStyle = StyleBorderDouble1Left;

                Hrow.CreateCell(2).SetCellValue(FR.VANNING_NO);
                Hrow.GetCell(2).CellStyle = StyleBorderDouble1Left;

                Hrow.CreateCell(3).SetCellValue(FR.INVOICE_DT);
                Hrow.GetCell(3).CellStyle = StyleBorderDouble1;

                Hrow.CreateCell(4).SetCellValue(FR.INVOICE_TO);
                Hrow.GetCell(4).CellStyle = StyleBorderDouble1;

                Hrow.CreateCell(5).SetCellValue(FR.MARKS);
                Hrow.GetCell(5).CellStyle = StyleBorderDouble1;

                Hrow.CreateCell(6).SetCellValue(FR.ETD);
                Hrow.GetCell(6).CellStyle = StyleBorderDouble1;

                Hrow.CreateCell(7).SetCellValue(FR.TAX_NO);
                Hrow.GetCell(7).CellStyle = StyleBorderDouble1;

                Hrow.CreateCell(8).SetCellValue(FR.LOT_CD);
                Hrow.GetCell(8).CellStyle = StyleBorderDouble1;

                Hrow.CreateCell(9).SetCellValue(FR.QTY);
                Hrow.GetCell(9).CellStyle = StyleBorderDouble1;

                Hrow.CreateCell(10).SetCellValue(FR.UOM);
                Hrow.GetCell(10).CellStyle = StyleBorderDouble1;

                Hrow.CreateCell(11).SetCellValue(FR.AMOUNT);
                Hrow.GetCell(11).CellStyle = StyleBorderDouble1Right;
                Hrow.GetCell(11).CellStyle.DataFormat = dataFormatCustom.GetFormat("#,##0.00");


                Hrow.CreateCell(12).SetCellValue(FR.CURRENCY);
                Hrow.GetCell(12).CellStyle = StyleBorderDouble1;

                Hrow.CreateCell(13).SetCellValue(FR.METHD);
                Hrow.GetCell(13).CellStyle = StyleBorderDouble1;

                //add agi 2017-09-05
                Hrow.CreateCell(14).SetCellValue(FR.GROSS_WEIGHT);
                Hrow.GetCell(14).CellStyle = StyleBorderDouble1Right;
                Hrow.GetCell(11).CellStyle.DataFormat = dataFormatCustom.GetFormat("#,##0.0");

                Hrow.CreateCell(15).SetCellValue(FR.NET_WEIGHT);
                Hrow.GetCell(15).CellStyle = StyleBorderDouble1Right;
                Hrow.GetCell(11).CellStyle.DataFormat = dataFormatCustom.GetFormat("#,##0.0");

                Hrow.CreateCell(16).SetCellValue(FR.MEASUREMENT);
                Hrow.GetCell(16).CellStyle = StyleBorderDouble1Right;
                Hrow.GetCell(11).CellStyle.DataFormat = dataFormatCustom.GetFormat("#,##0.000");

                row++;
            }

            row += 3;
            //style footer
            ICellStyle StyleBordersingle = workbook.CreateCellStyle();
            StyleBordersingle.BorderLeft = BorderStyle.Thin;
            StyleBordersingle.BorderBottom = BorderStyle.Thin;
            StyleBordersingle.BorderRight = BorderStyle.Thin;
            StyleBordersingle.BorderTop = BorderStyle.Thin;
            StyleBordersingle.Alignment = HorizontalAlignment.Center;
            FINANCE_REPORT FR_footer = new FINANCE_REPORT();
            FR_footer = FRs.First();
            //region  footer
            Hrow = sheetMain.CreateRow(row);
            Hrow.CreateCell(5).SetCellValue(FR_footer.CITY + ", " + FR_footer.CURRDATE);

            row++;
            Hrow = sheetMain.CreateRow(row);
            //column header RECIEVE_BY
            sheetMain.AddMergedRegion(new CellRangeAddress(
                row, //first row (0-based)
                row, //last row  (0-based)
                1, //first column (0-based)
                2  //last column  (0-based)
            ));
            Hrow.CreateCell(1).SetCellValue(FR_footer.HEADER_RECIEVE_BY);
            Hrow.CreateCell(2).SetCellType(CellType.Blank);
            Hrow.GetCell(1).CellStyle = StyleBordersingle;
            Hrow.GetCell(2).CellStyle = StyleBordersingle;

            //column HEADER_CHECKED
            sheetMain.AddMergedRegion(new CellRangeAddress(
                row, //first row (0-based)
                row, //last row  (0-based)
                5, //first column (0-based)
                6  //last column  (0-based)
            ));

            Hrow.CreateCell(5).SetCellValue(FR_footer.HEADER_CHECKED);
            Hrow.CreateCell(6).SetCellType(CellType.Blank);
            Hrow.GetCell(5).CellStyle = StyleBordersingle;
            Hrow.GetCell(6).CellStyle = StyleBordersingle;

            //column HEADER_PREFARED
            sheetMain.AddMergedRegion(new CellRangeAddress(
                row, //first row (0-based)
                row, //last row  (0-based)
                7, //first column (0-based)
                8  //last column  (0-based)
            ));

            Hrow.CreateCell(7).SetCellValue(FR_footer.HEADER_PREPARED);
            Hrow.CreateCell(8).SetCellType(CellType.Blank);
            Hrow.GetCell(7).CellStyle = StyleBordersingle;
            Hrow.GetCell(8).CellStyle = StyleBordersingle;

            row++;
            Hrow = sheetMain.CreateRow(row);
            //border RECIEVE_BY blank/ttd row 1
            sheetMain.AddMergedRegion(new CellRangeAddress(
                row, //first row (0-based)
                row + 3, //last row  (0-based)
                1, //first column (0-based)
                2  //last column  (0-based)
            ));
            Hrow.CreateCell(1).SetCellType(CellType.Blank);
            Hrow.CreateCell(2).SetCellType(CellType.Blank);
            Hrow.GetCell(1).CellStyle = StyleBordersingle;
            Hrow.GetCell(2).CellStyle = StyleBordersingle;


            //border HEADER_CHECKED row 1
            sheetMain.AddMergedRegion(new CellRangeAddress(
                row, //first row (0-based)
                row + 3, //last row  (0-based)
                5, //first column (0-based)
                6  //last column  (0-based)
            ));

            Hrow.CreateCell(5).SetCellType(CellType.Blank);
            Hrow.CreateCell(6).SetCellType(CellType.Blank);
            Hrow.GetCell(5).CellStyle = StyleBordersingle;
            Hrow.GetCell(6).CellStyle = StyleBordersingle;

            //border  HEADER_PREFARED ttd/blank row 1
            sheetMain.AddMergedRegion(new CellRangeAddress(
                row, //first row (0-based)
                row + 3, //last row  (0-based)
                7, //first column (0-based)
                8  //last column  (0-based)
            ));
            Hrow.CreateCell(7).SetCellType(CellType.Blank);
            Hrow.CreateCell(8).SetCellType(CellType.Blank);
            Hrow.GetCell(7).CellStyle = StyleBordersingle;
            Hrow.GetCell(8).CellStyle = StyleBordersingle;

            row++;
            Hrow = sheetMain.CreateRow(row);
            //border RECIEVE_BY blank/ttd row 2
            Hrow.CreateCell(1).SetCellType(CellType.Blank);
            Hrow.CreateCell(2).SetCellType(CellType.Blank);
            Hrow.GetCell(1).CellStyle = StyleBordersingle;
            Hrow.GetCell(2).CellStyle = StyleBordersingle;
            //border HEADER_CHECKED row 2
            Hrow.CreateCell(5).SetCellType(CellType.Blank);
            Hrow.CreateCell(6).SetCellType(CellType.Blank);
            Hrow.GetCell(5).CellStyle = StyleBordersingle;
            Hrow.GetCell(6).CellStyle = StyleBordersingle;
            //border  HEADER_PREFARED ttd/blank row 1
            Hrow.CreateCell(7).SetCellType(CellType.Blank);
            Hrow.CreateCell(8).SetCellType(CellType.Blank);
            Hrow.GetCell(7).CellStyle = StyleBordersingle;
            Hrow.GetCell(8).CellStyle = StyleBordersingle;

            row++;
            Hrow = sheetMain.CreateRow(row);
            //border RECIEVE_BY blank/ttd row 3
            Hrow.CreateCell(1).SetCellType(CellType.Blank);
            Hrow.CreateCell(2).SetCellType(CellType.Blank);
            Hrow.GetCell(1).CellStyle = StyleBordersingle;
            Hrow.GetCell(2).CellStyle = StyleBordersingle;
            //border HEADER_CHECKED row 3
            Hrow.CreateCell(5).SetCellType(CellType.Blank);
            Hrow.CreateCell(6).SetCellType(CellType.Blank);
            Hrow.GetCell(5).CellStyle = StyleBordersingle;
            Hrow.GetCell(6).CellStyle = StyleBordersingle;
            //border  HEADER_PREFARED ttd/blank row 3
            Hrow.CreateCell(7).SetCellType(CellType.Blank);
            Hrow.CreateCell(8).SetCellType(CellType.Blank);
            Hrow.GetCell(7).CellStyle = StyleBordersingle;
            Hrow.GetCell(8).CellStyle = StyleBordersingle;


            row++;
            Hrow = sheetMain.CreateRow(row);
            //border RECIEVE_BY blank/ttd row 4
            Hrow.CreateCell(1).SetCellType(CellType.Blank);
            Hrow.CreateCell(2).SetCellType(CellType.Blank);
            Hrow.GetCell(1).CellStyle = StyleBordersingle;
            Hrow.GetCell(2).CellStyle = StyleBordersingle;
            //border HEADER_CHECKED row 4
            Hrow.CreateCell(5).SetCellType(CellType.Blank);
            Hrow.CreateCell(6).SetCellType(CellType.Blank);
            Hrow.GetCell(5).CellStyle = StyleBordersingle;
            Hrow.GetCell(6).CellStyle = StyleBordersingle;
            //border  HEADER_PREFARED ttd/blank row 4
            Hrow.CreateCell(7).SetCellType(CellType.Blank);
            Hrow.CreateCell(8).SetCellType(CellType.Blank);
            Hrow.GetCell(7).CellStyle = StyleBordersingle;
            Hrow.GetCell(8).CellStyle = StyleBordersingle;

            row++;
            Hrow = sheetMain.CreateRow(row);

            //isi kolom/nama ttd
            //column RECIEVE_BY
            sheetMain.AddMergedRegion(new CellRangeAddress(
                row, //first row (0-based)
                row, //last row  (0-based)
                1, //first column (0-based)
                2  //last column  (0-based)
            ));
            Hrow.CreateCell(1).SetCellValue(FR_footer.RECIEVE_BY);
            Hrow.CreateCell(2).SetCellType(CellType.Blank);
            Hrow.GetCell(1).CellStyle = StyleBordersingle;
            Hrow.GetCell(2).CellStyle = StyleBordersingle;

            //column CHECKED
            sheetMain.AddMergedRegion(new CellRangeAddress(
                row, //first row (0-based)
                row, //last row  (0-based)
                5, //first column (0-based)
                6  //last column  (0-based)
            ));

            Hrow.CreateCell(5).SetCellValue(FR_footer.CHECKED);
            Hrow.CreateCell(6).SetCellType(CellType.Blank);
            Hrow.GetCell(5).CellStyle = StyleBordersingle;
            Hrow.GetCell(6).CellStyle = StyleBordersingle;

            //column PREFARED
            sheetMain.AddMergedRegion(new CellRangeAddress(
                row, //first row (0-based)
                row, //last row  (0-based)
                7, //first column (0-based)
                8  //last column  (0-based)
            ));

            Hrow.CreateCell(7).SetCellValue(FR_footer.PREPARED);
            Hrow.CreateCell(8).SetCellType(CellType.Blank);
            Hrow.GetCell(7).CellStyle = StyleBordersingle;
            Hrow.GetCell(8).CellStyle = StyleBordersingle;
        }

        public ActionResult SaveSegment(string pINVOICE_NO, string pREGISTER_NO, string pREGISTER_DT, string pVOYAGE_NO, string pDEST_PORT_CD, string pIMPORTER_CD, string pINSPECTION_DT, string pINV_CONTAINER_COUNT, string pETD, string pPACKING_COMPANY)
        {
            string[] r = null;
            string resultMessage = Invoice.SavePEBSegmentData(pINVOICE_NO, pREGISTER_NO, GetDate(pREGISTER_DT), GetDate(pINSPECTION_DT), pINV_CONTAINER_COUNT, GetDate(pETD), pPACKING_COMPANY, getpE_UserId.Username);

            string msg = "";
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = r[1];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = r[1]; ;
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult CancelSegment(string pREGISTER_NO)
        {
            string[] r = null;
            string resultMessage = Invoice.CancelPEBSegmentData(pREGISTER_NO);

            return Json(new { success = "true", messages = "success" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PopUpSegment(string p_InvoiceNo)
        {
            mPEB model = Invoice.GetPEBSegment(p_InvoiceNo);

            List<mPEB> listDat = Invoice.GetListPEBContainer(p_InvoiceNo);


            ViewData["ContainerList"] = listDat;
            ViewData["ContainerIndex"] = listDat.Count;

            return PartialView("~/Views/Invoice/PEBSegment.cshtml", model);
        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            ar = dt.Split('.');
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;
            success = int.TryParse(ar[0].ToString(), out day);
            success = int.TryParse(ar[1].ToString(), out month);
            success = int.TryParse(ar[2].ToString(), out year);

            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        public ActionResult CreateFormDProd(string pINVOICE_NO)
        {
            try
            {
                if (getpE_UserId == null)
                    return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

                string[] r = null;
                string msg = "";
                string success = "true";
                bool isLock = false;
                bool isSuccess = true;
                bool isWarning = false;
                string pID = "";
                string invoice_no_success = "";
                string[] InvoiceNos = pINVOICE_NO.Split(',');
                string param_msg = "";
                foreach (string InvoiceNo in InvoiceNos)
                {
                    if (InvoiceNo.Trim() == "")
                        continue;

                    string resultMessage = Invoice.CreateFormDProd(InvoiceNo, getpE_UserId.Username);

                    r = resultMessage.Split('|');


                    if (r[0].ToString().ToLower().Contains("exception"))
                    {
                        param_msg += "Process Invoice No [" + InvoiceNo + "] end with exception,";
                        isSuccess = false;
                    }
                    else if (r[0].ToString().ToLower().Contains("error"))
                    {
                        pID = r[2].ToString();
                        param_msg += " [<a href='#' onclick='OnHyperLinkClick(" + pID + "); return false;'>" + pID + "</a>] error,";
                        isSuccess = false;
                    }
                    else if (r[0].ToString().ToLower().Contains("lock"))
                    {
                        msg = r[1];
                        isLock = true;
                        break;
                    }
                    else if (r[0].ToString().ToLower().Contains("warning"))
                    {
                        pID = r[2].ToString();
                        param_msg += " [<a href='#' onclick='OnHyperLinkClick(" + pID + "); return false;'>" + pID + "</a>] warning,";
                        isWarning = true;
                    }
                    else
                    {
                        invoice_no_success += CreateFormDFile(InvoiceNo) + "|";
                        pID = r[2].ToString();
                        param_msg += " [<a href='#' onclick='OnHyperLinkClick(" + pID + "); return false;'>" + pID + "</a>] success,";
                    }
                }

                if (!isLock)
                {
                    if (!isSuccess)
                        success = "false";

                    if (param_msg != "")
                        param_msg = param_msg.Substring(param_msg.Length - 1, 1) == "," ? param_msg.Remove(param_msg.Length - 1, 1) : param_msg;

                    msg = msgError.getMSPX00139ERR(param_msg);
                }
                else
                    success = "false";

                if (invoice_no_success != "")
                    invoice_no_success = invoice_no_success.Substring(invoice_no_success.Length - 1, 1) == "|" ? invoice_no_success.Remove(invoice_no_success.Length - 1, 1) : invoice_no_success;


                return Json(new { success = success, messages = msg, process_id = pID, InvoiceNos = invoice_no_success }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Lock L = new Lock();
                L.UnlockFunction("F15-013");
                return Json(new { success = "false", messages = ex.Message, process_id = "", InvoiceNos = "" }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult PopUpInvoiceByAirEdit(string p_InvoiceNo)
        {
            mInvoice model = Invoice.getInvoiceByAirEdit(p_InvoiceNo, getpE_UserId.Username);
            ViewData["CASE_DATA"] = Invoice.getCaseListByAir(p_InvoiceNo, model.PACKING_COMPANY, getpE_UserId.Username);
            return PartialView("~/Views/Invoice/InvoiceByAirEdit.cshtml", model);
        }

        public ActionResult PopUpCaseInvoiceByAirEdit(string p_INVOICE_NO, string p_CASE_NO, string p_BUYER_PD, string p_PACKING_COMPANY)
        {
            mInvoice model = Invoice.GetCaseByAir(p_INVOICE_NO, p_CASE_NO, p_PACKING_COMPANY, getpE_UserId.Username);

            return PartialView("~/Views/Invoice/InvoiceByAirCaseEdit.cshtml", model);
        }

        public ActionResult GetCaseInvoiceByAirGrid(string p_INVOICE_NO, string p_PACKING_COMPANY)
        {
            List<mInvoice> model = Invoice.getCaseListByAir(p_INVOICE_NO, p_PACKING_COMPANY, getpE_UserId.Username);
            return PartialView("InvoiceByAirCaseGrid", model);
        }

        public ActionResult SubmitEditInvoiceByAir(string p_INVOICE_NO, string p_NET_WEIGHT, string p_GROSS_WEIGHT, string p_MEASUREMENT, string p_FORWARDING_AGENT, string p_DHL_AWB_NO)
        {
            if (getpE_UserId != null)
            {
                string UserID = getpE_UserId.Username;
                string resultMessage = "";

                resultMessage = Invoice.SaveInvoiceByAirEdit(p_INVOICE_NO, GetDecimal(p_NET_WEIGHT), GetDecimal(p_GROSS_WEIGHT), GetDecimal(p_MEASUREMENT), p_FORWARDING_AGENT, p_DHL_AWB_NO, UserID);

                string[] r = null;
                r = resultMessage.Split('|');

                if (r[0].ToString().ToLower().Contains("success"))
                    return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);
        }

        private decimal GetDecimal(string strValue)
        {
            decimal returnValue = 0;
            returnValue = Convert.ToDecimal(strValue);
            return returnValue;
        }

        public ActionResult SaveEditCaseInvoiceByAir(string p_INVOICE_NO, string p_PACKING_COMPANY, string p_CASE_NO, string p_CASE_NET_WEIGHT, string p_CASE_GROSS_WEIGHT, string p_CASE_LENGTH, string p_CASE_WIDTH, string p_CASE_HEIGHT)
        {
            if (getpE_UserId != null)
            {
                string UserID = getpE_UserId.Username;
                string resultMessage = "";

                mInvoice caseData = Invoice.GetCaseByAir(p_INVOICE_NO, p_CASE_NO, p_PACKING_COMPANY, UserID);

                resultMessage = Invoice.UpdateCaseByAir(p_INVOICE_NO, p_PACKING_COMPANY, p_CASE_NO, GetDecimal(p_CASE_GROSS_WEIGHT),
                    GetDecimal(p_CASE_NET_WEIGHT), GetDecimal(p_CASE_LENGTH), GetDecimal(p_CASE_WIDTH), GetDecimal(p_CASE_HEIGHT), UserID);

                string[] r = null;
                r = resultMessage.Split('|');
                var header = Invoice.getTempHeaderInvoiceByAir(p_INVOICE_NO, p_PACKING_COMPANY, UserID);

                if (r[0].ToString().ToLower().Contains("success"))
                    return Json(new { success = "true", messages = r[1], measurement = header.MEASUREMENT }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNewFAByAir(string p_INVOICE_NO, string p_PACKING_COMPANY, string p_GROSS_WEIGHT)
        {
            if (getpE_UserId != null)
            {
                string UserID = getpE_UserId.Username;
                string result = "";

                result = Invoice.GetNewFAByAir(p_INVOICE_NO, p_PACKING_COMPANY, GetDecimal(p_GROSS_WEIGHT), UserID);

                return Json(new { success = "true", messages = "success", forwarding_agent = result }, JsonRequestBehavior.AllowGet);

            }
            else
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);
        }
    }    
}
