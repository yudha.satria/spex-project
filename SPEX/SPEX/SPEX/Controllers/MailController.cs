﻿/**
 * Created By   : ark.yudha
 * Created Dt   : 12.02.2014
 * **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ActionMailer.Net.Mvc;
using SPEX.Models.MailNotification;
using SPEX.Models.Log;

namespace SPEX.Controllers
{
    public class MailController : MailerBase
    {

        public ActionResult Index()
        {
            return View();
        }

        public EmailResult sendNotificationMail(string p_ProcessName,long p_Process_ID, string p_Mail_Group_CD)
        {
            string trStart="<tr>";
            string tdStart = "<td>";
            string tdStartCenter = "<td align='center'>";
            string tdEnd = "</td>";
            string trEnd = "</tr>";
            
            string listLog="";

            IList<MailAddressForNotification> lForTOMail = MailNotificationProvider.Instance.getMailAddressForNotification(p_Mail_Group_CD, "N");
            foreach (MailAddressForNotification mTO in lForTOMail) {
                To.Add(mTO.MAIL_ADDRESS);
            }

            IList<MailAddressForNotification> lForCCMail = MailNotificationProvider.Instance.getMailAddressForNotification(p_Mail_Group_CD, "Y");
            foreach (MailAddressForNotification mCC in lForCCMail)
            {
                CC.Add(mCC.MAIL_ADDRESS);
            }

            From = "noreply@toyota.co.id";
            Subject = "[SPEX] Error notification";

            IList<LogForNotification> lLogForNotification = LogProvider.Instance.getLogForNotification(p_Process_ID, p_Mail_Group_CD);
            
            foreach(LogForNotification m in lLogForNotification){
                listLog += trStart;
                listLog += tdStartCenter;
                listLog += m.CREATED_DATE.ToString();
                listLog += tdEnd;

                listLog += tdStartCenter;
                listLog += m.MSG_ID;
                listLog += tdEnd;

                listLog += tdStartCenter;
                listLog += m.MSG_TYPE;
                listLog += tdEnd;
                
                //listLog += tdStart;
                //listLog += m.LOCATION;
                //listLog += tdEnd;

                listLog += tdStart;
                listLog += m.MESSAGE;
                listLog += tdEnd;
                listLog += trEnd;
            }
            ViewBag.processName = p_ProcessName;
            ViewBag.listLog = listLog;
        
            return Email("MailLayout");
        }
    }
}
