﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform.Starter.Models;

namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class PartMasterMaintenanceController : PageController
    {
        //
        // GET: /PartMasterMaintenance/

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public PartMasterMaintenanceController()
        {
            Settings.Title = "Part Master";
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            PartMasterMaintenance model = new PartMasterMaintenance();
            List<string> PartType = model.PartType();
            ViewData.Add("PartType", PartType);
        }

    }
}
