﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : SERVICE PARTS EXPORT (SPEX)
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : DeviceUserMaster
 * Function Name    : Device User Master
 * Function Group   : 
 * Program Id       : DeviceUserMaster
 * Program Name     : DeviceUserMasterController
 * Program Type     : Controller
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID.Yusuf
 * Version          : 01.00.00
 * Creation Date    : 05/09/2017 09:35:40
 * 

 *
 * Copyright(C) 2017 - . All Rights Reserved                                                                                              
 *************************************************************************************************/

using SPEX.Controllers.Base;
using SPEX.Models.DeviceUserMaster;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers.DeviceUserMaster
{
    public class DeviceUserMasterController : SPEXBaseController
    {
        public static string PACKING_COMPANY_COMBO_BOX = "PACKING_COMPANY_COMBO_BOX";
        public static string COMPANY_PLANT_CODE_COMBO_BOX = "COMPANY_PLANT_CODE_COMBO_BOX";
        public static string PLANT_LINE_CODE_COMBO_BOX = "PLANT_LINE_CODE_COMBO_BOX";
        public static string TRANSPORT_CODE_COMBO_BOX = "TRANSPORT_CODE_COMBO_BOX";
        public static string POSITION_COMBO_BOX = "POSITION_COMBO_BOX";

        public DeviceUserMasterController()
        {
            Settings.Title = "Device User Master";
        }        


        protected override void Startup()
        {
            getpE_UserId = (User)ViewData[SPEXBaseController.USER];
            ViewData[PACKING_COMPANY_COMBO_BOX] = DeviceUserMasterRepository.GetPackingCompanyList();
            ViewData[COMPANY_PLANT_CODE_COMBO_BOX] = DeviceUserMasterRepository.GetCompanyPlantCodeList();
            ViewData[PLANT_LINE_CODE_COMBO_BOX] = DeviceUserMasterRepository.GetPlantLineCodeList();
            ViewData[TRANSPORT_CODE_COMBO_BOX] = DeviceUserMasterRepository.GetTransportCodeList();
            ViewData[POSITION_COMBO_BOX] = DeviceUserMasterRepository.GetPositionList();
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        
        [HttpPost, ValidateInput(false)]
        public ActionResult DeviceUserMasterGridCallback(DeviceUserMasterForm param)
        {
            List<SPEX.Models.DeviceUserMaster.DeviceUserMaster> model = DeviceUserMasterRepository.getList(param.search);
            return PartialView("DeviceUserMasterGrid", model);
        }

        public ActionResult GetUserDeviceInfo(DeviceUserMasterForm param)
        {
            bool result = false;
            string message = "user data is not found";
            SPEX.Models.DeviceUserMaster.DeviceUserMaster deviceUserInfo = null;
            List<SPEX.Models.DeviceUserMaster.DeviceUserMaster> model = DeviceUserMasterRepository.getList(param.search);
            
            if (model != null && model.Any())
            {
                deviceUserInfo = model[0];
                result = true;
                if ((deviceUserInfo.LoginStatus ?? "").ToUpper().Equals("N"))
                {
                    message = "User login status is not login";
                }
            }

            return Json(
                new
                {
                    feedback = result,
                    detailInfo = deviceUserInfo,
                    message = message
                }
            );
        }

        public ActionResult DeleteUserDevice(DeviceUserMasterForm param)
        {
            if (getpE_UserId == null)
            {
                return Json(new { feedback = "false", message = "Please re-login" }, JsonRequestBehavior.AllowGet);
            }

            bool result = true;
            string message = "failed delete existing User Device Master data";
            string[] logins = param.search.UserLogin.Split(';');
            int amount = 0;

            try
            {
                DeviceUserMasterSearch searchCriteria = new DeviceUserMasterSearch();
                foreach (var id in logins)
                {
                    searchCriteria.UserLogin = id;
                    if (DeviceUserMasterRepository.getList(searchCriteria).Where(i => i.LoginStatus != null && i.LoginStatus == "1").Any())
                    {
                        result = false;
                        message = "there is " + id + " user login with status =1";
                        throw new Exception();
                    }
                }
                foreach (var id in logins)
                {
                    amount = amount + DeviceUserMasterRepository.DeleteUserDevice(id);
                }
                message = "success delete user master " + amount.ToString() + " rows";
            }
            catch (Exception)
            {
                result = false;
            }
            
            return Json(
                new
                {
                    feedback = result,
                    message = message
                }
            );
        }

        public ActionResult ResetUserDevice(DeviceUserMasterForm param)
        {
            if (getpE_UserId == null)
            {
                return Json(new { feedback = "false", message = "Please re-login" }, JsonRequestBehavior.AllowGet);
            }

            bool result = true;
            string message = "unknown error";
            string[] logins = param.search.UserLogin.Split(';');
            int amount = 0;

            try
            {
                DeviceUserMasterSearch searchCriteria = new DeviceUserMasterSearch();
                foreach (var id in logins)
                {
                    searchCriteria.UserLogin = id;
                    if (DeviceUserMasterRepository.getList(searchCriteria).Where(i => i.LoginStatus != null && i.LoginStatus == "1").Any())
                    {
                        result = false;
                        message = "there is " + id + " user login with status =1";
                        throw new Exception();
                    }
                }

                foreach (var id in logins)
                {
                    amount = amount + DeviceUserMasterRepository.ResetUserDevice(id, getpE_UserId.Username.ToString());
                }

                message = "success reset user master " + amount.ToString() + " rows";
                result = true;
            }
            catch (Exception)
            {

            }

            return Json(
                new
                {
                    feedback = result,
                    message = message
                }
            );
        }

        public ActionResult SaveAddUserDevice(DeviceUserMasterForm param)
        {
            if (getpE_UserId == null)
            {
                return Json(new { feedback = "false", message = "Please re-login" }, JsonRequestBehavior.AllowGet);
            }
         
            bool result = true;
            string message = "failed add new User Device Master data";
            try
            {
                validate(param.search);

                DeviceUserMasterSearch searchCriteria = new DeviceUserMasterSearch();
                searchCriteria.UserLogin = param.search.UserLogin;
                if (DeviceUserMasterRepository.getList(searchCriteria).Any())
                {             
                    throw new Exception("User Login [" + searchCriteria.UserLogin + "] already exist in User Device Master");
                }

                result = DeviceUserMasterRepository.AddUserDevice(param.search, getpE_UserId.Username) > 0;
                if (result)
                {
                    message = "adding new User Device Master data has been finished succesfully";
                }                
            }
            catch (Exception e)
            {
                result = false;
                if (e.InnerException != null)
                {
                    message = e.InnerException.Message;
                }
                else
                {
                    message = e.Message;
                }
            }

            return Json(
                new
                {
                    feedback = result,
                    message = message
                }
            );
        }

        public ActionResult SaveEditUserDevice(DeviceUserMasterForm param)
        {
            if (getpE_UserId == null)
            {
                return Json(new { feedback = "false", message = "Please re-login" }, JsonRequestBehavior.AllowGet);
            }
         

            bool result = false;
            string message = "failed update existing User Device Master data";

            try
            {
                validate(param.search);

                DeviceUserMasterSearch searchCriteria = new DeviceUserMasterSearch();
                searchCriteria.UserLogin = param.search.UserLogin;
                if (DeviceUserMasterRepository.getList(searchCriteria).Any().Equals(false))
                {
                    throw new Exception("User Login [" + searchCriteria.UserLogin + "] is not exist in User Device Master");
                }
                result = DeviceUserMasterRepository.UpdateUserDevice(param.search, getpE_UserId.Username)>0;
                if (result)
                {
                    message = "updating new User Device Master data has been finished succesfully";
                }
            }
            catch (Exception e)
            {
                result = false;
                if (e.InnerException != null)
                {
                    message = e.InnerException.Message;
                }
                else
                {
                    message = e.Message;
                }
            }

            return Json(
                new
                {
                    feedback = result,
                    message = message
                }
            );
        }

        public FileContentResult DownloadCSV(DeviceUserMasterForm param)
        {
            List<SPEX.Models.DeviceUserMaster.DeviceUserMaster> model = DeviceUserMasterRepository.getList(param.search);

            using (MemoryStream output = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(output, Encoding.UTF8))
                {
                    writer.Write("No");
                    writer.Write(",");
                    writer.Write("User Login");
                    writer.Write(",");
                    writer.Write("User Paswd");
                    writer.Write(",");
                    writer.Write("User Name");
                    writer.Write(",");
                    writer.Write("Packing Company");
                    writer.Write(",");
                    writer.Write("Company Plant Code");
                    writer.Write(",");
                    writer.Write("Plant Line Code");
                    writer.Write(",");
                    writer.Write("Tranport Code");
                    writer.Write(",");
                    writer.Write("Device NW Ident");
                    writer.Write(",");
                    writer.Write("Position Code");
                    writer.Write(",");
                    writer.Write("Login Status");
                    writer.Write(",");
                    writer.Write("Created By");
                    writer.Write(",");
                    writer.Write("Created Date");
                    writer.Write(",");
                    writer.Write("Changed By");
                    writer.Write(",");
                    writer.Write("Changed Date");
                    writer.WriteLine();
                    int n = 1;
                    foreach (var item in model)
                    {
                        writer.Write(n.ToString());
                        writer.Write(",");
                        writer.Write(item.UserLogin ?? "");
                        writer.Write(",");
                        writer.Write(item.UserPaswd ?? "");
                        writer.Write(",");
                        writer.Write(item.UserName ?? "");
                        writer.Write(",");
                        writer.Write(item.PackingCompany ?? "");
                        writer.Write(",");
                        writer.Write(item.CompanyPlantCode ?? "");
                        writer.Write(",");
                        writer.Write(item.PlantLineCode ?? "");
                        writer.Write(",");
                        writer.Write(item.TransportCode ?? "");
                        writer.Write(",");
                        writer.Write(item.DeviceNWIdent ?? "");
                        writer.Write(",");
                        writer.Write(item.Position ?? "");
                        writer.Write(",");
                        writer.Write(item.LoginStatus ?? "");
                        writer.Write(",");
                        writer.Write(item.CreatedBy ?? "");
                        writer.Write(",");
                        writer.Write(item.CreatedDtTxt ?? "");
                        writer.Write(",");
                        writer.Write(item.ChangedBy ?? "");
                        writer.Write(",");
                        writer.Write(item.ChangedDtTxt ?? "");
                        writer.WriteLine();
                        n++;
                    }
                    writer.Flush();
                }
                
                return File(output.ToArray(), "text/csv", "device_user_master.csv");
            }
        }

        public ActionResult ForceLogoffExistingUserDevice(DeviceUserMasterForm param)
        {
            if (getpE_UserId == null)
            {
                return Json(new { feedback = "false", message = "Please re-login" }, JsonRequestBehavior.AllowGet);
            }

            bool result = false;
            bool logoffSts = false;
            string message = "failed force logoff existing User Device Master data";

            try
            {
                DeviceUserMasterSearch searchCriteria = new DeviceUserMasterSearch();
                searchCriteria.UserLogin = param.search.UserLogin;
                List<SPEX.Models.DeviceUserMaster.DeviceUserMaster> user = DeviceUserMasterRepository.getList(searchCriteria);
                if (user.Any().Equals(false))
                {
                    throw new Exception("User Login [" + searchCriteria.UserLogin + "] is not exist in User Device Master");
                }
                else
                {
                    if (user.Where(i => i.UserLogin.Equals(getpE_UserId.Username)).Any())
                    {
                        throw new Exception("user login is different with selection");
                    }
                    else if (user.Where(i => i.UserLogin.Equals(getpE_UserId.Username) && i.Password.Equals(getpE_UserId.Password)).Any())
                    {
                        throw new Exception("user and password not vailid");
                    }
                    else
                    {
                        logoffSts = DeviceUserMasterRepository.ForceLogoffV2(param.search.UserLogin);
                        if (logoffSts)
                        {
                            message = "success force logoff for user " + param.search.UserLogin;
                            result = true;
                        }
                        else
                        {
                            message = "failed force logoff for user " + param.search.UserLogin;
                            result = false;
                        }
                    }
                }
                
            }
            catch (Exception e)
            {
                result = false;
                if (e.InnerException != null)
                {
                    message = e.InnerException.Message;
                }
                else
                {
                    message = e.Message;
                }
            }

            return Json(
                new
                {
                    feedback = result,
                    message = message
                }
            );
        }
    }
}