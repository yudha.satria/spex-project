﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : SERVICE PARTS EXPORT (SPEX)
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : DeviceUserMaster
 * Function Name    : Device User Master
 * Function Group   : 
 * Program Id       : DeviceUserMaster
 * Program Name     : DeviceUserMasterSearch
 * Program Type     : Search sub form
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID.Yusuf
 * Version          : 01.00.00
 * Creation Date    : 05/09/2017 09:35:40
 * 

 *
 * Copyright(C) 2017 - . All Rights Reserved                                                                                              
 *************************************************************************************************/

using SPEX.General.Validation;
using SPEX.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SPEX.Controllers.DeviceUserMaster
{
    public class DeviceUserMasterSearch : SPEXBaseModel
    {
        [SPEXMandatory(aliasName = "User Login")]
        public string UserLogin { get; set; }

        [SPEXMandatory(aliasName = "Password")]
        public string password { get; set; }

        [SPEXMandatory(aliasName = "User Name")]
        public string UserName { get; set; }

        [SPEXMandatory(aliasName = "Packing Company")]
        public string PackingCompany { get; set; }

        public string CompanyPlantCode { get; set; }

        [SPEXMandatory(aliasName = "Plant Line Code")]
        public string PlantLineCode { get; set; }        

        [SPEXMandatory(aliasName = "Transport Code")]
        public string TransportCode { get; set; }

        [SPEXMandatory(aliasName = "Position")]
        public string PositionCode { get; set; }

        
        public string DeviceNWIdent { get; set; }
        public string LoginStatus { get; set; }       
    }
}