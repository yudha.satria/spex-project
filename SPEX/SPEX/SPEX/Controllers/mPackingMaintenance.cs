﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using SPEX.Cls;

namespace SPEX.Controllers
{
    public class mPackingMaintenance
    {
        public string doReOpenCase(string Ids, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var q = db.SingleOrDefault<string>("PackingMaintenanceReOpenCase", new
            {
                IDS = Ids,
                USER_ID = UserID
            }).ToString();
            db.Close();
            return q;
        }

        public string doDeleteCase(mPackingMaintenanceCase Param, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var q = db.SingleOrDefault<string>("PackingMaintenanceDeleteCase", new
            {
                CASE_NO = Param.CASE_NO,
                CASE_TYPE = Param.CASE_TYPE,
                PREPARED_DT = Param.PREPARED_DT,
                USER_ID = UserID
            }).ToString();
            db.Close();
            return q;
        }

        public string doRePrintCase(string id,string printer, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var q = db.SingleOrDefault<string>("PackingMaintenanceRePrintCase", new
            {
                IDS = id,
                PRINTER = printer,
                USER_ID = UserID
            }).ToString();
            db.Close();
            return q;
        }

        public string doCancelCase(string Ids, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var q = db.SingleOrDefault<string>("PackingMaintenanceCancelCase", new
            {
                IDS = Ids,
                USER_ID = UserID
            }).ToString();
            db.Close();
            return q;
        }

        public string doDeleteKanban(mPackingMaintenanceKanban Param, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var q = db.SingleOrDefault<string>("PackingMaintenanceDeleteKanban", new
            {
                CASE_NO = Param.CASE_NO,
                CASE_TYPE = Param.CASE_TYPE,
                PREPARED_DT = Param.PREPARED_DT,
                KANBAN_ID = Param.KANBAN_ID,
                USER_ID = UserID
            }).ToString();
            db.Close();
            return q;
        }

        public string doCancelKanban(mPackingMaintenanceKanban Param, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var q = db.SingleOrDefault<string>("PackingMaintenanceCancelKanban", new
            {
                CASE_NO = Param.CASE_NO,
                CASE_TYPE = Param.CASE_TYPE,
                PREPARED_DT = Param.PREPARED_DT,
                KANBAN_ID = Param.KANBAN_ID,
                USER_ID = UserID
            }).ToString();
            db.Close();
            return q;
        }
    }

    public class mPackingMaintenanceCase
    {
        public string CASE_NO { get; set; }
        public string CASE_TYPE { get; set; }
        public string PREPARED_DT { get; set; }
        public string PACKING_DT { get; set; }
    }

    public class mPackingMaintenanceKanban : mPackingMaintenanceCase
    {
        public string KANBAN_ID { get; set; }
    }
}