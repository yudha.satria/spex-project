﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model


namespace SPEX.Controllers
{
    public class ExportOrderHeaderMasterController : PageController
    {

        mExportOrderHeaderMaster ExportOrderHeaderMaster = new mExportOrderHeaderMaster();
        MessagesString messageError = new MessagesString();

        
        public ExportOrderHeaderMasterController()
        {
            Settings.Title = "Export Order Header Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }


        protected override void Startup()
        {
            ComboPDCode();
            ComboBuyerCode();
            ViewBag.MSPX00001ERR = messageError.getMSPX00001ERR("Export Order Header");
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = messageError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        public void ComboPDCode()
        {
            List<mExportOrderHeaderMaster> model = new List<mExportOrderHeaderMaster>();

            model = ExportOrderHeaderMaster.getListPDCode();
            ViewData["PD_CD"] = model;
        }

        public void ComboBuyerCode()
        {
            List<mExportOrderHeaderMaster> model = new List<mExportOrderHeaderMaster>();

            model = ExportOrderHeaderMaster.getListBuyerCode();
            ViewData["BUYER_CD"] = model;
        }

        public ActionResult GridViewExportOrderHeader(string pPDCode, string pBuyerCode, string pOrderNo, string pOrderDateFrom, string pOrderDateTo)
        {
            List<mExportOrderHeaderMaster> model = new List<mExportOrderHeaderMaster>();

            {
                model = ExportOrderHeaderMaster.getListExportOrderHeaderMaster(pPDCode, pBuyerCode, pOrderNo, pOrderDateFrom, pOrderDateTo);
            }
            return PartialView("GridExportOrderHeaderMaster", model);

        }

        public void _DownloadExportOrderHeaderMaster(object sender, EventArgs e, string D_PDCode, string D_BuyerCode, string D_OrderNo, string D_OrderDateFrom, string D_OrderDateTo)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/ExportOrderHeader_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;

            ISheet sheet = workbook.GetSheet("ExportOrderHeaderMaster");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "DownloadExportOrderHeader_" + date + ".xls";
            string dateNow = DateTime.Now.ToString("dd-MM-yyyy");

            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(3).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(9).GetCell(3).SetCellValue(D_PDCode);
            sheet.GetRow(10).GetCell(3).SetCellValue(D_BuyerCode);
            sheet.GetRow(11).GetCell(3).SetCellValue(D_OrderNo);
            sheet.GetRow(12).GetCell(3).SetCellValue(D_OrderDateFrom);
            sheet.GetRow(13).GetCell(3).SetCellValue(D_OrderDateTo);

            int row = 19;
            int rowNum = 1;
            IRow Hrow;

            //insert data show in screen into document 
            List<mExportOrderHeaderMaster> model = new List<mExportOrderHeaderMaster>();
            model = ExportOrderHeaderMaster.ExportOrderHeaderDownload(D_PDCode, D_BuyerCode, D_OrderNo, D_OrderDateFrom, D_OrderDateTo);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);
                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PD_CD);
                Hrow.CreateCell(3).SetCellValue(result.BUYER_CD);
                Hrow.CreateCell(4).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(5).SetCellValue(result.ORDER_DT);
                Hrow.CreateCell(6).SetCellValue(result.TRANSPORTATION_CD);
                Hrow.CreateCell(7).SetCellValue(result.ORDER_TYPE);
                Hrow.CreateCell(8).SetCellValue(result.BO_INSTRUCTION_CD);
                Hrow.CreateCell(9).SetCellValue(result.PAYMENT_TERM_CD);
                Hrow.CreateCell(10).SetCellValue(result.CURRENCY_CD);
                Hrow.CreateCell(11).SetCellValue(result.SELLER_CD);
                Hrow.CreateCell(12).SetCellValue(result.TNSO_FILE_CD);
                Hrow.CreateCell(13).SetCellValue(result.PRICE_TERM_CD);
                Hrow.CreateCell(14).SetCellValue(result.ORIGINAL_BUYER_CD);

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent3;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;

                row++;
                rowNum++;

            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", filename));
        }

        public void _DownloadExportOrderHeaderMasterThicked(object sender, EventArgs e, string D_PDCode, string D_BuyerCode, string D_OrderNo, string D_OrderDateFrom, string D_OrderDateTo)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/ExportOrderHeader_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;

            ISheet sheet = workbook.GetSheet("ExportOrderHeaderMaster");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "DownloadExportOrderHeader_" + date + ".xls";
            string dateNow = DateTime.Now.ToString("dd-MM-yyyy");

            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(9).GetCell(3).SetCellValue(D_PDCode);
            sheet.GetRow(10).GetCell(3).SetCellValue(D_BuyerCode);
            sheet.GetRow(11).GetCell(3).SetCellValue(D_OrderNo);
            sheet.GetRow(12).GetCell(3).SetCellValue(D_OrderDateFrom);
            sheet.GetRow(13).GetCell(3).SetCellValue(D_OrderDateTo);

            int row = 19;
            int rowNum = 1;
            IRow Hrow;

            List<mExportOrderHeaderMaster> model = new List<mExportOrderHeaderMaster>();

            model = ExportOrderHeaderMaster.ExportOrderHeaderDownload(D_PDCode, D_BuyerCode,  D_OrderNo, D_OrderDateFrom, D_OrderDateTo);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PD_CD);
                Hrow.CreateCell(3).SetCellValue(result.BUYER_CD);
                Hrow.CreateCell(4).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(5).SetCellValue(result.ORDER_DT);
                Hrow.CreateCell(6).SetCellValue(result.TRANSPORTATION_CD);
                Hrow.CreateCell(7).SetCellValue(result.ORDER_TYPE);
                Hrow.CreateCell(8).SetCellValue(result.BO_INSTRUCTION_CD);
                Hrow.CreateCell(9).SetCellValue(result.PAYMENT_TERM_CD);
                Hrow.CreateCell(10).SetCellValue(result.CURRENCY_CD);
                Hrow.CreateCell(11).SetCellValue(result.SELLER_CD);
                Hrow.CreateCell(12).SetCellValue(result.TNSO_FILE_CD);
                Hrow.CreateCell(13).SetCellValue(result.PRICE_TERM_CD);
                Hrow.CreateCell(14).SetCellValue(result.ORIGINAL_BUYER_CD);

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent3;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;

                row++;
                rowNum++; 
            }
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", filename));
        }


    }
}
