﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
//using BarTender;
using SPEX.Cls;
using SPEX.Models;
using Telerik.Reporting.Processing;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

//using Seagull.BarTender.Print;
//using Seagull.Services.PrintScheduler;
namespace SPEX.Controllers
{
    public class PackingInstructionController : PageController
    {
        //
        // GET: /PackingInstruction/
        protected string  Function_Id="F17-001";
        protected string Module_Id="F17";
        
        public PackingInstructionController()
        {
            Settings.Title = "Packing Instruction";        
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            
            getpE_UserId = (User)ViewData["User"];
        }

        public ActionResult RePrint(string PartNo,string SUPLIER_PLANT, string Copy)
        {
            try
            {
                PackingInstruction PI = new PackingInstruction();
                string MESSAGE = PI.InsertReprint(PartNo, SUPLIER_PLANT, Int32.Parse(Copy), PI.GetSettingInsert(), getpE_UserId.Username, Module_Id, Function_Id);
                string[] result = MESSAGE.Split('|');
                if (result[0].ToString() == "true")
                {
                    //string filename = GeneratePDFREprint(result[1].ToString(), PartNo);
                    //var deviceInfo = new Hashtable();
                    //deviceInfo["JavaScript"] = "this.print({bUI: false, bSilent: true, bShrinkToFit: true});";
                    //printDocument(filename);
                    //RePrintDataBartender(Int64.Parse(result[1].ToString()), Copy);
                    return Json(new { success = result[0].ToString(), PID = result[1].ToString() }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    PI.deletePackingInstructionPartLabelTempByProcessId(Int64.Parse(result[1].ToString()));
                    return Json(new { success = result[0].ToString(), PID = result[1].ToString(), messages = result[2].ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = "false",  messages = ex.Message }, JsonRequestBehavior.AllowGet);

            }
            return null;
        }

        private string GeneratePDFREprint(string PROCESS_ID, string part_no)
        {
            
            string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            mTelerik report;
            //model.APPROVED_BY = HttpContext.Request.MapPath(model.APPROVED_BY);
            String source = Server.MapPath("~/Report/PartLabel.trdx");
            FileInfo fileInfo = new FileInfo(source);

            pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
            Directory.CreateDirectory(pdfFolderPath);

            //DeleteOlderFile(pdfFolderPath);

            pdfFilename = "REPRINT_"+ date;
            if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                System.IO.File.Delete(pdfFolderPath + pdfFilename);

            report = new mTelerik(source, pdfFilename);

            //report.SetConnectionString(System.IO.File.OpenText(HttpContext.Request.MapPath("~/Configurations/report-connection-string.txt")).ReadLine());
            report.AddParameters("PART_NO", part_no);
            report.AddParameters("PRINT_COUNT", "1");
            report.AddParameters("PART_TYPE", string.Empty);
            report.AddParameters("PROCESS_ID", PROCESS_ID);
            report.SetConnectionString(System.IO.File.OpenText(HttpContext.Request.MapPath(new PackingInstruction().GetReportDBPath())).ReadLine());
            Dictionary<string, string> uriParam = new Dictionary<string, string>();


            //report.SetSubConnectionString(uriParam, System.IO.File.OpenText(HttpContext.Request.MapPath("~/Configurations/report-connection-string.txt")).ReadLine());
            //report.SetSubConnectionString(uriParam, System.IO.File.OpenText(HttpContext.Request.MapPath(download.GetReportDBPath())).ReadLine());

            using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
            {
                byte[] info = report.GeneratePDF();
                fs.Write(info, 0, info.Length);
            }
            return report.ResultName;
        }
        
        //public void printDocument(string pdfname)
        //{
        //    string szPrinterName = @"\\win2pdf";
            

        //    string fullpath = HttpContext.Request.MapPath("~/Content/ReportResult/") + pdfname;
        //    StreamReader sr = new StreamReader(fullpath);
        //    string line = (char)27 + "*c32545D";
        //    line += sr.ReadToEnd();
        //    line += (char)27 + "*c5F";

        //    SPEX.Cls.Print.SendStringToPrinter(szPrinterName, line);
        //}


        public void RePrintData(string PROCESS_ID, string part_no)
        {
            
            long PID = Convert.ToInt64(PROCESS_ID);
            MessagesString MSG = new MessagesString();
            Log lg = new Log();
            PID = lg.createLogOnProgress(MSG.getMSG_Spex("MSPXPI004INF", PID), getpE_UserId.Username, "Reprint", PID, Module_Id, Function_Id);
            string pdfFilename = "";
            String source = Server.MapPath("~/Report/PartLabel.trdx"); //Server.MapPath("~") + @"Content\Report\Reprint.trdx";
            //String pdfFolderPath = HttpContext.Request.MapPath("Content/FileReport/");
            FileInfo FI = new FileInfo(source);
            mTelerik report;
            PackingInstruction PI = new PackingInstruction();
            if (FI.Exists)
            {
                #region
                pdfFilename = "ReprintPart_label_";
                report = new mTelerik(source, pdfFilename);
                report.SetResultName(pdfFilename + DateTime.Now.ToString("yyyyMMddhhmmss"));
                //report.SetConnectionString(DatabaseManager.Instance.GetConnectionDescriptor("CLS").ConnectionString);
                report.SetConnectionString(System.IO.File.OpenText(HttpContext.Request.MapPath(new PackingInstruction().GetReportDBPath())).ReadLine());
                report.AddParameters("PART_NO", part_no);
                report.AddParameters("PRINT_COUNT", "1");
                report.AddParameters("PART_TYPE", string.Empty);
                report.AddParameters("PROCESS_ID", PROCESS_ID);

                var deviceInfo = new Hashtable();
                deviceInfo["JavaScript"] = "this.print({bUI: false, bSilent: true, bShrinkToFit: true});";

                Telerik.Reporting.IReportDocument myReport = report.Report;
                var reportProcessor = new ReportProcessor();
                var reportSource = new Telerik.Reporting.InstanceReportSource();
                reportSource.ReportDocument = myReport;
                var renderingResult = reportProcessor.RenderReport("PDF", reportSource, deviceInfo);

                Response.Clear();
                Response.ContentType = renderingResult.MimeType;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.Expires = -1;
                Response.Buffer = true;
                Response.BinaryWrite(renderingResult.DocumentBytes);
                Response.End();
                #endregion
                PI.deletePackingInstructionPartLabelTempByProcessId(PID);
                PID = lg.createLogOnProgress(MSG.getMSG_Spex("MSPXPI005INF", PID), getpE_UserId.Username, "Reprint", PID, Module_Id, Function_Id);
                PID = lg.createLog(MSG.getMSG_Spex("MSPXPI006INF", PID), getpE_UserId.Username, "RePrint", PID, Module_Id, Function_Id);

                //update reprint
                //ReprintRepository.Instance.UpdateReprint(SLIP_PK);

            }
            else
            {
                PI.deletePackingInstructionPartLabelTempByProcessId(PID);
                PID = lg.createLog(MSG.getMSG_Spex("MSPXPI006INF", PID), getpE_UserId.Username, "RePrint", PID, Module_Id, Function_Id);
                TempData["msg"] = "<script>alert('Template file not found');</script>";
            }
        }

        //public void RePrintDataBartender(long PROCESS_ID, string Copy)
        //{
            
        //    PackingInstruction PI =new PackingInstruction();
        //    BarTender.ApplicationClass bt = new BarTender.ApplicationClass();
        //    Format bt_format = new Format();
        //    Messages bt_messsage ;
        //    Log lg = new Log();
        //    MessagesString MSG = new MessagesString();
        //    lg.createLogOnProgress(MSG.getMSG_Spex("MSPXPI004INF", PROCESS_ID), getpE_UserId.Username, "Reprint", PROCESS_ID, Module_Id, Function_Id);

        //    bt_format = bt.Formats.Open(Server.MapPath(Common.GetDataMasterSystem("PACKING_INSTRUCTION", "PATH")), false, "");
        //    var db=bt_format.Databases.GetDatabase(1);
        //    db.OLEDB.UserId = Common.GetDataMasterSystem("DB_CONFIG", "USER");
        //    db.OLEDB.Password = Common.GetDataMasterSystem("DB_CONFIG", "PASSWORD");
        //    db.OLEDB.SQLStatement = Common.GetDataMasterSystem("PACKING_INSTRUCTION", "SQL") + PROCESS_ID.ToString();
        //    bt_format.PrintSetup.IdenticalCopiesOfLabel = int.Parse(Copy);
        //    bt_format.PrintOut();
        //    bt_format.Close(BarTender.BtSaveOptions.btDoNotSaveChanges);
        //    PI.deletePackingInstructionPartLabelTempByProcessId(PROCESS_ID);
        //    lg.createLogOnProgress(MSG.getMSG_Spex("MSPXPI005INF", PROCESS_ID), getpE_UserId.Username, "Reprint", PROCESS_ID, Module_Id, Function_Id);
        //    lg.createLog(MSG.getMSG_Spex("MSPXPI006INF", PROCESS_ID), getpE_UserId.Username, "RePrint", PROCESS_ID, Module_Id, Function_Id);

        //    //using (Engine bt = new Engine())
        //    //{
        //    //    bt.Start()
        //    //    LabelFormatDocument format=bt.Documents.Open(HttpContext.Request.MapPath(cm.GetDataMasterSystem("PACKING_INSTRUCTION","PATH")))
        //    //    format.    
        //    //}
        //}

        //public ActionResult RePrintCaseLabel(string Copy)
        //{
        //    PackingInstruction PI = new PackingInstruction();
        //    BarTender.ApplicationClass bt = new BarTender.ApplicationClass();
        //    Format bt_format = new Format();
        //    Messages bt_messsage;
        //    Log lg = new Log();
        //    MessagesString MSG = new MessagesString();
        //    long PROCESS_ID = 0;
        //    PROCESS_ID=lg.createLogOnProgress(MSG.getMSG_Spex("MSPXCS01INF", PROCESS_ID), getpE_UserId.Username, "Reprint", PROCESS_ID, Module_Id, Function_Id);
        //    bt_format = bt.Formats.Open(Server.MapPath(Common.GetDataMasterSystem("PACKING_INSTRUCTION", "PATH")), false, "");
        //    var db = bt_format.Databases.GetDatabase(1);
        //    db.OLEDB.UserId = Common.GetDataMasterSystem("DB_CONFIG", "USER");
        //    db.OLEDB.Password = Common.GetDataMasterSystem("DB_CONFIG", "PASSWORD");
        //    db.OLEDB.SQLStatement = Common.GetDataMasterSystem("PACKING_INSTRUCTION", "SQL") + PROCESS_ID.ToString();
        //    bt_format.PrintSetup.IdenticalCopiesOfLabel = int.Parse(Copy);
        //    bt_format.PrintOut();
        //    bt_format.Close(BarTender.BtSaveOptions.btDoNotSaveChanges);
        //    PI.DeleteCaseLabelTemp(PROCESS_ID);
        //    //lg.createLogOnProgress(MSG.getMSG_Spex("MSPXPI005INF", PROCESS_ID), getpE_UserId.Username, "Reprint", PROCESS_ID, Module_Id, Function_Id);
        //    lg.createLog(MSG.getMSG_Spex("MSPXCS02INF", PROCESS_ID), getpE_UserId.Username, "RePrint", PROCESS_ID, Module_Id, Function_Id);
        //    return null;
        //}
      
    }
}
