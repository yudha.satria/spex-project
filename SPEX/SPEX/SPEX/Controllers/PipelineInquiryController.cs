﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Text;

namespace SPEX.Controllers
{
    public class PipeLineInquiryController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mCaseMaster caseMaster = new mCaseMaster();
        PipeLineInquiry pli = new PipeLineInquiry();

        public PipeLineInquiryController()
        {
            Settings.Title = "Pipe Line Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            //ViewData["BuyerPDCdList"] = pli.GetBuyerMasterList();
            //ViewData["TranpCdList"] = pli.GetTransportationMasterList();
            //ViewData["OrderTypeList"] = pli.GetOrderTypeMasterList();
            //ViewData["StatusList"] = pli.GetStatusList();
            //ViewData["ProcessList"] = pli.GetProcessList();
            ViewData["Range_Order_Date"] = pli.GetRangeOrderDate();
            GetComboBoxOrderType();
            GetComboBoxBuyerPD();
            GetComboboxTranpCode();
            GetComboboxStatus();
            GetComboboxStatusProcess();
            GetComboboxStockFlag();
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (string.IsNullOrEmpty(dt))
            {
                result = "";
            }
            else if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PipeLineInquiryGridCallback(string pOrderNo, string pBuyerPdCd, string pOrderDtFrom, string pOrderDtTo, string pItemNo, string pOrderType
            , string pEdtFrom, string pEdtTo, string pPartNo, string pTransportationCd, string pStatus, string pStockFlag, string pTMMINdate, string pTMMINdateTo, string pProcess)
        {
            List<PipeLineInquiry> model = new List<PipeLineInquiry>();

            //model = pli.getList(pOrderNo, pBuyerPdCd, reFormatDate(pOrderDtFrom), reFormatDate(pOrderDtTo), pItemNo, pOrderType, reFormatDate(pEdtFrom), reFormatDate(pEdtTo),
            //    pPartNo, pTransportationCd, pStatus, pStockFlag, reFormatDate(pTMMINdate), reFormatDate(pTMMINdateTo), pProcess);

            model = pli.getListPipeLineInquiry(pOrderNo, pBuyerPdCd, reFormatDate(pOrderDtFrom), reFormatDate(pOrderDtTo), pItemNo, pOrderType, reFormatDate(pEdtFrom), reFormatDate(pEdtTo),
               pPartNo, pTransportationCd, pStatus, pStockFlag, reFormatDate(pTMMINdate), reFormatDate(pTMMINdateTo), pProcess);
            //GetComboboxStockFlag();
            return PartialView("PipeLineInquiryGrid", model);
        }


        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }


        public void DownloadData_ByParameter(object sender, EventArgs e, string pOrderNo, string pBuyerPdCd, string pOrderDtFrom, string pOrderDtTo, string pItemNo, string pOrderType
            , string pEdtFrom, string pEdtTo, string pPartNo, string pTransportationCd, string pStatus, string pStockFlag, string pTMMINdate, string pTMMINdateTo, string pProcess)
        {
            string filename = "";
            PipeLineInquiry pli = new PipeLineInquiry();
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPEX_PIPELINE_" + date + ".csv";

            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            string dateNow = DateTime.Now.ToString();

            List<PipeLineInquiry> model = new List<PipeLineInquiry>();

            //model = pli.getList(pOrderNo, pBuyerPdCd, reFormatDate(pOrderDtFrom), reFormatDate(pOrderDtTo), pItemNo, pOrderType, reFormatDate(pEdtFrom), reFormatDate(pEdtTo),
            //    pPartNo, pTransportationCd, pStatus,  pStockFlag, reFormatDate(pTMMINdate), reFormatDate(pTMMINdateTo), pProcess);

            model = pli.getListPipeLineInquiry(pOrderNo, pBuyerPdCd, reFormatDate(pOrderDtFrom), reFormatDate(pOrderDtTo), pItemNo, pOrderType, reFormatDate(pEdtFrom), reFormatDate(pEdtTo),
                pPartNo, pTransportationCd, pStatus, pStockFlag, reFormatDate(pTMMINdate), reFormatDate(pTMMINdateTo), pProcess);


            StringBuilder gr = new StringBuilder("");

            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol_Download.Count; i++)
                {
                    gr.Append(Quote(CsvCol_Download[i].Text)); gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);


                foreach (PipeLineInquiry d in model)
                {
                    csvAddLine(gr, new string[] {                        
                    //Equs(d.TMMIN_SEND_DT), 
                    Equs(String.IsNullOrEmpty(d.TMMIN_SEND_DT) ? String.Empty : (string)d.TMMIN_SEND_DT.ToString()),

                    Equs(d.TmapOrderDtTxt), 
                    Equs(d.BUYERPD_CD), 
                    Equs(d.ORDER_TYPE), 
                    Equs(d.TRANSPORTATION_CD),
                    Equs(d.ORDER_NO), 
                    Equs(d.ITEM_NO),
                    Equs(d.PART_NO), 
                    Equs(d.SUPPLIER_CD), 
                    Equs(d.SUB_SUPPLIER_CD), 
                    Equs((string)d.ACCEPT_QTY.ToString()),

                    //Equs((string)d.ACTUAL_MF_RECEIVE_PLAN_DT.ToString()),
                    Equs(String.IsNullOrEmpty(d.ACTUAL_MF_RECEIVE_PLAN_DT) ? String.Empty : (string)d.ACTUAL_MF_RECEIVE_PLAN_DT.ToString()),
                    //Equs((string)d.ACTUAL_MF_RECEIVE_DT.ToString()),
                    Equs(String.IsNullOrEmpty(d.ACTUAL_MF_RECEIVE_DT) ? String.Empty : (string)d.ACTUAL_MF_RECEIVE_DT.ToString()),
                    //2018-12-12 as request TMMIN.Hesti
                    //Equs((string)d.MF_RECEIVE_QTY.ToString()),
                    Equs(d.MF_RECEIVE_STATUS),
                    Equs((string)d.MF_RECEIVE_DELAY.ToString()),

                    Equs((string)d.ALLOCATE_QTY.ToString()),

                    //2018-12-12 as request TMMIN.Hesti
                    Equs(String.IsNullOrEmpty(d.PICKING_DT) ? String.Empty : (string)d.PICKING_DT.ToString()),

                    Equs((string)d.PICKING_QTY.ToString()),


                    //Equs((string)d.ACTUAL_RECEIVE_PLAN_DT.ToString()),
                    Equs(String.IsNullOrEmpty(d.ACTUAL_RECEIVE_PLAN_DT) ? String.Empty : (string)d.ACTUAL_RECEIVE_PLAN_DT.ToString()),
                    //Equs((string)d.ACTUAL_RECEIVE_DT.ToString()),
                    Equs(String.IsNullOrEmpty(d.ACTUAL_RECEIVE_DT) ? String.Empty : (string)d.ACTUAL_RECEIVE_DT.ToString()),
                    Equs((string)d.RECEIVE_QTY.ToString()),
                    Equs(d.RECEIVE_STATUS),
                    Equs(d.RECEIVE_DELAY.ToString()),


                    //Equs((string)d.ACTUAL_PACKING_PLAN_DT.ToString()),
                    Equs(String.IsNullOrEmpty(d.ACTUAL_PACKING_PLAN_DT) ? String.Empty : (string)d.ACTUAL_PACKING_PLAN_DT.ToString()),
                    //Equs((string)d.ACTUAL_PACKING_DT.ToString()),
                    Equs(String.IsNullOrEmpty(d.ACTUAL_PACKING_DT) ? String.Empty : (string)d.ACTUAL_PACKING_DT.ToString()),
                    Equs((string)d.PACKING_QTY.ToString()),
                    Equs(d.PACKING_STATUS),
                    Equs(d.PACKING_DELAY.ToString()),


                    
                    //Equs((string)d.ACTUAL_VANNING_PLAN_DT.ToString()),
                    Equs(String.IsNullOrEmpty(d.ACTUAL_VANNING_PLAN_DT) ? String.Empty : (string)d.ACTUAL_VANNING_PLAN_DT.ToString()),
                    //Equs((string)d.ACTUAL_VANNING_DT.ToString()),
                    Equs(String.IsNullOrEmpty(d.ACTUAL_VANNING_DT) ? String.Empty : (string)d.ACTUAL_VANNING_DT.ToString()),
                    Equs((string)d.VANNING_QTY.ToString()),
                    Equs(d.VANNING_STATUS),
                    Equs(d.VANNING_DELAY.ToString()),
                    

                    //Equs((string)d.ACTUAL_SHIPMENT_PLAN_DT.ToString()),
                    Equs(String.IsNullOrEmpty(d.ACTUAL_SHIPMENT_PLAN_DT) ? String.Empty : (string)d.ACTUAL_SHIPMENT_PLAN_DT.ToString()),
                    //Equs((string)d.ACTUAL_SHIPMENT_DT.ToString()),                    
                    Equs(String.IsNullOrEmpty(d.ACTUAL_SHIPMENT_DT) ? String.Empty : (string)d.ACTUAL_SHIPMENT_DT.ToString()),
                    Equs((string)d.SHIPMENT_QTY.ToString()),
                    Equs(d.SHIPMENT_STATUS),
                    Equs(d.SHIPMENT_DELAY.ToString()),
                    Equs(d.TmapEtdTxt)
                   });

                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        public ActionResult DownloadData_ByTicked_Ajax(string pOrderNo, string pBuyerPdCd, string pOrderDtFrom, string pOrderDtTo, string pItemNo, string pOrderType
            , string pEdtFrom, string pEdtTo, string pPartNo, string pTransportationCd, string pStatus, string pStockFlag, string pTMMINdate, string pTMMINdateTo, string pProcess)
        {
            //remaks agi 2017-12-14
            //string pid =  pli.getPIDDownload(pOrderNo, pBuyerPdCd, reFormatDate(pOrderDtFrom), reFormatDate(pOrderDtTo), pItemNo, pOrderType, reFormatDate(pEdtFrom), reFormatDate(pEdtTo),
            // pPartNo, pTransportationCd, pStatus, pProcess);
            string pid = pli.getPIDDownload(pOrderNo, pBuyerPdCd, reFormatDate(pOrderDtFrom), reFormatDate(pOrderDtTo), pItemNo, pOrderType, reFormatDate(pEdtFrom), reFormatDate(pEdtTo),
            pPartNo, pTransportationCd, pStatus, pStockFlag, reFormatDate(pTMMINdate), reFormatDate(pTMMINdateTo), pProcess, getpE_UserId.Username);
            return Json(new { success = "true", messages = "OK", process_id = pid }, JsonRequestBehavior.AllowGet);
        }

        public void DownloadData_ByPID(object sender, EventArgs e, string pid)
        {
            string filename = "";
            PipeLineInquiry pli = new PipeLineInquiry();
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPEX_PIPELINE_" + date + ".csv";

            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            string dateNow = DateTime.Now.ToString();

            List<PipeLineInquiryDownloadDetail> model = new List<PipeLineInquiryDownloadDetail>();

            model = pli.getDownloadByPID(pid);

            StringBuilder gr = new StringBuilder("");

            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol_DownloadByTick.Count; i++)
                {
                    gr.Append(Quote(CsvCol_DownloadByTick[i].Text)); gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);


                foreach (PipeLineInquiryDownloadDetail d in model)
                {
                    csvAddLine(gr, new string[] {
                    Equs(d.TMMIN_SEND_DT),
                    Equs(d.TMAP_ORDER_DT),
                    Equs(d.BUYERPD_CD),
                    Equs(d.ORDER_TYPE),
                    Equs(d.TRANSPORTATION_CD),
                    Equs(d.ORDER_NO),
                    Equs(d.ITEM_NO),
                    Equs(d.PART_NO),
                    Equs(d.SUPPLIER_CD),
                    Equs(d.SUB_SUPPLIER_CD),
                    Equs(d.MANIFEST_NO),
                    Equs(""+d.ACCEPT_QTY),
                    Equs(d.KANBAN_ID),
                    Equs(d.MF_RECEIVE_DT),
                    //Equs(d.RECEIVE_DT),
                    //Equs(""+d.MF_RECEIVE_QTY),
                    Equs(d.MF_RECEIVE_STATUS),
                    Equs(""+d.MF_RECEIVE_DELAY),
                    Equs(""+d.ALLOCATE_QTY),
                    Equs(""+d.PICKING_DT),
                    Equs(""+d.PICKING_QTY),
                    Equs(""+d.RECEIVE_PLAN_DT),
                    //Equs(d.MF_RECEIVE_DT),
                    Equs(d.RECEIVE_DT),
                    Equs(""+d.RECEIVE_QTY),
                    Equs(d.RECEIVE_STATUS),
                    Equs(""+d.RECEIVE_DELAY),
                    Equs(d.CASE_NO),
                    Equs(""+d.PACKING_PLAN_DT),
                    Equs(d.PACKING_DT),
                    Equs(""+d.PACKING_QTY),
                    Equs(d.PACKING_STATUS),
                    Equs(""+d.PACKING_DELAY),
                    Equs(d.CONTAINER_NO),
                    Equs(""+d.VANNING_PLAN_DT),
                    Equs(d.VANNING_DT),
                    Equs(""+d.VANNING_QTY),
                    Equs(d.VANNING_STATUS),
                    Equs(""+d.VANNING_DELAY),
                    Equs(d.INVOICE_NO),
                    Equs(d.INVOICE_DT),
                    Equs(""+d.SHIPMENT_PLAN_DT),
                    Equs(d.SHIPMENT_DT),
                    Equs(""+d.SHIPMENT_QTY),
                    Equs(d.SHIPMENT_STATUS),
                    Equs(""+d.SHIPMENT_DELAY)
                   });
                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        public void DownloadData_ByTicked(object sender, EventArgs e, string pOrderNo, string pBuyerPdCd, string pOrderDtFrom, string pOrderDtTo, string pItemNo, string pOrderType
            , string pEdtFrom, string pEdtTo, string pPartNo, string pTransportationCd, string pStatus, string pStockFlag, string pTMMINdate, string pTMMINdateTo, string pProcess)
        {
            string filename = "";
            PipeLineInquiry pli = new PipeLineInquiry();
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPEX_PIPELINE_" + date + ".csv";

            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            string dateNow = DateTime.Now.ToString();

            List<PipeLineInquiry> model = new List<PipeLineInquiry>();

            model = pli.getDownloadByTick(pOrderNo, pBuyerPdCd, reFormatDate(pOrderDtFrom), reFormatDate(pOrderDtTo), pItemNo, pOrderType, reFormatDate(pEdtFrom), reFormatDate(pEdtTo),
                pPartNo, pTransportationCd, pStatus, pStockFlag, reFormatDate(pTMMINdate), reFormatDate(pTMMINdateTo), pProcess);

            StringBuilder gr = new StringBuilder("");

            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol_DownloadByTick.Count; i++)
                {
                    gr.Append(Quote(CsvCol_DownloadByTick[i].Text)); gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);


                foreach (PipeLineInquiry d in model)
                {
                    csvAddLine(gr, new string[] {
                    Equs(d.TmapOrderDtTxt), 
                    Equs(d.BUYERPD_CD), 
                    Equs(d.ORDER_TYPE), 
                    Equs(d.TRANSPORTATION_CD),
                    Equs(d.ORDER_NO), 
                    Equs(d.ITEM_NO),
                    Equs(d.PART_NO), 
                    Equs(d.ACCEPT_QTY.ToString()),

                    Equs(d.KANBAN_ID),
                    Equs(d.TmapRcvDtTxt),
                    Equs(d.CASE_NO),

                    Equs(d.TmapPckDtTxt),
                    Equs(d.CONTAINER_NO),
                    Equs(d.TmapVanDtTxt),

                    Equs(d.INVOICE_NO),
                    Equs(d.TmapInvDtTxt),
                    Equs(d.TmapShpDtTxt),
                   
                   });

                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        private List<CsvColumn> CsvCol_Download = CsvColumn.Parse(

           "Send Date   |C|1|0|TMMIN_SEND_DT;" +
           "Order Date  |C|1|0|TMAP_ORDER_DT;" +
           "Buyer PD    |C|6|1|BUYERPD_CD;" +
           "Order Type  |C|10|0|ORDER_TYPE;" +
           "Tranp. Code |C|5|0|TRANSPORTATION_CD;" +
           "Order No    |C|23|0|ORDER_NO;" +
           "Item No     |C|23|1|ITEM_NO;" +
           "Part No     |C|5|0|PART_NO;" +
           "Supplier    |C|5|0|SUPPLIER_CD;" +
           "Sub Supplier|C|5|0|SUB_SUPPLIER_CD;" +
           "Order Qty   |C|1|0|ACCEPT_QTY;" +

           "M/F Plan                |C|1|0|ACTUAL_MF_RECEIVE_PLAN_DT;" +
           "M/F Actual              |C|1|0|ACTUAL_MF_RECEIVE_DT;" +
            //2018-12-12 as request TMMIN.Hesti
            //"M/F Rcv Qty         |C|1|0|MF_RECEIVE_QTY;" +
           "M/F Rcv Status      |C|1|0|MF_RECEIVE_STATUS;" +
           "M/F Rcv Delay (Days)|C|1|0|MF_RECEIVE_DELAY;" +

           "Allocate Qty        |C|1|0|ALLOCATE_QTY;" +
            //2018-12-12 as request TMMIN.Hesti
           "Picking Actual         |C|1|0|PICKING_DT;" +
           "Picking Qty         |C|1|0|PICKING_QTY;" +

           "Sorting Plan                |C|1|0|ACTUAL_RECEIVE_PLAN_DT;" +
           "Sorting Actual              |C|1|0|ACTUAL_RECEIVE_DT;" +
           "Sorting Qty             |C|1|0|RECEIVE_QTY;" +
           "Sorting Status          |C|1|0|RECEIVE_STATUS;" +
           "Sorting Delay (Days)    |C|1|0|RECEIVE_DELAY;" +


           "Packing Plan                |C|1|0|ACTUAL_PACKING_PLAN_DT;" +
           "Packing Actual              |C|1|0|ACTUAL_PACKING_DT;" +
           "Packing Qty         |C|1|0|PACKING_QTY;" +
           "Packing Status      |C|1|0|PACKING_STATUS;" +
           "Packing Delay (Days)|C|1|0|PACKING_DELAY;" +



           "Vanning Plan                |C|1|0|ACTUAL_VANNING_PLAN_DT;" +
           "Vanning Actual              |C|1|0|ACTUAL_VANNING_DT;" +
           "Vanning Qty         |C|1|0|VANNING_QTY;" +
           "Vanning Status      |C|1|0|VANNING_STATUS;" +
           "Vanning Delay (Days)|C|1|0|VANNING_DELAY;" +



           "Shipment Plan                |C|1|0|ACTUAL_SHIPMENT_PLAN_DT;" +
           "Shipment Actual              |C|1|0|ACTUAL_SHIPMENT_DT;" +
           "Shipment Qty        |C|1|0|SHIPMENT_STATUS;" +
           "Shipment Status     |C|1|0|SHIPMENT_STATUS;" +
           "Shipment Delay (Days)|C|1|0|SHIPMENT_DELAY;" +
           "ETD                 |C|1|0|ETD;"
           );
        //remak agi 2017-12-14 
        //private List<CsvColumn> CsvCol_DownloadByTick = CsvColumn.Parse(
        //   "Order Date|C|1|0|TMAP_ORDER_DT;" +
        //   "Buyer PD|C|6|1|BUYERPD_CD;" +
        //   "Order Type|C|10|0|ORDER_TYPE;" +
        //   "Tranp. Code|C|5|0|TRANSPORTATION_CD;" +
        //   "Order No|C|23|0|ORDER_NO;" +
        //   "Item No|C|23|1|ITEM_NO;" +
        //   "Part No|C|5|0|PART_NO;" +
        //   "Order Qty|C|1|0|ACCEPT_QTY;" +

        //   "Kanban ID|C|1|0|KANBAN_ID;" +
        //   "Rcv Date|C|1|0|RECEIVE_DT;" +

        //   "Case No|C|1|0|CASE_NO;" +

        //   "Packing Date|C|1|0|PACKING_DT;" +
        //   "Container No|C|1|0|CONTAINER_NO;" +
        //   "Vanning Date|C|1|0|VANNING_DT;" +

        //   "Invoice No|C|1|0|INVOICE_NO;" +
        //   "Invoice Date|C|1|0|INVOICE_DT;" +
        //   "ETD|C|1|0|SHIPMENT_DT;" 
        //   );
        private List<CsvColumn> CsvCol_DownloadByTick = CsvColumn.Parse(
            "TMMIN Send Date|C|1|0|TMMIN_SEND_DT;" +
            "Order Date|C|1|0|TMAP_ORDER_DT;" +
            "Buyer PD|C|6|1|BUYERPD_CD;" +
            "Order Type|C|10|0|ORDER_TYPE;" +
            "Tranp. Code|C|5|0|TRANSPORTATION_CD;" +
            "Order No|C|23|0|ORDER_NO;" +
            "Item No|C|23|1|ITEM_NO;" +
            "Part No|C|5|0|PART_NO;" +
            "Supplier Code|C|5|0|SUPPLIER_CD;" +
            "Sub Supplier Code|C|5|0|SUB_SUPPLIER_CD;" +
            "Manifest No|C|5|0|MANIFEST_NO;" +
            "Order Qty|C|1|0|ACCEPT_QTY;" +
            "Kanban ID|C|1|0|KANBAN_ID;" +
            "M/F Rcv Date|C|1|0|MF_RECEIVE_DT;" +
            //"M/F Rcv Qty|C|1|0|MF_RECEIVE_QTY;" +
            "M/F Rcv Status|C|1|0|MF_RECEIVE_STATUS;" +
            "M/F Rcv Delay (Days)|C|2|0|MF_RECEIVE_DELAY;" +
            "Allocate Qty|C|1|0|ALLOCATE_QTY;" +
            //2018-12-12 as request TMMIN.Hesti
            "Picking Date|C|1|0|PICKING_DT;" +
            "Picking Qty|C|1|0|PICKING_QTY;" +
            "Sorting Plan|C|1|0|RECEIVE_PLAN_DT;" +
            "Sorting Date|C|1|0|RECEIVE_DT;" +
            "Sorting Qty|C|1|0|RECEIVE_QTY;" +
            "Sorting Status|C|1|0|RECEIVE_STATUS;" +
            "Sorting Delay (Days)|C|2|0|RECEIVE_DELAY;" +
            "Case No|C|1|0|CASE_NO;" +
            "Packing Plan|C|1|0|PACKING_PLAN_DT;" +
            "Packing Date|C|1|0|PACKING_DT;" +
            "Packing Qty|C|1|0|PACKING_QTY;" +
            "Packing Status|C|1|0|PACKING_STATUS;" +
            "Packing Delay (Days)|C|1|0|PACKING_DELAY;" +
            "Container No|C|1|0|CONTAINER_NO;" +
            "Vanning Plan|C|1|0|VANNING_PLAN_DT;" +
            "Vanning Date|C|1|0|VANNING_DT;" +
            "Vanning Qty|C|1|0|VANNING_QTY;" +
            "Vanning Status|C|1|0|VANNING_STATUS;" +
            "Vanning Delay (Days)|C|1|0|VANNING_DELAY;" +
            "Invoice No|C|1|0|INVOICE_NO;" +
            "Invoice Date|C|1|0|INVOICE_DT;" +
            "Shipment Plan|C|1|0|SHIPMENT_PLAN_DT;" +
            "ETD|C|1|0|SHIPMENT_DT;" +
            "Shipment Qty|C|1|0|SHIPMENT_STATUS;" +
            "Shipment Status|C|1|0|SHIPMENT_STATUS;" +
            "Shipment Delay (Days)|C|1|0|SHIPMENT_DELAY;"
            );

        public class CsvColumn
        {
            public string Text { get; set; }
            public string Column { get; set; }
            public string DataType { get; set; }
            public int Len { get; set; }
            public int Mandatory { get; set; }

            public static List<CsvColumn> Parse(string v)
            {
                List<CsvColumn> l = new List<CsvColumn>();

                string[] x = v.Split(';');


                for (int i = 0; i < x.Length; i++)
                {
                    string[] y = x[i].Split('|');
                    CsvColumn me = null;
                    if (y.Length >= 4)
                    {
                        int len = 0;
                        int mandat = 0;
                        int.TryParse(y[2], out len);
                        int.TryParse(y[3], out mandat);
                        me = new CsvColumn()
                        {
                            Text = y[0],
                            DataType = y[1],
                            Len = len,
                            Mandatory = mandat
                        };
                        l.Add(me);
                    }

                    if (y.Length > 4 && me != null)
                    {
                        me.Column = y[4];
                    }
                }
                return l;
            }

        }

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        private string _csv_sep;
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {
                    _csv_sep = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ";";
                }
                return _csv_sep;
            }
        }

        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        private string Equs(string s)
        {
            string separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (s != null)
            {
                if (s.Contains(separator))
                {
                    s = "\"" + s + "\"";
                }
                else
                {
                    s = "=\"" + s + "\"";
                }
            }
            return s;
        }

        public ActionResult checkSystemMaster()
        {

            string[] r = null;

            string resultMessage = pli.checkSystemMaster();

            r = resultMessage.Split('|');

            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult linkQty(string pOrderNo, string pItemNo, string pPartNo, string linkType)
        {

            string[] r = null;

            string resultMessage = pli.linkQty(pOrderNo, pItemNo, pPartNo, linkType);

            r = resultMessage.Split('|');

            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", date_from = r[1].ToString(), date_to = r[2].ToString() }, JsonRequestBehavior.AllowGet);
            }

        }

        public void GetComboboxStockFlag()
        {
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            List<mSystemMaster> model = new List<mSystemMaster>();
            model = (new mSystemMaster()).GetComboBoxList("STOCK_FLAG");
            model.Insert(0, item);
            ViewData["STOCK_FLAG"] = model;
        }

        public void GetComboboxStatus()
        {
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            List<mSystemMaster> model = new List<mSystemMaster>();
            model = (new mSystemMaster()).GetComboBoxList("PIPELINE_STATUS_1");
            model.Insert(0, item);
            ViewData["PIPELINE_STATUS_1"] = model;
        }

        public void GetComboboxStatusProcess()
        {
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            List<mSystemMaster> model = new List<mSystemMaster>();
            model = (new mSystemMaster()).GetComboBoxList("PIPELINE_STATUS_PROCESS");
            model.Insert(0, item);
            ViewData["PIPELINE_STATUS_PROCESS"] = model;
        }

        public void GetComboboxTranpCode()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            model = pli.GetComboBoxTranpCode();
            model.Insert(0, item);
            ViewData["TRANP_CODE"] = model;
        }

        public void GetComboBoxBuyerPD()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            model = pli.GetComboBoxBuyerPD();
            model.Insert(0, item);
            ViewData["BUYER_PD"] = model;
        }

        public void GetComboBoxOrderType()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            model = pli.GetComboBoxOrderType();
            model.Insert(0, item);
            ViewData["ORDER_TYPE"] = model;
        }

    }
}

