﻿using SPEX.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers.Base
{
    public class SPEXBaseController : PageController
    {
        public static string USER = "User";

        public void validate(SPEXBaseModel inputParam)
        {
            string messageResult = "";
            var context = new ValidationContext(inputParam, null, null);
            var result = new List<ValidationResult>();
            if (!Validator.TryValidateObject(inputParam, context, result, true))
            {
                messageResult = result[0].ErrorMessage;
                messageResult = messageResult.Replace("The field ", "");
                throw new Exception(messageResult);
            }
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }
    }
}