﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Globalization;

namespace SPEX.Controllers
{
    public class PackingCompInquiryController : PageController
    {
        public static string ORDER_NO = "ORDER_NO";
        public static string ITEM_NO = "ITEM_NO";
        public static string PART_NO = "PART_NO";
        public static string ORDER_START_DATE = "ORDER_START_DATE";
        public static string ORDER_END_DATE = "ORDER_END_DATE";
        public static string REDIRECT_PAGE = "REDIRECT";
        Printer mPrinter = new Printer();

        public ActionResult Redirect(string orderNo, string itemNo, string partNo, string pStartOrdDt, string pEndOrdDt)
        {
            ViewData[REDIRECT_PAGE] = REDIRECT_PAGE;
            ViewData[ORDER_NO] = orderNo;
            ViewData[ITEM_NO] = itemNo;
            ViewData[PART_NO] = partNo;
            ViewData[ORDER_START_DATE] = pStartOrdDt;
            ViewData[ORDER_END_DATE] = pEndOrdDt;
            return Index();
        }

        #region Class Attributes
        MessagesString msgError = new MessagesString();
        public string moduleID = "SPX06";
        public string functionID = "SPX060400";
        mPackingCompInquiry dPackCompInq = new mPackingCompInquiry();
        mPackingCompInquiryParam pPackCompInq = new mPackingCompInquiryParam();
        mPackingCompInquiryDet dPackCompInqDet = new mPackingCompInquiryDet();
        mPackingMaintenance PMB = new mPackingMaintenance();
        mTransp dTrans = new mTransp();
        #endregion

        #region Startup Method
        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public PackingCompInquiryController()
        {
            Settings.Title = "Packing Completion Inquiry";
        }

        #region Get ComboBox Data
        public void getPrivilegeComboBox()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "";

            model = (new mSystemMaster()).GetComboBoxList("PACKING_PRIVILEDGE").OrderByDescending(i => i.SYSTEM_VALUE).ToList();
            model.Insert(0, item);
            ViewData["PRIVILEGE"] = model;
        }

        public void getStatusComboBox()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();
            mSystemMaster item = new mSystemMaster();
            string[] status = { "CANC" };
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "";

            model = (new mSystemMaster()).GetComboBoxList("PACKING_STATUS").OrderByDescending(i => i.SYSTEM_VALUE).ToList();
            model.Insert(0, item);
            model = model.Where(u => !status.Contains(u.SYSTEM_CD)).ToList();
            ViewData["STATUS"] = model;
        }

        public void getHandlingTypeComboBox()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "";

            model = (new mSystemMaster()).GetComboBoxList("PACKING_HANDLING_TYPE").OrderByDescending(i => i.SYSTEM_VALUE).ToList();
            model.Insert(0, item);
            ViewData["HANDLING_TYPE"] = model;
        }

        public void getTransportFrSystemComboBox()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "";

            model = (new mSystemMaster()).GetComboBoxList("PACKING_TRANSPORT_CD").OrderByDescending(i => i.SYSTEM_VALUE).ToList();
            model.Insert(0, item);
            ViewData["TRANSP_CODE"] = model;
        }

        public void getTransportComboBox()
        {
            List<mTransp> model = new List<mTransp>();
            mTransp item = new mTransp();
            item.TRANSPORTATION_CD = "";
            item.TRANSPORTATION_DESC = "";

            model = dTrans.GetTranspList();
            model.Insert(0, item);
            ViewData["TRANSP_CODE"] = model;
        }

        public void getBuyerPDComboBox()
        {
            List<String> model = new List<String>();
            model.Add("");
            model.AddRange(dPackCompInq.GetBuyerMasterList());
            ViewData["BUYER_PD"] = model;
        }
        #endregion

        protected override void Startup()
        {
            ViewData["PrinterCmb"] = mPrinter.getList();
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Packing Completion Inquiry");
            ViewBag.MSPXF0032ERR = msgError.getMSPXF0032ERR().Split('|')[2];
            ViewBag.MSPXF0044ERR = msgError.getMSPXF0044ERR().Split('|')[2];
            ViewBag.MSPXF0073ERR = msgError.getMSPXF0073ERR().Split('|')[2];
            getpE_UserId = (User)ViewData["User"];
            getPrivilegeComboBox();
            getStatusComboBox();
            getHandlingTypeComboBox();
            getTransportComboBox();
            getBuyerPDComboBox();
            //add agi 2017-10-24
            string range = new mSystemMaster().GetSystemValueList("packing inquiry", "range day", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            ViewBag.range = Convert.ToInt32(range);
            ViewBag.MSPXP0007ERR = msgError.getMsgText("MSPXP0007ERR").Replace("{0}", range);
            ViewBag.MSPXP0006ERR = msgError.getMsgText("MSPXP0006ERR");
            ViewBag.MSPXP0005ERR = msgError.getMsgText("MSPXP0005ERR");
            ViewBag.MSPXM0001ERR = msgError.getMsgText("MSPXM0001ERR").Replace("{0}", range);
        }
        #endregion

        #region Get Data
        public ActionResult getPackingCompInquiryCallBack(string pTMAPOrderNo, string pTMMINOrderNo, string pPartNo,
            string pOrderDTFr, string pOrderDTTo, string pCaseNo, string pHandlingType, string pTransCode,
            string pPackingDTFr, string pPackingDTTo, string pBuyerPD, string pPrivilege, string pContainerNo,
            string pVanningDTFr, string pVanningDTTo, string pItemNo, string pStatus, string pMode
            //ADD AGI 2017-10-25 PROCESS DATE
            , string pPROCESS_DATE_FROM, string pPROCESS_DATE_TO
            )
        {
            if (pOrderDTFr.Contains("."))
            {
                DateTime dtFrom = DateTime.ParseExact(pOrderDTFr.Replace('.','/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime dtTo = DateTime.ParseExact(pOrderDTTo.Replace('.', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                pOrderDTFr = dtFrom.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                pOrderDTTo = dtTo.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

                //add agi 2017-10-25
                #region process date
                DateTime PrdtFrom = DateTime.ParseExact(pPROCESS_DATE_FROM.Replace('.', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                pPROCESS_DATE_FROM = PrdtFrom.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

                DateTime PrdtTo = DateTime.ParseExact(pPROCESS_DATE_TO.Replace('.', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                pPROCESS_DATE_TO = PrdtTo.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                #endregion process date 

            }            

            List<mPackingCompInquiry> data = new List<mPackingCompInquiry>();
            List<mPackingCompInquiry> dataTransaction = new List<mPackingCompInquiry>();
            List<mPackingCompInquiry> dataHistory = new List<mPackingCompInquiry>();
            pPackCompInq.TMAP_ORDER_NO = pTMAPOrderNo;
            pPackCompInq.CASE_NO = pCaseNo;
            pPackCompInq.BUYER_PD = pBuyerPD;
            pPackCompInq.ITEM_NO = pItemNo;
            pPackCompInq.TMMIN_ORDER_NO = pTMMINOrderNo;
            pPackCompInq.HANDLING_TYPE = pHandlingType;
            pPackCompInq.PRIVILEGE = pPrivilege;
            pPackCompInq.STATUS = pStatus;
            pPackCompInq.PART_NO = pPartNo;
            pPackCompInq.TRANSP_CODE = pTransCode;
            pPackCompInq.CONTAINER_NO = pContainerNo;
            if (pMode.ToUpper().Equals("CLEAR"))
            {
                pPackCompInq.ORDER_DT_FR = pOrderDTFr;
                pPackCompInq.ORDER_DT_TO = pOrderDTTo;
                pPackCompInq.PACKING_DT_FR = pPackingDTFr;
                pPackCompInq.PACKING_DT_TO = pPackingDTTo;
                pPackCompInq.VANNING_DT_FR = pVanningDTFr;
                pPackCompInq.VANNING_DT_TO = pVanningDTTo;
                //add agi 2017-10-25
                #region process date
                pPackCompInq.PROCESS_DT_FROM = pPROCESS_DATE_FROM;
                pPackCompInq.PROCESS_DT_TO = pPROCESS_DATE_TO;
                #endregion
            }
            else {
                pPackCompInq.ORDER_DT_FR = pOrderDTFr.Equals("1970-01-01") ? "" : pOrderDTFr;
                pPackCompInq.ORDER_DT_TO = pOrderDTTo.Equals("1970-01-01") ? "" : pOrderDTTo;
                pPackCompInq.PACKING_DT_FR = pPackingDTFr.Equals("1970-01-01") ? "" : pPackingDTFr;
                pPackCompInq.PACKING_DT_TO = pPackingDTTo.Equals("1970-01-01") ? "" : pPackingDTTo;
                pPackCompInq.VANNING_DT_FR = pVanningDTFr.Equals("1970-01-01") ? "" : pVanningDTFr;
                pPackCompInq.VANNING_DT_TO = pVanningDTTo.Equals("1970-01-01") ? "" : pVanningDTTo;

                pPackCompInq.PROCESS_DT_FROM = pPROCESS_DATE_FROM.Equals("1970-01-01") || string.IsNullOrEmpty(pPROCESS_DATE_FROM)  ? "" : pPROCESS_DATE_FROM;
                if (!string.IsNullOrEmpty(pPROCESS_DATE_TO))
                    pPackCompInq.PROCESS_DT_TO = pPROCESS_DATE_TO.Equals("1970-01-01") ? "" : pPROCESS_DATE_TO;
                else
                    pPackCompInq.PROCESS_DT_TO = "";
            }

            dataTransaction = dPackCompInq.GetData(pPackCompInq, pMode);
            //[R-20170825001 by FID.Arri] Removed history data
            //dataTransaction = dataTransaction.Select(c => { c.DATA_TYPE = "Transaction"; return c; }).ToList();
            //dataHistory = dPackCompInq.GetDataHistory(pPackCompInq, pMode);
            //dataHistory = dataHistory.Select(c => { c.DATA_TYPE = "History"; return c; }).ToList();
            //data.AddRange(dataHistory);
            //[R-20170825001 by FID.Arri] End
            data.AddRange(dataTransaction);
            return PartialView("_PackingCompInquiryGrid", data);
        }

        public ActionResult getPackingCompInquiryDet(string pCaseNo, string pCaseType, string pPackingDT, string pPreparedDT, string pKanbanID)
        {
            List<mPackingCompInquiryDet> data = new List<mPackingCompInquiryDet>();
            List<mPackingCompInquiryDet> dataTransaction = new List<mPackingCompInquiryDet>();
            List<mPackingCompInquiryDet> dataHistory = new List<mPackingCompInquiryDet>();
            dataTransaction = dPackCompInqDet.GetData(pCaseNo, pCaseType, pPreparedDT, pKanbanID);
            dataTransaction = dataTransaction.Select(c => { c.DET_DATA_TYPE = "Transaction"; c.DET_KANBAN_STATUS = ""; return c; }).ToList();
            //dataHistory = dPackCompInqDet.GetDataHistory(pCaseNo, pCaseType, pPackingDT, pKanbanID);
            //dataHistory = dataHistory.Select(c => { c.DET_DATA_TYPE = "History"; c.DET_KANBAN_STATUS = "Delete"; return c; }).ToList();
            data.AddRange(dataTransaction);
            data.AddRange(dataHistory);

            return PartialView("_PackingCompInquiryDetGrid", data);
        }

        public ActionResult getPopUpKanban(string pCaseNo, string pCaseType, string pPackingDT, string pPreparedDT, string pKanbanID)
        {
            List<mPackingCompInquiryDet> data = new List<mPackingCompInquiryDet>();
            List<mPackingCompInquiryDet> dataTransaction = new List<mPackingCompInquiryDet>();
            List<mPackingCompInquiryDet> dataHistory = new List<mPackingCompInquiryDet>();
            dataTransaction = dPackCompInqDet.GetData(pCaseNo, pCaseType, pPreparedDT, pKanbanID);
            dataTransaction = dataTransaction.Select(c => { c.DET_DATA_TYPE = "Transaction"; c.DET_KANBAN_STATUS = ""; return c; }).ToList();
            //dataHistory = dPackCompInqDet.GetDataHistory(pCaseNo, pCaseType, pPreparedDT, pKanbanID);
            //dataHistory = dataHistory.Select(c => { c.DET_DATA_TYPE = "History"; c.DET_KANBAN_STATUS = "Delete"; return c; }).ToList();
            data.AddRange(dataTransaction);
            data.AddRange(dataHistory);

            ViewData["SetCaseNo"] = pCaseNo;
            ViewData["SetCaseType"] = pCaseType;
            ViewData["SetPackingDT"] = pPackingDT;
            ViewData["SetPreparedDT"] = pPreparedDT;
            ViewData["SetKanbanID"] = pKanbanID;

            return PartialView("_PackingCompInquiryDet", data);

        }
        #endregion

        #region Export to CSV
        public void doDonwloadCSVPackingCompInquiry(string pTMAPOrderNo, string pTMMINOrderNo, string pPartNo,
            string pOrderDTFr, string pOrderDTTo, string pCaseNo, string pHandlingType, string pTransCode,
            string pPackingDTFr, string pPackingDTTo, string pBuyerPD, string pPrivilege, string pContainerNo,
            string pVanningDTFr, string pVanningDTTo, string pItemNo, string pStatus, string pMode
            , string pPROCESS_DATE_FROM, string pPROCESS_DATE_TO)
        {
            string filename = "";
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPEX_PACKING_COMPLETION_INQUIRY_" + date + ".csv";

            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            string dateNow = DateTime.Now.ToString();

            List<mPackingCompInquiryDownload> data = new List<mPackingCompInquiryDownload>();
            List<mPackingCompInquiryDownload> dataTransaction = new List<mPackingCompInquiryDownload>();
           // List<mPackingCompInquiry> dataHistory = new List<mPackingCompInquiry>();
            pPackCompInq.TMAP_ORDER_NO = pTMAPOrderNo;
            pPackCompInq.CASE_NO = pCaseNo;
            pPackCompInq.BUYER_PD = pBuyerPD;
            pPackCompInq.ITEM_NO = pItemNo;
            pPackCompInq.TMMIN_ORDER_NO = pTMMINOrderNo;
            pPackCompInq.HANDLING_TYPE = pHandlingType;
            pPackCompInq.PRIVILEGE = pPrivilege;
            pPackCompInq.STATUS = pStatus;
            pPackCompInq.PART_NO = pPartNo;
            pPackCompInq.TRANSP_CODE = pTransCode;
            pPackCompInq.CONTAINER_NO = pContainerNo;
            pPackCompInq.ORDER_DT_FR = pOrderDTFr.Equals("1970-01-01") ? "" : pOrderDTFr;
            pPackCompInq.ORDER_DT_TO = pOrderDTTo.Equals("1970-01-01") ? "" : pOrderDTTo;
            pPackCompInq.PACKING_DT_FR = pPackingDTFr.Equals("1970-01-01") ? "" : pPackingDTFr;
            pPackCompInq.PACKING_DT_TO = pPackingDTTo.Equals("1970-01-01") ? "" : pPackingDTTo;
            pPackCompInq.VANNING_DT_FR = pVanningDTFr.Equals("1970-01-01") ? "" : pVanningDTFr;
            pPackCompInq.VANNING_DT_TO = pVanningDTTo.Equals("1970-01-01") ? "" : pVanningDTTo;
            //add agi 2017-10-25
            #region process date
            pPackCompInq.PROCESS_DT_FROM = pPROCESS_DATE_FROM.Equals("1970-01-01") ? "" : pPROCESS_DATE_FROM;
            pPackCompInq.PROCESS_DT_TO = pPROCESS_DATE_TO.Equals("1970-01-01") ? "" : pPROCESS_DATE_TO;
            #endregion

            dataTransaction = dPackCompInq.GetDataDownload(pPackCompInq);
            //[R-20170825001 by FID.Arri] Removed history data
            //dataTransaction = dataTransaction.Select(c => { c.DATA_TYPE = "Transaction"; return c; }).ToList();
            //dataHistory = dPackCompInq.GetDataHistory(pPackCompInq, pMode);
            //dataHistory = dataHistory.Select(c => { c.DATA_TYPE = "History"; return c; }).ToList();
            //data.AddRange(dataHistory);
            //[R-20170825001 by FID.Arri] End
            data.AddRange(dataTransaction);

            StringBuilder gr = new StringBuilder("");

            if (data.Count > 0)
            {
                for (int i = 0; i < CsvCol.Count; i++)
                {
                    gr.Append(Quote(CsvCol[i].Text)); gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);
                int j = 1;
                foreach (mPackingCompInquiryDownload d in data)
                {
                    csvAddLine(gr, new string[] {
                    Equs(j.ToString()),
                    Equs(d.CASE_NO), 
                    Equs(d.CASE_TYPE), 
                    Equs(d.PACKING_DT), 
                    Equs(d.TMAP_ORDER_NO),
                    Equs(d.TMAP_ITEM_NO),
                    Equs(d.TMAP_PART_NO), 
                    Equs(d.PART_NO), 
                    Equs(d.PACKING_QTY),
                    Equs(d.TMAP_ORDER_DT), 
                    Equs(d.TMMIN_ORDER_NO),
                    Equs(d.TRANSP_CD),
                    Equs(d.BUYER_PD),
                    Equs(d.PRIVILEGE),
                    Equs(d.CASE_GW),
                    Equs(d.CASE_NW),
                    Equs(d.CASE_LENGTH), 
                    Equs(d.CASE_WIDTH),
                    Equs(d.CASE_HEIGHT),
                    Equs(d.CONTAINER_NO),
                    Equs(d.VANNING_DT)
                    //[R-20170825001 by FID.Arri] Removed history data
                    //,Equs(d.DATA_TYPE)
                    //[R-20170825001 by FID.Arri] End
                    
                    });
                    j++;
                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        private string _csv_sep;
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {
                    _csv_sep = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ";";
                }
                return _csv_sep;
            }
        }

        private List<CsvColumn> CsvCol = CsvColumn.Parse(
            "No|C|1|0|NO;" +
            "Case No|C|1|0|CASE_NO;" +
            "Case Type|C|6|1|CASE_TYPE;" +
            //"Prepare Date|C|10|0|PREPARED_DT;" +
            "Packing Date|C|10|0|PACKING_DT;" +
            "TMAP Order No|C|5|0|TMAP_ORDER_NO;" +
            "Item No|C|23|0|TMAP_ITEM_NO;" +
            "TMAP Part No|C|23|1|TMAP_PART_NO;" +
            "Part No|C|23|1|PART_NO;" +
            "Packing Qty|C|5|0|PACKING_QTY;" +
            "Order Date|C|1|0|TMAP_ORDER_DT;" +
            "TMMIN Order No|C|2|1|TMMIN_ORDER_NO;" +
            "Transp Code|C|2|1|TRANSP_CD;" +
            "Buyer PD|C|2|1|BUYER_PD;" +
            "Privilege|C|2|1|PRIVILEGE;" +
            "Case Gw|C|2|1|CASE_GW;" +
            "Case Nw|C|5|0|CASE_NW;" +
            "L|C|1|0|CASE_LENGTH;" +
            "W|C|2|1|CASE_WIDTH;" +
            "H|C|2|1|CASE_HEIGHT;" +
            "Container No|C|2|1|CONTAINER_NO;" +
            "Vanning Date|C|10|0|VANNING_DT;"
            //[R-20170825001 by FID.Arri] Removed history data
            //+ "Data Type|C|40|0|DATA_TYPE;"
            //[R-20170825001 by FID.Arri] End
            );

        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        /// <summary>
        /// put Equal Sign in front and quote char to prevent 'numerization' by Excel
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string Equs(string s)
        {
            string separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (s != null)
            {
                if (s.Contains(separator))
                {
                    s = "\"" + s + "\"";
                }
                else
                {
                    s = "=\"" + s + "\"";
                }
            }
            return s;
        }

        public class CsvColumn
        {
            public string Text { get; set; }
            public string Column { get; set; }
            public string DataType { get; set; }
            public int Len { get; set; }
            public int Mandatory { get; set; }

            public static List<CsvColumn> Parse(string v)
            {
                List<CsvColumn> l = new List<CsvColumn>();

                string[] x = v.Split(';');


                for (int i = 0; i < x.Length; i++)
                {
                    string[] y = x[i].Split('|');
                    CsvColumn me = null;
                    if (y.Length >= 4)
                    {
                        int len = 0;
                        int mandat = 0;
                        int.TryParse(y[2], out len);
                        int.TryParse(y[3], out mandat);
                        me = new CsvColumn()
                        {
                            Text = y[0],
                            DataType = y[1],
                            Len = len,
                            Mandatory = mandat
                        };
                        l.Add(me);
                    }

                    if (y.Length > 4 && me != null)
                    {
                        me.Column = y[4];
                    }
                }
                return l;
            }
        }
        #endregion

        #region Packing Maintenance Batch
        public JsonResult doReOpenCase(string pId)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;
            string result = PMB.doReOpenCase(pId, UserID);
            r = result.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                msg = r[1].ToString();
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                msg = r[1].ToString();
                return Json(new { success = "true", messages = msg }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult doDeleteCase(string pCaseNo, string pCaseType, string pPreparedDT)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            mPackingMaintenanceCase param = new mPackingMaintenanceCase();
            param.CASE_NO = pCaseNo;
            param.CASE_TYPE = pCaseType;
            param.PREPARED_DT = pPreparedDT;

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;
            string result = PMB.doDeleteCase(param, UserID);
            r = result.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                msg = r[1].ToString();
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                msg = r[1].ToString();
                return Json(new { success = "true", messages = msg }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult doCancelCase(string pId)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;
            string result = PMB.doCancelCase(pId, UserID);
            r = result.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                msg = r[1].ToString();
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                msg = r[1].ToString();
                return Json(new { success = "true", messages = msg }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult doRePrint(string pId, string printer)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;
            string result = PMB.doRePrintCase(pId, printer, UserID);
            r = result.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                msg = r[1].ToString();
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                msg = r[1].ToString();
                return Json(new { success = "true", messages = msg }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult doDeleteKanban(string pCaseNo, string pCaseType, string pPreparedDT, string pKanbanID)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            mPackingMaintenanceKanban param = new mPackingMaintenanceKanban();
            param.CASE_NO = pCaseNo;
            param.CASE_TYPE = pCaseType;
            param.PREPARED_DT = pPreparedDT;
            param.KANBAN_ID = pKanbanID;

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;
            string result = PMB.doDeleteKanban(param, UserID);
            r = result.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                msg = r[1].ToString();
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                msg = r[1].ToString();
                return Json(new { success = "true", messages = msg }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult doCancelKanban(string pCaseNo, string pCaseType, string pPreparedDT, string pKanbanID)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            mPackingMaintenanceKanban param = new mPackingMaintenanceKanban();
            param.CASE_NO = pCaseNo;
            param.CASE_TYPE = pCaseType;
            param.PREPARED_DT = pPreparedDT;
            param.KANBAN_ID = pKanbanID;

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;
            string result = PMB.doCancelKanban(param, UserID);
            r = result.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                msg = r[1].ToString();
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                msg = r[1].ToString();
                return Json(new { success = "true", messages = msg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}