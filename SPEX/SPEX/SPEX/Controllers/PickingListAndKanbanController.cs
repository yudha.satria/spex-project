﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class PickingListAndKanbanController : PageController
    {
        //
        // GET: /VanningInstructionByAir/

        public PickingListAndKanbanController()
        {
            Settings.Title = "Picking List And Kanban";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            MessagesString msgError = new MessagesString();
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Picking List And Kanban");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        
        private ActionResult GeneratePackingListKanban()
        {
            string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("ddMMyyyy");
            mTelerik report;
            String source = Server.MapPath("~/Report/Picking_List_And_Kanban.trdx");
            FileInfo fileInfo = new FileInfo(source);

            pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
            Directory.CreateDirectory(pdfFolderPath);

            pdfFilename = "PICKING_LIST_" + date;
            if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                System.IO.File.Delete(pdfFolderPath + pdfFilename);

            report = new mTelerik(source, pdfFilename);

            report.SetConnectionString(Common.GetTelerikConnectionString());
            Dictionary<string, string> uriParam = new Dictionary<string, string>();


            report.SetSubConnectionString(uriParam, Common.GetTelerikConnectionString());

            using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
            {
                byte[] info = report.GeneratePDF();
                fs.Write(info, 0, info.Length);
            }

            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenaretePDFVanning()
        {
            try
            {
                string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("ddMMyyyy");
                mTelerik report;
                //model.APPROVED_BY = HttpContext.Request.MapPath(model.APPROVED_BY);
                String source = Server.MapPath("~/Report/Picking_List_And_Kanban.trdx");
                FileInfo fileInfo = new FileInfo(source);

                pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
                Directory.CreateDirectory(pdfFolderPath);

                //DeleteOlderFile(pdfFolderPath);

                pdfFilename = "PICKING_LIST_" + date;
                if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                    System.IO.File.Delete(pdfFolderPath + pdfFilename);

                report = new mTelerik(source, pdfFilename);
                report.AddParameters("PICKING_LIST", "P117000208");
                string path = @"Data Source=10.165.8.80\MSSQLServer17;Initial Catalog=SPEX_DB;Persist Security Info=True;User ID=sa;Password=fid123!!";
                report.SetConnectionString(path);
                
                
                using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
                {
                    byte[] info = report.GeneratePDF();
                    fs.Write(info, 0, info.Length);
                }
                return Json(new { success = "true", messages = "sukses", FILE_NAME = report.ResultName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message, FILE_NAME = "" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }


        public ActionResult GenaretePDFDataSource()
        {
            try
            {
                 string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("ddMMyyyy");
                    mTelerik report;
                    //model.APPROVED_BY = HttpContext.Request.MapPath(model.APPROVED_BY);
                    String source = Server.MapPath("~/Report/Data_source_of_Picking_List.trdx");
                    FileInfo fileInfo = new FileInfo(source);

                    pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
                    Directory.CreateDirectory(pdfFolderPath);

                    //DeleteOlderFile(pdfFolderPath);

                    pdfFilename = "SL_" + date;
                    if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                        System.IO.File.Delete(pdfFolderPath + pdfFilename);

                    report = new mTelerik(source, pdfFilename);
                    report.AddParameters("MANIFEST_NO", "P117000004");
                    report.AddParameters("KANBAN_ID", "P11700000400200001");

                    //report.AddParameters("VANNING_NO", VANNING_NO);
                    string path = @"Data Source=10.165.8.80\MSSQLServer17;Initial Catalog=SPEX_DB;Persist Security Info=True;User ID=sa;Password=fid123!!";
                    report.SetConnectionString(path);
                
                
                    using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
                    {
                        byte[] info = report.GeneratePDF();
                        fs.Write(info, 0, info.Length);
                    }
                    return Json(new { success = "true", messages = "sukses", FILE_NAME = report.ResultName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = "false", messages = ex.Message, FILE_NAME = "" }, JsonRequestBehavior.AllowGet);
                }
                return null;
            
            
        }
    }
}
