﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SPEX.Cls;
using Toyota.Common.Credential;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Toyota.Common.Web.Platform;
using System.IO.Compression;
using Ionic.Zip;
using SPEX.Models;
using NPOI.XSSF.UserModel;

namespace SPEX.Controllers
{
    public class PackingOrderController : PageController
    {
        mPackingOrder packingOrder = new mPackingOrder();
        MessagesString msgError = new MessagesString();
        Log log = new Log();

        public PackingOrderController()
        {
            Settings.Title = "Packing Order";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Packing Order");
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            ViewBag.MSPX00009INB = msgError.getMSPX00009INB("");
            ViewBag.MSPX00010INB = msgError.getMSPX00010INB("");
            getpE_UserId = (User)ViewData["User"];
            ComboSupplier();
        }

        public void ComboSupplier()
        {
            List<string> model = new List<string>();

            model = Common.GetSupplierCDPlantList();
            ViewData["SUPPLIER"] = model;
        }


        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
                //result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));
            }

            return result;
        }

        public string reFormatDate_Download(string dt)
        {
            //string result = "";

            //if (dt != "01.01.0100") result = string.Format("{0:dd.MM.yyyy}", DateTime.Parse(dt));
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
                //result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));
            }

            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PackingOrderCallBack(string p_PACKING_ORDER_NO, string p_TMAP_ORDER_NO, string p_SUPPLIER, string p_PACKING_ORDER_DT_FROM, string p_PACKING_ORDER_DT_TO, string p_TMMIN_ORDER_NO, string p_ORDER_GENERATE_STATUS)
        {
            List<mPackingOrder> model = new List<mPackingOrder>();

            p_PACKING_ORDER_DT_FROM = reFormatDate(p_PACKING_ORDER_DT_FROM);
            p_PACKING_ORDER_DT_TO = reFormatDate(p_PACKING_ORDER_DT_TO);

            model = packingOrder.getListPackingOrder(p_PACKING_ORDER_NO, p_TMAP_ORDER_NO, p_SUPPLIER, p_PACKING_ORDER_DT_FROM, p_PACKING_ORDER_DT_TO, p_TMMIN_ORDER_NO, p_ORDER_GENERATE_STATUS);
            return PartialView("PackingOrderGrid", model);
        }

        public void DownloadData_ByParameter(object sender, EventArgs e, string p_PACKING_ORDER_NO, string p_TMAP_ORDER_NO, string p_SUPPLIER, string p_PACKING_ORDER_DT_FROM, string p_PACKING_ORDER_DT_TO, string p_TMMIN_ORDER_NO, string p_ORDER_GENERATE_STATUS)
        {
            string filename = DateTime.Now.ToString("yyyyMMddhhmmss");

            p_PACKING_ORDER_DT_FROM = reFormatDate(p_PACKING_ORDER_DT_FROM);
            p_PACKING_ORDER_DT_TO = reFormatDate(p_PACKING_ORDER_DT_TO);

            string filesTmp = HttpContext.Request.MapPath("~/Template/PackingOrderDownload.xls");

            List<mPackingOrder> PINoList = new List<mPackingOrder>();

            PINoList = packingOrder.getPINoList(p_PACKING_ORDER_NO, p_TMAP_ORDER_NO, p_SUPPLIER, p_PACKING_ORDER_DT_FROM, p_PACKING_ORDER_DT_TO, p_TMMIN_ORDER_NO, p_ORDER_GENERATE_STATUS);
            Directory.CreateDirectory(HttpContext.Request.MapPath("~/Download/" + filename));

            foreach (mPackingOrder data in PINoList)
            {
                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

                //setting style
                ICellStyle styleContent = workbook.CreateCellStyle();
                styleContent.VerticalAlignment = VerticalAlignment.Top;
                styleContent.BorderLeft = BorderStyle.Thin;
                styleContent.BorderRight = BorderStyle.Thin;
                styleContent.BorderBottom = BorderStyle.Thin;
                styleContent.Alignment = HorizontalAlignment.Left;

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.Top;
                styleContent2.BorderLeft = BorderStyle.Thin;
                styleContent2.BorderRight = BorderStyle.Thin;
                styleContent2.BorderBottom = BorderStyle.Thin;
                styleContent2.Alignment = HorizontalAlignment.Center;

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.Top;
                styleContent3.BorderLeft = BorderStyle.Thin;
                styleContent3.BorderRight = BorderStyle.Thin;
                styleContent3.BorderBottom = BorderStyle.Thin;
                styleContent3.Alignment = HorizontalAlignment.Right;

                ISheet sheet = workbook.GetSheet("EmergencyOrder");

                int row = 2;
                IRow Hrow;

                List<mPackingOrder> models = packingOrder.getDetailDownload(data.PACKING_ORDER_NO);

                foreach (mPackingOrder result in models)
                {
                    Hrow = sheet.CreateRow(row);

                    Hrow.CreateCell(0).SetCellValue(result.PACKING_ORDER_NO);
                    Hrow.CreateCell(1).SetCellValue(result.TMMIN_ORDER_NO);
                    Hrow.CreateCell(2).SetCellValue(result.SUPPLIER_CD);
                    Hrow.CreateCell(3).SetCellValue(result.SUPPLIER_PLANT);
                    Hrow.CreateCell(4).SetCellValue(result.ARRIVAL_DOCK_CD);
                    Hrow.CreateCell(5).SetCellValue(result.KANBAN_NO);
                    Hrow.CreateCell(6).SetCellValue(result.PART_NO_DOWNLOAD);
                    Hrow.CreateCell(7).SetCellValue(result.TMMIN_ORDER_QTY);

                    Hrow.CreateCell(8).SetCellValue("");
                    Hrow.CreateCell(9).SetCellValue("");
                    Hrow.CreateCell(10).SetCellValue("");
                    Hrow.CreateCell(11).SetCellValue("");
                    Hrow.CreateCell(12).SetCellValue("");

                    Hrow.GetCell(1).CellStyle = styleContent2;
                    Hrow.GetCell(2).CellStyle = styleContent2;
                    Hrow.GetCell(3).CellStyle = styleContent2;
                    Hrow.GetCell(4).CellStyle = styleContent2;
                    Hrow.GetCell(5).CellStyle = styleContent2;
                    Hrow.GetCell(6).CellStyle = styleContent2;
                    Hrow.GetCell(7).CellStyle = styleContent3;
                    Hrow.GetCell(8).CellStyle = styleContent;
                    Hrow.GetCell(9).CellStyle = styleContent2;
                    Hrow.GetCell(10).CellStyle = styleContent;
                    Hrow.GetCell(11).CellStyle = styleContent2;
                    Hrow.GetCell(12).CellStyle = styleContent2;

                    row++;
                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();

                FileStream fs = new FileStream(HttpContext.Request.MapPath("~/Download/" + filename + "/") + data.PACKING_ORDER_NO + ".xls", FileMode.CreateNew, FileAccess.ReadWrite);
                workbook.Write(fs);
                fs.Close();

            }


            using (ZipFile zip = new ZipFile())
            {
                zip.AddDirectory(HttpContext.Request.MapPath("~/Download/" + filename));

                MemoryStream output = new MemoryStream();
                zip.Save(output);
                Response.BinaryWrite(output.ToArray());
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".zip");
            }

            if (Directory.Exists(HttpContext.Request.MapPath("~/Download/" + filename)))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(HttpContext.Request.MapPath("~/Download/" + filename));
                FileInfo[] fileInfos = dirInfo.GetFiles();
                foreach (FileInfo fileInfo in fileInfos)
                {
                    if (System.IO.File.Exists(fileInfo.FullName))
                        System.IO.File.Delete(fileInfo.FullName);
                }
                Directory.Delete(HttpContext.Request.MapPath("~/Download/" + filename));
            }
        }

        public void DownloadData_ByTicked(object sender, EventArgs e, string p_PACKING_ORDER_NOS)
        {
            string filename = DateTime.Now.ToString("yyyyMMddhhmmss");


            string filesTmp = HttpContext.Request.MapPath("~/Template/PackingOrderDownload.xls");

            List<string> PINoList = p_PACKING_ORDER_NOS.Split(';').ToList();

            //PINoList = PackingOrder.getPINoList(p_SUPPLIER, p_PI_DT);

            Directory.CreateDirectory(HttpContext.Request.MapPath("~/Download/" + filename));

            foreach (string PACKING_ORDER_NO in PINoList)
            {
                if (PACKING_ORDER_NO.Trim() == "") continue;

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

                //setting style
                ICellStyle styleContent = workbook.CreateCellStyle();
                styleContent.VerticalAlignment = VerticalAlignment.Top;
                styleContent.BorderLeft = BorderStyle.None;
                styleContent.BorderRight = BorderStyle.None;
                styleContent.BorderBottom = BorderStyle.None;
                styleContent.BorderTop = BorderStyle.None;
                styleContent.Alignment = HorizontalAlignment.Left;

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.Top;
                styleContent2.BorderLeft = BorderStyle.None;
                styleContent2.BorderRight = BorderStyle.None;
                styleContent2.BorderBottom = BorderStyle.None;
                styleContent2.BorderTop = BorderStyle.None;
                styleContent2.Alignment = HorizontalAlignment.Center;

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.Top;
                styleContent3.BorderLeft = BorderStyle.None;
                styleContent3.BorderRight = BorderStyle.None;
                styleContent3.BorderBottom = BorderStyle.None;
                styleContent3.BorderTop = BorderStyle.None;
                styleContent3.Alignment = HorizontalAlignment.Right;

                ISheet sheet = workbook.GetSheet("EmergencyOrder");

                int row = 2;
                IRow Hrow;

                List<mPackingOrder> models = packingOrder.getDetailDownload(PACKING_ORDER_NO);

                foreach (mPackingOrder result in models)
                {
                    Hrow = sheet.CreateRow(row);

                    Hrow.CreateCell(0).SetCellValue(result.PACKING_ORDER_NO);
                    Hrow.CreateCell(1).SetCellValue(result.TMMIN_ORDER_NO);
                    Hrow.CreateCell(2).SetCellValue(result.SUPPLIER_CD);
                    Hrow.CreateCell(3).SetCellValue(result.SUPPLIER_PLANT);
                    Hrow.CreateCell(4).SetCellValue(result.ARRIVAL_DOCK_CD);
                    Hrow.CreateCell(5).SetCellValue(result.KANBAN_NO);
                    Hrow.CreateCell(6).SetCellValue(result.PART_NO_DOWNLOAD);
                    Hrow.CreateCell(7).SetCellValue(result.TMMIN_ORDER_QTY);

                    Hrow.CreateCell(8).SetCellValue("");
                    Hrow.CreateCell(9).SetCellValue("");
                    Hrow.CreateCell(10).SetCellValue("");
                    Hrow.CreateCell(11).SetCellValue("");
                    Hrow.CreateCell(12).SetCellValue("");

                    Hrow.GetCell(1).CellStyle = styleContent2;
                    Hrow.GetCell(2).CellStyle = styleContent2;
                    Hrow.GetCell(3).CellStyle = styleContent2;
                    Hrow.GetCell(4).CellStyle = styleContent2;
                    Hrow.GetCell(5).CellStyle = styleContent2;
                    Hrow.GetCell(6).CellStyle = styleContent2;
                    Hrow.GetCell(7).CellStyle = styleContent3;
                    Hrow.GetCell(8).CellStyle = styleContent;
                    Hrow.GetCell(9).CellStyle = styleContent2;
                    Hrow.GetCell(10).CellStyle = styleContent;
                    Hrow.GetCell(11).CellStyle = styleContent2;
                    Hrow.GetCell(12).CellStyle = styleContent2;

                    row++;
                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();

                FileStream fs = new FileStream(HttpContext.Request.MapPath("~/Download/" + filename + "/") + PACKING_ORDER_NO + ".xls", FileMode.CreateNew, FileAccess.ReadWrite);
                workbook.Write(fs);
                fs.Close();

            }


            using (ZipFile zip = new ZipFile())
            {
                zip.AddDirectory(HttpContext.Request.MapPath("~/Download/" + filename));

                MemoryStream output = new MemoryStream();
                zip.Save(output);
                Response.BinaryWrite(output.ToArray());
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".zip");
            }

            if (Directory.Exists(HttpContext.Request.MapPath("~/Download/" + filename)))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(HttpContext.Request.MapPath("~/Download/" + filename));
                FileInfo[] fileInfos = dirInfo.GetFiles();
                foreach (FileInfo fileInfo in fileInfos)
                {
                    if (System.IO.File.Exists(fileInfo.FullName))
                        System.IO.File.Delete(fileInfo.FullName);
                }
                Directory.Delete(HttpContext.Request.MapPath("~/Download/" + filename));
            }
        }

        //Function Update for Adjust / Cancel
        public ActionResult UpdatePackingOrder(string p_PI_NO, string p_TMMIN_ORDER_NO)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string UserID = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = packingOrder.UpdateData(p_PI_NO, p_TMMIN_ORDER_NO, UserID);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public void GenerateOrderToSupplier(object sender, EventArgs e)
        {
            string filename = DateTime.Now.ToString("yyyyMMddhhmmss");


            string filesTmp = HttpContext.Request.MapPath("~/Template/PackingOrderEmergencyDownload.xls");

            List<mPackingOrder> PINoList = new List<mPackingOrder>();

            PINoList = packingOrder.getPINoGenerateOrderList();

            Directory.CreateDirectory(HttpContext.Request.MapPath("~/Download/" + filename));

            foreach (mPackingOrder data in PINoList)
            {
                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

                //setting style
                ICellStyle styleContent = workbook.CreateCellStyle();
                styleContent.VerticalAlignment = VerticalAlignment.Top;
                styleContent.BorderLeft = BorderStyle.Thin;
                styleContent.BorderRight = BorderStyle.Thin;
                styleContent.BorderBottom = BorderStyle.Thin;
                styleContent.Alignment = HorizontalAlignment.Left;

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.Top;
                styleContent2.BorderLeft = BorderStyle.Thin;
                styleContent2.BorderRight = BorderStyle.Thin;
                styleContent2.BorderBottom = BorderStyle.Thin;
                styleContent2.Alignment = HorizontalAlignment.Center;

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.Top;
                styleContent3.BorderLeft = BorderStyle.Thin;
                styleContent3.BorderRight = BorderStyle.Thin;
                styleContent3.BorderBottom = BorderStyle.Thin;
                styleContent3.Alignment = HorizontalAlignment.Right;

                ISheet sheet = workbook.GetSheet("EmergencyOrder");

                int row = 1;
                IRow Hrow;
                //Remaks agi 2016-12-06 request mrs hesty
                //List<mPackingOrder> models = packingOrder.getDetailDownload(data.PACKING_ORDER_NO);
                //add  agi 2016-12-06 request mrs hesty
                List<mPackingOrder> models = packingOrder.getDetailGenerate(data.PACKING_ORDER_NO);
                foreach (mPackingOrder result in models)
                {
                    Hrow = sheet.CreateRow(row);

                    Hrow.CreateCell(0).SetCellValue(result.SUPPLIER_CD);
                    Hrow.CreateCell(1).SetCellValue(result.SUPPLIER_PLANT);
                    Hrow.CreateCell(2).SetCellValue(result.ARRIVAL_DOCK_CD);
                    Hrow.CreateCell(3).SetCellValue(result.KANBAN_NO);
                    Hrow.CreateCell(4).SetCellValue(result.PART_NO_DOWNLOAD);
                    Hrow.CreateCell(5).SetCellValue(result.TMMIN_ORDER_QTY);

                    Hrow.CreateCell(6).SetCellValue("");
                    Hrow.CreateCell(7).SetCellValue("");
                    Hrow.CreateCell(8).SetCellValue("");
                    Hrow.CreateCell(9).SetCellValue("");
                    Hrow.CreateCell(10).SetCellValue("");

                    Hrow.GetCell(1).CellStyle = styleContent2;
                    Hrow.GetCell(2).CellStyle = styleContent2;
                    Hrow.GetCell(3).CellStyle = styleContent2;
                    Hrow.GetCell(4).CellStyle = styleContent2;
                    Hrow.GetCell(5).CellStyle = styleContent3;
                    Hrow.GetCell(6).CellStyle = styleContent;
                    Hrow.GetCell(7).CellStyle = styleContent2;
                    Hrow.GetCell(8).CellStyle = styleContent;
                    Hrow.GetCell(9).CellStyle = styleContent2;
                    Hrow.GetCell(10).CellStyle = styleContent2;

                    row++;
                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();

                FileStream fs = new FileStream(HttpContext.Request.MapPath("~/Download/" + filename + "/") + data.PACKING_ORDER_NO + ".xls", FileMode.CreateNew, FileAccess.ReadWrite);
                workbook.Write(fs);
                fs.Close();

            }

            packingOrder.GenerateToSupplier(getpE_UserId.Username);

            using (ZipFile zip = new ZipFile())
            {
                zip.AddDirectory(HttpContext.Request.MapPath("~/Download/" + filename));

                MemoryStream output = new MemoryStream();
                zip.Save(output);
                Response.BinaryWrite(output.ToArray());
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".zip");
            }

            if (Directory.Exists(HttpContext.Request.MapPath("~/Download/" + filename)))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(HttpContext.Request.MapPath("~/Download/" + filename));
                FileInfo[] fileInfos = dirInfo.GetFiles();
                foreach (FileInfo fileInfo in fileInfos)
                {
                    if (System.IO.File.Exists(fileInfo.FullName))
                        System.IO.File.Delete(fileInfo.FullName);
                }
                Directory.Delete(HttpContext.Request.MapPath("~/Download/" + filename));
            }
        }

        //add agi 2017-02-07 perubahan download packing order
        //List<mPackingOrder> PINoList = new List<mPackingOrder>();
        // PINoList = packingOrder.getPINoList(p_PACKING_ORDER_NO, p_TMAP_ORDER_NO, p_SUPPLIER, p_PACKING_ORDER_DT_FROM, p_PACKING_ORDER_DT_TO, p_TMMIN_ORDER_NO, p_ORDER_GENERATE_STATUS);
        public ActionResult DownloadPackingOrderByParameter(string p_PACKING_ORDER_NO, string p_TMAP_ORDER_NO, string p_SUPPLIER, string p_PACKING_ORDER_DT_FROM, string p_PACKING_ORDER_DT_TO, string p_TMMIN_ORDER_NO, string p_ORDER_GENERATE_STATUS)
        {
            p_PACKING_ORDER_DT_FROM = reFormatDate(p_PACKING_ORDER_DT_FROM);
            p_PACKING_ORDER_DT_TO = reFormatDate(p_PACKING_ORDER_DT_TO);
            List<string> PINoList = new List<string>();
            mPackingOrder PO = new mPackingOrder();
            PINoList = PO.getPackingOrderNo(p_PACKING_ORDER_NO, p_TMAP_ORDER_NO, p_SUPPLIER, p_PACKING_ORDER_DT_FROM, p_PACKING_ORDER_DT_TO, p_TMMIN_ORDER_NO, p_ORDER_GENERATE_STATUS);
            return DownloadPackingOrder(PINoList);

        }
        
        public ActionResult  DownloadPackingOrder( List<string> p_PACKING_ORDER_NO)
        {
            try
            {
                string filename = "Packing_order_download_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
                string filesTmp = HttpContext.Request.MapPath("~/Template/PACKING_ORDER_DOWNLOAD.xlsx");
                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);
                XSSFWorkbook workbook = new XSSFWorkbook(ftmp);
                SetPackingOrderInHouse(p_PACKING_ORDER_NO, workbook);
                SetPackingOrderOutHouse(p_PACKING_ORDER_NO, workbook);

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                FileStream fs = new FileStream(HttpContext.Request.MapPath("~/Content/ReportResult/" + filename), FileMode.CreateNew, FileAccess.Write);
                workbook.Write(fs);
                //byte[] bytes = new byte[ms.Length];
                //ms.Read(bytes, 0, (int)ms.Length);
                //fs.Write(bytes, 0, bytes.Length);
                fs.Close();
                ftmp.Close();
                //Response.BinaryWrite(ms.ToArray());
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
                
                return Json(new { success = "1", fileName = filename, messages = "" });
            }
            catch (Exception ex)
            {
                return Json(new { success = "0", fileName = "", messages = ex.Message });
            }

        }

        public void SetPackingOrderInHouse(List<string> p_PACKING_ORDER_NO, XSSFWorkbook workbook)
        {
            List<mPackingOrder> POs = new List<mPackingOrder>();
            mPackingOrder PO = new mPackingOrder();   
            POs = PO.getListPackingOrder_Download_InHouse(p_PACKING_ORDER_NO);
            ISheet sheet = workbook.GetSheet("IH");
            int row = 1;
            IRow Hrow;
            foreach(mPackingOrder P in POs)
            {
                Hrow = sheet.CreateRow(row);
                //PACKING_ORDER_NO
                if (!string.IsNullOrEmpty(P.PACKING_ORDER_NO))
                    Hrow.CreateCell(0).SetCellValue(P.PACKING_ORDER_NO);
                else
                    Hrow.CreateCell(0).SetCellType(CellType.Blank);
                //TMMIN_ORDER_NO
                if (!string.IsNullOrEmpty(P.TMMIN_ORDER_NO))
                    Hrow.CreateCell(1).SetCellValue(P.TMMIN_ORDER_NO);
                else
                    Hrow.CreateCell(1).SetCellType(CellType.Blank);
                //tmap_order_no
                if(!string.IsNullOrEmpty(P.TMAP_ORDER_NO))
                    Hrow.CreateCell(2).SetCellValue(P.TMAP_ORDER_NO);
                else
                    Hrow.CreateCell(2).SetCellType(CellType.Blank);
                //buyer pd
                if (!string.IsNullOrEmpty(P.BUYERPD_CD))
                    Hrow.CreateCell(3).SetCellValue(P.BUYERPD_CD);
                else
                    Hrow.CreateCell(3).SetCellType(CellType.Blank);
                //Supplier Code
                if (!string.IsNullOrEmpty(P.SUPPLIER_CD))
                    Hrow.CreateCell(4).SetCellValue(P.SUPPLIER_CD);
                else
                    Hrow.CreateCell(4).SetCellType(CellType.Blank);
                //Supplier Plant
                if (!string.IsNullOrEmpty(P.SUPPLIER_PLANT))
                    Hrow.CreateCell(5).SetCellValue(P.SUPPLIER_PLANT);
                else
                    Hrow.CreateCell(5).SetCellType(CellType.Blank);
                //Dock Code
                if (!string.IsNullOrEmpty(P.ARRIVAL_DOCK_CD))
                    Hrow.CreateCell(6).SetCellValue(P.ARRIVAL_DOCK_CD);
                else
                    Hrow.CreateCell(6).SetCellType(CellType.Blank);
                //Unique No
                if (!string.IsNullOrEmpty(P.KANBAN_NO))
                    Hrow.CreateCell(7).SetCellValue(P.KANBAN_NO);
                else
                    Hrow.CreateCell(7).SetCellType(CellType.Blank);
                //Part No
                if (!string.IsNullOrEmpty(P.PART_NO_DOWNLOAD))
                    Hrow.CreateCell(8).SetCellValue(P.PART_NO_DOWNLOAD);
                else
                    Hrow.CreateCell(8).SetCellType(CellType.Blank);
                //Kanban Qty                
                    Hrow.CreateCell(9).SetCellValue(P.TMMIN_ORDER_QTY);
                 //Address
                 if (!string.IsNullOrEmpty(P.ADDRESS))
                    Hrow.CreateCell(10).SetCellValue(P.ADDRESS);
                 else
                    Hrow.CreateCell(10).SetCellType(CellType.Blank);

                 //Importir Info 1
                 if (!string.IsNullOrEmpty(P.IMPORTIR_INFO_1))
                     Hrow.CreateCell(11).SetCellValue(P.IMPORTIR_INFO_1);
                 else
                     Hrow.CreateCell(11).SetCellType(CellType.Blank);
                 //Part Bar Code
                 if (!string.IsNullOrEmpty(P.PART_BARCODE))
                     Hrow.CreateCell(12).SetCellValue(P.PART_BARCODE);
                 else
                     Hrow.CreateCell(12).SetCellType(CellType.Blank);
                 //Progress Lane No
                 if (!string.IsNullOrEmpty(P.PROGRESS_LANE_NUMBER))
                     Hrow.CreateCell(13).SetCellValue(P.PROGRESS_LANE_NUMBER);
                 else
                     Hrow.CreateCell(13).SetCellType(CellType.Blank);
                 //Conveyance No
                 if (!string.IsNullOrEmpty(P.CONVAYANCE_NO))
                     Hrow.CreateCell(14).SetCellValue(P.CONVAYANCE_NO);
                 else
                     Hrow.CreateCell(14).SetCellType(CellType.Blank);
                row++;
            }

        }

        public void SetPackingOrderOutHouse(List<string> p_PACKING_ORDER_NO, XSSFWorkbook workbook)
        {
            List<mPackingOrder> POs = new List<mPackingOrder>();
            mPackingOrder PO = new mPackingOrder();
            POs = PO.getListPackingOrder_Download_OutHouse(p_PACKING_ORDER_NO);
            ISheet sheet = workbook.GetSheet("OH");
            int row = 1;
            IRow Hrow;
            foreach (mPackingOrder P in POs)
            {
                Hrow = sheet.CreateRow(row);
                //PACKING_ORDER_NO
                if (!string.IsNullOrEmpty(P.PACKING_ORDER_NO))
                    Hrow.CreateCell(0).SetCellValue(P.PACKING_ORDER_NO);
                else
                    Hrow.CreateCell(0).SetCellType(CellType.Blank);
                //TMMIN_ORDER_NO
                if (!string.IsNullOrEmpty(P.TMMIN_ORDER_NO))
                    Hrow.CreateCell(1).SetCellValue(P.TMMIN_ORDER_NO);
                else
                    Hrow.CreateCell(1).SetCellType(CellType.Blank);
                //tmap_order_no
                if (!string.IsNullOrEmpty(P.TMAP_ORDER_NO))
                    Hrow.CreateCell(2).SetCellValue(P.TMAP_ORDER_NO);
                else
                    Hrow.CreateCell(2).SetCellType(CellType.Blank);
                //buyer pd
                if (!string.IsNullOrEmpty(P.BUYERPD_CD))
                    Hrow.CreateCell(3).SetCellValue(P.BUYERPD_CD);
                else
                    Hrow.CreateCell(3).SetCellType(CellType.Blank);
                //Supplier Code
                if (!string.IsNullOrEmpty(P.SUPPLIER_CD))
                    Hrow.CreateCell(4).SetCellValue(P.SUPPLIER_CD);
                else
                    Hrow.CreateCell(4).SetCellType(CellType.Blank);
                //Supplier Plant
                if (!string.IsNullOrEmpty(P.SUPPLIER_PLANT))
                    Hrow.CreateCell(5).SetCellValue(P.SUPPLIER_PLANT);
                else
                    Hrow.CreateCell(5).SetCellType(CellType.Blank);
                //Dock Code
                if (!string.IsNullOrEmpty(P.ARRIVAL_DOCK_CD))
                    Hrow.CreateCell(6).SetCellValue(P.ARRIVAL_DOCK_CD);
                else
                    Hrow.CreateCell(6).SetCellType(CellType.Blank);
                //Unique No
                if (!string.IsNullOrEmpty(P.KANBAN_NO))
                    Hrow.CreateCell(7).SetCellValue(P.KANBAN_NO);
                else
                    Hrow.CreateCell(7).SetCellType(CellType.Blank);
                //Part No
                if (!string.IsNullOrEmpty(P.PART_NO_DOWNLOAD))
                    Hrow.CreateCell(8).SetCellValue(P.PART_NO_DOWNLOAD);
                else
                    Hrow.CreateCell(8).SetCellType(CellType.Blank);
                //Kanban Qty                
                Hrow.CreateCell(9).SetCellValue(P.TMMIN_ORDER_QTY);
                //Address
                if (!string.IsNullOrEmpty(P.ADDRESS))
                    Hrow.CreateCell(10).SetCellValue(P.ADDRESS);
                else
                    Hrow.CreateCell(10).SetCellType(CellType.Blank);

                //Importir Info 1
                if (!string.IsNullOrEmpty(P.IMPORTIR_INFO_1))
                    Hrow.CreateCell(11).SetCellValue(P.IMPORTIR_INFO_1);
                else
                    Hrow.CreateCell(11).SetCellType(CellType.Blank);
                //Part Bar Code
                if (!string.IsNullOrEmpty(P.PART_BARCODE))
                    Hrow.CreateCell(12).SetCellValue(P.PART_BARCODE);
                else
                    Hrow.CreateCell(12).SetCellType(CellType.Blank);
                //Progress Lane No
                if (!string.IsNullOrEmpty(P.PROGRESS_LANE_NUMBER))
                    Hrow.CreateCell(13).SetCellValue(P.PROGRESS_LANE_NUMBER);
                else
                    Hrow.CreateCell(13).SetCellType(CellType.Blank);
                //Conveyance No
                if (!string.IsNullOrEmpty(P.CONVAYANCE_NO))
                    Hrow.CreateCell(14).SetCellValue(P.CONVAYANCE_NO);
                else
                    Hrow.CreateCell(14).SetCellType(CellType.Blank);
                row++;
            }

        }
        [HttpGet]
        [DeleteFileAttribute]
        public ActionResult Download(string file)
        {
            //get the temp folder and file path in server
            string fullPath = Path.Combine(Server.MapPath("~/Content/ReportResult/"),file);

            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"
            return File(fullPath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", file);
        }

    }
    public class DeleteFileAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Flush();

            //convert the current filter context to file and get the file path
            string filePath = (filterContext.Result as FilePathResult).FileName;

            //delete the file after download
            System.IO.File.Delete(filePath);
        }
    }
}