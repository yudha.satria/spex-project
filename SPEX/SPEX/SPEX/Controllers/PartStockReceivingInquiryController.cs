﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;
using NPOI.HSSF.UserModel;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class PartStockReceivingInquiryController : PageController
    {

        //Model Maintenance Warehouse Master
        mPartStockReceivingInquiry ms = new mPartStockReceivingInquiry();
        MessagesString msgError = new MessagesString();


        public PartStockReceivingInquiryController()
        {
            Settings.Title = "Part Stock Receiving (Binning) Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }


        public void GetComboboxStatus()
        {
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            List<mSystemMaster> model = new List<mSystemMaster>();
            model = (new mSystemMaster()).GetComboBoxList("BINNING_STATUS");
            model.Insert(0, item);

            ViewData["PICKING_LIST_FLAG"] = model;
        }

        protected override void Startup()
        {

            //Call error message

            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();

            /*No data found*/
            ViewBag.MSPX00001ERR = msgError.getMsgText("MSPXS2006ERR");
            /*Cannot find template file {0} on folder {1}.*/
            ViewBag.MSPXS2034ERR = msgError.getMsgText("MSPXS2034ERR");
            /*Error writing file = {0}. Reason = {1}.*/
            ViewBag.MSPXS2035ERR = msgError.getMsgText("MSPXS2035ERR");
            /*Are you sure you want to download this data?*/
            ViewBag.MSPXS2021INF = msgError.getMsgText("MSPXS2021INF"); 

            getpE_UserId = (User)ViewData["User"]; 
            GetComboboxStatus();

        }

        public ActionResult PartStockReceivingInquiryCallBack(string pORDER_NO, string pKANBAN_ID, string pBINNING_DATE, string pBINNING_DATE_TO, string pSUPPLIER_CODE, string pPART_NO, string pRELEASE_DATE, string pRELEASE_DATE_TO, string pSUB_SUPPLIER_CODE, string pPART_ADDRESS, string pSTATUS, string pSUB_MF_NO, string pMANIFEST, string pSUPPLIER_PLANT, string pSUB_SUPPLIER_PLANT, string Mode)
        {
            List<mPartStockReceivingInquiry> model = new List<mPartStockReceivingInquiry>();
            if (Mode.Equals("Search"))
            {
                pBINNING_DATE = reFormatDate(pBINNING_DATE);
                pBINNING_DATE_TO = reFormatDate(pBINNING_DATE_TO);
                pRELEASE_DATE = reFormatDate(pRELEASE_DATE);
                pRELEASE_DATE_TO = reFormatDate(pRELEASE_DATE_TO);
                model = ms.getListPartStockReceivingInquiry(pORDER_NO, pKANBAN_ID, pBINNING_DATE, pBINNING_DATE_TO, pSUPPLIER_CODE, pPART_NO, pRELEASE_DATE, pRELEASE_DATE_TO, pSUB_SUPPLIER_CODE, pPART_ADDRESS, pSTATUS, pSUB_MF_NO, pMANIFEST, pSUPPLIER_PLANT, pSUB_SUPPLIER_PLANT);
            }
            GetComboboxStatus();
            return PartialView("PartStockReceivingInquiryGrid", model);
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }

            return result;
        }

        #region Function Download
        //function download 
        public ActionResult DownloadPartStockReceivingInquiry(object sender, EventArgs e, string pORDER_NO, string pKANBAN_ID, string pBINNING_DATE, string pBINNING_DATE_TO, string pSUPPLIER_CODE, string pPART_NO, string pRELEASE_DATE, string pRELEASE_DATE_TO, string pSUB_SUPPLIER_CODE, string pPART_ADDRESS, string pSTATUS, string pSUB_MF_NO, string pMANIFEST, string pSUPPLIER_PLANT, string pSUB_SUPPLIER_PLANT)
        {
            string rExtract = string.Empty;
            string filename = "Part_Stock_Receiving_Inquiry_Download.xls";
            string directory = "Template";
            string filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);
            List<mPartStockReceivingInquiry> model = new List<mPartStockReceivingInquiry>();
            pBINNING_DATE = reFormatDate(pBINNING_DATE);
            pBINNING_DATE_TO = reFormatDate(pBINNING_DATE_TO);
            pRELEASE_DATE = reFormatDate(pRELEASE_DATE);
            pRELEASE_DATE_TO = reFormatDate(pRELEASE_DATE_TO);
            model = ms.getListPartStockReceivingInquiry(pORDER_NO, pKANBAN_ID, pBINNING_DATE, pBINNING_DATE_TO, pSUPPLIER_CODE, pPART_NO, pRELEASE_DATE, pRELEASE_DATE_TO, pSUB_SUPPLIER_CODE, pPART_ADDRESS, pSTATUS, pSUB_MF_NO, pMANIFEST, pSUPPLIER_PLANT, pSUB_SUPPLIER_PLANT);


            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            else if (model.Count() == 0)
            {
                lError = 1;
                status = "Data";
                rExtract = model.Count().ToString();
            }
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1)
                return Json(new { success = false, messages = rExtract, status = status }, JsonRequestBehavior.AllowGet);
            #endregion

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Part Stock Receiving Inquiry");
            string date = DateTime.Now.ToString("ddMMyyyyHHmm");
            filename = "Part_Stock_Receiving_Inquiry_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            int row = 10;
            int rowNum = 1;
            IRow Hrow;

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.ORDER_DATE);
                Hrow.CreateCell(3).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(4).SetCellValue(result.MANIFEST_NO);
                Hrow.CreateCell(5).SetCellValue(result.SUB_MANIFEST_NO);
                Hrow.CreateCell(6).SetCellValue(result.KANBAN_ID);
                Hrow.CreateCell(7).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(8).SetCellValue(result.PART_NO);
                Hrow.CreateCell(9).SetCellValue(result.PART_ADDRESS);
                Hrow.CreateCell(10).SetCellValue(result.PART_STATUS);
                Hrow.CreateCell(11).SetCellValue(result.BINNING_DATE);
                Hrow.CreateCell(12).SetCellValue(result.SUPPLIER_CODE);
                Hrow.CreateCell(13).SetCellValue(result.SUPPLIER_PLANT);
                Hrow.CreateCell(14).SetCellValue(result.SUB_SUPPLIER_CODE);
                Hrow.CreateCell(15).SetCellValue(result.SUB_SUPPLIER_PLANT);

                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            MemoryStream memory = new MemoryStream();
            workbook.Write(memory);
            ftmp.Close();
            Response.BinaryWrite(memory.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);

        }

        #endregion


    }
}
