﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DevExpress.Web.ASPxGridView;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class CalendarMasterController : PageController
    {
        //
        // GET: /BuyerPD/
        //mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();

        public CalendarMasterController()
        {
            Settings.Title = "Calender Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            
            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Calender Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
            SPEX.Cls.Common cmn = new SPEX.Cls.Common();
            List<string> Shifts=cmn.GetSystemMasterToList("CalenderMaster", "Shift", ";");
            ViewData["Shift"] = Shifts;// cmn.GetSystemMasterToList("CalenderMaster", "Shift", ";"); //SPEX.Cls.Common.;
            ViewData["January"] = DateTime.Now.Year.ToString() + "-01-01";
            ViewData["February"] = DateTime.Now.Year.ToString() + "-02-01";
            ViewData["March"] = DateTime.Now.Year.ToString() + "-03-01";
            ViewData["April"] = DateTime.Now.Year.ToString() + "-04-01";
            ViewData["May"] = DateTime.Now.Year.ToString() + "-05-01";
            ViewData["June"] = DateTime.Now.Year.ToString() + "-06-01";
            ViewData["July"] = DateTime.Now.Year.ToString() + "-07-01";
            ViewData["August"] = DateTime.Now.Year.ToString() + "-08-01";
            ViewData["September"] = DateTime.Now.Year.ToString() + "-09-01";
            ViewData["October"] = DateTime.Now.Year.ToString() + "-10-01";
            ViewData["November"] = DateTime.Now.Year.ToString() + "-11-01";
            ViewData["December"] = DateTime.Now.Year.ToString() + "-12-01";
            mCalendarMaster cal = new mCalendarMaster();
            List<mCalendarMaster> Model = cal.GetHoliday(DateTime.Now.Year.ToString(), Shifts.First().ToString(), getpE_UserId.Username).ToList();
            List<mCalendarMaster_SumWorkingDay> WD = cal.GetSumWorkingDay(DateTime.Now.Year.ToString(), "Shift1");

            List<string> Year = cal.GetYear();
            ViewData["Year"] = Year;
            //Session.Add("Calenders", Model);
            //ViewData["WDmonth1"] = "working Day :" + WD.Where(wd => wd.Month == 1).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth2"] = "working Day :" + WD.Where(wd => wd.Month == 2).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth3"] = "working Day :" + WD.Where(wd => wd.Month == 3).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth4"] = "working Day :" + WD.Where(wd => wd.Month == 4).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth5"] = "working Day :" + WD.Where(wd => wd.Month == 5).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth6"] = "working Day :" + WD.Where(wd => wd.Month == 6).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth7"] = "working Day :" + WD.Where(wd => wd.Month == 7).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth8"] = "working Day :" + WD.Where(wd => wd.Month == 8).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth9"] = "working Day :" + WD.Where(wd => wd.Month == 9).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth10"] = "working Day :" + WD.Where(wd => wd.Month == 10).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth11"] = "working Day :" + WD.Where(wd => wd.Month == 11).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth12"] = "working Day :" + WD.Where(wd => wd.Month == 12).First().SumOfWorkingDay.ToString() + " days";

        }

        public ActionResult Search(string Year,string Shift)
        {
            mCalendarMaster cal = new mCalendarMaster();
            List<mCalendarMaster> Model = cal.GetHoliday(Year, Shift,getpE_UserId.Username).ToList();
            ViewData["Calenders"] = Model;
            ViewData["January"] = Year + "-01-01";
            ViewData["February"] = Year + "-02-01";
            ViewData["March"] = Year + "-03-01";
            ViewData["April"] = Year + "-04-01";
            ViewData["May"] = Year + "-05-01";
            ViewData["June"] = Year + "-06-01";
            ViewData["July"] = Year + "-07-01";
            ViewData["August"] = Year + "-08-01";
            ViewData["September"] = Year + "-09-01";
            ViewData["October"] = Year + "-10-01";
            ViewData["November"] = Year + "-11-01";
            ViewData["December"] = Year + "-12-01";
            List<mCalendarMaster_SumWorkingDay> WD = cal.GetSumWorkingDay(DateTime.Now.Year.ToString(),Shift);
            Session.Add("Calenders", Model);
            ViewData["WDmonth1"] ="Working Day :"+WD.Where(wd => wd.Month == 1).First().SumOfWorkingDay.ToString()+" days";
            ViewData["WDmonth2"] = "Working Day :" + WD.Where(wd => wd.Month == 2).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth3"] = "Working Day :" + WD.Where(wd => wd.Month == 3).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth4"] = "Working Day :" + WD.Where(wd => wd.Month == 4).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth5"] = "Working Day :" + WD.Where(wd => wd.Month == 5).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth6"] = "Working Day :" + WD.Where(wd => wd.Month == 6).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth7"] = "Working Day :" + WD.Where(wd => wd.Month == 7).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth8"] = "Working Day :" + WD.Where(wd => wd.Month == 8).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth9"] = "Working Day :" + WD.Where(wd => wd.Month == 9).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth10"] = "Working Day :" + WD.Where(wd => wd.Month == 10).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth11"] = "Working Day :" + WD.Where(wd => wd.Month == 11).First().SumOfWorkingDay.ToString() + " days";
            ViewData["WDmonth12"] = "Working Day :" + WD.Where(wd => wd.Month == 12).First().SumOfWorkingDay.ToString() + " days";
            return PartialView("_CallbackCalendar");
        }

        public ActionResult PopUpCalendar(string p_PART_NO)
        {
            SPEX.Cls.Common cmn = new SPEX.Cls.Common();
            List<string> Shifts = cmn.GetSystemMasterToList("CalenderMaster", "Shift", ";");
            ViewData["Shift"] = Shifts;
            //List<mCalendarMaster> WorkingDays = new List<mCalendarMaster>();
            //mCalendarMaster cl = new mCalendarMaster();
            //cl.WorkingDay=0;
            //cl.WorkingDayDescription = "Holiday";
            //WorkingDays.Add(cl);
            //cl = new mCalendarMaster();
            //cl.WorkingDayDescription = "Working Day";
            //cl.WorkingDay = 1;
            //WorkingDays.Add(cl);
            //ViewData["WorkingDays"] = WorkingDays;
            return PartialView("_CalendarMasterMaintenance");
        }

        public ActionResult SaveData(string DateFrom, string DateTo, List<string> Shifts, string WorkingDay, string Reason)
        {
            try
            {
                if (getpE_UserId == null)
                    return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

                mCalendarMaster M = new mCalendarMaster();
                string x = String.Join(", ", Shifts.ToArray());
                string Username=getpE_UserId.Username;
                string result = M.SaveData(DateFrom, DateTo, Shifts, WorkingDay, Reason,Username);
                if (result == "succes")
                {
                    return Json(new { success = "true", messages = result }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = "false", messages = result }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
            
        }

        //public ActionResult _CallbackCalendar()
        //{
        //    return PartialView();
        //}

        //public ActionResult _January()
        //{
        //    return null;
        //}

        //public ActionResult _AllMonth()
        //{
        //    return PartialView("_AllMonth");
        //}
    }
}
