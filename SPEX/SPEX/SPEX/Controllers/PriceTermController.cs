﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class PriceTermController : PageController
    {
        mPriceTerm PriceTerm = new mPriceTerm();
        MessagesString msgError = new MessagesString();
        User usr = new User();

        public PriceTermController()
        {
            Settings.Title = "Price Term Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            CallbackComboPriceTerm();

            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Price Term Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }
        
        public ActionResult PriceTermCallBack(string p_PRICE_TERM_CD, string p_DESCRIPTION1 )
        {
            List<mPriceTerm> model = new List<mPriceTerm>();

            {
                model = PriceTerm.getListPriceTerm
                (p_PRICE_TERM_CD, p_DESCRIPTION1 );
            }
            return PartialView("PriceTermGrid", model);
        }

        //get order type
        public void CallbackComboPriceTerm()
        {
            List<mPriceTerm> modelPriceTerm = PriceTerm.getComboPriceTerm();
            ViewData["ComboPriceTerm"] = modelPriceTerm;
        }

        //function download      
        public void DownloadPriceTerm(object sender, EventArgs e, string p_PRICE_TERM_CD, string p_DESCRIPTION1)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Price_Term_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Price Term");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "PriceTerm" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(8).GetCell(2).SetCellValue(p_PRICE_TERM_CD);
            sheet.GetRow(9).GetCell(2).SetCellValue(p_DESCRIPTION1);


            int row = 12;
            int rowNum = 1;
            IRow Hrow;

            List<mPriceTerm> model = new List<mPriceTerm>();

            model = PriceTerm.getListPriceTerm(p_PRICE_TERM_CD, p_DESCRIPTION1);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);
                
                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PRICE_TERM_CD);
                Hrow.CreateCell(3).SetCellValue(result.DESCRIPTION_1);
                Hrow.CreateCell(4).SetCellValue(result.DESCRIPTION_2);
                Hrow.CreateCell(5).SetCellValue(result.DELETION_FLAG);
                Hrow.CreateCell(6).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(7).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(8).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent;
                Hrow.GetCell(9).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        //Function Add
        public ActionResult AddNewPriceTerm(string p_PRICE_TERM_CD, string p_DESCRIPTION1, string p_DESCRIPTION2, string p_CREATED_BY)
        {
            p_CREATED_BY = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = PriceTerm.SaveData(p_PRICE_TERM_CD, p_DESCRIPTION1, p_DESCRIPTION2, p_CREATED_BY);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        //Function Edit - Update
        public ActionResult UpdatePriceTerm(string p_PRICE_TERM_CD, string p_DESCRIPTION1, string p_DESCRIPTION2, string p_DELETION_FLAG, string p_CHANGED_BY)
        {
            p_CHANGED_BY = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = PriceTerm.UpdateData(p_PRICE_TERM_CD, p_DESCRIPTION1, p_DESCRIPTION2, p_DELETION_FLAG, p_CHANGED_BY);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        //Fucntion Delete Data
        public ActionResult DeletePriceTerm(string p_PRICE_TERM_CD, string p_CHANGED_BY)
        {
            p_CHANGED_BY = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = PriceTerm.DeleteData(p_PRICE_TERM_CD, p_CHANGED_BY);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
    }
}
