﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class PORT_LOADINGController : PageController
    {
        //
        // GET: /BuyerPD/
        //mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();

        public PORT_LOADINGController()
        {
            Settings.Title = "Port Loading Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //CallbackComboPriceTerm();

            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("PORT LOADING Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        public ActionResult PORT_LOADINGCallBack(string PORT_LOADING_CD, string PORT_LOADING_NAME)
        {
            if (PORT_LOADING_CD != null)
            {
                if (PORT_LOADING_CD.Contains("\""))
                {
                    PORT_LOADING_CD = "";
                    PORT_LOADING_NAME = "";
                }
            }
            else
            {
                PORT_LOADING_CD = "XXX/XXXX";
                PORT_LOADING_CD = "XXX/XXXX";
            }

            PORT_LOADING PL = new PORT_LOADING();
            PL.PORT_LOADING_CD = PORT_LOADING_CD;
            PL.PORT_LOADING_NAME = PORT_LOADING_NAME;
            List<PORT_LOADING> model = new List<PORT_LOADING>();
            model = PL.getPORT_LOADING(PL);

            return PartialView("_PORT_LOADINGGrid", model);
        }

        public ActionResult DeletePORT_LOADING(string PORT_LOADING_CD)
        {
            PORT_LOADING PL = new PORT_LOADING();
            string resultMessage = string.Empty;
            string error = string.Empty;
            try
            {


                resultMessage = PL.DeleteData(PORT_LOADING_CD);

            }
            catch (Exception ex)
            {
                resultMessage = "Error";
                error = ex.Message;
            }

            if (resultMessage == "Error")
            {
                return Json(new { success = "false", messages = error }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = resultMessage }, JsonRequestBehavior.AllowGet);
            }

            return null;
        }
        //#endregion 

        // #region Function Add
        public ActionResult AddNewPORT_LOADING(string PORT_LOADING_CD, string PORT_LOADING_NAME)
        {
            //p_CREATED_BY = getpE_UserId.Username;
            //p_CREATED_BY = "SYSTEM";
            string[] r = null;
            PORT_LOADING PL = new PORT_LOADING();
            PL.PORT_LOADING_CD = PORT_LOADING_CD;
            PL.PORT_LOADING_NAME = PORT_LOADING_NAME;
            PL.CREATED_BY = getpE_UserId.Username;
            string resultMessage = PL.SaveData(PL);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion

        //#region Function Edit -
        public ActionResult UpdatePORT_LOADING(string PORT_LOADING_CD, string PORT_LOADING_NAME)
        {
            //string p_CHANGED_BY = getpE_UserId.Username;
            //pCHANGED_BY = "SYSTEM";
            string[] r = null;
            PORT_LOADING PL = new PORT_LOADING();
            PL.PORT_LOADING_CD = PORT_LOADING_CD;
            PL.PORT_LOADING_NAME = PORT_LOADING_NAME;
            PL.CHANGED_BY = getpE_UserId.Username;
            string resultMessage = PL.UpdateData(PL);

            //string resultMessage = BuyerPD.UpdateData(pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION, pDELETION_FLAG, pCHANGED_BY);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion
    }
}
