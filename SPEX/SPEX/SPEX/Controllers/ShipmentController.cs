﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class ShipmentController : PageController
    {
        mShipment Shipment = new mShipment();
        MessagesString messageError = new MessagesString();
        Log log = new Log();

        public ShipmentController()
        {
            Settings.Title = "Shipment History";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {

            ViewBag.MSPX00001ERR = messageError.getMSPX00001ERR("ETD Shipment History");
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        public string reFormatDateSearch(string dt)
        {
            string result = "";

            if (dt != "01.01.0100") result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));

            return result;
        }

        public ActionResult GridShipment(string pPartNo, string pOrderNo, string pItemNo, string pOrderDateFrom, string pOrderDateTo)
        {
            List<mShipment> model = new List<mShipment>();
            //pOrderDateFrom = reFormatDateSearch(pOrderDateFrom);
            //pOrderDateTo = reFormatDateSearch(pOrderDateTo);
            {
                model = Shipment.getListShipment
                (pPartNo, pOrderNo, pItemNo, pOrderDateFrom, pOrderDateTo);
            }
            return PartialView("GridShipment", model);
        }

        public void DownloadShipment(object sender, EventArgs e, string D_PartNo, string D_OrderNo, string D_ItemNo, string D_OrderDt, string D_OrderDtTo)
        {

            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/ETD_Shipment_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;

            ISheet sheet = workbook.GetSheet("ETDTracking");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "DownloadShipment_" + date + ".xls";
            //string judul = "TMMIN";
            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(3).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(9).GetCell(4).SetCellValue(D_PartNo);
            sheet.GetRow(10).GetCell(4).SetCellValue(D_OrderNo);
            sheet.GetRow(11).GetCell(4).SetCellValue(D_ItemNo);
            sheet.GetRow(12).GetCell(3).SetCellValue(D_OrderDt);
            sheet.GetRow(12).GetCell(5).SetCellValue(D_OrderDtTo);
            //sheet.GetRow(13).GetCell(4).SetCellValue(D_OrderDtTo);

            int row = 18;
            int rowNum = 1;
            IRow Hrow;

            List<mShipment> model = new List<mShipment>();
            model = Shipment.ShipmentDownload(D_PartNo, D_OrderNo, D_ItemNo, D_OrderDt, D_OrderDtTo);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.BUYERPD_CD);
                Hrow.CreateCell(3).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(4).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm}", result.ORDER_DT));
                Hrow.CreateCell(5).SetCellValue(result.PART_NO);
                Hrow.CreateCell(6).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(7).SetCellValue(result.ORDER_TYPE);
                Hrow.CreateCell(8).SetCellValue(result.TRANSPORTATION_CD);
                Hrow.CreateCell(9).SetCellValue(result.ACCEPT_QTY);
                Hrow.CreateCell(10).SetCellValue(result.SUPPLIED_PART_NO);
                Hrow.CreateCell(11).SetCellValue(result.SELLER_INV_NO);
                Hrow.CreateCell(12).SetCellValue(result.SHIPMENT_QTY);
                Hrow.CreateCell(13).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm}", result.SHIPMENT_DT));


                //sheet.AddMergedRegion(new CellRangeAddress(row, row, 3, 4));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                //Hrow.GetCell(3).IsMergedCell=
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent;


                row++;
                rowNum++;
            }
            //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
            //    sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }



        public void DownloadShipment_ByTicked(object sender, EventArgs e, string D_KeyDownload, string D_PartNo, string D_OrderNo, string D_ItemNo, string D_OrderDt, string D_OrderDtTo)
        {

            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/ETD_Shipment_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;

            ISheet sheet = workbook.GetSheet("ETDTracking");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "DownloadShipment_" + date + ".xls";
            //string judul = "TMMIN";
            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(3).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(9).GetCell(4).SetCellValue(D_PartNo);
            sheet.GetRow(10).GetCell(4).SetCellValue(D_OrderNo);
            sheet.GetRow(11).GetCell(4).SetCellValue(D_ItemNo);
            sheet.GetRow(12).GetCell(4).SetCellValue(D_OrderDt);
            //sheet.GetRow(13).GetCell(4).SetCellValue(D_OrderDtTo);

            int row = 18;
            int rowNum = 1;
            IRow Hrow;

            List<mShipment> model = new List<mShipment>();
            model = Shipment.ShipmentDownloadTicked(D_KeyDownload);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.BUYERPD_CD);
                Hrow.CreateCell(3).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(4).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm}", result.ORDER_DT));
                Hrow.CreateCell(5).SetCellValue(result.PART_NO);
                Hrow.CreateCell(6).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(7).SetCellValue(result.ORDER_TYPE);
                Hrow.CreateCell(8).SetCellValue(result.TRANSPORTATION_CD);
                Hrow.CreateCell(9).SetCellValue(result.ACCEPT_QTY);
                Hrow.CreateCell(10).SetCellValue(result.SUPPLIED_PART_NO);
                Hrow.CreateCell(11).SetCellValue(result.SELLER_INV_NO);
                Hrow.CreateCell(12).SetCellValue(result.SHIPMENT_QTY);
                Hrow.CreateCell(13).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm}", result.SHIPMENT_DT));


                //sheet.AddMergedRegion(new CellRangeAddress(row, row, 3, 4));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                //Hrow.GetCell(3).IsMergedCell=
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent;


                row++;
                rowNum++;
            }
            //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
            //    sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }


        
    }
}
