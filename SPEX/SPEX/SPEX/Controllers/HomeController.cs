﻿///
/// <author>lufty.abdillah@gmail.com</author>
/// <summary>
/// Toyota .Net Development Kit
/// Copyright (c) Toyota Motor Manufacturing Indonesia, All Right Reserved.
/// </summary>
/// 
using System;
using System.Linq;
using System.Web.Mvc;
using Toyota.Common.Credential.TMMIN;
using Toyota.Common.Web.Platform;
using System.Collections.Generic;
namespace SPEX.Controllers
{
    /*
     * By default, TDK will finds Home controller as the default controller.
     * You can override this behaviour at Global.asax as follows:
     *      ApplicationSettings.Instance.Runtime.HomeController = [Your desired controller name]
     */
    public class HomeController : PageController
    {
        /*
         * Write your basic settings here. 
         * Most of controller's basic settings can be defined at Startup(), 
         * but some of them may need to be defined at the constructor.
         * So shortly, just put any basic settings here.
         */ 

        public ActionResult TestPopUp(string p_PARAM1) {
            ViewBag.Param1 = p_PARAM1;
            return View();
        }

        /*
         * Write your startup codes here
         */ 
        protected override void Startup()
        {
            List<SPEX.Models.mSystemMaster> master = new SPEX.Models.mSystemMaster().GetSystemValueList("General", "SessionTimeout", "4", ";");
            Session.Timeout =Convert.ToInt32(master.FirstOrDefault().SYSTEM_VALUE);
            //Session.Timeout = new SPEX.Models.mSystemMaster().GetSystemValueList("General","SessionTimeout","4",";");
            Settings.Title = "Home";
            TmminUser u = Lookup.Get<TmminUser>();
        }
    }
}
