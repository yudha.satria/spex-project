﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class PartPriceMasterController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mPartPriceMaster partPrice = new mPartPriceMaster();

        public PartPriceMasterController()
        {
            Settings.Title = "Part Price Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PartPriceGridCallback(string pPART_NO, string pVALID_FROM, string pVALID_TO, string pLAST_PRICE)
        {
            List<mPartPriceMaster> model = new List<mPartPriceMaster>();

            model = partPrice.GetList(pPART_NO, GetDateSQL(pVALID_FROM).ToString(), GetDateSQL(pVALID_TO).ToString(), pLAST_PRICE);

            return PartialView("PartPriceGrid", model);
        }

        public ActionResult DeleteData(string pPART_NO, string pVALID_FROM)
        {
            string[] r = null;
            string msg = "";

            string resultMessage = partPrice.DeleteData(pPART_NO, GetDate(pVALID_FROM), getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddPartPrice(string pPART_NO, string pPRICE, string pCURRENCY, string pVALID_FROM, string pVALID_TO)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = partPrice.AddData(pPART_NO, pPRICE, pCURRENCY, GetDate(pVALID_FROM), GetDate(pVALID_TO), getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditPartPrice(string pPART_NO, string pVALID_FROM, string pVALID_TO)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = partPrice.EditData(pPART_NO, GetDate(pVALID_FROM), GetDate(pVALID_TO), getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNewValidFrom(string pPART_NO)
        {
            //string UserID = getpE_UserId.Username;
            string result = "";

            result = partPrice.GetNewValidFrom(pPART_NO);

            if (result == "NOT FOUND")
            {
                return Json(new { success = "false", messages = "PART NO NOT EXISTS", valid_from = result }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = "success", valid_from = result }, JsonRequestBehavior.AllowGet);
            }

        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        private string GetDateSQL(string dt)
        {
            if (dt == "01.01.0100")
                return "";

            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime date = new DateTime(year, month, day);

            return date.ToString("yyyy-MM-dd");
        }

        public ActionResult UploadBackgroundProsess()
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;

            string resultMessage = partPrice.UploadPartPrice(UserID);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = msgError.getMSPX00034INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00045INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }


        private string FunctionID = "F16-029b";
        private string ModuleID = "F16";
        public void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_PART_PRICE";
            #region createlog
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);

            string templateFileName = "TemplatePartPriceMaster";
            string UploadDirectory = Server.MapPath("~/Content/FileUploadResult");
            long ProcessID = 0;
            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock lockTable = new Lock();

            int IsLock = lockTable.is_lock(FunctionID, out ProcessID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(ProcessID.ToString());
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = partPrice.DeleteTempData();
                if (rExtract == "SUCCESS")
                {
                    string resultFilePath = UploadDirectory + templateFileName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, templateFileName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);
                    //ReadAndInsertExcelData(pathfile);
                    if (Path.GetExtension(e.UploadedFile.FileName).ToLower()==".xlsx")
                    {
                        ReadAndInsertExcelData(pathfile);
                    }
                    else if (Path.GetExtension(e.UploadedFile.FileName).ToLower()==".csv")
                    {
                        tb_t_name = "spex.TB_T_PART_PRICE_CSV";
                        rExtract = Upload.BulkCopyCSV(pathfile, tb_t_name,Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                        rExtract = partPrice.BulkInsertTempCSVToTempPartPrice();
                    }
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .xls OR xlsx";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        #region Upload function when user click button upload
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, FileUploadComplete);
            return null;
        }
        #endregion

        #region cek format file to Upload  Price
        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplatePartPriceMaster";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".csv" },
                //MaxFileSize = 20971520
                MaxFileSize = 30000000
            };
        }
        #endregion

        public void DownloadAll(string pPART_NO, string pVALID_FROM, string pVALID_TO, string pLAST_PRICE)
        {
            try
            {
                string filename = "";
                string filesTmp = HttpContext.Request.MapPath("~/Template/PartPriceDownload.xlsx");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

                IDataFormat format = workbook.CreateDataFormat();

                ICellStyle styleDecimal = workbook.CreateCellStyle();
                styleDecimal.DataFormat = HSSFDataFormat.GetBuiltinFormat("##,##0.0####");
                styleDecimal.Alignment = HorizontalAlignment.Center;
                styleDecimal.BorderBottom = BorderStyle.Thin;
                styleDecimal.BorderTop = BorderStyle.Thin;
                styleDecimal.BorderLeft = BorderStyle.Thin;
                styleDecimal.BorderRight = BorderStyle.Thin;

                ICellStyle styleDate1 = workbook.CreateCellStyle();
                styleDate1.DataFormat = HSSFDataFormat.GetBuiltinFormat("dd/MM/yyyy HH:mm:ss");
                styleDate1.Alignment = HorizontalAlignment.Center;
                styleDate1.BorderBottom = BorderStyle.Thin;
                styleDate1.BorderTop = BorderStyle.Thin;
                styleDate1.BorderLeft = BorderStyle.Thin;
                styleDate1.BorderRight = BorderStyle.Thin;

                ICellStyle style1 = workbook.CreateCellStyle();
                style1.VerticalAlignment = VerticalAlignment.Center;
                style1.Alignment = HorizontalAlignment.Center;
                style1.BorderBottom = BorderStyle.Thin;
                style1.BorderTop = BorderStyle.Thin;
                style1.BorderLeft = BorderStyle.Thin;
                style1.BorderRight = BorderStyle.Thin;

                ICellStyle style2 = workbook.CreateCellStyle();
                style2.VerticalAlignment = VerticalAlignment.Center;
                style2.Alignment = HorizontalAlignment.Left;
                style2.BorderBottom = BorderStyle.Thin;
                style2.BorderRight = BorderStyle.Thin;
                style2.BorderLeft = BorderStyle.Thin;
                style2.BorderRight = BorderStyle.None;


                ISheet sheetMain = workbook.GetSheet("PriceMaster");
                string date = DateTime.Now.ToString("ddMMyyyyHHmmss");
                filename = "PartPrice_" + date + ".xlsx";

                string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

                List<mPartPriceMaster> models = partPrice.GetList(pPART_NO, GetDateSQL(pVALID_FROM).ToString(), GetDateSQL(pVALID_TO).ToString(), pLAST_PRICE);
                int startRow = 12;
                IRow Hrow;

                Hrow = sheetMain.GetRow(5);
                Hrow.CreateCell(2).SetCellValue(DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                Hrow.GetCell(2).CellStyle = style2;
                Hrow = sheetMain.GetRow(6);
                Hrow.CreateCell(2).SetCellValue(getpE_UserId.Username);
                Hrow.GetCell(2).CellStyle = style2;

                foreach (mPartPriceMaster price in models)
                {
                    Hrow = sheetMain.CreateRow(startRow);
                    //Hrow.CreateCell(0).SetCellValue("");
                    Hrow.CreateCell(1).SetCellValue((startRow - 11).ToString());
                    Hrow.GetCell(1).CellStyle = style1;
                    Hrow.CreateCell(2).SetCellValue(price.PART_NO);
                    Hrow.GetCell(2).CellStyle = style1;
                    Hrow.CreateCell(3).SetCellValue(price.PRICE.ToString("##,###.######"));
                    Hrow.GetCell(3).CellStyle = style1;
                    Hrow.CreateCell(4).SetCellValue(price.CURRENCY_CD);
                    Hrow.GetCell(4).CellStyle = style1;
                    Hrow.CreateCell(5).SetCellValue(((DateTime)price.VALID_FROM).ToString("dd.MM.yyyy"));
                    Hrow.GetCell(5).CellStyle = style1;
                    Hrow.CreateCell(6).SetCellValue(((DateTime)price.VALID_TO).ToString("dd.MM.yyyy"));
                    Hrow.GetCell(6).CellStyle = style1;
                    Hrow.CreateCell(7).SetCellValue(price.DELETE_FLAG == null ? "" : price.DELETE_FLAG);
                    Hrow.GetCell(7).CellStyle = style1;
                    Hrow.CreateCell(8).SetCellValue(price.CREATED_BY);
                    Hrow.GetCell(8).CellStyle = style1;
                    //Hrow.CreateCell(9).SetCellValue(((DateTime)price.CREATED_DT).ToString("dd/MM/yyyy HH:mm:ss"));
                    //Hrow.GetCell(9).CellStyle = style1;
                    if (price.CREATED_DT != null)
                    {
                        Hrow.CreateCell(9).SetCellValue(((DateTime)price.CREATED_DT).ToString("dd.MM.yyyy HH:mm:ss"));
                        Hrow.GetCell(9).CellStyle = style1;
                    }
                    else
                    {
                        Hrow.CreateCell(9).SetCellValue(" ");
                        Hrow.GetCell(9).CellStyle = style1;
                    }
                    Hrow.CreateCell(10).SetCellValue(price.CHANGED_BY);
                    Hrow.GetCell(10).CellStyle = style1;
                    if (price.CHANGED_DT != null)
                    {
                        Hrow.CreateCell(11).SetCellValue(((DateTime)price.CHANGED_DT).ToString("dd.MM.yyyy HH:mm:ss"));
                        Hrow.GetCell(11).CellStyle = style1;
                    }
                    else
                    {
                        Hrow.CreateCell(11).SetCellValue(" ");
                        Hrow.GetCell(11).CellStyle = style1;
                    }

                    startRow++;
                }

                ExcelInsertHeaderImage(workbook, sheetMain, HttpContext.Request.MapPath("~/Content/images/excel_header.png"), 0);

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                Response.BinaryWrite(ms.ToArray());
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            }
            catch (Exception ex)
            {

            }
        }

        private void ExcelInsertHeaderImage(XSSFWorkbook workbook, ISheet sheet1, string path, int rowDest)
        {
            XSSFDrawing patriarch = (XSSFDrawing)sheet1.CreateDrawingPatriarch();
            XSSFClientAnchor anchor;
            anchor = new XSSFClientAnchor(0, 0, 0, 0, 1, rowDest, 2, rowDest);
            anchor.AnchorType = (AnchorType)2;
            //load the picture and get the picture index in the workbook
            XSSFPicture picture = (XSSFPicture)patriarch.CreatePicture(anchor, ExcelLoadImage(path, workbook));
            //Reset the image to the original size.
            picture.Resize();
            //picture.LineStyle = HSSFPicture.LINESTYLE_DASHDOTGEL;
        }

        public int ExcelLoadImage(string path, XSSFWorkbook workbook)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, (int)file.Length);
            return workbook.AddPicture(buffer, PictureType.JPEG);

        }

        private string ReadAndInsertExcelData(string FileName)
        {
            try
            {
                XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                ISheet sheet = xssfworkbook.GetSheetAt(0);
                int lastrow = sheet.LastRowNum;
                List<mPartPriceMaster.mPartPriceMasterUpload> listPrice = new List<mPartPriceMaster.mPartPriceMasterUpload>();
                int index = 1;

                mPartPriceMaster.mPartPriceMasterUpload priceUpload;
                for (int i = index; i <= lastrow; i++)
                {
                    priceUpload = new mPartPriceMaster.mPartPriceMasterUpload();
                    IRow row = sheet.GetRow(i);
                    

                    if (row == null) continue;
                    if ((row.GetCell(0) == null || row.GetCell(0).StringCellValue != "D") || (row.GetCell(1) == null && row.GetCell(2) == null && row.GetCell(3) == null && row.GetCell(4) == null)) continue;

                    //part no
                    if (row.GetCell(1) == null) priceUpload.PART_NO = "";
                    else if (row.GetCell(1).CellType == CellType.Blank || (row.GetCell(1).CellType == CellType.String && (row.GetCell(1).StringCellValue == null || row.GetCell(1).StringCellValue == "")))
                        priceUpload.PART_NO = "";
                    else if (row.GetCell(1).CellType == CellType.Numeric && (row.GetCell(1).NumericCellValue.ToString() == ""))
                        priceUpload.PART_NO = "";
                    else if (row.GetCell(1).CellType == CellType.String)
                        priceUpload.PART_NO = row.GetCell(1).StringCellValue;
                    else if (row.GetCell(1).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(1);
                        string formatString = row.GetCell(1).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            priceUpload.PART_NO = row.GetCell(1).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            priceUpload.PART_NO = row.GetCell(1).NumericCellValue.ToString();
                    }
                    else
                        priceUpload.PART_NO = row.GetCell(1).ToString();

                    //price
                    if (row.GetCell(2) == null) priceUpload.PRICE = "";
                    else if (row.GetCell(2).CellType == CellType.Blank || (row.GetCell(2).CellType == CellType.String && (row.GetCell(2).StringCellValue == null || row.GetCell(2).StringCellValue == "")))
                        priceUpload.PRICE = "";
                    else if (row.GetCell(2).CellType == CellType.Numeric && (row.GetCell(2).NumericCellValue.ToString() == ""))
                        priceUpload.PRICE = "";
                    else if (row.GetCell(2).CellType == CellType.String)
                        priceUpload.PRICE = row.GetCell(2).StringCellValue;
                    else if (row.GetCell(2).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(2);
                        string formatString = row.GetCell(2).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            priceUpload.PRICE = row.GetCell(2).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            priceUpload.PRICE = row.GetCell(2).NumericCellValue.ToString();
                    }
                    else
                        priceUpload.PRICE = row.GetCell(2).ToString();

                    //currency code
                    if (row.GetCell(3) == null) priceUpload.CURRENCY_CD = "";
                    else if (row.GetCell(3).CellType == CellType.Blank || (row.GetCell(3).CellType == CellType.String && (row.GetCell(3).StringCellValue == null || row.GetCell(3).StringCellValue == "")))
                        priceUpload.CURRENCY_CD = "";
                    else if (row.GetCell(3).CellType == CellType.Numeric && (row.GetCell(3).NumericCellValue.ToString() == ""))
                        priceUpload.CURRENCY_CD = "";
                    else if (row.GetCell(3).CellType == CellType.String)
                        priceUpload.CURRENCY_CD = row.GetCell(3).StringCellValue;
                    else if (row.GetCell(3).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(3);
                        string formatString = row.GetCell(3).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            priceUpload.CURRENCY_CD = row.GetCell(3).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            priceUpload.CURRENCY_CD = row.GetCell(3).NumericCellValue.ToString();
                    }
                    else
                        priceUpload.CURRENCY_CD = row.GetCell(3).ToString();

                    //valid from
                    if (row.GetCell(4) == null) priceUpload.VALID_FROM = "";
                    else if (row.GetCell(4).CellType == CellType.Blank || (row.GetCell(4).CellType == CellType.String && (row.GetCell(4).StringCellValue == null || row.GetCell(4).StringCellValue == "")))
                        priceUpload.VALID_FROM = "";
                    else if (row.GetCell(4).CellType == CellType.Numeric && (row.GetCell(4).NumericCellValue.ToString() == ""))
                        priceUpload.VALID_FROM = "";
                    else if (row.GetCell(4).CellType == CellType.String)
                        priceUpload.VALID_FROM = row.GetCell(4).StringCellValue;
                    else if (row.GetCell(4).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(4);
                        string formatString = row.GetCell(4).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            priceUpload.VALID_FROM = row.GetCell(4).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            priceUpload.VALID_FROM = row.GetCell(4).NumericCellValue.ToString();
                    }
                    else
                        priceUpload.VALID_FROM = row.GetCell(4).ToString();

                    //valid to
                    if (row.GetCell(5) == null) priceUpload.VALID_TO = "";
                    else if (row.GetCell(5).CellType == CellType.Blank || (row.GetCell(5).CellType == CellType.String && (row.GetCell(5).StringCellValue == null || row.GetCell(5).StringCellValue == "")))
                        priceUpload.VALID_TO = "";
                    else if (row.GetCell(5).CellType == CellType.Numeric && (row.GetCell(5).NumericCellValue.ToString() == ""))
                        priceUpload.VALID_TO = "";
                    else if (row.GetCell(5).CellType == CellType.String)
                        priceUpload.VALID_TO = row.GetCell(5).StringCellValue;
                    else if (row.GetCell(5).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(5);
                        string formatString = row.GetCell(5).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            priceUpload.VALID_TO = row.GetCell(5).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            priceUpload.VALID_TO = row.GetCell(5).NumericCellValue.ToString();
                    }
                    else
                        priceUpload.VALID_TO = row.GetCell(5).ToString();

                    listPrice.Add(priceUpload);
                }

                foreach (mPartPriceMaster.mPartPriceMasterUpload data in listPrice)
                {
                    string insertResult = (new mPartPriceMaster()).InsertTempPartPrice(data);
                }

                return "0|success";
            }
            catch (Exception ex)
            {
                return "1|" + ex.Message;
            }
        }

        
    }
}

