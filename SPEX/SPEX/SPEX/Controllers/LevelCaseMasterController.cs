﻿using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class LevelCaseMasterController : PageController
    {
        Log log = new Log();
        mBackOrder BackOrder = new mBackOrder();
        MessagesString msgError = new MessagesString();
        public string moduleID = "5";
        public string functionID = "50003";
        public const string UploadDirectory = "Content\\FileUploadResult";
        public const string TemplateFName = "UPLOAD_PACKING_PART_MASTER";
        public LevelCaseMasterController()
        {
            Settings.Title = "Level Case Master";
        }

        public Toyota.Common.Credential.User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Level Case Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            ViewBag.MSPX000018ERR = msgError.getMSPX00018ERR();
            getpE_UserId = (User)ViewData["User"];
            LevelCaseMaster lc = new LevelCaseMaster();
            List<mSystemMaster> HANDLING_TYPEs = lc.GetDataBySystemMaster("level_case", "HANDLING_TYPE"); //system.GetSystemValueList("ShutterMaster", "HANDLING_TYPE", "1", ";");
            List<Transportation> Transporations = new Transportation().GetAllTransportation();
            List<string> OrderTypes = new BUYER_ORDER_TYPE_MASTER().GetDistinctBUYER_ORDER_TYPE();
            List<mBuyerPD> BuyerPDs = new mBuyerPD().GetAllBuyerPDNoDelete();
            List<mSystemMaster> Finish_Case_Flags = lc.GetDataBySystemMaster("level_case", "Finish_Case_Flag");
            ViewData["HANDLING_TYPEs"] = HANDLING_TYPEs;
            ViewData["Transporations"] = Transporations;
            ViewData["OrderTypes"] = OrderTypes;
            ViewData["BuyerPDs"] = BuyerPDs;
            ViewData["Finish_Case_Flags"] = Finish_Case_Flags;            
        }

        public ActionResult LevelCaseMasterCallBack(string Data)
        {
            List<LevelCaseMaster> model = new List<LevelCaseMaster>();
            LevelCaseMaster lc = new LevelCaseMaster();
            lc = System.Web.Helpers.Json.Decode<LevelCaseMaster>(Data);
            model = lc.GetData(lc);
            return PartialView("_PartialMainGrid", model);
        }

        public ActionResult PopUp(string Data,string Mode)
        {
            LevelCaseMaster lc = new LevelCaseMaster();
            if (Mode=="Add")
            {
                lc.DELETION_FLAG = "N";
                ViewBag.Mode = "Add";
            }
            else
            {
                lc = System.Web.Helpers.Json.Decode<LevelCaseMaster>(Data);
                lc = lc.GetData(lc).FirstOrDefault();
                ViewBag.Mode = "Edit";
            }
            List<string> DeletionFlags = new List<string>();
            DeletionFlags.Add("Y");
            DeletionFlags.Add("N");
            ViewData["DeletionFlags"] = DeletionFlags;
            //List<mSystemMaster> HANDLING_TYPEs = lc.GetDataBySystemMaster("level_case", "HANDLING_TYPE"); //system.GetSystemValueList("ShutterMaster", "HANDLING_TYPE", "1", ";");
            //List<Transportation> Transporations = new Transportation().GetAllTransportation();
            //List<string> OrderTypes = new BUYER_ORDER_TYPE_MASTER().GetDistinctBUYER_ORDER_TYPE();
            //List<mBuyerPD> BuyerPDs = new mBuyerPD().GetAllBuyerPDNoDelete();
            //List<mSystemMaster> Finish_Case_Flags = lc.GetDataBySystemMaster("level_case", "Finish_Case_Flag");
            //ViewData["HANDLING_TYPEs"] = HANDLING_TYPEs;
            //ViewData["Transporations"] = Transporations;
            //ViewData["OrderTypes"] = OrderTypes;
            //ViewData["BuyerPDs"] = BuyerPDs;
            //ViewData["Finish_Case_Flags"] = Finish_Case_Flags;            

            return PartialView("_PartialForm", lc);
        }

        public  ActionResult Add(string Data)
        {
            LevelCaseMaster Model = System.Web.Helpers.Json.Decode<LevelCaseMaster>(Data);
            if (getpE_UserId != null)
            {
                Model.CREATED_DT = DateTime.Now;
                Model.CREATED_BY = getpE_UserId.Username;
                string result = Model.AddData(Model);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
            return null;
        }

        public ActionResult Update(string Data)
        {
            LevelCaseMaster Model = System.Web.Helpers.Json.Decode<LevelCaseMaster>(Data);
            if (getpE_UserId != null)
            {
                Model.CHANGED_DT = DateTime.Now;
                Model.CHANGED_BY = getpE_UserId.Username;
                string result = Model.UpdateData(Model);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
            return null;
        }

        public ActionResult Delete(string Data)
        {
            List<LevelCaseMaster> lcms= System.Web.Helpers.Json.Decode<List<LevelCaseMaster>>(Data);
            LevelCaseMaster Model = new LevelCaseMaster();
            if (getpE_UserId != null)
            {
                string UserId = getpE_UserId.Username;
                string result = Model.Delete(lcms, UserId);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
            return null;
        }

        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplateLevelCaseMaster";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                //AllowedFileExtensions = new string[] { ".xlsx" },
                AllowedFileExtensions = new string[] { ".csv" },
                MaxFileSize = 20971520
            };
        }

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            LevelCaseMaster LC = new LevelCaseMaster();

            string[] r = null;
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_LEVEL_CASE";
            #region createlog
            Guid PROCESS_ID = Guid.NewGuid();
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            string MESSAGE_DETAIL = "";
            long pid = 0;
            //string subFuncName = "EXcelToSQL";
            string isError = "N";
            string processName = "Upload Level Case Master";
            string resultMessage = "";

            int columns;
            int rows;

            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock L = new Lock();
            string functionIDUpload = "50003";
            int IsLock = L.is_lock(functionIDUpload);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMsgText("MSPX00092ERR");
            }

            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = LC.DeleteDataTBT();
                if (rExtract == "SUCCES")
                {
                    string user_id = getpE_UserId.Username;
                    Log lg = new Log();
                    string msg = "MSPX00035INF|INF|" + new MessagesString().getMsgText("MSPX00035INF").Replace("{0}", processName);
                    pid = lg.createLog(msg, user_id, processName , pid, moduleID, functionIDUpload);
                    L.CREATED_BY = user_id;
                    L.CREATED_DT = DateTime.Now;
                    L.PROCESS_ID = pid;
                    L.FUNCTION_ID = functionIDUpload;
                    L.LOCK_REF = functionIDUpload;
                    L.LockFunction(L);
                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    //Function untuk nge-extract file excel
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                    //rExtract = ReadAndInsertExcelData(pathfile);
                    //''string delimiter=Upload.GetDelimiter(e.UploadedFile.s)
                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    if (rExtract == string.Empty || rExtract == "SUCCESS")
                    {
                        rExtract = LC.BackgroundProsessSave(functionIDUpload, moduleID, user_id, pid);
                        if (rExtract == "SUCCES")
                            //msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2];
                            rExtract = "0|" + msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2] + "|" + pid.ToString();
                    }
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                //MESSAGE_DETAIL = "File is not .xls";
                //pid = log.createLog(msgError.getMSPX00019ERR("Price"), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                rExtract = "1| File is not .xls OR xlsx";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract;//+ "|" + pid.ToString();
        }


        public void Download(string Data)
        {
            LevelCaseMaster LCM = new LevelCaseMaster();
            LCM = System.Web.Helpers.Json.Decode<LevelCaseMaster>(Data);
            List<LevelCaseMaster> LCMs = new List<LevelCaseMaster>();
            LCMs = LCM.GetData(LCM);
            string filesTmp = HttpContext.Request.MapPath("~/Template/LevelCaseMasterTemplate.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);
            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);
            ISheet sheet = workbook.GetSheet("LevelCaseMaster");
            string date = DateTime.Now.ToString("ddMMyyyy");
            string filename = "LevelCaseMasterDownload_" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            int row = 1;
            IRow Hrow;
            foreach (LevelCaseMaster result in LCMs)
            {
                Hrow = sheet.CreateRow(row);
                Hrow.CreateCell(0).SetCellValue(row);
                Hrow.CreateCell(1).SetCellValue(result.PART_NO);
                Hrow.CreateCell(2).SetCellValue(result.ORDER_TYPE);
                Hrow.CreateCell(3).SetCellValue(result.BUYER_PD);
                Hrow.CreateCell(4).SetCellValue(result.TRANSP_CD);
                Hrow.CreateCell(5).SetCellValue(result.HANDLING_TYPE);
                Hrow.CreateCell(6).SetCellValue(result.FINISH_CASE_FLAG);
                Hrow.CreateCell(7).SetCellValue(result.DELETION_FLAG);
                Hrow.CreateCell(8).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(10).SetCellValue(result.CHANGED_BY);
                if (!string.IsNullOrEmpty(result.CHANGED_BY))
                {
                    Hrow.CreateCell(11).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));
                }
                
                row++;
            }
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
    
    }
}