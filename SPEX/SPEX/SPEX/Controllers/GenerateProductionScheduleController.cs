﻿// Default Using
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
//Default Toyota Model
using Toyota.Common.Web.Platform;
using Toyota.Common.Web.Platform.Starter.Models;
using Toyota.Common.Credential;
using Toyota.Common.Database;
//Default Devexpress Model
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using NPOI.POIFS.FileSystem;
using NPOI.HPSF;
using System.Web.UI;

namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class GenerateProductionScheduleController : PageController
    {
        //
        // GET: /GenerationProductionSchedule/
        mDailyOrder DailyOrder = new mDailyOrder();
        mMasterLine MasterLine = new mMasterLine();
        mMasterPlant MasterPlant = new mMasterPlant();
        mPart MasterPart = new mPart();

        protected override void Startup()
        {
            SPN221GetMasterLine();
            SPN221GetMasterPlant();

            List<mDailyOrder> model = DailyOrder.SPN221GetDailyOrder();
            ViewData["DataDailyOrder"] = model;

            ViewBag.FromDate = DateTime.Now;
        }

        public void SPN221GetMasterLine()
        {
            mMasterLine AllMasterLine = new mMasterLine();
            AllMasterLine.LINE_CD = "ALL";
            AllMasterLine.PLANT_CD = "";
            AllMasterLine.SLOC_CD = "";
            AllMasterLine.DESCRIPTION = "";

            List<mMasterLine> modelMasterLine = MasterLine.getListMasterLine();
            modelMasterLine.Insert(0, AllMasterLine);
            
            ViewData["DataMasterLine"] = modelMasterLine;
        }

        public void SPN221GetMasterPlant()
        {
            List<mMasterPlant> modelMasterPlant = MasterPlant.getListMasterPlant();
            
            ViewData["DataMasterPlant"] = modelMasterPlant;
        }

        public void SPN221GetMasterPart()
        {
            List<mPart> modelMasterPart = MasterPart.SPN311GetListMasterPartSpecific("", "", "");
            ViewData["DataMasterPart"] = modelMasterPart;
        }

        public ActionResult SPN221PopUpAdd()
        {
            SPN221GetMasterLine();
            SPN221GetMasterPart();

            return PartialView("_PartialAddDaily");
        }

        public string SPN221SaveData(string p_Prod_Month, string p_Order_Type, string p_Part_No, string p_Line_Cd, string p_Order_Dt, string p_Production_Dt, string p_Ratio, string Time_Usage_1, string Time_Usage_2, string Time_Usage_3, string Time_Usage_4, string Time_Usage_5, string Time_Usage_6, string Time_Usage_7, string Time_Usage_8, string Time_Usage_9, string Time_Usage_10, string Time_Usage_11, string Time_Usage_12, string Time_Usage_13, string Time_Usage_14, string Time_Usage_15, string Time_Usage_16, string Time_Usage_17, string Time_Usage_18, string Time_Usage_19, string Time_Usage_20, string Time_Usage_21, string Time_Usage_22, string Time_Usage_23, string Time_Usage_24, string Time_Usage_25, string Time_Usage_26, string Time_Usage_27, string Time_Usage_28, string Time_Usage_29, string Time_Usage_30, string Time_Usage_31, string Time_Avail_1, string Time_Avail_2, string Time_Avail_3, string Time_Avail_4, string Time_Avail_5, string Time_Avail_6, string Time_Avail_7, string Time_Avail_8, string Time_Avail_9, string Time_Avail_10, string Time_Avail_11, string Time_Avail_12, string Time_Avail_13, string Time_Avail_14, string Time_Avail_15, string Time_Avail_16, string Time_Avail_17, string Time_Avail_18, string Time_Avail_19, string Time_Avail_20, string Time_Avail_21, string Time_Avail_22, string Time_Avail_23, string Time_Avail_24, string Time_Avail_25, string Time_Avail_26, string Time_Avail_27, string Time_Avail_28, string Time_Avail_29, string Time_Avail_30, string Time_Avail_31, string Qty_D_1, string Qty_D_2, string Qty_D_3, string Qty_D_4, string Qty_D_5, string Qty_D_6, string Qty_D_7, string Qty_D_8, string Qty_D_9, string Qty_D_10, string Qty_D_11, string Qty_D_12, string Qty_D_13, string Qty_D_14, string Qty_D_15, string Qty_D_16, string Qty_D_17, string Qty_D_18, string Qty_D_19, string Qty_D_20, string Qty_D_21, string Qty_D_22, string Qty_D_23, string Qty_D_24, string Qty_D_25, string Qty_D_26, string Qty_D_27, string Qty_D_28, string Qty_D_29, string Qty_D_30, string Qty_D_31, string p_Created_By)
        {
            string new_p_Order_Dt = p_Order_Dt.Substring(6, 4) + "-" + p_Order_Dt.Substring(3, 2) + "-" + p_Order_Dt.Substring(0, 2);
            string new_p_Production_Dt = p_Order_Dt.Substring(6, 4) + "-" + p_Order_Dt.Substring(3, 2) + "-" + p_Order_Dt.Substring(0, 2);
            return DailyOrder.SPN221SaveDailyOrder(p_Prod_Month, p_Order_Type, p_Part_No, p_Line_Cd, new_p_Order_Dt, new_p_Production_Dt, p_Ratio, Time_Usage_1, Time_Usage_2, Time_Usage_3, Time_Usage_4, Time_Usage_5, Time_Usage_6, Time_Usage_7, Time_Usage_8, Time_Usage_9, Time_Usage_10, Time_Usage_11, Time_Usage_12, Time_Usage_13, Time_Usage_14, Time_Usage_15, Time_Usage_16, Time_Usage_17, Time_Usage_18, Time_Usage_19, Time_Usage_20, Time_Usage_21, Time_Usage_22, Time_Usage_23, Time_Usage_24, Time_Usage_25, Time_Usage_26, Time_Usage_27, Time_Usage_28, Time_Usage_29, Time_Usage_30, Time_Usage_31, Time_Avail_1, Time_Avail_2, Time_Avail_3, Time_Avail_4, Time_Avail_5, Time_Avail_6, Time_Avail_7, Time_Avail_8, Time_Avail_9, Time_Avail_10, Time_Avail_11, Time_Avail_12, Time_Avail_13, Time_Avail_14, Time_Avail_15, Time_Avail_16, Time_Avail_17, Time_Avail_18, Time_Avail_19, Time_Avail_20, Time_Avail_21, Time_Avail_22, Time_Avail_23, Time_Avail_24, Time_Avail_25, Time_Avail_26, Time_Avail_27, Time_Avail_28, Time_Avail_29, Time_Avail_30, Time_Avail_31, Qty_D_1, Qty_D_2, Qty_D_3, Qty_D_4, Qty_D_5, Qty_D_6, Qty_D_7, Qty_D_8, Qty_D_9, Qty_D_10, Qty_D_11, Qty_D_12, Qty_D_13, Qty_D_14, Qty_D_15, Qty_D_16, Qty_D_17, Qty_D_18, Qty_D_19, Qty_D_20, Qty_D_21, Qty_D_22, Qty_D_23, Qty_D_24, Qty_D_25, Qty_D_26, Qty_D_27, Qty_D_28, Qty_D_29, Qty_D_30, Qty_D_31, p_Created_By);
        }

        public string SPN221DeleteData(string p_Prod_Month, string p_Order_Type, string p_Part_No, string p_Line_Cd)
        {
            return DailyOrder.SPN211DeleteDailyOrder(p_Prod_Month, p_Order_Type, p_Part_No, p_Line_Cd);
        }

        public string SPN221SearchAvailUsage(string p_Prod_Month, string p_Order_Type, string p_Plant_Cd, string p_Line_Cd)
        {
            List<mDailyOrder> model = new List<mDailyOrder>();
            string list = "";

            model = DailyOrder.SPN221GetDailyOrderAvailUsage(p_Prod_Month, p_Order_Type, p_Plant_Cd, p_Line_Cd);

            if (model.Count() > 0)
            {
                list = list + model[0].Time_Avail_1.ToString() + ";";
                list = list + model[0].Time_Avail_2.ToString() + ";";
                list = list + model[0].Time_Avail_3.ToString() + ";";
                list = list + model[0].Time_Avail_4.ToString() + ";";
                list = list + model[0].Time_Avail_5.ToString() + ";";
                list = list + model[0].Time_Avail_6.ToString() + ";";
                list = list + model[0].Time_Avail_7.ToString() + ";";
                list = list + model[0].Time_Avail_8.ToString() + ";";
                list = list + model[0].Time_Avail_9.ToString() + ";";
                list = list + model[0].Time_Avail_10.ToString() + ";";
                list = list + model[0].Time_Avail_11.ToString() + ";";
                list = list + model[0].Time_Avail_12.ToString() + ";";
                list = list + model[0].Time_Avail_13.ToString() + ";";
                list = list + model[0].Time_Avail_14.ToString() + ";";
                list = list + model[0].Time_Avail_15.ToString() + ";";
                list = list + model[0].Time_Avail_16.ToString() + ";";
                list = list + model[0].Time_Avail_17.ToString() + ";";
                list = list + model[0].Time_Avail_18.ToString() + ";";
                list = list + model[0].Time_Avail_19.ToString() + ";";
                list = list + model[0].Time_Avail_20.ToString() + ";";
                list = list + model[0].Time_Avail_21.ToString() + ";";
                list = list + model[0].Time_Avail_22.ToString() + ";";
                list = list + model[0].Time_Avail_23.ToString() + ";";
                list = list + model[0].Time_Avail_24.ToString() + ";";
                list = list + model[0].Time_Avail_25.ToString() + ";";
                list = list + model[0].Time_Avail_26.ToString() + ";";
                list = list + model[0].Time_Avail_27.ToString() + ";";
                list = list + model[0].Time_Avail_28.ToString() + ";";
                list = list + model[0].Time_Avail_29.ToString() + ";";
                list = list + model[0].Time_Avail_30.ToString() + ";";
                list = list + model[0].Time_Avail_31.ToString() + ";";
                list = list + model[0].Time_Usage_1.ToString() + ";";
                list = list + model[0].Time_Usage_2.ToString() + ";";
                list = list + model[0].Time_Usage_3.ToString() + ";";
                list = list + model[0].Time_Usage_4.ToString() + ";";
                list = list + model[0].Time_Usage_5.ToString() + ";";
                list = list + model[0].Time_Usage_6.ToString() + ";";
                list = list + model[0].Time_Usage_7.ToString() + ";";
                list = list + model[0].Time_Usage_8.ToString() + ";";
                list = list + model[0].Time_Usage_9.ToString() + ";";
                list = list + model[0].Time_Usage_10.ToString() + ";";
                list = list + model[0].Time_Usage_11.ToString() + ";";
                list = list + model[0].Time_Usage_12.ToString() + ";";
                list = list + model[0].Time_Usage_13.ToString() + ";";
                list = list + model[0].Time_Usage_14.ToString() + ";";
                list = list + model[0].Time_Usage_15.ToString() + ";";
                list = list + model[0].Time_Usage_16.ToString() + ";";
                list = list + model[0].Time_Usage_17.ToString() + ";";
                list = list + model[0].Time_Usage_18.ToString() + ";";
                list = list + model[0].Time_Usage_19.ToString() + ";";
                list = list + model[0].Time_Usage_20.ToString() + ";";
                list = list + model[0].Time_Usage_21.ToString() + ";";
                list = list + model[0].Time_Usage_22.ToString() + ";";
                list = list + model[0].Time_Usage_23.ToString() + ";";
                list = list + model[0].Time_Usage_24.ToString() + ";";
                list = list + model[0].Time_Usage_25.ToString() + ";";
                list = list + model[0].Time_Usage_26.ToString() + ";";
                list = list + model[0].Time_Usage_27.ToString() + ";";
                list = list + model[0].Time_Usage_28.ToString() + ";";
                list = list + model[0].Time_Usage_29.ToString() + ";";
                list = list + model[0].Time_Usage_30.ToString() + ";";
                list = list + model[0].Time_Usage_31.ToString() + ";";
                list = list + model[0].Time_Percent_1.ToString() + ";";
                list = list + model[0].Time_Percent_2.ToString() + ";";
                list = list + model[0].Time_Percent_3.ToString() + ";";
                list = list + model[0].Time_Percent_4.ToString() + ";";
                list = list + model[0].Time_Percent_5.ToString() + ";";
                list = list + model[0].Time_Percent_6.ToString() + ";";
                list = list + model[0].Time_Percent_7.ToString() + ";";
                list = list + model[0].Time_Percent_8.ToString() + ";";
                list = list + model[0].Time_Percent_9.ToString() + ";";
                list = list + model[0].Time_Percent_10.ToString() + ";";
                list = list + model[0].Time_Percent_11.ToString() + ";";
                list = list + model[0].Time_Percent_12.ToString() + ";";
                list = list + model[0].Time_Percent_13.ToString() + ";";
                list = list + model[0].Time_Percent_14.ToString() + ";";
                list = list + model[0].Time_Percent_15.ToString() + ";";
                list = list + model[0].Time_Percent_16.ToString() + ";";
                list = list + model[0].Time_Percent_17.ToString() + ";";
                list = list + model[0].Time_Percent_18.ToString() + ";";
                list = list + model[0].Time_Percent_19.ToString() + ";";
                list = list + model[0].Time_Percent_20.ToString() + ";";
                list = list + model[0].Time_Percent_21.ToString() + ";";
                list = list + model[0].Time_Percent_22.ToString() + ";";
                list = list + model[0].Time_Percent_23.ToString() + ";";
                list = list + model[0].Time_Percent_24.ToString() + ";";
                list = list + model[0].Time_Percent_25.ToString() + ";";
                list = list + model[0].Time_Percent_26.ToString() + ";";
                list = list + model[0].Time_Percent_27.ToString() + ";";
                list = list + model[0].Time_Percent_28.ToString() + ";";
                list = list + model[0].Time_Percent_29.ToString() + ";";
                list = list + model[0].Time_Percent_30.ToString() + ";";
                list = list + model[0].Time_Percent_31.ToString();
            } else {
                list = "null";
            }

            return list;
        }

        public ActionResult SPN221PartialGridView(string p_Prod_Month, string p_Order_Type, string p_Plant_Cd, string p_Line_Cd)
        {
            List<mDailyOrder> model = new List<mDailyOrder>();

            if (p_Prod_Month == "NULLSTATE" && p_Plant_Cd == "NULLSTATE" && p_Line_Cd == "NULLSTATE")
            {
                model = DailyOrder.SPN221GetDailyOrder();
            }
            else
            {
                model = DailyOrder.SPN221GetDailyOrderSpecific(p_Prod_Month, p_Order_Type, p_Plant_Cd, p_Line_Cd);
            }

            return PartialView("_PartialGridView", model);
        }

        public void SPN221DownloadData(object sender, EventArgs e, string p_Prod_Month, string p_Plant_Cd, string p_Line_Cd, string p_Order_Type)
        {
            List<mDailyOrder> modelData = new List<mDailyOrder>();
            List<mDailyOrder> modelUsage = new List<mDailyOrder>();

            modelData = DailyOrder.SPN221GetDailyOrderSpecific(p_Prod_Month, p_Order_Type, p_Plant_Cd, p_Line_Cd);
            modelUsage = DailyOrder.SPN221GetDailyOrderAvailUsage(p_Prod_Month, p_Order_Type, p_Plant_Cd, p_Line_Cd);

            try
            {
                string filename = "";
                string filesTmp = HttpContext.Request.MapPath("~/Template/Generate_Production_Schedule_Download.xls");
                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = BorderStyle.THIN;
                styleContent1.BorderRight = BorderStyle.THIN;
                styleContent1.BorderBottom = BorderStyle.THIN;

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = BorderStyle.THIN;
                styleContent2.BorderRight = BorderStyle.THIN;
                styleContent2.BorderBottom = BorderStyle.THIN;
                styleContent2.Alignment = HorizontalAlignment.CENTER;

                ISheet sheet = workbook.GetSheet("Part Master");
                string date = DateTime.Now.ToString("dd-MM-yyyy");
                filename = "GenerateProductionSchedule_" + date + ".xls";
                sheet.ForceFormulaRecalculation = true;

                sheet.GetRow(6).GetCell(3).SetCellValue(date);
                sheet.GetRow(7).GetCell(3).SetCellValue("pid.kevin");
                sheet.GetRow(9).GetCell(4).SetCellValue(p_Prod_Month);
                sheet.GetRow(10).GetCell(4).SetCellValue(p_Plant_Cd);
                sheet.GetRow(11).GetCell(4).SetCellValue(p_Line_Cd);
                sheet.GetRow(12).GetCell(4).SetCellValue(p_Order_Type);

                int row = 20;
                string GetPartNo = string.Empty;
                string GetPartName = string.Empty;
                string GetPartSpec = string.Empty;
                string GetModel = string.Empty;
                string GetD1 = string.Empty;
                string GetD2 = string.Empty;
                string GetD3 = string.Empty;
                string GetD4 = string.Empty;
                string GetD5 = string.Empty;
                string GetD6 = string.Empty;
                string GetD7 = string.Empty;
                string GetD8 = string.Empty;
                string GetD9 = string.Empty;
                string GetD10 = string.Empty;
                string GetD11 = string.Empty;
                string GetD12 = string.Empty;
                string GetD13 = string.Empty;
                string GetD14 = string.Empty;
                string GetD15 = string.Empty;
                string GetD16 = string.Empty;
                string GetD17 = string.Empty;
                string GetD18 = string.Empty;
                string GetD19 = string.Empty;
                string GetD20 = string.Empty;
                string GetD21 = string.Empty;
                string GetD22 = string.Empty;
                string GetD23 = string.Empty;
                string GetD24 = string.Empty;
                string GetD25 = string.Empty;
                string GetD26 = string.Empty;
                string GetD27 = string.Empty;
                string GetD28 = string.Empty;
                string GetD29 = string.Empty;
                string GetD30 = string.Empty;
                string GetD31 = string.Empty;
                string GetPlantCd = string.Empty;
                string GetLineCd = string.Empty;
                string GetSloc = string.Empty;
                string GetPcsKbn = string.Empty;
                string GetTaktTime = string.Empty;
                string GetOrderType = string.Empty;
                string GetOrderDate = string.Empty;
                int index = 0;
                IRow Hrow;

                foreach (var result in modelData)
                {
                    index++;

                    Hrow = sheet.CreateRow(row);

                    GetPartNo = result.Part_No;
                    GetPartName = result.Part_Name;
                    GetPartSpec = result.Part_Spec;
                    GetModel = result.Model;
                    GetD1 = result.Qty_D_1.ToString();
                    GetD2 = result.Qty_D_2.ToString();
                    GetD3 = result.Qty_D_3.ToString();
                    GetD4 = result.Qty_D_4.ToString();
                    GetD5 = result.Qty_D_5.ToString();
                    GetD6 = result.Qty_D_6.ToString();
                    GetD7 = result.Qty_D_7.ToString();
                    GetD8 = result.Qty_D_8.ToString();
                    GetD9 = result.Qty_D_9.ToString();
                    GetD10 = result.Qty_D_10.ToString();
                    GetD11 = result.Qty_D_11.ToString();
                    GetD12 = result.Qty_D_12.ToString();
                    GetD13 = result.Qty_D_13.ToString();
                    GetD14 = result.Qty_D_14.ToString();
                    GetD15 = result.Qty_D_15.ToString();
                    GetD16 = result.Qty_D_16.ToString();
                    GetD17 = result.Qty_D_17.ToString();
                    GetD18 = result.Qty_D_18.ToString();
                    GetD19 = result.Qty_D_19.ToString();
                    GetD20 = result.Qty_D_20.ToString();
                    GetD21 = result.Qty_D_21.ToString();
                    GetD22 = result.Qty_D_22.ToString();
                    GetD23 = result.Qty_D_23.ToString();
                    GetD24 = result.Qty_D_24.ToString();
                    GetD25 = result.Qty_D_25.ToString();
                    GetD26 = result.Qty_D_26.ToString();
                    GetD27 = result.Qty_D_27.ToString();
                    GetD28 = result.Qty_D_28.ToString();
                    GetD29 = result.Qty_D_29.ToString();
                    GetD30 = result.Qty_D_30.ToString();
                    GetD31 = result.Qty_D_31.ToString();
                    GetPlantCd = result.Plant_Cd; 
                    GetLineCd = result.Line_Cd;
                    GetSloc = result.Sloc_Cd;
                    GetPcsKbn = result.PcsKbn.ToString();
                    GetTaktTime = result.Takt_Time.ToString(); 
                    GetOrderType = result.Order_Type;
                    GetOrderDate = result.Order_Dt.ToString(); 

                    Hrow.CreateCell(1).SetCellValue(index.ToString());
                    Hrow.CreateCell(2).SetCellValue(GetPartNo);
                    Hrow.CreateCell(3).SetCellValue(GetPartName);
                    Hrow.CreateCell(4).SetCellValue(GetPartSpec);
                    Hrow.CreateCell(5).SetCellValue(GetModel);
                    Hrow.CreateCell(6).SetCellValue("");
                    Hrow.CreateCell(7).SetCellValue(GetD1);
                    Hrow.CreateCell(8).SetCellValue(GetD2);
                    Hrow.CreateCell(9).SetCellValue(GetD3);
                    Hrow.CreateCell(10).SetCellValue(GetD4);
                    Hrow.CreateCell(11).SetCellValue(GetD5);
                    Hrow.CreateCell(12).SetCellValue(GetD6);
                    Hrow.CreateCell(13).SetCellValue(GetD7);
                    Hrow.CreateCell(14).SetCellValue(GetD8);
                    Hrow.CreateCell(15).SetCellValue(GetD9);
                    Hrow.CreateCell(16).SetCellValue(GetD10);
                    Hrow.CreateCell(17).SetCellValue(GetD11);
                    Hrow.CreateCell(18).SetCellValue(GetD12);
                    Hrow.CreateCell(19).SetCellValue(GetD13);
                    Hrow.CreateCell(20).SetCellValue(GetD14);
                    Hrow.CreateCell(21).SetCellValue(GetD15);
                    Hrow.CreateCell(22).SetCellValue(GetD16);
                    Hrow.CreateCell(23).SetCellValue(GetD17);
                    Hrow.CreateCell(24).SetCellValue(GetD18);
                    Hrow.CreateCell(25).SetCellValue(GetD19);
                    Hrow.CreateCell(26).SetCellValue(GetD20);
                    Hrow.CreateCell(27).SetCellValue(GetD21);
                    Hrow.CreateCell(28).SetCellValue(GetD22);
                    Hrow.CreateCell(29).SetCellValue(GetD23);
                    Hrow.CreateCell(30).SetCellValue(GetD24);
                    Hrow.CreateCell(31).SetCellValue(GetD25);
                    Hrow.CreateCell(32).SetCellValue(GetD26);
                    Hrow.CreateCell(33).SetCellValue(GetD27);
                    Hrow.CreateCell(34).SetCellValue(GetD28);
                    Hrow.CreateCell(35).SetCellValue(GetD29);
                    Hrow.CreateCell(36).SetCellValue(GetD30);
                    Hrow.CreateCell(37).SetCellValue(GetD31);
                    Hrow.CreateCell(38).SetCellValue(GetPlantCd);
                    Hrow.CreateCell(39).SetCellValue(GetLineCd);
                    Hrow.CreateCell(40).SetCellValue(GetSloc);
                    Hrow.CreateCell(41).SetCellValue(GetPcsKbn);
                    Hrow.CreateCell(42).SetCellValue(GetTaktTime);
                    Hrow.CreateCell(43).SetCellValue(GetOrderType);
                    Hrow.CreateCell(44).SetCellValue(GetOrderDate);

                    Hrow.GetCell(1).CellStyle = styleContent2;
                    Hrow.GetCell(2).CellStyle = styleContent2;
                    Hrow.GetCell(3).CellStyle = styleContent1;
                    Hrow.GetCell(4).CellStyle = styleContent1;
                    Hrow.GetCell(5).CellStyle = styleContent1;
                    Hrow.GetCell(6).CellStyle = styleContent1;
                    Hrow.GetCell(7).CellStyle = styleContent1;
                    Hrow.GetCell(8).CellStyle = styleContent1;
                    Hrow.GetCell(9).CellStyle = styleContent1;
                    Hrow.GetCell(10).CellStyle = styleContent1;
                    Hrow.GetCell(11).CellStyle = styleContent1;
                    Hrow.GetCell(12).CellStyle = styleContent1;
                    Hrow.GetCell(13).CellStyle = styleContent1;
                    Hrow.GetCell(14).CellStyle = styleContent1;
                    Hrow.GetCell(15).CellStyle = styleContent1;
                    Hrow.GetCell(16).CellStyle = styleContent1;
                    Hrow.GetCell(17).CellStyle = styleContent1;
                    Hrow.GetCell(18).CellStyle = styleContent1;
                    Hrow.GetCell(19).CellStyle = styleContent1;
                    Hrow.GetCell(20).CellStyle = styleContent1;
                    Hrow.GetCell(21).CellStyle = styleContent1;
                    Hrow.GetCell(22).CellStyle = styleContent1;
                    Hrow.GetCell(23).CellStyle = styleContent1;
                    Hrow.GetCell(24).CellStyle = styleContent1;
                    Hrow.GetCell(25).CellStyle = styleContent1;
                    Hrow.GetCell(26).CellStyle = styleContent1;
                    Hrow.GetCell(27).CellStyle = styleContent1;
                    Hrow.GetCell(28).CellStyle = styleContent1;
                    Hrow.GetCell(29).CellStyle = styleContent1;
                    Hrow.GetCell(30).CellStyle = styleContent1;
                    Hrow.GetCell(31).CellStyle = styleContent1;
                    Hrow.GetCell(32).CellStyle = styleContent1;
                    Hrow.GetCell(33).CellStyle = styleContent1;
                    Hrow.GetCell(34).CellStyle = styleContent1;
                    Hrow.GetCell(35).CellStyle = styleContent1;
                    Hrow.GetCell(36).CellStyle = styleContent1;
                    Hrow.GetCell(37).CellStyle = styleContent1;
                    Hrow.GetCell(38).CellStyle = styleContent2;
                    Hrow.GetCell(39).CellStyle = styleContent2;
                    Hrow.GetCell(40).CellStyle = styleContent2;
                    Hrow.GetCell(41).CellStyle = styleContent1;
                    Hrow.GetCell(42).CellStyle = styleContent1;
                    Hrow.GetCell(43).CellStyle = styleContent2;
                    Hrow.GetCell(44).CellStyle = styleContent2;

                    row++;
                }

                row = 17;

                sheet.GetRow(row).GetCell(7).SetCellValue(modelUsage[0].Time_Avail_1.ToString());
                sheet.GetRow(row).GetCell(8).SetCellValue(modelUsage[0].Time_Avail_2.ToString());
                sheet.GetRow(row).GetCell(9).SetCellValue(modelUsage[0].Time_Avail_3.ToString());
                sheet.GetRow(row).GetCell(10).SetCellValue(modelUsage[0].Time_Avail_4.ToString());
                sheet.GetRow(row).GetCell(11).SetCellValue(modelUsage[0].Time_Avail_5.ToString());
                sheet.GetRow(row).GetCell(12).SetCellValue(modelUsage[0].Time_Avail_6.ToString());
                sheet.GetRow(row).GetCell(13).SetCellValue(modelUsage[0].Time_Avail_7.ToString());
                sheet.GetRow(row).GetCell(14).SetCellValue(modelUsage[0].Time_Avail_8.ToString());
                sheet.GetRow(row).GetCell(15).SetCellValue(modelUsage[0].Time_Avail_9.ToString());
                sheet.GetRow(row).GetCell(16).SetCellValue(modelUsage[0].Time_Avail_10.ToString());
                sheet.GetRow(row).GetCell(17).SetCellValue(modelUsage[0].Time_Avail_11.ToString());
                sheet.GetRow(row).GetCell(18).SetCellValue(modelUsage[0].Time_Avail_12.ToString());
                sheet.GetRow(row).GetCell(19).SetCellValue(modelUsage[0].Time_Avail_13.ToString());
                sheet.GetRow(row).GetCell(20).SetCellValue(modelUsage[0].Time_Avail_14.ToString());
                sheet.GetRow(row).GetCell(21).SetCellValue(modelUsage[0].Time_Avail_15.ToString());
                sheet.GetRow(row).GetCell(22).SetCellValue(modelUsage[0].Time_Avail_16.ToString());
                sheet.GetRow(row).GetCell(23).SetCellValue(modelUsage[0].Time_Avail_17.ToString());
                sheet.GetRow(row).GetCell(24).SetCellValue(modelUsage[0].Time_Avail_18.ToString());
                sheet.GetRow(row).GetCell(25).SetCellValue(modelUsage[0].Time_Avail_19.ToString());
                sheet.GetRow(row).GetCell(26).SetCellValue(modelUsage[0].Time_Avail_20.ToString());
                sheet.GetRow(row).GetCell(27).SetCellValue(modelUsage[0].Time_Avail_21.ToString());
                sheet.GetRow(row).GetCell(28).SetCellValue(modelUsage[0].Time_Avail_22.ToString());
                sheet.GetRow(row).GetCell(29).SetCellValue(modelUsage[0].Time_Avail_23.ToString());
                sheet.GetRow(row).GetCell(30).SetCellValue(modelUsage[0].Time_Avail_24.ToString());
                sheet.GetRow(row).GetCell(31).SetCellValue(modelUsage[0].Time_Avail_25.ToString());
                sheet.GetRow(row).GetCell(32).SetCellValue(modelUsage[0].Time_Avail_26.ToString());
                sheet.GetRow(row).GetCell(33).SetCellValue(modelUsage[0].Time_Avail_27.ToString());
                sheet.GetRow(row).GetCell(34).SetCellValue(modelUsage[0].Time_Avail_28.ToString());
                sheet.GetRow(row).GetCell(35).SetCellValue(modelUsage[0].Time_Avail_29.ToString());
                sheet.GetRow(row).GetCell(36).SetCellValue(modelUsage[0].Time_Avail_30.ToString());
                sheet.GetRow(row).GetCell(37).SetCellValue(modelUsage[0].Time_Avail_31.ToString());

                row = 18;

                sheet.GetRow(row).GetCell(7).SetCellValue(modelUsage[0].Time_Usage_1.ToString());
                sheet.GetRow(row).GetCell(8).SetCellValue(modelUsage[0].Time_Usage_2.ToString());
                sheet.GetRow(row).GetCell(9).SetCellValue(modelUsage[0].Time_Usage_3.ToString());
                sheet.GetRow(row).GetCell(10).SetCellValue(modelUsage[0].Time_Usage_4.ToString());
                sheet.GetRow(row).GetCell(11).SetCellValue(modelUsage[0].Time_Usage_5.ToString());
                sheet.GetRow(row).GetCell(12).SetCellValue(modelUsage[0].Time_Usage_6.ToString());
                sheet.GetRow(row).GetCell(13).SetCellValue(modelUsage[0].Time_Usage_7.ToString());
                sheet.GetRow(row).GetCell(14).SetCellValue(modelUsage[0].Time_Usage_8.ToString());
                sheet.GetRow(row).GetCell(15).SetCellValue(modelUsage[0].Time_Usage_9.ToString());
                sheet.GetRow(row).GetCell(16).SetCellValue(modelUsage[0].Time_Usage_10.ToString());
                sheet.GetRow(row).GetCell(17).SetCellValue(modelUsage[0].Time_Usage_11.ToString());
                sheet.GetRow(row).GetCell(18).SetCellValue(modelUsage[0].Time_Usage_12.ToString());
                sheet.GetRow(row).GetCell(19).SetCellValue(modelUsage[0].Time_Usage_13.ToString());
                sheet.GetRow(row).GetCell(20).SetCellValue(modelUsage[0].Time_Usage_14.ToString());
                sheet.GetRow(row).GetCell(21).SetCellValue(modelUsage[0].Time_Usage_15.ToString());
                sheet.GetRow(row).GetCell(22).SetCellValue(modelUsage[0].Time_Usage_16.ToString());
                sheet.GetRow(row).GetCell(23).SetCellValue(modelUsage[0].Time_Usage_17.ToString());
                sheet.GetRow(row).GetCell(24).SetCellValue(modelUsage[0].Time_Usage_18.ToString());
                sheet.GetRow(row).GetCell(25).SetCellValue(modelUsage[0].Time_Usage_19.ToString());
                sheet.GetRow(row).GetCell(26).SetCellValue(modelUsage[0].Time_Usage_20.ToString());
                sheet.GetRow(row).GetCell(27).SetCellValue(modelUsage[0].Time_Usage_21.ToString());
                sheet.GetRow(row).GetCell(28).SetCellValue(modelUsage[0].Time_Usage_22.ToString());
                sheet.GetRow(row).GetCell(29).SetCellValue(modelUsage[0].Time_Usage_23.ToString());
                sheet.GetRow(row).GetCell(30).SetCellValue(modelUsage[0].Time_Usage_24.ToString());
                sheet.GetRow(row).GetCell(31).SetCellValue(modelUsage[0].Time_Usage_25.ToString());
                sheet.GetRow(row).GetCell(32).SetCellValue(modelUsage[0].Time_Usage_26.ToString());
                sheet.GetRow(row).GetCell(33).SetCellValue(modelUsage[0].Time_Usage_27.ToString());
                sheet.GetRow(row).GetCell(34).SetCellValue(modelUsage[0].Time_Usage_28.ToString());
                sheet.GetRow(row).GetCell(35).SetCellValue(modelUsage[0].Time_Usage_29.ToString());
                sheet.GetRow(row).GetCell(36).SetCellValue(modelUsage[0].Time_Usage_30.ToString());
                sheet.GetRow(row).GetCell(37).SetCellValue(modelUsage[0].Time_Usage_31.ToString());

                row = 19;

                sheet.GetRow(row).GetCell(7).SetCellValue(modelUsage[0].Time_Percent_1.ToString());
                sheet.GetRow(row).GetCell(8).SetCellValue(modelUsage[0].Time_Percent_2.ToString());
                sheet.GetRow(row).GetCell(9).SetCellValue(modelUsage[0].Time_Percent_3.ToString());
                sheet.GetRow(row).GetCell(10).SetCellValue(modelUsage[0].Time_Percent_4.ToString());
                sheet.GetRow(row).GetCell(11).SetCellValue(modelUsage[0].Time_Percent_5.ToString());
                sheet.GetRow(row).GetCell(12).SetCellValue(modelUsage[0].Time_Percent_6.ToString());
                sheet.GetRow(row).GetCell(13).SetCellValue(modelUsage[0].Time_Percent_7.ToString());
                sheet.GetRow(row).GetCell(14).SetCellValue(modelUsage[0].Time_Percent_8.ToString());
                sheet.GetRow(row).GetCell(15).SetCellValue(modelUsage[0].Time_Percent_9.ToString());
                sheet.GetRow(row).GetCell(16).SetCellValue(modelUsage[0].Time_Percent_10.ToString());
                sheet.GetRow(row).GetCell(17).SetCellValue(modelUsage[0].Time_Percent_11.ToString());
                sheet.GetRow(row).GetCell(18).SetCellValue(modelUsage[0].Time_Percent_12.ToString());
                sheet.GetRow(row).GetCell(19).SetCellValue(modelUsage[0].Time_Percent_13.ToString());
                sheet.GetRow(row).GetCell(20).SetCellValue(modelUsage[0].Time_Percent_14.ToString());
                sheet.GetRow(row).GetCell(21).SetCellValue(modelUsage[0].Time_Percent_15.ToString());
                sheet.GetRow(row).GetCell(22).SetCellValue(modelUsage[0].Time_Percent_16.ToString());
                sheet.GetRow(row).GetCell(23).SetCellValue(modelUsage[0].Time_Percent_17.ToString());
                sheet.GetRow(row).GetCell(24).SetCellValue(modelUsage[0].Time_Percent_18.ToString());
                sheet.GetRow(row).GetCell(25).SetCellValue(modelUsage[0].Time_Percent_19.ToString());
                sheet.GetRow(row).GetCell(26).SetCellValue(modelUsage[0].Time_Percent_20.ToString());
                sheet.GetRow(row).GetCell(27).SetCellValue(modelUsage[0].Time_Percent_21.ToString());
                sheet.GetRow(row).GetCell(28).SetCellValue(modelUsage[0].Time_Percent_22.ToString());
                sheet.GetRow(row).GetCell(29).SetCellValue(modelUsage[0].Time_Percent_23.ToString());
                sheet.GetRow(row).GetCell(30).SetCellValue(modelUsage[0].Time_Percent_24.ToString());
                sheet.GetRow(row).GetCell(31).SetCellValue(modelUsage[0].Time_Percent_25.ToString());
                sheet.GetRow(row).GetCell(32).SetCellValue(modelUsage[0].Time_Percent_26.ToString());
                sheet.GetRow(row).GetCell(33).SetCellValue(modelUsage[0].Time_Percent_27.ToString());
                sheet.GetRow(row).GetCell(34).SetCellValue(modelUsage[0].Time_Percent_28.ToString());
                sheet.GetRow(row).GetCell(35).SetCellValue(modelUsage[0].Time_Percent_29.ToString());
                sheet.GetRow(row).GetCell(36).SetCellValue(modelUsage[0].Time_Percent_30.ToString());
                sheet.GetRow(row).GetCell(37).SetCellValue(modelUsage[0].Time_Percent_31.ToString());

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                Response.BinaryWrite(ms.ToArray());
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            }
            catch (Exception err)
            {
                string x = err.ToString();

                x = err.ToString();
            }
        }
    }
}
