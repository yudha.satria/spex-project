﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class PackingCompletionController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mPackingCompletion packingCompletion = new mPackingCompletion();

        public PackingCompletionController()
        {
            Settings.Title = "Packing Completion";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
        }


        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PackingCompletionGridCallback(string p_PACKING_DT_FROM, string p_PACKING_DT_TO, string p_TMAP_ORDER_NO,
                string p_PART_NO, string p_ITEM_NO, string p_CASE_NO)
        {
            List<mPackingCompletion> model = new List<mPackingCompletion>();

            model = packingCompletion.getListPackingCompletion(reFormatDate(p_PACKING_DT_FROM), reFormatDate(p_PACKING_DT_TO), p_TMAP_ORDER_NO,
                 p_PART_NO, p_ITEM_NO, p_CASE_NO);

            return PartialView("PackingCompletionGrid", model);
        }

        public ActionResult UploadBackgroundProsess()
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;

            string resultMessage = packingCompletion.Upload(UserID);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = msgError.getMSPX00066INF("Upload Packing Completion", "<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00065INF("Upload Packing Completion", "<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }


        private string FunctionID = "D01-003";
        private string ModuleID = "D01";
        public void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_PACKING_COMPLETION";
            #region createlog
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);

            string templateFileName = "TemplatePackingCompletion";
            string UploadDirectory = Server.MapPath("~/Content/FileUploadResult");
            long ProcessID = 0;
            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock lockTable = new Lock();

            int IsLock = lockTable.is_lock(FunctionID, out ProcessID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(ProcessID.ToString());
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = packingCompletion.DeleteTempData();
                if (rExtract == "SUCCESS")
                {
                    string resultFilePath = UploadDirectory + templateFileName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, templateFileName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);
                    rExtract = ReadAndInsertExcelData(pathfile);
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .xls OR xlsx";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        #region Upload function when user click button upload
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, FileUploadComplete);
            return null;
        }
        #endregion

        #region cek format file to Upload  Price
        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplatePackingCompletion";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xlsx" },
                MaxFileSize = 20971520
            };
        }
        #endregion

        private string ReadAndInsertExcelData(string FileName)
        {
            try
            {
                XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                ISheet sheet = xssfworkbook.GetSheetAt(0);
                int lastrow = sheet.LastRowNum;
                List<mPackingCompletion.mPackingCompletionUpload> listPrice = new List<mPackingCompletion.mPackingCompletionUpload>();
                int index = 1;

                DataFormatter dataFormatter = new DataFormatter(CultureInfo.CurrentCulture);

                mPackingCompletion.mPackingCompletionUpload packingUpload;
                for (int i = index; i <= lastrow; i++)
                {
                    packingUpload = new mPackingCompletion.mPackingCompletionUpload();
                    IRow row = sheet.GetRow(i);

                    if (row == null) continue;
                    if (row.GetCell(0) == null && row.GetCell(1) == null && row.GetCell(2) == null) continue;


                    //data id
                    if (row.GetCell(0) == null) packingUpload.DATA_ID = "";
                    else if (row.GetCell(0).CellType == CellType.Blank || (row.GetCell(0).CellType == CellType.String && (row.GetCell(0).StringCellValue == null || row.GetCell(0).StringCellValue == "")))
                        packingUpload.DATA_ID = "";
                    else if (row.GetCell(0).CellType == CellType.Numeric && (row.GetCell(0).NumericCellValue.ToString() == ""))
                        packingUpload.DATA_ID = "";
                    else if (row.GetCell(0).CellType == CellType.String)
                        packingUpload.DATA_ID = row.GetCell(0).StringCellValue;
                    else if (row.GetCell(0).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(0);
                        string formatString = row.GetCell(0).CellStyle.GetDataFormatString();

                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            packingUpload.DATA_ID = row.GetCell(0).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            packingUpload.DATA_ID = row.GetCell(0).NumericCellValue.ToString();
                    }
                    else
                        packingUpload.DATA_ID = row.GetCell(0).ToString();

                    if (packingUpload.DATA_ID.ToLower() != "d")
                        continue;

                    //CASE_NO
                    if (row.GetCell(1) == null) packingUpload.CASE_NO = "";
                    else if (row.GetCell(1).CellType == CellType.Blank || (row.GetCell(1).CellType == CellType.String && (row.GetCell(1).StringCellValue == null || row.GetCell(1).StringCellValue == "")))
                        packingUpload.CASE_NO = "";
                    else if (row.GetCell(1).CellType == CellType.Numeric && (row.GetCell(1).NumericCellValue.ToString() == ""))
                        packingUpload.CASE_NO = "";
                    else if (row.GetCell(1).CellType == CellType.String)
                        packingUpload.CASE_NO = row.GetCell(1).StringCellValue;
                    else if (row.GetCell(1).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(1);
                        string formatString = row.GetCell(1).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            packingUpload.CASE_NO = row.GetCell(1).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            packingUpload.CASE_NO = row.GetCell(1).NumericCellValue.ToString();
                    }
                    else
                        packingUpload.CASE_NO = row.GetCell(1).ToString();

                    //case type
                    if (row.GetCell(2) == null) packingUpload.CASE_TYPE = "";
                    else if (row.GetCell(2).CellType == CellType.Blank || (row.GetCell(2).CellType == CellType.String && (row.GetCell(2).StringCellValue == null || row.GetCell(2).StringCellValue == "")))
                        packingUpload.CASE_TYPE = "";
                    else if (row.GetCell(2).CellType == CellType.Numeric && (row.GetCell(2).NumericCellValue.ToString() == ""))
                        packingUpload.CASE_TYPE = "";
                    else if (row.GetCell(2).CellType == CellType.String)
                        packingUpload.CASE_TYPE = row.GetCell(2).StringCellValue;
                    else if (row.GetCell(2).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(2);
                        string formatString = row.GetCell(2).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            packingUpload.CASE_TYPE = row.GetCell(2).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            packingUpload.CASE_TYPE = row.GetCell(2).NumericCellValue.ToString();
                    }
                    else
                        packingUpload.CASE_TYPE = row.GetCell(2).ToString();

                    if (row.GetCell(3) == null) packingUpload.PACKING_DT = "";
                    else if (row.GetCell(3).CellType == CellType.Blank || (row.GetCell(3).CellType == CellType.String && (row.GetCell(3).StringCellValue == null || row.GetCell(3).StringCellValue == "")))
                        packingUpload.PACKING_DT = "";
                    else if (row.GetCell(3).CellType == CellType.Numeric && (row.GetCell(3).NumericCellValue.ToString() == ""))
                        packingUpload.PACKING_DT = "";
                    else if (row.GetCell(3).CellType == CellType.String)
                        packingUpload.PACKING_DT = row.GetCell(3).StringCellValue;
                    else if (row.GetCell(3).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(3);
                        string formatString = row.GetCell(3).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            packingUpload.PACKING_DT = row.GetCell(3).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            packingUpload.PACKING_DT = row.GetCell(3).NumericCellValue.ToString();
                    }
                    else
                        packingUpload.PACKING_DT = row.GetCell(3).ToString();

                    if (row.GetCell(4) == null) packingUpload.PART_NO = "";
                    else if (row.GetCell(4).CellType == CellType.Blank || (row.GetCell(4).CellType == CellType.String && (row.GetCell(4).StringCellValue == null || row.GetCell(4).StringCellValue == "")))
                        packingUpload.PART_NO = "";
                    else if (row.GetCell(4).CellType == CellType.Numeric && (row.GetCell(4).NumericCellValue.ToString() == ""))
                        packingUpload.PART_NO = "";
                    else if (row.GetCell(4).CellType == CellType.String)
                        packingUpload.PART_NO = row.GetCell(4).StringCellValue;
                    else if (row.GetCell(4).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(4);
                        string formatString = row.GetCell(4).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            packingUpload.PART_NO = row.GetCell(4).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            packingUpload.PART_NO = row.GetCell(4).NumericCellValue.ToString();
                    }
                    else
                        packingUpload.PART_NO = row.GetCell(4).ToString();

                    if (row.GetCell(5) == null) packingUpload.QTY = "";
                    else if (row.GetCell(5).CellType == CellType.Blank || (row.GetCell(5).CellType == CellType.String && (row.GetCell(5).StringCellValue == null || row.GetCell(5).StringCellValue == "")))
                        packingUpload.QTY = "";
                    else if (row.GetCell(5).CellType == CellType.Numeric && (row.GetCell(5).NumericCellValue.ToString() == ""))
                        packingUpload.QTY = "";
                    else if (row.GetCell(5).CellType == CellType.String)
                        packingUpload.QTY = row.GetCell(5).StringCellValue;
                    else if (row.GetCell(5).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(5);
                        string formatString = row.GetCell(5).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            packingUpload.QTY = row.GetCell(5).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            packingUpload.QTY = row.GetCell(5).NumericCellValue.ToString();
                    }
                    else
                        packingUpload.QTY = row.GetCell(5).ToString();

                    if (row.GetCell(6) == null) packingUpload.TMMIN_ORDER_NO = "";
                    else if (row.GetCell(6).CellType == CellType.Blank || (row.GetCell(6).CellType == CellType.String && (row.GetCell(6).StringCellValue == null || row.GetCell(6).StringCellValue == "")))
                        packingUpload.TMMIN_ORDER_NO = "";
                    else if (row.GetCell(6).CellType == CellType.Numeric && (row.GetCell(6).NumericCellValue.ToString() == ""))
                        packingUpload.TMMIN_ORDER_NO = "";
                    else if (row.GetCell(6).CellType == CellType.String)
                        packingUpload.TMMIN_ORDER_NO = row.GetCell(6).StringCellValue;
                    else if (row.GetCell(6).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(6);
                        string formatString = row.GetCell(6).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            packingUpload.TMMIN_ORDER_NO = row.GetCell(6).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            packingUpload.TMMIN_ORDER_NO = row.GetCell(6).NumericCellValue.ToString();
                    }
                    else
                        packingUpload.TMMIN_ORDER_NO = row.GetCell(6).ToString();

                    listPrice.Add(packingUpload);
                }

                foreach (mPackingCompletion.mPackingCompletionUpload data in listPrice)
                {
                    string insertResult = (new mPackingCompletion()).InsertTemp(data);
                }

                return "0|success";
            }
            catch (Exception ex)
            {
                return "1|" + ex.Message;
            }
        }

        public void DownloadData_ByParameter(object sender, EventArgs e, string p_PACKING_DT_FROM, string p_PACKING_DT_TO, string p_TMAP_ORDER_NO,
                string p_PART_NO, string p_ITEM_NO, string p_CASE_NO)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/PackingCompletionDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Packing Completion");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "PackingCompletion" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(p_PACKING_DT_FROM + "   to   " + p_PACKING_DT_TO);
            sheet.GetRow(9).GetCell(2).SetCellValue(p_TMAP_ORDER_NO);

            sheet.GetRow(8).GetCell(6).SetCellValue(p_PART_NO);
            sheet.GetRow(9).GetCell(6).SetCellValue(p_ITEM_NO);

            int row = 16;
            int rowNum = 1;
            IRow Hrow;

            List<mPackingCompletion> model = new List<mPackingCompletion>();

            p_PACKING_DT_FROM = reFormatDate(p_PACKING_DT_FROM);
            p_PACKING_DT_TO = reFormatDate(p_PACKING_DT_TO);

            model = packingCompletion.getListPackingCompletion(p_PACKING_DT_FROM, p_PACKING_DT_TO, p_TMAP_ORDER_NO, p_PART_NO, p_ITEM_NO, p_CASE_NO);


            foreach (mPackingCompletion result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PACKING_ID);
                Hrow.CreateCell(3).SetCellValue(result.TMMIN_ORDER_NO);
                Hrow.CreateCell(4).SetCellValue(result.BUYER_CD);
                Hrow.CreateCell(5).SetCellValue(result.PD_CD);
                Hrow.CreateCell(6).SetCellValue(result.PART_NO);
                Hrow.CreateCell(7).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(8).SetCellValue(result.TMAP_ORDER_NO);
                Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMAP_ORDER_DT));
                Hrow.CreateCell(10).SetCellValue(result.PART_QTY);
                Hrow.CreateCell(11).SetCellValue(result.CASE_NO);
                Hrow.CreateCell(12).SetCellValue(result.CASE_TYPE);
                //Hrow.CreateCell(13).SetCellValue(result.CASE_GROSS_WEIGHT);
                Hrow.CreateCell(13).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.PACKING_DT));
                Hrow.CreateCell(14).SetCellValue(result.PACKING_COMPANY);
                Hrow.CreateCell(15).SetCellValue(result.TRANSPORT_CD);
                Hrow.CreateCell(16).SetCellValue(result.DANGER_FLAG);
                Hrow.CreateCell(17).SetCellValue(result.CONTAINER_NO);
                Hrow.CreateCell(18).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.VANNING_DT));
                Hrow.CreateCell(19).SetCellValue(result.SEAL_NO);
                Hrow.CreateCell(20).SetCellValue(result.CONTAINER_SIZE);
                Hrow.CreateCell(21).SetCellValue(result.CONTAINER_TYPE);
                Hrow.CreateCell(22).SetCellValue(result.RC_FLAG);
                Hrow.CreateCell(23).SetCellValue(result.VC_FLAG);
                Hrow.CreateCell(24).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(25).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(26).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(27).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent;
                Hrow.GetCell(12).CellStyle = styleContent;
                //Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent2;
                Hrow.GetCell(21).CellStyle = styleContent2;
                Hrow.GetCell(22).CellStyle = styleContent;
                Hrow.GetCell(23).CellStyle = styleContent;
                Hrow.GetCell(24).CellStyle = styleContent;
                Hrow.GetCell(25).CellStyle = styleContent2;
                Hrow.GetCell(26).CellStyle = styleContent;
                Hrow.GetCell(27).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        public void DownloadData_ByTicked(object sender, EventArgs e,string p_KEY_DOWNLOADS, string p_PACKING_DT_FROM, string p_PACKING_DT_TO, string p_TMAP_ORDER_NO,
                string p_PART_NO, string p_ITEM_NO, string p_CASE_NO)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/PackingCompletionDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Packing Completion");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "PackingCompletion" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(p_PACKING_DT_FROM + "   to   " + p_PACKING_DT_TO);
            sheet.GetRow(9).GetCell(2).SetCellValue(p_TMAP_ORDER_NO);

            sheet.GetRow(8).GetCell(6).SetCellValue(p_PART_NO);
            sheet.GetRow(9).GetCell(6).SetCellValue(p_ITEM_NO);

            int row = 16;
            int rowNum = 1;
            IRow Hrow;

            List<mPackingCompletion> model = new List<mPackingCompletion>();

            p_PACKING_DT_FROM = reFormatDate(p_PACKING_DT_FROM);
            p_PACKING_DT_TO = reFormatDate(p_PACKING_DT_TO);

            model = packingCompletion.DownloadData(p_KEY_DOWNLOADS, p_PACKING_DT_FROM, p_PACKING_DT_TO, p_TMAP_ORDER_NO, p_PART_NO, p_ITEM_NO, p_CASE_NO);


            foreach (mPackingCompletion result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PACKING_ID);
                Hrow.CreateCell(3).SetCellValue(result.TMMIN_ORDER_NO);
                Hrow.CreateCell(4).SetCellValue(result.BUYER_CD);
                Hrow.CreateCell(5).SetCellValue(result.PD_CD);
                Hrow.CreateCell(6).SetCellValue(result.PART_NO);
                Hrow.CreateCell(7).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(8).SetCellValue(result.TMAP_ORDER_NO);
                Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMAP_ORDER_DT));
                Hrow.CreateCell(10).SetCellValue(result.PART_QTY);
                Hrow.CreateCell(11).SetCellValue(result.CASE_NO);
                Hrow.CreateCell(12).SetCellValue(result.CASE_TYPE);
                //Hrow.CreateCell(13).SetCellValue(result.CASE_GROSS_WEIGHT);
                Hrow.CreateCell(13).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.PACKING_DT));
                Hrow.CreateCell(14).SetCellValue(result.PACKING_COMPANY);
                Hrow.CreateCell(15).SetCellValue(result.TRANSPORT_CD);
                Hrow.CreateCell(16).SetCellValue(result.DANGER_FLAG);
                Hrow.CreateCell(17).SetCellValue(result.CONTAINER_NO);
                Hrow.CreateCell(18).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.VANNING_DT));
                Hrow.CreateCell(19).SetCellValue(result.SEAL_NO);
                Hrow.CreateCell(20).SetCellValue(result.CONTAINER_SIZE);
                Hrow.CreateCell(21).SetCellValue(result.CONTAINER_TYPE);
                Hrow.CreateCell(22).SetCellValue(result.RC_FLAG);
                Hrow.CreateCell(23).SetCellValue(result.VC_FLAG);
                Hrow.CreateCell(24).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(25).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(26).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(27).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent;
                Hrow.GetCell(12).CellStyle = styleContent;
                //Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent2;
                Hrow.GetCell(21).CellStyle = styleContent2;
                Hrow.GetCell(22).CellStyle = styleContent;
                Hrow.GetCell(23).CellStyle = styleContent;
                Hrow.GetCell(24).CellStyle = styleContent;
                Hrow.GetCell(25).CellStyle = styleContent2;
                Hrow.GetCell(26).CellStyle = styleContent;
                Hrow.GetCell(27).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
    }
}
