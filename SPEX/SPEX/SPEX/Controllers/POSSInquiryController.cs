﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class POSSInquiryController : PageController
    {
        mPOSSInquiry POSSInquiry = new mPOSSInquiry();
        MessagesString messageError = new MessagesString();
        Log log = new Log();

        public POSSInquiryController()
        {
            Settings.Title = "POSS Inquiry Screen";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public string conversiDate(string dt)
        {
            string result = "";
            string[] adt = null;

            adt = dt.Split('.');
            dt = adt[1] + "." + adt[0] + "." + adt[2];

            if (dt != "01.01.0100") result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));

            return result;
        }

        protected override void Startup()
        {
            
            //ViewBag.MSPX00003ERR = messageError.getMSPX00003ERR("POSS Date From and POSS Date To");
            ViewBag.MSPX00001ERR = messageError.getMSPX00001ERR("POSS Inquiry");
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            getpE_UserId = (User)ViewData["User"];
 
        }

        public ActionResult GridViewPOSSInquiry(string pOrderDateFrom, string pOrderDateTo, string pOrderNo, string pPartNo, string pItemNo, string pStatusPOSS)
        {
            List<mPOSSInquiry> model = new List<mPOSSInquiry>();
            pOrderDateFrom = conversiDate(pOrderDateFrom);
            pOrderDateTo = conversiDate(pOrderDateTo);
            {
                model = POSSInquiry.getListPOSSInquiry(pOrderDateFrom, pOrderDateTo, pOrderNo, pPartNo, pItemNo, pStatusPOSS);
            }

            return PartialView("GridPOSSInquiry", model);
        }


        //Pop Up
        public ActionResult _CallPOSSDetail(string pU_OrderNo, string pU_ItemNo, string pU_PartNo, string pU_OrderQty)
        {
            
            List<mPOSSInquiry> model = POSSInquiry.getListPOSSDetail(pU_OrderNo, pU_ItemNo);
            ViewData["POSSInquiry"] = model;
            ViewBag.pU_OrderNo = pU_OrderNo;
            ViewBag.pU_ItemNo = pU_ItemNo;
            ViewBag.pU_PartNo = pU_PartNo;
            ViewBag.pU_OrderQty = pU_OrderQty;
            return PartialView("_PartialPOSSDetail");


        }


    }
}
