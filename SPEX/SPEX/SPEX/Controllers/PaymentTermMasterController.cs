﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Models.PaymentTermMaster;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class PaymentTermMasterController : PageController
    {
        //
        // GET: /PaymentTermMaster/
        mPaymentTermMaster PaymentTermMaster = new mPaymentTermMaster();

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }
        
        public PaymentTermMasterController()
        {
            Settings.Title = "Payment Term Master";
        }
        protected override void Startup()
        {
            ComboPaymentCD();
            getpE_UserId = (User)ViewData["User"];
        }
        // controller untuk DELTE
        public ActionResult DeletePaymentTerm(string p_PAYMENT_TERM_CD)
        {
            string[] r = null;
            string resultMessage = PaymentTermMaster.DeleteData(p_PAYMENT_TERM_CD);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        //controller untuk UP date
        public ActionResult UpdatePaymentTerm(string p_PAYMENT_TERM_CD, string p_DESCRIPTION1, string p_DESCRIPTION2, string p_CHANGED_BY)
        {
            p_CHANGED_BY = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = PaymentTermMaster.UpdateData(p_PAYMENT_TERM_CD, p_DESCRIPTION1, p_DESCRIPTION2, p_CHANGED_BY);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //controller untuk Add
        public ActionResult AddNewPaymentTerm(string p_PAYMENT_TERM_CD, string p_DESCRIPTION1, string p_DESCRIPTION2, string p_CREATED_BY)
        {
            p_CREATED_BY = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = PaymentTermMaster.SaveData(p_PAYMENT_TERM_CD, p_DESCRIPTION1, p_DESCRIPTION2, p_CREATED_BY);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        } 
        public ActionResult GridViewPaymentTerm(string pPaymentCd, string pDesc1)
        {
            List<mPaymentTermMaster> model = new List<mPaymentTermMaster>();

            {
                model = PaymentTermMaster.getPaymentTerm
                (pPaymentCd, pDesc1);
            }
            return PartialView("GridPayment", model);
        }

        public void ComboPaymentCD()
        {
            List<mPaymentTermMaster> model = new List<mPaymentTermMaster>();

            model = PaymentTermMaster.GetListPaymentCD();
            ViewData["PAYMENT_TERM_CD"] = model;
        }
        public void _DownloadPaymentTermMaster(object sender, EventArgs e, string D_PaymentCD, string D_Descript1)
        {

            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Payment_Term_Master.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;

            ISheet sheet = workbook.GetSheet("PaymentTermMaster");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "DownloadPaymentTermMaster_" + date + ".xls";
            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(4).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(9).GetCell(4).SetCellValue(D_PaymentCD);
            sheet.GetRow(10).GetCell(4).SetCellValue(D_Descript1);




            int row = 16;
            int rowNum = 1;
            IRow Hrow;

            List<mPaymentTermMaster> model = new List<mPaymentTermMaster>();
            model = PaymentTermMaster.getPaymentTerm(D_PaymentCD, D_Descript1);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PAYMENT_TERM_CD);
                Hrow.CreateCell(3).SetCellValue(result.DESCRIPTION_1);
                Hrow.CreateCell(4).SetCellValue(result.DESCRIPTION_2);
                //sheet.AddMergedRegion(new CellRangeAddress(row, row, 3, 4));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                //Hrow.GetCell(3).IsMergedCell=
                //Hrow.GetCell(4).CellStyle = styleContent3;
                //Hrow.GetCell(5).CellStyle = styleContent2;
                // Hrow.GetCell(6).CellStyle = styleContent;


                row++;
                rowNum++;
            }
            //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
            //    sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

    }

}
