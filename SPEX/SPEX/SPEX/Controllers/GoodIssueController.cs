﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class GoodIssueController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mGoodIssue GoodIssue = new mGoodIssue();

        public GoodIssueController()
        {
            Settings.Title = "Good Issue";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GoodIssueCallBack(string p_ETD_FROM, string p_ETD_TO, string p_INVOICE_DT_FROM, string p_INVOICE_DT_TO, string p_GOOD_ISSUE_STATUS, string p_INVOICE_NO)
        {
            List<mGoodIssue> model = new List<mGoodIssue>();

            p_ETD_FROM = p_ETD_FROM == "01.01.0100" ? "" : reFormatDate(p_ETD_FROM);
            p_ETD_TO = p_ETD_TO == "01.01.0100" ? "" : reFormatDate(p_ETD_TO);
            p_INVOICE_DT_FROM = p_INVOICE_DT_FROM == "01.01.0100" ? "" : reFormatDate(p_INVOICE_DT_FROM);
            p_INVOICE_DT_TO = p_INVOICE_DT_TO == "01.01.0100" ? "" : reFormatDate(p_INVOICE_DT_TO);

            model = GoodIssue.getList(p_ETD_FROM, p_ETD_TO, p_INVOICE_DT_FROM, p_INVOICE_DT_TO, p_GOOD_ISSUE_STATUS, p_INVOICE_NO);

            return PartialView("GoodIssueGrid", model);
        }

        public ActionResult GetInvoiceDetailGrid(string p_INVOICE_NO)
        {
            List<mInvoice> model = (new mInvoice()).getListInvoiceDetail(p_INVOICE_NO);
            return PartialView("InvoiceDetailGrid", model);
        }


        public ActionResult PopUpPosting(string p_ETD_FROM, string p_ETD_TO, string p_INVOICE_NOS, string MODE)
        {
            mGoodIssue model = GoodIssue.getGoodIssue(p_ETD_FROM, p_ETD_TO, p_INVOICE_NOS);

            if (model.INVOICE_NO != null)
                model.NOTE = "";
            else
                model.NOTE = "All date below is retrieved from smallest invoice no in selection";

            return PartialView("~/Views/GoodIssue/GoodIssuePosting.cshtml", model);
        }

        public ActionResult PostingGoodIssue(string p_ETD_FROM, string p_ETD_TO, string p_INVOICE_DT_FROM, string p_INVOICE_DT_TO, string p_GOOD_ISSUE_STATUS, string p_INVOICE_NO, string p_INVOICE_NO_PARAM, string p_MODE)
        {
            string[] r = null;
            string msg = "";

            p_ETD_FROM = p_ETD_FROM == "01.01.0100" ? "" : reFormatDate(p_ETD_FROM);
            p_ETD_TO = p_ETD_TO == "01.01.0100" ? "" : reFormatDate(p_ETD_TO);
            p_INVOICE_DT_FROM = p_INVOICE_DT_FROM == "01.01.0100" ? "" : reFormatDate(p_INVOICE_DT_FROM);
            p_INVOICE_DT_TO = p_INVOICE_DT_TO == "01.01.0100" ? "" : reFormatDate(p_INVOICE_DT_TO);

            string resultMessage = GoodIssue.SendPosting(p_ETD_FROM, p_ETD_TO, p_INVOICE_DT_FROM, p_INVOICE_DT_TO, p_GOOD_ISSUE_STATUS, p_INVOICE_NO, p_INVOICE_NO_PARAM, p_MODE, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = "";
                if (r.Count() > 2)
                    pID = r[2];
                msg = r[1];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00065INF("Good Issue Posting", "<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RePostingGoodIssue(string p_INVOICE_NO_PARAM, string p_MODE)
        {
            string[] r = null;
            string msg = "";

            string resultMessage = GoodIssue.SendPosting("", "", "", "", "", "", p_INVOICE_NO_PARAM, p_MODE, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = r[1];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00065INF("Good Issue Re-Posting", "<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }

        public void DownloadPosting(object sender, EventArgs e, string p_DEPARTURE_DT_FROM, string p_DEPARTURE_DT_TO)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/GoodIssueDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Good Issue");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "GoodIssue" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            //sheet.GetRow(8).GetCell(2).SetCellValue(p_TMAP_ORDER_DT + "   to   " + p_TMAP_ORDER_DT_TO);
            //sheet.GetRow(9).GetCell(2).SetCellValue(p_TMAP_ORDER_NO);

            int row = 16;
            int rowNum = 1;
            IRow Hrow;

            List<mGoodIssue> model = new List<mGoodIssue>();

            p_DEPARTURE_DT_FROM = p_DEPARTURE_DT_FROM == "01.01.0100" ? "" : reFormatDate(p_DEPARTURE_DT_FROM);
            p_DEPARTURE_DT_TO = p_DEPARTURE_DT_TO == "01.01.0100" ? "" : reFormatDate(p_DEPARTURE_DT_TO);

            model = GoodIssue.getDownload(p_DEPARTURE_DT_FROM, p_DEPARTURE_DT_TO);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.INVOICE_NO);
                Hrow.CreateCell(3).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.INVOICE_DT));
                Hrow.CreateCell(4).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.ETD));
                //Hrow.CreateCell(5).SetCellValue(result.ORDER_QTY.ToString());
                //Hrow.CreateCell(6).SetCellValue(result.ADJUST_QTY.ToString());
                //Hrow.CreateCell(7).SetCellValue(result.ACCEPT_QTY.ToString());
                //Hrow.CreateCell(8).SetCellValue(result.CREATED_BY);
                //Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                //Hrow.CreateCell(10).SetCellValue(result.CHANGED_BY);
                //Hrow.CreateCell(11).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                //Hrow.GetCell(5).CellStyle = styleContent3;
                //Hrow.GetCell(6).CellStyle = styleContent3;
                //Hrow.GetCell(7).CellStyle = styleContent3;
                //Hrow.GetCell(34).CellStyle = styleContent;
                //Hrow.GetCell(35).CellStyle = styleContent2;
                //Hrow.GetCell(36).CellStyle = styleContent;
                //Hrow.GetCell(37).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            //add agi 2017-04-07 request mrs hesti
            DownloadDetail(p_DEPARTURE_DT_FROM, p_DEPARTURE_DT_TO, filesTmp, workbook);
            
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        //add agi 2017-04-06 request Mrs Hesti
        public void DownloadDetail(string p_DEPARTURE_DT_FROM, string p_DEPARTURE_DT_TO, string filesTmp, XSSFWorkbook workbook)
        {
            //string filename = "";
            //string filesTmp = HttpContext.Request.MapPath("~/Template/GoodIssueDownloadDeatil.xlsx");
            //FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            //XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;



            ISheet sheet = workbook.GetSheet("Good Issue Detail");
            //string date = DateTime.Now.ToString("ddMMyyyy");
            //filename = "GoodIssue" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            //sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            //sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            //sheet.GetRow(8).GetCell(2).SetCellValue(p_TMAP_ORDER_DT + "   to   " + p_TMAP_ORDER_DT_TO);
            //sheet.GetRow(9).GetCell(2).SetCellValue(p_TMAP_ORDER_NO);

            int row = 1;
            int rowNum = 0;
            IRow Hrow;

            List<mInvoice> model = new List<mInvoice>();

            //p_DEPARTURE_DT_FROM = p_DEPARTURE_DT_FROM == "01.01.0100" ? "" : reFormatDate(p_DEPARTURE_DT_FROM);
            //p_DEPARTURE_DT_TO = p_DEPARTURE_DT_TO == "01.01.0100" ? "" : reFormatDate(p_DEPARTURE_DT_TO);

            model = GoodIssue.getDownloadDetail(p_DEPARTURE_DT_FROM, p_DEPARTURE_DT_TO);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);
                Hrow.CreateCell(0).SetCellValue(result.INVOICE_NO);
                if (result.INVOICE_DT!=null)
                {
                    Hrow.CreateCell(1).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.INVOICE_DT));
                }
                else
                {
                    Hrow.CreateCell(1).SetCellType(CellType.Blank);
                }
                Hrow.CreateCell(2).SetCellValue(result.SELLER_CODE);
                Hrow.CreateCell(3).SetCellValue(result.BUYER_CD);
                Hrow.CreateCell(4).SetCellValue(result.BUYER_PD);
                Hrow.CreateCell(5).SetCellValue(result.CURRENCY);
                Hrow.CreateCell(6).SetCellValue(result.CASE_NO);
                Hrow.CreateCell(7).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(8).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(9).SetCellValue(result.PART_NO);
                Hrow.CreateCell(10).SetCellValue(result.PART_NAME);
                if (result.INVOICE_QTY != null)
                {
                    Hrow.CreateCell(11).SetCellValue(result.INVOICE_QTY.ToString());
                }
                else
                {
                    Hrow.CreateCell(11).SetCellType(CellType.Blank);
                }
                Hrow.CreateCell(11).SetCellValue(result.INVOICE_QTY.ToString());
                Hrow.CreateCell(12).SetCellValue(result.UNIT_PRICE.ToString());
                Hrow.CreateCell(13).SetCellValue(result.AMOUNT.ToString());
                Hrow.CreateCell(14).SetCellValue(result.TAM_INVOICE);
                Hrow.CreateCell(15).SetCellValue(result.CONTAINER_NO);
                Hrow.CreateCell(16).SetCellValue(result.SEAL_NO);
                if (result.VANNING_DT != null)
                {
                    Hrow.CreateCell(17).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.VANNING_DT));
                }
                else
                {
                    Hrow.CreateCell(17).SetCellType(CellType.Blank);
                }                
                Hrow.CreateCell(18).SetCellValue(result.BYAIR_CASEAMOUNT.ToString());
                Hrow.CreateCell(19).SetCellValue(result.TRANSPORT_CD);
                Hrow.CreateCell(20).SetCellValue(result.HS_NO);
                Hrow.CreateCell(21).SetCellValue(result.RVC.ToString());
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent2;
                Hrow.GetCell(21).CellStyle = styleContent2;
                row++;
                rowNum++;
            }

            //MemoryStream ms = new MemoryStream();
            //workbook.Write(ms);
            //ftmp.Close();
            //Response.BinaryWrite(ms.ToArray());
            //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

    }
}
