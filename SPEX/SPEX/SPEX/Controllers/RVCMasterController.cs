﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class RVCMasterController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mPartPriceMaster partPrice = new mPartPriceMaster();

        public RVCMasterController()
        {
            Settings.Title = "RVC Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult RVCGridCallback(string pPART_NO, string pVALID_FROM, string pVALID_TO)
        {
            List<mPartPriceMaster> model = new List<mPartPriceMaster>();

            model = partPrice.GetListRVC(pPART_NO, GetDateSQL(pVALID_FROM), GetDateSQL(pVALID_TO));

            return PartialView("RVCGrid", model);
        }

        public ActionResult EditRVC(string pPART_NO, string pVALID_FROM, decimal? pRVC)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = partPrice.EditRVCData(pPART_NO, GetDate(pVALID_FROM), pRVC, getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        private string GetDateSQL(string dt)
        {
            if (dt == "01.01.0100")
                return "";

            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime date = new DateTime(year, month, day);

            return date.ToString("yyyy-MM-dd");
        }

        public ActionResult UploadBackgroundProsess()
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;

            string resultMessage = partPrice.UploadRVC(UserID);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                //msg = msgError.getMSPX00034INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                msg = msgError.getMSPX00066INF("Upload RVC Master", "<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00065INF("Upload RVC Master", "<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }


        private string FunctionID = "F15-048b";
        private string ModuleID = "F15";
        public void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_RVC";
            #region createlog
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);

            string templateFileName = "TemplateRVCMaster";
            string UploadDirectory = Server.MapPath("~/Content/FileUploadResult");
            long ProcessID = 0;
            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock lockTable = new Lock();

            int IsLock = lockTable.is_lock(FunctionID, out ProcessID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(ProcessID.ToString());
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = partPrice.DeleteTempRVCData();
                if (rExtract == "SUCCESS")
                {
                    string resultFilePath = UploadDirectory + templateFileName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, templateFileName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);
                    rExtract = ReadAndInsertExcelData(pathfile);
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .xls OR xlsx";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        #region Upload function when user click button upload
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, FileUploadComplete);
            return null;
        }
        #endregion

        #region cek format file to Upload  Price
        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplateRVCMaster";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xlsx" },
                MaxFileSize = 20971520
            };
        }
        #endregion

        public void DownloadTemplate()
        {
            try
            {
                string filename = "";
                string filesTmp = HttpContext.Request.MapPath("~/Template/TemplateRVCMaster.xlsx");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

                IDataFormat format = workbook.CreateDataFormat();


                ICellStyle style1 = workbook.CreateCellStyle();
                style1.VerticalAlignment = VerticalAlignment.Center;
                style1.Alignment = HorizontalAlignment.Center;
                style1.BorderBottom = BorderStyle.Thin;
                style1.BorderTop = BorderStyle.Thin;
                style1.BorderLeft = BorderStyle.Thin;
                style1.BorderRight = BorderStyle.Thin;

                ICellStyle style2 = workbook.CreateCellStyle();
                style2.VerticalAlignment = VerticalAlignment.Center;
                style2.Alignment = HorizontalAlignment.Left;
                style2.BorderBottom = BorderStyle.Thin;
                style2.BorderRight = BorderStyle.Thin;
                style2.BorderLeft = BorderStyle.Thin;
                style2.BorderRight = BorderStyle.None;

                ISheet sheetMain = workbook.GetSheet("Sheet1");
                string date = DateTime.Now.ToString("ddMMyyyyHHmmss");
                filename = "TemplateRVCMaster.xlsx";

                string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

                List<mPartPriceMaster> models = partPrice.GetRVCDownloadList();
                int startRow = 1;
                IRow Hrow;


                foreach (mPartPriceMaster rvc in models)
                {
                    Hrow = sheetMain.CreateRow(startRow);
                    //Hrow.CreateCell(0).SetCellValue("");
                    Hrow.CreateCell(0).SetCellValue(rvc.PART_NO);
                    Hrow.GetCell(0).CellStyle = style1;
                    if (rvc.RVC_STRING == "")
                        Hrow.CreateCell(1).SetCellValue(rvc.RVC_STRING);
                    else
                        Hrow.CreateCell(1).SetCellValue((double)rvc.RVC);
                    Hrow.GetCell(1).CellStyle = style1;
                    Hrow.CreateCell(2).SetCellValue(((DateTime)rvc.VALID_FROM).ToString("yyyy-MM-dd"));
                    Hrow.GetCell(2).CellStyle = style1;
                    startRow++;
                }

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                Response.BinaryWrite(ms.ToArray());
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            }
            catch (Exception ex)
            {

            }
        }

        private string ReadAndInsertExcelData(string FileName)
        {
            try
            {
                XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                ISheet sheet = xssfworkbook.GetSheetAt(0);
                int lastrow = sheet.LastRowNum;
                List<mPartPriceMaster.mPartPriceMasterUpload> listPrice = new List<mPartPriceMaster.mPartPriceMasterUpload>();
                int index = 1;

                DataFormatter dataFormatter = new DataFormatter(CultureInfo.CurrentCulture);
                        
                mPartPriceMaster.mPartPriceMasterUpload priceUpload;
                for (int i = index; i <= lastrow; i++)
                {
                    priceUpload = new mPartPriceMaster.mPartPriceMasterUpload();
                    IRow row = sheet.GetRow(i);

                    if (row == null) continue;
                    if (row.GetCell(0) == null && row.GetCell(1) == null && row.GetCell(2) == null) continue;

                    
                    //part no
                    if (row.GetCell(0) == null) priceUpload.PART_NO = "";
                    else if (row.GetCell(0).CellType == CellType.Blank || (row.GetCell(0).CellType == CellType.String && (row.GetCell(0).StringCellValue == null || row.GetCell(0).StringCellValue == "")))
                        priceUpload.PART_NO = "";
                    else if (row.GetCell(0).CellType == CellType.Numeric && (row.GetCell(0).NumericCellValue.ToString() == ""))
                        priceUpload.PART_NO = "";
                    else if (row.GetCell(0).CellType == CellType.String)
                        priceUpload.PART_NO = row.GetCell(0).StringCellValue;
                    else if (row.GetCell(0).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(0);
                        string formatString = row.GetCell(0).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            priceUpload.PART_NO = row.GetCell(0).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            priceUpload.PART_NO = row.GetCell(0).NumericCellValue.ToString();
                    }
                    else
                        priceUpload.PART_NO = row.GetCell(0).ToString();

                    //rvc
                    if (row.GetCell(1) == null) priceUpload.RVC = "";
                    else if (row.GetCell(1).CellType == CellType.Blank || (row.GetCell(1).CellType == CellType.String && (row.GetCell(1).StringCellValue == null || row.GetCell(1).StringCellValue == "")))
                        priceUpload.RVC = "";
                    else if (row.GetCell(1).CellType == CellType.Numeric && (row.GetCell(1).NumericCellValue.ToString() == ""))
                        priceUpload.RVC = "";
                    else if (row.GetCell(1).CellType == CellType.String)
                        priceUpload.RVC = row.GetCell(1).StringCellValue;
                    else if (row.GetCell(1).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(1);
                        string formatString = row.GetCell(1).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            priceUpload.RVC = row.GetCell(1).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            priceUpload.RVC = row.GetCell(1).NumericCellValue.ToString();
                    }
                    else
                        priceUpload.RVC = row.GetCell(1).ToString();

                    //valid date
                    if (row.GetCell(2) == null) priceUpload.VALID_FROM = "";
                    else if (row.GetCell(2).CellType == CellType.Blank || (row.GetCell(2).CellType == CellType.String && (row.GetCell(2).StringCellValue == null || row.GetCell(2).StringCellValue == "")))
                        priceUpload.VALID_FROM = "";
                    else if (row.GetCell(2).CellType == CellType.Numeric && (row.GetCell(2).NumericCellValue.ToString() == ""))
                        priceUpload.VALID_FROM = "";
                    else if (row.GetCell(2).CellType == CellType.String)
                        priceUpload.VALID_FROM = row.GetCell(2).StringCellValue;
                    else if (row.GetCell(2).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(2);
                        string formatString = row.GetCell(2).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            priceUpload.VALID_FROM = row.GetCell(2).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            priceUpload.VALID_FROM = row.GetCell(2).NumericCellValue.ToString();
                    }
                    else
                        priceUpload.VALID_FROM = row.GetCell(2).ToString();

                    listPrice.Add(priceUpload);
                }

                foreach (mPartPriceMaster.mPartPriceMasterUpload data in listPrice)
                {
                    string insertResult = (new mPartPriceMaster()).InsertTempRVC(data);
                }

                return  "0|success";
            }
            catch (Exception ex)
            {
                return "1|" + ex.Message;
            }
        }
    }
}

