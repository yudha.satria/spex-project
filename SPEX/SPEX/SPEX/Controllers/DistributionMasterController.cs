﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class DistributionMasterController : PageController
    {

        mDistributionMaster DistributionMaster = new mDistributionMaster();
        MessagesString messageError = new MessagesString();

        public DistributionMasterController()
        {
            Settings.Title = "Distribution Code Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            ComboDistributorCode();
            ViewBag.MSPX00001ERR = messageError.getMSPX00001ERR("Distribution Code Master");
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = messageError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        public void ComboDistributorCode()
        {
            List<mDistributionMaster> model = new List<mDistributionMaster>();

            model = DistributionMaster.getListDistributorCode();
            ViewData["DISTRIBUTOR_CD"] = model;
        }

        public ActionResult GridViewDistribution(string pDistributorCode, string pDistributorName, string pDistributorCountry)
        {
            List<mDistributionMaster> model = new List<mDistributionMaster>();

            {
                model = DistributionMaster.getListDistributionMaster(pDistributorCode, pDistributorName, pDistributorCountry);
            }
            return PartialView("GridDistributionMaster", model);
        }

        public void _DownloadDistributionMaster(object sender, EventArgs e, string D_DistributorCode, string D_DistributorName, string D_DistributorCountry)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Distribution_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;

            //insert data in header of xls
            ISheet sheet = workbook.GetSheet("DistributionMaster");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "DownloadDistributionMaster_" + date + ".xls";
            string dateNow = DateTime.Now.ToString("dd-MM-yyyy");

            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(3).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(9).GetCell(3).SetCellValue(D_DistributorCode);
            sheet.GetRow(10).GetCell(3).SetCellValue(D_DistributorName);
            sheet.GetRow(11).GetCell(3).SetCellValue(D_DistributorCountry);

            int row = 17;
            int rowNum = 1;
            IRow Hrow;

            //insert data show in screen into document
            List<mDistributionMaster> model = new List<mDistributionMaster>();
            model = DistributionMaster.DistributionMasterDownload(D_DistributorCode, D_DistributorName, D_DistributorCountry);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);
                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.DISTRIBUTOR_CD);
                Hrow.CreateCell(3).SetCellValue(result.OLD_DISTRIBUTOR_CD);
                Hrow.CreateCell(4).SetCellValue(result.DISTRIBUTOR_NAME);
                Hrow.CreateCell(5).SetCellValue(result.DISTRIBUTOR_COUNTRY);
                Hrow.CreateCell(6).SetCellValue(result.BUYER_CD);
                Hrow.CreateCell(7).SetCellValue(result.SELLER_CD);
                Hrow.CreateCell(8).SetCellValue(result.VENDOR_CD);
                Hrow.CreateCell(9).SetCellValue(result.PAYMENT_TERM);
                Hrow.CreateCell(10).SetCellValue(result.PRICE_TERM);

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent3;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        //Add Data
        public ActionResult AddDistribution(string pDistributorCode, string pOldDistributorCode, string pDistributorName, string pDistributorCountry, string pBuyerCode, string pSellerCode, string pVendorCode, string pPaymentTerm, string pPriceTerm, string pCREATED_BY)
        {
            
            string[] r = null;
            string resultMessage = DistributionMaster.SaveData(pDistributorCode, pOldDistributorCode, pDistributorName, pDistributorCountry, pBuyerCode, pSellerCode, pVendorCode, pPaymentTerm, pPriceTerm, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }


        //Edit Data
        public ActionResult EditDistribution(string pDistributorCode, string pOldDistributorCode, string pDistributorName, string pDistributorCountry, string pBuyerCode, string pSellerCode, string pVendorCode, string pPaymentTerm, string pPriceTerm, string pCHANGED_BY)
        {
            pCHANGED_BY = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = DistributionMaster.SaveDataUpdate(pDistributorCode, pOldDistributorCode, pDistributorName, pDistributorCountry, pBuyerCode, pSellerCode, pVendorCode, pPaymentTerm, pPriceTerm, pCHANGED_BY);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        //Delete Data
        public ActionResult DeleteDistribution(string pDistributorCode)
        {
            string[] r = null;
            string resultMessage = DistributionMaster.DeleteData(pDistributorCode);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

    }
}
