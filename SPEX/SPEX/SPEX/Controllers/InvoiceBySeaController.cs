﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class InvoiceBySeaController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mInvoice Invoice = new mInvoice();

        public InvoiceBySeaController()
        {
            Settings.Title = "Invoice Maintenance Screen";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public List<ReadyCargoInquiry> ReadyCargoList
        {
            get
            {
                if (Session["ReadyCargoInquiry" + getpE_UserId.Username] != null)
                    return (List<ReadyCargoInquiry>)Session["ReadyCargoInquiry" + getpE_UserId.Username];
                else return new List<ReadyCargoInquiry>();
            }
            set
            {
                Session["ReadyCargoInquiry" + getpE_UserId.Username] = value;
            }
        }

        public ReadyCargoInquiry ReadyCargo
        {
            get
            {
                if (ReadyCargoList.Count > 0)
                    return ReadyCargoList[0];
                else return new ReadyCargoInquiry();
            }

        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            if (getpE_UserId != null)
            {
                string p_INVOICE_NO = GetParameter("p_INVOICE_NO");
                string p_MODE = GetParameter("p_MODE");
                if (p_INVOICE_NO == null)
                {
                    ViewData["PREV_LINK"] = "ReadyCargoInquiry";
                    ViewData["IS_NEW"] = true;
                }
                else
                {
                    if (p_MODE == "R")
                        ViewData["PREV_LINK"] = "ReadyCargoInquiry";
                    else if (p_MODE == "I")
                        ViewData["PREV_LINK"] = "Invoice";
                    else
                        ViewData["PREV_LINK"] = "Invoice";

                    ViewData["IS_NEW"] = false;
                }

                GetInvoiceBySea(p_INVOICE_NO);
            }
            else
            {
                mInvoice model = new mInvoice() { ETD = DateTime.Now, INVOICE_DT = DateTime.Now, VANNING_DT = DateTime.Now, VANNING_DT_FROM = DateTime.Now, VANNING_DT_TO = DateTime.Now };
                ViewData["INVOICE_DATA"] = model;
            }
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }


        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            ar = dt.Split('.');
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;
            success = int.TryParse(ar[0].ToString(), out day);
            success = int.TryParse(ar[1].ToString(), out month);
            success = int.TryParse(ar[2].ToString(), out year);

            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        public void GetInvoiceBySea(string p_INVOICE_NO)
        {
            string UserID = "UserID";
            mInvoice model = new mInvoice();

            if (p_INVOICE_NO == null)
            {
                model = Invoice.getNewInvoiceBySea(ReadyCargo.BUYER_CD, ReadyCargo.PACKING_COMPANY, GetReadyCargoDetails(), UserID);
                model.VANNING_DT_FROM = ReadyCargo.VANNING_DT_FROM;
                model.VANNING_DT_TO = ReadyCargo.VANNING_DT_TO;
                model.PACKING_COMPANY = ReadyCargo.PACKING_COMPANY;
            }
            else
                model = Invoice.getInvoiceBySea(p_INVOICE_NO);

            ViewData["INVOICE_DATA"] = model;
            //return View("InvoiceBySea", model);
        }

        private string GetReadyCargoDetails()
        {
            string result = "";
            foreach (ReadyCargoInquiry item in ReadyCargoList)
            {
                result += (item.BUYER_CD + "|" + item.PD_CD + "|" + item.CONTAINER_NO + "|" + item.SEAL_NO + "|" + ((DateTime)item.VANNING_DT).ToString("dd.MM.yyyy") + ";");
            }

            result = result.Length > 0 ? result.Remove(result.Length - 1, 1) : result;

            return result;
        }

        public ActionResult PopUpInvoicePreview(string p_INVOICE_NO, string p_BUYER_CD, string p_VANNING_NO)
        {
            mInvoice model = new mInvoice();
            model.BUYER_CD = p_BUYER_CD;
            model.INVOICE_NO = p_INVOICE_NO;
            model.VANNING_NO = p_VANNING_NO;
            string UserID = getpE_UserId.Username;

            ViewData["InvoiceDetail"] = Invoice.getInvoiceDetailBySea(p_INVOICE_NO, ReadyCargo.PACKING_COMPANY, GetReadyCargoDetails(), UserID);

            return PartialView("~/Views/InvoiceBySea/InvoicePreview.cshtml", model);
        }

        public ActionResult GetInvoiceDetailGrid(string p_INVOICE_NO)
        {
            string UserID = getpE_UserId.Username;
            List<mInvoice> model = Invoice.getInvoiceDetailBySea(p_INVOICE_NO, ReadyCargo.PACKING_COMPANY, GetReadyCargoDetails(), UserID);
            return PartialView("InvoiceDetailGrid", model);
        }

        public ActionResult SaveData(string p_INVOICE_NO, string p_BUYER_CD, string p_BUYER_NAME, string p_VANNING_NO, string p_INVOICE_DT,
            string p_VESSEL_NAME, string p_ETD, string p_TOTAL_CONTAINER, string p_TOTAL_CASE, string p_TOTAL_ITEM, string p_TOTAL_QTY,
            string p_MEASUREMENT, string p_FOB_AMOUNT, string p_NET_WEIGHT, string p_GROSS_WEIGHT, string p_FREIGHT, string p_INSURANCE_AMOUNT,
            string p_PACKING_COMPANY, string p_FEEDER_VOYAGE)
        {
            string UserID = getpE_UserId.Username;
            string resultMessage = "";
            if (ReadyCargo.BUYER_CD == null)
                resultMessage = Invoice.SaveInvoiceBySea(p_INVOICE_NO, GetDate(p_INVOICE_DT), p_BUYER_CD, p_BUYER_NAME, p_VANNING_NO,
                   p_VESSEL_NAME, GetDate(p_ETD), GetDecimal(p_TOTAL_CONTAINER), GetDecimal(p_TOTAL_CASE), GetDecimal(p_TOTAL_ITEM),
                   GetDecimal(p_TOTAL_QTY), GetDecimal(p_MEASUREMENT), GetDecimal(p_FOB_AMOUNT), GetDecimal(p_NET_WEIGHT),
                   GetDecimal(p_GROSS_WEIGHT), GetDecimal(p_FREIGHT), GetDecimal(p_INSURANCE_AMOUNT), p_PACKING_COMPANY,p_FEEDER_VOYAGE,
                   GetReadyCargoDetails(), "Y", UserID);
            else
                resultMessage = Invoice.SaveInvoiceBySea(p_INVOICE_NO, GetDate(p_INVOICE_DT), p_BUYER_CD, p_BUYER_NAME, p_VANNING_NO,
               p_VESSEL_NAME, GetDate(p_ETD), GetDecimal(p_TOTAL_CONTAINER), GetDecimal(p_TOTAL_CASE), GetDecimal(p_TOTAL_ITEM),
               GetDecimal(p_TOTAL_QTY), GetDecimal(p_MEASUREMENT), GetDecimal(p_FOB_AMOUNT), GetDecimal(p_NET_WEIGHT),
               GetDecimal(p_GROSS_WEIGHT), GetDecimal(p_FREIGHT), GetDecimal(p_INSURANCE_AMOUNT), p_PACKING_COMPANY,p_FEEDER_VOYAGE,
               GetReadyCargoDetails(), "N", UserID);

            string[] r = null;
            r = resultMessage.Split('|');

            if (r[0].ToString().ToLower().Contains("success"))
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);

        }

        private decimal GetDecimal(string strValue)
        {
            decimal returnValue = 0;
            returnValue = Convert.ToDecimal(strValue);
            return returnValue;
        }

        public ActionResult CheckInvoice(string p_INVOICE_NO)
        {
            mInvoice inv = Invoice.getInvoice(p_INVOICE_NO);

            if (inv != null)
                return Json(new { success = "true", messages = "Invoice already exists, are you sure to replace it ? " }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "false", messages = "not exist" }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult UpdateData(string p_INVOICE_NO, string p_BUYER_CD, string p_BUYER_NAME, string p_VANNING_NO, string p_INVOICE_DT,
            string p_VESSEL_NAME, string p_ETD, string p_TOTAL_CONTAINER, string p_TOTAL_CASE, string p_TOTAL_ITEM, string p_TOTAL_QTY,
            string p_MEASUREMENT, string p_FOB_AMOUNT, string p_NET_WEIGHT, string p_GROSS_WEIGHT, string p_FREIGHT, string p_INSURANCE_AMOUNT,
            string p_PACKING_COMPANY, string p_FEEDER_VOYAGE)
        {
            string UserID = getpE_UserId.Username;
            string resultMessage = "";

            resultMessage = Invoice.SaveInvoiceBySea(p_INVOICE_NO, GetDate(p_INVOICE_DT), p_BUYER_CD, p_BUYER_NAME, p_VANNING_NO,
               p_VESSEL_NAME, GetDate(p_ETD), GetDecimal(p_TOTAL_CONTAINER), GetDecimal(p_TOTAL_CASE), GetDecimal(p_TOTAL_ITEM),
               GetDecimal(p_TOTAL_QTY), GetDecimal(p_MEASUREMENT), GetDecimal(p_FOB_AMOUNT), GetDecimal(p_NET_WEIGHT),
               GetDecimal(p_GROSS_WEIGHT), GetDecimal(p_FREIGHT), GetDecimal(p_INSURANCE_AMOUNT), p_PACKING_COMPANY, p_FEEDER_VOYAGE,
               GetReadyCargoDetails(), "Y", UserID);


            string[] r = null;
            r = resultMessage.Split('|');

            if (r[0].ToString().ToLower().Contains("success"))
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);

        }
    }
}
