﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Text;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Linq;

namespace SPEX.Controllers
{
    public class PartReceivingInquiryController : PageController
    {
        public static string ORDER_NO = "ORDER_NO";
        public static string ITEM_NO = "ITEM_NO";
        public static string PART_NO = "PART_NO";
        public static string ORDER_START_DATE = "ORDER_START_DATE";
        public static string ORDER_END_DATE = "ORDER_END_DATE";
        public static string REDIRECT_PAGE = "REDIRECT";
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        PartReceivingInquiry pri = new PartReceivingInquiry();
        
        public ActionResult Redirect(string orderNo, string itemNo, string partNo, string pStartOrdDt, string pEndOrdDt)
        {
            ViewData[REDIRECT_PAGE] = REDIRECT_PAGE;
            ViewData[ORDER_NO] = orderNo;
            ViewData[ITEM_NO] = itemNo;
            ViewData[PART_NO] = partNo;
            ViewData[ORDER_START_DATE] = pStartOrdDt;
            ViewData[ORDER_END_DATE] = pEndOrdDt;
            return Index();
        }

        public PartReceivingInquiryController()
        {
            Settings.Title = "Part Receiving Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            ViewData["BuyerPDCdList"] = pri.GetBuyerMasterList();
            //add agi 2017-10-24
            string range = new mSystemMaster().GetSystemValueList("part inquiry", "range day", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            ViewBag.range = Convert.ToInt32(range);
            ViewBag.MSPXP0004ERR = msgError.getMsgText("MSPXP0004ERR").Replace("{0}", range);
            ViewBag.MSPXP0003ERR = msgError.getMsgText("MSPXP0003ERR");
            ViewBag.MSPXP0002ERR = msgError.getMsgText("MSPXP0002ERR");
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (string.IsNullOrEmpty(dt))
            {
                result = "";
            }
            else if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        //CHANGE AGI 2017-10-28
        //public ActionResult PartRecvInquiryGridCallback(string pBuyer_Pd, string pKanban_Id, string pStartOrdDt, string pEndOrdDt, string pTmap_Order_No
        //    , string pPart_No, string pTmmin_Order_No, string pManifest_No, string pItem_No)
        public ActionResult PartRecvInquiryGridCallback(string pBuyer_Pd, string pKanban_Id, string pStartOrdDt, string pEndOrdDt, string pTmap_Order_No
            , string pPart_No, string pTmmin_Order_No, string pManifest_No, string pItem_No, string pStartReleaseDt, string pEndReleaseDt, string pSupplier_Code, string pSupplier_Plant, string pSubSupplier_Code, string pSubSupplier_Plant)
        {            
            List<PartReceivingInquiry> model = new List<PartReceivingInquiry>();

            //model = pri.getList(pBuyer_Pd, pTmap_Order_No, pTmmin_Order_No, pManifest_No, pPart_No, pKanban_Id, reFormatDate(pStartOrdDt), reFormatDate(pEndOrdDt), pItem_No);
            model = pri.getList(pBuyer_Pd, pTmap_Order_No, pTmmin_Order_No, pManifest_No, pPart_No, pKanban_Id, reFormatDate(pStartOrdDt), reFormatDate(pEndOrdDt), pItem_No, reFormatDate(pStartReleaseDt), reFormatDate(pEndReleaseDt), pSupplier_Code, pSupplier_Plant, pSubSupplier_Code, pSubSupplier_Plant);
            return PartialView("PartRecvInquiryGrid", model);
        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        private string GetDateSQL(string dt)
        {
            if (dt == "01.01.0100")
                return "";

            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime date = new DateTime(year, month, day);

            return date.ToString("yyyy-MM-dd");
        }

        #region Export to Excel
        public void DownloadData_ByParameter_Excel(object sender, EventArgs e, string pBuyer_Pd, string pKanban_Id, string pStartOrdDt
            , string pEndOrdDt, string pTmap_Order_No, string pPart_No, string pTmmin_Order_No, string pManifest_No, string pItem_No
            , string pSupplier_Code, string pSupplier_Plant, string pSubSupplier_Code, string pSubSupplier_Plant)
        {
            string pBuyerPDCd = pBuyer_Pd ?? String.Empty;
            string pTMAPOrderNo = pTmap_Order_No ?? String.Empty;
            string pTMMINOrderNo = pTmmin_Order_No ?? String.Empty;
            string pKanbanId = pKanban_Id ?? String.Empty;
            string pPartNo = pPart_No ?? String.Empty;
            string pManifestNo = pManifest_No ?? String.Empty;
            string pRecvDTFr = pStartOrdDt ?? String.Empty;
            string pRecvDTTo = pEndOrdDt ?? String.Empty;
            string pItemNo = pItem_No ?? String.Empty;

            string pSupplierCode = pSupplier_Code ?? String.Empty;
            string pSupplierPlant = pSupplier_Plant ?? String.Empty;
            string pSubSupplierCode = pSubSupplier_Code ?? String.Empty;
            string pSubSupplierPlant = pSubSupplier_Plant ?? String.Empty;

            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/TemplatePartRecvInquiry.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            IFont font = workbook.CreateFont();
            font.FontName = "Arial";
            font.FontHeightInPoints = 8;

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Right;
            styleContent.SetFont(font);

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;
            styleContent2.SetFont(font);

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;
            styleContent3.SetFont(font);

            ISheet sheet = workbook.GetSheet("PartRecvInquiry");
            string date = DateTime.Now.ToString("yyyyMMddHHmmss");
            filename = "SPEX_Part_Receiving_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(2).GetCell(2).SetCellValue(pBuyerPDCd.Equals(String.Empty) ? "ALL" : pBuyerPDCd);
            sheet.GetRow(2).GetCell(6).SetCellValue(pKanbanId.Equals(String.Empty) ? "ALL" : pKanbanId);
            sheet.GetRow(2).GetCell(11).SetCellValue((pRecvDTFr.Equals(String.Empty) ? "01.01.0001" : pRecvDTFr));
            sheet.GetRow(2).GetCell(13).SetCellValue((pRecvDTTo.Equals(String.Empty) ? "01.01.0001" : pRecvDTTo));

            sheet.GetRow(3).GetCell(2).SetCellValue(pTMAPOrderNo.Equals(String.Empty) ? "ALL" : pTMAPOrderNo);
            sheet.GetRow(3).GetCell(6).SetCellValue(pPartNo.Equals(String.Empty) ? "ALL" : pPartNo);
            sheet.GetRow(3).GetCell(11).SetCellValue((pItemNo.Equals(String.Empty) ? "ALL" : pItemNo));

            sheet.GetRow(4).GetCell(2).SetCellValue(pTMMINOrderNo.Equals(String.Empty) ? "ALL" : pTMMINOrderNo);
            sheet.GetRow(4).GetCell(6).SetCellValue(pManifestNo.Equals(String.Empty) ? "ALL" : pManifestNo);

            List<PartReceivingInquiry> model = new List<PartReceivingInquiry>();

            model = pri.getList(pBuyerPDCd, pTMAPOrderNo, pTMMINOrderNo, pManifestNo, pPartNo, pKanbanId, reFormatDate(pRecvDTFr), reFormatDate(pRecvDTTo), pItemNo, string.Empty, string.Empty, pSupplierCode, pSupplierPlant, pSubSupplierCode, pSubSupplierPlant);

            int row = 9;
            int rowNum = 1;
            IRow Hrow;

            foreach (PartReceivingInquiry d in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(0).SetCellValue(rowNum);
                Hrow.CreateCell(1).SetCellValue(d.BUYERPD_CD);
                Hrow.CreateCell(2).SetCellValue(d.TMAP_ORDER_NO);
                Hrow.CreateCell(3).SetCellValue(d.ORDER_DT);
                Hrow.CreateCell(4).SetCellValue(d.TMMIN_ORDER_NO);
                Hrow.CreateCell(5).SetCellValue(d.MANIFEST_NO);
                Hrow.CreateCell(6).SetCellValue(d.KANBAN_ID);
                Hrow.CreateCell(7).SetCellValue(d.ITEM_NO);
                Hrow.CreateCell(8).SetCellValue(d.PART_NO);
                Hrow.CreateCell(9).SetCellValue(d.TRANSPORTATION_CD);
                Hrow.CreateCell(10).SetCellValue(d.PRIVILEGE);
                Hrow.CreateCell(11).SetCellValue(d.HANDLING_TYPE);
                Hrow.CreateCell(12).SetCellValue(d.PART_STATUS);
                Hrow.CreateCell(13).SetCellValue(d.PLAN_RECEIVE);
                Hrow.CreateCell(14).SetCellValue(d.RECEIVED_DT);

                Hrow.CreateCell(15).SetCellValue(d.SUPPLIER_CD);
                Hrow.CreateCell(16).SetCellValue(d.SUPPLIER_PLANT);
                Hrow.CreateCell(17).SetCellValue(d.SUB_SUPPLIER_CD);
                Hrow.CreateCell(18).SetCellValue(d.SUB_SUPPLIER_PLANT);

                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent3;
                Hrow.GetCell(3).CellStyle = styleContent3;
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;

                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;

                row++;
                rowNum++;
            }
            
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
        #endregion

        public JsonResult doUpdateRecvSts(String KANBAN_ID)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string pKanbanID = KANBAN_ID;
            string msg = "";
            string UserID = getpE_UserId.Username;
            String result = pri.doUpdateRecvSts(pKanbanID, UserID);
            r = result.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                //msg = r[1].Trim();
                msg = "MSPXF0069ERR |" + msgError.getMSG_SpexHref("MSPXF0069ERR", "<a href='#' onclick='MessageShow(" + r[2] + "); return false;'>" + r[2] + "</a>").Split('|')[2] + "|" + r[2].ToString();
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                msg = r[1].Trim();
                return Json(new { success = "true", messages = msg }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Export to CSV
        //change agi 2017-10-24
        //public void DownloadData_ByParameter(string pBuyer_Pd, string pKanban_Id, string pStartOrdDt
        //    , string pEndOrdDt, string pTmap_Order_No, string pPart_No, string pTmmin_Order_No, string pManifest_No, string pItem_No)
        public void DownloadData_ByParameter(string pBuyer_Pd, string pKanban_Id, string pStartOrdDt
            , string pEndOrdDt, string pTmap_Order_No, string pPart_No, string pTmmin_Order_No, string pManifest_No, string pItem_No
            , string pStartReleaseDt, string pEndReleaseDt, string pSupplier_Code, string pSupplier_Plant, string pSubSupplier_Code, string pSubSupplier_Plant)
        {
            string filename = "";
            mMOPInquiryDownload MOP = new mMOPInquiryDownload();
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPEX_Part_Receiving_" + date + ".csv";
                
            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            string dateNow = DateTime.Now.ToString();

            string pBuyerPDCd = pBuyer_Pd ?? String.Empty;
            string pTMAPOrderNo = pTmap_Order_No ?? String.Empty;
            string pTMMINOrderNo = pTmmin_Order_No ?? String.Empty;
            string pKanbanId = pKanban_Id ?? String.Empty;
            string pPartNo = pPart_No ?? String.Empty;
            string pManifestNo = pManifest_No ?? String.Empty;
            string pRecvDTFr = pStartOrdDt ?? String.Empty;
            string pRecvDTTo = pEndOrdDt ?? String.Empty;
            string pItemNo = pItem_No ?? String.Empty;

            string pSupplierCode = pSupplier_Code ?? String.Empty;
            string pSupplierPlant = pSupplier_Plant ?? String.Empty;
            string pSubSupplierCode = pSubSupplier_Code ?? String.Empty;
            string pSubSupplierPlant = pSubSupplier_Plant ?? String.Empty;

            List<PartReceivingInquiry> model = new List<PartReceivingInquiry>();

            //change agi 2017-10-24
            //model = pri.getList(pBuyerPDCd, pTMAPOrderNo, pTMMINOrderNo, pManifestNo, pPartNo, pKanbanId, reFormatDate(pRecvDTFr), reFormatDate(pRecvDTTo), pItemNo);
            model = pri.getList(pBuyerPDCd, pTMAPOrderNo, pTMMINOrderNo, pManifestNo, pPartNo, pKanbanId, reFormatDate(pRecvDTFr), reFormatDate(pRecvDTTo), pItemNo,reFormatDate(pStartReleaseDt),reFormatDate(pEndReleaseDt), pSupplierCode, pSupplierPlant, pSubSupplierCode, pSubSupplierPlant);
            StringBuilder gr = new StringBuilder("");

            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol.Count; i++)
                {
                    gr.Append(Quote(CsvCol[i].Text)); gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);

                foreach (PartReceivingInquiry d in model)
                {
                    csvAddLine(gr, new string[] {
                    Equs(d.BUYERPD_CD), 
                    Equs(d.TMMIN_ORDER_NO), 
                    Equs(d.TMAP_ORDER_NO), 
                    Equs(d.MANIFEST_NO),
                    Equs(d.PART_NO), 
                    Equs(d.KANBAN_ID),
                    Equs(d.TRANSPORTATION_CD), 
                    Equs(d.PRIVILEGE),
                    Equs(d.HANDLING_TYPE), 
                    Equs(d.PART_STATUS),
                    Equs(d.PLAN_RECEIVE),
                    Equs(d.RECEIVED_DT),

                    Equs(d.SUPPLIER_CD),
                    Equs(d.SUPPLIER_PLANT),
                    Equs(d.SUB_SUPPLIER_CD),
                    Equs(d.SUB_SUPPLIER_PLANT)
                    });
                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        private string _csv_sep;
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {
                    _csv_sep = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ";";
                }
                return _csv_sep;
            }
        }

        private List<CsvColumn> CsvCol = CsvColumn.Parse(
            "Buyer PD|C|10|0|BUYERPD_CD;" +
            "TMMIN Order No|C|12|0|TMMIN_ORDER_NO;" +
            "TMAP Order No|C|12|0|TMAP_ORDER_NO;" +
            "Manifest No|C|16|0|MANIFEST_NO;" +
            "Part No|C|12|0|PART_NO;" +
            "Kanban ID|C|25|0|KANBAN_ID;" +
            "Trans Code|C|10|0|TRANSPORTATION_CD;" +
            "Privilege|C|10|0|PRIVILEGE;" +
            "Handling Type|C|10|0|HANDLING_TYPE;" +
            "Part Status|C|15|0|PART_STATUS;" +
            "Plan Receive|C|15|0|PLAN_RECEIVE;" +
            "Sortir Date|C|15|0|RECEIVED_DT;" +

            "Supplier Code|C|4|0|SUPPLIER_CD;" +
            "Supplier Plant|C|1|0|SUPPLIER_PLANT;" +
            "Sub Supplier Code|C|4|0|SUB_SUPPLIER_CD;" +
            "Sub Supplier Plant|C|1|0|SUB_SUPPLIER_PLANT;"
            );

        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        /// <summary>
        /// put Equal Sign in front and quote char to prevent 'numerization' by Excel
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string Equs(string s)
        {
            string separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (s != null)
            {
                if (s.Contains(separator))
                {
                    s = "\"" + s + "\"";
                }
                else
                {
                    s = "=\"" + s + "\"";
                }
            }
            return s;
        }

        public class CsvColumn
        {
            public string Text { get; set; }
            public string Column { get; set; }
            public string DataType { get; set; }
            public int Len { get; set; }
            public int Mandatory { get; set; }

            public static List<CsvColumn> Parse(string v)
            {
                List<CsvColumn> l = new List<CsvColumn>();

                string[] x = v.Split(';');


                for (int i = 0; i < x.Length; i++)
                {
                    string[] y = x[i].Split('|');
                    CsvColumn me = null;
                    if (y.Length >= 4)
                    {
                        int len = 0;
                        int mandat = 0;
                        int.TryParse(y[2], out len);
                        int.TryParse(y[3], out mandat);
                        me = new CsvColumn()
                        {
                            Text = y[0],
                            DataType = y[1],
                            Len = len,
                            Mandatory = mandat
                        };
                        l.Add(me);
                    }

                    if (y.Length > 4 && me != null)
                    {
                        me.Column = y[4];
                    }
                }
                return l;
            }
        }
        #endregion
    }
}

