﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Collections;

namespace SPEX.Controllers
{
    public class FormD_Data_UploadController : PageController
    {
        //
        // GET: /BuyerPD/
        //mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();
        public string moduleID ;
        public string functionID ;
        public string location = "Form D data upload";
        Log log = new Log();
        public const string UploadDirectory = "Content\\FileUploadResult";
        public string TemplateFName;
        public FormD_Data_UploadController()
        {
            Settings.Title = "Form D Data Upload";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //Call error message
            //ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("VESSEL SCHEDULE Master");
            //ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            //ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            //GetBuyyerForCombo();
            //ViewData["Packing_Company"] = GetPackingCompany();
            getpE_UserId = (User)ViewData["User"];
        }

       
        //public ActionResult UploadExcel(HttpPostedFileBase fileexcel)
        //{
        //    DeleteAllOlderFile(HttpContext.Request.MapPath("~/Content/FileUploadResult/"));

        //    string FileName = fileexcel.FileName;
        //    //string fileName = Path.Combine(fileexcel.FileName);
        //    long pid = 0;
        //    string batchName = "SynchronizeBatch";
        //    string moduleID = "1";
        //    string functionID = "10011";
        //    string subFuncName = "UPLOAD DATA";
        //    string userBatch = getpE_UserId.Username;
        //    string location = "VESSEL_SCHEDULE";
        //    Log log = new Log();
        //    pid = log.createLog(msgError.getMSPX00005INFForLog(batchName), userBatch, location + "." + subFuncName, 0, moduleID, functionID);
        //    var path_file = Path.Combine(Server.MapPath("~/FileUpload/RunDown/UploadCustomVESSELScreen/CustomVESSEL"), FileName);
        //    string customVESSEL_dir = Server.MapPath("~/FileUpload/RunDown/UploadCustomVESSELScreen/CustomVESSEL");
        //    if (Directory.Exists(customVESSEL_dir))
        //    {
        //        fileexcel.SaveAs(path_file);
        //    }

        //    return RedirectToAction("");
        //}


        // //function download      
        // public void DownloadBuyerPD(object sender, EventArgs e, string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION)
        // {
        //     string filename = "";
        //     string filesTmp = HttpContext.Request.MapPath("~/Template/BuyerPD_Code_Download.xls");
        //     FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

        //     HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

        //     ICellStyle styleContent = workbook.CreateCellStyle();
        //     styleContent.VerticalAlignment = VerticalAlignment.Top;
        //     styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent.Alignment = HorizontalAlignment.Left;

        //     ICellStyle styleContent2 = workbook.CreateCellStyle();
        //     styleContent2.VerticalAlignment = VerticalAlignment.Top;
        //     styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent2.Alignment = HorizontalAlignment.Center;

        //     ICellStyle styleContent3 = workbook.CreateCellStyle();
        //     styleContent3.VerticalAlignment = VerticalAlignment.Top;
        //     styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent3.Alignment = HorizontalAlignment.Right;

        //     ISheet sheet = workbook.GetSheet("BuyerPD Code");
        //     string date = DateTime.Now.ToString("ddMMyyyy");
        //     filename = "BuyerPD Code Master" + date + ".xls";

        //     string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

        //     sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
        //     sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);
        //     sheet.GetRow(8).GetCell(2).SetCellValue(pBUYER_PD_CD);
        //     sheet.GetRow(9).GetCell(2).SetCellValue(pCUSTOMER_NO);
        //     sheet.GetRow(10).GetCell(2).SetCellValue(pDESCRIPTION);


        //     int row = 13;
        //     int rowNum = 1;
        //     IRow Hrow;

        //     List<mBuyerPD> model = new List<mBuyerPD>();

        //     model = BuyerPD.getListBuyerPD
        //         (pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION);

        //     foreach (var result in model)
        //     {
        //         Hrow = sheet.CreateRow(row);

        //         Hrow.CreateCell(1).SetCellValue(rowNum);
        //         Hrow.CreateCell(2).SetCellValue(result.BUYER_PD_CD);
        //         Hrow.CreateCell(3).SetCellValue(result.CUSTOMER_NO);
        //         Hrow.CreateCell(4).SetCellValue(result.DESCRIPTION);
        //         Hrow.CreateCell(5).SetCellValue(result.DELETION_FLAG);
        //         Hrow.CreateCell(6).SetCellValue(result.CREATED_BY);
        //         Hrow.CreateCell(7).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
        //         Hrow.CreateCell(8).SetCellValue(result.CHANGED_BY);
        //         Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

        //         Hrow.GetCell(1).CellStyle = styleContent3;
        //         Hrow.GetCell(2).CellStyle = styleContent2;
        //         Hrow.GetCell(3).CellStyle = styleContent2;
        //         Hrow.GetCell(4).CellStyle = styleContent;
        //         Hrow.GetCell(5).CellStyle = styleContent2;
        //         Hrow.GetCell(6).CellStyle = styleContent2;
        //         Hrow.GetCell(7).CellStyle = styleContent2;
        //         Hrow.GetCell(8).CellStyle = styleContent2;
        //         Hrow.GetCell(9).CellStyle = styleContent2;

        //         row++;
        //         rowNum++;
        //     }

        //     MemoryStream ms = new MemoryStream();
        //     workbook.Write(ms);
        //     ftmp.Close();
        //     Response.BinaryWrite(ms.ToArray());
        //     Response.ContentType = "application/vnd.ms-excel";
        //     Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        // }

        

        // #region Function Add
        //public ActionResult AddNewVESSEL_AGENT(string SHIP_AGENT_CD, string SHIP_AGENT_NAME, string PIC_NAME, string CONTACT_NO, string DESCR1, string DESCR2, string DESCR3)
        //{
        //    //p_CREATED_BY = getpE_UserId.Username;
        //    //p_CREATED_BY = "SYSTEM";
        //    string[] r = null;
        //    VESSEL_AGENT ship = new VESSEL_AGENT();
        //    ship.SHIP_AGENT_CD = SHIP_AGENT_CD;
        //    ship.SHIP_AGENT_NAME = SHIP_AGENT_NAME;
        //    ship.PIC_NAME = PIC_NAME;
        //    ship.CONTACT_NO = CONTACT_NO;
        //    ship.DESCR1 = DESCR1;
        //    ship.DESCR2 = DESCR2;
        //    ship.DESCR3 = DESCR3;
        //    ship.CREATED_BY = getpE_UserId.Username;
        //    string resultMessage = ship.SaveData(ship);

        //    r = resultMessage.Split('|');
        //    if (r[0] == "Error ")
        //    {
        //        return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
        //    }
        //    return null;
        //}
        //#endregion
        //public void DoWork(object obj)
        //{
        //    // do some work
        //    string[] objs = obj.ToString().Split('|');
        //    long pid=Convert.ToInt64(objs[0]);
        //    string user_id =objs[1].ToString(); //getpE_UserId.Username;
        //    new VESSEL_SCHEDULE().Save_Master_VESSEL_SCHEDULE(Convert.ToInt64(pid), user_id,functionID,moduleID);
        //}

        //#region Function Edit -
        public ActionResult UploadBackgroundProsessInvoiceHeader(string pid)
        {
            string message = string.Empty;
            try
            {
                string user_id = getpE_UserId.Username;
                string subFuncName = "EXcelToSQL";
                moduleID="FDD";
                functionID="FDD-IH";

                //long pid = log.createLog(msgError.getMSPX00005INFForLog("upload invoice header for FormD"), user_id, location + '.' + subFuncName, 0, moduleID, functionID);

                new FormD_Data_Upload().Save_InvoiceHeader(Convert.ToInt64(pid), user_id, functionID, moduleID);
                message = msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2];
                return Json(new { success = "true", messages = message + '|' + pid }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion
        public ActionResult CallBackInvoiceHeadertUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButtonInvoiceHeader", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public ActionResult CallBackInvoiceDetailtUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButtonInvoiceDetail", UploadControlHelper.ValidationSettings, uc_FileInvoiceDetailUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "Price_Master_Upload";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xls" },
                MaxFileSize = 20971520
            };
        }

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            //VESSEL_SCHEDULE Ship = new VESSEL_SCHEDULE();

            string[] r = null;
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_IH";
            functionID = "FDD-IH";
            moduleID = "FDD";
            
            
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            string MESSAGE_DETAIL = "";
            long pid = 0;
            string subFuncName = "excel to tb t ih";
            string isError = "N";
            string processName = "FDD-IH. Function temporary [temp upload invoice header for FormD] ";
            string resultMessage = "";
            int columns;
            int rows;

            DeleteAllOlderFile(HttpContext.Request.MapPath("~/Content/FileUploadResult/"));
            
            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock L = new Lock();
            FormD_Data_Upload up = new FormD_Data_Upload();
            int IsLock = L.is_lock(functionID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMsgText("MSPX00092ERR");
            }
            else if (getpE_UserId.Username==null)
            {
                rExtract = "3| session expired please re login" ;
            }
            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                pid = log.createLog(msgError.getMSG_Spex("MSPXFD001INF", pid), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);

                rExtract = up.DeleteData_TB_T_IH();
                if (rExtract == "SUCCES")
                {
                    string user_id = getpE_UserId.Username;

                    L.CREATED_BY = user_id;
                    L.CREATED_DT = DateTime.Now;
                    L.PROCESS_ID = pid;
                    L.FUNCTION_ID = functionID;
                    L.LOCK_REF = functionID;
                    L.LockFunction(L);
                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    //Function untuk nge-extract file excel
                    rExtract = ReadAndInsertExcelDataInvoiceHeader(pathfile);
                    rExtract = rExtract + '|' + pid.ToString();
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                    //if (rExtract == string.Empty || rExtract == "SUCCESS")
                    //{
                    //    rExtract = up.CheckExistingDataInvoiceHeaderTemp; //Ship.CheckExistingDataInvoiceHeaderTemp();
                    //}
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                //MESSAGE_DETAIL = "File is not .xls";
                //pid = log.createLog(msgError.getMSPX00019ERR("Price"), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                rExtract = "1| File is not .xls";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract;//+ "|" + pid.ToString();
        }


        public void uc_FileInvoiceDetailUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            //VESSEL_SCHEDULE Ship = new VESSEL_SCHEDULE();

            string[] r = null;
            string rExtract = string.Empty;
            
            functionID = "FDD-ID";
            moduleID = "FDD";


            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            string MESSAGE_DETAIL = "";
            long pid = 0;
            string subFuncName = "excel to tb t iD";
            string isError = "N";
            string processName = "FDD-IH. Function temporary [temp upload invoice header for FormD] ";
            string resultMessage = "";
            int columns;
            int rows;

            DeleteAllOlderFile(HttpContext.Request.MapPath("~/Content/FileUploadResult/"));

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock L = new Lock();
            FormD_Data_Upload up = new FormD_Data_Upload();
            int IsLock = L.is_lock(functionID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMsgText("MSPX00092ERR");
            }
            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                pid = log.createLog(msgError.getMSG_Spex("MSPXFD001INF", pid), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);

                rExtract = up.DeleteDataTB_T_ID();
                if (rExtract == "SUCCES")
                {
                    string user_id = getpE_UserId.Username;

                    L.CREATED_BY = user_id;
                    L.CREATED_DT = DateTime.Now;
                    L.PROCESS_ID = pid;
                    L.FUNCTION_ID = functionID;
                    L.LOCK_REF = functionID;
                    L.LockFunction(L);
                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    //Function untuk nge-extract file excel
                    rExtract = ReadAndInsertExcelDataInvoiceDetail(pathfile);
                    rExtract = rExtract + '|' + pid.ToString();
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                    //if (rExtract == string.Empty || rExtract == "SUCCESS")
                    //{
                    //    rExtract = up.CheckExistingDataInvoiceHeaderTemp; //Ship.CheckExistingDataInvoiceHeaderTemp();
                    //}
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                //MESSAGE_DETAIL = "File is not .xls";
                //pid = log.createLog(msgError.getMSPX00019ERR("Price"), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                rExtract = "1| File is not .xls";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract;//+ "|" + pid.ToString();
        }

        public void UnLock()
        {
            Lock L = new Lock();
            L.UnlockFunction(functionID);
        }

        public ActionResult IsCheckLock(string form_d_upload_name)
        {
            Lock L = new Lock();
            if (form_d_upload_name=="InvoiceHeader")
            {
                functionID = "FDD-IH";
            }
            if (form_d_upload_name == "OrderData")
            {
                functionID = "FDD-OD";
            }
            if (form_d_upload_name=="InvoiceDetail")
            {
                functionID = "FDD-ID";
            }
            int Lock = L.is_lock(functionID);

            if (Lock == 0)
            {
                return Json(new { success = "true", messages = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string msg = string.Empty;
                msg = msgError.getMsgText("MSPX00088ERR");
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        private void DeleteAllOlderFile(string folderPath)
        {
            IEnumerable<string> allFiles = new List<string>();
            allFiles = System.IO.Directory.EnumerateFiles(folderPath, "*.*");
            foreach (string file in allFiles)
            {
                FileInfo info = new FileInfo(file);
                if (info.CreationTime < DateTime.Now.AddDays(-3))
                    System.IO.File.Delete(file);
            }
        }
        

        private string ReadAndInsertExcelDataInvoiceHeader(string FileName)
        {
            try
            {
                //XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                //ISheet sheet = xssfworkbook.GetSheetAt(0);
                //int lastrow = sheet.LastRowNum;
                List<FormD_Data_Upload.InvoiceHeaderUpload> ListInvoiceHeader = new List<FormD_Data_Upload.InvoiceHeaderUpload>();
                //int index = 1;

                FormD_Data_Upload.InvoiceHeaderUpload IH;
                //for (int i = index; i <= lastrow; i++)
                //{
                HSSFWorkbook workbook = null;
                using (FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read))
                {
                    workbook = new HSSFWorkbook(fs);
                }
                HSSFSheet sheet = workbook.GetSheetAt(0) as HSSFSheet;
                IEnumerator enumerator = sheet.GetRowEnumerator();
                FormD_Data_Upload.Orderdata OD;
                while (enumerator.MoveNext())
                {
                    HSSFRow row = enumerator.Current as HSSFRow;
                    //}

                    if (row.RowNum > 0)
                    {
                        IH = new FormD_Data_Upload.InvoiceHeaderUpload();
                        //IRow row = sheet.GetRow(i);

                        //if (row == null) continue;
                        //if (row.GetCell(0) == null && row.GetCell(1) == null && row.GetCell(2) == null && row.GetCell(3) == null) continue;

                        //I9INVN
                        if (row.GetCell(0) == null)
                            IH.I9INVN = "";
                        else if (row.GetCell(0).CellType == CellType.Blank || (row.GetCell(0).CellType == CellType.String && (row.GetCell(0).StringCellValue == null || row.GetCell(0).StringCellValue == "")))
                            IH.I9INVN = "";
                        else if (row.GetCell(0).CellType == CellType.String)
                            IH.I9INVN = row.GetCell(0).StringCellValue;
                        else
                            IH.I9INVN = row.GetCell(0).ToString();

                        //I9INVD
                        if (row.GetCell(1) == null) IH.I9INVD = "";
                        else if (row.GetCell(1).CellType == CellType.Blank || (row.GetCell(1).CellType == CellType.String && (row.GetCell(1).StringCellValue == null || row.GetCell(1).StringCellValue == "")))
                            IH.I9INVD = "";
                        else if (row.GetCell(1).CellType == CellType.String)
                            IH.I9INVD = row.GetCell(1).StringCellValue;
                        else
                            IH.I9INVD = row.GetCell(1).ToString();

                        //I9NAME
                        if (row.GetCell(2) == null) IH.I9NAME = "";
                        else if (row.GetCell(2).CellType == CellType.Blank || (row.GetCell(2).CellType == CellType.String && (row.GetCell(2).StringCellValue == null || row.GetCell(2).StringCellValue == "")))
                            IH.I9NAME = "";
                        else if (row.GetCell(2).CellType == CellType.Numeric && (row.GetCell(2).NumericCellValue.ToString() == ""))
                            IH.I9NAME = "";
                        else if (row.GetCell(2).CellType == CellType.String)
                            IH.I9NAME = row.GetCell(2).StringCellValue;
                        else
                            IH.I9NAME = row.GetCell(2).ToString();

                        //I9ADR1
                        if (row.GetCell(3) == null) IH.I9ADR1 = "";
                        else if (row.GetCell(3).CellType == CellType.Blank || (row.GetCell(3).CellType == CellType.String && (row.GetCell(3).StringCellValue == null || row.GetCell(3).StringCellValue == "")))
                            IH.I9ADR1 = "";
                        else if (row.GetCell(3).CellType == CellType.Numeric && (row.GetCell(3).NumericCellValue.ToString() == ""))
                            IH.I9ADR1 = "";
                        else if (row.GetCell(3).CellType == CellType.String)
                            IH.I9ADR1 = row.GetCell(3).StringCellValue;
                        else
                            IH.I9ADR1 = row.GetCell(3).ToString();

                        //I9ADR2
                        if (row.GetCell(4) == null) IH.I9ADR2 = "";
                        else if (row.GetCell(4).CellType == CellType.Blank || (row.GetCell(4).CellType == CellType.String && (row.GetCell(4).StringCellValue == null || row.GetCell(4).StringCellValue == "")))
                            IH.I9ADR2 = "";
                        else if (row.GetCell(4).CellType == CellType.Numeric && (row.GetCell(4).NumericCellValue.ToString() == ""))
                            IH.I9ADR2 = "";
                        else if (row.GetCell(4).CellType == CellType.String)
                            IH.I9ADR2 = row.GetCell(4).StringCellValue;
                        else
                            IH.I9ADR2 = row.GetCell(4).ToString();

                        //I9ADR3
                        if (row.GetCell(5) == null)
                            IH.I9ADR3 = "";
                        else if (row.GetCell(5).CellType == CellType.Blank || (row.GetCell(5).CellType == CellType.String && (row.GetCell(5).StringCellValue == null || row.GetCell(5).StringCellValue == "")))
                            IH.I9ADR3 = "";
                        else if (row.GetCell(5).CellType == CellType.Numeric && (row.GetCell(5).NumericCellValue.ToString() == ""))
                            IH.I9ADR3 = "";
                        else if (row.GetCell(5).CellType == CellType.String)
                            IH.I9ADR3 = row.GetCell(5).StringCellValue;
                        else
                            IH.I9ADR3 = row.GetCell(5).ToString();

                        //I9LOAD
                        if (row.GetCell(6) == null) IH.I9LOAD = "";
                        else if (row.GetCell(6).CellType == CellType.Blank || (row.GetCell(6).CellType == CellType.String && (row.GetCell(6).StringCellValue == null || row.GetCell(6).StringCellValue == "")))
                            IH.I9LOAD = "";
                        else if (row.GetCell(6).CellType == CellType.Numeric && (row.GetCell(6).NumericCellValue.ToString() == ""))
                            IH.I9LOAD = "";
                        else if (row.GetCell(6).CellType == CellType.String)
                            IH.I9LOAD = row.GetCell(6).StringCellValue;
                        else
                            IH.I9LOAD = row.GetCell(6).ToString();

                        //I9DISC
                        if (row.GetCell(7) == null) IH.I9DISC = "";
                        else if (row.GetCell(7).CellType == CellType.Blank || (row.GetCell(7).CellType == CellType.String && (row.GetCell(7).StringCellValue == null || row.GetCell(7).StringCellValue == "")))
                            IH.I9DISC = "";
                        else if (row.GetCell(7).CellType == CellType.Numeric && (row.GetCell(7).NumericCellValue.ToString() == ""))
                            IH.I9DISC = "";
                        else if (row.GetCell(7).CellType == CellType.String)
                            IH.I9DISC = row.GetCell(7).StringCellValue;
                        else
                            IH.I9DISC = row.GetCell(7).ToString();

                        //I9VESL
                        if (row.GetCell(8) == null) IH.I9VESL = "";
                        else if (row.GetCell(8).CellType == CellType.Blank || (row.GetCell(8).CellType == CellType.String && (row.GetCell(8).StringCellValue == null || row.GetCell(8).StringCellValue == "")))
                            IH.I9VESL = "";
                        else if (row.GetCell(8).CellType == CellType.Numeric && (row.GetCell(8).NumericCellValue.ToString() == ""))
                            IH.I9VESL = "";
                        else if (row.GetCell(8).CellType == CellType.String)
                            IH.I9VESL = row.GetCell(8).StringCellValue;
                        else
                            IH.I9VESL = row.GetCell(8).ToString();

                        //I9ETD
                        if (row.GetCell(9) == null) IH.I9ETD = "";
                        else if (row.GetCell(9).CellType == CellType.Blank || (row.GetCell(9).CellType == CellType.String && (row.GetCell(9).StringCellValue == null || row.GetCell(9).StringCellValue == "")))
                            IH.I9ETD = "";
                        else if (row.GetCell(9).CellType == CellType.Numeric && (row.GetCell(9).NumericCellValue.ToString() == ""))
                            IH.I9ETD = "";
                        else if (row.GetCell(9).CellType == CellType.String)
                            IH.I9ETD = row.GetCell(9).StringCellValue;
                        else if (row.GetCell(9).CellType == CellType.Numeric)
                        {
                            ICell cell = row.GetCell(9);
                            string formatString = row.GetCell(9).CellStyle.GetDataFormatString();

                            //string value = dataFormatter.FormatCellValue(cell);
                            if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                                IH.I9ETD = row.GetCell(9).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                            else
                                IH.I9ETD = row.GetCell(9).NumericCellValue.ToString();
                        }
                        else
                            IH.I9ETD = row.GetCell(9).ToString();

                        //I9PAY1
                        if (row.GetCell(10) == null)
                            IH.I9PAY1 = "";
                        else if (row.GetCell(10).CellType == CellType.Blank || (row.GetCell(10).CellType == CellType.String && (row.GetCell(10).StringCellValue == null || row.GetCell(10).StringCellValue == "")))
                            IH.I9PAY1 = "";
                        else if (row.GetCell(10).CellType == CellType.Numeric && (row.GetCell(10).NumericCellValue.ToString() == ""))
                            IH.I9PAY1 = "";
                        else if (row.GetCell(10).CellType == CellType.String)
                            IH.I9PAY1 = row.GetCell(10).StringCellValue;
                        else
                            IH.I9PAY1 = row.GetCell(10).ToString();

                        //I9PAY2
                        if (row.GetCell(11) == null) IH.I9PAY2 = "";
                        else if (row.GetCell(11).CellType == CellType.Blank || (row.GetCell(11).CellType == CellType.String && (row.GetCell(11).StringCellValue == null || row.GetCell(11).StringCellValue == "")))
                            IH.I9PAY2 = "";
                        else if (row.GetCell(11).CellType == CellType.Numeric && (row.GetCell(11).NumericCellValue.ToString() == ""))
                            IH.I9PAY2 = "";
                        else if (row.GetCell(11).CellType == CellType.String)
                            IH.I9PAY2 = row.GetCell(11).StringCellValue;
                        else
                            IH.I9PAY2 = row.GetCell(11).ToString();

                        //I9SPC1
                        if (row.GetCell(12) == null)
                            IH.I9SPC1 = "";
                        else if (row.GetCell(12).CellType == CellType.Blank || (row.GetCell(12).CellType == CellType.String && (row.GetCell(12).StringCellValue == null || row.GetCell(12).StringCellValue == "")))
                            IH.I9SPC1 = "";
                        else if (row.GetCell(12).CellType == CellType.Numeric && (row.GetCell(12).NumericCellValue.ToString() == ""))
                            IH.I9SPC1 = "";
                        else if (row.GetCell(12).CellType == CellType.String)
                            IH.I9SPC1 = row.GetCell(12).StringCellValue;
                        else
                            IH.I9SPC1 = row.GetCell(12).ToString();


                        //I9SPC2
                        if (row.GetCell(13) == null) IH.I9SPC2 = "";
                        else if (row.GetCell(13).CellType == CellType.Blank || (row.GetCell(13).CellType == CellType.String && (row.GetCell(13).StringCellValue == null || row.GetCell(13).StringCellValue == "")))
                            IH.I9SPC2 = "";
                        else if (row.GetCell(13).CellType == CellType.Numeric && (row.GetCell(13).NumericCellValue.ToString() == ""))
                            IH.I9SPC2 = "";
                        else if (row.GetCell(13).CellType == CellType.String)
                            IH.I9SPC2 = row.GetCell(13).StringCellValue;
                        else
                            IH.I9SPC2 = row.GetCell(13).ToString();

                        //I9SPC3
                        if (row.GetCell(14) == null)
                            IH.I9SPC3 = "";
                        else if (row.GetCell(14).CellType == CellType.Blank || (row.GetCell(14).CellType == CellType.String && (row.GetCell(14).StringCellValue == null || row.GetCell(14).StringCellValue == "")))
                            IH.I9SPC3 = "";
                        else if (row.GetCell(14).CellType == CellType.Numeric && (row.GetCell(14).NumericCellValue.ToString() == ""))
                            IH.I9SPC3 = "";
                        else if (row.GetCell(14).CellType == CellType.String)
                            IH.I9SPC3 = row.GetCell(14).StringCellValue;
                        else
                            IH.I9SPC3 = row.GetCell(14).ToString();

                        //I9SPC4
                        if (row.GetCell(15) == null) IH.I9SPC4 = "";
                        else if (row.GetCell(15).CellType == CellType.Blank || (row.GetCell(15).CellType == CellType.String && (row.GetCell(15).StringCellValue == null || row.GetCell(15).StringCellValue == "")))
                            IH.I9SPC4 = "";
                        else if (row.GetCell(15).CellType == CellType.Numeric && (row.GetCell(15).NumericCellValue.ToString() == ""))
                            IH.I9SPC4 = "";
                        else if (row.GetCell(15).CellType == CellType.String)
                            IH.I9SPC4 = row.GetCell(15).StringCellValue;
                        else
                            IH.I9SPC4 = row.GetCell(15).ToString();

                        //I9SPC5
                        if (row.GetCell(16) == null) IH.I9SPC5 = "";
                        else if (row.GetCell(16).CellType == CellType.Blank || (row.GetCell(16).CellType == CellType.String && (row.GetCell(16).StringCellValue == null || row.GetCell(16).StringCellValue == "")))
                            IH.I9SPC5 = "";
                        else if (row.GetCell(16).CellType == CellType.Numeric && (row.GetCell(16).NumericCellValue.ToString() == ""))
                            IH.I9SPC5 = "";
                        else if (row.GetCell(16).CellType == CellType.String)
                            IH.I9SPC5 = row.GetCell(16).StringCellValue;
                        else
                            IH.I9SPC5 = row.GetCell(16).ToString();

                        //I9TQTY
                        if (row.GetCell(17) == null) IH.I9TQTY = "";
                        else if (row.GetCell(17).CellType == CellType.Blank || (row.GetCell(17).CellType == CellType.String && (row.GetCell(17).StringCellValue == null || row.GetCell(17).StringCellValue == "")))
                            IH.I9TQTY = "";
                        else if (row.GetCell(17).CellType == CellType.Numeric && (row.GetCell(17).NumericCellValue.ToString() == ""))
                            IH.I9TQTY = "";
                        else if (row.GetCell(17).CellType == CellType.String)
                            IH.I9TQTY = row.GetCell(17).StringCellValue;
                        else
                            IH.I9TQTY = row.GetCell(17).ToString();

                        //I9FOBA
                        if (row.GetCell(18) == null) IH.I9FOBA = "";
                        else if (row.GetCell(18).CellType == CellType.Blank || (row.GetCell(18).CellType == CellType.String && (row.GetCell(18).StringCellValue == null || row.GetCell(18).StringCellValue == "")))
                            IH.I9FOBA = "";
                        else if (row.GetCell(18).CellType == CellType.Numeric && (row.GetCell(18).NumericCellValue.ToString() == ""))
                            IH.I9FOBA = "";
                        else if (row.GetCell(18).CellType == CellType.String)
                            IH.I9FOBA = row.GetCell(18).StringCellValue;
                        else
                            IH.I9FOBA = row.GetCell(18).ToString();

                        //I9FRGA
                        if (row.GetCell(19) == null) IH.I9FRGA = "";
                        else if (row.GetCell(19).CellType == CellType.Blank || (row.GetCell(19).CellType == CellType.String && (row.GetCell(19).StringCellValue == null || row.GetCell(19).StringCellValue == "")))
                            IH.I9FRGA = "";
                        else if (row.GetCell(19).CellType == CellType.Numeric && (row.GetCell(19).NumericCellValue.ToString() == ""))
                            IH.I9FRGA = "";
                        else if (row.GetCell(19).CellType == CellType.String)
                            IH.I9FRGA = row.GetCell(19).StringCellValue;
                        else
                            IH.I9FRGA = row.GetCell(19).ToString();

                        //I9INSA
                        if (row.GetCell(20) == null) IH.I9INSA = "";
                        else if (row.GetCell(20).CellType == CellType.Blank || (row.GetCell(20).CellType == CellType.String && (row.GetCell(20).StringCellValue == null || row.GetCell(20).StringCellValue == "")))
                            IH.I9INSA = "";
                        else if (row.GetCell(20).CellType == CellType.Numeric && (row.GetCell(20).NumericCellValue.ToString() == ""))
                            IH.I9INSA = "";
                        else if (row.GetCell(20).CellType == CellType.String)
                            IH.I9INSA = row.GetCell(20).StringCellValue;
                        else
                            IH.I9INSA = row.GetCell(20).ToString();

                        //I9PRCT
                        if (row.GetCell(21) == null) IH.I9PRCT = "";
                        else if (row.GetCell(21).CellType == CellType.Blank || (row.GetCell(21).CellType == CellType.String && (row.GetCell(21).StringCellValue == null || row.GetCell(21).StringCellValue == "")))
                            IH.I9PRCT = "";
                        else if (row.GetCell(21).CellType == CellType.Numeric && (row.GetCell(21).NumericCellValue.ToString() == ""))
                            IH.I9PRCT = "";
                        else if (row.GetCell(21).CellType == CellType.String)
                            IH.I9PRCT = row.GetCell(21).StringCellValue;
                        else
                            IH.I9PRCT = row.GetCell(21).ToString();

                        //I9TOTL
                        if (row.GetCell(22) == null) IH.I9TOTL = "";
                        else if (row.GetCell(22).CellType == CellType.Blank || (row.GetCell(22).CellType == CellType.String && (row.GetCell(22).StringCellValue == null || row.GetCell(22).StringCellValue == "")))
                            IH.I9TOTL = "";
                        else if (row.GetCell(22).CellType == CellType.Numeric && (row.GetCell(22).NumericCellValue.ToString() == ""))
                            IH.I9TOTL = "";
                        else if (row.GetCell(22).CellType == CellType.String)
                            IH.I9TOTL = row.GetCell(22).StringCellValue;
                        else
                            IH.I9TOTL = row.GetCell(22).ToString();

                        //I9TOTC
                        if (row.GetCell(23) == null)
                            IH.I9TOTC = "";
                        else if (row.GetCell(23).CellType == CellType.Blank || (row.GetCell(23).CellType == CellType.String && (row.GetCell(23).StringCellValue == null || row.GetCell(23).StringCellValue == "")))
                            IH.I9TOTC = "";
                        else if (row.GetCell(23).CellType == CellType.Numeric && (row.GetCell(23).NumericCellValue.ToString() == ""))
                            IH.I9TOTC = "";
                        else if (row.GetCell(23).CellType == CellType.String)
                            IH.I9TOTC = row.GetCell(23).StringCellValue;
                        else
                            IH.I9TOTC = row.GetCell(23).ToString();

                        //I9GROS
                        if (row.GetCell(24) == null)
                            IH.I9GROS = "";
                        else if (row.GetCell(24).CellType == CellType.Blank || (row.GetCell(24).CellType == CellType.String && (row.GetCell(24).StringCellValue == null || row.GetCell(24).StringCellValue == "")))
                            IH.I9GROS = "";
                        else if (row.GetCell(24).CellType == CellType.Numeric && (row.GetCell(24).NumericCellValue.ToString() == ""))
                            IH.I9GROS = "";
                        else if (row.GetCell(23).CellType == CellType.String)
                            IH.I9GROS = row.GetCell(24).StringCellValue;
                        else
                            IH.I9GROS = row.GetCell(24).ToString();

                        //I9NET
                        if (row.GetCell(25) == null)
                            IH.I9NET = "";
                        else if (row.GetCell(25).CellType == CellType.Blank || (row.GetCell(25).CellType == CellType.String && (row.GetCell(25).StringCellValue == null || row.GetCell(25).StringCellValue == "")))
                            IH.I9NET = "";
                        else if (row.GetCell(25).CellType == CellType.Numeric && (row.GetCell(25).NumericCellValue.ToString() == ""))
                            IH.I9NET = "";
                        else if (row.GetCell(25).CellType == CellType.String)
                            IH.I9NET = row.GetCell(25).StringCellValue;
                        else
                            IH.I9NET = row.GetCell(25).ToString();


                        //I9MEAS
                        if (row.GetCell(26) == null)
                            IH.I9MEAS = "";
                        else if (row.GetCell(26).CellType == CellType.Blank || (row.GetCell(26).CellType == CellType.String && (row.GetCell(26).StringCellValue == null || row.GetCell(26).StringCellValue == "")))
                            IH.I9MEAS = "";
                        else if (row.GetCell(26).CellType == CellType.Numeric && (row.GetCell(26).NumericCellValue.ToString() == ""))
                            IH.I9MEAS = "";
                        else if (row.GetCell(26).CellType == CellType.String)
                            IH.I9MEAS = row.GetCell(26).StringCellValue;
                        else
                            IH.I9MEAS = row.GetCell(26).ToString();


                        //I9FPSN
                        if (row.GetCell(27) == null)
                            IH.I9FPSN = "";
                        else if (row.GetCell(27).CellType == CellType.Blank || (row.GetCell(27).CellType == CellType.String && (row.GetCell(27).StringCellValue == null || row.GetCell(27).StringCellValue == "")))
                            IH.I9FPSN = "";
                        else if (row.GetCell(27).CellType == CellType.Numeric && (row.GetCell(27).NumericCellValue.ToString() == ""))
                            IH.I9FPSN = "";
                        else if (row.GetCell(27).CellType == CellType.String)
                            IH.I9FPSN = row.GetCell(27).StringCellValue;
                        else
                            IH.I9FPSN = row.GetCell(27).ToString();

                        //I9FPRT
                        if (row.GetCell(28) == null)
                            IH.I9FPRT = "";
                        else if (row.GetCell(28).CellType == CellType.Blank || (row.GetCell(28).CellType == CellType.String && (row.GetCell(28).StringCellValue == null || row.GetCell(28).StringCellValue == "")))
                            IH.I9FPRT = "";
                        else if (row.GetCell(28).CellType == CellType.Numeric && (row.GetCell(28).NumericCellValue.ToString() == ""))
                            IH.I9FPRT = "";
                        else if (row.GetCell(28).CellType == CellType.String)
                            IH.I9FPRT = row.GetCell(28).StringCellValue;
                        else
                            IH.I9FPRT = row.GetCell(28).ToString();

                        //I9FPHJ
                        if (row.GetCell(29) == null)
                            IH.I9FPHJ = "";
                        else if (row.GetCell(29).CellType == CellType.Blank || (row.GetCell(29).CellType == CellType.String && (row.GetCell(29).StringCellValue == null || row.GetCell(29).StringCellValue == "")))
                            IH.I9FPHJ = "";
                        else if (row.GetCell(29).CellType == CellType.Numeric && (row.GetCell(29).NumericCellValue.ToString() == ""))
                            IH.I9FPHJ = "";
                        else if (row.GetCell(29).CellType == CellType.String)
                            IH.I9FPHJ = row.GetCell(29).StringCellValue;
                        else
                            IH.I9FPHJ = row.GetCell(29).ToString();
                        ListInvoiceHeader.Add(IH);
                    }
                }

                foreach (FormD_Data_Upload.InvoiceHeaderUpload data in ListInvoiceHeader)
                {
                    new FormD_Data_Upload().InsertTempInvoiceHeaderUpload(data, getpE_UserId.Username);
                }

                return "0|SUCCESS";
            }
            catch (Exception ex)
            {
                return "1|" + ex.Message;
            }
        }

        private string ReadAndInsertExcelDataInvoiceDetail(string FileName)
        {
            try
            {
                //XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                //ISheet sheet = xssfworkbook.GetSheetAt(0);
                //int lastrow = sheet.LastRowNum;
                List<FormD_Data_Upload.InvoiceDetailUpload> ListInvoiceDetail = new List<FormD_Data_Upload.InvoiceDetailUpload>();
                //int index = 1;

                FormD_Data_Upload.InvoiceDetailUpload ID;
                HSSFWorkbook workbook = null;
                using (FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read))
                {
                    workbook = new HSSFWorkbook(fs);
                }
                HSSFSheet sheet = workbook.GetSheetAt(0) as HSSFSheet;
                IEnumerator enumerator = sheet.GetRowEnumerator();
                FormD_Data_Upload.Orderdata OD;
                while (enumerator.MoveNext())
                {
                    HSSFRow row = enumerator.Current as HSSFRow;
                //}

                    if (row.RowNum > 0)
                    {
                        //for (int i = index; i <= lastrow; i++)
                        //{
                        ID = new FormD_Data_Upload.InvoiceDetailUpload();
                        //IRow row = sheet.GetRow(i);

                        //if (row == null) continue;
                        //if (row.GetCell(0) == null && row.GetCell(1) == null && row.GetCell(2) == null && row.GetCell(3) == null) continue;

                        //F9INVN
                        if (row.GetCell(0) == null)
                            ID.F9INVN = "";
                        else if (row.GetCell(0).CellType == CellType.Blank || (row.GetCell(0).CellType == CellType.String && (row.GetCell(0).StringCellValue == null || row.GetCell(0).StringCellValue == "")))
                            ID.F9INVN = "";
                        else if (row.GetCell(0).CellType == CellType.String)
                            ID.F9INVN = row.GetCell(0).StringCellValue;
                        else
                            ID.F9INVN = row.GetCell(0).ToString();

                        //F9INVD
                        if (row.GetCell(1) == null)
                            ID.F9INVD = "";
                        else if (row.GetCell(1).CellType == CellType.Blank || (row.GetCell(1).CellType == CellType.String && (row.GetCell(1).StringCellValue == null || row.GetCell(1).StringCellValue == "")))
                            ID.F9INVD = "";
                        else if (row.GetCell(1).CellType == CellType.String)
                            ID.F9INVD = row.GetCell(1).StringCellValue;
                        else
                            ID.F9INVD = row.GetCell(1).ToString();

                        //F9SELR
                        if (row.GetCell(2) == null)
                            ID.F9SELR = "";
                        else if (row.GetCell(2).CellType == CellType.Blank || (row.GetCell(2).CellType == CellType.String && (row.GetCell(2).StringCellValue == null || row.GetCell(2).StringCellValue == "")))
                            ID.F9SELR = "";
                        else if (row.GetCell(2).CellType == CellType.String)
                            ID.F9SELR = row.GetCell(2).StringCellValue;
                        else
                            ID.F9SELR = row.GetCell(2).ToString();

                        //F9BYR
                        if (row.GetCell(3) == null)
                            ID.F9BYR = "";
                        else if (row.GetCell(3).CellType == CellType.Blank || (row.GetCell(3).CellType == CellType.String && (row.GetCell(3).StringCellValue == null || row.GetCell(3).StringCellValue == "")))
                            ID.F9BYR = "";
                        else if (row.GetCell(3).CellType == CellType.String)
                            ID.F9BYR = row.GetCell(3).StringCellValue;
                        else
                            ID.F9BYR = row.GetCell(3).ToString();

                        //F9PD
                        if (row.GetCell(4) == null)
                            ID.F9PD = "";
                        else if (row.GetCell(4).CellType == CellType.Blank || (row.GetCell(4).CellType == CellType.String && (row.GetCell(4).StringCellValue == null || row.GetCell(4).StringCellValue == "")))
                            ID.F9PD = "";
                        else if (row.GetCell(4).CellType == CellType.String)
                            ID.F9PD = row.GetCell(4).StringCellValue;
                        else
                            ID.F9PD = row.GetCell(4).ToString();

                        //F9CURR
                        if (row.GetCell(5) == null)
                            ID.F9CURR = "";
                        else if (row.GetCell(5).CellType == CellType.Blank || (row.GetCell(5).CellType == CellType.String && (row.GetCell(5).StringCellValue == null || row.GetCell(5).StringCellValue == "")))
                            ID.F9CURR = "";
                        else if (row.GetCell(5).CellType == CellType.String)
                            ID.F9CURR = row.GetCell(5).StringCellValue;
                        else
                            ID.F9CURR = row.GetCell(5).ToString();

                        //F9CASN
                        if (row.GetCell(6) == null)
                            ID.F9CASN = "";
                        else if (row.GetCell(6).CellType == CellType.Blank || (row.GetCell(6).CellType == CellType.String && (row.GetCell(6).StringCellValue == null || row.GetCell(6).StringCellValue == "")))
                            ID.F9CASN = "";
                        else if (row.GetCell(6).CellType == CellType.String)
                            ID.F9CASN = row.GetCell(6).StringCellValue;
                        else
                            ID.F9CASN = row.GetCell(6).ToString();


                        //F9ORDN
                        if (row.GetCell(7) == null)
                            ID.F9ORDN = "";
                        else if (row.GetCell(7).CellType == CellType.Blank || (row.GetCell(7).CellType == CellType.String && (row.GetCell(7).StringCellValue == null || row.GetCell(7).StringCellValue == "")))
                            ID.F9ORDN = "";
                        else if (row.GetCell(7).CellType == CellType.String)
                            ID.F9ORDN = row.GetCell(7).StringCellValue;
                        else
                            ID.F9ORDN = row.GetCell(7).ToString();

                        //F9ITEM
                        if (row.GetCell(8) == null)
                            ID.F9ITEM = "";
                        else if (row.GetCell(8).CellType == CellType.Blank || (row.GetCell(8).CellType == CellType.String && (row.GetCell(8).StringCellValue == null || row.GetCell(8).StringCellValue == "")))
                            ID.F9ITEM = "";
                        else if (row.GetCell(8).CellType == CellType.String)
                            ID.F9ITEM = row.GetCell(8).StringCellValue;
                        else
                            ID.F9ITEM = row.GetCell(8).ToString();

                        ////F9PNO
                        if (row.GetCell(9) == null)
                            ID.F9PNO = "";
                        else if (row.GetCell(9).CellType == CellType.Blank || (row.GetCell(9).CellType == CellType.String && (row.GetCell(9).StringCellValue == null || row.GetCell(9).StringCellValue == "")))
                            ID.F9PNO = "";
                        else if (row.GetCell(9).CellType == CellType.String)
                            ID.F9PNO = row.GetCell(9).StringCellValue;
                        else
                            ID.F9PNO = row.GetCell(9).ToString();

                        //F9PNME
                        if (row.GetCell(10) == null)
                            ID.F9PNME = "";
                        else if (row.GetCell(10).CellType == CellType.Blank || (row.GetCell(10).CellType == CellType.String && (row.GetCell(10).StringCellValue == null || row.GetCell(10).StringCellValue == "")))
                            ID.F9PNME = "";
                        else if (row.GetCell(10).CellType == CellType.String)
                            ID.F9PNME = row.GetCell(10).StringCellValue;
                        else
                            ID.F9PNME = row.GetCell(10).ToString();

                        //F9QTY
                        if (row.GetCell(11) == null)
                            ID.F9QTY = "";
                        else if (row.GetCell(11).CellType == CellType.Blank || (row.GetCell(11).CellType == CellType.String && (row.GetCell(11).StringCellValue == null || row.GetCell(11).StringCellValue == "")))
                            ID.F9QTY = "";
                        else if (row.GetCell(11).CellType == CellType.String)
                            ID.F9QTY = row.GetCell(11).StringCellValue;
                        else
                            ID.F9QTY = row.GetCell(11).ToString();

                        //F9UNIT
                        if (row.GetCell(12) == null)
                            ID.F9UNIT = "";
                        else if (row.GetCell(12).CellType == CellType.Blank || (row.GetCell(12).CellType == CellType.String && (row.GetCell(12).StringCellValue == null || row.GetCell(12).StringCellValue == "")))
                            ID.F9UNIT = "";
                        else if (row.GetCell(12).CellType == CellType.String)
                            ID.F9UNIT = row.GetCell(12).StringCellValue;
                        else
                            ID.F9UNIT = row.GetCell(12).ToString();

                        //F9AMNT
                        if (row.GetCell(13) == null)
                            ID.F9AMNT = "";
                        else if (row.GetCell(13).CellType == CellType.Blank || (row.GetCell(13).CellType == CellType.String && (row.GetCell(13).StringCellValue == null || row.GetCell(13).StringCellValue == "")))
                            ID.F9AMNT = "";
                        else if (row.GetCell(13).CellType == CellType.String)
                            ID.F9AMNT = row.GetCell(13).StringCellValue;
                        else
                            ID.F9AMNT = row.GetCell(13).ToString();

                        //F9TINV
                        if (row.GetCell(14) == null)
                            ID.F9TINV = "";
                        else if (row.GetCell(14).CellType == CellType.Blank || (row.GetCell(14).CellType == CellType.String && (row.GetCell(14).StringCellValue == null || row.GetCell(14).StringCellValue == "")))
                            ID.F9TINV = "";
                        else if (row.GetCell(14).CellType == CellType.String)
                            ID.F9TINV = row.GetCell(14).StringCellValue;
                        else
                            ID.F9TINV = row.GetCell(14).ToString();

                        ListInvoiceDetail.Add(ID);
                    }
                }

                foreach (FormD_Data_Upload.InvoiceDetailUpload data in ListInvoiceDetail)
                {
                    new FormD_Data_Upload().InsertTempInvoiceDetaiUpload(data, getpE_UserId.Username);
                }

                return "0|SUCCESS";
            }
            catch (Exception ex)
            {
                return "1|" + ex.Message;
            }
        }

        public ActionResult CallBackOrderDataUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButtonOrderData", UploadControlHelper.ValidationSettings, uc_FileUploadOrderDataComplete);
            return null;
        }

        public void uc_FileUploadOrderDataComplete(object sender, FileUploadCompleteEventArgs e)
        {
            //VESSEL_SCHEDULE Ship = new VESSEL_SCHEDULE();

            string[] r = null;
            string rExtract = string.Empty;
            //string tb_t_name = "spex.TB_T_OD";
            functionID = "FDD-OD";
            moduleID = "FDD";


            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            string MESSAGE_DETAIL = "";
            long pid = 0;
            string subFuncName = "excel to tb t od";
            string isError = "N";
            string processName = "FDD-IH. Function temporary [temp upload invoice header for FormD] ";
            string resultMessage = "";
            int columns;
            int rows;

            DeleteAllOlderFile(HttpContext.Request.MapPath("~/Content/FileUploadResult/"));

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock L = new Lock();
            FormD_Data_Upload up = new FormD_Data_Upload();
            int IsLock = L.is_lock(functionID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMsgText("MSPX00092ERR");
            }
            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                pid = log.createLog(msgError.getMSG_Spex("MSPXFD001INF", pid), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);

                rExtract = up.DeleteData_TB_T_OD();
                if (rExtract == "SUCCES")
                {
                    string user_id = getpE_UserId.Username;

                    L.CREATED_BY = user_id;
                    L.CREATED_DT = DateTime.Now;
                    L.PROCESS_ID = pid;
                    L.FUNCTION_ID = functionID;
                    L.LOCK_REF = functionID;
                    L.LockFunction(L);
                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    //Function untuk nge-extract file excel
                    rExtract = ReadAndInsertExcelOrderData(pathfile);
                    rExtract = rExtract + '|' + pid.ToString();
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                    //if (rExtract == string.Empty || rExtract == "SUCCESS")
                    //{
                    //    rExtract = up.CheckExistingDataInvoiceHeaderTemp; //Ship.CheckExistingDataInvoiceHeaderTemp();
                    //}
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                //MESSAGE_DETAIL = "File is not .xls";
                //pid = log.createLog(msgError.getMSPX00019ERR("Price"), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                rExtract = "1| File is not .xls";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract;//+ "|" + pid.ToString();
        }

        private string ReadAndInsertExcelOrderData(string FileName)
        {
            try
            {
                //XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                //ISheet sheet = xssfworkbook.GetSheetAt(1);
                //int lastrow = sheet.LastRowNum;
                List<FormD_Data_Upload.Orderdata> ListOrderData = new List<FormD_Data_Upload.Orderdata>();
                //int index = 1;
                HSSFWorkbook workbook = null;
                using (FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read))
                {
                    workbook = new HSSFWorkbook(fs);
                }
                HSSFSheet sheet = workbook.GetSheetAt(0) as HSSFSheet;
                IEnumerator enumerator = sheet.GetRowEnumerator();
                FormD_Data_Upload.Orderdata OD;
                while (enumerator.MoveNext())
                {
                    HSSFRow row = enumerator.Current as HSSFRow;
                //}

                if (row.RowNum>0)
                {
                    OD = new FormD_Data_Upload.Orderdata();
                    //IRow row = sheet.GetRow(i);

                    //if (row == null) continue;
                    //if (row.GetCell(0) == null && row.GetCell(1) == null && row.GetCell(2) == null && row.GetCell(3) == null) continue;

                    //OHTNSO
                    if (row.GetCell(0) == null)
                        OD.OHTNSO = "";
                    else if (row.GetCell(0).CellType == CellType.Blank || (row.GetCell(0).CellType == CellType.String && (row.GetCell(0).StringCellValue == null || row.GetCell(0).StringCellValue == "")))
                        OD.OHTNSO = "";
                    else if (row.GetCell(0).CellType == CellType.String)
                        OD.OHTNSO = row.GetCell(0).StringCellValue;
                    else
                        OD.OHTNSO = row.GetCell(0).ToString();

                    //OHBYR
                    if (row.GetCell(1) == null)
                        OD.OHBYR = "";
                    else if (row.GetCell(1).CellType == CellType.Blank || (row.GetCell(1).CellType == CellType.String && (row.GetCell(1).StringCellValue == null || row.GetCell(1).StringCellValue == "")))
                        OD.OHBYR = "";
                    else if (row.GetCell(1).CellType == CellType.String)
                        OD.OHBYR = row.GetCell(1).StringCellValue;
                    else
                        OD.OHBYR = row.GetCell(1).ToString();

                    //OHPD
                    if (row.GetCell(2) == null)
                        OD.OHPD = "";
                    else if (row.GetCell(2).CellType == CellType.Blank || (row.GetCell(2).CellType == CellType.String && (row.GetCell(2).StringCellValue == null || row.GetCell(2).StringCellValue == "")))
                        OD.OHPD = "";
                    else if (row.GetCell(2).CellType == CellType.String)
                        OD.OHPD = row.GetCell(2).StringCellValue;
                    else
                        OD.OHPD = row.GetCell(2).ToString();

                    //OHORDN
                    if (row.GetCell(3) == null)
                        OD.OHORDN = "";
                    else if (row.GetCell(3).CellType == CellType.Blank || (row.GetCell(3).CellType == CellType.String && (row.GetCell(3).StringCellValue == null || row.GetCell(3).StringCellValue == "")))
                        OD.OHORDN = "";
                    else if (row.GetCell(3).CellType == CellType.String)
                        OD.OHORDN = row.GetCell(3).StringCellValue;
                    else
                        OD.OHORDN = row.GetCell(3).ToString();


                    //OHORDD
                    if (row.GetCell(4) == null)
                        OD.OHORDD = "";
                    else if (row.GetCell(4).CellType == CellType.Blank || (row.GetCell(4).CellType == CellType.String && (row.GetCell(4).StringCellValue == null || row.GetCell(4).StringCellValue == "")))
                        OD.OHORDD = "";
                    else if (row.GetCell(4).CellType == CellType.String)
                        OD.OHORDD = row.GetCell(4).StringCellValue;
                    else
                        OD.OHORDD = row.GetCell(4).ToString();

                    //OHTRAN
                    if (row.GetCell(5) == null)
                        OD.OHTRAN = "";
                    else if (row.GetCell(5).CellType == CellType.Blank || (row.GetCell(5).CellType == CellType.String && (row.GetCell(5).StringCellValue == null || row.GetCell(5).StringCellValue == "")))
                        OD.OHTRAN = "";
                    else if (row.GetCell(5).CellType == CellType.String)
                        OD.OHTRAN = row.GetCell(5).StringCellValue;
                    else
                        OD.OHTRAN = row.GetCell(5).ToString();

                    //OHORDT
                    if (row.GetCell(6) == null)
                        OD.OHORDT = "";
                    else if (row.GetCell(6).CellType == CellType.Blank || (row.GetCell(6).CellType == CellType.String && (row.GetCell(6).StringCellValue == null || row.GetCell(6).StringCellValue == "")))
                        OD.OHORDT = "";
                    else if (row.GetCell(6).CellType == CellType.String)
                        OD.OHORDT = row.GetCell(6).StringCellValue;
                    else
                        OD.OHORDT = row.GetCell(6).ToString();

                    //OHBOIN
                    if (row.GetCell(7) == null)
                        OD.OHBOIN = "";
                    else if (row.GetCell(7).CellType == CellType.Blank || (row.GetCell(7).CellType == CellType.String && (row.GetCell(7).StringCellValue == null || row.GetCell(7).StringCellValue == "")))
                        OD.OHBOIN = "";
                    else if (row.GetCell(7).CellType == CellType.String)
                        OD.OHBOIN = row.GetCell(7).StringCellValue;
                    else
                        OD.OHBOIN = row.GetCell(7).ToString();


                    //OHPAYT
                    if (row.GetCell(8) == null)
                        OD.OHPAYT = "";
                    else if (row.GetCell(8).CellType == CellType.Blank || (row.GetCell(8).CellType == CellType.String && (row.GetCell(8).StringCellValue == null || row.GetCell(8).StringCellValue == "")))
                        OD.OHPAYT = "";
                    else if (row.GetCell(8).CellType == CellType.String)
                        OD.OHPAYT = row.GetCell(8).StringCellValue;
                    else
                        OD.OHPAYT = row.GetCell(8).ToString();

                    //OHPRCT
                    if (row.GetCell(9) == null)
                        OD.OHPRCT = "";
                    else if (row.GetCell(9).CellType == CellType.Blank || (row.GetCell(9).CellType == CellType.String && (row.GetCell(9).StringCellValue == null || row.GetCell(9).StringCellValue == "")))
                        OD.OHPRCT = "";
                    else if (row.GetCell(9).CellType == CellType.String)
                        OD.OHPRCT = row.GetCell(9).StringCellValue;
                    else
                        OD.OHPRCT = row.GetCell(9).ToString();

                    //OHCURR
                    if (row.GetCell(10) == null)
                        OD.OHCURR = "";
                    else if (row.GetCell(10).CellType == CellType.Blank || (row.GetCell(10).CellType == CellType.String && (row.GetCell(10).StringCellValue == null || row.GetCell(10).StringCellValue == "")))
                        OD.OHCURR = "";
                    else if (row.GetCell(10).CellType == CellType.String)
                        OD.OHCURR = row.GetCell(10).StringCellValue;
                    else
                        OD.OHCURR = row.GetCell(10).ToString();

                    //OHSLR
                    if (row.GetCell(11) == null)
                        OD.OHSLR = "";
                    else if (row.GetCell(11).CellType == CellType.Blank || (row.GetCell(11).CellType == CellType.String && (row.GetCell(11).StringCellValue == null || row.GetCell(11).StringCellValue == "")))
                        OD.OHSLR = "";
                    else if (row.GetCell(11).CellType == CellType.String)
                        OD.OHSLR = row.GetCell(11).StringCellValue;
                    else
                        OD.OHSLR = row.GetCell(11).ToString();

                    //OHOBYR
                    if (row.GetCell(12) == null)
                        OD.OHOBYR = "";
                    else if (row.GetCell(12).CellType == CellType.Blank || (row.GetCell(12).CellType == CellType.String && (row.GetCell(12).StringCellValue == null || row.GetCell(12).StringCellValue == "")))
                        OD.OHOBYR = "";
                    else if (row.GetCell(12).CellType == CellType.String)
                        OD.OHOBYR = row.GetCell(12).StringCellValue;
                    else
                        OD.OHOBYR = row.GetCell(12).ToString();

                    //OHOPD
                    if (row.GetCell(13) == null)
                        OD.OHOPD = "";
                    else if (row.GetCell(13).CellType == CellType.Blank || (row.GetCell(13).CellType == CellType.String && (row.GetCell(13).StringCellValue == null || row.GetCell(13).StringCellValue == "")))
                        OD.OHOPD = "";
                    else if (row.GetCell(13).CellType == CellType.String)
                        OD.OHOPD = row.GetCell(13).StringCellValue;
                    else
                        OD.OHOPD = row.GetCell(13).ToString();

                    //OHSPLT
                    if (row.GetCell(14) == null)
                        OD.OHSPLT = "";
                    else if (row.GetCell(14).CellType == CellType.Blank || (row.GetCell(14).CellType == CellType.String && (row.GetCell(14).StringCellValue == null || row.GetCell(14).StringCellValue == "")))
                        OD.OHSPLT = "";
                    else if (row.GetCell(14).CellType == CellType.String)
                        OD.OHSPLT = row.GetCell(14).StringCellValue;
                    else
                        OD.OHSPLT = row.GetCell(14).ToString();

                    //OHTITM
                    if (row.GetCell(15) == null)
                        OD.OHTITM = "";
                    else if (row.GetCell(15).CellType == CellType.Blank || (row.GetCell(15).CellType == CellType.String && (row.GetCell(15).StringCellValue == null || row.GetCell(15).StringCellValue == "")))
                        OD.OHTITM = "";
                    else if (row.GetCell(15).CellType == CellType.String)
                        OD.OHTITM = row.GetCell(15).StringCellValue;
                    else
                        OD.OHTITM = row.GetCell(15).ToString();

                    //OHTQTY
                    if (row.GetCell(16) == null)
                        OD.OHTQTY = "";
                    else if (row.GetCell(16).CellType == CellType.Blank || (row.GetCell(16).CellType == CellType.String && (row.GetCell(16).StringCellValue == null || row.GetCell(16).StringCellValue == "")))
                        OD.OHTQTY = "";
                    else if (row.GetCell(16).CellType == CellType.String)
                        OD.OHTQTY = row.GetCell(16).StringCellValue;
                    else
                        OD.OHTQTY = row.GetCell(16).ToString();

                    //OHOITM
                    if (row.GetCell(17) == null)
                        OD.OHOITM = "";
                    else if (row.GetCell(17).CellType == CellType.Blank || (row.GetCell(17).CellType == CellType.String && (row.GetCell(17).StringCellValue == null || row.GetCell(17).StringCellValue == "")))
                        OD.OHOITM = "";
                    else if (row.GetCell(17).CellType == CellType.String)
                        OD.OHOITM = row.GetCell(17).StringCellValue;
                    else
                        OD.OHOITM = row.GetCell(17).ToString();

                    //OHOQTY
                    if (row.GetCell(18) == null)
                        OD.OHOQTY = "";
                    else if (row.GetCell(18).CellType == CellType.Blank || (row.GetCell(18).CellType == CellType.String && (row.GetCell(18).StringCellValue == null || row.GetCell(18).StringCellValue == "")))
                        OD.OHOQTY = "";
                    else if (row.GetCell(18).CellType == CellType.String)
                        OD.OHOQTY = row.GetCell(18).StringCellValue;
                    else
                        OD.OHOQTY = row.GetCell(18).ToString();

                    //OHCRTD
                    if (row.GetCell(19) == null)
                        OD.OHCRTD = "";
                    else if (row.GetCell(19).CellType == CellType.Blank || (row.GetCell(19).CellType == CellType.String && (row.GetCell(19).StringCellValue == null || row.GetCell(19).StringCellValue == "")))
                        OD.OHCRTD = "";
                    else if (row.GetCell(19).CellType == CellType.String)
                        OD.OHCRTD = row.GetCell(19).StringCellValue;
                    else
                        OD.OHCRTD = row.GetCell(19).ToString();


                    //OHLUPD
                    if (row.GetCell(20) == null)
                        OD.OHLUPD = "";
                    else if (row.GetCell(20).CellType == CellType.Blank || (row.GetCell(20).CellType == CellType.String && (row.GetCell(20).StringCellValue == null || row.GetCell(20).StringCellValue == "")))
                        OD.OHLUPD = "";
                    else if (row.GetCell(20).CellType == CellType.String)
                        OD.OHLUPD = row.GetCell(20).StringCellValue;
                    else
                        OD.OHLUPD = row.GetCell(20).ToString();

                    ListOrderData.Add(OD);
                }
                
                    
                }

                foreach (FormD_Data_Upload.Orderdata data in ListOrderData)
                {
                    new FormD_Data_Upload().InsertTempOrderDataUpload(data, getpE_UserId.Username);
                }

                return "0|SUCCESS";
            }
            catch (Exception ex)
            {
                return "1|" + ex.Message;
            }
        }

        public ActionResult UploadBackgroundProsessOrderData(string pid)
        {
            string message = string.Empty;
            try
            {
                string user_id = getpE_UserId.Username;
                string subFuncName = "EXcelToSQL";
                moduleID = "FDD";
                functionID = "FDD-OD";

                //long pid = log.createLog(msgError.getMSPX00005INFForLog("upload invoice header for FormD"), user_id, location + '.' + subFuncName, 0, moduleID, functionID);

                new FormD_Data_Upload().Save_OrderData(Convert.ToInt64(pid), user_id, functionID, moduleID);
                message = msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2];
                return Json(new { success = "true", messages = message + '|' + pid }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public ActionResult UploadBackgroundProsessInvoiceDetail(string pid)
        {
            string message = string.Empty;
            try
            {
                string user_id = getpE_UserId.Username;
                string subFuncName = "EXcelToSQL";
                moduleID = "FDD";
                functionID = "FDD-ID";

                //long pid = log.createLog(msgError.getMSPX00005INFForLog("upload invoice header for FormD"), user_id, location + '.' + subFuncName, 0, moduleID, functionID);

                new FormD_Data_Upload().Save_InvoiceDetail(Convert.ToInt64(pid), user_id, functionID, moduleID);
                message = msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2];
                return Json(new { success = "true", messages = message + '|' + pid }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

    }
}
