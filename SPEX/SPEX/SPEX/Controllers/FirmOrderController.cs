﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;
using Toyota.Common.Web.Platform.Starter.Models;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using NPOI.POIFS.FileSystem;
using NPOI.HPSF;
using System.IO;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxGridView;
using System.Web.UI.WebControls;
using System.Web.UI;
namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class FirmOrderController : PageController
    {
        mMasterLine MasterLine = new mMasterLine();
        mMasterPlant MasterPlant = new mMasterPlant();
        mFirmOrder FirmOrder = new mFirmOrder();

        public string get_Message_Upload
        {
            get
            {
                if (Session["get_Message_Upload"] != null)
                {
                    return Session["get_Message_Upload"].ToString();
                }
                else return null;
            }
            set
            {
                Session["get_Message_Upload"] = value;
            }
        }

        public FirmOrderController()
        {

        }

        protected override void Startup()
        {
            SPN113GetMasterLine();
            SPN113GetMasterPlant();
        }

        public void SPN113GetMasterLine()
        {
            List<mMasterLine> modelMasterLine = MasterLine.getListMasterLine();
            ViewData["DataMasterLine"] = modelMasterLine;
        }

        public void SPN113GetMasterPlant()
        {
            List<mMasterPlant> modelMasterPlant = MasterPlant.getListMasterPlant();
            ViewData["DataMasterPlant"] = modelMasterPlant;
        }

        public ActionResult SPN111GridViewPartialView(string ps_ProductionMonth, string ps_PartNo, string ps_PlantCode, string ps_LineCode, string ps_OrderType)
        {
            List<mFirmOrder> model = new List<mFirmOrder>();

            model = FirmOrder.SPN111FirmOrderSearch(ps_ProductionMonth, ps_PartNo, ps_PlantCode, ps_LineCode, ps_OrderType);
            
            return PartialView("GridViewPartialView", model);
        }

        public string SPN111GetCountOkNotGood(string ps_ProductionMonth, string ps_PartNo, string ps_PlantCode, string ps_LineCode, string ps_OrderType)
        {        
            List<mFirmOrder> modelCount = new List<mFirmOrder>();

            modelCount = FirmOrder.SPN111FirmOrderCountOkNotGood(ps_ProductionMonth, ps_PartNo, ps_PlantCode, ps_LineCode, ps_OrderType);

            string ValueOkNotGood =  string.Empty;

            foreach(var list in modelCount)
            {
                ValueOkNotGood = list.Count_Ok + "|" + list.Count_Not_Good;
            }

            return ValueOkNotGood;
        }

        public void _SPN111DownloadFirmOrder(object sender, EventArgs e, string pD_ProductionMonth, string pD_PartNo, string pD_PlantCode, string pD_LineCode, string pD_OrderType)
        {
            
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Firm_Order_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.TOP;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.TOP;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.Alignment = HorizontalAlignment.CENTER;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.TOP;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent3.Alignment = HorizontalAlignment.RIGHT;

            ISheet sheet = workbook.GetSheet("Firm Order");
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPIN111FirmOrder_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd-MM-yyyy");

            sheet.GetRow(6).GetCell(4).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(4).SetCellValue("Sys");
            sheet.GetRow(9).GetCell(5).SetCellValue(pD_ProductionMonth);
            sheet.GetRow(10).GetCell(5).SetCellValue(pD_PartNo);
            sheet.GetRow(11).GetCell(5).SetCellValue(pD_PlantCode);
            sheet.GetRow(12).GetCell(5).SetCellValue(pD_LineCode);
            sheet.GetRow(13).GetCell(5).SetCellValue(pD_OrderType);


            int row = 18;
            int rowNum = 1;
            IRow Hrow;

            List<mFirmOrder> model = new List<mFirmOrder>();
            model = FirmOrder.SPN111FirmOrderDownload(pD_ProductionMonth, pD_PartNo, pD_PlantCode, pD_LineCode, pD_OrderType);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PART_NO);
                Hrow.CreateCell(3).SetCellValue(result.PART_NAME);
                Hrow.CreateCell(4).SetCellValue("");
                Hrow.CreateCell(5).SetCellValue(result.PART_SPEC);
                Hrow.CreateCell(6).SetCellValue(result.MODEL);
                Hrow.CreateCell(7).SetCellValue(result.QTY_N.ToString());
                Hrow.CreateCell(8).SetCellValue(result.QTY_N_1.ToString());
                Hrow.CreateCell(9).SetCellValue(result.QTY_N_2.ToString());
                Hrow.CreateCell(10).SetCellValue(result.QTY_N_3.ToString());
                Hrow.CreateCell(11).SetCellValue(result.PLANT_CD);
                Hrow.CreateCell(12).SetCellValue(result.LINE_CD);
                Hrow.CreateCell(13).SetCellValue(result.SLOC_CD);
                Hrow.CreateCell(14).SetCellValue(result.PCSKBN.ToString());
                Hrow.CreateCell(15).SetCellValue(result.LATEST_ORDER.ToString("dd-MM-yyyy"));
                Hrow.CreateCell(16).SetCellValue(result.ORDER_TYPE);
                Hrow.CreateCell(17).SetCellValue(result.TAKT_TIME.ToString());
                Hrow.CreateCell(18).SetCellValue(result.ORDER_DT.ToString("dd-MM-yyyy"));
                Hrow.CreateCell(19).SetCellValue("");

                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent;
                Hrow.GetCell(3).CellStyle = styleContent;
                Hrow.GetCell(4).CellStyle = styleContent;
                Hrow.GetCell(5).CellStyle = styleContent;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent3;
                Hrow.GetCell(10).CellStyle = styleContent3;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent3;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent;
                Hrow.GetCell(17).CellStyle = styleContent3;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
            //    sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        public ActionResult SPN111CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadFirmOrder", UploadControlHelper.ValidationSettings, SPN111CallbackUploaduc_FileUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public const string ThumbnailFormat = "Thumbnail{0}{1}";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xls" },
                MaxFileSize = 209751520
            };
        }

        public void SPN111CallbackUploaduc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                var resultFilePath = HttpContext.Request.MapPath("~/Content/FileUploadResult/FirmOrder_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xls");
                e.UploadedFile.SaveAs(resultFilePath);
                IUrlResolutionService urlResolver = sender as IUrlResolutionService;
                if (urlResolver != null)
                {
                    e.CallbackData = urlResolver.ResolveClientUrl(resultFilePath);
                }

                string result = SPN111CallbackUploadExcelToSQL(resultFilePath);
            }
        }

        public string SPN111CallbackUploadExcelToSQL(string resultFilePath)
        {
            try
            {
                FileStream ftmp = new FileStream(resultFilePath, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
                ISheet sheet = workbook.GetSheet("Firm Order");

                string CreatedDT = DateTime.Now.ToString("yyyy-MM-dd");
                string CreatedBy = "Sys";

                int row = 6;
                mProcessID ModProcessID = new mProcessID();
                string ProcessId = ModProcessID.GetProcessID();

                string ProdMonth = (sheet.GetRow(0).GetCell(2).NumericCellValue).ToString();
                string OrderType = sheet.GetRow(1).GetCell(2).StringCellValue;
                string Timing = sheet.GetRow(2).GetCell(2).StringCellValue;

                for (int i = 0; i < sheet.LastRowNum; i++)
                {
                    string PartNo = string.Empty;
                    string PartName = string.Empty;
                    string Model = string.Empty;
                    string QTY_N = string.Empty;
                    string QTY_N_1 = string.Empty;
                    string QTY_N_2 = string.Empty;
                    string QTY_N_3 = string.Empty;

                    ICell PartNoCell = sheet.GetRow(row).GetCell(1);
                    ICell PartNameCell = sheet.GetRow(row).GetCell(2);
                    ICell ModelCell = sheet.GetRow(row).GetCell(3);
                    ICell QTY_NCell = sheet.GetRow(row).GetCell(4);
                    ICell QTY_N_1Cell = sheet.GetRow(row).GetCell(5);
                    ICell QTY_N_2Cell = sheet.GetRow(row).GetCell(6);
                    ICell QTY_N_3Cell = sheet.GetRow(row).GetCell(7);

                    if (PartNoCell != null)
                    {
                        if (PartNoCell.CellType != CellType.BLANK)
                        {
                            PartNo = sheet.GetRow(row).GetCell(1).StringCellValue;
                        }
                    }

                    if (PartNameCell != null)
                    {
                        if (PartNameCell.CellType != CellType.BLANK)
                        {
                            PartName = sheet.GetRow(row).GetCell(2).StringCellValue;
                        }
                    }

                    if (ModelCell != null)
                    {
                        if (ModelCell.CellType != CellType.BLANK)
                        {
                            Model = sheet.GetRow(row).GetCell(3).StringCellValue;
                        }
                    }

                    if (QTY_NCell != null)
                    {
                        if (QTY_NCell.CellType != CellType.BLANK)
                        {
                            QTY_N = (sheet.GetRow(row).GetCell(4).NumericCellValue).ToString();
                        }
                    }

                    if (QTY_N_1Cell != null)
                    {
                        if (QTY_N_1Cell.CellType != CellType.BLANK)
                        {
                            QTY_N_1 = (sheet.GetRow(row).GetCell(5).NumericCellValue).ToString();
                        }
                    }


                    if (QTY_N_2Cell != null)
                    {
                        if (QTY_N_2Cell.CellType != CellType.BLANK)
                        {
                            QTY_N_2 = (sheet.GetRow(row).GetCell(6).NumericCellValue).ToString();
                        }
                    }

                    if (QTY_N_3Cell != null)
                    {
                        if (QTY_N_3Cell.CellType != CellType.BLANK)
                        {
                           QTY_N_3 = (sheet.GetRow(row).GetCell(7).NumericCellValue).ToString();
                        }
                    }

                    if (String.IsNullOrEmpty(PartNo) && String.IsNullOrEmpty(PartName) && String.IsNullOrEmpty(Model) && String.IsNullOrEmpty(QTY_N) && String.IsNullOrEmpty(QTY_N_1) && String.IsNullOrEmpty(QTY_N_2) && String.IsNullOrEmpty(QTY_N_3))
                    {
                        break;
                    }

                    FirmOrder.SPN111CallbackUploadExcelToSQL(ProdMonth, OrderType, Timing,PartNo, PartName, Model, QTY_N, QTY_N_1, QTY_N_2, QTY_N_3, CreatedBy, CreatedDT, ProcessId);
                    row++;
                }

                get_Message_Upload = FirmOrder.SPN111CheckBasicValidation(ProcessId, CreatedBy);

                if (get_Message_Upload == "Upload Successfully")
                {
                    FirmOrder.SPN111CheckDataValidation(ProcessId, ProdMonth, CreatedBy);

                    return get_Message_Upload;
                }
                else
                {
                    return get_Message_Upload = "Error : please check Process ID " + ProcessId;
                }
            }
            catch (Exception err)
            {
                return get_Message_Upload = "Error | " + Convert.ToString(err.Message);
            }
        }

        public string SPN111GetInfo()
        {

            return get_Message_Upload;
        }

        public string SPN111Confirm(string pC_FirmOrder)
        {
            string resultMessage = FirmOrder.SPN111Confirm(pC_FirmOrder);

            return resultMessage;
        }

        public string SPN111Reporcess(string pR_FirmOrder)
        {
            string result = string.Empty;
            try
            {
                string CreatedBy = "Sys";
                mProcessID ModProcessID = new mProcessID();
                string ProcessId = ModProcessID.GetProcessID();
                string[] listFirmOrder = pR_FirmOrder.Split(',');
                string paramProdMonth = string.Empty;
                string paramFlagComplete = string.Empty;

                for (int i = 0; i < listFirmOrder.Length; i++)
                {
                    string[] ProdMonth = listFirmOrder[i].ToString().Split('|');
                    paramProdMonth = ProdMonth[0].ToString();
                    paramFlagComplete = ProdMonth[3].ToString();

                    if (paramFlagComplete == "Temporary")
                    {

                        FirmOrder.SPN111CheckDataValidation(ProcessId, paramProdMonth, CreatedBy);
                    }
                }

                return result = "Reprocess Succussfully";
            }
            catch (Exception err)
            {
                return result = "Error | " + Convert.ToString(err.Message);
            }
        }

    }
}
