﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class ShippingInstructionTentativeController : PageController
    {
        //
        // GET: /BuyerPD/
        //mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mShippingInstructionTentative shipIns = new mShippingInstructionTentative();

        public ShippingInstructionTentativeController()
        {
            Settings.Title = "Tentative Shipping Instruction";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            GetBuyyerForCombo();
            ViewData["Packing_Company"] = GetPackingCompany();
            getpE_UserId = (User)ViewData["User"];
        }

        public List<string> GetPackingCompany()
        {
            return new Common().GetPackingCompany();
        }

        public void GetBuyyerForCombo()
        {
            string BUYER_CD = string.Empty;
            string BUYER_NAME = string.Empty;
            List<BUYER> model = new List<BUYER>();
            model = GetListBUYER(BUYER_CD, BUYER_NAME);
            ViewData["BUYER"] = model;
        }

        public List<BUYER> GetListBUYER(string BUYER_CD, string BUYER_NAME)
        {

            List<BUYER> model = new List<BUYER>();
            BUYER BYR = new BUYER();
            BYR.BUYER_CD = string.IsNullOrEmpty(BUYER_CD) ? string.Empty : BUYER_CD;
            BYR.BUYER_NAME = string.IsNullOrEmpty(BUYER_CD) ? string.Empty : BUYER_NAME;
            model = BYR.getBUYER(BYR);
            return model;
        }

        public ActionResult SITentativeCallBack(string BUYER_CD, string PACKING_COMPANY, DateTime? VANNING_DT_FROM, DateTime? VANNING_DT_TO, DateTime? ETD_FROM, DateTime? ETD_TO)
        {
            List<mShippingInstructionTentative> model = new List<mShippingInstructionTentative>();
            model = shipIns.getListSearch(BUYER_CD, PACKING_COMPANY, VANNING_DT_FROM, VANNING_DT_TO, ETD_FROM, ETD_TO);
            return PartialView("SITentativeGrid", model);
        }

        public ActionResult PopUpSILayoutTent(string p_BUYER_CD, string p_ETD, string p_VANNING_DT_FROM, string p_VANNING_DT_TO, string p_VERSION, string p_PACKING_COMPANY)
        {
            mShippingInstructionTentative model = shipIns.getNewShippingIntruction(p_BUYER_CD, GetDate(p_ETD), GetDate(p_VANNING_DT_FROM), GetDate(p_VANNING_DT_TO), p_VERSION, p_PACKING_COMPANY);

            List<mShippingInstructionTentative> ContainerGradeList = shipIns.getListContainerGrade();
            ViewData["DataGradeMaster"] = ContainerGradeList;
            ViewData["Destination"] = model.BUYER_CD + " - " + model.BUYER_NAME;


            return PartialView("~/Views/ShippingInstructionTentative/SILayoutTent.cshtml", model);
        }

        public ActionResult CheckCreateTent(List<mShippingInstructionTentative> VesselDatas)
        {
            mShippingInstructionTentative vesselDat = VesselDatas.FirstOrDefault();
            string resultMessage = shipIns.CheckCreateTent(vesselDat.BUYER_CD, (DateTime)vesselDat.ETD, (DateTime)vesselDat.VANNING_DT_FROM, (DateTime)vesselDat.VANNING_DT_TO, vesselDat.PACKING_COMPANY);
            string[] r = null;
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new
                {
                    success = "true",
                    messages = r[1],
                    buyer_cd = vesselDat.BUYER_CD,
                    packing_company = vesselDat.PACKING_COMPANY,
                    vanning_dt_from = ((DateTime)vesselDat.VANNING_DT_FROM).ToString("dd.MM.yyyy"),
                    vanning_dt_to = ((DateTime)vesselDat.VANNING_DT_TO).ToString("dd.MM.yyyy"),
                    etd = ((DateTime)vesselDat.ETD).ToString("dd.MM.yyyy")
                }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckExist(string pSI_NO, string pVERSION)
        {
            bool exist = shipIns.CheckSINoExist(pSI_NO);

            if (exist)
                return Json(new { exist = "true", messages = "Exist" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { exist = "false", messages = "Not Exist" }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ConfirmTentSave(string pBUYER_CD, string pETD, string pVANNING_DT_FROM, string pVANNING_DT_TO, string pPACKING_COMPANY, string pVERSION,
            string pSI_NO, string pSHIPPER, string pCONSIGNEE, string pNOTIFY_PARTY, string pCONTAINER_GRADE)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string UserID = getpE_UserId.Username;

            string resultMessage = shipIns.ConfirmTentSave(pBUYER_CD, GetDate(pETD), GetDate(pVANNING_DT_FROM), GetDate(pVANNING_DT_TO), pPACKING_COMPANY, pVERSION,
                    pSI_NO, pSHIPPER, pCONSIGNEE, pNOTIFY_PARTY, pCONTAINER_GRADE, UserID);
            string[] r = null;
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("success"))
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);

        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            ar = dt.Split('.');
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;
            success = int.TryParse(ar[0].ToString(), out day);
            success = int.TryParse(ar[1].ToString(), out month);
            success = int.TryParse(ar[2].ToString(), out year);

            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        public void GenerateExcelMainTent(string pSHIP_INSTRUCTION_NO, string pVERSION)
        {
            DeleteAllOlderFile(HttpContext.Request.MapPath("~/Content/ReportResult/"));

            string filename = "";
            string filesTmp = "";

            filesTmp = HttpContext.Request.MapPath("~/Template/SI_Download.xlsx");

            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            IDataFormat format = workbook.CreateDataFormat();

            ICellStyle styleDate1 = workbook.CreateCellStyle();
            styleDate1.DataFormat = format.GetFormat("mmmm d, yyyy");
            styleDate1.Alignment = HorizontalAlignment.Center;

            ICellStyle style1 = workbook.CreateCellStyle();
            style1.VerticalAlignment = VerticalAlignment.Center;
            style1.Alignment = HorizontalAlignment.Left;
            style1.WrapText = true;
            //add agi 2017-09-20
            style1.BorderLeft = BorderStyle.Thin;
            style1.BorderRight = BorderStyle.None;
            style1.BorderTop = BorderStyle.None;
            style1.BorderBottom = BorderStyle.None;

            ISheet sheetMain = workbook.GetSheet("SILayout");
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            filename = "SI_Tentative_" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            mShippingInstructionTentative model = new mShippingInstructionTentative();

            model = shipIns.getShippingInstructionDownload(pSHIP_INSTRUCTION_NO, pVERSION, "");

            GenerateExcelHeaderShipperConsigneeNotifyParty(style1, sheetMain, model);
            GenerateExcelMesrsTent(sheetMain, model);
            GenerateExcelFeederConnectPort(styleDate1, sheetMain, model);
            GenerateExcelGoodsDescriptionTent(sheetMain, model);
            GenerateExcelGradeAndFooter(workbook, sheetMain, model);
            GenerateExcelMNOSSheet(workbook, model, false);
            GenerateExcelDeleteUnUsedSheetFirm(workbook);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));

        }

        private static void GenerateExcelHeaderShipperConsigneeNotifyParty(ICellStyle style1, ISheet sheetMain, mShippingInstructionTentative model)
        {
            sheetMain.GetRow(0).GetCell(0).SetCellValue(model.XLS_HEADER_1_LABEL);
            sheetMain.GetRow(1).GetCell(0).SetCellValue(model.XLS_HEADER_2_LABEL);
            sheetMain.GetRow(2).GetCell(0).SetCellValue(model.XLS_HEADER_3_LABEL);

            sheetMain.GetRow(4).GetCell(0).SetCellValue(model.XLS_SHIPPER_LABEL);
            sheetMain.GetRow(5).GetCell(0).SetCellValue(model.SHIPPER);
            sheetMain.GetRow(5).GetCell(0).CellStyle = style1;
            sheetMain.GetRow(9).GetCell(0).SetCellValue(model.XLS_CONSIGNEE_LABEL);
            sheetMain.GetRow(10).GetCell(0).SetCellValue(model.CONSIGNEE);
            sheetMain.GetRow(10).GetCell(0).CellStyle = style1;
            sheetMain.GetRow(16).GetCell(0).SetCellValue(model.XLS_NOTIFY_PARTY_LABEL);
            sheetMain.GetRow(17).GetCell(0).SetCellValue(model.NOTIFY_PARTY);
            sheetMain.GetRow(17).GetCell(0).CellStyle = style1;
        }

        private static void GenerateExcelMesrsTent(ISheet sheetMain, mShippingInstructionTentative model)
        {
            sheetMain.GetRow(4).GetCell(11).SetCellValue(model.XLS_MESRS_LABEL);
            sheetMain.GetRow(6).GetCell(12).SetCellValue(model.SHIPPING_AGENT_NAME);
            sheetMain.GetRow(7).GetCell(12).SetCellValue(string.Format(model.XLS_ATTN1_LABEL, model.SHIP_AGENT_PIC_NAME == null ? "" : model.SHIP_AGENT_PIC_NAME));


            sheetMain.GetRow(9).GetCell(12).SetCellValue(model.FORWARD_AGENT_NAME);
            sheetMain.GetRow(10).GetCell(12).SetCellValue(model.XLS_ATTN2_LABEL);
            sheetMain.GetRow(11).GetCell(12).SetCellValue(model.XLS_FAX_NO_LABEL);
            sheetMain.GetRow(10).GetCell(12).SetCellValue(string.Format(model.XLS_ATTN2_LABEL, model.FORWARD_AGENT_PIC_NO == null ? "" : model.FORWARD_AGENT_PIC_NO));
            sheetMain.GetRow(11).GetCell(12).SetCellValue(string.Format(model.XLS_FAX_NO_LABEL, model.FORWARD_AGENT_CONTACT_NO == null ? "" : model.FORWARD_AGENT_CONTACT_NO));

            sheetMain.GetRow(13).GetCell(12).SetCellValue(string.Format(model.XLS_BOOKING_NO_LABEL, ""));
            sheetMain.GetRow(13).GetCell(16).SetCellValue(string.Format(model.BOOKING_NO, ""));

            sheetMain.GetRow(14).GetCell(12).SetCellValue(string.Format(model.XLS_PEB_NO_LABEL, ""));
            sheetMain.GetRow(14).GetCell(16).SetCellValue(string.Format(model.XLS_TGL_LABEL, ""));

            sheetMain.GetRow(16).GetCell(12).SetCellValue(model.XLS_CONTAINER_NO_LABEL);
            sheetMain.GetRow(16).GetCell(16).SetCellValue(model.XLS_SEAL_NO_LABEL);

            sheetMain.GetRow(25).GetCell(11).SetCellValue(model.XLS_SHIPPING_INSTRUCTION_LABEL);
            sheetMain.GetRow(27).GetCell(11).SetCellValue(model.XLS_SI_NO_LABEL);

            sheetMain.GetRow(25).GetCell(11).SetCellValue(model.XLS_SHIPPING_INSTRUCTION_LABEL);
            sheetMain.GetRow(27).GetCell(14).SetCellValue(model.SHIP_INSTRUCTION_NO);
        }

        private static void GenerateExcelFeederConnectPort(ICellStyle styleDate1, ISheet sheetMain, mShippingInstructionTentative model)
        {
            sheetMain.GetRow(23).GetCell(0).SetCellValue(model.XLS_FEEDER_VESSEL_LABEL);
            sheetMain.GetRow(23).GetCell(5).SetCellValue(model.XLS_FEEDER_VOY_LABEL);
            sheetMain.GetRow(23).GetCell(7).SetCellValue(model.XLS_SHIPMENT_LABEL);
            sheetMain.GetRow(24).GetCell(0).SetCellValue(model.FEEDER_VESSEL);
            sheetMain.GetRow(24).GetCell(5).SetCellValue(model.VOYAGE1);
            sheetMain.GetRow(24).GetCell(6).SetCellValue(((DateTime)model.SHIPMENT_DT).ToString("MMMM dd, yyyy"));
            sheetMain.GetRow(24).GetCell(6).CellStyle = styleDate1;

            sheetMain.GetRow(25).GetCell(0).SetCellValue(model.XLS_CONNECT_VESSEL_LABEL);
            sheetMain.GetRow(25).GetCell(5).SetCellValue(model.XLS_CONNECT_VOY_LABEL);
            sheetMain.GetRow(25).GetCell(7).SetCellValue(model.XLS_ARRIVAL_LABEL);
            sheetMain.GetRow(26).GetCell(0).SetCellValue(model.CONNECT_VESSEL);
            sheetMain.GetRow(26).GetCell(5).SetCellValue(model.VOYAGE2);

            if (model.ARRIVAL_DT != null)
            {
                sheetMain.GetRow(26).GetCell(6).SetCellValue(((DateTime)model.ARRIVAL_DT).ToString("MMMM dd, yyyy"));
                sheetMain.GetRow(26).GetCell(6).CellStyle = styleDate1;
            }

            sheetMain.GetRow(27).GetCell(0).SetCellValue(model.XLS_PORT_LOADING_LABEL);
            sheetMain.GetRow(28).GetCell(0).SetCellValue(model.PORT_LOADING);

            sheetMain.GetRow(27).GetCell(6).SetCellValue(model.XLS_PORT_DISCHARGE_LABEL);
            sheetMain.GetRow(28).GetCell(6).SetCellValue(model.PORT_DISCHARGE);

        }

        private static void GenerateExcelGoodsDescriptionTent(ISheet sheetMain, mShippingInstructionTentative model)
        {
            sheetMain.GetRow(29).GetCell(0).SetCellValue(model.XLS_MARKS_NUMBERS_LABEL);
            sheetMain.GetRow(32).GetCell(0).SetCellValue(string.Format(model.XLS_MARKS_NUMBER_DESC1_LABEL, "0"));
            sheetMain.GetRow(33).GetCell(0).SetCellValue(model.XLS_MARKS_NUMBER_DESC2_LABEL);


            sheetMain.GetRow(35).GetCell(0).SetCellValue(model.XLS_GENUINE_PARTS_LABEL);
            sheetMain.GetRow(36).GetCell(0).SetCellValue(string.Format(model.XLS_INVOICE_NO_LABEL, ""));


            sheetMain.GetRow(29).GetCell(6).SetCellValue(model.XLS_DESCRIPTION_GOODS_LABEL);
            sheetMain.GetRow(32).GetCell(7).SetCellValue(model.XLS_DESCRIPTION_GOODS_UNIT1_LABEL);
            sheetMain.GetRow(32).GetCell(9).SetCellValue(model.TOTAL_CASE);
            sheetMain.GetRow(32).GetCell(11).SetCellValue(model.XLS_DESCRIPTION_GOODS_UNIT2_LABEL);

            sheetMain.GetRow(29).GetCell(13).SetCellValue(model.XLS_GROSS_WEIGHT_LABEL);
            sheetMain.GetRow(30).GetCell(13).SetCellValue(model.XLS_GROSS_WEIGHT_UNIT_LABEL);
            sheetMain.GetRow(32).GetCell(16).SetCellValue(model.XLS_GROSS_WEIGHT_UNIT_DESC1_LABEL);
            sheetMain.GetRow(33).GetCell(16).SetCellValue(model.XLS_GROSS_WEIGHT_UNIT_DESC2_LABEL);

            sheetMain.GetRow(29).GetCell(17).SetCellValue(model.XLS_MEASUREMENT_LABEL);
            sheetMain.GetRow(30).GetCell(17).SetCellValue(model.XLS_MEASUREMENT_UNIT_LABEL);
            sheetMain.GetRow(32).GetCell(19).SetCellValue(model.XLS_MEASUREMENT_UNIT_LABEL);
            sheetMain.GetRow(32).GetCell(17).SetCellValue((double)model.MEASUREMENT);
        }

        private void GenerateExcelGradeAndFooter(XSSFWorkbook workbook, ISheet sheet, mShippingInstructionTentative model)
        {
            IRow Hrow;
            int rowAdd = 0;
            IFont font = workbook.CreateFont();
            font.FontName = "Tahoma";
            font.FontHeight = 10;

            IDataFormat format = workbook.CreateDataFormat();
            ICellStyle styleDate1 = workbook.CreateCellStyle();
            styleDate1.DataFormat = format.GetFormat("mmmm d, yyyy");
            styleDate1.Alignment = HorizontalAlignment.Center;
            styleDate1.BorderLeft = BorderStyle.Thin;
            styleDate1.BorderBottom = BorderStyle.Thin;
            styleDate1.BorderTop = BorderStyle.Thin;
            styleDate1.BorderRight = BorderStyle.Thin;
            styleDate1.SetFont(font);

            ICellStyle style = workbook.CreateCellStyle();
            styleDate1.Alignment = HorizontalAlignment.Left;
            styleDate1.SetFont(font);

            ICellStyle styleLocation = workbook.CreateCellStyle();
            styleLocation.Alignment = HorizontalAlignment.Center;
            styleLocation.VerticalAlignment = VerticalAlignment.Center;
            styleLocation.SetFont(font);

            ICellStyle styleTabLeft = workbook.CreateCellStyle();
            styleTabLeft.Alignment = HorizontalAlignment.Left;
            styleTabLeft.BorderLeft = BorderStyle.Thin;
            styleTabLeft.BorderRight = BorderStyle.Thin;
            styleTabLeft.BorderTop = BorderStyle.Thin;
            styleTabLeft.BorderBottom = BorderStyle.Thin;
            styleTabLeft.SetFont(font);

            ICellStyle styleTabCenter = workbook.CreateCellStyle();
            styleTabCenter.Alignment = HorizontalAlignment.Center;
            styleTabCenter.BorderLeft = BorderStyle.Thin;
            styleTabCenter.BorderRight = BorderStyle.Thin;
            styleTabCenter.BorderTop = BorderStyle.Thin;
            styleTabCenter.BorderBottom = BorderStyle.Thin;
            styleTabCenter.SetFont(font);

            ICellStyle styleLeftTop = workbook.CreateCellStyle();
            styleLeftTop.Alignment = HorizontalAlignment.Left;
            styleLeftTop.BorderLeft = BorderStyle.Thin;
            styleLeftTop.BorderTop = BorderStyle.Thin;
            styleLeftTop.SetFont(font);

            ICellStyle styleLeftButtom = workbook.CreateCellStyle();
            styleLeftButtom.Alignment = HorizontalAlignment.Left;
            styleLeftButtom.BorderLeft = BorderStyle.Thin;
            styleLeftButtom.BorderBottom = BorderStyle.Thin;
            styleLeftButtom.BorderTop = BorderStyle.None;
            styleLeftButtom.BorderRight = BorderStyle.None;
            styleLeftButtom.SetFont(font);

            ICellStyle styleTopButtom = workbook.CreateCellStyle();
            styleTopButtom.Alignment = HorizontalAlignment.Left;
            styleTopButtom.BorderTop = BorderStyle.Thin;
            styleTopButtom.BorderBottom = BorderStyle.Thin;
            styleTopButtom.SetFont(font);

            ICellStyle styleRightTop = workbook.CreateCellStyle();
            styleRightTop.Alignment = HorizontalAlignment.Left;
            styleRightTop.BorderRight = BorderStyle.Thin;
            styleRightTop.BorderTop = BorderStyle.Thin;
            styleRightTop.SetFont(font);

            ICellStyle styleRightButtom = workbook.CreateCellStyle();
            styleRightButtom.Alignment = HorizontalAlignment.Left;
            styleRightButtom.BorderRight = BorderStyle.Thin;
            styleRightButtom.BorderBottom = BorderStyle.Thin;
            styleRightButtom.SetFont(font);

            ICellStyle styleButtom = workbook.CreateCellStyle();
            styleButtom.Alignment = HorizontalAlignment.Left;
            styleButtom.BorderBottom = BorderStyle.Thin;
            styleButtom.SetFont(font);

            ICellStyle styleTop = workbook.CreateCellStyle();
            styleTop.Alignment = HorizontalAlignment.Center;
            styleTop.BorderTop = BorderStyle.Thin;
            styleTop.BorderBottom = BorderStyle.None;
            styleTop.SetFont(font);

            ICellStyle styleLeft = workbook.CreateCellStyle();
            styleLeft.Alignment = HorizontalAlignment.Left;
            styleLeft.BorderLeft = BorderStyle.Thin;
            styleLeft.BorderBottom = BorderStyle.None;
            styleLeft.BorderTop = BorderStyle.None;
            styleLeft.BorderRight = BorderStyle.None;
            styleLeft.SetFont(font);

            ICellStyle styleRight = workbook.CreateCellStyle();
            styleRight.Alignment = HorizontalAlignment.Left;
            styleRight.BorderRight = BorderStyle.Thin;
            styleRight.BorderTop = BorderStyle.None;
            styleRight.BorderLeft = BorderStyle.None;
            styleRight.BorderBottom = BorderStyle.None;
            styleRight.SetFont(font);

            ICellStyle styleAllBorderCenter = workbook.CreateCellStyle();
            styleAllBorderCenter.Alignment = HorizontalAlignment.Center;
            styleAllBorderCenter.BorderLeft = BorderStyle.Thin;
            styleAllBorderCenter.BorderBottom = BorderStyle.Thin;
            styleAllBorderCenter.BorderTop = BorderStyle.Thin;
            styleAllBorderCenter.BorderRight = BorderStyle.Thin;
            styleAllBorderCenter.SetFont(font);

            List<mShippingInstructionTentative> containerGradeList = shipIns.getListSIVanningDownloadData(model.SHIP_INSTRUCTION_NO, model.VERSION);
            int containerGradeCount = containerGradeList.Count();

            sheet.GetRow(38).GetCell(1).SetCellValue(string.Format(model.XLS_CONTAINER_GRADE_LABEL, model.CONTAINER_GRADE));

            rowAdd = containerGradeCount > 7 ? containerGradeCount - 6 : 0;

            sheet.GetRow(39).GetCell(1).SetCellValue(model.XLS_VANNING_DT_LABEL);
            sheet.GetRow(39).GetCell(3).SetCellValue(model.XLS_TOTAL_CONTAINER_LABEL);
            sheet.GetRow(39).GetCell(7).SetCellValue(model.XLS_LOCATION_CY_LABEL);
            sheet.GetRow(40).GetCell(3).SetCellValue(model.XLS_CONTAINER_PLAN_LABEL);
            sheet.GetRow(40).GetCell(5).SetCellValue(model.XLS_CONTAINER_ACTUAL_LABEL);

            int startRowDetail = 40;



            CellRangeAddress range = new CellRangeAddress(0, 0, 0, 0);

            foreach (var detail in containerGradeList)
            {
                startRowDetail++;
                Hrow = sheet.CreateRow(startRowDetail);

                Hrow.CreateCell(0).SetCellValue("");
                Hrow.GetCell(0).CellStyle=styleLeft;

                Hrow.CreateCell(1).SetCellValue(((DateTime)detail.VANNING_DT).ToString("dd-MMM-yy"));
                Hrow.CreateCell(2).SetCellValue("");
                range = new CellRangeAddress(startRowDetail, startRowDetail, 1, 2);
                sheet.AddMergedRegion(range);
                Hrow.GetCell(1).CellStyle = styleTabLeft;
                Hrow.GetCell(2).CellStyle = styleTabLeft;
                Hrow.CreateCell(3).SetCellValue(detail.CONTAINER_QTYPLAN);
                Hrow.CreateCell(4).SetCellValue("");
                range = new CellRangeAddress(startRowDetail, startRowDetail, 3, 4);
                sheet.AddMergedRegion(range);
                Hrow.GetCell(3).CellStyle = styleTabCenter;
                Hrow.GetCell(4).CellStyle = styleTabCenter;
                if (model.VERSION == "T")
                    Hrow.CreateCell(5).SetCellValue("");
                else
                    Hrow.CreateCell(5).SetCellValue(detail.CONTAINER_QTYACTUAL);
                Hrow.CreateCell(6).SetCellValue("");
                range = new CellRangeAddress(startRowDetail, startRowDetail, 5, 6);
                sheet.AddMergedRegion(range);
                Hrow.GetCell(5).CellStyle = styleTabCenter;
                Hrow.GetCell(6).CellStyle = styleTabCenter;
                Hrow.CreateCell(8).SetCellValue("");
                Hrow.GetCell(8).CellStyle = styleTabCenter;

                if (startRowDetail == 41)
                    Hrow.CreateCell(7).SetCellValue(model.XLS_LOCATION_CY_DESC_LABEL);


                Hrow.CreateCell(19).SetCellValue("");
                Hrow.GetCell(19).CellStyle = styleRight;

            }

            range = new CellRangeAddress(41, startRowDetail, 7, 8);
            sheet.AddMergedRegion(range);
            sheet.GetRow(41).GetCell(7).CellStyle = styleLocation;

            startRowDetail++;
            Hrow = sheet.CreateRow(startRowDetail);

            Hrow.CreateCell(0).SetCellValue("");
            Hrow.GetCell(0).CellStyle = styleLeft;


            Hrow.CreateCell(1).SetCellValue(model.XLS_CONTAINER_TOTAL_QTY_LABEL);
            Hrow.CreateCell(2).SetCellValue("");
            range = new CellRangeAddress(startRowDetail, startRowDetail, 1, 2);
            sheet.AddMergedRegion(range);
            Hrow.GetCell(1).CellStyle = styleTabLeft;
            Hrow.GetCell(2).CellStyle = styleTabLeft;
            Hrow.CreateCell(3).SetCellValue(model.TOTAL_CONTAINER_PLAN);
            Hrow.CreateCell(4).SetCellValue("");
            range = new CellRangeAddress(startRowDetail, startRowDetail, 3, 4);
            sheet.AddMergedRegion(range);
            Hrow.GetCell(3).CellStyle = styleTabCenter;
            Hrow.GetCell(4).CellStyle = styleTabCenter;
            Hrow.CreateCell(5).SetCellValue(model.TOTAL_CONTAINER_ACTUAL);
            Hrow.CreateCell(6).SetCellValue("");
            range = new CellRangeAddress(startRowDetail, startRowDetail, 5, 6);
            sheet.AddMergedRegion(range);
            Hrow.GetCell(5).CellStyle = styleTabCenter;
            Hrow.GetCell(6).CellStyle = styleTabCenter;
            Hrow.CreateCell(7).SetCellValue(model.XLS_CONTAINER_TOTAL_LOCATION_LABEL);
            Hrow.CreateCell(8).SetCellValue("");
            range = new CellRangeAddress(startRowDetail, startRowDetail, 7, 8);
            sheet.AddMergedRegion(range);
            Hrow.GetCell(7).CellStyle = styleTabCenter;
            Hrow.GetCell(8).CellStyle = styleTabCenter;


            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleRight;



            int rowFooter = 49 + rowAdd;

            rowFooter = rowFooter + 3;
            Hrow = sheet.CreateRow(rowFooter);
            Hrow.CreateCell(0).SetCellValue(model.XLS_FREIGHT_PAYABLE_LABEL);
            Hrow.GetCell(0).CellStyle = styleLeftTop;
            Hrow.CreateCell(1).SetCellValue("");
            Hrow.GetCell(1).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(2).SetCellValue("");
            Hrow.GetCell(2).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(3).SetCellValue("");
            Hrow.GetCell(3).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(4).SetCellValue("");
            Hrow.GetCell(4).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 0, 4);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(5).SetCellValue(model.XLS_PREPAID_LABEL);
            Hrow.GetCell(5).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(6).SetCellValue("");
            Hrow.GetCell(6).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(7).SetCellValue("");
            Hrow.GetCell(7).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 5, 7);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(8).SetCellValue(model.XLS_COLLECT_LABEL);
            Hrow.GetCell(8).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(9).SetCellValue("");
            Hrow.GetCell(9).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(10).SetCellValue("");
            Hrow.GetCell(10).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 8, 10);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(11).SetCellValue(model.XLS_BL_RELEASE_LABEL);
            Hrow.GetCell(11).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(12).SetCellValue("");
            Hrow.GetCell(12).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(13).SetCellValue("");
            Hrow.GetCell(13).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(14).SetCellValue("");
            Hrow.GetCell(14).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 11, 14);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(15).SetCellValue(model.XLS_DATE_ISSUE_LABEL);
            Hrow.GetCell(15).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(16).SetCellValue("");
            Hrow.GetCell(16).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(17).SetCellValue("");
            Hrow.GetCell(17).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(18).SetCellValue("");
            Hrow.GetCell(18).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 15, 19);
            sheet.AddMergedRegion(range);

            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);
            Hrow.CreateCell(0).SetCellValue(model.FREIGHT_PAYABLE);
            Hrow.GetCell(0).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(1).SetCellValue("");
            Hrow.GetCell(1).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(2).SetCellValue("");
            Hrow.GetCell(2).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(3).SetCellValue("");
            Hrow.GetCell(3).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(4).SetCellValue("");
            Hrow.GetCell(4).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 0, 4);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(5).SetCellValue(model.PREPAID);
            Hrow.GetCell(5).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(6).SetCellValue("");
            Hrow.GetCell(6).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(7).SetCellValue("");
            Hrow.GetCell(7).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 5, 7);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(8).SetCellValue(model.COLLECT);
            Hrow.GetCell(8).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(9).SetCellValue("");
            Hrow.GetCell(9).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(10).SetCellValue("");
            Hrow.GetCell(10).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 8, 10);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(11).SetCellValue(model.BL_RELEASE);
            Hrow.GetCell(11).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(12).SetCellValue("");
            Hrow.GetCell(12).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(13).SetCellValue("");
            Hrow.GetCell(13).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(14).SetCellValue("");
            Hrow.GetCell(14).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 11, 14);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(15).SetCellValue((DateTime)model.ISSUE_DT);
            Hrow.GetCell(15).CellStyle = styleAllBorderCenter;
            Hrow.GetCell(15).CellStyle = styleDate1;
            Hrow.CreateCell(16).SetCellValue("");
            Hrow.GetCell(16).CellStyle = styleDate1;
            //Hrow.GetCell(16).CellStyle = styleLeftButtom;
            Hrow.CreateCell(17).SetCellValue("");
            Hrow.GetCell(17).CellStyle = styleDate1;
            Hrow.CreateCell(18).SetCellValue("");
            Hrow.GetCell(18).CellStyle = styleDate1;
            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleDate1;
            range = new CellRangeAddress(rowFooter, rowFooter, 15, 19);
            sheet.AddMergedRegion(range);

            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);

            Hrow.CreateCell(0).SetCellValue("");
            Hrow.GetCell(0).CellStyle = styleLeft;

            Hrow.CreateCell(0).SetCellValue(model.XLS_NOTED_LABEL);
            //Hrow.GetCell(0).CellStyle = style;
            Hrow.GetCell(0).CellStyle = styleLeft;

            Hrow.CreateCell(2).SetCellValue(model.XLS_NOTE_1_LABEL);
            Hrow.GetCell(2).CellStyle = style;
            Hrow.CreateCell(15).SetCellValue(model.XLS_APPROVE_BY_LABEL);
            Hrow.GetCell(15).CellStyle = styleLeftTop;
            Hrow.CreateCell(16).SetCellValue("");
            Hrow.GetCell(16).CellStyle = styleTop;
            Hrow.CreateCell(17).SetCellValue("");
            Hrow.GetCell(17).CellStyle = styleTop;
            Hrow.CreateCell(18).SetCellValue("");
            Hrow.GetCell(18).CellStyle = styleTop;
            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleTop;
            range = new CellRangeAddress(rowFooter, rowFooter, 15, 19);
            sheet.AddMergedRegion(range);

            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleRight;

            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);

            Hrow.CreateCell(0).SetCellValue("");
            Hrow.GetCell(0).CellStyle = styleLeft;


            Hrow.CreateCell(2).SetCellValue(model.XLS_NOTE_2_LABEL);
            Hrow.GetCell(2).CellStyle = style;
            Hrow.CreateCell(15).SetCellValue("");
            Hrow.GetCell(15).CellStyle = styleLeft;

            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleRight;

            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);

            Hrow.CreateCell(0).SetCellValue("");
            Hrow.GetCell(0).CellStyle = styleLeft;


            Hrow.CreateCell(2).SetCellValue(model.XLS_NOTE_3_LABEL);
            Hrow.GetCell(2).CellStyle = style;
            Hrow.CreateCell(15).SetCellValue("");
            Hrow.GetCell(15).CellStyle = styleLeft;

            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleRight;


            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);

            Hrow.CreateCell(0).SetCellValue("");
            Hrow.GetCell(0).CellStyle = styleLeft;

            Hrow.CreateCell(2).SetCellValue(model.XLS_NOTE_4_LABEL);
            Hrow.GetCell(2).CellStyle = style;
            Hrow.CreateCell(15).SetCellValue("");
            Hrow.GetCell(15).CellStyle = styleLeft;

            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleRight;

            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);

            Hrow.CreateCell(0).SetCellValue("");
            Hrow.GetCell(0).CellStyle = styleLeft;
            
            Hrow.CreateCell(2).SetCellValue(model.XLS_NOTE_5_LABEL);
            Hrow.GetCell(2).CellStyle = style;
            Hrow.CreateCell(15).SetCellValue("");
            Hrow.GetCell(15).CellStyle = styleLeft;

            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleRight;


            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);
            for (int i = 0; i <= 19; i++)
            {
                Hrow.CreateCell(i).SetCellValue("");
                if (i == 0)
                    Hrow.GetCell(i).CellStyle = styleLeftButtom;
                else if (i == 14)
                    Hrow.GetCell(i).CellStyle = styleRightButtom;
                else if (i == 19)
                    Hrow.GetCell(i).CellStyle = styleRightButtom;
                else
                    Hrow.GetCell(i).CellStyle = styleButtom;
            }
        }

        private void GenerateExcelMNOSSheet(XSSFWorkbook workbook, mShippingInstructionTentative model, bool isFirm)
        {
            IFont fontHeader = workbook.CreateFont();
            fontHeader.Color = HSSFColor.Black.Index;
            fontHeader.Boldweight = (short)FontBoldWeight.Bold;
            fontHeader.FontHeight = 12;
            fontHeader.IsItalic = true;
            fontHeader.FontName = "Tahoma";

            IFont fontDetail = workbook.CreateFont();
            fontDetail.Color = HSSFColor.Black.Index;
            fontDetail.Boldweight = (short)FontBoldWeight.Bold;
            fontDetail.FontHeight = 10;
            fontDetail.FontName = "Tahoma";

            ICellStyle styleHeader = workbook.CreateCellStyle();
            styleHeader.SetFont(fontHeader);

            ICellStyle styleDetail = workbook.CreateCellStyle();
            styleDetail.SetFont(fontDetail);


            IRow Hrow;
            ISheet sheetMNOS;
            if (isFirm)
                sheetMNOS = workbook.GetSheet("MNOS").CopySheet("MNOS " + model.SHEET_NAME);
            else
                sheetMNOS = workbook.GetSheet("MNOS");

            Hrow = sheetMNOS.CreateRow(0);
            Hrow.CreateCell(1).SetCellValue(model.XLS_MNOS_DESC1_LABEL);
            Hrow.GetCell(1).CellStyle = styleHeader;

            int startRowCase = 2;
            if (isFirm)
            {
                List<mShippingInstructionTentative> caseList = shipIns.getListSICaseDownloadData(model.SHIP_INSTRUCTION_NO, model.VERSION);
                foreach (mShippingInstructionTentative caseData in caseList)
                {
                    Hrow = sheetMNOS.CreateRow(startRowCase);
                    Hrow.CreateCell(1).SetCellValue(caseData.CASE_NO);
                    Hrow.GetCell(1).CellStyle = styleDetail;

                    startRowCase++;
                }
            }

            startRowCase++;
            Hrow = sheetMNOS.CreateRow(startRowCase);
            Hrow.CreateCell(1).SetCellValue(model.XLS_MNOS_DESC2_LABEL);
            Hrow.GetCell(1).CellStyle = styleHeader;

            startRowCase++;
            Hrow = sheetMNOS.CreateRow(startRowCase);
            Hrow.CreateCell(1).SetCellValue(model.XLS_MNOS_DESC3_LABEL + model.XLS_MNOS_DESC4_LABEL);
            Hrow.GetCell(1).CellStyle = styleHeader;
        }

        private static void GenerateExcelDeleteUnUsedSheetFirm(XSSFWorkbook workbook)
        {
            ISheet sheet3 = workbook.GetSheet("Breakdown");
            ISheet sheet4 = workbook.GetSheet("TotContainer");

            workbook.RemoveSheetAt(workbook.GetSheetIndex(sheet3));
            workbook.RemoveSheetAt(workbook.GetSheetIndex(sheet4));
        }

        private void DeleteAllOlderFile(string folderPath)
        {
            IEnumerable<string> allFiles = new List<string>();
            allFiles = System.IO.Directory.EnumerateFiles(folderPath, "*.*");
            foreach (string file in allFiles)
            {
                FileInfo info = new FileInfo(file);
                if (info.CreationTime < DateTime.Now.AddDays(-2))
                    System.IO.File.Delete(file);
            }
        }

        public ActionResult PopUpSIFirmDetail(string pSHIP_INSTRUCTION_NO)
        {
            mShippingInstructionTentative model = shipIns.GetSINoFirmList(pSHIP_INSTRUCTION_NO);
            return PartialView("~/Views/ShippingInstructionTentative/SIFirmDetail.cshtml", model);
        }

    }
}
