﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Text;
using System.Linq;
using System.Web;

namespace SPEX.Controllers
{
    public class ServiceMappingMasterController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        ServiceMappingMaster smm = new ServiceMappingMaster();
        private string FunctionID = "SM001";
        private string ModuleID = "SM";

        public ServiceMappingMasterController()
        {
            Settings.Title = "Vendor Service Mapping Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User) ViewData["User"];

            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();

            ViewData["ServiceReg"] = smm.GetServiceId();
            ViewData["VendorCD"] = smm.GetVendorCode();
            ViewData["VendorType"] = smm.GetVendorType();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GetServiceMappingMasterCallBack(string VendorCd, string VendorName, string ServiceCode,
                                                            string VendorType, string ServiceReg, string ServiceName)
        {
            List<ServiceMappingMaster> model = new List<ServiceMappingMaster>();
            model = smm.getServiceMappingMaster(VendorCd, VendorName, ServiceCode, VendorType, ServiceReg, ServiceName, "");

            return PartialView("ServiceMappingMasterGrid", model);
        }

        #region DOWNLOAD DATA
        private List<CsvColumn> CsvCol_Download = CsvColumn.Parse(
            "Vendor CD|C|1|0|VENDOR_CD;" +
            "Vendor Type|C|6|1|VENDOR_TYPE;" +
            "Vendor Name|C|10|0|VENDOR_NAME;" +
            "Service Registration|C|1|0|SERVICE_REGISTRATION;" +
            "Service Code|C|6|1|SERVICE_CODE;" +
            "Service Name|C|10|0|SERVICE_NAME;" +
            "Created By|C|1|0|CREATED_BY;" +
            "Created Date|C|10|0|CREATED_DATE;" +
            "Changed By|C|1|0|CHANGED_BY;" +
            "Changed Date|C|1|0|CHANGED_DATE;"
        );

        private List<CsvColumn> CsvCol_Template = CsvColumn.Parse(
            "Vendor CD|C|1|0|VENDOR_CD;" +
            "Vendor Type|C|6|1|VENDOR_TYPE;" +
            "Service Code|C|6|1|SERVICE_CODE;"
        );

        public class CsvColumn
        {
            public string Text { get; set; }
            public string Column { get; set; }
            public string DataType { get; set; }
            public int Len { get; set; }
            public int Mandatory { get; set; }

            public static List<CsvColumn> Parse(string v)
            {
                List<CsvColumn> l = new List<CsvColumn>();

                string[] x = v.Split(';');

                for (int i = 0; i < x.Length; i++)
                {
                    string[] y = x[i].Split('|');
                    CsvColumn me = null;
                    if (y.Length >= 4)
                    {
                        int len = 0;
                        int mandat = 0;
                        int.TryParse(y[2], out len);
                        int.TryParse(y[3], out mandat);
                        me = new CsvColumn()
                        {
                            Text = y[0],
                            DataType = y[1],
                            Len = len,
                            Mandatory = mandat
                        };
                        l.Add(me);
                    }

                    if (y.Length > 4 && me != null)
                    {
                        me.Column = y[4];
                    }
                }
                return l;
            }
        }

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        private string _csv_sep;
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {
                    _csv_sep = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ";";
                }
                return _csv_sep;
            }
        }

        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        private string Equs(string s)
        {
            string separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (s != null)
            {
                if (s.Contains(separator))
                {
                    s = "\"" + s + "\"";
                }
                else
                {
                    s = "=\"" + s + "\"";
                }
            }
            return s;
        }

        public void DownloadData(object sender, EventArgs e, string VendorCd, string VendorName,
                                 string ServiceCode, string VendorType, string ServiceReg, string ServiceName)
        {
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            string filename = "Service_Mapping_Master_" + date + ".csv";
            StringBuilder gr = new StringBuilder("");
            string dateNow = DateTime.Now.ToString();
            List<ServiceMappingMaster> model = new List<ServiceMappingMaster>();

            model = smm.getServiceMappingMaster(VendorCd, VendorName, ServiceCode, VendorType, ServiceReg, ServiceName, "");
            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol_Download.Count; i++)
                {
                    gr.Append(Quote(CsvCol_Download[i].Text)); gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);
                foreach (ServiceMappingMaster d in model)
                {
                    csvAddLine(gr, new string[] {
                        Equs(d.VENDOR_CD),
                        Equs(d.VENDOR_TYPE),
                        Equs(d.VENDOR_NAME),
                        Equs(d.SERVICE_REGISTRATION),
                        Equs(d.SERVICE_CODE),
                        Equs(d.SERVICE_NAME),
                        Equs(d.CREATED_BY),
                        Equs(d.CREATED_DATE),
                        Equs(d.CHANGED_BY),
                        Equs(d.CHANGED_DATE)
                   });
                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        public void DownloadTemplate()
        {
            string filename = "";
            string format = "";
            filename = "Service_Mapping_Master.csv";

            StringBuilder gr = new StringBuilder("");
            for (int i = 0; i < CsvCol_Template.Count; i++)
            {
                gr.Append(Quote(CsvCol_Template[i].Text + format));
                gr.Append(CSV_SEP);
            }

            gr.Append(Environment.NewLine);

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }
        #endregion

        #region SAVE AND EDIT
        public ActionResult PopUp(string Data, string Mode)
        {
            List<ServiceMappingMasterPrice> model = new List<ServiceMappingMasterPrice>();
            
            if (Mode == "Add")
            {
                ViewBag.Mode = "Add";
                model = smm.getServicePriceMaster("", "", Mode);
                ViewData["ServicePriceGrid"] = model;
            }
            else
            {
                smm = System.Web.Helpers.Json.Decode<ServiceMappingMaster>(Data);
                smm = smm.GetVendorTypeAndName(smm.VENDOR_CD);

                model = smm.getServicePriceMaster(smm.VENDOR_CD, "", Mode);
                ViewData["ServicePriceGrid"] = model;

                ViewBag.Mode = "Edit";
            }
            return PartialView("PartialForm", smm);
        }

        public string OnVendorCodeChanged(string VendorCd, string VendorType)
        {
            string result = "";
            ServiceMappingMaster data = smm.GetVendorTypeAndName(VendorCd);
            result = data.VENDOR_TYPE + ';' + data.VENDOR_NAME;
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GetServicePriceMasterCallBack(string VendorCd, string VendorType, string Mode)
        {
            List<ServiceMappingMasterPrice> model = new List<ServiceMappingMasterPrice>();
            model = smm.getServicePriceMaster(VendorCd, VendorType, Mode);

            ViewData["ServicePriceGrid"] = model;

            return PartialView("AddServicePriceMasterGrid", model);
        }

        public ActionResult SaveAdd(string Data)
        {
            ServiceMappingMaster Model = System.Web.Helpers.Json.Decode<ServiceMappingMaster>(Data);
            if (getpE_UserId != null)
            {
                Model.CREATED_BY = getpE_UserId.Username;
                string result = Model.AddData(Model);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }

        public ActionResult SaveEdit(string Data)
        {
            ServiceMappingMaster Model = System.Web.Helpers.Json.Decode<ServiceMappingMaster>(Data);
            if (getpE_UserId != null)
            {
                Model.CREATED_BY = getpE_UserId.Username;
                string result = Model.EditData(Model);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }

        public ActionResult Delete(string Data)
        {
            List<ServiceMappingMaster> lcms = System.Web.Helpers.Json.Decode<List<ServiceMappingMaster>>(Data);
            ServiceMappingMaster Model = new ServiceMappingMaster();
            if (getpE_UserId != null)
            {
                string UserId = getpE_UserId.Username;
                string result = Model.DeleteData(lcms, UserId);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }
        #endregion

        #region UPLOAD DATA
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, FileUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplateServiceMappingMaster";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".csv" },
                MaxFileSize = 20971520
            };
        }

        public void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_SERVICE_MAPPING";

            string templateFileName = "TemplateServiceMappingMaster";
            string UploadDirectory = Server.MapPath("~/Content/FileUploadResult");
            long ProcessID = 0;

            Lock lockTable = new Lock();
            int IsLock = lockTable.is_lock(FunctionID, out ProcessID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(ProcessID.ToString());
                Session["IS_LOCKED"] = true;
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                ProcessID = log.createLog("MSGSPX0001|INF|Upload Process Started", getpE_UserId.Username, "Upload.BulkCSV", 0, ModuleID, FunctionID);

                lockTable.PROCESS_ID = ProcessID;
                lockTable.FUNCTION_ID = FunctionID;
                lockTable.LOCK_REF = FunctionID;
                lockTable.CREATED_BY = getpE_UserId.Username;
                lockTable.CREATED_DT = DateTime.Now;
                lockTable.LockFunction(lockTable);

                Session["IS_LOCKED"] = false;
                Session["UPLOAD_PID"] = ProcessID;

                rExtract = smm.DeleteTempData();
                if (rExtract == "SUCCESS")
                {
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);
                    var pathfile = Path.Combine(updir, templateFileName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));
                    e.UploadedFile.SaveAs(pathfile);

                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));

                    ProcessID = log.createLog("MSGSPX0001|INF|Upload to Temporary Table Success", getpE_UserId.Username, "Upload.BulkCSV", ProcessID, ModuleID, FunctionID);
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        public string UploadValidationAndSave()
        {
            string result = "";
            bool is_locked = Session["IS_LOCKED"] == null ? false : Convert.ToBoolean(Session["IS_LOCKED"]);
            long pid = 0;

            if (is_locked)
            {
                return "LOCKED";
            }
            else
            {
                pid = Session["UPLOAD_PID"] == null ? 0 : Convert.ToInt64(Session["UPLOAD_PID"]);
                if (pid == 0)
                {
                    pid = log.createLog("MSGSPX0001|INF|Upload Validation Process Started", getpE_UserId.Username, "Upload.BulkCSV", 0, ModuleID, FunctionID);
                }

                result = smm.BulkInsertTempCSVToTempPartPrice(getpE_UserId.Username, pid);

                if (result != "ERROR")
                {
                    smm.DeleteTempData();
                    Lock unlock = new Lock();
                    unlock.UnlockFunction(FunctionID);
                }

                Session.Remove("IS_LOCKED");

                string msg = "";
                if (result == "ERROR")
                {
                    msg = msgError.getMSPX00066INF("Upload Vendor Service Mapping Master", "<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>");
                }
                else
                {
                    msg = msgError.getMSPX00065INF("Upload Vendor Service Mapping Master", "<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>");
                }

                return msg + "|" + pid;
            }
        }

        public void DownloadErrorUpload(object sender, EventArgs e)
        {
            string filename = "";
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "ErrorUpload_Service_Mapping_Master_" + date + ".csv";

            string dateNow = DateTime.Now.ToString();

            List<ServiceMappingMaster> model = new List<ServiceMappingMaster>();
            model = smm.getServiceMappingErrorUpload();

            StringBuilder gr = new StringBuilder("");
            string format = "";

            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol_Template.Count; i++)
                {
                    gr.Append(Quote(CsvCol_Template[i].Text + format));
                    gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);

                foreach (ServiceMappingMaster d in model)
                {
                    csvAddLine(gr, new string[] {
                    Equs(d.VENDOR_CD),
                    Equs(d.VENDOR_TYPE),
                    Equs(d.SERVICE_CODE),
                    Equs(d.ERROR_MSG)
                   });
                }
            }

            smm.DeleteTempData();
            Lock unlock = new Lock();
            unlock.UnlockFunction(FunctionID);

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }
        #endregion
    }
}

