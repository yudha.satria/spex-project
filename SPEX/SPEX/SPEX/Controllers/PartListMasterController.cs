﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.IO;
using System.Drawing;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using Toyota.Common.Credential;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using Toyota.Common.Web.Platform.Starter.Models;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using NPOI.POIFS.FileSystem;
using NPOI.HPSF;


namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class PartListMasterController : PageController
    {
        mPartListMaster PartListMaster = new mPartListMaster();

        public PartListMasterController()
        {
           Settings.Title = "Part List Master";
        }
               
        protected override void Startup()
        {
       
        }



        public ActionResult GridViewPartListMaster(string p_PARENT_PART_NO, string p_PART_NO, string p_PROD_MONTH, string p_PROD_MONTH_to)
        {
            List<mPartListMaster> model = new List<mPartListMaster>();

            {
                model = PartListMaster.getListPartListMaster
                (p_PARENT_PART_NO, p_PART_NO, p_PROD_MONTH, p_PROD_MONTH_to);
            }
            return PartialView("GridViewPartListMaster", model);
        }

        public void _DownloadSourcePartListMaster(object sender, EventArgs e, string pD_PARENT_PART_NO, string pD_PART_NO, string pD_PROD_MONTH, string pD_PROD_MONTH_to)
        {
           
            List<mPartListMaster> model = new List<mPartListMaster>();
            model = PartListMaster.getListPartListMaster(pD_PARENT_PART_NO, pD_PART_NO, pD_PROD_MONTH, pD_PROD_MONTH_to);


            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Part_List_Master_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.TOP;
            styleContent.BorderLeft = BorderStyle.THIN;
            styleContent.BorderRight = BorderStyle.THIN;
            styleContent.BorderBottom = BorderStyle.THIN;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.TOP;
            styleContent2.BorderLeft = BorderStyle.THIN;
            styleContent2.BorderRight = BorderStyle.THIN;
            styleContent2.BorderBottom = BorderStyle.THIN;
            styleContent2.Alignment = HorizontalAlignment.CENTER;

            ISheet sheet = workbook.GetSheet("Part List Master"); 
            string date = DateTime.Now.ToString("dd-MM-yyyy");
            filename = "PartListMaster_" + date + ".xls";
            sheet.ForceFormulaRecalculation = true;

           
            sheet.GetRow(6).GetCell(3).SetCellValue(date);
            sheet.GetRow(7).GetCell(3).SetCellValue("Reyza");
            sheet.GetRow(9).GetCell(4).SetCellValue(pD_PROD_MONTH);
            sheet.GetRow(9).GetCell(9).SetCellValue(pD_PROD_MONTH_to);
            sheet.GetRow(10).GetCell(4).SetCellValue(pD_PARENT_PART_NO);
            sheet.GetRow(11).GetCell(4).SetCellValue(pD_PART_NO);
          
            

            int row = 17;
            string PRODMONTH = string.Empty;
            string PARENTPARTNO = string.Empty;
            string PARENTPRODPURPOSE = string.Empty;
            string PARENTSOURCETYPE = string.Empty;
            string PARENTPLANTCD = string.Empty;
            string PARTNO = string.Empty;
            string PRODPURPOSE = string.Empty;
            string SOURCETYPE = string.Empty;
            string PLANTCD = string.Empty;
            string SLOCCD = string.Empty;
            string DOCCD = string.Empty;
            string QTYN1 = string.Empty;
            string QTYN2 = string.Empty;
            string QTYN3 = string.Empty;
            string QTYN4 = string.Empty;
            string QTYN5 = string.Empty;
            string QTYN6 = string.Empty;
            string QTYN7 = string.Empty;
            string QTYN8 = string.Empty;
            string QTYN9 = string.Empty;
            string QTYN10 = string.Empty;
            string QTYN11 = string.Empty;
            string QTYN12 = string.Empty;
            string QTYN13 = string.Empty;
            string QTYN14 = string.Empty;
            string QTYN15 = string.Empty;
            string QTYN16 = string.Empty;
            string QTYN17 = string.Empty;
            string QTYN18 = string.Empty;
            string QTYN19 = string.Empty;
            string QTYN20 = string.Empty;
            string QTYN21 = string.Empty;
            string QTYN22 = string.Empty;
            string QTYN23 = string.Empty;
            string QTYN24 = string.Empty;
            string QTYN25 = string.Empty;
            string QTYN26 = string.Empty;
            string QTYN27 = string.Empty;
            string QTYN28 = string.Empty;
            string QTYN29 = string.Empty;
            string QTYN30 = string.Empty;
            string QTYN31 = string.Empty;

            foreach (var result in model)
            {
                PRODMONTH = result.PROD_MONTH;
                PARENTPARTNO = result.PARENT_PART_NO;
                PARENTPRODPURPOSE = result.PARENT_PROD_PURPOSE;
                PARENTSOURCETYPE = result.PARENT_SOURCE_TYPE;
                PARENTPLANTCD = result.PARENT_PLANT_CD;
                PARTNO = result.PART_NO;
                PRODPURPOSE = result.PROD_PURPOSE;
                SOURCETYPE = result.SOURCE_TYPE;
                PLANTCD = result.PLANT_CD;
                SLOCCD = result.SLOC_CD;
                DOCCD = result.DOCK_CD;
                QTYN1 = result.QTY_N_1;
                QTYN2 = result.QTY_N_2;
                QTYN3 = result.QTY_N_3;
                QTYN4 = result.QTY_N_4;
                QTYN5 = result.QTY_N_5;
                QTYN6 = result.QTY_N_6;
                QTYN7 = result.QTY_N_7;
                QTYN8 = result.QTY_N_8;
                QTYN9 = result.QTY_N_9;
                QTYN10 = result.QTY_N_10;
                QTYN11 = result.QTY_N_11;
                QTYN12 = result.QTY_N_12;
                QTYN13 = result.QTY_N_13;
                QTYN14 = result.QTY_N_14;
                QTYN15 = result.QTY_N_15;
                QTYN16 = result.QTY_N_16;
                QTYN17 = result.QTY_N_17;
                QTYN18 = result.QTY_N_18;
                QTYN19 = result.QTY_N_19;
                QTYN20 = result.QTY_N_20;
                QTYN21 = result.QTY_N_21;
                QTYN22 = result.QTY_N_22;
                QTYN23 = result.QTY_N_23;
                QTYN24 = result.QTY_N_24;
                QTYN25 = result.QTY_N_25;
                QTYN26 = result.QTY_N_26;
                QTYN27 = result.QTY_N_27;
                QTYN28 = result.QTY_N_28;
                QTYN29 = result.QTY_N_29;
                QTYN30 = result.QTY_N_30;
                QTYN31 = result.QTY_N_31;

                
                sheet.GetRow(row).GetCell(1).SetCellValue(PRODMONTH);
                sheet.GetRow(row).GetCell(2).SetCellValue(PARENTPARTNO);
                sheet.GetRow(row).GetCell(3).SetCellValue(PARENTSOURCETYPE);
                sheet.GetRow(row).GetCell(4).SetCellValue(PARENTPRODPURPOSE);
                sheet.GetRow(row).GetCell(5).SetCellValue(PARENTPLANTCD);
                sheet.GetRow(row).GetCell(6).SetCellValue(PARTNO);
                sheet.GetRow(row).GetCell(7).SetCellValue(SOURCETYPE);
                sheet.GetRow(row).GetCell(8).SetCellValue(PRODPURPOSE);
                sheet.GetRow(row).GetCell(9).SetCellValue(PLANTCD);
                sheet.GetRow(row).GetCell(10).SetCellValue(SLOCCD);
                sheet.GetRow(row).GetCell(11).SetCellValue(DOCCD);
                sheet.GetRow(row).GetCell(12).SetCellValue(QTYN1);
                sheet.GetRow(row).GetCell(13).SetCellValue(QTYN2);
                sheet.GetRow(row).GetCell(14).SetCellValue(QTYN3);
                sheet.GetRow(row).GetCell(15).SetCellValue(QTYN4);
                sheet.GetRow(row).GetCell(16).SetCellValue(QTYN5);
                sheet.GetRow(row).GetCell(17).SetCellValue(QTYN6);
                sheet.GetRow(row).GetCell(18).SetCellValue(QTYN7);
                sheet.GetRow(row).GetCell(19).SetCellValue(QTYN8);
                sheet.GetRow(row).GetCell(20).SetCellValue(QTYN9);
                sheet.GetRow(row).GetCell(21).SetCellValue(QTYN10);
                sheet.GetRow(row).GetCell(22).SetCellValue(QTYN11);
                sheet.GetRow(row).GetCell(23).SetCellValue(QTYN12);
                sheet.GetRow(row).GetCell(24).SetCellValue(QTYN13);
                sheet.GetRow(row).GetCell(25).SetCellValue(QTYN14);
                sheet.GetRow(row).GetCell(26).SetCellValue(QTYN15);
                sheet.GetRow(row).GetCell(27).SetCellValue(QTYN16);
                sheet.GetRow(row).GetCell(28).SetCellValue(QTYN17);
                sheet.GetRow(row).GetCell(29).SetCellValue(QTYN18);
                sheet.GetRow(row).GetCell(30).SetCellValue(QTYN19);
                sheet.GetRow(row).GetCell(31).SetCellValue(QTYN20);
                sheet.GetRow(row).GetCell(32).SetCellValue(QTYN21);
                sheet.GetRow(row).GetCell(33).SetCellValue(QTYN22);
                sheet.GetRow(row).GetCell(34).SetCellValue(QTYN23);
                sheet.GetRow(row).GetCell(35).SetCellValue(QTYN24);
                sheet.GetRow(row).GetCell(36).SetCellValue(QTYN25);
                sheet.GetRow(row).GetCell(37).SetCellValue(QTYN26);
                sheet.GetRow(row).GetCell(38).SetCellValue(QTYN27);
                sheet.GetRow(row).GetCell(39).SetCellValue(QTYN28);
                sheet.GetRow(row).GetCell(40).SetCellValue(QTYN29);
                sheet.GetRow(row).GetCell(41).SetCellValue(QTYN30);
                sheet.GetRow(row).GetCell(42).SetCellValue(QTYN31);
                

                sheet.GetRow(row).GetCell(1).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(2).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(3).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(4).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(5).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(6).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(7).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(8).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(9).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(10).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(11).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(12).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(13).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(14).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(15).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(16).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(17).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(18).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(19).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(20).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(21).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(22).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(23).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(24).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(25).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(26).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(27).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(28).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(29).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(30).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(31).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(32).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(33).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(34).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(35).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(36).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(37).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(38).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(39).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(40).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(41).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(42).CellStyle = styleContent2;
                row++;
            }

            for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
                sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));  
        }
    }



    }









