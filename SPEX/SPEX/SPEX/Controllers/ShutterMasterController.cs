﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DevExpress.Web.ASPxGridView;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class ShutterMasterController : PageController
    {
        //
        // GET: /BuyerPD/
        //mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();

        public ShutterMasterController()
        {
            Settings.Title = "Shutter Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //CallbackComboPriceTerm();

            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Shutter Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
            mSystemMaster system = new mSystemMaster();
            ShutterMaster st= new ShutterMaster();
            List<mSystemMaster> Transportation = system.GetSystemValueList("TRANSPORT_CD", "1;2", "3", ";").Where(t => t.SYSTEM_VALUE != string.Empty && t.SYSTEM_VALUE != null).ToList();
            List<mSystemMaster> PRIVILLAGEs = st.GetDataBySystemMaster("ShutterMaster", "Privillage"); //system.GetSystemValueList("ShutterMaster", "Privillage", "1", ";");
            List<mSystemMaster> HANDLING_TYPEs = st.GetDataBySystemMaster("ShutterMaster", "HANDLING_TYPE"); //system.GetSystemValueList("ShutterMaster", "HANDLING_TYPE", "1", ";");
            ViewData["TRANSPORTATIONs"] = Transportation;
            ViewData["PRIVILLAGEs"] = PRIVILLAGEs;
            ViewData["HANDLING_TYPEs"] = HANDLING_TYPEs;
            GetBuyyerForCombo();
            GetCaseGrouping();
        }

        public void GetBuyyerForCombo()
        {
            
            List<mBuyerPD> model = new List<mBuyerPD>();
            mBuyerPD Byr = new mBuyerPD();
            model = Byr.GetAllBuyerPD();
            ViewData["DESTINATIONs"] = model;
        }

        public ActionResult ShutterMasterSearchCallBack(string Data)
        {
            ShutterMaster Shutter = new ShutterMaster();
            Shutter = System.Web.Helpers.Json.Decode<ShutterMaster>(Data);
            //Shutter.DESTINATION = DESTINATION;
            //Shutter.BUYER_NAME = BUYER_NAME;
            List<ShutterMaster> model = new List<ShutterMaster>();
            model = Shutter.GetShutter(Shutter);
            return PartialView("_Grid", model);
        }

        public ActionResult AddeDataShutterMaster(string Data)
        {
            ShutterMaster Shutter = new ShutterMaster();
            Shutter = System.Web.Helpers.Json.Decode<ShutterMaster>(Data);
            //Shutter.DESTINATION = DESTINATION;
            //Shutter.BUYER_NAME = BUYER_NAME;
            try
            {
                string result = Shutter.AddDataShutterMaster(Shutter, getpE_UserId.Username);
                string[] r = null;
                r = result.Split('|');

                if (r[0].ToLower().Contains("false"))
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);     
            }
            catch (Exception ex)
            {
                return Json(new { success = "true", messages = ex.Message}, JsonRequestBehavior.AllowGet);  
            }
                   
        }

        public ActionResult UpadteDataShutterMaster(string Data)
        {
            ShutterMaster Shutter = new ShutterMaster();
            Shutter = System.Web.Helpers.Json.Decode<ShutterMaster>(Data);
            try
            {
                string result = Shutter.UpadteDataShutterMaster(Shutter, getpE_UserId.Username);
                string[] r = null;
                r = result.Split('|');

                if (r[0].ToLower().Contains("false"))
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "true", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }

        public ActionResult DeleteDataShutterMaster(List<string> ShutterCodes)
        {
            
            ShutterMaster Shutter = new ShutterMaster();
            //Shutter.DESTINATION = DESTINATION;
            //Shutter.BUYER_NAME = BUYER_NAME;
            try
            {
                string result = Shutter.DeleteDataShutterMaster(ShutterCodes);
                string[] r = null;
                r = result.Split('|');

                if (r[0].ToLower().Contains("false"))
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "true", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }

        public ActionResult ShowPopUpShutterMaster(string Mode, string Data)
        {
            ShutterMaster Model = new ShutterMaster();
            mSystemMaster system = new mSystemMaster();
            List<mSystemMaster> Transportation = system.GetSystemValueList("TRANSPORT_CD", "1;2", "3", ";").Where(t=>t.SYSTEM_VALUE!=string.Empty && t.SYSTEM_VALUE != null).ToList();
            List<mSystemMaster> PRIVILLAGEs = Model.GetDataBySystemMaster("ShutterMaster", "Privillage"); //system.GetSystemValueList("ShutterMaster", "Privillage", "1", ";");
            List<mSystemMaster> HANDLING_TYPEs = Model.GetDataBySystemMaster("ShutterMaster", "HANDLING_TYPE"); //system.GetSystemValueList("ShutterMaster", "HANDLING_TYPE", "1", ";");
            ViewData["TRANSPORTATIONs"] = Transportation;
            ViewData["PRIVILLAGEs"] = PRIVILLAGEs;
            ViewData["HANDLING_TYPEs"] = HANDLING_TYPEs;
            GetBuyyerForCombo();
            List<string> CGs=GetCaseGrouping();
            if (Mode == "Edit")
            {
                Model = System.Web.Helpers.Json.Decode<ShutterMaster>(Data);
                Model = Model.GetShutter(Model).First();
            }
            if (Mode=="Add")
            {
                Model.CASE_GROUPING = CGs.First();
                Model.HANDLING_TYPE = HANDLING_TYPEs.First().SYSTEM_VALUE;
                Model.PRIVILLAGE = PRIVILLAGEs.First().SYSTEM_VALUE;
            }
            return PartialView("_Form", Model);
        }
        //add agi 2017-08-08
        public List<string>  GetCaseGrouping()
        {
            ShutterMaster sm = new ShutterMaster();
            List<string> CGs = sm.GetCaseGrouping();
            ViewData["CaseGroupings"] = CGs;
            return CGs;
        }

        public void Download(string Data)
        {
            ShutterMaster Shutter = new ShutterMaster();
            Shutter = System.Web.Helpers.Json.Decode<ShutterMaster>(Data);
            List<ShutterMaster> ShutterMasters = new List<ShutterMaster>();
            ShutterMasters = Shutter.GetShutter(Shutter);
            string filesTmp = HttpContext.Request.MapPath("~/Template/shutter_master_download_template.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);
            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);
            ISheet sheet = workbook.GetSheet("shutter");
            string date = DateTime.Now.ToString("ddMMyyyy");
            string filename = "shutter_master_download_" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            int row = 1;
            IRow Hrow;
            foreach (ShutterMaster result in ShutterMasters)
            {
                Hrow = sheet.CreateRow(row);
                Hrow.CreateCell(0).SetCellValue(result.SHUTTER_CODE);
                Hrow.CreateCell(1).SetCellValue(result.TRANSPORTATION);
                Hrow.CreateCell(2).SetCellValue(result.DESTINATION);
                Hrow.CreateCell(3).SetCellValue(result.PRIVILLAGE);
                Hrow.CreateCell(4).SetCellValue(result.HANDLING_TYPE);
                Hrow.CreateCell(5).SetCellValue(result.CASE_GROUPING);
                Hrow.CreateCell(6).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(7).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(8).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));
                row++;
            }
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
    }
}
