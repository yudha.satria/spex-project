﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Text;
using System.Linq;
namespace SPEX.Controllers
{
    public class ManifestRecivingInquiryController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mCaseMaster caseMaster = new mCaseMaster();
        ManifestRecivingInquiry mri = new ManifestRecivingInquiry();

        public ManifestRecivingInquiryController()
        {
            Settings.Title = "Manifest Receiving Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            ViewData["MRIStatusList"] = mri.GetManifestRecivingInquiryStatusList();
            //add agi 2017-10-23
            string range = new mSystemMaster().GetSystemValueList("manifest inquiry", "range day", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            ViewBag.range = Convert.ToInt32(range);
            ViewBag.MSPXM0001ERR = msgError.getMsgText("MSPXM0001ERR").Replace("{0}", range);
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (string.IsNullOrEmpty(dt))
            {
                result = "";
            }
            else if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult ManifestRecivingInquiryGridCallback(string pManifest_No, string pDock_Cd, string pStartTmapOrdDt, string pEndTmapOrdDt, string pSupplier_Cd, string pRcvPlant_Cd, string pTmapOrder_No, string pPart_No, string pSubSupplier_Cd, string pSubSupplier_Plant, string pStartOrdRlsDt, string pEndOrdRlsDt, string pSendOrderADMStatus, string pSendGRADMStatus)
        {
            List<ManifestRecivingInquiry> model = new List<ManifestRecivingInquiry>();

            model = mri.getList(pManifest_No, pDock_Cd, reFormatDate(pStartTmapOrdDt), reFormatDate(pEndTmapOrdDt), pSupplier_Cd, pRcvPlant_Cd, pTmapOrder_No, pPart_No, pSubSupplier_Cd, pSubSupplier_Plant, reFormatDate(pStartOrdRlsDt), reFormatDate(pEndOrdRlsDt), pSendOrderADMStatus, pSendGRADMStatus);

            return PartialView("ManifestRecivingInquiryGrid", model);
        }

        public ActionResult EditManifestRecivingInquiry(string id, decimal? pCancelQty)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = mri.Update(id, pCancelQty, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult IsStatusManifestRecivingInquiryHold(string ids)
        {
            ManifestRecivingInquiry o = mri.getOBR(ids);
            string os = "false";
           /* if (o.OBR_STATUS == "1")
            {
                os = "true";
            }*/
            return Json(new { success = os }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditStatusManifestRecivingInquiry(string ids, string pStatusID)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = mri.UpdateStatus(ids, pStatusID, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddCase(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT,
            string pDESCRIPTION)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = caseMaster.Add(pCASE_TYPE, pCASE_NET_WEIGHT, pCASE_LENGTH, pCASE_WIDTH, pCASE_HEIGHT, pDESCRIPTION, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteCase(string pCASE_TYPE)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = caseMaster.Delete(pCASE_TYPE, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        private string GetDateSQL(string dt)
        {
            if (dt == "01.01.0100")
                return "";

            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime date = new DateTime(year, month, day);

            return date.ToString("yyyy-MM-dd");
        }

        
        public void DownloadData_ByParameter(object sender, EventArgs e, string pManifest_No, string pSupplier_Code, string pOrder_No, string pDock_Code, string pPlant_Code, string pPart_No, string pStartTmapOrdDt, string pEndTmapOrdDt, string pSubSupplier_Cd, string pSubSupplier_Plant, string pStartOrdRlsDt, string pEndOrdRlsDt, string pSendOrderADMStatus, string pSendGRADMStatus)
        {
            string filename = "";
            ManifestRecivingInquiry mri = new ManifestRecivingInquiry();
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPEX_Manifest_Report_" + date + ".csv";

            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            string dateNow = DateTime.Now.ToString();

            List<ManifestRecivingInquiry> model = new List<ManifestRecivingInquiry>();

            model = mri.getList(pManifest_No, pDock_Code, reFormatDate(pStartTmapOrdDt), reFormatDate(pEndTmapOrdDt), pSupplier_Code, pPlant_Code, pOrder_No, pPart_No, pSubSupplier_Cd, pSubSupplier_Plant, reFormatDate(pStartOrdRlsDt), reFormatDate(pEndOrdRlsDt), pSendOrderADMStatus, pSendGRADMStatus);

            StringBuilder gr = new StringBuilder("");

            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol_Download.Count; i++)
                {
                    gr.Append(Quote(CsvCol_Download[i].Text)); gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);

                
                foreach (ManifestRecivingInquiry d in model)
                {
                    csvAddLine(gr, new string[] {
                    Equs(d.MANIFEST_NO), 
                    Equs(d.TMAP_ORDER_NO), 
                    Equs(d.PART_NO), 
                    Equs(d.TMAP_PART_NO),
                    Equs(d.ORDER_TYPE),
                    Equs(d.SUPPLIER_CD), 
                    Equs(d.RCV_PLANT_CD),
                    Equs(d.SUB_SUPPLIER_CD),
                    Equs(d.SUB_SUPPLIER_PLANT),
                    Equs(d.DOCK_CD), 
                    Equs(d.TMAP_ORDER_QTY),
                    Equs(d.RECEIVE_FLAG),
                    Equs(d.ORDER_RELEASE_DT),
                    Equs(d.SEND_PO_ADM_FLAG),
                    Equs(d.SEND_GR_ADM_FLAG)
                   });
                   
                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        public void DownloadData_ByTicked(object sender, EventArgs e, string pManifest_No, string pSupplier_Code, string pOrder_No, string pDock_Code, string pPlant_Code, string pPart_No, string pStartTmapOrdDt, string pEndTmapOrdDt, string pSubSupplier_Cd, string pSubSupplier_Plant, string pStartOrdRlsDt, string pEndOrdRlsDt, string pSendOrderADMStatus, string pSendGRADMStatus)
        {
            string filename = "";
            ManifestRecivingInquiry mri = new ManifestRecivingInquiry();
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPEX_Manifest_Report_" + date + ".csv";

            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            string dateNow = DateTime.Now.ToString();

            List<ManifestRecivingInquiry> model = new List<ManifestRecivingInquiry>();

            model = mri.getDownload_ByTicked(pManifest_No, pDock_Code, reFormatDate(pStartTmapOrdDt), reFormatDate(pEndTmapOrdDt), pSupplier_Code, pPlant_Code, pOrder_No, pPart_No, pSubSupplier_Cd, pSubSupplier_Plant, reFormatDate(pStartOrdRlsDt), reFormatDate(pEndOrdRlsDt), pSendOrderADMStatus, pSendGRADMStatus);

            StringBuilder gr = new StringBuilder("");

            if (model.Count > 0) 
            {
                for (int i = 0; i < CsvCol_DownloadByTick.Count; i++)
                {
                    gr.Append(Quote(CsvCol_DownloadByTick[i].Text)); gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);


                foreach (ManifestRecivingInquiry d in model)
                {
                    csvAddLine(gr, new string[] {
                    Equs(d.MANIFEST_NO), 
                    Equs(d.TMAP_ORDER_NO), 
                    Equs(d.PART_NO), 
                    Equs(d.TMAP_PART_NO),
                    Equs(d.ORDER_TYPE),
                    Equs(d.SUPPLIER_CD), 
                    Equs(d.RCV_PLANT_CD),
                    Equs(d.SUB_SUPPLIER_CD),
                    Equs(d.SUB_SUPPLIER_PLANT),
                    Equs(d.DOCK_CD), 
                    Equs(d.TMAP_ORDER_QTY),
                    Equs(d.RECEIVE_FLAG),
                    Equs(d.KANBAN_ID),
                    Equs(d.ORDER_RELEASE_DT),
                    Equs(d.SEND_PO_ADM_FLAG),
                    Equs(d.SEND_GR_ADM_FLAG)
                   });

                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        private List<CsvColumn> CsvCol_Download = CsvColumn.Parse(
            "Manifest No|C|1|0|MANIFEST_NO;" +
            "Order No|C|6|1|TMAP_ORDER_NO;" +
            "Part No|C|10|0|PART_NO;" +
            "TMAP Part No|C|10|0|TMAP_PART_NO;" +
            "Manifest Type|C|5|0|MANIFEST_TYPE;" +
            "Supplier Code|C|23|0|SUPPLIER_CD;" +
            "Plant Code|C|23|1|RCV_PLANT_CD;" +
            "Sub Supplier Code|C|23|0|SUB_SUPPLIER_CD;" +
            "Sub Supplier Plant|C|23|1|SUB_SUPPLIER_PLANT;" +
            "Dock Code|C|5|0|DOCK_CD;" +
            "Qty|C|1|0|TMAP_ORDER_QTY;" +
            "Receiving Status|C|1|0|RECEIVE_FLAG;" +
            "Release Date|C|10|0|ORDER_RELEASE_DT;" +
            "Send Order ADM Status|C|1|0|SEND_PO_ADM_FLAG;" +
            "Send GR ADM Status|C|1|0|SEND_GR_ADM_FLAG;"
            );

        private List<CsvColumn> CsvCol_DownloadByTick = CsvColumn.Parse(
            "Manifest No|C|1|0|MANIFEST_NO;" +
            "Order No|C|6|1|TMAP_ORDER_NO;" +
            "Part No|C|10|0|PART_NO;" +
            "TMAP Part No|C|10|0|TMAP_PART_NO;" +
            "Manifest Type|C|5|0|MANIFEST_TYPE;" +
            "Supplier Code|C|23|0|SUPPLIER_CD;" +
            "Plant Code|C|23|1|RCV_PLANT_CD;" +
            "Sub Supplier Code|C|23|0|SUB_SUPPLIER_CD;" +
            "Sub Supplier Plant|C|23|1|SUB_SUPPLIER_PLANT;" +
            "Dock Code|C|5|0|DOCK_CD;" +
            "Qty|C|1|0|TMAP_ORDER_QTY;" +
            "Receiving Status|C|1|0|RECEIVE_FLAG;" +
            "Release Date|C|10|0|ORDER_RELEASE_DT;" +
            "Kanban ID|C|1|0|KANBAN_ID;" +
            "Send Order ADM Status|C|1|0|SEND_PO_ADM_FLAG;" +
            "Send GR ADM Status|C|1|0|SEND_GR_ADM_FLAG;"
            );

        public class CsvColumn
        {
            public string Text { get; set; }
            public string Column { get; set; }
            public string DataType { get; set; }
            public int Len { get; set; }
            public int Mandatory { get; set; }

            public static List<CsvColumn> Parse(string v)
            {
                List<CsvColumn> l = new List<CsvColumn>();

                string[] x = v.Split(';');


                for (int i = 0; i < x.Length; i++)
                {
                    string[] y = x[i].Split('|');
                    CsvColumn me = null;
                    if (y.Length >= 4)
                    {
                        int len = 0;
                        int mandat = 0;
                        int.TryParse(y[2], out len);
                        int.TryParse(y[3], out mandat);
                        me = new CsvColumn()
                        {
                            Text = y[0],
                            DataType = y[1],
                            Len = len,
                            Mandatory = mandat
                        };
                        l.Add(me);
                    }

                    if (y.Length > 4 && me != null)
                    {
                        me.Column = y[4];
                    }
                }
                return l;
            }

        }

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        private string _csv_sep;
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {
                    _csv_sep = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ";";
                }
                return _csv_sep;
            }
        }

        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        private string Equs(string s)
        {
            string separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (s != null)
            {
                if (s.Contains(separator))
                {
                    s = "\"" + s + "\"";
                }
                else
                {
                    s = "=\"" + s + "\"";
                }
            }
            return s;
        }

        public ActionResult _popUpQty(string pManifestNo, string pPartNo, string pTmapOrderNo)
        {
            List<ManifestRecivingInquiry> model = new List<ManifestRecivingInquiry>();

            model = mri.getDetailQty(pManifestNo, pPartNo, pTmapOrderNo);

            ViewData["ContainerData"] = model;

            return PartialView("_PartialPopUpMaster");

        }

        public ActionResult getDetailQty(string pManifestNo, string pPartNo, string pTmapOrderNo)
        {
            List<ManifestRecivingInquiry> model = new List<ManifestRecivingInquiry>();

            model = mri.getDetailQty(pManifestNo, pPartNo, pTmapOrderNo);

            ViewData["ContainerData"] = model;

            return PartialView("_partial_load_detail_qty", model);
        }

    }
}

