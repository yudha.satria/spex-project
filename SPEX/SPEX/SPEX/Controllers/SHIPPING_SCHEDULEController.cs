﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
//Default Toyota Model
using Toyota.Common.Web.Platform;
using Toyota.Common.Web.Platform.Starter.Models;
using Toyota.Common.Credential;
using Toyota.Common.Database;
//Default Devexpress Model
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Web.UI;
using SPEX.Cls;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;

namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class SHIPPING_SCHEDULEController : PageController
    {
        //
        // GET: /BuyerPD/
        //mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();
        public  string moduleID = "F15";
        public  string functionID = "F15-009";
        public  string location = "SHIPPING SCHEDULE";
        Log log = new Log();
        public const string UploadDirectory = "Content/FileUploadResult";
        public const string TemplateFName = "UPLOAD_SHIPPING_SCHEDULE";
        public SHIPPING_SCHEDULEController()
        {
            Settings.Title = "SHIPPING SCHEDULE";        
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

         protected override void Startup()
        {
            //CallbackComboPriceTerm();

            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("SHIPPING_ CHEDULE Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            GetBuyyerForCombo();
            //GetSHIPPING_AGENTForCombo();
            //GetFORWARDING_AGENTForCombo();
            getpE_UserId = (User)ViewData["User"];
        }
         public void GetBuyyerForCombo()
         {
             string BUYER_CD =string.Empty;
             string BUYER_NAME = string.Empty;
             List<BUYER> model = new List<BUYER>();             
             model=GetListBUYER(BUYER_CD,BUYER_NAME);
             ViewData["BUYER"] = model;
         }
         public List<BUYER> GetListBUYER(string BUYER_CD, string BUYER_NAME)
         {
             
             List<BUYER> model = new List<BUYER>();
             BUYER BYR = new BUYER();
             BYR.BUYER_CD=string.IsNullOrEmpty(BUYER_CD)?string.Empty:BUYER_CD;
             BYR.BUYER_NAME = string.IsNullOrEmpty(BUYER_CD) ? string.Empty : BUYER_NAME;
             model = BYR.getBUYER(BYR);
             //ViewData["BUYER"] = model;
             return model;
         }
         //public void GetSHIPPING_AGENTForCombo()
         //{
         //    string SHIP_AGENT_CD = string.Empty;
         //    string SHIP_AGENT_NAME = string.Empty;
         //    List<SHIPPING_AGENT> model = new List<SHIPPING_AGENT>();
         //    model = GetListSHIPPING_AGENT(SHIP_AGENT_CD, SHIP_AGENT_NAME);
         //    ViewData["SHIPPING_AGENT"] = model;
         //}
         //public List<SHIPPING_AGENT> GetListSHIPPING_AGENT(string SHIP_AGENT_CD, string SHIP_AGENT_NAME)
         //{

         //    List<SHIPPING_AGENT> model = new List<SHIPPING_AGENT>();
         //    SHIPPING_AGENT SA = new SHIPPING_AGENT();
         //    SA.SHIP_AGENT_CD = string.IsNullOrEmpty(SHIP_AGENT_CD) ? string.Empty : SHIP_AGENT_CD;
         //    SA.SHIP_AGENT_NAME = string.IsNullOrEmpty(SHIP_AGENT_NAME) ? string.Empty : SHIP_AGENT_NAME;
         //    model = SA.getSHIPPING_AGENT(SA).ToList();
         //    //ViewData["BUYER"] = model;
         //    return model;
         //}
         //public void GetPORT_LOADINGForCombo()
         //{
         //    string PORT_LOADING_CD = string.Empty;
         //    string PORT_LOADING_NAME = string.Empty;
         //    List<PORT_LOADING> model = new List<PORT_LOADING>();
         //    model = GetListPORT_LOADING(PORT_LOADING_CD, PORT_LOADING_NAME);
         //    ViewData["PORT_LOADING"] = model;
         //}
         //public List<PORT_LOADING> GetListPORT_LOADING(string PORT_LOADING_CD, string PORT_LOADING_NAME)
         //{

         //    List<PORT_LOADING> model = new List<PORT_LOADING>();
         //    PORT_LOADING PL = new PORT_LOADING();
         //    PL.PORT_LOADING_CD = string.IsNullOrEmpty(PORT_LOADING_CD) ? string.Empty : PORT_LOADING_CD;
         //    PL.PORT_LOADING_NAME = string.IsNullOrEmpty(PORT_LOADING_NAME) ? string.Empty : PORT_LOADING_NAME;
         //    model = PL.getPORT_LOADING(PL).ToList();
         //    //ViewData["BUYER"] = model;
         //    return model;
         //}
         //public void GetFORWARDING_AGENTForCombo()
         //{
         //    string FORWARD_AGENT_CD = string.Empty;
         //    string FORWARD_AGENT_NAME = string.Empty;
         //    List<FORWARDING_AGENT> model = new List<FORWARDING_AGENT>();
         //    model = GetListFORWARDING_AGENT(FORWARD_AGENT_CD, FORWARD_AGENT_NAME);
         //    ViewData["FORWARDING_AGENT"] = model;
         //}

         //public List<FORWARDING_AGENT> GetListFORWARDING_AGENT(string FORWARD_AGENT_CD, string FORWARD_AGENT_NAME)
         //{

         //    List<FORWARDING_AGENT> model = new List<FORWARDING_AGENT>();
         //    FORWARDING_AGENT fa = new FORWARDING_AGENT();
         //    fa.FORWARD_AGENT_CD = string.IsNullOrEmpty(FORWARD_AGENT_CD) ? string.Empty : FORWARD_AGENT_CD;
         //    fa.FORWARD_AGENT_NAME = string.IsNullOrEmpty(FORWARD_AGENT_NAME) ? string.Empty : FORWARD_AGENT_NAME;
         //    model = fa.getFORWARDING_AGENT(fa).ToList();
         //    //ViewData["BUYER"] = model;
         //    return model;
         //}

         public ActionResult SHIPPING_SCHEDULECallBack(string BUYER_CD, DateTime VANNING_DT_FROM, DateTime VANNING_DT_TO, DateTime ETD_FROM, DateTime ETD_TO)
         {
             List<SHIPPING_SCHEDULE> model = new List<SHIPPING_SCHEDULE>();
             model = new SHIPPING_SCHEDULE().getSHIPPING_SCHEDULE(BUYER_CD, VANNING_DT_FROM, VANNING_DT_TO, ETD_FROM, ETD_TO);
             return PartialView("_SHIPPING_SCHEDULEGrid", model);
         }
         public ActionResult UploadExcel(HttpPostedFileBase fileexcel)
         {
             string FileName = fileexcel.FileName;
             //string fileName = Path.Combine(fileexcel.FileName);
             long pid = 0;
             string batchName = "SynchronizeBatch";
             string moduleID = "1";
             string functionID = "10011";
             string subFuncName = "UPLOAD DATA";
             string userBatch = getpE_UserId.Username;
             string location = "SHIPPING_SCHEDULE";
             Log log = new Log(); 
             pid = log.createLog(msgError.getMSPX00005INFForLog(batchName), userBatch, location + "." + subFuncName, 0, moduleID, functionID);
             var path_file = Path.Combine(Server.MapPath("~/FileUpload/RunDown/UploadCustomShippingScreen/CustomShipping"), FileName);
             string customShipping_dir = Server.MapPath("~/FileUpload/RunDown/UploadCustomShippingScreen/CustomShipping");
             if (Directory.Exists(customShipping_dir))
             {
                 fileexcel.SaveAs(path_file);
             }
             
             return RedirectToAction("");
         }
         
        
        // //function download      
        // public void DownloadBuyerPD(object sender, EventArgs e, string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION)
        // {
        //     string filename = "";
        //     string filesTmp = HttpContext.Request.MapPath("~/Template/BuyerPD_Code_Download.xls");
        //     FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

        //     HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

        //     ICellStyle styleContent = workbook.CreateCellStyle();
        //     styleContent.VerticalAlignment = VerticalAlignment.TOP;
        //     styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
        //     styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
        //     styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
        //     styleContent.Alignment = HorizontalAlignment.LEFT;

        //     ICellStyle styleContent2 = workbook.CreateCellStyle();
        //     styleContent2.VerticalAlignment = VerticalAlignment.TOP;
        //     styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
        //     styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
        //     styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
        //     styleContent2.Alignment = HorizontalAlignment.CENTER;

        //     ICellStyle styleContent3 = workbook.CreateCellStyle();
        //     styleContent3.VerticalAlignment = VerticalAlignment.TOP;
        //     styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
        //     styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
        //     styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
        //     styleContent3.Alignment = HorizontalAlignment.RIGHT;

        //     ISheet sheet = workbook.GetSheet("BuyerPD Code");
        //     string date = DateTime.Now.ToString("ddMMyyyy");
        //     filename = "BuyerPD Code Master" + date + ".xls";

        //     string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

        //     sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
        //     sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);
        //     sheet.GetRow(8).GetCell(2).SetCellValue(pBUYER_PD_CD);
        //     sheet.GetRow(9).GetCell(2).SetCellValue(pCUSTOMER_NO);
        //     sheet.GetRow(10).GetCell(2).SetCellValue(pDESCRIPTION);


        //     int row = 13;
        //     int rowNum = 1;
        //     IRow Hrow;

        //     List<mBuyerPD> model = new List<mBuyerPD>();

        //     model = BuyerPD.getListBuyerPD
        //         (pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION);

        //     foreach (var result in model)
        //     {
        //         Hrow = sheet.CreateRow(row);

        //         Hrow.CreateCell(1).SetCellValue(rowNum);
        //         Hrow.CreateCell(2).SetCellValue(result.BUYER_PD_CD);
        //         Hrow.CreateCell(3).SetCellValue(result.CUSTOMER_NO);
        //         Hrow.CreateCell(4).SetCellValue(result.DESCRIPTION);
        //         Hrow.CreateCell(5).SetCellValue(result.DELETION_FLAG);
        //         Hrow.CreateCell(6).SetCellValue(result.CREATED_BY);
        //         Hrow.CreateCell(7).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
        //         Hrow.CreateCell(8).SetCellValue(result.CHANGED_BY);
        //         Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

        //         Hrow.GetCell(1).CellStyle = styleContent3;
        //         Hrow.GetCell(2).CellStyle = styleContent2;
        //         Hrow.GetCell(3).CellStyle = styleContent2;
        //         Hrow.GetCell(4).CellStyle = styleContent;
        //         Hrow.GetCell(5).CellStyle = styleContent2;
        //         Hrow.GetCell(6).CellStyle = styleContent2;
        //         Hrow.GetCell(7).CellStyle = styleContent2;
        //         Hrow.GetCell(8).CellStyle = styleContent2;
        //         Hrow.GetCell(9).CellStyle = styleContent2;

        //         row++;
        //         rowNum++;
        //     }

        //     MemoryStream ms = new MemoryStream();
        //     workbook.Write(ms);
        //     ftmp.Close();
        //     Response.BinaryWrite(ms.ToArray());
        //     Response.ContentType = "application/vnd.ms-excel";
        //     Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        // }

        // #region Fucntion Delete Data
         public ActionResult DeleteSHIPPING_SCHEDULE(List<string> keyValues)
         {

             SHIPPING_SCHEDULE Ship = new SHIPPING_SCHEDULE();

             string resultMessage = Ship.DeleteDataTBR(keyValues);
             //if (resultMessage == "Error ")
             //{
               //  return Json(new { success = "true",messages = resultMessage }, JsonRequestBehavior.AllowGet);
             //}
             //else
             //{
                 return Json(new { success = "true", messages = resultMessage }, JsonRequestBehavior.AllowGet);
             //}

             return null;
         }
        //#endregion 

        // #region Function Add
         //public ActionResult AddNewSHIPPING_AGENT(string SHIP_AGENT_CD, string SHIP_AGENT_NAME, string PIC_NAME, string CONTACT_NO, string DESCR1, string DESCR2, string DESCR3)
         //{
         //    //p_CREATED_BY = getpE_UserId.Username;
         //    //p_CREATED_BY = "SYSTEM";
         //    string[] r = null;
         //    SHIPPING_AGENT ship = new SHIPPING_AGENT();
         //    ship.SHIP_AGENT_CD = SHIP_AGENT_CD;
         //    ship.SHIP_AGENT_NAME = SHIP_AGENT_NAME;
         //    ship.PIC_NAME = PIC_NAME;
         //    ship.CONTACT_NO = CONTACT_NO;
         //    ship.DESCR1 = DESCR1;
         //    ship.DESCR2 = DESCR2;
         //    ship.DESCR3 = DESCR3;
         //    ship.CREATED_BY = getpE_UserId.Username;
         //    string resultMessage = ship.SaveData(ship);

         //    r = resultMessage.Split('|');
         //    if (r[0] == "Error ")
         //    {
         //        return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
         //    }
         //    else
         //    {
         //        return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
         //    }
         //    return null;
         //}
        //#endregion
         public void DoWork(object obj)
         {
             // do some work
             string[] objs = obj.ToString().Split('|');
             long pid=Convert.ToInt64(objs[0]);
             string user_id =objs[1].ToString(); //getpE_UserId.Username;
             new SHIPPING_SCHEDULE().Save_Master_SHIPPING_SCHEDULE(Convert.ToInt64(pid), user_id);
         }

        //#region Function Edit -
         public ActionResult UploadBackgroundProsess()
         {
             string message = string.Empty;
             try
             {
             string user_id = getpE_UserId.Username;
             string subFuncName = "EXcelToSQL";
             long pid = log.createLog(msgError.getMSPX00005INFForLog("UPLOAD SHIPPING SCHEDULE"), user_id, location + '.' + subFuncName, 0, moduleID, functionID);
             Thread worker = new Thread(DoWork);
		     worker.IsBackground = true;
		     worker.SetApartmentState(System.Threading.ApartmentState.STA);
             worker.Start(pid.ToString()+"|"+user_id);
             //message = "Upload Success, and data will be process. To see progress, please see in log detail with process id:"+pid;
             message = msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2];
             return Json(new { success = "true", messages = message + '|' + pid }, JsonRequestBehavior.AllowGet);
             }
             catch  (Exception ex)
             {
                 return Json(new { success = "false", messages = ex.Message }, JsonRequestBehavior.AllowGet);
             }
             return null;
         }
        //#endregion
         public ActionResult CallbackUpload()
         {
             UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
             return null;
         }
         public class UploadControlHelper
         {
             public const string UploadDirectory = "Content/FileUploadResult";
             public const string TemplateFName = "Price_Master_Upload";
             public static readonly ValidationSettings ValidationSettings = new ValidationSettings
             {
                 AllowedFileExtensions = new string[] { ".xls", ".xlsx" },
                 MaxFileSize = 20971520
             };
         }

         public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
         {
             SHIPPING_SCHEDULE Ship=new SHIPPING_SCHEDULE();
             
             string[] r = null;
             string rExtract = string.Empty;
             string tb_t_name = "spex.TB_T_SHIPPING_SCHEDULE";
             #region createlog
             Guid PROCESS_ID = Guid.NewGuid();
             string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
             string MESSAGE_DETAIL = "";
             long pid = 0;
             //string subFuncName = "EXcelToSQL";
             string isError = "N";
             string processName = "SHIPPING SCHEDULE UPLOAD";
             string resultMessage = "";

             int columns;
             int rows;

             #endregion

             // MESSAGE_DETAIL = "Starting proces upload Price Master";
             SPEX.Cls.Lock L = new SPEX.Cls.Lock();
             
             int IsLock =L.is_lock(functionID);
             if (IsLock > 0)
             {
                 rExtract = "3|"+msgError.getMsgText("MSPX00092ERR");
             }
             //checking file 
             else if (e.UploadedFile.IsValid && IsLock==0)
             {
                     rExtract=Ship.DeleteDataTBT();
                     if (rExtract == "SUCCES")
                     {
                         string user_id = getpE_UserId.Username;

                         L.CREATED_BY = user_id;
                         L.CREATED_DT = DateTime.Now;
                         L.PROCESS_ID = pid;
                         L.FUNCTION_ID = functionID;
                         L.LOCK_REF = functionID;
                         L.LockFunction(L);
                         string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                         string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                         //lokasi path di server untuk file yang di upload
                         var pathfile = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                         //save file upload sesuai lokasi path di server
                         e.UploadedFile.SaveAs(pathfile);

                         //Function untuk nge-extract file excel
                         rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                         if (rExtract == string.Empty || rExtract == "SUCCES")
                         {
                             rExtract = Ship.CheckExistingShipingSchedule();
                         }
                     }
                     else
                     {
                         rExtract = "1|" + rExtract;
                     }
             }
             else
             {
                     //MESSAGE_DETAIL = "File is not .xls";
                     //pid = log.createLog(msgError.getMSPX00019ERR("Price"), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                      rExtract ="1| File is not .xls OR xlsx";
             }
             
             IUrlResolutionService urlResolver = sender as IUrlResolutionService;
             if (urlResolver != null)
                 e.CallbackData = rExtract;//+ "|" + pid.ToString();
         }

         public void UnLock()
         {
             Lock L = new Lock();
             L.UnlockFunction(functionID);
         }

    }
}
