﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class ShippingInstructionController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mShippingInstruction shipIns = new mShippingInstruction();

        public ShippingInstructionController()
        {
            Settings.Title = "Final Shipping Instruction";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];

            List<mShippingInstruction> containerSizeList = shipIns.getListBuyerCD();
            ViewData["DataBuyerMaster"] = containerSizeList;

            mInvoice Invoice = new mInvoice();
            ViewData["PackingCompanyList"] = Invoice.GetPackingCompany();

            //add agi 2017-10-23
            string range = new mSystemMaster().GetSystemValueList("vessel schedule inquiry", "range day", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            ViewBag.range = Convert.ToInt32(range);
            ViewBag.MSPXS0001ERR = msgError.getMsgText("MSPXS0001ERR").Replace("{0}", range);
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            //DD.MM.YYYY
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                //MM/DD/YYYY
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        //public ActionResult ShippingInstructionCallBack(string p_BUYER_CD, string p_CREATED_DT_FROM, string p_CREATED_DT_TO, 
        //    string p_VERSION, string p_PACKING_COMPANY, string p_SI_NO)
            //changed agi 2017-09-18 request mrs hesti 
        public ActionResult ShippingInstructionCallBack(string p_BUYER_CD, string p_CREATED_DT_FROM, string p_CREATED_DT_TO,
            string p_VERSION, string p_PACKING_COMPANY, string p_SI_NO, string pETD_FROM, string pETD_TO)
        {
            List<mShippingInstruction> model = new List<mShippingInstruction>();

            if (p_BUYER_CD == null)
                p_BUYER_CD = "";
           // model = shipIns.getListShippingInstruction(p_BUYER_CD, GetDate(p_CREATED_DT_FROM), GetDate(p_CREATED_DT_TO), p_VERSION, p_PACKING_COMPANY, p_SI_NO);
            model = shipIns.getListShippingInstruction(p_BUYER_CD, GetDate(p_CREATED_DT_FROM), GetDate(p_CREATED_DT_TO), p_VERSION, p_PACKING_COMPANY, p_SI_NO, GetDate(pETD_FROM), GetDate(pETD_TO));
            return PartialView("SIGrid", model);
        }

        public ActionResult GetSIDetailGrid(string pSHIP_INSTRUCTION_NO, string pVERSION)
        {
            List<mShippingInstruction> model = shipIns.getShippingInstructionDetail(pSHIP_INSTRUCTION_NO, pVERSION);
            return PartialView("SIDetailGrid", model);
        }

        //Pop Up
        public ActionResult GetSIInvoiceGrid(string pSHIP_INSTRUCTION_NO)
        {
            List<mShippingInstruction> model = shipIns.getListSIInvoice(pSHIP_INSTRUCTION_NO);
            return PartialView("SIInvoiceGrid", model);
        }

        public ActionResult PopUpSILayoutFirm(string pSHIP_INSTRUCTION_NO, string pINVOICE_LIST)
        {
            string pVERSION = "F";
            mShippingInstruction model = shipIns.getShippingInstruction(pSHIP_INSTRUCTION_NO, "T");
            model.VERSION = pVERSION;
            if (pVERSION == "T")
                ViewData["IsTent"] = true;
            else if (pVERSION == "F")
                ViewData["IsTent"] = false;

            List<mShippingInstruction> ContainerGradeList = shipIns.getListContainerGrade();
            ViewData["DataGradeMaster"] = ContainerGradeList;
            ViewData["Destination"] = model.BUYER_CD + " - " + model.BUYER_NAME;
            ViewData["InvoiceList"] = pINVOICE_LIST;

            return PartialView("~/Views/ShippingInstruction/SILayoutFirm.cshtml", model);
        }

        public ActionResult PopUpInvoice(string pSHIP_INSTRUCTION_NO)
        {
            mShippingInstruction model = shipIns.getShippingInstruction(pSHIP_INSTRUCTION_NO, "T");

            List<mShippingInstruction> listDat = shipIns.getListSIInvoice(pSHIP_INSTRUCTION_NO);

            ViewData["SIInvoiceList"] = listDat;

            return PartialView("~/Views/ShippingInstruction/SIInvoice.cshtml", model);
        }

        public ActionResult PopUpDetail(string pSHIP_INSTRUCTION_NO, string pVERSION)
        {
            List<mShippingInstruction> model = shipIns.getShippingInstructionDetail(pSHIP_INSTRUCTION_NO, pVERSION);
            return PartialView("~/Views/ShippingInstruction/SIDetailGrid.cshtml", model);
        }

        public string reFormatReportDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[0] + ar[1] + ar[2];
            }
            return result;
        }

        private DateTime GetDate(string dt)
        {
            if (dt == "01.01.0100")
                return new DateTime(1900, 1, 1);
            string[] ar = null;
            ar = dt.Split('.');
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;
            success = int.TryParse(ar[0].ToString(), out day);
            success = int.TryParse(ar[1].ToString(), out month);
            success = int.TryParse(ar[2].ToString(), out year);


            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        public ActionResult DeleteData(List<mShippingInstruction> ShippingInstructions)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string UserID = getpE_UserId.Username;
            foreach (mShippingInstruction siData in ShippingInstructions)
            {
                string resultMessage = shipIns.DeleteData(siData.SHIP_INSTRUCTION_NO, siData.VERSION, UserID);
                r = resultMessage.Split('|');
            }

            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckCreateTent(string pBUYER_CD, string pVANNING_DT_FROM, string pVANNING_DT_TO, string pPACKING_COMPANY)
        {
            string resultMessage = shipIns.CheckCreateTent(pBUYER_CD, GetDate(pVANNING_DT_FROM), GetDate(pVANNING_DT_TO), pPACKING_COMPANY);
            string[] r = null;
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckExist(string pSI_NO, string pVERSION)
        {
            bool exist = shipIns.CheckSINoExist(pSI_NO);

            if (exist)
                return Json(new { exist = "true", messages = "Exist" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { exist = "false", messages = "Not Exist" }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ConfirmTentSave(string pBUYER_CD, string pVANNING_DT_FROM, string pVANNING_DT_TO, string pPACKING_COMPANY, string pVERSION,
            string pSI_NO, string pSHIPPER, string pCONSIGNEE, string pNOTIFY_PARTY, string pCONTAINER_GRADE)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string UserID = getpE_UserId.Username;

            string resultMessage = shipIns.ConfirmTentSave(pBUYER_CD, GetDate(pVANNING_DT_FROM), GetDate(pVANNING_DT_TO), pPACKING_COMPANY, pVERSION,
                    pSI_NO, pSHIPPER, pCONSIGNEE, pNOTIFY_PARTY, pCONTAINER_GRADE, UserID);
            string[] r = null;
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("success"))
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ConfirmFirmSave(string pSHIP_INSTRUCTION_NO, string pVERSION, string pSHIPPER, string pCONSIGNEE, string pNOTIFY_PARTY, string pCONTAINER_GRADE, string pINVOICE_LIST)
        {
            Lock l = new Lock();
            long processID = 0;
            string FunctionID = "F15-037";

            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);
            try
            {
                MessagesString msg = new MessagesString();
                if (l.is_lock(FunctionID, out processID) == 1)
                    return Json(new { success = "false", messages = msg.getMSPX00150ERR(processID) }, JsonRequestBehavior.AllowGet);
                string UserID = getpE_UserId.Username;

                Log log = new Log();
                processID = log.createLog("MSPX00001INF|INF|" + msg.getMSPX00035INF("SI Firm Creation"), UserID, "SI.ConfirmFirm", 0, "F15", FunctionID);

                l.PROCESS_ID = processID;
                l.FUNCTION_ID = FunctionID;
                l.LOCK_REF = "";
                l.CREATED_BY = getpE_UserId.Username;
                l.CREATED_DT = DateTime.Now;
                l.LockFunction(l);

                List<mShippingInstruction> firmListResult = shipIns.ConfirmFirmSaveWithListResult(processID, pSHIP_INSTRUCTION_NO, pVERSION, pSHIPPER, pCONSIGNEE, pNOTIFY_PARTY, pCONTAINER_GRADE, pINVOICE_LIST, UserID);
                string[] r = null;

                r = firmListResult.First().Text.Split('|');
                if (r[0].ToString().ToLower().Contains("success"))
                {
                    string excelFileResult = GenerateExcelMainFirm(pSHIP_INSTRUCTION_NO, pVERSION, firmListResult);
                    string pdfFileResult = GeneratePDFReport(pSHIP_INSTRUCTION_NO, pVERSION, firmListResult);

                    processID = log.createLog("MSPX00001INF|INF|" + msg.getMSPX00001INF("SI Firm Creation"), UserID, "SI.ConfirmFirm", processID, "F15", FunctionID);

                    l.UnlockFunction(FunctionID);
                    return Json(new { success = "true", messages = r[1], excelFileResult = excelFileResult, pdfFileResult = pdfFileResult }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    processID = log.createLog("MSPX00001INF|INF|" + msg.getMSPX00012INF("SI Firm Creation"), UserID, "SI.ConfirmFirm", processID, "F15", FunctionID);
                    l.UnlockFunction(FunctionID);
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                l.UnlockFunction(FunctionID);

                return Json(new { success = "false", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            finally
            {

            }
        }

        public void GenerateExcelMainTent(string pSHIP_INSTRUCTION_NO, string pVERSION)
        {
            DeleteAllOlderFile(HttpContext.Request.MapPath("~/Content/ReportResult/"));

            string filename = "";
            string filesTmp = "";

            filesTmp = HttpContext.Request.MapPath("~/Template/SI_Download.xlsx");

            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            IDataFormat format = workbook.CreateDataFormat();

            ICellStyle styleDate1 = workbook.CreateCellStyle();
            styleDate1.DataFormat = format.GetFormat("mmmm d, yyyy");
            styleDate1.Alignment = HorizontalAlignment.Center;

            ICellStyle style1 = workbook.CreateCellStyle();
            style1.VerticalAlignment = VerticalAlignment.Center;
            style1.Alignment = HorizontalAlignment.Left;
            style1.WrapText = true;

            ISheet sheetMain = workbook.GetSheet("SILayout");
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            filename = "SI_Tentative_" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            mShippingInstruction model = new mShippingInstruction();

            model = shipIns.getShippingInstructionDownload(pSHIP_INSTRUCTION_NO, pVERSION, "");

            GenerateExcelHeaderShipperConsigneeNotifyParty(style1, sheetMain, model);
            GenerateExcelMesrsTent(sheetMain, model);
            GenerateExcelFeederConnectPort(styleDate1, sheetMain, model);
            GenerateExcelGoodsDescriptionTent(sheetMain, model);
            GenerateExcelGradeAndFooter(workbook, sheetMain, model);
            GenerateExcelMNOSSheet(workbook, model, false);
            GenerateExcelDeleteUnUsedSheetFirm(workbook);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));

        }

        private static void GenerateExcelGoodsDescriptionTent(ISheet sheetMain, mShippingInstruction model)
        {
            sheetMain.GetRow(29).GetCell(0).SetCellValue(model.XLS_MARKS_NUMBERS_LABEL);
            sheetMain.GetRow(32).GetCell(0).SetCellValue(string.Format(model.XLS_MARKS_NUMBER_DESC1_LABEL, "0"));
            sheetMain.GetRow(33).GetCell(0).SetCellValue(model.XLS_MARKS_NUMBER_DESC2_LABEL);


            sheetMain.GetRow(35).GetCell(0).SetCellValue(model.XLS_GENUINE_PARTS_LABEL);
            sheetMain.GetRow(36).GetCell(0).SetCellValue(string.Format(model.XLS_INVOICE_NO_LABEL, ""));


            sheetMain.GetRow(29).GetCell(6).SetCellValue(model.XLS_DESCRIPTION_GOODS_LABEL);
            sheetMain.GetRow(32).GetCell(7).SetCellValue(model.XLS_DESCRIPTION_GOODS_UNIT1_LABEL);
            sheetMain.GetRow(32).GetCell(9).SetCellValue(model.TOTAL_CASE);
            sheetMain.GetRow(32).GetCell(11).SetCellValue(model.XLS_DESCRIPTION_GOODS_UNIT2_LABEL);

            sheetMain.GetRow(29).GetCell(13).SetCellValue(model.XLS_GROSS_WEIGHT_LABEL);
            sheetMain.GetRow(30).GetCell(13).SetCellValue(model.XLS_GROSS_WEIGHT_UNIT_LABEL);
            sheetMain.GetRow(32).GetCell(16).SetCellValue(model.XLS_GROSS_WEIGHT_UNIT_DESC1_LABEL);
            sheetMain.GetRow(33).GetCell(16).SetCellValue(model.XLS_GROSS_WEIGHT_UNIT_DESC2_LABEL);

            sheetMain.GetRow(29).GetCell(17).SetCellValue(model.XLS_MEASUREMENT_LABEL);
            sheetMain.GetRow(30).GetCell(17).SetCellValue(model.XLS_MEASUREMENT_UNIT_LABEL);
            sheetMain.GetRow(32).GetCell(19).SetCellValue(model.XLS_MEASUREMENT_UNIT_LABEL);
            sheetMain.GetRow(32).GetCell(17).SetCellValue((double)model.MEASUREMENT);
        }

        private static void GenerateExcelMesrsTent(ISheet sheetMain, mShippingInstruction model)
        {
            sheetMain.GetRow(4).GetCell(11).SetCellValue(model.XLS_MESRS_LABEL);
            sheetMain.GetRow(6).GetCell(12).SetCellValue(model.SHIPPING_AGENT_NAME);
            sheetMain.GetRow(7).GetCell(12).SetCellValue(string.Format(model.XLS_ATTN1_LABEL, model.SHIP_AGENT_PIC_NAME == null ? "" : model.SHIP_AGENT_PIC_NAME));


            sheetMain.GetRow(9).GetCell(12).SetCellValue(model.FORWARD_AGENT_NAME);
            sheetMain.GetRow(10).GetCell(12).SetCellValue(model.XLS_ATTN2_LABEL);
            sheetMain.GetRow(11).GetCell(12).SetCellValue(model.XLS_FAX_NO_LABEL);
            sheetMain.GetRow(10).GetCell(12).SetCellValue(string.Format(model.XLS_ATTN2_LABEL, model.FORWARD_AGENT_PIC_NO == null ? "" : model.FORWARD_AGENT_PIC_NO));
            sheetMain.GetRow(11).GetCell(12).SetCellValue(string.Format(model.XLS_FAX_NO_LABEL, model.FORWARD_AGENT_CONTACT_NO == null ? "" : model.FORWARD_AGENT_CONTACT_NO));

            sheetMain.GetRow(13).GetCell(12).SetCellValue(string.Format(model.XLS_BOOKING_NO_LABEL, ""));
            sheetMain.GetRow(13).GetCell(16).SetCellValue(string.Format(model.BOOKING_NO, ""));

            sheetMain.GetRow(14).GetCell(12).SetCellValue(string.Format(model.XLS_PEB_NO_LABEL, ""));
            sheetMain.GetRow(14).GetCell(16).SetCellValue(string.Format(model.XLS_TGL_LABEL, ""));

            sheetMain.GetRow(16).GetCell(12).SetCellValue(model.XLS_CONTAINER_NO_LABEL);
            sheetMain.GetRow(16).GetCell(16).SetCellValue(model.XLS_SEAL_NO_LABEL);

            sheetMain.GetRow(25).GetCell(11).SetCellValue(model.XLS_SHIPPING_INSTRUCTION_LABEL);
            sheetMain.GetRow(27).GetCell(11).SetCellValue(model.XLS_SI_NO_LABEL);

            sheetMain.GetRow(25).GetCell(11).SetCellValue(model.XLS_SHIPPING_INSTRUCTION_LABEL);
            sheetMain.GetRow(27).GetCell(14).SetCellValue(model.SHIP_INSTRUCTION_NO);
        }

        private static void GenerateExcelDeleteUnUsedSheetFirm(XSSFWorkbook workbook)
        {
            ISheet sheet3 = workbook.GetSheet("Breakdown");
            ISheet sheet4 = workbook.GetSheet("TotContainer");

            workbook.RemoveSheetAt(workbook.GetSheetIndex(sheet3));
            workbook.RemoveSheetAt(workbook.GetSheetIndex(sheet4));
        }

        private void GenerateExcelGradeAndFooter(XSSFWorkbook workbook, ISheet sheet, mShippingInstruction model)
        {
            IRow Hrow;
            int rowAdd = 0;
            IFont font = workbook.CreateFont();
            font.FontName = "Tahoma";
            font.FontHeight = 10;

            IDataFormat format = workbook.CreateDataFormat();
            ICellStyle styleDate1 = workbook.CreateCellStyle();
            styleDate1.DataFormat = format.GetFormat("mmmm d, yyyy");
            styleDate1.Alignment = HorizontalAlignment.Center;
            styleDate1.BorderLeft = BorderStyle.Thin;
            styleDate1.BorderBottom = BorderStyle.Thin;
            styleDate1.BorderTop = BorderStyle.Thin;
            styleDate1.BorderRight = BorderStyle.Thin;
            styleDate1.SetFont(font);

            ICellStyle style = workbook.CreateCellStyle();
            styleDate1.Alignment = HorizontalAlignment.Left;
            styleDate1.SetFont(font);

            ICellStyle styleLocation = workbook.CreateCellStyle();
            styleLocation.Alignment = HorizontalAlignment.Center;
            styleLocation.VerticalAlignment = VerticalAlignment.Center;
            styleLocation.SetFont(font);

            ICellStyle styleTabLeft = workbook.CreateCellStyle();
            styleTabLeft.Alignment = HorizontalAlignment.Left;
            styleTabLeft.BorderLeft = BorderStyle.Thin;
            styleTabLeft.BorderRight = BorderStyle.Thin;
            styleTabLeft.BorderTop = BorderStyle.Thin;
            styleTabLeft.BorderBottom = BorderStyle.Thin;
            styleTabLeft.SetFont(font);

            ICellStyle styleTabCenter = workbook.CreateCellStyle();
            styleTabCenter.Alignment = HorizontalAlignment.Center;
            styleTabCenter.BorderLeft = BorderStyle.Thin;
            styleTabCenter.BorderRight = BorderStyle.Thin;
            styleTabCenter.BorderTop = BorderStyle.Thin;
            styleTabCenter.BorderBottom = BorderStyle.Thin;
            styleTabCenter.SetFont(font);

            ICellStyle styleLeftTop = workbook.CreateCellStyle();
            styleLeftTop.Alignment = HorizontalAlignment.Left;
            styleLeftTop.BorderLeft = BorderStyle.Thin;
            styleLeftTop.BorderTop = BorderStyle.Thin;
            styleLeftTop.SetFont(font);

            ICellStyle styleLeftButtom = workbook.CreateCellStyle();
            styleLeftButtom.Alignment = HorizontalAlignment.Left;
            styleLeftButtom.BorderLeft = BorderStyle.Thin;
            styleLeftButtom.BorderBottom = BorderStyle.Thin;
            styleLeftButtom.BorderRight = BorderStyle.None;
            styleLeftButtom.BorderTop = BorderStyle.None;
            styleLeftButtom.SetFont(font);

            ICellStyle styleTopButtom = workbook.CreateCellStyle();
            styleTopButtom.Alignment = HorizontalAlignment.Left;
            styleTopButtom.BorderTop = BorderStyle.Thin;
            styleTopButtom.BorderBottom = BorderStyle.Thin;
            styleTopButtom.SetFont(font);

            ICellStyle styleRightTop = workbook.CreateCellStyle();
            styleRightTop.Alignment = HorizontalAlignment.Left;
            styleRightTop.BorderRight = BorderStyle.Thin;
            styleRightTop.BorderTop = BorderStyle.Thin;
            styleRightTop.SetFont(font);

            ICellStyle styleRightButtom = workbook.CreateCellStyle();
            styleRightButtom.Alignment = HorizontalAlignment.Left;
            styleRightButtom.BorderRight = BorderStyle.Thin;
            styleRightButtom.BorderBottom = BorderStyle.Thin;
            styleRightButtom.SetFont(font);

            ICellStyle styleButtom = workbook.CreateCellStyle();
            styleButtom.Alignment = HorizontalAlignment.Left;
            styleButtom.BorderBottom = BorderStyle.Thin;
            styleButtom.SetFont(font);

            ICellStyle styleTop = workbook.CreateCellStyle();
            styleTop.Alignment = HorizontalAlignment.Center;
            styleTop.BorderTop = BorderStyle.Thin;
            styleTop.BorderBottom = BorderStyle.None;
            styleTop.SetFont(font);

            ICellStyle styleLeft = workbook.CreateCellStyle();
            styleLeft.Alignment = HorizontalAlignment.Left;
            styleLeft.BorderLeft = BorderStyle.Thin;
            styleLeft.BorderBottom = BorderStyle.None;
            styleLeft.BorderTop = BorderStyle.None;
            styleLeft.SetFont(font);

            ICellStyle styleRight = workbook.CreateCellStyle();
            styleRight.Alignment = HorizontalAlignment.Left;
            //styleRight.BorderLeft = BorderStyle.;
            styleRight.BorderTop = BorderStyle.None;
            styleRight.BorderRight = BorderStyle.Thin;
            styleRight.BorderBottom = BorderStyle.None;
            styleRight.SetFont(font);

            ICellStyle styleAllBorderCenter = workbook.CreateCellStyle();
            styleAllBorderCenter.Alignment = HorizontalAlignment.Center;
            styleAllBorderCenter.BorderLeft = BorderStyle.Thin;
            styleAllBorderCenter.BorderBottom = BorderStyle.Thin;
            styleAllBorderCenter.BorderTop = BorderStyle.Thin;
            styleAllBorderCenter.BorderRight = BorderStyle.Thin;
            styleAllBorderCenter.SetFont(font);

            List<mShippingInstruction> containerGradeList = shipIns.getListSIVanningDownloadData(model.SHIP_INSTRUCTION_NO, model.VERSION);
            int containerGradeCount = containerGradeList.Count();

            sheet.GetRow(38).GetCell(1).SetCellValue(string.Format(model.XLS_CONTAINER_GRADE_LABEL, model.CONTAINER_GRADE));

            rowAdd = containerGradeCount > 7 ? containerGradeCount - 6 : 0;

            sheet.GetRow(39).GetCell(1).SetCellValue(model.XLS_VANNING_DT_LABEL);
            sheet.GetRow(39).GetCell(3).SetCellValue(model.XLS_TOTAL_CONTAINER_LABEL);
            sheet.GetRow(39).GetCell(7).SetCellValue(model.XLS_LOCATION_CY_LABEL);
            sheet.GetRow(40).GetCell(3).SetCellValue(model.XLS_CONTAINER_PLAN_LABEL);
            sheet.GetRow(40).GetCell(5).SetCellValue(model.XLS_CONTAINER_ACTUAL_LABEL);

            int startRowDetail = 40;



            CellRangeAddress range = new CellRangeAddress(0, 0, 0, 0);

            foreach (var detail in containerGradeList)
            {
                startRowDetail++;
                Hrow = sheet.CreateRow(startRowDetail);

                Hrow.CreateCell(0).SetCellValue("");
                Hrow.CreateCell(0).CellStyle = styleLeft;

                Hrow.CreateCell(1).SetCellValue(((DateTime)detail.VANNING_DT).ToString("dd-MMM-yy"));
                Hrow.CreateCell(2).SetCellValue("");
                range = new CellRangeAddress(startRowDetail, startRowDetail, 1, 2);
                sheet.AddMergedRegion(range);
                Hrow.GetCell(1).CellStyle = styleTabLeft;
                Hrow.GetCell(2).CellStyle = styleTabLeft;
                Hrow.CreateCell(3).SetCellValue(detail.CONTAINER_QTYPLAN);
                Hrow.CreateCell(4).SetCellValue("");
                range = new CellRangeAddress(startRowDetail, startRowDetail, 3, 4);
                sheet.AddMergedRegion(range);
                Hrow.GetCell(3).CellStyle = styleTabCenter;
                Hrow.GetCell(4).CellStyle = styleTabCenter;
                if (model.VERSION == "T")
                    Hrow.CreateCell(5).SetCellValue("");
                else
                    Hrow.CreateCell(5).SetCellValue(detail.CONTAINER_QTYACTUAL);
                Hrow.CreateCell(6).SetCellValue("");
                range = new CellRangeAddress(startRowDetail, startRowDetail, 5, 6);
                sheet.AddMergedRegion(range);
                Hrow.GetCell(5).CellStyle = styleTabCenter;
                Hrow.GetCell(6).CellStyle = styleTabCenter;
                Hrow.CreateCell(8).SetCellValue("");
                Hrow.GetCell(8).CellStyle = styleTabCenter;

                if (startRowDetail == 41)
                    Hrow.CreateCell(7).SetCellValue(model.XLS_LOCATION_CY_DESC_LABEL);

                Hrow.CreateCell(19).SetCellValue("");
                Hrow.CreateCell(19).CellStyle = styleRight;

            }

            range = new CellRangeAddress(41, startRowDetail, 7, 8);
            sheet.AddMergedRegion(range);
            sheet.GetRow(41).GetCell(7).CellStyle = styleLocation;

            startRowDetail++;
            Hrow = sheet.CreateRow(startRowDetail);

            Hrow.CreateCell(0).SetCellValue("");
            Hrow.CreateCell(0).CellStyle = styleLeft;

            Hrow.CreateCell(1).SetCellValue(model.XLS_CONTAINER_TOTAL_QTY_LABEL);
            Hrow.CreateCell(2).SetCellValue("");
            range = new CellRangeAddress(startRowDetail, startRowDetail, 1, 2);
            sheet.AddMergedRegion(range);
            Hrow.GetCell(1).CellStyle = styleTabLeft;
            Hrow.GetCell(2).CellStyle = styleTabLeft;
            Hrow.CreateCell(3).SetCellValue(model.TOTAL_CONTAINER_PLAN);
            Hrow.CreateCell(4).SetCellValue("");
            range = new CellRangeAddress(startRowDetail, startRowDetail, 3, 4);
            sheet.AddMergedRegion(range);
            Hrow.GetCell(3).CellStyle = styleTabCenter;
            Hrow.GetCell(4).CellStyle = styleTabCenter;
            Hrow.CreateCell(5).SetCellValue(model.TOTAL_CONTAINER_ACTUAL);
            Hrow.CreateCell(6).SetCellValue("");
            range = new CellRangeAddress(startRowDetail, startRowDetail, 5, 6);
            sheet.AddMergedRegion(range);
            Hrow.GetCell(5).CellStyle = styleTabCenter;
            Hrow.GetCell(6).CellStyle = styleTabCenter;
            Hrow.CreateCell(7).SetCellValue(model.XLS_CONTAINER_TOTAL_LOCATION_LABEL);
            Hrow.CreateCell(8).SetCellValue("");
            range = new CellRangeAddress(startRowDetail, startRowDetail, 7, 8);
            sheet.AddMergedRegion(range);
            Hrow.GetCell(7).CellStyle = styleTabCenter;
            Hrow.GetCell(8).CellStyle = styleTabCenter;

            Hrow.CreateCell(19).SetCellValue("");
            Hrow.CreateCell(19).CellStyle = styleRight;


            int rowFooter = 49 + rowAdd;

            rowFooter = rowFooter + 3;
            Hrow = sheet.CreateRow(rowFooter);
            Hrow.CreateCell(0).SetCellValue(model.XLS_FREIGHT_PAYABLE_LABEL);
            Hrow.GetCell(0).CellStyle = styleLeftTop;
            Hrow.CreateCell(1).SetCellValue("");
            Hrow.GetCell(1).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(2).SetCellValue("");
            Hrow.GetCell(2).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(3).SetCellValue("");
            Hrow.GetCell(3).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(4).SetCellValue("");
            Hrow.GetCell(4).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 0, 4);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(5).SetCellValue(model.XLS_PREPAID_LABEL);
            Hrow.GetCell(5).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(6).SetCellValue("");
            Hrow.GetCell(6).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(7).SetCellValue("");
            Hrow.GetCell(7).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 5, 7);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(8).SetCellValue(model.XLS_COLLECT_LABEL);
            Hrow.GetCell(8).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(9).SetCellValue("");
            Hrow.GetCell(9).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(10).SetCellValue("");
            Hrow.GetCell(10).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 8, 10);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(11).SetCellValue(model.XLS_BL_RELEASE_LABEL);
            Hrow.GetCell(11).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(12).SetCellValue("");
            Hrow.GetCell(12).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(13).SetCellValue("");
            Hrow.GetCell(13).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(14).SetCellValue("");
            Hrow.GetCell(14).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 11, 14);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(15).SetCellValue(model.XLS_DATE_ISSUE_LABEL);
            Hrow.GetCell(15).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(16).SetCellValue("");
            Hrow.GetCell(16).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(17).SetCellValue("");
            Hrow.GetCell(17).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(18).SetCellValue("");
            Hrow.GetCell(18).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 15, 19);
            sheet.AddMergedRegion(range);

            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);
            Hrow.CreateCell(0).SetCellValue(model.FREIGHT_PAYABLE);
            Hrow.GetCell(0).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(1).SetCellValue("");
            Hrow.GetCell(1).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(2).SetCellValue("");
            Hrow.GetCell(2).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(3).SetCellValue("");
            Hrow.GetCell(3).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(4).SetCellValue("");
            Hrow.GetCell(4).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 0, 4);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(5).SetCellValue(model.PREPAID);
            Hrow.GetCell(5).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(6).SetCellValue("");
            Hrow.GetCell(6).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(7).SetCellValue("");
            Hrow.GetCell(7).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 5, 7);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(8).SetCellValue(model.COLLECT);
            Hrow.GetCell(8).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(9).SetCellValue("");
            Hrow.GetCell(9).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(10).SetCellValue("");
            Hrow.GetCell(10).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 8, 10);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(11).SetCellValue(model.BL_RELEASE);
            Hrow.GetCell(11).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(12).SetCellValue("");
            Hrow.GetCell(12).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(13).SetCellValue("");
            Hrow.GetCell(13).CellStyle = styleAllBorderCenter;
            Hrow.CreateCell(14).SetCellValue("");
            Hrow.GetCell(14).CellStyle = styleAllBorderCenter;
            range = new CellRangeAddress(rowFooter, rowFooter, 11, 14);
            sheet.AddMergedRegion(range);
            Hrow.CreateCell(15).SetCellValue((DateTime)model.ISSUE_DT);
            Hrow.GetCell(15).CellStyle = styleAllBorderCenter;
            Hrow.GetCell(15).CellStyle = styleDate1;
            Hrow.CreateCell(16).SetCellValue("");
            Hrow.GetCell(16).CellStyle = styleDate1;
            //Hrow.GetCell(16).CellStyle = styleLeftButtom;
            Hrow.CreateCell(17).SetCellValue("");
            Hrow.GetCell(17).CellStyle = styleDate1;
            Hrow.CreateCell(18).SetCellValue("");
            Hrow.GetCell(18).CellStyle = styleDate1;
            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleDate1;
            range = new CellRangeAddress(rowFooter, rowFooter, 15, 19);
            sheet.AddMergedRegion(range);

            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);
            Hrow.CreateCell(0).SetCellValue(model.XLS_NOTED_LABEL);
            Hrow.GetCell(0).CellStyle = styleLeft;
            Hrow.CreateCell(2).SetCellValue(model.XLS_NOTE_1_LABEL);
            Hrow.GetCell(2).CellStyle = style;
            Hrow.CreateCell(15).SetCellValue(model.XLS_APPROVE_BY_LABEL);
            Hrow.GetCell(15).CellStyle = styleLeft;
            Hrow.CreateCell(16).SetCellValue("");
            Hrow.GetCell(16).CellStyle = styleTop;
            Hrow.CreateCell(17).SetCellValue("");
            Hrow.GetCell(17).CellStyle = styleTop;
            Hrow.CreateCell(18).SetCellValue("");
            Hrow.GetCell(18).CellStyle = styleTop;
            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleRight;
            range = new CellRangeAddress(rowFooter, rowFooter, 15, 19);
            sheet.AddMergedRegion(range);
            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);
            Hrow.CreateCell(0).SetCellValue("");
            Hrow.GetCell(0).CellStyle = styleLeft;
            Hrow.CreateCell(2).SetCellValue(model.XLS_NOTE_2_LABEL);
            Hrow.GetCell(2).CellStyle = style;
            Hrow.CreateCell(15).SetCellValue("");
            Hrow.GetCell(15).CellStyle = styleLeft;
            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleRight;
            rowFooter++;
            if (model.VERSION == "F")
                ExcelInsertApprovedImage(workbook, sheet, HttpContext.Request.MapPath(model.APPROVED_BY), rowFooter);
            Hrow = sheet.CreateRow(rowFooter);
            Hrow.CreateCell(0).SetCellValue("");
            Hrow.GetCell(0).CellStyle = styleLeft;
            Hrow.CreateCell(2).SetCellValue(model.XLS_NOTE_3_LABEL);
            Hrow.GetCell(2).CellStyle = style;
            Hrow.CreateCell(15).SetCellValue("");
            Hrow.GetCell(15).CellStyle = styleLeft;
            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleRight;
            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);
            Hrow.CreateCell(0).SetCellValue("");
            Hrow.GetCell(0).CellStyle = styleLeft;
            Hrow.CreateCell(2).SetCellValue(model.XLS_NOTE_4_LABEL);
            Hrow.GetCell(2).CellStyle = style;
            Hrow.CreateCell(15).SetCellValue("");
            Hrow.GetCell(15).CellStyle = styleLeft;
            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleRight;
            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);
            Hrow.CreateCell(0).SetCellValue("");
            Hrow.GetCell(0).CellStyle = styleLeft;
            Hrow.CreateCell(2).SetCellValue(model.XLS_NOTE_5_LABEL);
            Hrow.GetCell(2).CellStyle = style;
            Hrow.CreateCell(15).SetCellValue("");
            Hrow.GetCell(15).CellStyle = styleLeft;
            Hrow.CreateCell(19).SetCellValue("");
            Hrow.GetCell(19).CellStyle = styleRight;

            rowFooter++;
            Hrow = sheet.CreateRow(rowFooter);
            for (int i = 0; i <= 19; i++)
            {
                Hrow.CreateCell(i).SetCellValue("");
                if (i==0)
                    Hrow.GetCell(i).CellStyle = styleLeftButtom;
                else if (i == 14)
                    Hrow.GetCell(i).CellStyle = styleRightButtom;
                else if (i==19)                    
                    Hrow.GetCell(i).CellStyle = styleRightButtom;
                else
                    Hrow.GetCell(i).CellStyle = styleButtom;

            }
        }

        private string GenerateExcelMainFirm(string pSHIP_INSTRUCTION_NO, string pVERSION, List<mShippingInstruction> firmList)
        {
            DeleteAllOlderFile(HttpContext.Request.MapPath("~/Content/ReportResult/"));

            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            string filename = "SI_Firm_" + date + ".xlsx";
            string fullFileName = HttpContext.Request.MapPath("~/Content/ReportResult/") + filename;
            string filesTmp = HttpContext.Request.MapPath("~/Template/SI_Download.xlsx");

            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            IDataFormat format = workbook.CreateDataFormat();


            ISheet sheetMain = workbook.GetSheet("SILayout");
            ISheet sheetMNOS = workbook.GetSheet("MNOS");
            ISheet sheetTotContainer = workbook.GetSheet("TotContainer");
            ISheet breakdownSheet = workbook.GetSheet("Breakdown");


            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            mShippingInstruction model = new mShippingInstruction();
            string SINoBreakdown = "";
            //List<mShippingInstruction> firmList = shipIns.GetListSIFirmDownload(pSHIP_INSTRUCTION_NO);

            foreach (mShippingInstruction firm in firmList)
            {
                SINoBreakdown += firm.SHIP_INSTRUCTION_NO + ';';
                ISheet newMainSheet = workbook.GetSheet("SILayout").CopySheet(firm.SHEET_NAME);
                model = new mShippingInstruction();
                //mShippingInstruction model = new mShippingInstruction();
                model = shipIns.getShippingInstructionDownload(firm.SHIP_INSTRUCTION_NO, pVERSION, firm.SHEET_NAME);
                bool IsCreateSheetContainer;

                GenerateExcelSIMainLayout(workbook, newMainSheet, model, out IsCreateSheetContainer);

                GenerateExcelMNOSSheet(workbook, model, true);

                if (IsCreateSheetContainer)
                    GenerateExcelTotalContainerSheet(firm.SHIP_INSTRUCTION_NO, pVERSION, workbook, model);
            }

            GenerateExcelBreakdownSheet(SINoBreakdown, pVERSION, workbook, model);

            workbook.RemoveSheetAt(workbook.GetSheetIndex(sheetMain));
            workbook.RemoveSheetAt(workbook.GetSheetIndex(sheetMNOS));
            workbook.RemoveSheetAt(workbook.GetSheetIndex(breakdownSheet));
            workbook.RemoveSheetAt(workbook.GetSheetIndex(sheetTotContainer));

            //MemoryStream ms = new MemoryStream();
            //workbook.Write(ms);

            //FileStream xfile = new FileStream(fullFileName, FileMode.Create, System.IO.FileAccess.Write);
            //byte[] bytes = new byte[ms.Length];
            //ms.Read(bytes, 0, (int)ms.Length);
            //xfile.Write(bytes, 0, bytes.Length);
            //xfile.Close();
            //ms.Close();

            using (var fileResult = new FileStream(fullFileName, FileMode.Create, FileAccess.ReadWrite))
            {
                workbook.Write(fileResult);
                fileResult.Close();
            }

            return filename;
        }

        public void GenerateExcelMainFirm(string pSHIP_INSTRUCTION_NO, string pVERSION)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/SI_Download.xlsx");

            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            IDataFormat format = workbook.CreateDataFormat();

            ISheet sheetMain = workbook.GetSheet("SILayout");
            ISheet sheetMNOS = workbook.GetSheet("MNOS");
            ISheet sheetTotContainer = workbook.GetSheet("TotContainer");
            ISheet breakdownSheet = workbook.GetSheet("Breakdown");

            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            filename = "SI_Firm_" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            mShippingInstruction model = new mShippingInstruction();

            List<mShippingInstruction> firmList = shipIns.GetListSIFirmDownload(pSHIP_INSTRUCTION_NO, getpE_UserId.Username);
            string SINoBreakdown = "";
            foreach (mShippingInstruction firm in firmList)
            {
                ISheet newMainSheet = workbook.GetSheet("SILayout").CopySheet(firm.SHEET_NAME);
                SINoBreakdown += firm.SHIP_INSTRUCTION_NO + ";";
                model = new mShippingInstruction();
                //mShippingInstruction model = new mShippingInstruction();
                model = shipIns.getShippingInstructionDownload(firm.SHIP_INSTRUCTION_NO, pVERSION, firm.SHEET_NAME);
                bool IsCreateSheetContainer;

                GenerateExcelSIMainLayout(workbook, newMainSheet, model, out IsCreateSheetContainer);

                GenerateExcelMNOSSheet(workbook, model, true);

                if (IsCreateSheetContainer)
                    GenerateExcelTotalContainerSheet(firm.SHIP_INSTRUCTION_NO, pVERSION, workbook, model);
                //else
                //{
                //    ISheet sheetTotContainer = workbook.GetSheet("TotContainer");
                //    workbook.RemoveSheetAt(workbook.GetSheetIndex(sheetTotContainer));
                //}
            }

            GenerateExcelBreakdownSheet(SINoBreakdown, pVERSION, workbook, model);

            workbook.RemoveSheetAt(workbook.GetSheetIndex(sheetMain));
            workbook.RemoveSheetAt(workbook.GetSheetIndex(sheetMNOS));
            workbook.RemoveSheetAt(workbook.GetSheetIndex(breakdownSheet));
            workbook.RemoveSheetAt(workbook.GetSheetIndex(sheetTotContainer));

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        private void GenerateExcelSIMainLayout(XSSFWorkbook workbook, ISheet sheetMain, mShippingInstruction model, out bool IsCreateSheetContainer)
        {
            int containerCount = 0;
            ICellStyle styleDate1 = workbook.CreateCellStyle();
            styleDate1.DataFormat = HSSFDataFormat.GetBuiltinFormat("mmmm d, yyyy");
            styleDate1.Alignment = HorizontalAlignment.Center;

            ICellStyle style1 = workbook.CreateCellStyle();
            style1.VerticalAlignment = VerticalAlignment.Center;
            style1.Alignment = HorizontalAlignment.Left;
            style1.WrapText = true;

            //add agi2017-09-11
            ICellStyle styleborderleftright = workbook.CreateCellStyle();
            styleborderleftright.VerticalAlignment = VerticalAlignment.Center;
            styleborderleftright.Alignment = HorizontalAlignment.Left;
            styleborderleftright.WrapText = true;
            styleborderleftright.BorderLeft = BorderStyle.Thin;
            styleborderleftright.BorderRight = BorderStyle.Thin;
            styleborderleftright.BorderTop = BorderStyle.None;
            styleborderleftright.BorderRight = BorderStyle.None;

            //GenerateExcelHeaderShipperConsigneeNotifyParty(style1, sheetMain, model);
            GenerateExcelHeaderShipperConsigneeNotifyParty(styleborderleftright, sheetMain, model);

            GenerateExcelMesrsFirm(workbook, sheetMain, model, out IsCreateSheetContainer, out containerCount);

            GenerateExcelFeederConnectPort(styleDate1, sheetMain, model);

            GenerateExcelGoodsDescriptionFirm(workbook, sheetMain, model, containerCount);

            GenerateExcelGradeAndFooter(workbook, sheetMain, model);

            if (model.XLS_SI_NEED_SPLIT_LABEL != "")
                AddNeedSplitExcelTextBoxt(sheetMain, workbook, model.XLS_SI_NEED_SPLIT_LABEL);

            workbook.SetSheetName(workbook.GetSheetIndex(sheetMain), model.SHEET_NAME);
        }

        private static void GenerateExcelGoodsDescriptionFirm(XSSFWorkbook workbook, ISheet sheetMain, mShippingInstruction model, int containerCount)
        {
            string InvoiceNo = string.Format(model.XLS_INVOICE_NO_LABEL, model.INVOICE_NO);
            IFont font = workbook.CreateFont();
            font.Color = HSSFColor.Black.Index;
            font.FontHeight = 11;
            font.FontName = "Tahoma";

            ICellStyle styleInvoice = workbook.CreateCellStyle();
            styleInvoice.SetFont(font);
            styleInvoice.Alignment = HorizontalAlignment.Left;
            styleInvoice.VerticalAlignment = VerticalAlignment.Top;
            styleInvoice.WrapText = true;
            //add agi 2017-09-20
            styleInvoice.BorderLeft = BorderStyle.Thin;
            styleInvoice.BorderTop = BorderStyle.None;
            styleInvoice.BorderBottom = BorderStyle.None;

            sheetMain.GetRow(36).GetCell(0).SetCellValue(InvoiceNo);
            sheetMain.GetRow(36).GetCell(0).CellStyle = styleInvoice;
            sheetMain.GetRow(36).Height = (short)(Math.Ceiling((decimal)(InvoiceNo.Length) / 75) * 300);

            sheetMain.GetRow(29).GetCell(0).SetCellValue(model.XLS_MARKS_NUMBERS_LABEL);
            sheetMain.GetRow(32).GetCell(0).SetCellValue(string.Format(model.XLS_MARKS_NUMBER_DESC1_LABEL, containerCount.ToString()));
            sheetMain.GetRow(33).GetCell(0).SetCellValue(model.XLS_MARKS_NUMBER_DESC2_LABEL);

            sheetMain.GetRow(35).GetCell(0).SetCellValue(model.XLS_GENUINE_PARTS_LABEL);

            sheetMain.GetRow(32).GetCell(19).SetCellValue(model.XLS_MEASUREMENT_UNIT_LABEL);
            sheetMain.GetRow(32).GetCell(18).SetCellValue(model.TOTAL_MEASUREMENT);

            sheetMain.GetRow(29).GetCell(6).SetCellValue(model.XLS_DESCRIPTION_GOODS_LABEL);
            sheetMain.GetRow(32).GetCell(7).SetCellValue(model.XLS_DESCRIPTION_GOODS_UNIT1_LABEL);
            sheetMain.GetRow(32).GetCell(6).SetCellValue(model.TOTAL_CASE);
            sheetMain.GetRow(32).GetCell(11).SetCellValue(model.XLS_DESCRIPTION_GOODS_UNIT2_LABEL);
            sheetMain.GetRow(32).GetCell(9).SetCellValue(model.TOTAL_PCS);

            sheetMain.GetRow(29).GetCell(13).SetCellValue(model.XLS_GROSS_WEIGHT_LABEL);
            sheetMain.GetRow(30).GetCell(13).SetCellValue(model.XLS_GROSS_WEIGHT_UNIT_LABEL);
            sheetMain.GetRow(32).GetCell(16).SetCellValue(model.XLS_GROSS_WEIGHT_UNIT_DESC1_LABEL);
            sheetMain.GetRow(33).GetCell(16).SetCellValue(model.XLS_GROSS_WEIGHT_UNIT_DESC2_LABEL);
            sheetMain.GetRow(32).GetCell(14).SetCellValue(model.TOTAL_GW);
            sheetMain.GetRow(33).GetCell(14).SetCellValue(model.TOTAL_NET);

            sheetMain.GetRow(29).GetCell(17).SetCellValue(model.XLS_MEASUREMENT_LABEL);
            sheetMain.GetRow(33).GetCell(16).SetCellValue(model.XLS_GROSS_WEIGHT_UNIT_DESC2_LABEL);
        }

        private static void GenerateExcelFeederConnectPort(ICellStyle styleDate1, ISheet sheetMain, mShippingInstruction model)
        {
            sheetMain.GetRow(23).GetCell(0).SetCellValue(model.XLS_FEEDER_VESSEL_LABEL);
            sheetMain.GetRow(23).GetCell(5).SetCellValue(model.XLS_FEEDER_VOY_LABEL);
            sheetMain.GetRow(23).GetCell(7).SetCellValue(model.XLS_SHIPMENT_LABEL);
            sheetMain.GetRow(24).GetCell(0).SetCellValue(model.FEEDER_VESSEL);
            sheetMain.GetRow(24).GetCell(5).SetCellValue(model.VOYAGE1);
            sheetMain.GetRow(24).GetCell(6).SetCellValue(((DateTime)model.SHIPMENT_DT).ToString("MMMM dd, yyyy"));
            sheetMain.GetRow(24).GetCell(6).CellStyle = styleDate1;

            sheetMain.GetRow(25).GetCell(0).SetCellValue(model.XLS_CONNECT_VESSEL_LABEL);
            sheetMain.GetRow(25).GetCell(5).SetCellValue(model.XLS_CONNECT_VOY_LABEL);
            sheetMain.GetRow(25).GetCell(7).SetCellValue(model.XLS_ARRIVAL_LABEL);
            sheetMain.GetRow(26).GetCell(0).SetCellValue(model.CONNECT_VESSEL);
            sheetMain.GetRow(26).GetCell(5).SetCellValue(model.VOYAGE2);

            if (model.ARRIVAL_DT != null)
            {
                sheetMain.GetRow(26).GetCell(6).SetCellValue(((DateTime)model.ARRIVAL_DT).ToString("MMMM dd, yyyy"));
                sheetMain.GetRow(26).GetCell(6).CellStyle = styleDate1;
            }

            sheetMain.GetRow(27).GetCell(0).SetCellValue(model.XLS_PORT_LOADING_LABEL);
            sheetMain.GetRow(28).GetCell(0).SetCellValue(model.PORT_LOADING);

            sheetMain.GetRow(27).GetCell(6).SetCellValue(model.XLS_PORT_DISCHARGE_LABEL);
            sheetMain.GetRow(28).GetCell(6).SetCellValue(model.PORT_DISCHARGE);

        }

        private void GenerateExcelMesrsFirm(XSSFWorkbook workbook, ISheet sheetMain, mShippingInstruction model, out bool IsCreateSheetContainer, out int containerCount)
        {
            IFont fontBlue = workbook.CreateFont();
            fontBlue.Color = HSSFColor.Blue.Index;
            fontBlue.FontName = "Tahoma";
            fontBlue.FontHeight = 11;

            IFont fontBlack = workbook.CreateFont();
            fontBlack.Color = HSSFColor.Blue.Index;
            fontBlack.FontName = "Tahoma";
            fontBlack.FontHeight = 11;

            ICellStyle styleContainerComplete = workbook.CreateCellStyle();
            styleContainerComplete.SetFont(fontBlack);

            ICellStyle styleContainerNotComplete = workbook.CreateCellStyle();
            styleContainerNotComplete.SetFont(fontBlue);
            styleContainerNotComplete.FillForegroundColor = HSSFColor.Yellow.Index;
            styleContainerNotComplete.FillPattern = FillPattern.SolidForeground;

            ICellStyle stylePEBAttachment = workbook.CreateCellStyle();
            stylePEBAttachment.SetFont(fontBlack);
            stylePEBAttachment.WrapText = true;


            sheetMain.GetRow(4).GetCell(11).SetCellValue(model.XLS_MESRS_LABEL);
            sheetMain.GetRow(6).GetCell(12).SetCellValue(model.SHIPPING_AGENT_NAME);
            sheetMain.GetRow(7).GetCell(12).SetCellValue(string.Format(model.XLS_ATTN1_LABEL, model.SHIP_AGENT_PIC_NAME == null ? "" : model.SHIP_AGENT_PIC_NAME));


            sheetMain.GetRow(9).GetCell(12).SetCellValue(model.FORWARD_AGENT_NAME);
            sheetMain.GetRow(10).GetCell(12).SetCellValue(model.XLS_ATTN2_LABEL);
            sheetMain.GetRow(11).GetCell(12).SetCellValue(model.XLS_FAX_NO_LABEL);
            sheetMain.GetRow(10).GetCell(12).SetCellValue(string.Format(model.XLS_ATTN2_LABEL, model.FORWARD_AGENT_PIC_NO == null ? "" : model.FORWARD_AGENT_PIC_NO));
            sheetMain.GetRow(11).GetCell(12).SetCellValue(string.Format(model.XLS_FAX_NO_LABEL, model.FORWARD_AGENT_CONTACT_NO == null ? "" : model.FORWARD_AGENT_CONTACT_NO));

            sheetMain.GetRow(13).GetCell(12).SetCellValue(model.XLS_BOOKING_NO_LABEL);
            sheetMain.GetRow(13).GetCell(12).CellStyle = styleContainerComplete;
            sheetMain.GetRow(13).GetCell(16).SetCellValue(model.BOOKING_NO);
            sheetMain.GetRow(13).GetCell(16).CellStyle = styleContainerComplete;

            IsCreateSheetContainer = false;

            List<mShippingInstruction> pebList = shipIns.getListSIPEBDownloadData(model.SHIP_INSTRUCTION_NO, model.VERSION);
            if (model.TOTAL_PEB > 2)
            {
                CellRangeAddress range = new CellRangeAddress(14, 15, 12, 15);
                sheetMain.AddMergedRegion(range);
                sheetMain.GetRow(14).GetCell(12).SetCellValue(string.Format(model.XLS_PEB_NO_LABEL, model.XLS_SEE_ATTACHMENT_LABEL));
                sheetMain.GetRow(14).GetCell(12).CellStyle = stylePEBAttachment;
                sheetMain.GetRow(14).GetCell(16).SetCellValue(string.Format(model.XLS_TGL_LABEL, ""));
                IsCreateSheetContainer = true;
            }
            else
            {
                int startPEBRow = 14;
                foreach (mShippingInstruction peb in pebList)
                {
                    sheetMain.GetRow(startPEBRow).GetCell(12).SetCellValue(string.Format(model.XLS_PEB_NO_LABEL, peb.PEB_NO));
                    sheetMain.GetRow(startPEBRow).GetCell(12).CellStyle = styleContainerComplete;
                    sheetMain.GetRow(startPEBRow).GetCell(16).SetCellValue(string.Format(model.XLS_TGL_LABEL, ((DateTime)peb.PEB_DT).ToString("dd-MM-yyyy")));
                    sheetMain.GetRow(startPEBRow).GetCell(16).CellStyle = styleContainerComplete;

                    startPEBRow++;
                }
            }

            sheetMain.GetRow(16).GetCell(12).SetCellValue(model.XLS_CONTAINER_NO_LABEL);
            sheetMain.GetRow(16).GetCell(12).CellStyle = styleContainerComplete;

            sheetMain.GetRow(16).GetCell(16).SetCellValue(model.XLS_SEAL_NO_LABEL);
            sheetMain.GetRow(16).GetCell(16).CellStyle = styleContainerComplete;
            List<mShippingInstruction> containerList = shipIns.getListSIContainerDownloadData(model.SHIP_INSTRUCTION_NO, model.VERSION, 1, getpE_UserId.Username);
            containerCount = containerList.Count;
            if (model.TOTAL_CONTAINER > 8)
            {
                sheetMain.GetRow(17).GetCell(12).SetCellValue(model.XLS_SEE_ATTACHMENT_LABEL);
                sheetMain.GetRow(17).GetCell(12).CellStyle = styleContainerComplete;
                IsCreateSheetContainer = true;
            }
            else
            {
                int startContainerRow = 17;
                for (int i = 0; i < containerList.Count; i++)
                {
                    mShippingInstruction container = containerList.Where(c => c.SEQ_NO == i + 1).FirstOrDefault();

                    sheetMain.GetRow(startContainerRow).GetCell(12).SetCellValue(container.CONTAINER_NO);
                    sheetMain.GetRow(startContainerRow).GetCell(16).SetCellValue(container.SEAL_NO);

                    if (container.IS_COMPLETE == true)
                    {
                        sheetMain.GetRow(startContainerRow).GetCell(12).CellStyle = styleContainerComplete;
                        sheetMain.GetRow(startContainerRow).GetCell(16).CellStyle = styleContainerComplete;
                    }
                    else
                    {
                        sheetMain.GetRow(startContainerRow).GetCell(12).CellStyle = styleContainerNotComplete;
                        sheetMain.GetRow(startContainerRow).GetCell(13).CellStyle = styleContainerNotComplete;
                        sheetMain.GetRow(startContainerRow).GetCell(14).CellStyle = styleContainerNotComplete;
                        sheetMain.GetRow(startContainerRow).GetCell(15).CellStyle = styleContainerNotComplete;
                        sheetMain.GetRow(startContainerRow).GetCell(16).CellStyle = styleContainerNotComplete;
                        sheetMain.GetRow(startContainerRow).GetCell(17).CellStyle = styleContainerNotComplete;
                        sheetMain.GetRow(startContainerRow).GetCell(18).CellStyle = styleContainerNotComplete;
                    }

                    startContainerRow++;
                }
            }

            sheetMain.GetRow(25).GetCell(11).SetCellValue(model.XLS_SHIPPING_INSTRUCTION_LABEL);
            sheetMain.GetRow(27).GetCell(11).SetCellValue(model.XLS_SI_NO_LABEL);


            sheetMain.GetRow(25).GetCell(11).SetCellValue(model.XLS_SHIPPING_INSTRUCTION_LABEL);
            sheetMain.GetRow(27).GetCell(14).SetCellValue(model.SHIP_INSTRUCTION_NO);
        }

        private static void GenerateExcelHeaderShipperConsigneeNotifyParty(ICellStyle style1, ISheet sheetMain, mShippingInstruction model)
        {
            sheetMain.GetRow(0).GetCell(0).SetCellValue(model.XLS_HEADER_1_LABEL);
            sheetMain.GetRow(1).GetCell(0).SetCellValue(model.XLS_HEADER_2_LABEL);
            sheetMain.GetRow(2).GetCell(0).SetCellValue(model.XLS_HEADER_3_LABEL);

            sheetMain.GetRow(4).GetCell(0).SetCellValue(model.XLS_SHIPPER_LABEL);
            sheetMain.GetRow(5).GetCell(0).SetCellValue(model.SHIPPER);
            sheetMain.GetRow(5).GetCell(0).CellStyle = style1;
            sheetMain.GetRow(9).GetCell(0).SetCellValue(model.XLS_CONSIGNEE_LABEL);
            sheetMain.GetRow(10).GetCell(0).SetCellValue(model.CONSIGNEE);
            sheetMain.GetRow(10).GetCell(0).CellStyle = style1;
            sheetMain.GetRow(16).GetCell(0).SetCellValue(model.XLS_NOTIFY_PARTY_LABEL);
            sheetMain.GetRow(17).GetCell(0).SetCellValue(model.NOTIFY_PARTY);
            sheetMain.GetRow(17).GetCell(0).CellStyle = style1;
        }

        private void GenerateExcelMNOSSheet(XSSFWorkbook workbook, mShippingInstruction model, bool isFirm)
        {
            IFont fontHeader = workbook.CreateFont();
            fontHeader.Color = HSSFColor.Black.Index;
            fontHeader.Boldweight = (short)FontBoldWeight.Bold;
            fontHeader.FontHeight = 12;
            fontHeader.IsItalic = true;
            fontHeader.FontName = "Tahoma";

            IFont fontDetail = workbook.CreateFont();
            fontDetail.Color = HSSFColor.Black.Index;
            fontDetail.Boldweight = (short)FontBoldWeight.Bold;
            fontDetail.FontHeight = 10;
            fontDetail.FontName = "Tahoma";

            ICellStyle styleHeader = workbook.CreateCellStyle();
            styleHeader.SetFont(fontHeader);

            ICellStyle styleDetail = workbook.CreateCellStyle();
            styleDetail.SetFont(fontDetail);


            IRow Hrow;
            ISheet sheetMNOS = workbook.GetSheet("MNOS").CopySheet("MNOS " + model.SHEET_NAME);

            Hrow = sheetMNOS.CreateRow(0);
            Hrow.CreateCell(1).SetCellValue(model.XLS_MNOS_DESC1_LABEL);
            Hrow.GetCell(1).CellStyle = styleHeader;

            int startRowCase = 2;
            if (isFirm)
            {
                List<mShippingInstruction> caseList = shipIns.getListSICaseDownloadData(model.SHIP_INSTRUCTION_NO, model.VERSION);
                foreach (mShippingInstruction caseData in caseList)
                {
                    Hrow = sheetMNOS.CreateRow(startRowCase);
                    Hrow.CreateCell(1).SetCellValue(caseData.CASE_NO);
                    Hrow.GetCell(1).CellStyle = styleDetail;

                    startRowCase++;
                }
            }

            startRowCase++;
            Hrow = sheetMNOS.CreateRow(startRowCase);
            Hrow.CreateCell(1).SetCellValue(model.XLS_MNOS_DESC2_LABEL);
            Hrow.GetCell(1).CellStyle = styleHeader;

            startRowCase++;
            Hrow = sheetMNOS.CreateRow(startRowCase);
            Hrow.CreateCell(1).SetCellValue(model.XLS_MNOS_DESC3_LABEL + model.XLS_MNOS_DESC4_LABEL);
            Hrow.GetCell(1).CellStyle = styleHeader;
        }

        private void ExcelInsertApprovedImage(XSSFWorkbook workbook, ISheet sheet1, string path, int colDest)
        {
            XSSFDrawing patriarch = (XSSFDrawing)sheet1.CreateDrawingPatriarch();
            XSSFClientAnchor anchor;
            anchor = new XSSFClientAnchor(0, 0, 0, 0, 16, colDest, 19, colDest);
            anchor.AnchorType = (AnchorType)2;
            //load the picture and get the picture index in the workbook
            XSSFPicture picture = (XSSFPicture)patriarch.CreatePicture(anchor, ExcelLoadImage(path, workbook));
            //Reset the image to the original size.
            picture.Resize();
            //picture.LineStyle = HSSFPicture.LINESTYLE_DASHDOTGEL;
        }

        public int ExcelLoadImage(string path, XSSFWorkbook workbook)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, (int)file.Length);
            return workbook.AddPicture(buffer, PictureType.JPEG);

        }

        private void GenerateExcelBreakdownSheet(string pSHIP_INSTRUCTION_NO, string pVERSION, XSSFWorkbook workbook, mShippingInstruction model)
        {
            IDataFormat dataFormat = workbook.CreateDataFormat();

            IFont fontHeader = workbook.CreateFont();
            fontHeader.Color = HSSFColor.Black.Index;
            fontHeader.Boldweight = (short)FontBoldWeight.Bold;
            fontHeader.FontHeight = 11;
            fontHeader.FontName = "Tahoma";

            IFont fontDetail = workbook.CreateFont();
            fontDetail.Color = HSSFColor.Black.Index;
            fontDetail.Boldweight = (short)FontBoldWeight.Normal;
            fontDetail.FontHeight = 10;
            fontDetail.FontName = "Tahoma";

            ICellStyle styleHeader = workbook.CreateCellStyle();
            styleHeader.SetFont(fontHeader);
            styleHeader.Alignment = HorizontalAlignment.Center;

            ICellStyle styleDetail = workbook.CreateCellStyle();
            styleDetail.SetFont(fontDetail);
            styleDetail.Alignment = HorizontalAlignment.Left;

            ICellStyle styleDetailNumeric = workbook.CreateCellStyle();
            styleDetailNumeric.SetFont(fontDetail);
            styleDetailNumeric.Alignment = HorizontalAlignment.Right;
            styleDetailNumeric.DataFormat = dataFormat.GetFormat("##,##0.0");

            ICellStyle styleHeaderNumeric = workbook.CreateCellStyle();
            styleHeaderNumeric.SetFont(fontHeader);
            styleHeaderNumeric.Alignment = HorizontalAlignment.Right;
            styleHeaderNumeric.DataFormat = dataFormat.GetFormat("##,##0.0");

            ICellStyle styleHeaderInt = workbook.CreateCellStyle();
            styleHeaderInt.SetFont(fontHeader);
            styleHeaderInt.Alignment = HorizontalAlignment.Right;
            styleHeaderInt.DataFormat = dataFormat.GetFormat("#####");

            ICellStyle styleDetailM3Numeric = workbook.CreateCellStyle();
            styleDetailM3Numeric.SetFont(fontDetail);
            styleDetailM3Numeric.Alignment = HorizontalAlignment.Right;
            //styleDetailNumeric.DataFormat = dataFormat.GetFormat("##,##0.0##");
            styleDetailM3Numeric.DataFormat = dataFormat.GetFormat("##,##0.000");

            ICellStyle styleHeaderM3Numeric = workbook.CreateCellStyle();
            styleHeaderM3Numeric.SetFont(fontHeader);
            styleHeaderM3Numeric.Alignment = HorizontalAlignment.Right;
            styleHeaderM3Numeric.DataFormat = dataFormat.GetFormat("##,##0.000");

            ISheet breakdownSheet = workbook.GetSheet("Breakdown").CopySheet("Breakdown " + model.SHIP_INSTRUCTION_NO.Substring(3, 4));
            List<mShippingInstruction> breakdownList = shipIns.getListSIBreakdownDownloadData(pSHIP_INSTRUCTION_NO, pVERSION);
            IRow Hrow;

            int startRowTotContainer = 0;

            //List<mShippingInstruction> invoices = breakdownList.GroupBy(b => new { b.INVOICE_NO }).Select(b => b.FirstOrDefault()).OrderBy(m => m.INVOICE_NO).ToList();
            //var invoices = breakdownList.GroupBy(x => new { x.INVOICE_NO, x.CONTAINER_NO }).Select(b => b.Select(c => new  { INVOICE_NO = c.INVOICE_NO, CONTAINER_NO = c.CONTAINER_NO })).ToList();
            var invoices=(from c in breakdownList
                orderby c.INVOICE_NO,c.CONTAINER_NO
                group c by new
                {
                    c.INVOICE_NO,
                    c.CONTAINER_NO
                } into gcs
                 select new mShippingInstruction()
                {
                    INVOICE_NO = gcs.Key.INVOICE_NO,
                    CONTAINER_NO = gcs.Key.CONTAINER_NO
                });
            //foreach (mShippingInstruction invoice in invoices)
            foreach (var invoice in invoices)
            {
                Hrow = breakdownSheet.CreateRow(startRowTotContainer);
                Hrow.CreateCell(0).SetCellValue(model.XLS_HEADER_TBL_NO_LABEL);
                Hrow.GetCell(0).CellStyle = styleHeader;
                Hrow.CreateCell(1).SetCellValue(model.XLS_HEADER_TBL_INVOICE_NO_LABEL);
                Hrow.GetCell(1).CellStyle = styleHeader;
                Hrow.CreateCell(2).SetCellValue(model.XLS_HEADER_TBL_CONTAINER_LABEL);
                Hrow.GetCell(2).CellStyle = styleHeader;
                Hrow.CreateCell(3).SetCellValue(model.XLS_HEADER_TBL_CASE_NO_LABEL);
                Hrow.GetCell(3).CellStyle = styleHeader;
                Hrow.CreateCell(4).SetCellValue(model.XLS_HEADER_TBL_NW_LABEL);
                Hrow.GetCell(4).CellStyle = styleHeader;
                Hrow.CreateCell(5).SetCellValue(model.XLS_HEADER_TBL_GW_LABEL);
                Hrow.GetCell(5).CellStyle = styleHeader;
                Hrow.CreateCell(6).SetCellValue(model.XLS_HEADER_TBL_M3_LABEL);
                Hrow.GetCell(6).CellStyle = styleHeaderM3Numeric;

                startRowTotContainer++;
                //List<mShippingInstruction> breakdowns = breakdownList.Where(b => b.INVOICE_NO == invoice.INVOICE_NO).OrderBy(b => b.CONTAINER_NO)
                //    .ThenBy(b => b.CASE_NO).ThenBy(b => b.NET_WEIGHT).ThenBy(b => b.GROSS_WEIGHT).ThenBy(b => b.MEASUREMENT).ToList();
                List<mShippingInstruction> breakdowns = breakdownList.Where(b => b.INVOICE_NO == invoice.INVOICE_NO && b.CONTAINER_NO == invoice.CONTAINER_NO).OrderBy(b => b.CONTAINER_NO)
                    .ThenBy(b => b.CASE_NO).ThenBy(b => b.NET_WEIGHT).ThenBy(b => b.GROSS_WEIGHT).ThenBy(b => b.MEASUREMENT).ToList();
                int startRow = 1;
                string invoice_no = "";
                foreach (mShippingInstruction breakdown in breakdowns)
                {
                    Hrow = breakdownSheet.CreateRow(startRowTotContainer);
                    Hrow.CreateCell(0).SetCellValue(startRow);
                    Hrow.GetCell(0).CellStyle = styleDetail;
                    Hrow.CreateCell(1).SetCellValue(breakdown.INVOICE_NO);
                    invoice_no = breakdown.INVOICE_NO;
                    Hrow.GetCell(1).CellStyle = styleDetail;
                    Hrow.CreateCell(2).SetCellValue(breakdown.CONTAINER_NO);
                    Hrow.GetCell(2).CellStyle = styleDetail;
                    Hrow.CreateCell(3).SetCellValue(breakdown.CASE_NO);
                    Hrow.GetCell(3).CellStyle = styleDetail;
                    Hrow.CreateCell(4).SetCellValue(breakdown.NET_WEIGHT);
                    Hrow.GetCell(4).CellStyle = styleDetailNumeric;
                    Hrow.CreateCell(5).SetCellValue(breakdown.GROSS_WEIGHT);
                    Hrow.GetCell(5).CellStyle = styleDetailNumeric;
                    Hrow.CreateCell(6).SetCellValue(breakdown.MEASUREMENT);
                    Hrow.GetCell(6).CellStyle = styleDetailM3Numeric;

                    startRowTotContainer++;
                    startRow++;
                }

                Hrow = breakdownSheet.CreateRow(startRowTotContainer);

                Hrow.CreateCell(2).SetCellValue(model.XLS_TOTAL_CASE_LABEL);
                Hrow.GetCell(2).CellStyle = styleHeader;
                //remaks agi 2017-09-13
                //var net = breakdowns.Where(b => b.INVOICE_NO == invoice_no).FirstOrDefault().TOTAL_NET_WEIGHT;
                //var gross = breakdowns.Where(b => b.INVOICE_NO == invoice_no).FirstOrDefault().TOTAL_GROSS_WEIGHT;
                //var measurement = breakdowns.Where(b => b.INVOICE_NO == invoice_no).FirstOrDefault().TOTAL_MEASUREMENT;
                //var total_case = breakdowns.Where(b => b.INVOICE_NO == invoice_no).FirstOrDefault().TOTAL_CASE;

                var net = breakdowns.Sum(b => b.NET_WEIGHT);
                var gross = breakdowns.Sum(b => b.GROSS_WEIGHT);
                var measurement = breakdowns.Sum(b => b.MEASUREMENT);
                var total_case = breakdowns.Count();
                Hrow.CreateCell(3).SetCellValue(total_case);
                Hrow.GetCell(3).CellStyle = styleHeaderInt;
                Hrow.CreateCell(4).SetCellValue((double)net);
                Hrow.GetCell(4).CellStyle = styleHeaderNumeric;
                Hrow.CreateCell(5).SetCellValue((double)gross);
                Hrow.GetCell(5).CellStyle = styleHeaderNumeric;
                Hrow.CreateCell(6).SetCellValue(measurement);
                Hrow.GetCell(6).CellStyle = styleHeaderM3Numeric;

                startRowTotContainer += 2;
            }
        }

        private void GenerateExcelTotalContainerSheet(string pSHIP_INSTRUCTION_NO, string pVERSION, XSSFWorkbook workbook, mShippingInstruction model)
        {
            string InvoiceNo = string.Format(model.XLS_TOT_CONTAINER_HDR_INVOICE_LABEL, model.INVOICE_NO);

            IDataFormat dataFormat = workbook.CreateDataFormat();

            IFont fontHeader = workbook.CreateFont();
            fontHeader.Color = HSSFColor.Black.Index;
            fontHeader.Boldweight = (short)FontBoldWeight.Bold;
            fontHeader.FontHeight = 11;
            fontHeader.FontName = "Tahoma";

            IFont fontBlue = workbook.CreateFont();
            fontBlue.Color = HSSFColor.Blue.Index;
            fontBlue.FontHeight = 10;
            fontBlue.FontName = "Tahoma";

            IFont fontBlack = workbook.CreateFont();
            fontBlack.Color = HSSFColor.Black.Index;
            fontBlack.FontHeight = 10;
            fontBlack.FontName = "Tahoma";

            ICellStyle styleHeader = workbook.CreateCellStyle();
            styleHeader.SetFont(fontHeader);
            styleHeader.Alignment = HorizontalAlignment.Center;

            ICellStyle styleHeaderLeft = workbook.CreateCellStyle();
            styleHeaderLeft.SetFont(fontHeader);
            styleHeaderLeft.Alignment = HorizontalAlignment.Left;
            styleHeaderLeft.WrapText = true;

            ICellStyle styleContainerComplete = workbook.CreateCellStyle();
            styleContainerComplete.SetFont(fontBlack);

            ICellStyle styleDateComplete = workbook.CreateCellStyle();
            styleDateComplete.DataFormat = dataFormat.GetFormat("m/d/yyyy");
            styleDateComplete.Alignment = HorizontalAlignment.Right;
            styleDateComplete.SetFont(fontBlack);

            ICellStyle styleDateNotComplete = workbook.CreateCellStyle();
            styleDateNotComplete.DataFormat = dataFormat.GetFormat("m/d/yyyy");
            styleDateNotComplete.Alignment = HorizontalAlignment.Right;
            //styleDateNotComplete.FillForegroundColor = HSSFColor.Yellow.Index;
            //styleDateNotComplete.FillPattern = FillPattern.SolidForeground;
            styleDateNotComplete.SetFont(fontBlue);


            ICellStyle styleContainerNotComplete = workbook.CreateCellStyle();
            styleContainerNotComplete.SetFont(fontBlue);

            ISheet sheetContainer = workbook.GetSheet("TotContainer").CopySheet("TOT. CONTAINER " + model.SHEET_NAME);


            List<mShippingInstruction> totContainerList = shipIns.getListSIContainerDownloadData(pSHIP_INSTRUCTION_NO, pVERSION, 2, getpE_UserId.Username);
            IRow Hrow;
            Hrow = sheetContainer.CreateRow(0);
            Hrow.CreateCell(1);
            Hrow.CreateCell(2);
            Hrow.CreateCell(3);
            Hrow.CreateCell(4);
            Hrow.CreateCell(5);
            Hrow.CreateCell(6);
            Hrow.CreateCell(7);
            Hrow.CreateCell(8);
            Hrow.CreateCell(9);
            Hrow.CreateCell(0);
            CellRangeAddress range = new CellRangeAddress(0, 0, 0, 9);
            sheetContainer.AddMergedRegion(range);
            Hrow.GetCell(0).SetCellValue(InvoiceNo);
            Hrow.Height = (short)(Math.Ceiling((decimal)(InvoiceNo.Length) / 80) * 300);
            Hrow.GetCell(0).CellStyle = styleHeaderLeft;

            Hrow = sheetContainer.CreateRow(1);
            Hrow.CreateCell(0).SetCellValue(model.XLS_HEADER_TBL_NO_LABEL);
            Hrow.GetCell(0).CellStyle = styleHeader;
            Hrow.CreateCell(1).SetCellValue(model.XLS_HEADER_TBL_CONTAINER_LABEL);
            Hrow.GetCell(1).CellStyle = styleHeader;
            Hrow.CreateCell(2).SetCellValue(model.XLS_HEADER_TBL_SEAL_LABEL);
            Hrow.GetCell(2).CellStyle = styleHeader;
            Hrow.CreateCell(4).SetCellValue(model.XLS_HEADER_TBL_PEB_NO_LABEL);
            Hrow.GetCell(4).CellStyle = styleHeader;
            Hrow.CreateCell(5).SetCellValue(model.XLS_HEADER_TBL_PEB_DT_LABEL);
            Hrow.GetCell(5).CellStyle = styleHeader;


            int startRowTotContainer = 2;

            for (int i = 0; i < totContainerList.Count; i++)
            {
                mShippingInstruction container = totContainerList.Where(c => c.SEQ_NO == i + 1).FirstOrDefault();

                Hrow = sheetContainer.CreateRow(startRowTotContainer);
                Hrow.CreateCell(0).SetCellValue(container.SEQ_NO);
                Hrow.CreateCell(1).SetCellValue(container.CONTAINER_NO);
                Hrow.CreateCell(2).SetCellValue(container.SEAL_NO);
                Hrow.CreateCell(4).SetCellValue(container.PEB_NO);

                Hrow.CreateCell(5).SetCellType(CellType.Numeric);
                Hrow.GetCell(5).SetCellValue(string.Format("{0:MM/dd/yyyy}", (DateTime)container.PEB_DT));

                if (container.IS_COMPLETE == true)
                {
                    Hrow.GetCell(0).CellStyle = styleContainerComplete;
                    Hrow.GetCell(1).CellStyle = styleContainerComplete;
                    Hrow.GetCell(2).CellStyle = styleContainerComplete;
                    Hrow.GetCell(4).CellStyle = styleContainerComplete;
                    Hrow.GetCell(5).CellStyle = styleDateComplete;
                }
                else
                {
                    Hrow.GetCell(0).CellStyle = styleContainerNotComplete;
                    Hrow.GetCell(1).CellStyle = styleContainerNotComplete;
                    Hrow.GetCell(2).CellStyle = styleContainerNotComplete;
                    Hrow.GetCell(4).CellStyle = styleContainerNotComplete;
                    Hrow.GetCell(5).CellStyle = styleDateNotComplete;
                }

                startRowTotContainer++;
            }
        }

        public string GeneratePDFReport(string pSHIP_INSTRUCTION_NO, string pVERSION, List<mShippingInstruction> firmList)
        {
            string result = "";

            string SINoBreakdown = "";
            foreach (mShippingInstruction firm in firmList)
            {
                SINoBreakdown += firm.SHIP_INSTRUCTION_NO + ";";
                result += GeneratePDFReport(firm.SHIP_INSTRUCTION_NO, pVERSION, firm.SHEET_NAME) + "|";
            }
            mShippingInstruction model = shipIns.getShippingInstructionDownload(firmList.First().SHIP_INSTRUCTION_NO, pVERSION, "");
            result += GeneratePDFBreakdownLayout(SINoBreakdown, pVERSION, model) + "|";


            return result.Remove(result.Length - 1, 1);
        }

        private string GeneratePDFReport(string pSHIP_INSTRUCTION_NO, string pVERSION, string pSHEET_NAME)
        {
            string result = "";

            mShippingInstruction model = shipIns.getShippingInstructionDownload(pSHIP_INSTRUCTION_NO, pVERSION, pSHEET_NAME);

            result += GeneratePDFFirmLayout(pSHIP_INSTRUCTION_NO, pVERSION, model) + "|";
            result += GeneratePDFMNOSLayout(pSHIP_INSTRUCTION_NO, pVERSION, model) + "|";
            //result += GeneratePDFBreakdownLayout(pSHIP_INSTRUCTION_NO, pVERSION, model) + "|";

            if (model.TOTAL_CONTAINER > 8 || model.TOTAL_PEB > 2)
                result += GeneratePDFTotalContainerLayout(pSHIP_INSTRUCTION_NO, pVERSION, model) + "|";


            return result.Remove(result.Length - 1, 1);
        }

        private string GeneratePDFFirmLayout(string pSHIP_INSTRUCTION_NO, string pVERSION, mShippingInstruction model)
        {
            string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("yyyyMMddHHmmss");
            mTelerik report;

            String source = Server.MapPath("~/Report/SI_Firm_Layout.trdx");
            String uri1 = Server.MapPath("~/Report/SI_Firm_PEBNo.trdx");
            String uri2 = Server.MapPath("~/Report/SI_Firm_ContainerSeal.trdx");
            String uri3 = Server.MapPath("~/Report/SI_Firm_ContainerGrade.trdx");
            FileInfo fileInfo = new FileInfo(source);

            pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
            Directory.CreateDirectory(pdfFolderPath);

            //DeleteOlderFile(pdfFolderPath);

            pdfFilename = model.SHEET_NAME + "_" + date;
            if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                System.IO.File.Delete(pdfFolderPath + pdfFilename);

            report = new mTelerik(source, pdfFilename);

            report.AddParameters("URI1", uri1);
            report.AddParameters("URI2", uri2);
            report.AddParameters("URI3", uri3);
            report.AddParameters("SHIP_INSTRUCTION_NO", pSHIP_INSTRUCTION_NO);
            report.AddParameters("VERSION", pVERSION);
            report.AddParameters("MODE", "1");
            report.AddParameters("APPROVAL_IMAGE_PATH", HttpContext.Request.MapPath(model.APPROVED_BY));
            report.AddParameters("USER_ID", getpE_UserId.Username);

            report.SetConnectionString(Common.GetTelerikConnectionString());
            Dictionary<string, string> uriParam = new Dictionary<string, string>();
            uriParam.Add("=Parameters.URI1.Value", uri1);
            uriParam.Add("=Parameters.URI2.Value", uri2);
            uriParam.Add("=Parameters.URI3.Value", uri3);

            report.SetSubConnectionString(uriParam, Common.GetTelerikConnectionString());

            using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
            {
                byte[] info = report.GeneratePDF();
                fs.Write(info, 0, info.Length);
            }

            return report.ResultName;
        }

        private string GeneratePDFMNOSLayout(string pSHIP_INSTRUCTION_NO, string pVERSION, mShippingInstruction model)
        {
            string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("yyyyMMddHHmmss");
            mTelerik report;

            String source = Server.MapPath("~/Report/SI_Firm_MNOS.trdx");
            FileInfo fileInfo = new FileInfo(source);

            pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
            Directory.CreateDirectory(pdfFolderPath);

            pdfFilename = "MNOS " + model.SHEET_NAME + "_" + date;
            if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                System.IO.File.Delete(pdfFolderPath + pdfFilename);


            report = new mTelerik(source, pdfFilename);
            report.AddParameters("HEADER", model.XLS_MNOS_DESC1_LABEL);
            report.AddParameters("FOOTER1", model.XLS_MNOS_DESC2_LABEL);
            report.AddParameters("FOOTER2", model.XLS_MNOS_DESC3_LABEL);
            report.AddParameters("FOOTER3", model.XLS_MNOS_DESC4_LABEL);

            report.SetConnectionString(Common.GetTelerikConnectionString());
            report.AddParameters("SHIP_INSTRUCTION_NO", pSHIP_INSTRUCTION_NO);
            report.AddParameters("VERSION", pVERSION);
            using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
            {
                byte[] info = report.GeneratePDF();
                fs.Write(info, 0, info.Length);
            }

            return report.ResultName;
        }

        private string GeneratePDFTotalContainerLayout(string pSHIP_INSTRUCTION_NO, string pVERSION, mShippingInstruction model)
        {
            string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("yyyyMMddHHmmss");
            mTelerik report;


            String source = Server.MapPath("~/Report/SI_Firm_TotalContainer.trdx");
            FileInfo fileInfo = new FileInfo(source);

            pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
            Directory.CreateDirectory(pdfFolderPath);

            //DeleteOlderFile(pdfFolderPath);

            pdfFilename = "TOT CONTAINER " + model.SHEET_NAME + "_" + date;
            if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                System.IO.File.Delete(pdfFolderPath + pdfFilename);

            report = new mTelerik(source, pdfFilename);

            report.AddParameters("INVOICE_NO_HEADER", string.Format(model.XLS_TOT_CONTAINER_HDR_INVOICE_LABEL, model.INVOICE_NO));
            report.AddParameters("NO_LABEL", model.XLS_HEADER_TBL_NO_LABEL);
            report.AddParameters("CONTAINER_NO_LABEL", model.XLS_HEADER_TBL_CONTAINER_LABEL);
            report.AddParameters("SEAL_NO_LABEL", model.XLS_HEADER_TBL_SEAL_LABEL);
            report.AddParameters("PEB_NO_LABEL", model.XLS_HEADER_TBL_PEB_NO_LABEL);
            report.AddParameters("PEB_DT_LABEL", model.XLS_HEADER_TBL_PEB_DT_LABEL);
            report.AddParameters("SHIP_INSTRUCTION_NO", pSHIP_INSTRUCTION_NO);
            report.AddParameters("VERSION", pVERSION);
            report.AddParameters("MODE", "2");
            report.AddParameters("USER_ID", getpE_UserId.Username);

            report.SetConnectionString(Common.GetTelerikConnectionString());

            using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
            {
                byte[] info = report.GeneratePDF();
                fs.Write(info, 0, info.Length);
            }

            return report.ResultName;
        }

        private string GeneratePDFBreakdownLayout(string pSHIP_INSTRUCTION_NO, string pVERSION, mShippingInstruction model)
        {
            string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("yyyyMMddHHmmss");
            mTelerik report;


            String source = Server.MapPath("~/Report/SI_Firm_Breakdown.trdx");
            FileInfo fileInfo = new FileInfo(source);

            pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
            Directory.CreateDirectory(pdfFolderPath);

            pdfFilename = "Breakdown " + model.SHIP_INSTRUCTION_NO.Substring(3, 4) + "_" + date;
            if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                System.IO.File.Delete(pdfFolderPath + pdfFilename);

            report = new mTelerik(source, pdfFilename);

            report.AddParameters("NO_LABEL", model.XLS_HEADER_TBL_NO_LABEL);
            report.AddParameters("INVOICE_NO_LABEL", model.XLS_HEADER_TBL_INVOICE_NO_LABEL);
            report.AddParameters("CONTAINER_NO_LABEL", model.XLS_HEADER_TBL_CONTAINER_LABEL);
            report.AddParameters("CASE_NO_LABEL", model.XLS_HEADER_TBL_CASE_NO_LABEL);
            report.AddParameters("NW_LABEL", model.XLS_HEADER_TBL_NW_LABEL);
            report.AddParameters("GW_LABEL", model.XLS_HEADER_TBL_GW_LABEL);
            report.AddParameters("M3_LABEL", model.XLS_HEADER_TBL_M3_LABEL);
            report.AddParameters("TOTAL_CASE_LABEL", model.XLS_TOTAL_CASE_LABEL);
            report.AddParameters("SHIP_INSTRUCTION_NO", pSHIP_INSTRUCTION_NO);
            report.AddParameters("VERSION", pVERSION);
            report.SetConnectionString(Common.GetTelerikConnectionString());

            using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
            {
                byte[] info = report.GeneratePDF();
                fs.Write(info, 0, info.Length);
            }

            return report.ResultName;
        }

        private void AddNeedSplitExcelTextBoxt(ISheet sheet, XSSFWorkbook workbook, string text)
        {
            // Create the Drawing patriarch.  This is the top level container for
            // all shapes. This will clear out any existing shapes for that sheet.
            XSSFDrawing patriarch = (XSSFDrawing)sheet.CreateDrawingPatriarch();
            IFont fontBlue = workbook.CreateFont();
            fontBlue.Color = HSSFColor.Blue.Index;
            fontBlue.FontHeight = 14;
            fontBlue.FontName = "Tahoma";
            fontBlue.Boldweight = (short)FontBoldWeight.Bold;

            XSSFRichTextString str = new XSSFRichTextString(text);
            str.ApplyFont(0, 28, fontBlue);


            IClientAnchor anc = new XSSFClientAnchor(0, 0, 0, 0, (short)12, 41, (short)18, 45);

            // Create a couple of textboxes
            XSSFTextBox textbox1 = patriarch.CreateTextbox(anc);
            textbox1.SetText(str);
            textbox1.VerticalAlignment = VerticalAlignment.Distributed;
        }

        private void DeleteAllOlderFile(string folderPath)
        {
            IEnumerable<string> allFiles = new List<string>();
            allFiles = System.IO.Directory.EnumerateFiles(folderPath, "*.*");
            foreach (string file in allFiles)
            {
                FileInfo info = new FileInfo(file);
                if (info.CreationTime <= DateTime.Now.AddDays(-1))
                    System.IO.File.Delete(file);
            }
        }

        public ActionResult CreateFirmValidation(string pINVOICE_LIST)
        {
            string resultMessage = shipIns.CreateFirmValidation(pINVOICE_LIST);
            string[] r = null;
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("success"))
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Reprint(string SI_NO,string VERSION)
        {
            //string SheetName=string.Empty;
            //mShippingInstruction model = shipIns.getShippingInstructionDownload(SI_NO, VERSION, SheetName);
            try
            {
                List<mShippingInstruction> firmListResult = shipIns.GetListRePrint(SI_NO, VERSION);

                string excelFileResult = GenerateExcelMainFirm(SI_NO, VERSION, firmListResult);
                string pdfFileResult = GeneratePDFReport(SI_NO, VERSION, firmListResult);


                return Json(new { success = "true", messages = "succes", excelFileResult = excelFileResult, pdfFileResult = pdfFileResult }, JsonRequestBehavior.AllowGet);

            }
            catch(Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message, msg = ex.Message }, JsonRequestBehavior.AllowGet);

            }
        }

        
    }
}
