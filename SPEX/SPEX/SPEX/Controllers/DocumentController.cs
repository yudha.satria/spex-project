﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxUploadControl;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.UI;
using DevExpress.Web.Internal;
using DevExpress.Web.Mvc.BinderSettings;
using System.Threading;

//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class DocumentController : PageController
    {
        //
        // GET: /BuyerPD/
        //mBuyerPD BuyerPD = new mBuyerPD();
        public string UploadDirectory=string.Empty;// {get { return new mSystemMaster().GetSystemValueList("Document", "", "4", ";").FirstOrDefault().SYSTEM_VALUE;}} //"~/Content/Document/";
        //public string moduleID = "5";
        public string functionID = "OD010";
        public string processName = "Upload Document";
        public string Message_Error = string.Empty;
        public int FileCount=0;
        
        public List<FileUploadCompleteEventArgs> Files = new List<FileUploadCompleteEventArgs>();
        //public string[] extensions { get { return new mSystemMaster().GetSystemValueList("DOCUMENT", "DOC_FILE_EXTENTION", "4", ";").FirstOrDefault().SYSTEM_VALUE.Split(';'); } }
        List<Document> FileNames = new List<Document>();
        MessagesString msgError = new MessagesString();
        int Error = 0;

        decimal AllFileSize { get; set; }

        public DocumentController()
        {
            Settings.Title = "Document";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //CallbackComboPriceTerm();

            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Document Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }


        public static readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions =new mSystemMaster().GetSystemValueList("DOCUMENT", "DOC_FILE_EXTENTION", "4", ";").Select(a=>a.SYSTEM_VALUE).ToArray(),//,//new string [] {".xlsx"},
            MaxFileSize =  (Convert.ToInt64(new mSystemMaster().GetSystemValueList("DOCUMENT", "DOC_MAXSIZE", "4", ";").FirstOrDefault().SYSTEM_VALUE)*1024)*1024,
            MaxFileSizeErrorText = new MessagesString().getMsgText("MSPXDOC02ERR").ToString(),
            NotAllowedFileExtensionErrorText = new MessagesString().getMsgText("MSPXDOC01ERR")
            
            //400000
            //4194304
        };
        public ActionResult CallbackUpload()
        {
            //UploadedFile[] files = UploadControlExtension.GetUploadedFiles("UploadButton", ValidationSettings);
            //Validate(files);
            //Lock L = new Lock();
            //L.LOCK_REF="";
            //L.PROCESS_ID=0;
            //L.REMARKS="";
            //L.CREATED_BY=getpE_UserId.Username;
            //L.CREATED_DT=DateTime.Now;
            //L.FUNCTION_ID = functionID;
            //L.LockFunction(L);
            UploadControlExtension.GetUploadedFiles("UploadButton", ValidationSettings, uc_FileUploadComplete);
            //Document d = new Document();
            //if (Error==0)
            //{
            //    try
            //    {
                    
            //        foreach (FileUploadCompleteEventArgs Fl in Files)
            //        {
            //            //Fl.UploadedFile.SaveAs()


            //            string FullFileName = Path.Combine(Server.MapPath(UploadDirectory), Fl.UploadedFile.FileName);
            //            d.UploadFile(Fl, FullFileName);
            //            //FileNames.Add(Fl.UploadedFile.FileName);
            //            //ViewBag.UploadMultiFile("sukses");
            //        }
            //        d.Save(FileNames,getpE_UserId.Username);
            //        d.Message = "upload sukses";
            //        ViewBag.Message = "upload sukses";
            //    }
            //    catch (Exception ex)
            //    {
            //        d.Message = ex.Message;
            //        ViewBag.Message = ex.Message; 
            //    }
                
            //    //Session.Add("Files", Files);

            //}
            //else
            //{
            //    d.Message = Message_Error;
            //    ViewBag.Message = Message_Error;
            //}
            //string JavaScriptName = "window.alert('"+Message_Error+"');";
            return null; //JavaScript(JavaScriptName); 
        }


        

        //public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        //{
        //    if(Error==0)
        //    {
                
        //        string extensionfile = Path.GetExtension(e.UploadedFile.FileName);
        //        //long size = ((e.UploadedFile.ContentLength / 1024)) / 1024;
        //        decimal size_convert = 1024;
        //        decimal size = ((Convert.ToDecimal(e.UploadedFile.ContentLength)) / size_convert) / size_convert;   //((Convert. e.UploadedFile.ContentLength / 1024)) / 1024;
        //        //1048576
        //        //1048576
        //        //AllFileSize = AllFileSize + (size);
        //        decimal MaxFileSize = 20;
        //        decimal AllMaxFileSize = 50;
        //        string IsExists=new Document().GetExistingDocument(e.UploadedFile.FileName);
        //        if (size > MaxFileSize)
        //        {
        //            e.CallbackData = "File [" + e.UploadedFile.FileName + "] size must be smaller than 20 MB";
        //            Error = Error + 1;//e.UploadedFile.FileName();
        //            Message_Error = "File [" + e.UploadedFile.FileName + "] size must be smaller than 20 MB";
        //        }
        //        else if (AllFileSize > AllMaxFileSize)
        //        {
        //            e.CallbackData = "Total storage for document not enough, please delete some documents to reduce storage";
        //            Error = Error + 1;
        //            Message_Error = "Total storage for document not enough, please delete some documents to reduce storage";
        //        }
        //        else if(IsExists=="true")
        //        {
        //            e.CallbackData = "Document "+e.UploadedFile.FileName+" already exist in server";
        //            Error = Error + 1;
        //            Message_Error = "Document " + e.UploadedFile.FileName + " already exist in server";
        //        }  
        //        else
        //        {
        //            Document Doc = new Document();
        //            //Doc.DOCNAME = e.UploadedFile.FileName;
        //            //Doc.DOC_SIZE = size;
        //            //FileNames.Add(Doc);
        //            //Files.Add(e);
        //            //string FullFileName = Path.Combine(Server.MapPath(UploadDirectory), e.UploadedFile.FileName);
        //            //Doc.UploadFile(e, FullFileName);
        //            //Doc.Save(e.UploadedFile.FileName, size, getpE_UserId.Username);
        //            Files.Add(e);
        //            Session["DocumentFiles"] = Files;
        //            if (!string.IsNullOrEmpty(Message_Error))
        //            {
        //            //    Message_Error = "Document" + e.UploadedFile.FileName + "Succes";
        //            //}
        //            //else
        //            //{
        //                Message_Error =Message_Error+ "<br/> Document" + e.UploadedFile.FileName + "Succes";
        //            }

        //            e.CallbackData =Error.ToString()+"|"+Message_Error;
        //            //Session.Add("Files", Files);
        //        }                
        //    }
        //    else
        //    {
        //        e.CallbackData = Message_Error;
        //    }
        //}
        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
           
            Files.Add(e);
            Session["DocumentFiles"] = Files;
            FileCount = FileCount + 1;
            e.CallbackData = FileCount.ToString();
        }

        //public ActionResult  CheckLock()
        //{
        //    //string functionID = "OD010";
        //    Lock L = new Lock();
        //    int Lock = L.is_lock(functionID);

        //    if (Lock == 0)
        //    {
        //        return Json(new { success = "true", messages = "" }, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        string msg = string.Empty;
        //        msg = msgError.getMsgText("MSPX00088ERR");
        //        return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
        //    }
        //    return null;
        //}

        public ActionResult Upload()
        {
            UploadDirectory = new mSystemMaster().GetSystemValueList("Document", "DOC_PATH", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            Message_Error = string.Empty;
            try
            {
                Validate();
            }
            catch ( Exception ex)
            {
                 return Json(new { success = "false", messages ="Validate"+ ex.Message }, JsonRequestBehavior.AllowGet);

            }
            
           // Lock L = new Lock();
            try
            {
                if (Error == 0)
                {
                    Document doc = new Document();
                    foreach (FileUploadCompleteEventArgs fl in Files)
                    {
                        string FullFileName = string.Empty;
                        //try
                        //{
                            FullFileName = Path.Combine(Server.MapPath(UploadDirectory), fl.UploadedFile.FileName);
                        //}
                        //catch (Exception ex)
                        //{
                        //    return Json(new { success = "false", messages = Message_Error+"|"+UploadDirectory }, JsonRequestBehavior.AllowGet);
                        //}
                            try
                            {
                                doc.UploadFile(fl, FullFileName);
                            }
                            catch (Exception ex)
                            {
                                return Json(new { success = "false", messages = "Upload"+ex.Message + "|" + FullFileName }, JsonRequestBehavior.AllowGet);
                            }
                        
                        decimal size_convert = 1024;
                        decimal size = 0;
                        try
                        {
                            size = ((Convert.ToDecimal(fl.UploadedFile.ContentLength)) / size_convert) / size_convert;
                        }
                        catch (Exception ex)
                        {
                            return Json(new { success = "false", messages ="Size"+ ex.Message + "|" + size.ToString() }, JsonRequestBehavior.AllowGet);
                        }
                        //((Convert. e.UploadedFile.ContentLength / 1024)) / 1024;
                        try
                        {
                            doc.Save(fl.UploadedFile.FileName, size, getpE_UserId.Username);
                        }
                        catch (Exception ex)
                        {
                            return Json(new { success = "false", messages ="Save"+ ex.Message + "|" + FullFileName }, JsonRequestBehavior.AllowGet);
                        }
                        

                    }

                    Session.Remove("DocumentFiles");
                    //L.UnlockFunction(functionID);
                    return Json(new { success = "true", messages = "Upload Document Succses" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Session.Remove("DocumentFiles");
                    //L.UnlockFunction(functionID);
                    return Json(new { success = "false", messages = Message_Error }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message  }, JsonRequestBehavior.AllowGet);
            }
           
        }


        public void Validate()
        {
            //string MESSAGE = string.Empty;
            Error = 0;
            Files =(List<FileUploadCompleteEventArgs>)Session["DocumentFiles"];
            foreach(FileUploadCompleteEventArgs fl in Files)
            {
                if (Error == 0)
                {
                    string extensionfile = Path.GetExtension(fl.UploadedFile.FileName);
                    decimal size_convert = 1024;
                    decimal size = ((Convert.ToDecimal(fl.UploadedFile.ContentLength)) / size_convert) / size_convert;
                    AllFileSize = AllFileSize + size;
                    Document doc= new Document();
                    decimal MaxFileSize = doc.GetMaxFileSize();
                    decimal AllMaxFileSize = doc.GetMaxAllFileSize();

                    string IsExists = doc.GetExistingDocument(fl.UploadedFile.FileName);
                    string Valid_Ext_File=doc.ExtFileCheck(Path.GetExtension(fl.UploadedFile.FileName));
                    MessagesString M = new MessagesString();
                    if (Valid_Ext_File=="false")
                    {
                        Error = Error + 1;
                        Message_Error = M.getMsgText("MSPXDOC01ERR").Replace("{0}", fl.UploadedFile.FileName); //"File = [ " + fl.UploadedFile.FileName + " ] extention not valid";
                    }
                    else if (size > MaxFileSize)
                    {
                        Error = Error + 1;//e.UploadedFile.FileName();
                        Message_Error = M.getMsgText("MSPXDOC02ERR").Replace("{0}", fl.UploadedFile.FileName).Replace("{1}",MaxFileSize.ToString()); //"File [" + fl.UploadedFile.FileName + "] size must be smaller than 20 MB";
                    }
                    //else if (AllFileSize > AllMaxFileSize)
                    //{
                    //    Error = Error + 1;
                    //    Message_Error = M.getMsgText("MSPXDOC03ERR");// "Total storage for document not enough, please delete some documents to reduce storage";
                    //}
                    else if (size +doc.GetExistingSize() > AllMaxFileSize)
                    {
                        Error = Error + 1;
                        Message_Error = M.getMsgText("MSPXDOC03ERR");// "Total storage for document not enough, please delete some documents to reduce storage";
                    }
                        
                    else if (IsExists == "true")
                    {
                        Error = Error + 1;
                        Message_Error = M.getMsgText("MSPXDOC04ERR").Replace("{0}",fl.UploadedFile.FileName); //"Document " + fl.UploadedFile.FileName + " already exist in server";
                    }
                    
                }
            }
        }

      


        public ActionResult DocumentSearch(string DocName)
        {
            List<Document> Docs = new List<Document>();
            Document Doc = new Document();
            Docs=Doc.Search(DocName);
            return PartialView("_PartialGrid", Docs);
        }

        public ActionResult Delete(string DocumentName)
        {
            string MESSAGE = string.Empty;
            UploadDirectory = new mSystemMaster().GetSystemValueList("Document", "DOC_PATH", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            Document DOC = new Document();
            MESSAGE =DOC.Delete(DocumentName);
            if (Directory.Exists(HttpContext.Request.MapPath(UploadDirectory)))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(HttpContext.Request.MapPath(UploadDirectory));
                FileInfo[] fileInfos = dirInfo.GetFiles(DocumentName);
                foreach (FileInfo fileInfo in fileInfos)
                {
                    if (System.IO.File.Exists(fileInfo.FullName))
                        System.IO.File.Delete(fileInfo.FullName);
                }
                //Directory.Delete(HttpContext.Request.MapPath(UploadDirectory + DocumentName));
            }
            return Json(new { success = "true", messages = MESSAGE }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UploadShow()
        {
            Files = (List<FileUploadCompleteEventArgs>)Session["DocumentFiles"];
            List<Document> Docs = new List<Document>();

            foreach(FileUploadCompleteEventArgs fl in Files)
            {
                Document doc = new Document();
                doc.DOCNAME=fl.UploadedFile.FileName;
                doc.DELETE_LINK_IMAGE=fl.UploadedFile.FileName;
                Docs.Add(doc);
            }
            return PartialView("_PartialDocumentUpload", Docs);
        }

        public ActionResult DeleteUpload(String DOCNAME)
        {
            Files = (List<FileUploadCompleteEventArgs>)Session["DocumentFiles"];
            FileUploadCompleteEventArgs FileDelete = null;
            foreach(FileUploadCompleteEventArgs fl in Files)
            {
                if(fl.UploadedFile.FileName==DOCNAME)
                {
                    FileDelete = fl;
                   
                }
            }
            Files.Remove(FileDelete);
            Session["DocumentFiles"] = Files;
            return Json(new { success = "true", messages = "succes" }, JsonRequestBehavior.AllowGet); ;
        }
        public ActionResult PopupPartial()
        {
            return PartialView();
        }
        
    }

    
}
