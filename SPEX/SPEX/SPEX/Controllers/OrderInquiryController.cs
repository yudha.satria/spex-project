﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using NPOI.XSSF.UserModel;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class OrderInquiryController : PageController
    {
        mOrderInquiry OrderInquiry = new mOrderInquiry();
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        
        public OrderInquiryController()
        {
            Settings.Title = "Order Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            CallbackOrderType();
            ViewBag.MSPX00003ERR = msgError.getMSPX00003ERR("Order Date From and Order Date To");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Order Inquiry");
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            ViewBag.MSPX00009INB = msgError.getMSPX00009INB("");
            ViewBag.MSPX00010INB = msgError.getMSPX00010INB("");
            getpE_UserId = (User)ViewData["User"];
            ComboPackingCompany();
            ComboPartType();
            ComboSupplier();
            ComboSubSupplier();
            ComboOBR();
        }

        public void ComboPackingCompany()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();

            model = Common.GetPackingCompanyList();
            ViewData["PACKING_COMPANY"] = model;
        }

        public void ComboPartType()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();

            model = Common.GetPartTypeList();
            ViewData["PART_TYPE"] = model;
        }

        public void ComboSupplier()
        {
            List<string> model = new List<string>();

            model = Common.GetSupplierCDPlantList();
            ViewData["SUPPLIER"] = model;
        }

        public void ComboSubSupplier()
        {
            List<string> model = new List<string>();

            model = Common.GetSubSupplierCDPlantList();
            ViewData["SUB_SUPPLIER"] = model;
        }
        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
                //result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));
            }

            return result;
        }

        public string reFormatDate_Download(string dt)
        {
            //string result = "";

            //if (dt != "01.01.0100") result = string.Format("{0:dd.MM.yyyy}", DateTime.Parse(dt));
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
                //result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));
            }

            return result;
        }

        [HttpPost, ValidateInput(false)]
        //remaks agi 2017-07-12 
        //public ActionResult OrderInquiryCallBack(string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_TMAP_ORDER_NO, string p_ITEM_NO,
        //    string p_PART_NO, string p_ORDER_TYPE, string p_VALIDATION_FLAG, string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_TMMIN_DEL_PLAN_DT,
        //    string p_TMMIN_DEL_PLAN_DT_TO, string p_SEND_DT, string p_SEND_DT_TO, string p_SEND_TO_TAM, string p_PACKING_COMPANY, string p_PART_TYPE, string p_SUPPLIER)
        public ActionResult OrderInquiryCallBack(string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_TMAP_ORDER_NO, string p_ITEM_NO,
            string p_PART_NO, string p_ORDER_TYPE, string p_VALIDATION_FLAG, string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_TMMIN_DEL_PLAN_DT,
            string p_TMMIN_DEL_PLAN_DT_TO, string p_SEND_DT, string p_SEND_DT_TO, string p_SEND_TO_TAM, string p_PACKING_COMPANY, string p_PART_TYPE, string p_SUPPLIER, string p_SUB_SUPPLIER, string p_OBR_STATUS)
        {
            List<mOrderInquiry> model = new List<mOrderInquiry>();

            p_TMAP_ORDER_DT = reFormatDate(p_TMAP_ORDER_DT);
            p_TMAP_ORDER_DT_TO = reFormatDate(p_TMAP_ORDER_DT_TO);
            p_TMMIN_REC_DT = reFormatDate(p_TMMIN_REC_DT);
            p_TMMIN_REC_DT_TO = reFormatDate(p_TMMIN_REC_DT_TO);
            p_TMMIN_DEL_PLAN_DT = reFormatDate(p_TMMIN_DEL_PLAN_DT);
            p_TMMIN_DEL_PLAN_DT_TO = reFormatDate(p_TMMIN_DEL_PLAN_DT_TO);
            p_SEND_DT = reFormatDate(p_SEND_DT);
            p_SEND_DT_TO = reFormatDate(p_SEND_DT_TO);

            {
                //remaks agi 2017-07-12 
                //model = OrderInquiry.getListOrderInquiry(p_TMAP_ORDER_DT, p_TMAP_ORDER_DT_TO, p_TMAP_ORDER_NO, p_ITEM_NO, p_PART_NO, p_ORDER_TYPE, p_VALIDATION_FLAG,
                //p_TMMIN_REC_DT, p_TMMIN_REC_DT_TO, p_TMMIN_DEL_PLAN_DT, p_TMMIN_DEL_PLAN_DT_TO, p_SEND_DT, p_SEND_DT_TO, p_SEND_TO_TAM, p_PACKING_COMPANY, p_PART_TYPE, p_SUPPLIER);

                //add agi 2017-07-12 obr status
                model = OrderInquiry.getListOrderInquiry(p_TMAP_ORDER_DT, p_TMAP_ORDER_DT_TO, p_TMAP_ORDER_NO, p_ITEM_NO, p_PART_NO, p_ORDER_TYPE, p_VALIDATION_FLAG,
                p_TMMIN_REC_DT, p_TMMIN_REC_DT_TO, p_TMMIN_DEL_PLAN_DT, p_TMMIN_DEL_PLAN_DT_TO, p_SEND_DT, p_SEND_DT_TO, p_SEND_TO_TAM, p_PACKING_COMPANY, p_PART_TYPE, p_SUPPLIER, p_SUB_SUPPLIER, p_OBR_STATUS);
            }
            return PartialView("OrderInquiryGrid", model);
        }

        //get order type
        public void CallbackOrderType()
        {
            List<mOrderInquiry> modelOrderType = OrderInquiry.getListOrderType();
            ViewData["GridOrderType"] = modelOrderType;
        }

        //function download Order Inquiry by ticked checkbox 
        //REMAKS AGI 2017-07-12 OBR
        //public void DownloadOrderInquiry_ByTicked(object sender, EventArgs e, string p_Order, string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_TMAP_ORDER_NO, string p_ITEM_NO,
        //    string p_PART_NO, string p_ORDER_TYPE, string p_VALIDATION_FLAG, string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_TMMIN_DEL_PLAN_DT, string p_TMMIN_DEL_PLAN_DT_TO,
        //    string p_SEND_DT, string p_SEND_DT_TO, string p_SEND_TO_TAM, string p_PACKING_COMPANY, string p_PART_TYPE)
        public void DownloadOrderInquiry_ByTicked(object sender, EventArgs e, string p_Order, string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_TMAP_ORDER_NO, string p_ITEM_NO,
            string p_PART_NO, string p_ORDER_TYPE, string p_VALIDATION_FLAG, string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_TMMIN_DEL_PLAN_DT, string p_TMMIN_DEL_PLAN_DT_TO,
            string p_SEND_DT, string p_SEND_DT_TO, string p_SEND_TO_TAM, string p_PACKING_COMPANY, string p_PART_TYPE, string p_OBR_STATUS)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Order_Inquiry_Download2.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Order Inquiry");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "OrderInquiry" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(p_TMAP_ORDER_DT + "   to   " + p_TMAP_ORDER_DT_TO);
            sheet.GetRow(9).GetCell(2).SetCellValue(p_TMAP_ORDER_NO);
            sheet.GetRow(10).GetCell(2).SetCellValue(p_ITEM_NO);
            sheet.GetRow(11).GetCell(2).SetCellValue(p_PART_NO);
            sheet.GetRow(12).GetCell(2).SetCellValue(p_ORDER_TYPE);

            sheet.GetRow(8).GetCell(6).SetCellValue(p_VALIDATION_FLAG);
            sheet.GetRow(9).GetCell(6).SetCellValue(p_SEND_TO_TAM);
            sheet.GetRow(10).GetCell(6).SetCellValue(reFormatDate_Download(p_TMMIN_REC_DT) + "   to   " + reFormatDate_Download(p_TMMIN_REC_DT_TO));
            sheet.GetRow(11).GetCell(6).SetCellValue(reFormatDate_Download(p_SEND_DT) + "   to   " + reFormatDate_Download(p_SEND_DT_TO));
            sheet.GetRow(12).GetCell(6).SetCellValue(reFormatDate_Download(p_TMMIN_DEL_PLAN_DT) + "   to   " + reFormatDate_Download(p_TMMIN_DEL_PLAN_DT_TO));

            int row = 16;
            int rowNum = 1;
            IRow Hrow;

            List<mOrderInquiry> model = new List<mOrderInquiry>();

            model = OrderInquiry.getListOrderInquiry_Download(p_Order);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.TMAP_ORDER_NO);
                Hrow.CreateCell(3).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(4).SetCellValue(result.PART_NO);
                Hrow.CreateCell(5).SetCellValue(result.ORDER_QTY.ToString());
                Hrow.CreateCell(6).SetCellValue(result.ADJUST_QTY.ToString());
                Hrow.CreateCell(7).SetCellValue(result.ACCEPT_QTY.ToString());
                Hrow.CreateCell(8).SetCellValue(((int)result.ALREADY_PACKING_QTY).ToString());
                Hrow.CreateCell(9).SetCellValue(((int)result.REMAIN_PACKING_QTY).ToString());
                Hrow.CreateCell(10).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMAP_ORDER_DT));
                Hrow.CreateCell(11).SetCellValue(result.ORDER_TYPE);
                Hrow.CreateCell(12).SetCellValue(result.PACKING_COMPANY);
                Hrow.CreateCell(13).SetCellValue(result.PART_TYPE);
                Hrow.CreateCell(14).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(15).SetCellValue(result.SUPPLIER_PLANT);

                Hrow.CreateCell(16).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(17).SetCellValue(result.SUB_SUPPLIER_PLANT);
                Hrow.CreateCell(18).SetCellValue(result.SUPPLIER_NAME);

                Hrow.CreateCell(19).SetCellValue(result.VALIDATION_FLAG);
                //add agi 2017-07-25
                Hrow.CreateCell(20).SetCellValue(result.OBR_STATUS_NAME);
                Hrow.CreateCell(21).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_REC_DT));
                Hrow.CreateCell(22).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_SEND_DT));
                Hrow.CreateCell(23).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_DELIVERY_PLAN_DT));
                Hrow.CreateCell(24).SetCellValue(result.BUYERPD_CD);
                Hrow.CreateCell(25).SetCellValue(result.TRANSPORTATION_CD);
                Hrow.CreateCell(26).SetCellValue(result.PAYMENT_TERM_CD);
                Hrow.CreateCell(27).SetCellValue(result.PRICE_TERM_CD);
                Hrow.CreateCell(28).SetCellValue(result.CURRENCY_CD);
                Hrow.CreateCell(29).SetCellValue(result.REMARKS);
                Hrow.CreateCell(30).SetCellValue(result.ICR_ORDER_DATA);
                Hrow.CreateCell(31).SetCellValue(result.ICR_TRANSPORTANTION_CD);
                Hrow.CreateCell(32).SetCellValue(result.ICR_BO_TYPE);
                Hrow.CreateCell(33).SetCellValue(result.ICH_TERM_MASTER);
                Hrow.CreateCell(34).SetCellValue(result.ICH_DISTRIBUTION);
                Hrow.CreateCell(35).SetCellValue(result.ICH_BUYER_PD);
                Hrow.CreateCell(36).SetCellValue(result.ICW_FRANCHCD);
                Hrow.CreateCell(37).SetCellValue(result.ICW_PRICE);

                Hrow.CreateCell(38).SetCellValue(result.PMSP_FLAG);

                Hrow.CreateCell(39).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(40).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(41).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(42).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                //remaks agi 2017-07-25 pindah posisi obr status
                //Hrow.CreateCell(17).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_REC_DT));
                //Hrow.CreateCell(18).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_SEND_DT));
                //Hrow.CreateCell(19).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_DELIVERY_PLAN_DT));
                //Hrow.CreateCell(20).SetCellValue(result.BUYERPD_CD);
                //Hrow.CreateCell(21).SetCellValue(result.TRANSPORTATION_CD);
                //Hrow.CreateCell(22).SetCellValue(result.PAYMENT_TERM_CD);
                //Hrow.CreateCell(23).SetCellValue(result.PRICE_TERM_CD);
                //Hrow.CreateCell(24).SetCellValue(result.CURRENCY_CD);
                //Hrow.CreateCell(25).SetCellValue(result.REMARKS);
                //Hrow.CreateCell(26).SetCellValue(result.ICR_ORDER_DATA);
                //Hrow.CreateCell(27).SetCellValue(result.ICR_TRANSPORTANTION_CD);
                //Hrow.CreateCell(28).SetCellValue(result.ICR_BO_TYPE);
                //Hrow.CreateCell(29).SetCellValue(result.ICH_TERM_MASTER);
                //Hrow.CreateCell(30).SetCellValue(result.ICH_DISTRIBUTION);
                //Hrow.CreateCell(31).SetCellValue(result.ICH_BUYER_PD);
                //Hrow.CreateCell(32).SetCellValue(result.ICW_FRANCHCD);
                //Hrow.CreateCell(33).SetCellValue(result.ICW_PRICE);
                ////REMAKS AGI 2017-07-12 OBR
                ////Hrow.CreateCell(34).SetCellValue(result.CREATED_BY);
                ////Hrow.CreateCell(35).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                ////Hrow.CreateCell(36).SetCellValue(result.CHANGED_BY);
                ////Hrow.CreateCell(37).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));
                //Hrow.CreateCell(34).SetCellValue(result.OBR_STATUS_NAME);
                //Hrow.CreateCell(35).SetCellValue(result.CREATED_BY);
                //Hrow.CreateCell(36).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                //Hrow.CreateCell(37).SetCellValue(result.CHANGED_BY);
                //Hrow.CreateCell(38).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent3;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent; 
                Hrow.GetCell(12).CellStyle = styleContent;
                Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(14).CellStyle = styleContent;
                Hrow.GetCell(15).CellStyle = styleContent;                
                Hrow.GetCell(16).CellStyle = styleContent;
                //add agi 2017-07-25 pindah posisi obr status
                Hrow.GetCell(17).CellStyle = styleContent;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent2;
                Hrow.GetCell(22).CellStyle = styleContent2;
                Hrow.GetCell(22).CellStyle = styleContent2;
                Hrow.GetCell(23).CellStyle = styleContent2;
                Hrow.GetCell(24).CellStyle = styleContent2;
                Hrow.GetCell(25).CellStyle = styleContent2;
                Hrow.GetCell(26).CellStyle = styleContent;
                Hrow.GetCell(27).CellStyle = styleContent;
                Hrow.GetCell(28).CellStyle = styleContent;
                Hrow.GetCell(29).CellStyle = styleContent;
                Hrow.GetCell(30).CellStyle = styleContent;
                Hrow.GetCell(31).CellStyle = styleContent;
                Hrow.GetCell(32).CellStyle = styleContent;
                Hrow.GetCell(33).CellStyle = styleContent;
                Hrow.GetCell(34).CellStyle = styleContent;
                Hrow.GetCell(35).CellStyle = styleContent;
                Hrow.GetCell(36).CellStyle = styleContent;
                Hrow.GetCell(37).CellStyle = styleContent;
                Hrow.GetCell(38).CellStyle = styleContent;

                Hrow.GetCell(39).CellStyle = styleContent;
                Hrow.GetCell(40).CellStyle = styleContent2;
                Hrow.GetCell(41).CellStyle = styleContent;
                Hrow.GetCell(42).CellStyle = styleContent2;

                //remaks agi 2017-07-25 pindah posisi obr status
                //Hrow.GetCell(16).CellStyle = styleContent;
                //Hrow.GetCell(17).CellStyle = styleContent2;
                //Hrow.GetCell(18).CellStyle = styleContent2;
                //Hrow.GetCell(19).CellStyle = styleContent2;
                //Hrow.GetCell(20).CellStyle = styleContent2;
                //Hrow.GetCell(21).CellStyle = styleContent2;
                //Hrow.GetCell(22).CellStyle = styleContent2;
                //Hrow.GetCell(23).CellStyle = styleContent2;
                //Hrow.GetCell(24).CellStyle = styleContent2;
                //Hrow.GetCell(25).CellStyle = styleContent;
                //Hrow.GetCell(26).CellStyle = styleContent;
                //Hrow.GetCell(27).CellStyle = styleContent;
                //Hrow.GetCell(28).CellStyle = styleContent;
                //Hrow.GetCell(29).CellStyle = styleContent;
                //Hrow.GetCell(30).CellStyle = styleContent;
                //Hrow.GetCell(31).CellStyle = styleContent;
                //Hrow.GetCell(32).CellStyle = styleContent;
                //Hrow.GetCell(33).CellStyle = styleContent;
                ////Hrow.GetCell(34).CellStyle = styleContent;
                ////Hrow.GetCell(35).CellStyle = styleContent2;
                ////Hrow.GetCell(36).CellStyle = styleContent;
                ////Hrow.GetCell(37).CellStyle = styleContent2;
                //Hrow.GetCell(34).CellStyle = styleContent;
                //Hrow.GetCell(35).CellStyle = styleContent;
                //Hrow.GetCell(36).CellStyle = styleContent2;
                //Hrow.GetCell(37).CellStyle = styleContent;
                //Hrow.GetCell(38).CellStyle = styleContent2;

                row++;
                rowNum++;
            }
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        //function download order inquiry by paramater
        //remaks agi 2017-07-12
        //public void DownloadOrderInquiry_ByParameter(object sender, EventArgs e, string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_TMAP_ORDER_NO, string p_ITEM_NO,
        //    string p_PART_NO, string p_ORDER_TYPE, string p_VALIDATION_FLAG, string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_TMMIN_DEL_PLAN_DT, string p_TMMIN_DEL_PLAN_DT_TO,
        //    string p_SEND_DT, string p_SEND_DT_TO, string p_SEND_TO_TAM, string p_PACKING_COMPANY, string p_PART_TYPE, string p_SUPPLIER)
        public void DownloadOrderInquiry_ByParameter(object sender, EventArgs e, string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_TMAP_ORDER_NO, string p_ITEM_NO,
            string p_PART_NO, string p_ORDER_TYPE, string p_VALIDATION_FLAG, string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_TMMIN_DEL_PLAN_DT, string p_TMMIN_DEL_PLAN_DT_TO,
            string p_SEND_DT, string p_SEND_DT_TO, string p_SEND_TO_TAM, string p_PACKING_COMPANY, string p_PART_TYPE, string p_SUPPLIER, string p_SUB_SUPPLIER, string p_OBR_STATUS)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Order_Inquiry_Download2.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Order Inquiry");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "OrderInquiry" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(p_TMAP_ORDER_DT + "   to   " + p_TMAP_ORDER_DT_TO);
            sheet.GetRow(9).GetCell(2).SetCellValue(p_TMAP_ORDER_NO);
            sheet.GetRow(10).GetCell(2).SetCellValue(p_ITEM_NO);
            sheet.GetRow(11).GetCell(2).SetCellValue(p_PART_NO);
            sheet.GetRow(12).GetCell(2).SetCellValue(p_ORDER_TYPE);

            sheet.GetRow(8).GetCell(6).SetCellValue(p_VALIDATION_FLAG);
            sheet.GetRow(9).GetCell(6).SetCellValue(p_SEND_TO_TAM);
            sheet.GetRow(10).GetCell(6).SetCellValue(reFormatDate_Download(p_TMMIN_REC_DT) + "   to   " + reFormatDate_Download(p_TMMIN_REC_DT_TO));
            sheet.GetRow(11).GetCell(6).SetCellValue(reFormatDate_Download(p_SEND_DT) + "   to   " + reFormatDate_Download(p_SEND_DT_TO));
            sheet.GetRow(12).GetCell(6).SetCellValue(reFormatDate_Download(p_TMMIN_DEL_PLAN_DT) + "   to   " + reFormatDate_Download(p_TMMIN_DEL_PLAN_DT_TO));

            int row = 16;
            int rowNum = 1;
            IRow Hrow;

            List<mOrderInquiry> model = new List<mOrderInquiry>();

            p_TMAP_ORDER_DT = reFormatDate(p_TMAP_ORDER_DT);
            p_TMAP_ORDER_DT_TO = reFormatDate(p_TMAP_ORDER_DT_TO);
            p_TMMIN_REC_DT = reFormatDate(p_TMMIN_REC_DT);
            p_TMMIN_REC_DT_TO = reFormatDate(p_TMMIN_REC_DT_TO);
            p_TMMIN_DEL_PLAN_DT = reFormatDate(p_TMMIN_DEL_PLAN_DT);
            p_TMMIN_DEL_PLAN_DT_TO = reFormatDate(p_TMMIN_DEL_PLAN_DT_TO);
            p_SEND_DT = reFormatDate(p_SEND_DT);
            p_SEND_DT_TO = reFormatDate(p_SEND_DT_TO);

            //REMKS AGI 2017-07-12 
            //model = OrderInquiry.getListOrderInquiry
            //   (p_TMAP_ORDER_DT, p_TMAP_ORDER_DT_TO, p_TMAP_ORDER_NO, p_ITEM_NO, p_PART_NO, p_ORDER_TYPE, p_VALIDATION_FLAG,
            //   p_TMMIN_REC_DT, p_TMMIN_REC_DT_TO, p_TMMIN_DEL_PLAN_DT, p_TMMIN_DEL_PLAN_DT_TO, p_SEND_DT, p_SEND_DT_TO, p_SEND_TO_TAM, p_PACKING_COMPANY, p_PART_TYPE, p_SUPPLIER);

            model = OrderInquiry.getListOrderInquiry
              (p_TMAP_ORDER_DT, p_TMAP_ORDER_DT_TO, p_TMAP_ORDER_NO, p_ITEM_NO, p_PART_NO, p_ORDER_TYPE, p_VALIDATION_FLAG,
              p_TMMIN_REC_DT, p_TMMIN_REC_DT_TO, p_TMMIN_DEL_PLAN_DT, p_TMMIN_DEL_PLAN_DT_TO, p_SEND_DT, p_SEND_DT_TO, p_SEND_TO_TAM, p_PACKING_COMPANY, p_PART_TYPE, p_SUPPLIER, p_SUB_SUPPLIER, p_OBR_STATUS);


            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);
                
                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.TMAP_ORDER_NO);
                Hrow.CreateCell(3).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(4).SetCellValue(result.PART_NO);
                Hrow.CreateCell(5).SetCellValue(result.ORDER_QTY.ToString());
                Hrow.CreateCell(6).SetCellValue(result.ADJUST_QTY.ToString());
                Hrow.CreateCell(7).SetCellValue(result.ACCEPT_QTY.ToString());
                //remaks agi 2017-09-15 request mrs hesti packing qty dan remain qty di delete kolomnya
                //Hrow.CreateCell(8).SetCellValue(result.ALREADY_PACKING_QTY.ToString());
                //Hrow.CreateCell(9).SetCellValue(result.REMAIN_PACKING_QTY.ToString());
                Hrow.CreateCell(8).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMAP_ORDER_DT));
                Hrow.CreateCell(9).SetCellValue(result.ORDER_TYPE);
                Hrow.CreateCell(10).SetCellValue(result.PACKING_COMPANY);
                Hrow.CreateCell(11).SetCellValue(result.PART_TYPE);
                Hrow.CreateCell(12).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(13).SetCellValue(result.SUPPLIER_PLANT);

                Hrow.CreateCell(14).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(15).SetCellValue(result.SUB_SUPPLIER_PLANT);
                Hrow.CreateCell(16).SetCellValue(result.SUPPLIER_NAME);

                Hrow.CreateCell(17).SetCellValue(result.VALIDATION_FLAG);
                //add agi 2017-07-25 pindah posisi obr status
                Hrow.CreateCell(18).SetCellValue(result.OBR_STATUS_NAME);
                Hrow.CreateCell(19).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_REC_DT));
                Hrow.CreateCell(20).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_SEND_DT));
                Hrow.CreateCell(21).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_DELIVERY_PLAN_DT));
                Hrow.CreateCell(22).SetCellValue(result.BUYERPD_CD);
                Hrow.CreateCell(23).SetCellValue(result.TRANSPORTATION_CD);
                Hrow.CreateCell(24).SetCellValue(result.PAYMENT_TERM_CD);
                Hrow.CreateCell(25).SetCellValue(result.PRICE_TERM_CD);
                Hrow.CreateCell(26).SetCellValue(result.CURRENCY_CD);
                Hrow.CreateCell(27).SetCellValue(result.REMARKS);
                Hrow.CreateCell(28).SetCellValue(result.ICR_ORDER_DATA);
                Hrow.CreateCell(29).SetCellValue(result.ICR_TRANSPORTANTION_CD);
                Hrow.CreateCell(30).SetCellValue(result.ICR_BO_TYPE);
                Hrow.CreateCell(31).SetCellValue(result.ICH_TERM_MASTER);
                Hrow.CreateCell(32).SetCellValue(result.ICH_DISTRIBUTION);
                Hrow.CreateCell(33).SetCellValue(result.ICH_BUYER_PD);
                Hrow.CreateCell(34).SetCellValue(result.ICW_FRANCHCD);
                Hrow.CreateCell(35).SetCellValue(result.ICW_PRICE);

                Hrow.CreateCell(36).SetCellValue(result.PMSP_FLAG);

                Hrow.CreateCell(37).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(38).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(39).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(40).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                //remaks agi 2017-07-25 pindah posisi obr status                
                //Hrow.CreateCell(17).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_REC_DT));
                //Hrow.CreateCell(18).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_SEND_DT));
                //Hrow.CreateCell(19).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMMIN_DELIVERY_PLAN_DT));
                //Hrow.CreateCell(20).SetCellValue(result.BUYERPD_CD);
                //Hrow.CreateCell(21).SetCellValue(result.TRANSPORTATION_CD);
                //Hrow.CreateCell(22).SetCellValue(result.PAYMENT_TERM_CD);
                //Hrow.CreateCell(23).SetCellValue(result.PRICE_TERM_CD);
                //Hrow.CreateCell(24).SetCellValue(result.CURRENCY_CD);
                //Hrow.CreateCell(25).SetCellValue(result.REMARKS);
                //Hrow.CreateCell(26).SetCellValue(result.ICR_ORDER_DATA);
                //Hrow.CreateCell(27).SetCellValue(result.ICR_TRANSPORTANTION_CD);
                //Hrow.CreateCell(28).SetCellValue(result.ICR_BO_TYPE);
                //Hrow.CreateCell(29).SetCellValue(result.ICH_TERM_MASTER);
                //Hrow.CreateCell(30).SetCellValue(result.ICH_DISTRIBUTION);
                //Hrow.CreateCell(31).SetCellValue(result.ICH_BUYER_PD);
                //Hrow.CreateCell(32).SetCellValue(result.ICW_FRANCHCD);
                //Hrow.CreateCell(33).SetCellValue(result.ICW_PRICE);
                ////REMAKS AGI 2017-07-12
                ////Hrow.CreateCell(34).SetCellValue(result.CREATED_BY);
                ////Hrow.CreateCell(35).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                ////Hrow.CreateCell(36).SetCellValue(result.CHANGED_BY);
                ////Hrow.CreateCell(37).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));
                ////ADD AGI 2017-07-12
                //Hrow.CreateCell(34).SetCellValue(result.OBR_STATUS_NAME);
                //Hrow.CreateCell(35).SetCellValue(result.CREATED_BY);
                //Hrow.CreateCell(36).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                //Hrow.CreateCell(37).SetCellValue(result.CHANGED_BY);
                //Hrow.CreateCell(38).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                //remaks agi 2017-09-15 request mrs hesti packing qty dan remain qty di delete kolomnya
                //Hrow.GetCell(8).CellStyle = styleContent3;
                //Hrow.GetCell(9).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent;
                Hrow.GetCell(12).CellStyle = styleContent;
                Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(14).CellStyle = styleContent;
                //add agi 2017-07-25 pindah posisi obr status
                //add agi 2017-07-25 pindah posisi obr status
                Hrow.GetCell(15).CellStyle = styleContent;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent2;
                Hrow.GetCell(21).CellStyle = styleContent2;
                Hrow.GetCell(22).CellStyle = styleContent2;
                Hrow.GetCell(23).CellStyle = styleContent2;
                Hrow.GetCell(24).CellStyle = styleContent;
                Hrow.GetCell(25).CellStyle = styleContent;
                Hrow.GetCell(26).CellStyle = styleContent;
                Hrow.GetCell(27).CellStyle = styleContent;
                Hrow.GetCell(28).CellStyle = styleContent;
                Hrow.GetCell(29).CellStyle = styleContent;
                Hrow.GetCell(30).CellStyle = styleContent;
                Hrow.GetCell(31).CellStyle = styleContent;
                Hrow.GetCell(32).CellStyle = styleContent;

                Hrow.GetCell(33).CellStyle = styleContent;
                Hrow.GetCell(34).CellStyle = styleContent;
                Hrow.GetCell(35).CellStyle = styleContent;
                Hrow.GetCell(36).CellStyle = styleContent;

                Hrow.GetCell(37).CellStyle = styleContent;
                Hrow.GetCell(38).CellStyle = styleContent2;
                Hrow.GetCell(39).CellStyle = styleContent;
                Hrow.GetCell(40).CellStyle = styleContent2;

                //remaks agi 2017-07-25
                //Hrow.GetCell(17).CellStyle = styleContent2;
                //Hrow.GetCell(18).CellStyle = styleContent2;
                //Hrow.GetCell(19).CellStyle = styleContent2;
                //Hrow.GetCell(20).CellStyle = styleContent2;
                //Hrow.GetCell(21).CellStyle = styleContent2;
                //Hrow.GetCell(22).CellStyle = styleContent2;
                //Hrow.GetCell(23).CellStyle = styleContent2;
                //Hrow.GetCell(24).CellStyle = styleContent2;
                //Hrow.GetCell(25).CellStyle = styleContent;
                //Hrow.GetCell(26).CellStyle = styleContent;
                //Hrow.GetCell(27).CellStyle = styleContent;
                //Hrow.GetCell(28).CellStyle = styleContent;
                //Hrow.GetCell(29).CellStyle = styleContent;
                //Hrow.GetCell(30).CellStyle = styleContent;
                //Hrow.GetCell(31).CellStyle = styleContent;
                //Hrow.GetCell(32).CellStyle = styleContent;
                //Hrow.GetCell(33).CellStyle = styleContent;
                //Hrow.GetCell(34).CellStyle = styleContent;
                //Hrow.GetCell(35).CellStyle = styleContent;
                //Hrow.GetCell(36).CellStyle = styleContent2;
                //Hrow.GetCell(37).CellStyle = styleContent;
                //Hrow.GetCell(38).CellStyle = styleContent2;


                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        //Function Update for Adjust / Cancel
        public ActionResult UpdateOrderInquiry(string p_TMAP_ORDER_NO, string p_ITEM_NO, string p_PART_NO, string p_ORDER_QTY, string p_ADJUST_QTY, string p_ACCEPT_QTY, string p_VALIDATION_FLAG, string p_REMARKS, string p_CHANGED_BY)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            p_CHANGED_BY = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = OrderInquiry.UpdateData(p_TMAP_ORDER_NO, p_ITEM_NO, p_PART_NO, p_ORDER_QTY, p_ADJUST_QTY, p_ACCEPT_QTY, p_VALIDATION_FLAG, p_REMARKS, p_CHANGED_BY);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        //REMARK BY RIA @20160713
        ////Send Parameter to [TB_R_BACKGROUND_TASK_REGISTRY] for Sending POSS to TAM  Batch
        //public ActionResult SendParameter()
        //{
        //    long pid = 0;
        //    string batchName = "OrderSendingBatch";
        //    string moduleID = "1";
        //    string functionID = "10005";
        //    string subFuncName = "Send Param";
        //    string userBatch = getpE_UserId.Username;
        //    string location = "OrderInquiry.SendParameter";

        //    //starting logmessageError
        //    pid = log.createLog(msgError.getMSPX00001INB(batchName), userBatch, location + "." + subFuncName, 0, moduleID, functionID);

        //    string pParameter = "{&quot;ProcessID&quot;:&quot;" + pid + "&quot;}";

        //    string pID = pid.ToString();
        //    string pName = "Sending Order";
        //    string pDescription = "Sending Order Data to TAM";
        //    string pSubmitter = getpE_UserId.Username;
        //    string pFunctionName = "Sending Order Batch";
        //    string pType = "0";
        //    string pStatus = "0";
        //    //for trial
        //    string pCommand = @"D:\SPIN\Background_Task\Tasks\SendToTAM\Debug\SendOrderToTAM.exe";
        //    //for trial
        //    //string pCommand = @"D:\SPIN\Background_Task\Tasks\SendToTA\SendOrderToTAM.exe";
        //    //string pCommand = @"D:\Background_Task\Tasks\SendToTAM\SendOrderToTAM.exe";
        //    string pStartDate = "0";
        //    string pEndDate = "0";
        //    string pPeriodicType = "4";
        //    string pInterval = null;
        //    string pExecutionDays = "";
        //    string pExecutionMonths = "";
        //    string pTime = "6840000";

        //    string[] r = null;
        //    int modelResult = OrderInquiry.getValidate();
        //    int modelResult_Warning = OrderInquiry.getValidate_Warning();
        //    string msg = "";

        //    if (modelResult <= 0)
        //    {
        //        return Json(new { success = "true", messages = msgError.getMSPX00008INB("send").Split('|')[2] }, JsonRequestBehavior.AllowGet);                
        //    }
        //    else
        //    {
        //        if (modelResult_Warning > 0)
        //        {
        //            msg = msgError.getMSPX00010INB("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
        //        }
        //        else
        //        {
        //            msg = msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
        //        }
        //        List<mOrderInquiry> model = new List<mOrderInquiry>();
        //        string resultMessage = OrderInquiry.InsertParam(pID, pName, pDescription, pSubmitter, pFunctionName, pParameter, pType, pStatus, pCommand, pStartDate, pEndDate, pPeriodicType, pInterval, pExecutionDays, pExecutionMonths, pTime);

        //        //update end log
        //        //log.createLog(msgError.getMSPX00002INB(batchName), userBatch, location + "." + subFuncName, pid, moduleID, functionID);
        //        r = resultMessage.Split('|');
        //        if (r[0] == "Error ")
        //        {
        //            return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            return Json(new { success = "true", messages = msg + '|' + pID }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    return null;
        //}

        //ADD BY RIA @20160713
        //USE FOR SEND TO GTOPAS LIVE JUNI 2016
        //remaks agi 2017-05-29 request MRS Hesty
        //public ActionResult SendParameterGTOPAS()

        //REMAKS AGI 2017-07-21
        //public ActionResult SendParameterGTOPAS(string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_PACKING_COMPANY)    
        public ActionResult SendParameterGTOPAS(string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_PACKING_COMPANY,string p_ORDER_TYPE)      
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);
            
            string[] r = null;
            int modelResult = OrderInquiry.getValidate();
            int modelResult_Warning = OrderInquiry.getValidate_Warning();
            string msg = "";
            string ProcessID = "";

            List<mOrderInquiry> model = new List<mOrderInquiry>();
            //string resultMessage = OrderInquiry.InsertParamJobGTOPAS(getpE_UserId.Username, reFormatDate(p_TMMIN_REC_DT), reFormatDate(p_TMMIN_REC_DT_TO),p_PACKING_COMPANY);
            string resultMessage = OrderInquiry.InsertParamJobGTOPAS(getpE_UserId.Username, reFormatDate(p_TMMIN_REC_DT), reFormatDate(p_TMMIN_REC_DT_TO), p_PACKING_COMPANY,p_ORDER_TYPE);

            r = resultMessage.Split('|');

            if (r[0].ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ProcessID = r[2];
                string ProcessID2=string.Empty;
                //string messages = r[1].Replace(ProcessID, "<a href='#' onclick='MessageShow(" + ProcessID + "); return false;'>" + ProcessID + "</a>");
                string messages = r[1].Replace(ProcessID, "<a href='#' onclick='SetPIDShowMessage(" + ProcessID + "); return false;'>" + ProcessID + "</a>");
                if (string.IsNullOrEmpty(p_PACKING_COMPANY))
                {
                    ProcessID2=r[5];
                    //string messages2=", "+ r[4].Replace(ProcessID2, "<a href='#' onclick='MessageShow(" + ProcessID2 + "); return false;'>" + ProcessID2 + "</a>");
                    string messages2 = ", " + r[4].Replace(ProcessID2, "<a href='#' onclick='SetPIDShowMessage(" + ProcessID2 + "); return false;'>" + ProcessID2 + "</a>");
                    messages = messages + messages2;
                }

                return Json(new { success = "true", messages = messages + '|' + ProcessID }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SendParameter()
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            int modelResult = OrderInquiry.getValidate();
            int modelResult_Warning = OrderInquiry.getValidate_Warning();
            string msg = "";
            string ProcessID = "";

            List<mOrderInquiry> model = new List<mOrderInquiry>();
            string resultMessage = OrderInquiry.InsertParamJob(getpE_UserId.Username);

            r = resultMessage.Split('|');

            if (r[0].ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ProcessID = r[2];
                return Json(new { success = "true", messages = r[1].Replace(ProcessID, "<a href='#' onclick='MessageShow(" + ProcessID + "); return false;'>" + ProcessID + "</a>") + '|' + ProcessID }, JsonRequestBehavior.AllowGet);
            }
        }

        //Untuk pesan notify
        public void SendNotify()
        {
            List<mOrderInquiry> model = new List<mOrderInquiry>();
            OrderInquiry.SendNotify();
        }

        public ActionResult SendSynchronizeData()
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            int modelResult = OrderInquiry.getValidate();
            int modelResult_Warning = OrderInquiry.getValidate_Warning();
            string msg = "";
            string ProcessID = "";

            int modelResult_Reprocess = OrderInquiry.getValidate_Reprocess();
            if (modelResult_Reprocess <= 0)
            {
                return Json(new { success = "true", messages = msgError.getMSPX00008INB("reprocess").Split('|')[2] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<mOrderInquiry> model = new List<mOrderInquiry>();
                string resultMessage = OrderInquiry.Reprocess(getpE_UserId.Username);

                r = resultMessage.Split('|');

                if (r[0].ToLower().Contains("error"))
                {
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ProcessID = r[2];
                    return Json(new { success = "true", messages = r[1].Replace(ProcessID, "<a href='#' onclick='MessageShow(" + ProcessID + "); return false;'>" + ProcessID + "</a>") + '|' + ProcessID }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        public ActionResult SendOrderAcknowledgement()
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            long pid = 0;
            string processName = "Ordering Acknowledgement";
            string moduleID = "1";
            string functionID = "10015";
            string subFuncName = "SendOrderAcknowledgement";
            string user = getpE_UserId.Username;
            string location = "OrderInquiry.SendOrderAcknowledgement";

            //starting logmessageError
            pid = log.createLog(msgError.getMSPX00001INB(processName), user, location + "." + subFuncName, 0, moduleID, functionID);

            string pParameter = "{&quot;PROCESS_ID&quot;:&quot;" + pid + "&quot;}";

            string pID = pid.ToString();

            string[] r = null;
            int modelResult_OrderACK = OrderInquiry.getValidate_OrderAck();
            string msg = "";

            if (modelResult_OrderACK <= 0)
            {
                msg = msgError.getMSPX00010INB("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
            }
            else
            {
                msg = msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];

                CreateOrderACK();

                //update end log
                log.createLog(msgError.getMSPX00002INB(processName), user, location + "." + subFuncName, pid, moduleID, functionID);

                //List<mOrderInquiry> model = new List<mOrderInquiry>();

                string resultMessage = msg;

                r = resultMessage.Split('|');

                if (r[0] == "Error ")
                {
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = "true", messages = msg + '|' + pID }, JsonRequestBehavior.AllowGet);
                }
            }
            return null;
        }


        //function download order inquiry by paramater 

        #region create File order ACK
        public JsonResult CreateOrderACK()
        {
            string msg = "";
            try
            {
                string filename = "";
                string filesTmp = HttpContext.Request.MapPath("~/Template/Order_Acknowledgement.xls");
                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

                //setting style
                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.Top;
                styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                styleContent2.Alignment = HorizontalAlignment.Center;

                ICellStyle styleContent3 = workbook.CreateCellStyle();
                styleContent3.VerticalAlignment = VerticalAlignment.Top;
                styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                styleContent3.Alignment = HorizontalAlignment.Right;

                ISheet sheet = workbook.GetSheet("Sheet1");
                string date = DateTime.Now.ToString("ddMMyyyyHHMMss");
                filename = "OrderAcknowledgement_" + date + ".xls";

                string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

                int row = 10;
                int rowNum = 1;
                IRow Hrow;
                int sum_Rcv_Line = 0, sum_Rcv_Qty = 0, sum_Accept_Line = 0, sum_Accept_Qty = 0, sum_Hold_Line = 0, sum_Hold_QTY = 0, sum_Reject_Line = 0, sum_Reject_Qty = 0, sum_Cancel_Line = 0, sum_Cancel_Qty = 0;

                List<mOrderInquiry> model = new List<mOrderInquiry>();
                mOrderInquiry model_header = new mOrderInquiry();

                model = OrderInquiry.getListOrderACK();
                model_header = OrderInquiry.getListHeaderOrderACK();

                sheet.GetRow(3).CreateCell(1).SetCellValue(": " + model_header.HEADER_TO);
                sheet.GetRow(4).CreateCell(1).SetCellValue(": " + model_header.HEADER_ATTN);
                sheet.GetRow(5).CreateCell(1).SetCellValue(": " + model_header.HEADER_CC);


                foreach (var result in model)
                {
                    Hrow = sheet.CreateRow(row);

                    Hrow.CreateCell(0).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.TMAP_ORDER_DT));
                    Hrow.CreateCell(1).SetCellValue(result.TMAP_ORDER_NO);
                    Hrow.CreateCell(2).SetCellValue(result.ORDER_TYPE);
                    Hrow.CreateCell(3).SetCellValue(result.RCV_LINE);
                    Hrow.CreateCell(4).SetCellValue(result.RCV_QTY);
                    Hrow.CreateCell(5).SetCellValue(result.ACCEPT_LINE);
                    Hrow.CreateCell(6).SetCellValue(result.ACCEPT_QTY);
                    Hrow.CreateCell(7).SetCellValue(result.HOLD_LINE);
                    Hrow.CreateCell(8).SetCellValue(result.HOLD_QTY);
                    Hrow.CreateCell(9).SetCellValue(result.REJECT_LINE);
                    Hrow.CreateCell(10).SetCellValue(result.REJECT_QTY);
                    Hrow.CreateCell(11).SetCellValue(result.CANCEL_LINE);
                    Hrow.CreateCell(12).SetCellValue(result.CANCEL_QTY);
                    Hrow.CreateCell(13).SetCellValue("");

                    Hrow.GetCell(0).CellStyle = styleContent2;
                    Hrow.GetCell(1).CellStyle = styleContent2;
                    Hrow.GetCell(2).CellStyle = styleContent3;
                    Hrow.GetCell(3).CellStyle = styleContent3;
                    Hrow.GetCell(4).CellStyle = styleContent3;
                    Hrow.GetCell(5).CellStyle = styleContent3;
                    Hrow.GetCell(6).CellStyle = styleContent3;
                    Hrow.GetCell(7).CellStyle = styleContent3;
                    Hrow.GetCell(8).CellStyle = styleContent3;
                    Hrow.GetCell(9).CellStyle = styleContent3;
                    Hrow.GetCell(10).CellStyle = styleContent3;
                    Hrow.GetCell(11).CellStyle = styleContent3;
                    Hrow.GetCell(12).CellStyle = styleContent3;
                    Hrow.GetCell(13).CellStyle = styleContent2;

                    row++;
                    rowNum++;
                    sum_Rcv_Line = sum_Rcv_Line + result.RCV_LINE;
                    sum_Rcv_Qty = sum_Rcv_Qty + result.RCV_QTY;
                    sum_Accept_Line = sum_Accept_Line + result.ACCEPT_LINE;
                    sum_Accept_Qty = sum_Accept_Qty + result.ACCEPT_QTY;
                    sum_Hold_Line = sum_Hold_Line + result.HOLD_LINE;
                    sum_Hold_QTY = sum_Hold_QTY + result.HOLD_QTY;
                    sum_Reject_Line = sum_Reject_Line + result.REJECT_LINE;
                    sum_Reject_Qty = sum_Reject_Qty + result.REJECT_QTY;
                    sum_Cancel_Line = sum_Cancel_Line + result.CANCEL_LINE;
                    sum_Cancel_Qty = sum_Cancel_Qty + result.CANCEL_QTY;
                }

                Hrow = sheet.CreateRow(row);
                Hrow.CreateCell(2).SetCellValue("TOTAL");
                Hrow.CreateCell(3).SetCellValue(sum_Rcv_Line);
                Hrow.CreateCell(4).SetCellValue(sum_Rcv_Qty);
                Hrow.CreateCell(5).SetCellValue(sum_Accept_Line);
                Hrow.CreateCell(6).SetCellValue(sum_Accept_Qty);
                Hrow.CreateCell(7).SetCellValue(sum_Hold_Line);
                Hrow.CreateCell(8).SetCellValue(sum_Hold_QTY);
                Hrow.CreateCell(9).SetCellValue(sum_Reject_Line);
                Hrow.CreateCell(10).SetCellValue(sum_Reject_Qty);
                Hrow.CreateCell(11).SetCellValue(sum_Cancel_Line);
                Hrow.CreateCell(12).SetCellValue(sum_Cancel_Qty);
                Hrow.CreateCell(13).SetCellValue("");

                Hrow.GetCell(2).CellStyle = styleContent3;
                Hrow.GetCell(3).CellStyle = styleContent3;
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent3;
                Hrow.GetCell(10).CellStyle = styleContent3;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent3;
                Hrow.GetCell(13).CellStyle = styleContent2;

                msg = "write File";
                //Save File
                using (FileStream file = new FileStream(@"\\10.33.4.10\spex\data\Order Acknowledgement\" + filename, FileMode.Create, FileAccess.Write))
                {
                    workbook.Write(file);
                    file.Close();
                }


                // Send Email
                OrderInquiry.sendOrderACK(filename);

                //DeleteFile
                if (System.IO.File.Exists(@"\\10.33.4.10\spex\data\Order Acknowledgement\" + filename))
                {
                    System.IO.File.Delete(@"\\10.33.4.10\spex\data\Order Acknowledgement\" + filename);
                    ViewBag.deleteSuccess = "true";
                }

                msg = "Success...";

                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                msg = e.Message;
                return Json(msg, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion
    
        //ADD AGI 2016-12-08 REQUEST MR AMIN pop up part no
        public ActionResult _PopUpDetailPartNo()
        {
            mOrderInquiry data = System.Web.Helpers.Json.Decode<mOrderInquiry>(Request.Params["OrderInquiryByPart"]);
            List<OrderInquryDetailPartNo> DetailParts= OrderInquiry.OrderInquryGetDetailPartNo(data);
            return PartialView("_PartialGridPartDetail", DetailParts);
            //return null;
        }

        public void DownloadPart(string PART_NO, string ITEM_NO, string TMAP_ORDER_NO, string ORDER_QTY, string PACKING_COMPANY, string PART_TYPE)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/OrderInquryDownloadPartNoDetail.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //ICellStyle styleContent = workbook.CreateCellStyle();
            //styleContent.VerticalAlignment = VerticalAlignment.Top;
            //styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            //styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            //styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            //ICellStyle styleContent3 = workbook.CreateCellStyle();
            //styleContent3.VerticalAlignment = VerticalAlignment.Top;
            //styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            //styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            //styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            //styleContent3.Alignment = HorizontalAlignment.Left;

            //insert data in header of xls
            ISheet sheet = workbook.GetSheet("OredrInquiry");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "DownloadOrderInquryDetailPartNo_" + date + ".xlsx";
            string dateNow = DateTime.Now.ToString("dd-MM-yyyy");

            //sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            //sheet.GetRow(7).GetCell(3).SetCellValue(getpE_UserId.Username);
            //sheet.GetRow(9).GetCell(3).SetCellValue(D_DistributorCode);
            //sheet.GetRow(10).GetCell(3).SetCellValue(D_DistributorName);
            //sheet.GetRow(11).GetCell(3).SetCellValue(D_DistributorCountry);

            int row = 1;
            int rowNum = 1;
            IRow Hrow;

            //insert data show in screen into document
            //List<mDistributionMaster> model = new List<mDistributionMaster>();
            //model = DistributionMaster.DistributionMasterDownload(D_DistributorCode, D_DistributorName, D_DistributorCountry);
            mOrderInquiry data = new mOrderInquiry();
            data.PART_NO = PART_NO;
            data.PART_TYPE = PART_TYPE;
            data.ITEM_NO = ITEM_NO;
            data.TMAP_ORDER_NO = TMAP_ORDER_NO;
            data.PACKING_COMPANY = PACKING_COMPANY;
            if(ORDER_QTY!=null)
            data.ORDER_QTY = Convert.ToInt32(ORDER_QTY);
            //System.Web.Helpers.Json.Decode<mOrderInquiry>(Request.Params["OrderInquiryByPart"]);
            List<OrderInquryDetailPartNo> DetailParts = OrderInquiry.OrderInquryGetDetailPartNo(data);
            foreach (var result in DetailParts)
            {
                Hrow = sheet.CreateRow(row);
                //Hrow.CreateCell(0).SetCellValue(rowNum);
                Hrow.CreateCell(0).SetCellValue(result.TMAP_ORDER_NO);
                Hrow.CreateCell(1).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(2).SetCellValue(result.PART_NO);
                if (result.ORDER_QTY!=null)
                    Hrow.CreateCell(3).SetCellValue(result.ORDER_QTY.ToString());
                else
                    Hrow.CreateCell(3).SetCellType(CellType.Blank);
                if (result.PACKING_QTY != null)
                    Hrow.CreateCell(4).SetCellValue(result.PACKING_QTY.ToString());
                else
                    Hrow.CreateCell(4).SetCellType(CellType.Blank);
                Hrow.CreateCell(5).SetCellValue(result.CASE_NO);
                if (result.PACKING_DT != null)
                    Hrow.CreateCell(6).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.PACKING_DT));
                else
                    Hrow.CreateCell(6).SetCellType(CellType.Blank);
                Hrow.CreateCell(7).SetCellValue(result.TMMIN_ORDER_NO);
                Hrow.CreateCell(8).SetCellValue(result.CONTAINER_NO);
                if (result.VANNING_DT != null)
                    Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.VANNING_DT));
                else
                    Hrow.CreateCell(9).SetCellType(CellType.Blank);
                Hrow.CreateCell(10).SetCellValue(result.INVOICE_NO);
                if (result.SHIPMENT_QTY != null)
                    Hrow.CreateCell(11).SetCellValue(result.SHIPMENT_QTY.ToString());
                else
                    Hrow.CreateCell(11).SetCellType(CellType.Blank);
                if (result.ETD != null)
                    Hrow.CreateCell(12).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.ETD));
                else
                    Hrow.CreateCell(12).SetCellType(CellType.Blank);
                Hrow.GetCell(0).CellStyle = styleContent2;
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                row++;
                rowNum++;
            }

            //MemoryStream ms = new MemoryStream();
            //workbook.Write(ms);
            //ftmp.Close();
            //Response.BinaryWrite(ms.ToArray());
            //Response.ContentType = "application/vnd.ms-excel";
            //Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }


        //add method agi 2017-07-12
        public void ComboOBR()
        {
            List<mOrderInquiry> model = new List<mOrderInquiry>();
            model = OrderInquiry.Get_List_OBR_Status();
            ViewData["LIST_OBR_STATUS"] = model;
        }
    }
}