﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class OrderTypeConversionMasterController : PageController
    {
        mOrderTypeConversionMaster OrderTypeConversionMaster = new mOrderTypeConversionMaster();
        MessagesString alert = new MessagesString();

        public OrderTypeConversionMasterController()
        {
            Settings.Title = "Order Type Conversion Master";
        }


        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            ComboBoxBuyer();
            ComboBoxSupply();
            ComboBoxTrans();
            ViewBag.MSPX00001ERR = alert.getMSPX00001ERR("Order Type Conversion Master");
            getpE_UserId = (User)ViewData["User"];
        }
        

        public ActionResult GridViewOrderType(string pBuyerType, string pBuyerTrans, string pOrderTypeDescription, string pSupplyType )
        {
            List<mOrderTypeConversionMaster> model = new List<mOrderTypeConversionMaster>();

            {
                model = OrderTypeConversionMaster.getListOrderTypeMaster
                (pBuyerType, pBuyerTrans, pOrderTypeDescription, pSupplyType);
            }
            return PartialView("GridOrderTypeConv", model);
        }


        public void ComboBoxBuyer()
        {
            List<mOrderTypeConversionMaster> model = new List<mOrderTypeConversionMaster>();

            model = OrderTypeConversionMaster.getListCombobox();
            ViewData["BUYER_ORDER_TYPE"] = model;
        }

        public void ComboBoxTrans()
        {
            List<mOrderTypeConversionMaster> model = new List<mOrderTypeConversionMaster>();

            model = OrderTypeConversionMaster.getListCbBuyerTrans();
            ViewData["BUYER_TRANSPORTATION_CD"] = model;
        }

        public void ComboBoxSupply()
        {
            List<mOrderTypeConversionMaster> model = new List<mOrderTypeConversionMaster>();

            model = OrderTypeConversionMaster.getListCbSupply();
            ViewData["SUPPLY_TYPE"] = model;
        }

        public void _DownloadOrderType(object sender, EventArgs e, string D_BuyerOrderType, string D_BuyerTranscd, string D_OrderTypeDescription, string D_SupplyType)
        {

            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Order_Type_Conversion_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;

            ISheet sheet = workbook.GetSheet("OrderConversionMaster");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "DownloadOrderTypeConversionMaster_" + date + ".xls";
            //string judul = "TMMIN";
            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
           
            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(3).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(9).GetCell(4).SetCellValue(D_BuyerOrderType);
            sheet.GetRow(10).GetCell(4).SetCellValue(D_BuyerTranscd);
            sheet.GetRow(11).GetCell(4).SetCellValue(D_OrderTypeDescription);
            sheet.GetRow(12).GetCell(4).SetCellValue(D_SupplyType);
            

            

            int row = 18;
            int rowNum = 1;
            IRow Hrow;

            List<mOrderTypeConversionMaster> model = new List<mOrderTypeConversionMaster>();
            model = OrderTypeConversionMaster.OrderTypeConvDownload( D_BuyerOrderType, D_BuyerTranscd, D_OrderTypeDescription, D_SupplyType);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.BUYER_ORDER_TYPE);
                Hrow.CreateCell(3).SetCellValue(result.BUYER_TRANSPORTATION_CD);
                //Hrow.CreateCell(4).SetCellValue("");
                Hrow.CreateCell(4).SetCellValue(result.ORDER_TYPE_DESCRIPTION);
                Hrow.CreateCell(5).SetCellValue(result.SUPPLY_TYPE);

                //sheet.AddMergedRegion(new CellRangeAddress(row, row, 3, 4));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                //Hrow.GetCell(3).IsMergedCell=
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent2;
               // Hrow.GetCell(6).CellStyle = styleContent;
                

                row++;
                rowNum++;
            }
            //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
            //    sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
    }



}









