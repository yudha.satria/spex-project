﻿using System;
using System.Collections.Generic;
using System;
using System.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class PORT_DISCHARGEController : PageController
    {
         //mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();

        public PORT_DISCHARGEController()
        {
            Settings.Title = "PORT DISCHARGE";
        }
        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }
        protected override void Startup()
        {
            //CallbackComboPriceTerm();

            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("PORT DISCHARGE MASTER");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        public ActionResult PORT_DISCHARGECallBack(string PORT_DISCHARGE_CD)
        {
            if (PORT_DISCHARGE_CD != null)
            {
                if (PORT_DISCHARGE_CD.Contains("\""))
                {
                    PORT_DISCHARGE_CD = "";                    
                }

            }
            else
            {
                PORT_DISCHARGE_CD = "XXX/XXXX";
            }
            PORT_DISCHARGE PD = new PORT_DISCHARGE();
            PD.PORT_DISCHARGE_CD = PORT_DISCHARGE_CD;
            //PL.PORT_LOADING_NAME = PORT_LOADING_NAME;
            List<PORT_DISCHARGE> model = new List<PORT_DISCHARGE>();
            model = PD.getPORT_DISCHARGE(PD);
            return PartialView("_PORT_DISCHARGEGrid", model);
        }
        public ActionResult Update(string PORT_DISCHARGE_CD, string PORT_DISCHARGE_NAME)
        {
            //string p_CHANGED_BY = getpE_UserId.Username;
            //pCHANGED_BY = "SYSTEM";
            string[] r = null;
            PORT_DISCHARGE PG = new PORT_DISCHARGE();
            PG.PORT_DISCHARGE_CD = PORT_DISCHARGE_CD;
            PG.PORT_DISCHARGE_NAME = PORT_DISCHARGE_NAME;
            PG.CHANGED_BY = getpE_UserId.Username;
            string resultMessage = PG.UpdateData(PG);
            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public ActionResult Delete(string PORT_DISCHARGE_CD)
        {
            PORT_DISCHARGE PG = new PORT_DISCHARGE();
            string resultMessage = string.Empty;
            string error = string.Empty;
            try
            {


                resultMessage = PG.DeleteData(PORT_DISCHARGE_CD);

            }
            catch (Exception ex)
            {
                resultMessage = "Error";
                error = ex.Message;
            }

            if (resultMessage == "Error")
            {
                return Json(new { success = "false", messages = error }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = resultMessage }, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        public ActionResult AddNew(string PORT_DISCHARGE_CD, string PORT_DISCHARGE_NAME)
        {
            //p_CREATED_BY = getpE_UserId.Username;
            //p_CREATED_BY = "SYSTEM";
            string[] r = null;
            PORT_DISCHARGE PG = new PORT_DISCHARGE();
            PG.PORT_DISCHARGE_CD = PORT_DISCHARGE_CD;
            PG.PORT_DISCHARGE_NAME = PORT_DISCHARGE_NAME;
            PG.CREATED_BY = getpE_UserId.Username;
            string resultMessage = PG.SaveData(PG);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

    }
}