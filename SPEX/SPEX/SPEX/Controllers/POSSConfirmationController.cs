﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Linq;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class POSSConfirmationController : PageController
    {

        mPOSSConfirmation POSSConfirmation = new mPOSSConfirmation();
        MessagesString messageError = new MessagesString();
        Log log = new Log();

        public POSSConfirmationController()
        {
            Settings.Title = "POSS Confirmation Screen";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public string conversiDate(string dt)
        {
            string result = "";
            string[] adt = null;

            adt = dt.Split('.');
            dt = adt[1] + "." + adt[0] + "." + adt[2];

            if (dt != "01.01.0100") result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));

            return result;
        }

        protected override void Startup()
        {
            ComboBOCode();
            ComboRACode();
            ComboPortionCode();
            ComboBOInstructCode();
            ComboPackingCompany();
            ComboTGATGPFlag();
            ComboValidationStatus();
            ViewBag.MSPX00003ERR = messageError.getMSPX00003ERR("POSS Date From and POSS Date To");
            ViewBag.MSPX00001ERR = messageError.getMSPX00001ERR("POSS Confirmation");
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            getpE_UserId = (User)ViewData["User"];
            //add agi 2017-10-24
            string range = new mSystemMaster().GetSystemValueList("poss confirmation inquiry", "range day", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            ViewBag.range = Convert.ToInt32(range);
            ViewBag.MSPXP0001ERR = messageError.getMsgText("MSPXP0001ERR").Replace("{0}", range);
        }


        public ActionResult GridViewPOSSConfirmation(string pOrderNo, string pPartNo, string pItemNo,
            string pRACode, string pPOSSDateFrom, string pPOSSDateTo, string pBOCode, string pPortionCode,
            string pBOInstrCode, string pSendFlag, string pSendDateFrom, string pSendDateTo, string pValidationStatus,
            string pPackingCompany, string pTGATGPFlag)
        {
            List<mPOSSConfirmation> model = new List<mPOSSConfirmation>();
            pPOSSDateFrom = conversiDate(pPOSSDateFrom);
            pPOSSDateTo = conversiDate(pPOSSDateTo);
            pSendDateFrom = conversiDate(pSendDateFrom);
            pSendDateTo = conversiDate(pSendDateTo);
            {
                model = POSSConfirmation.getListPOSSConfirmation(pPOSSDateFrom,
                        pPOSSDateTo, pOrderNo, pPartNo, pItemNo, pRACode,
                        pBOCode, pPortionCode, pBOInstrCode, pSendFlag,
                        pSendDateFrom, pSendDateTo, pValidationStatus, pPackingCompany, pTGATGPFlag);
            }
            return PartialView("GridPOSSConfirmation", model);
        }

        public void ComboBOInstructCode()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();

            model = (new mSystemMaster()).GetSystemValueList("SPX_POSS_DEF_VALUE", "BO_INSTRUCT_CD_", "2");
            ViewData["BO_INSTRUCT_CODE"] = model;

        }

        public void ComboTGATGPFlag()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();

            model = Common.GetPartTypeList();
            ViewData["TGA_TGP_FLAG"] = model;
        }

        public void ComboPackingCompany()
        {
            List<string> model = new List<string>();

            model = (new Common()).GetPackingCompany();
            ViewData["PACKING_COMPANY"] = model;
        }

        public void ComboValidationStatus()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();

            model = (new Common()).GetValidationStatusList();
            ViewData["VALIDATION_STATUS"] = model;
        }

        public void ComboBOCode()
        {
            List<mPOSSConfirmation> model = new List<mPOSSConfirmation>();

            model = POSSConfirmation.getListBOCode();
            ViewData["BO_CODE"] = model;

        }

        public void ComboPortionCode()
        {
            List<mPOSSConfirmation> model = new List<mPOSSConfirmation>();

            model = POSSConfirmation.getListPortionCode();
            ViewData["PORTION_CODE"] = model;

        }

        public void ComboRACode()
        {
            List<mPOSSConfirmation> model = new List<mPOSSConfirmation>();

            model = POSSConfirmation.getListRACode();
            ViewData["RA_CODE"] = model;

        }

        public void ComboPopUpRACode()
        {
            List<mPOSSConfirmation> model = new List<mPOSSConfirmation>();

            model = POSSConfirmation.getListPopUpRACode();
            ViewData["RA_CODE"] = model;

        }

        protected DateTime? reformatDate(string dt)
        {
            DateTime? ndt = null;
            string[] adt;
            if (dt != "")
            {
                adt = dt.Split('.');
                ndt = DateTime.Parse(adt[1] + '/' + adt[0] + '/' + adt[2]);
            }
            return ndt;
        }

        protected string reformatDate_edit(string dt)
        {
            string ndt = null;
            string[] adt;
            char nol = '0';
            if (dt != "")
            {
                adt = dt.Split('/');
                adt[0] = adt[0].PadLeft(2, nol);
                adt[1] = adt[1].PadLeft(2, nol);

                ndt = adt[2] + '-' + adt[0] + '-' + adt[1];
            }
            return ndt;
        }

        //Pop Up
        public ActionResult SendConfirmation(string POSSDateFrom, string POSSDateTo, string PortionCode)
        {
            //ViewBag.Param1 = p_PARAM1;
            ComboPopUpRACode();
            ComboPortionCode();
            ViewBag.POSSDateFrom = reformatDate(POSSDateFrom);
            ViewBag.POSSDateTo = reformatDate(POSSDateTo);
            ViewBag.PortionCode = PortionCode;

            return View();
        }

        //Download POSS Data
        public void DownloadPOSS_ByParameter(object sender, EventArgs e, string pOrderNo, string pPartNo, string pItemNo,
            string pRACode, string pPOSSDateFrom, string pPOSSDateTo, string pBOCode, string pPortionCode,
            string pBOInstrCode, string pSendFlag, string pSendDateFrom, string pSendDateTo, string pValidationStatus,
            string pPackingCompany, string pTGATGPFlag)
        {
            pPOSSDateFrom = conversiDate(pPOSSDateFrom);
            pPOSSDateTo = conversiDate(pPOSSDateTo);
            pSendDateFrom = conversiDate(pSendDateFrom);
            pSendDateTo = conversiDate(pSendDateTo);

            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/POSS_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;

            ISheet sheet = workbook.GetSheet("POSS");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "POSSDownload_" + date + ".xls";
            string dateNow = DateTime.Now.ToString("dd-MM-yyyy");

            sheet.GetRow(6).GetCell(3).SetCellValue(date);
            sheet.GetRow(9).GetCell(3).SetCellValue(pOrderNo);
            sheet.GetRow(10).GetCell(3).SetCellValue(pPartNo);
            sheet.GetRow(11).GetCell(3).SetCellValue(pItemNo);
            sheet.GetRow(12).GetCell(3).SetCellValue(pRACode);
            sheet.GetRow(13).GetCell(3).SetCellValue(pPOSSDateFrom);
            sheet.GetRow(14).GetCell(3).SetCellValue(pPOSSDateTo);
            sheet.GetRow(15).GetCell(3).SetCellValue(pBOCode);
            sheet.GetRow(16).GetCell(3).SetCellValue(pPortionCode);
            sheet.GetRow(17).GetCell(3).SetCellValue(pBOInstrCode);
            sheet.GetRow(18).GetCell(3).SetCellValue(pSendFlag);
            sheet.GetRow(19).GetCell(3).SetCellValue(pSendDateFrom);
            sheet.GetRow(20).GetCell(3).SetCellValue(pSendDateTo);
            sheet.GetRow(21).GetCell(3).SetCellValue(pValidationStatus);
            sheet.GetRow(22).GetCell(3).SetCellValue(pPackingCompany);
            sheet.GetRow(23).GetCell(3).SetCellValue(pTGATGPFlag);
            int row = 27;
            int rowNum = 1;
            IRow Hrow;

            //insert data to document

            List<mPOSSConfirmation> model = new List<mPOSSConfirmation>();
            model = POSSConfirmation.getListPOSSConfirmation(pPOSSDateFrom,
                        pPOSSDateTo, pOrderNo, pPartNo, pItemNo, pRACode,
                        pBOCode, pPortionCode, pBOInstrCode, pSendFlag,
                        pSendDateFrom, pSendDateTo, pValidationStatus, pPackingCompany, pTGATGPFlag);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);
                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(((DateTime)result.CREATED_DT).ToString("yyyy-MM-dd"));
                Hrow.CreateCell(3).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(4).SetCellValue(result.ORDER_ITEM_NO);
                Hrow.CreateCell(5).SetCellValue(result.PROCESS_PART_NO);
                Hrow.CreateCell(6).SetCellValue(result.ORDER_PART_NO);
                Hrow.CreateCell(7).SetCellValue(result.PORTION_DESC);
                Hrow.CreateCell(8).SetCellValue(result.PROCESS_QTY);
                Hrow.CreateCell(9).SetCellValue(result.ORDER_QTY);
                Hrow.CreateCell(10).SetCellValue(result.BO_INSTRUCT_DESC);
                Hrow.CreateCell(11).SetCellValue(result.BO_DESC);
                Hrow.CreateCell(12).SetCellValue(result.RA_DESC);
                Hrow.CreateCell(13).SetCellValue(result.UNIT_FOB_PRICE);
                Hrow.CreateCell(14).SetCellValue(result.FINAL_FOB_PRICE);
                Hrow.CreateCell(15).SetCellValue(result.TARIF_CODE);
                Hrow.CreateCell(16).SetCellValue(result.HS_CODE);
                Hrow.CreateCell(17).SetCellValue(result.DIST_COLUMN);
                Hrow.CreateCell(18).SetCellValue(result.ETD == null? "" :((DateTime)result.ETD).ToString("yyyy-MM-dd"));
                if (result.TMAP_SEND_DT == null)
                    Hrow.CreateCell(19).SetCellValue("");
                else
                    Hrow.CreateCell(19).SetCellValue(((DateTime)result.TMAP_SEND_DT).ToString("yyyy-MM-dd HH:mm:ss"));
                Hrow.CreateCell(20).SetCellValue(result.VALIDATION_DESC);
                Hrow.CreateCell(21).SetCellValue(result.VALIDATION_STATUS);

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent3;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent3;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent3;
                Hrow.GetCell(13).CellStyle = styleContent3;
                Hrow.GetCell(14).CellStyle = styleContent3;
                Hrow.GetCell(15).CellStyle = styleContent3;
                Hrow.GetCell(16).CellStyle = styleContent3;
                Hrow.GetCell(17).CellStyle = styleContent3;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent3;
                Hrow.GetCell(21).CellStyle = styleContent3;


                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", filename));


        }

        //Send Parameter to [TB_R_BACKGROUND_TASK_REGISTRY] for Sending POSS Batch

        public ActionResult SendParameter(string p_Company)
        {
            string[] r = null;
            string msg = "";

            string resultMessage = POSSConfirmation.InsertParam(getpE_UserId.Username, p_Company);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = messageError.getMSPX00066INF("Send POSS Data", "<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = messageError.getMSPX00065INF("Send POSS Data", "<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }

        //Reproces Process
        public ActionResult SynchronizePOSSData()
        {

            string[] r = null;
            string msg = "";

            string resultMessage = POSSConfirmation.InsertParam_synchronize(getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                msg = r[1];
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                msg = r[1];
                return Json(new { success = "true", messages = msg}, JsonRequestBehavior.AllowGet);
            }

        }

        //Edit POSS Data, only for RA Code Editing
        public ActionResult UpdatePOSSConfirmation(string p_POSSDate, string p_CustNo, string p_OrderNo, string p_InvoiceNo,
            string p_InvoiceItem, string p_BOReleaseFlag, string p_PortionCd, string p_OrderPartNo, string p_RACode, string p_ItemNo,
            string p_ProcessPartNo, string p_CHANGED_BY)
        {
            p_POSSDate = reformatDate_edit(p_POSSDate);
            p_CHANGED_BY = getpE_UserId.Username;
            if (p_PortionCd == "" || p_PortionCd == null) { p_PortionCd = " "; }
            string[] r = null;
            string resultMessage = POSSConfirmation.UpdateData(p_POSSDate, p_CustNo, p_OrderNo, p_InvoiceNo, p_InvoiceItem, p_BOReleaseFlag,
                                    p_BOReleaseFlag, p_OrderPartNo, p_RACode, p_ItemNo, p_ProcessPartNo, p_CHANGED_BY);
            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }

            return null;
        }
    }

}
