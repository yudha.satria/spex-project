﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class ETDTrackingController : PageController
    {
        Log log = new Log();
        mTrackingETD ETDTracking = new mTrackingETD();
        MessagesString msgError = new MessagesString();


        public readonly string moduleID = "5";
        public readonly string functionID = "50001";
        public readonly string location = "PriceMaster";
        public readonly string userName = "Ulil.Zakiyatunisa";

        public ETDTrackingController()
        {
            Settings.Title = "Order Tracking";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {

            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Order Tracking");
            ViewBag.MSPX00003ERR = msgError.getMSPX00003ERR("Order No or Part No");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            ViewBag.MSPX000018ERR = msgError.getMSPX00018ERR();
            getpE_UserId = (User)ViewData["User"];
        }


        public string getpE_ItemNo
        {
            get
            {
                if (Session["getpE_ItemNo"] != null)
                {
                    return Session["getpE_ItemNo"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_ItemNo"] = value;
            }
        }


        public string getpE_OrderNo
        {
            get
            {
                if (Session["getpE_OrderNo"] != null)
                {
                    return Session["getpE_OrderNo"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_OrderNo"] = value;
            }
        }

        public string getpE_PartNo
        {
            get
            {
                if (Session["getpE_PartNo"] != null)
                {
                    return Session["getpE_PartNo"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_PartNo"] = value;
            }
        }


        public ActionResult _CallPUShipment(string pE_OrderNo, string pE_PartNo, string pE_ItemNo)
        {

            //ViewData["SetOrderNo"] = getpE_OrderNo;
            //ViewData["SetPartNo"] = getpE_PartNo;
            //ViewData["SetItemNo"] = getpE_ItemNo;
            List<mTrackingETD> model = ETDTracking.getListShipment(pE_OrderNo, pE_PartNo, pE_ItemNo);
            ViewData["ETDTracking"] = model;
            ViewBag.pE_OrderNo = pE_OrderNo;
            ViewBag.pE_PartNo = pE_PartNo;
            ViewBag.pE_ItemNo = pE_ItemNo;
            //ViewBag.pE_OrderQty = pE_OrderQty;
            return PartialView("_PartialShipment");

        }


        public ActionResult _CallPUReadyCargo(string pE_OrderNo, string pE_PartNo, string pE_ItemNo)
        {

            //ViewData["SetOrderNo"] = getpE_OrderNo;
            //ViewData["SetPartNo"] = getpE_PartNo;
            //ViewData["SetItemNo"] = getpE_ItemNo;
            List<mTrackingETD> model = ETDTracking.getListCargo(pE_OrderNo, pE_PartNo, pE_ItemNo);
            ViewData["ETDTracking"] = model;
            ViewBag.pE_OrderNo = pE_OrderNo;
            ViewBag.pE_PartNo = pE_PartNo;
            ViewBag.pE_ItemNo = pE_ItemNo;
            //ViewBag.pE_OrderQty = pE_OrderQty;
            return PartialView("_PartialReadyCargo");

        }



        public ActionResult _CallPUSupply(string pE_OrderNo, string pE_PartNo, string pE_ItemNo)
        {

            //ViewData["SetOrderNo"] = getpE_OrderNo;
            //ViewData["SetPartNo"] = getpE_PartNo;
            //ViewData["SetItemNo"] = getpE_ItemNo;
            
            List<mTrackingETD> model = ETDTracking.getListSupply(pE_OrderNo, pE_PartNo, pE_ItemNo);
            ViewData["ETDTracking"] = model;
            ViewBag.pE_OrderNo = pE_OrderNo;
            ViewBag.pE_PartNo = pE_PartNo;
            ViewBag.pE_ItemNo = pE_ItemNo;
           // ViewBag.pE_OrderQty = pE_OrderQty;
            return PartialView("_PartialSupply");

        }


        public ActionResult _CallPUBO(string pE_OrderNo, string pE_PartNo, string pE_ItemNo)
        {

            //ViewData["SetOrderNo"] = getpE_OrderNo;
            //ViewData["SetPartNo"] = getpE_PartNo;
            //ViewData["SetItemNo"] = getpE_ItemNo;
            //List<mTrackingETD> model = ETDTracking.getListBackOrder(pE_OrderNo, pE_PartNo, pE_ItemNo);
            List<mTrackingETD> model = ETDTracking.getListBackOrder(pE_OrderNo, pE_PartNo);
            ViewData["ETDTracking"] = model;
            ViewBag.pE_OrderNo = pE_OrderNo;
            ViewBag.pE_PartNo = pE_PartNo;
            //ViewBag.pE_ItemNo = pE_ItemNo;
            //ViewBag.pE_OrderQty = pE_OrderQty;
            return PartialView("_PartialBO");

        }

        public ActionResult _CallPUOTD(string pE_OrderNo, string pE_PartNo, string pE_ItemNo)
        {

            //ViewData["SetOrderNo"] = getpE_OrderNo;
            //ViewData["SetPartNo"] = getpE_PartNo;
            //ViewData["SetItemNo"] = getpE_ItemNo;
            List<mTrackingETD> model = ETDTracking.getListOTD(pE_OrderNo, pE_PartNo, pE_ItemNo);
            ViewData["ETDTracking"] = model;
            return PartialView("_PartialOTD");

        }


       


        //public ActionResult GridViewShipment(string pu_PART_NO, string pu_ITEM_NO, string pu_ORDER_NO, string pu_ORDER_QTY)
        //{
        //    List<mTrackingETD> model = new List<mTrackingETD>();

        //    {
        //        model = ETDTracking.getListShipment
        //        (pu_PART_NO, pu_ORDER_NO, pu_ITEM_NO, pu_ORDER_QTY);
        //    }
        //    return PartialView("popUpShipment", model);
        //}

        //public string reFormatDateSearch(string dt)
        //{
        //    string result = "";

        //    if (dt != "01.01.0100") result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));

        //    return result;
        //}


        public string reFormatDateSearch(string dt)
        {
            string result = "";

            string[] d = dt.Split('.');
            dt = d[2] + "." + d[1] + "." + d[0];//20.05.2014

            if (dt != "0100.01.01") result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));

            return result;

        }

        //Reload data in grid view

        public ActionResult GridViewETDTracking(string pParam, string pPathFile, string pOrderNo, string pPartNo, string pItemNo, string pOrderDt, string pOrderDtTo, string pOTD)
        {
           
            List<mTrackingETD> model = new List<mTrackingETD>();
            pOrderDt = reFormatDateSearch(pOrderDt);
            pOrderDtTo = reFormatDateSearch(pOrderDtTo);

            
            if (pPathFile != ""  )
            {
                //if (fileNameUpload == "TemplateETDTracking")
                //{
                    pParam = EXcelToSQL(pPathFile);
                    //call SP Upload Search
                    model = ETDTracking.UploadSearch(pParam);
                //}

            }
            else

            {
                
                    model = ETDTracking.getListETDTracking
                    (pPartNo, pOrderNo, pItemNo, pOrderDt, pOrderDtTo, pOTD);
               
            }
            return PartialView("GridETDTracking", model);
        }

        public ActionResult popUpReadyCargo()
        {
            return View();
        }



        #region cek format file to Upload  Price
        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplateETDTracking";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xls" },
                MaxFileSize = 20971520
            };
        }
        #endregion

        #region Extract Excel
        public const string UploadDirectory = "Content/FileUploadResult";
        public const string TemplateFName = "TemplateETDTracking";

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {

            string[] r = null;
            string rExtract = "";
            #region createlog
            Guid PROCESS_ID = Guid.NewGuid();
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            string MESSAGE_DETAIL = "";
            long pid = 0;
            string subFuncName = "EXcelToSQL";
            string isError = "N";
            string processName = "Upload ETD Tracking";
            string resultMessage = "";
            var pathfile="";
            int columns;
            int rows;

            #endregion


            //checking file name
            if (fileNameUpload == "TemplateETDTracking")
            {
                //MESSAGE_DETAIL = "Nama file Benar";
                if (e.UploadedFile.IsValid)
                {
                    

                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                     pathfile= Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + "System" + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    //rExtract = EXcelToSQL(pathfile);

                    #region generate message after upload
                    string[] aExtract = rExtract.Split('|');

                    if (aExtract[0] == "success")
                    {
                        MESSAGE_DETAIL = "Extracting data is success..";

                        //"success| " + totalUpload.ToString() + "|" + totalUploadSuccess.ToString() + "|" + totalUpdated.ToString() + "|" + totalCantUpdated.ToString() + "|" + totalInvalidData.ToString();

                        MESSAGE_DETAIL = "Total record=" + aExtract[1] + ", " + " Total new record=" + aExtract[2] + ", " + "Total updated record=" + aExtract[3] + ", " + " Total record can't updated=" + aExtract[4] + ", Total Invalid data=" + aExtract[5] + ".";


                        rExtract = "Success to retreive data,\n";
                      
                    }
                    else if (aExtract[0] == "error")
                    {
                        rExtract = "error : " + aExtract[1];
                        MESSAGE_DETAIL = "Extratcting data is falied, " + rExtract;

                    }
                    #endregion

                }
                else
                {
                    MESSAGE_DETAIL = "File is not .xls";
                    //pid = log.createLog(msgError.getMSPX00019ERR("Price"), userName, location + '.' + subFuncName, 0, moduleID, functionID);
                }
            }
            else
            {
                //pid = log.createLog(msgError.getMSPX00019ERR("Price"), userName, location + '.' + subFuncName, 0, moduleID, functionID);
                MESSAGE_DETAIL = "File name is not TemplateETDTracking";
            }
            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract + "|" + pathfile;
        }

        public string EXcelToSQL(string path)
        {
            //IPPCS_QAEntities db = new IPPCS_QAEntities();
            int totalUpload = 0;
            int totalUploadSuccess = 0;
            int totalUpdated = 0;
            int totalCantUpdated = 0;
            int totalInvalidData = 0;

            string PartNumber = "";
            string FranchiseCode = "";

            string PartNumbNotExis = "";
            string FranchiseCdNotExis = "";

            string[] OrderDate2 = null;
            string[] ValidTo2 = null;

            string sOrderDate = "";
            string OrderTanggal = "";
            string sValidTo = "";
            string subFuncName = "EXcelToSQL";
            string isError = "N";
            string processName = "Upload Price";
            string resultMessage = "";

            try
            {
                string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1;MAXSCANROWS=0;\"";
                //string strConn = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", path);

                string result = null;
                DataSet output = new DataSet();
                using (OleDbConnection conn = new OleDbConnection(strConn))
                {
                    conn.Open();
                    DataTable schemaTable = conn.GetOleDbSchemaTable(
                    OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    string BuyerPDCode = "";
                    string OrderNo = "";
                    DateTime OrderDate;
                    //DateTime ValidFrom;
                    //DateTime ValidTo;
                    string PartNo = "";
                    string ItemNo = "";
                    string OrderType = "";
                    string Transport = "";
                    string OrderQty = "";

                    string sheet = "[Sheet1$]";
                    OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + sheet, conn);
                    cmd.CommandType = CommandType.Text;
                    DataTable outputTable = new DataTable(sheet);
                    output.Tables.Add(outputTable);
                    new OleDbDataAdapter(cmd).Fill(outputTable);
                   
                    int x = outputTable.Rows.Count;
                    
                    for (int i = 0; i < outputTable.Rows.Count; i++)
                    {
                        
                        //if (outputTable.Rows[i][0].ToString() != "" && outputTable.Rows[i][1].ToString() != "")
                        //if (outputTable.Rows[i][1].ToString() != "")
                        //{

                            totalUpload = totalUpload + 1;



                            OrderDate2 = Convert.ToString(outputTable.Rows[i][2]).Split('.');
                            //ValidTo2 = Convert.ToString(outputTable.Rows[i][4]).Split('.');


                            //2012/05/29
                            sOrderDate = OrderDate2[2] + "/" + OrderDate2[1] + "/" + OrderDate2[0];
                            //sValidTo = ValidTo2[1] + "/" + ValidTo2[0] + "/" + ValidTo2[2];

                            BuyerPDCode = outputTable.Rows[i][0].ToString();
                            OrderNo = outputTable.Rows[i][1].ToString();
                            OrderTanggal = Convert.ToString(sOrderDate);
                            //OrderDate = outputTable.Rows[i][2].ToString();
                            PartNo = outputTable.Rows[i][3].ToString();
                            ItemNo = outputTable.Rows[i][4].ToString();
                            //OrderType = outputTable.Rows[i][5].ToString();
                            //Transport = outputTable.Rows[i][6].ToString();
                            OrderQty = outputTable.Rows[i][5].ToString();


                             //result = ETDTracking.UploadSearch(pParam);
                            if (OrderNo != "" && PartNo != "")
                            {
                                if (result == null)
                                {
                                    result = "'" + BuyerPDCode + OrderNo + OrderTanggal + PartNo + ItemNo + OrderQty + "'";


                                }

                                else
                                {
                                    result = result + ",'" + BuyerPDCode + OrderNo + OrderTanggal + PartNo + ItemNo + OrderQty + "'";

                                }
                            }
                            else
                            {
                                resultMessage = "Mandatory is blank";
                                

                            }

                           
                        //}
                    }
                }

               return  result ;
                //return "success| " + totalUpload.ToString() + "|" + totalUploadSuccess.ToString() + "|" + totalUpdated.ToString() + "|" + totalCantUpdated.ToString() + "|" + totalInvalidData.ToString();
            }
            catch (Exception err)
            {
                return "error|" + err.Message.ToString();
            }
        }
        #endregion




        #region Upload function when user click button upload
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }
        #endregion




        public void DownloadETDTracking_ByTicked(object sender, EventArgs e,string D_KeyDownload, string D_PartNo, string D_OrderNo, string D_ItemNo, string D_OrderDt, string D_OrderDtTo, string D_OTD)
        {

            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Order_Tracking_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
              
            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;

            ISheet sheet = workbook.GetSheet("OrderTracking");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "DownloadOrderTracking_" + date + ".xls";
            //string judul = "TMMIN";
            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(3).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(9).GetCell(4).SetCellValue(D_PartNo);
            sheet.GetRow(10).GetCell(4).SetCellValue(D_OrderNo);
            sheet.GetRow(11).GetCell(4).SetCellValue(D_ItemNo);
            sheet.GetRow(12).GetCell(4).SetCellValue(D_OrderDt);
            //sheet.GetRow(13).GetCell(4).SetCellValue(D_OrderDtTo);

            int row = 18;
            int rowNum = 1;
            IRow Hrow;

            List<mTrackingETD> model = new List<mTrackingETD>();
            model = ETDTracking.ETDDownloadByTicked(D_KeyDownload);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.BUYERPD_CD);
                Hrow.CreateCell(3).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(4).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm}", result.ORDER_DT));
                //Hrow.CreateCell(4).SetCellValue("");
                Hrow.CreateCell(5).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(6).SetCellValue(result.PART_NO);

                Hrow.CreateCell(7).SetCellValue(result.PROCESS_PART_NO);
                Hrow.CreateCell(8).SetCellValue(result.RA_CODE);
                Hrow.CreateCell(9).SetCellValue(result.ORDER_QTY);

                Hrow.CreateCell(10).SetCellValue(result.PROCESS_QTY);
                Hrow.CreateCell(11).SetCellValue(result.TOTAL_BO_QTY);
                Hrow.CreateCell(12).SetCellValue(result.CALCULATE_SUPPLY_QTY);
                Hrow.CreateCell(13).SetCellValue(result.CALCULATE_PACKED_QTY);
                Hrow.CreateCell(14).SetCellValue(result.SUPPLIED_QTY);

                Hrow.CreateCell(15).SetCellValue(result.OTD);

                //sheet.AddMergedRegion(new CellRangeAddress(row, row, 3, 4));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                //Hrow.GetCell(3).IsMergedCell=
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(14).CellStyle = styleContent;
                Hrow.GetCell(15).CellStyle = styleContent;
               
                row++;
                rowNum++;
            }
            //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
            //    sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }





        public void DownloadETDTracking_ByParameter(object sender, EventArgs e, string D_PartNo, string D_OrderNo, string D_ItemNo, string D_OrderDt, string D_OrderDtTo, string D_OTD)
        {

            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Order_Tracking_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;

            ISheet sheet = workbook.GetSheet("OrderTracking");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "DownloadOrderTracking_" + date + ".xls";
            //string judul = "TMMIN";
            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(4).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(9).GetCell(4).SetCellValue(D_PartNo);
            sheet.GetRow(10).GetCell(4).SetCellValue(D_OrderNo);
            sheet.GetRow(11).GetCell(4).SetCellValue(D_ItemNo);
            sheet.GetRow(12).GetCell(3).SetCellValue(D_OrderDt);
            sheet.GetRow(12).GetCell(5).SetCellValue(D_OrderDtTo);
            //sheet.GetRow(13).GetCell(4).SetCellValue(Convert.ToString(D_OTD));
            sheet.GetRow(13).GetCell(4).SetCellValue(D_OTD);
            int row = 18;
            int rowNum = 1;
            IRow Hrow;

            List<mTrackingETD> model = new List<mTrackingETD>();
            model = ETDTracking.ETDDownload(D_PartNo, D_OrderNo, D_ItemNo, D_OrderDt, D_OrderDtTo, D_OTD);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.BUYERPD_CD);
                Hrow.CreateCell(3).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(4).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm}", result.ORDER_DT));
                //Hrow.CreateCell(4).SetCellValue("");
                Hrow.CreateCell(5).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(6).SetCellValue(result.PART_NO);
                
                Hrow.CreateCell(7).SetCellValue(result.PROCESS_PART_NO);
                Hrow.CreateCell(8).SetCellValue(result.RA_CODE);
                Hrow.CreateCell(9).SetCellValue(result.ORDER_QTY);
                
                Hrow.CreateCell(10).SetCellValue(result.PROCESS_QTY);
                Hrow.CreateCell(11).SetCellValue(result.TOTAL_BO_QTY);
                Hrow.CreateCell(12).SetCellValue(result.CALCULATE_SUPPLY_QTY);
                Hrow.CreateCell(13).SetCellValue(result.CALCULATE_PACKED_QTY);
                Hrow.CreateCell(14).SetCellValue(result.SUPPLIED_QTY);
                
                Hrow.CreateCell(15).SetCellValue(result.OTD);


                //sheet.AddMergedRegion(new CellRangeAddress(row, row, 3, 4));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                //Hrow.GetCell(3).IsMergedCell=
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(14).CellStyle = styleContent;

                Hrow.GetCell(15).CellStyle = styleContent;
               
                row++;
                rowNum++;
            }
            //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
            //    sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
    }

}



