﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Text;
using System.Linq;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
//using iTextSharp.tool.xml;
//using iTextSharp.text.html.simpleparser;

namespace SPEX.Controllers
{
    public class VanningInquireController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mCaseMaster caseMaster = new mCaseMaster();
        VanningInquiry mVi = new VanningInquiry();
        Printer mPrinter = new Printer();

        public VanningInquireController()
        {
            Settings.Title = "Vanning Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            ViewData["PrinterCmb"] = mPrinter.getList();
            getpE_UserId = (User)ViewData["User"];
            ViewData["DestinationMasterList"]=mVi.getListDestination();
            ViewData["DefaultPrinter"] = mVi.getDefaultPrinter();
            //add agi 2017-10-23
            string range = new mSystemMaster().GetSystemValueList("vessel schedule inquiry", "range day", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            ViewBag.range = Convert.ToInt32(range);
            ViewBag.MSPXV0001ERR = msgError.getMsgText("MSPXV0001ERR").Replace("{0}", range);
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (string.IsNullOrEmpty(dt))
            {
                result = "";
            }
            else if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult OBRGridCallback(string pContainerNo, string pSealNo, string pVanningLabel, string pVanningLablePrintDtFrom, string pVanningLablePrintDtTo)
        {
           
            List<VanningInquiry> model = new List<VanningInquiry>();

            model = mVi.getList(pContainerNo, pSealNo, pVanningLabel, reFormatDate(pVanningLablePrintDtFrom), reFormatDate(pVanningLablePrintDtTo));

            return PartialView("VanningInquireGrid", model);
        }

        public ActionResult getVanningSTS(string vanningLabel)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            List<VanningInquiry> result = new List<VanningInquiry>();
            result = mVi.getVanningSTS(vanningLabel);

            return Json(new { vanningStatus = result}, JsonRequestBehavior.AllowGet);
           
        }

        public ActionResult Cancel(string pVanning_Label, string pTransportation_Code, string pTransportation_Desc)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = mVi.Cancel("'" + pVanning_Label + "'", "'" + pTransportation_Code + "'","'" + pTransportation_Desc + "'", getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CancelContainerNo(string pCase_No, string pVanning_Label, string pContainer_No,string pTransportation_Code)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = mVi.CancelContainerNo( "'"+pCase_No+"'" ,  pVanning_Label ,   pContainer_No ,  pTransportation_Code , getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

       
        public ActionResult vanningInquireReOpen(string ids)
        {
            if (getpE_UserId == null) 
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);
            
            string[] r = null;
            string msg = "";

            string resultMessage = mVi.VanningInquireReOpen("'"+ids+"'",  getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }


        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        private string GetDateSQL(string dt)
        {
            if (dt == "01.01.0100")
                return "";

            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime date = new DateTime(year, month, day);

            return date.ToString("yyyy-MM-dd");
        }

        public void DownloadData_ByParameter(object sender, EventArgs e, string pContainerNo, string pSealNo, string pVanningLable, string pVanningLablePrintDtFrom, string pVanningLablePrintDtTo)
        {
            string filename = "";
            VanningInquiry mVi = new VanningInquiry();
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPEX_VANN_" + date + ".csv";

            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            string dateNow = DateTime.Now.ToString();

            List<VanningInquiry> model = new List<VanningInquiry>();

            model = mVi.getList(pContainerNo, pSealNo, pVanningLable, reFormatDate(pVanningLablePrintDtFrom), reFormatDate(pVanningLablePrintDtTo));
            
            StringBuilder gr = new StringBuilder("");

            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol.Count; i++)
                {
                    gr.Append(Quote(CsvCol[i].Text)); gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);

                var n = 1;
                foreach (VanningInquiry d in model)
                {
                    csvAddLine(gr, new string[] {
                    Equs(n.ToString()), 
                    Equs(d.CONTAINER_NO), 
                    Equs(d.VANNING_DATE.ToString()), 
                    Equs(d.SEALNO),
                    Equs(d.CONTAINER_SIZE.ToString()), 
                    Equs(d.CONTAINER_TYPE),
                    Equs(d.NET_WEIGHT.ToString()), 
                    Equs(d.CASE_QTY.ToString()),
                   });
                    n++;
                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        private List<CsvColumn> CsvCol = CsvColumn.Parse(
            "No|C|1|0|UPDATE_FLAG;" +
            "Container No|C|6|1|CONTAINER_NO;" +
            "Vanning Date|C|10|0|VANNING_CLOSE_DT;" +
            "Seal No|C|5|0|SEALNO;" +
            "Container Size|C|23|0|CONTAINER_SIZE;" +
            "Container Type|C|23|1|CONTAINER_TYPE;" +
            "Net Weight|C|5|0|NETT_WEIGHT;" +
            "Sum Case|C|1|0|SUM_CASE;");

        public class CsvColumn
        {
            public string Text { get; set; }
            public string Column { get; set; }
            public string DataType { get; set; }
            public int Len { get; set; }
            public int Mandatory { get; set; }

            public static List<CsvColumn> Parse(string v)
            {
                List<CsvColumn> l = new List<CsvColumn>();

                string[] x = v.Split(';');


                for (int i = 0; i < x.Length; i++)
                {
                    string[] y = x[i].Split('|');
                    CsvColumn me = null;
                    if (y.Length >= 4)
                    {
                        int len = 0;
                        int mandat = 0;
                        int.TryParse(y[2], out len);
                        int.TryParse(y[3], out mandat);
                        me = new CsvColumn()
                        {
                            Text = y[0],
                            DataType = y[1],
                            Len = len,
                            Mandatory = mandat
                        };
                        l.Add(me);
                    }

                    if (y.Length > 4 && me != null)
                    {
                        me.Column = y[4];
                    }
                }
                return l;
            }
        
        }

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        private string _csv_sep;
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {
                    _csv_sep = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ";";
                }
                return _csv_sep;
            }
        }

        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        private string Equs(string s)
        {
            string separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (s != null)
            {
                if (s.Contains(separator))
                {
                    s = "\"" + s + "\"";
                }
                else
                {
                    s = "=\"" + s + "\"";
                }
            }
            return s;
        }

        public void DownloadData_ByTicked(object sender, EventArgs e, string pCASE_KEY, string pCASE_TYPE, string pDESCRIPTION)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/CaseMasterDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Case Master");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "CaseMaster" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(pCASE_TYPE);
            sheet.GetRow(9).GetCell(2).SetCellValue(pDESCRIPTION);

            int row = 16;
            int rowNum = 1;
            IRow Hrow;

            List<mCaseMaster> model = new List<mCaseMaster>();

            model = caseMaster.getDownloadList(pCASE_KEY);


            foreach (mCaseMaster result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.CASE_TYPE);
                Hrow.CreateCell(3).SetCellValue(result.DESCRIPTION);
                if (result.CASE_NET_WEIGHT != null)
                    Hrow.CreateCell(4).SetCellValue((double)result.CASE_NET_WEIGHT);
                else
                    Hrow.CreateCell(4).SetCellValue("");
                if (result.CASE_LENGTH != null)
                    Hrow.CreateCell(5).SetCellValue((double)result.CASE_LENGTH);
                else
                    Hrow.CreateCell(5).SetCellValue("");
                if (result.CASE_WIDTH != null)
                    Hrow.CreateCell(6).SetCellValue((double)result.CASE_WIDTH);
                else
                    Hrow.CreateCell(6).SetCellValue("");
                if (result.CASE_HEIGHT != null)
                    Hrow.CreateCell(7).SetCellValue((double)result.CASE_HEIGHT);
                else
                    Hrow.CreateCell(7).SetCellValue("");
                if (result.MEASUREMENT != null)
                    Hrow.CreateCell(8).SetCellValue((double)result.MEASUREMENT);
                else
                    Hrow.CreateCell(8).SetCellValue("");
                Hrow.CreateCell(9).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(10).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(11).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(12).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent;
                Hrow.GetCell(12).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        public ActionResult _popUpContainerNo(string pVanningLabel, string pContainerNo)
        {
            List<VanningInquiry> model = new List<VanningInquiry>();

            model = mVi.getDetailContainerNo(pVanningLabel, pContainerNo);

            ViewData["ContainerNo"] = pContainerNo;
            ViewData["ContainerData"] = model;

            return PartialView("_PartialPopUpMaster");

        }
        public ActionResult getDetailContainerNo(string pVanningLabel, string pContainerNo)
        {
            List<VanningInquiry> model = new List<VanningInquiry>();

            model = mVi.getDetailContainerNo(pVanningLabel, pContainerNo);

            ViewData["ContainerData"] = model;

            return PartialView("_partial_load_vanning_inquire", model); 
        }

        public ActionResult _popUpOK(string pSI_No)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string model = mVi.getDataPopUpSiNo(pSI_No);

            r = model.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string TransCD = r[1].ToString();

                ViewData["SINoData"] = pSI_No;

                ViewData["TransportationData"] = TransCD + "-" +mVi.getTransDesc(TransCD.Trim()).ToUpper();
               
                if (TransCD=="1")
                {
                    ViewData["ContainerNoData"] = "DUMMY_BYAIR";
                    ViewData["setReadOnlyContNo"] = false;
                }
                else {
                    ViewData["ContainerNoData"] = "";
                    ViewData["setReadOnlyContNo"] = true;
                }

                ViewData["DestinationData"] = mVi.getDestination();

                if (TransCD=="1")
                {
                    ViewData["SealNoData"] = "";
                    ViewData["setReadOnlySealNo"] = false;
                    ViewData["SealNoLabel"] = "Seal No";
                }else{
                    ViewData["SealNoData"] = "";
                    ViewData["setReadOnlySealNo"] = true;
                    ViewData["SealNoLabel"] = "*Seal No";
                }

                if (TransCD=="1")
                {
                    ViewData["TareData"] = "";
                    ViewData["setReadOnlyTare"] = false;
                    ViewData["TareLabel"] = "Tare";
                }else{
                    ViewData["TareData"] = "";
                    ViewData["setReadOnlyTare"] = true;
                    ViewData["TareLabel"] = "*Tare";
                }

                if (TransCD=="1")
                {
                    ViewData["setReadOnlyConType"] = false;
                    ViewData["ContainerTypeData"] ="";
                    ViewData["ContainerTypeLabel"] = "Container Type";
                }
                else
                {
                    ViewData["setReadOnlyConType"] = true;
                    ViewData["ContainerTypeData"] = mVi.getContainerType();
                    ViewData["ContainerTypeLabel"] = "*Container Type";
                }

                ViewData["VendorNameData"] = mVi.getVendorName();


               // return Json(new { message = mVi.getVendorName() }, JsonRequestBehavior.AllowGet);

              return PartialView("_partial_load_vanning_inquire_ok");
            }

          

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveAndPrint(string pSI_No, string pTrans_CD, string pTrans_DESC ,string pContainer_No, string pDestination,
            string pSeal_No, string pTare, string pContainerType, string pContainerSize, string pVendor_Name,string printer)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;

            string resultMessage = mVi.SaveAndPrint(pSI_No, pTrans_CD, pTrans_DESC, pContainer_No, pDestination, pSeal_No, pTare, pContainerType,pContainerSize, pVendor_Name, getpE_UserId.Username,printer);

            r = resultMessage.Split('|');

            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult PrintDeliveryNote(string pVanning_Label,string printer)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;

            string resultMessage = mVi.PrintDeliveryNote(pVanning_Label, getpE_UserId.Username,printer);

            r = resultMessage.Split('|');

            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult RePrint(string pVanning_Label, string printer)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;

            string resultMessage = mVi.RePrint("'"+pVanning_Label+"'", getpE_UserId.Username, printer);

            r = resultMessage.Split('|');

            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }

}

