﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Text;
using System.Linq;
using System.Web;

namespace SPEX.Controllers
{
    public class ServicePriceMasterController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        ServicePriceMaster spm = new ServicePriceMaster();
        ServicePriceMasterEditAdd spm2 = new ServicePriceMasterEditAdd();

        public ServicePriceMasterController()
        {
            Settings.Title = "Service Price Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User) ViewData["User"];
            ViewData["ServiceID"] = spm.GetServiceId();
            ViewData["Type"] = spm.GetTipe();

            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();

            List<string> Type = new ServicePriceMaster().GetTipe();
            List<string> ContainerSize = new ServicePriceMaster().GetContainerSize();
            List<string> Curency = new ServicePriceMaster().GetCurrency();
            List<string> VendorType = new ServicePriceMaster().GetVendorType();

            ViewData["Type"] = Type;
            ViewData["ContainerSize"] = ContainerSize;
            ViewData["Currency"] = Curency;
            ViewData["VendorType"] = VendorType;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GetServicePriceMasterCallBack(string ServiceReg, string ServiceCode, string ServiceName, string ServiceType, 
                                                          string ValidFrom, string ValidTo, string IsLatest, string VendorType, string Vat)
        {
            if (ValidFrom == "01.01.0100")
            {
                ValidFrom = "";
            }

            if (ValidTo == "01.01.0100")
            {
                ValidTo = "";
            }

            List<ServicePriceMaster> model = new List<ServicePriceMaster>();
            model = spm.getServicePriceMaster(ServiceReg, ServiceCode, ServiceName, ServiceType, ValidFrom, ValidTo, IsLatest, VendorType, Vat);

            return PartialView("ServicePriceMasterGrid", model);
        }

        #region UPLOAD DATA
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, FileUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplateServicePriceMaster";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".csv" },
                MaxFileSize = 20971520
            };
        }

        private string FunctionID = "SP001";
        private string ModuleID = "SP";
        public void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_SERVICE_PRICE";

            string templateFileName = "TemplateServicePriceMaster";
            string UploadDirectory = Server.MapPath("~/Content/FileUploadResult");
            long ProcessID = 0;

            Lock lockTable = new Lock();
            int IsLock = lockTable.is_lock(FunctionID, out ProcessID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(ProcessID.ToString());
                Session["IS_LOCKED"] = true;
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                ProcessID = log.createLog("MSGSPX0001|INF|Upload Process Started", getpE_UserId.Username, "Upload.BulkCSV", 0, ModuleID, FunctionID);

                lockTable.PROCESS_ID = ProcessID;
                lockTable.FUNCTION_ID = FunctionID;
                lockTable.LOCK_REF = FunctionID;
                lockTable.CREATED_BY = getpE_UserId.Username;
                lockTable.CREATED_DT = DateTime.Now;
                lockTable.LockFunction(lockTable);

                Session["IS_LOCKED"] = false;
                Session["UPLOAD_PID"] = ProcessID;

                rExtract = spm.DeleteTempData();
                if (rExtract == "SUCCESS")
                {
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);
                    var pathfile = Path.Combine(updir, templateFileName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));
                    e.UploadedFile.SaveAs(pathfile);

                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));

                    ProcessID = log.createLog("MSGSPX0001|INF|Upload to Temporary Table Success", getpE_UserId.Username, "Upload.BulkCSV", ProcessID, ModuleID, FunctionID);
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        public string UploadValidationAndSave()
        {
            string result = "";
            bool is_locked = Session["IS_LOCKED"] == null ? false : Convert.ToBoolean(Session["IS_LOCKED"]);
            long pid = 0;

            if (is_locked)
            {
                return "LOCKED";
            }
            else
            {
                pid = Session["UPLOAD_PID"] == null ? 0 : Convert.ToInt64(Session["UPLOAD_PID"]);
                if (pid == 0)
                {
                    pid = log.createLog("MSGSPX0001|INF|Upload Validation Process Started", getpE_UserId.Username, "Upload.BulkCSV", 0, ModuleID, FunctionID);
                }

                result = spm.BulkInsertTempCSVToTempPartPrice(getpE_UserId.Username, pid);

                if (result != "ERROR")
                {
                    spm.DeleteTempData();
                    Lock unlock = new Lock();
                    unlock.UnlockFunction(FunctionID);
                }

                Session.Remove("IS_LOCKED");

                string msg = "";
                if (result == "ERROR")
                {
                    msg = msgError.getMSPX00066INF("Upload Service Price Master", "<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>");
                }
                else
                {
                    msg = msgError.getMSPX00065INF("Upload Service Price Master", "<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>");
                }

                return msg + "|" + pid;
            }
        }
        #endregion

        #region DOWNLOAD DATA
        public void DownloadErrorUpload(object sender, EventArgs e)
        {
            string filename = "";
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "ErrorUpload_Service_Price_Master_" + date + ".csv";

            string dateNow = DateTime.Now.ToString();

            List<ServicePriceMaster> model = new List<ServicePriceMaster>();

            model = spm.getServicePriceErrorUpload();

            StringBuilder gr = new StringBuilder("");
            string format = "";

            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol_Download.Count; i++)
                {
                    if (i != 0 && i < 12)
                    {
                        format = (CsvCol_Download[i].Text == "Valid From" || CsvCol_Download[i].Text == "Valid To") ? "[dd.MM.yyyy]" : "";
                        gr.Append(Quote(CsvCol_Download[i].Text + format));
                        gr.Append(CSV_SEP);
                    }
                }

                gr.Append(Environment.NewLine);

                foreach (ServicePriceMaster d in model)
                {
                    csvAddLine(gr, new string[] {
                    Equs(d.SERVICE_CODE),
                    Equs(d.SERVICE_NAME),
                    Equs(d.SERVICE_TYPE),
                    Equs(d.CONTAINER_SIZE),
                    Equs(d.VENDOR_TYPE),
                    Equs(d.PRICE),
                    Equs(d.CURRENCY),
                    Equs(d.VAT),
                    Equs(d.COST_CENTER),
                    Equs(d.VALID_FROM),
                    Equs(d.VALID_TO),
                    Equs(d.ERROR_MSG)
                   });
                }
            }

            spm.DeleteTempData();
            Lock unlock = new Lock();
            unlock.UnlockFunction(FunctionID);

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        public void DownloadTemplate()
        {
            string filename = "";
            string format = "";
            filename = "Service_Price_Master.csv";

            StringBuilder gr = new StringBuilder("");
            for (int i = 0; i < CsvCol_Download.Count; i++)
            {
                if (i != 0 && i < 12)
                {
                    format = (CsvCol_Download[i].Text == "Valid From" || CsvCol_Download[i].Text == "Valid To") ? "[dd.MM.yyyy]" : "";
                    gr.Append(Quote(CsvCol_Download[i].Text + format));
                    gr.Append(CSV_SEP);
                }
            }

            gr.Append(Environment.NewLine);

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        public void DownloadData(object sender, EventArgs e, string ServiceReg, string ServiceCode, string ServiceName,
                                 string ServiceType, string ValidFrom, string ValidTo, string IsLatest, string VendorType, string Vat)
        {
            string filename = "";
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "Service_Price_Master_" + date + ".csv";

            string dateNow = DateTime.Now.ToString();

            List<ServicePriceMaster> model = new List<ServicePriceMaster>();

            model = spm.getServicePriceMaster(ServiceReg, ServiceCode, ServiceName, ServiceType, reFormatDate(ValidFrom), reFormatDate(ValidTo), IsLatest, VendorType, Vat);

            StringBuilder gr = new StringBuilder("");

            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol_Download.Count; i++)
                {
                    gr.Append(Quote(CsvCol_Download[i].Text)); gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);


                foreach (ServicePriceMaster d in model)
                {
                    csvAddLine(gr, new string[] {
                    Equs(d.SERVICE_REGISTRATION),
                    Equs(d.SERVICE_CODE),
                    Equs(d.SERVICE_NAME),
                    Equs(d.SERVICE_TYPE),
                    Equs(d.CONTAINER_SIZE),
                    Equs(d.VENDOR_TYPE),
                    Equs(d.PRICE),
                    Equs(d.CURRENCY),
                    Equs(d.VAT),
                    Equs(d.COST_CENTER),
                    Equs(d.VALID_FROM),
                    Equs(d.VALID_TO),
                    Equs(d.IS_LATEST),
                    Equs(d.CREATED_BY),
                    Equs(d.CREATED_DATE),
                    Equs(d.CHANGED_BY),
                    Equs(d.CHANGED_DATE)
                   });
                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        private List<CsvColumn> CsvCol_Download = CsvColumn.Parse(
            "Service Registration|C|1|0|SERVICE_REGISTRATION;" +
            "Service Code|C|6|1|SERVICE_CODE;" +
            "Service Name|C|10|0|SERVICE_NAME;" +
            "Service Type|C|5|0|SERVICE_TYPE;" +
            "Container Size|C|10|0|CONTAINER_SIZE;" +
            "Vendor Type|C|1|0|VENDOR_TYPE;" +
            "Price|C|23|0|PRICE;" +
            "Currency|C|23|1|CURRENCY;" +
            "VAT|C|23|0|VAT;" +
            "Cost Center|C|23|1|COST_CENTER;" +
            "Valid From|C|5|0|VALID_FROM;" +
            "Valid To|C|1|0|VALID_TO;" +
            "Latest|C|1|0|IS_LATEST;" +
            "Created By|C|1|0|CREATED_BY;" +
            "Created Date|C|10|0|CREATED_DATE;" +
            "Changed By|C|1|0|CHANGED_BY;" +
            "Changed Date|C|1|0|CHANGED_DATE;"
        );

        public class CsvColumn
        {
            public string Text { get; set; }
            public string Column { get; set; }
            public string DataType { get; set; }
            public int Len { get; set; }
            public int Mandatory { get; set; }

            public static List<CsvColumn> Parse(string v)
            {
                List<CsvColumn> l = new List<CsvColumn>();

                string[] x = v.Split(';');

                for (int i = 0; i < x.Length; i++)
                {
                    string[] y = x[i].Split('|');
                    CsvColumn me = null;
                    if (y.Length >= 4)
                    {
                        int len = 0;
                        int mandat = 0;
                        int.TryParse(y[2], out len);
                        int.TryParse(y[3], out mandat);
                        me = new CsvColumn()
                        {
                            Text = y[0],
                            DataType = y[1],
                            Len = len,
                            Mandatory = mandat
                        };
                        l.Add(me);
                    }

                    if (y.Length > 4 && me != null)
                    {
                        me.Column = y[4];
                    }
                }
                return l;
            }
        }

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        private string _csv_sep;
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {
                    _csv_sep = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ";";
                }
                return _csv_sep;
            }
        }

        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        private string Equs(string s)
        {
            string separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (s != null)
            {
                if (s.Contains(separator))
                {
                    s = "\"" + s + "\"";
                }
                else
                {
                    s = "=\"" + s + "\"";
                }
            }
            return s;
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (string.IsNullOrEmpty(dt))
            {
                result = "";
            }
            else if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }
        #endregion

        #region ADD, EDIT, DELETE DATA
        public ActionResult PopUp(string Data, string Mode)
        {
            if (Mode == "Add")
            {
                spm2.SERVICE_REGISTRATION = spm2.GenerateNewServiceID();
                ViewBag.Mode = "Add";
            }
            else
            {
                spm2 = System.Web.Helpers.Json.Decode<ServicePriceMasterEditAdd>(Data);
                spm2 = spm2.GetData(spm2, Mode).FirstOrDefault();
                ViewBag.Mode = "Edit";
            }
            
            return PartialView("PartialForm", spm2);
        }

        public ActionResult Add(string Data)
        {
            ServicePriceMasterEditAdd Model = System.Web.Helpers.Json.Decode<ServicePriceMasterEditAdd>(Data);
            if (getpE_UserId != null)
            {
                Model.CREATED_DATE2 = DateTime.Now;
                Model.CREATED_BY = getpE_UserId.Username;
                string result = Model.AddData(Model);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
            return null;
        }

        public ActionResult Update(string Data)
        {
            ServicePriceMasterEditAdd Model = System.Web.Helpers.Json.Decode<ServicePriceMasterEditAdd>(Data);
            if (getpE_UserId != null)
            {
                Model.CHANGED_DATE2 = DateTime.Now;
                Model.CHANGED_BY = getpE_UserId.Username;
                string result = Model.UpdateData(Model);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }

        public ActionResult Delete(string ServiceReg)
        {
            ServicePriceMasterEditAdd Model = new ServicePriceMasterEditAdd();
            if (getpE_UserId != null)
            {
                string UserId = getpE_UserId.Username;
                string result = Model.Delete(ServiceReg, UserId);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }
        #endregion
    }
}

