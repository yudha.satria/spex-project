﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class PackingCompletionInquiryController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mCaseMaster caseMaster = new mCaseMaster();
        OBR obr = new OBR();

        public PackingCompletionInquiryController()
        {
            Settings.Title = "Packing Completion Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            ViewData["ObrStatusList"] =obr.GetOBRStatusList();
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (string.IsNullOrEmpty(dt))
            {
                result = "";
            }
            else if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult OBRGridCallback(string pObr_No, string pItem_No, string pStartOrdDt, string pEndOrdDt, string pOrder_No, string pPart_No, string pStatus)
        {
            List<OBR> model = new List<OBR>();

            model = obr.getList(pObr_No, pItem_No, reFormatDate(pStartOrdDt), reFormatDate(pEndOrdDt), pOrder_No, pPart_No, pStatus);

            return PartialView("OBRGrid", model);
        }

        public ActionResult EditOBR(string id, decimal? pCancelQty)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = obr.Update(id, pCancelQty, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult IsStatusOBRHold(string ids)
        {
            OBR o = obr.getOBR(ids);
            string os = "false";
            if (o.OBR_STATUS == "1")
            {
                os = "true";
            }
            return Json(new { success = os }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditStatusOBR(string ids, string pStatusID)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = obr.UpdateStatus(ids, pStatusID, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddCase(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT,
            string pDESCRIPTION)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = caseMaster.Add(pCASE_TYPE, pCASE_NET_WEIGHT, pCASE_LENGTH, pCASE_WIDTH, pCASE_HEIGHT, pDESCRIPTION, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteCase(string pCASE_TYPE)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = caseMaster.Delete(pCASE_TYPE, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        private string GetDateSQL(string dt)
        {
            if (dt == "01.01.0100")
                return "";

            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime date = new DateTime(year, month, day);

            return date.ToString("yyyy-MM-dd");
        }

        public void DownloadData_ByParameter(object sender, EventArgs e, string pObr_No, string pItem_No, string pStartOrdDt, string pEndOrdDt, string pOrder_No, string pPart_No, string pStatus, string pStatusName)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/OBR_Download_Report.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            //styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            //styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;
            IFont font = workbook.CreateFont();
            font.FontHeight = 8;
            styleContent.SetFont(font);

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            //styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("OBR");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "OBR" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(2).GetCell(7).SetCellValue(pObr_No);
            sheet.GetRow(2).GetCell(31).SetCellValue(pItem_No);
            sheet.GetRow(2).GetCell(55).SetCellValue((pStartOrdDt == "01.01.0100" ? "" : pStartOrdDt));
            sheet.GetRow(2).GetCell(67).SetCellValue((pEndOrdDt == "01.01.0100" ? "" : pEndOrdDt));

            sheet.GetRow(3).GetCell(7).SetCellValue(pOrder_No);
            sheet.GetRow(3).GetCell(31).SetCellValue(pPart_No);
            sheet.GetRow(3).GetCell(55).SetCellValue(pStatusName);

            List<OBR> model = new List<OBR>();

            model = obr.getList(pObr_No, pItem_No, reFormatDate(pStartOrdDt), reFormatDate(pEndOrdDt), pOrder_No, pPart_No, pStatus);

            int row = 7;
            int rowNum = 1;
            IRow Hrow;
            int RowTemplate = 3;

            foreach (OBR d in model)
            {
                Hrow = sheet.CreateRow(row);
                for (int i = 0; i <= 79; i++)
                {
                    Hrow.CreateCell(i).SetCellValue("");
                }
                Hrow.CreateCell(0).SetCellValue(d.OBR_NO);
                Hrow.CreateCell(7).SetCellValue(d.ObrDtTxt);
                Hrow.CreateCell(15).SetCellValue(d.TMAP_ORDER_NO);

                Hrow.CreateCell(21).SetCellValue(d.TMAP_ITEM_NO);
                Hrow.CreateCell(27).SetCellValue(d.PART_NO);
                Hrow.CreateCell(33).SetCellValue(d.TmapOrderDtTxt);
                Hrow.CreateCell(40).SetCellValue(d.ORDER_QTY.ToString());
                Hrow.CreateCell(45).SetCellValue(d.CANCEL_QTY.ToString());
                Hrow.CreateCell(50).SetCellValue(d.ACCEPT_QTY.ToString());
                Hrow.CreateCell(55).SetCellValue(d.OBR_STATUS_NAME);
                
                for (int i = 0; i <= 79; i++)
                {
                    if (i == 79 || i == 74 || i == 6 || i==14 || i == 20 || i == 26 || i == 32 || i == 39 || i == 44 || i == 49 || i == 54 || i == 59 || i == 64 || i == 69)
                    {
                        Hrow.GetCell(i).CellStyle = styleContent2;
                    }
                    else
                    {
                        Hrow.GetCell(i).CellStyle = styleContent;
                    }
                }

                row++;
            }


            
            
            /*
            List<mCaseMaster> model = new List<mCaseMaster>();

            model = caseMaster.getList(pCASE_TYPE, pDESCRIPTION);


            foreach (mCaseMaster result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.CASE_TYPE);
                Hrow.CreateCell(3).SetCellValue(result.DESCRIPTION);
                if (result.CASE_NET_WEIGHT != null)
                    Hrow.CreateCell(4).SetCellValue((double)result.CASE_NET_WEIGHT);
                else
                    Hrow.CreateCell(4).SetCellValue("");
                if (result.CASE_LENGTH != null)
                    Hrow.CreateCell(5).SetCellValue((double)result.CASE_LENGTH);
                else
                    Hrow.CreateCell(5).SetCellValue("");
                if (result.CASE_WIDTH != null)
                    Hrow.CreateCell(6).SetCellValue((double)result.CASE_WIDTH);
                else
                    Hrow.CreateCell(6).SetCellValue("");
                if (result.CASE_HEIGHT != null)
                    Hrow.CreateCell(7).SetCellValue((double)result.CASE_HEIGHT);
                else
                    Hrow.CreateCell(7).SetCellValue("");
                if (result.MEASUREMENT != null)
                    Hrow.CreateCell(8).SetCellValue((double)result.MEASUREMENT);
                else
                    Hrow.CreateCell(8).SetCellValue("");
                Hrow.CreateCell(9).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(10).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(11).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(12).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent;
                Hrow.GetCell(12).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            */
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        public void DownloadData_ByTicked(object sender, EventArgs e, string pCASE_KEY, string pCASE_TYPE, string pDESCRIPTION)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/CaseMasterDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Case Master");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "CaseMaster" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(pCASE_TYPE);
            sheet.GetRow(9).GetCell(2).SetCellValue(pDESCRIPTION);

            int row = 16;
            int rowNum = 1;
            IRow Hrow;

            List<mCaseMaster> model = new List<mCaseMaster>();

            model = caseMaster.getDownloadList(pCASE_KEY);


            foreach (mCaseMaster result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.CASE_TYPE);
                Hrow.CreateCell(3).SetCellValue(result.DESCRIPTION);
                if (result.CASE_NET_WEIGHT != null)
                    Hrow.CreateCell(4).SetCellValue((double)result.CASE_NET_WEIGHT);
                else
                    Hrow.CreateCell(4).SetCellValue("");
                if (result.CASE_LENGTH != null)
                    Hrow.CreateCell(5).SetCellValue((double)result.CASE_LENGTH);
                else
                    Hrow.CreateCell(5).SetCellValue("");
                if (result.CASE_WIDTH != null)
                    Hrow.CreateCell(6).SetCellValue((double)result.CASE_WIDTH);
                else
                    Hrow.CreateCell(6).SetCellValue("");
                if (result.CASE_HEIGHT != null)
                    Hrow.CreateCell(7).SetCellValue((double)result.CASE_HEIGHT);
                else
                    Hrow.CreateCell(7).SetCellValue("");
                if (result.MEASUREMENT != null)
                    Hrow.CreateCell(8).SetCellValue((double)result.MEASUREMENT);
                else
                    Hrow.CreateCell(8).SetCellValue("");
                Hrow.CreateCell(9).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(10).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(11).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(12).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent3;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent;
                Hrow.GetCell(12).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
    }
}

