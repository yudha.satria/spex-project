﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.IO;
using System.Drawing;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using Toyota.Common.Credential;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using Toyota.Common.Web.Platform.Starter.Models;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using NPOI.POIFS.FileSystem;
using NPOI.HPSF;
using SPEX.Cls;

namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class ETDController : PageController
    {
       
        public ETDController()
        {
            Settings.Title = "ETD INQUERY";
        }

        protected override void Startup()
        {


        }
        
        //untuk Pop UP ORI order
        public ActionResult popUpOriOrder()
        {
            return View();
        }

        //untuk Pop Up POSS
        public ActionResult popUpPOSS()
        {
            return View();
        }

        //untuk pop up BO
        public ActionResult popUpBO()
        {
            return View();
        }

        //untuk pop up Supply
        public ActionResult popUpSupply()
        {
            return View();
        }

        //untuk pop up Ready Cargo
        public ActionResult popUpReadyCargo()
        {
            return View();
        }

        //untuk pop up Vanning
        public ActionResult popUpVanning()
        {
            return View();
        }

        //utnuk pop up shipment
        public ActionResult popUpShipment()
        {
            return View();
        }

    }
}
