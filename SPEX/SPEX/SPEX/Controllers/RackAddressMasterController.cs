﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.XSSF.UserModel;
using NPOI.XSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using SPEX.Cls;
using SPEX.Controllers.Util;
using SPEX.Models;
using SPEX.Models.Combobox;
using SPEX.Models.RackAddressMaster;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class RackAddressMasterController : PageController
    {
        MessagesString messageError = new MessagesString();
        String ScreenName = "Rack Address Master";
        public string moduleID = "SPX12";
        public string screenID = "SPX120100";
        public string batchID = "SPX120200";
        public string reportID = "SPX120300";
        public const string fileTemplateDownloadLocation = "~/Template/RackAddressMasterDownload.xlsx";
        public const string UploadDirectory = "Content\\FileUploadResult";
        public const string TemplateFName = "TemplateRackAddressMaster";

        public RackAddressMasterController()
        {
            Settings.Title = ScreenName;
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            ViewBag.MSPX00001ERR = messageError.getMSPX00001ERR(ScreenName);
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = messageError.getMSPX00009ERR();
            /*No data found*/
            ViewBag.MSPXS2006ERR = messageError.getMsgText("MSPXS2006ERR");
            /*{0} should not be empty.*/
            ViewBag.MSPXS2005ERR = messageError.getMsgText("MSPXS2005ERR");
            /*A single record must be selected.*/
            ViewBag.MSPXS2017ERR = messageError.getMsgText("MSPXS2017ERR");
            /*Duplication found for {0}.*/
            ViewBag.MSPXS2011ERR = messageError.getMsgText("MSPXS2011ERR");
            /*{0} is not exists in  {1}*/
            ViewBag.MSPXS2010ERR = messageError.getMsgText("MSPXS2010ERR");
            /*Cannot find template file {0} on folder {1}.*/
            ViewBag.MSPXS2034ERR = messageError.getMsgText("MSPXS2034ERR");
            /*Error writing file = {0}. Reason = {1}.*/
            ViewBag.MSPXS2035ERR = messageError.getMsgText("MSPXS2035ERR");
            /*Invalid data type of {0}. Should be {1}.*/
            ViewBag.MSPXS2012ERR = messageError.getMsgText("MSPXS2012ERR");
            /*Are you sure you want to confirm the operation?*/
            ViewBag.MSPXS2018INF = messageError.getMsgText("MSPXS2018INF");
            /*Are you sure you want to abort the operation?*/
            ViewBag.MSPXS2019INF = messageError.getMsgText("MSPXS2019INF");
            /*Are you sure you want to delete the record?*/
            ViewBag.MSPXS2020INF = messageError.getMsgText("MSPXS2020INF");
            /*Are you sure you want to download this data?*/
            ViewBag.MSPXS2021INF = messageError.getMsgText("MSPXS2021INF");

            getpE_UserId = (User)ViewData["User"];
            RackAddressCombobox("0");
        }

        #region Combobox
        public void RackAddressCombobox(String isMandatory)
        {
            mCombobox itemNotMandatory = new mCombobox();
            if (isMandatory.Equals("1"))
            {
                itemNotMandatory.COMBO_LABEL = "--SELECT--";
                itemNotMandatory.COMBO_VALUE = "";
            }
            else
            {
                itemNotMandatory.COMBO_LABEL = "--ALL--";
                itemNotMandatory.COMBO_VALUE = "";
            }

            List<mCombobox> ListZone = null;
            ListZone = ComboboxRepository.Instance.ZoneCode();
            ListZone.Insert(0, itemNotMandatory);
            ViewData["ZONE_CD"] = ListZone;

            List<mCombobox> ListAisle = null;
            ListAisle = ComboboxRepository.Instance.AisleCode();
            ListAisle.Insert(0, itemNotMandatory);
            ViewData["AISLE_CD"] = ListAisle;

            List<mCombobox> ListRack = null;
            ListRack = ComboboxRepository.Instance.RackCode();
            ListRack.Insert(0, itemNotMandatory);
            ViewData["RACK_CD"] = ListRack;

            List<mCombobox> ListDeleteFlag = null;
            ListDeleteFlag = ComboboxRepository.Instance.SystemMasterCode("DELETE_FLAG");
            ListDeleteFlag.Insert(0, itemNotMandatory);
            ViewData["DELETE_FLAG"] = ListDeleteFlag;
        }
        #endregion

        #region C.R.U.D.S
        #region Search Data
        public ActionResult GridData(String pRackAddressCode, String pZoneCode, String pAisleCode, String pRackCode, String pDeleteFlag)
        {
            sRackAddressMaster searchModel = new sRackAddressMaster();
            searchModel.RackAddressCode = pRackAddressCode;
            searchModel.ZoneCode = pZoneCode;
            searchModel.AisleCode = pAisleCode;
            searchModel.RackCode = pRackCode;
            searchModel.DeleteFlag = pDeleteFlag;

            List<mRackAddressMaster> model = null;
            model = RackAddressMasterRepository.Instance.SearchData(searchModel);

            RackAddressCombobox("1");

            return PartialView("RackAddressMasterGrid", model);
        }
        #endregion

        #region Save Data
        public ActionResult SaveData(String iMode, String iRackAddressCode, String iZoneCode, String iAisleCode, String iRackCode, String iRow, String iColumn, String iLength, String iWidth, String iHeight, String iChangedBy, String iChangedDt)
        {
            List<mRackAddressMaster> listDataModel = new List<mRackAddressMaster>();
            mRackAddressMaster dataModel = new mRackAddressMaster();
            if (iMode.Equals("delete"))
            {
                var listKeyHeader = iRackAddressCode.Split(';');
                foreach (var lsKeyHeader in listKeyHeader)
                {
                    var listKeyDetail = lsKeyHeader.Split('|');
                    dataModel = new mRackAddressMaster();
                    dataModel.MODE = iMode;
                    dataModel.RACK_ADDRESS_CD = listKeyDetail[0].ToString();
                    dataModel.CHANGED_BY = listKeyDetail[1].ToString();
                    dataModel.CHANGED_DT = listKeyDetail[2].ToString();
                    dataModel.USER_ID = getpE_UserId.Username;

                    listDataModel.Add(dataModel);
                }
            }
            else
            {
                dataModel.MODE = iMode;
                dataModel.RACK_ADDRESS_CD = iMode != "add" ? iRackAddressCode : String.Format("{0}{1}{2}-{3}-{4}", iZoneCode, iAisleCode, iRackCode, iRow.PadLeft(2, '0'), iColumn.PadLeft(2, '0'));
                dataModel.ZONE_CD = iZoneCode;
                dataModel.AISLE_CD = iAisleCode;
                dataModel.RACK_CD = iRackCode;
                dataModel.ROW = iRow.PadLeft(2, '0');
                dataModel.COLUMN = iColumn.PadLeft(2, '0');
                dataModel.LENGTH = String.IsNullOrEmpty(iLength) ? "0" : iLength;
                dataModel.WIDTH = String.IsNullOrEmpty(iWidth) ? "0" : iWidth;
                dataModel.HEIGHT = String.IsNullOrEmpty(iHeight) ? "0" : iHeight;
                dataModel.CHANGED_BY = iChangedBy;
                dataModel.CHANGED_DT = iChangedDt;
                dataModel.USER_ID = getpE_UserId.Username;

                listDataModel.Add(dataModel);
            }

            String resultMessage = RackAddressMasterRepository.Instance.SaveData(listDataModel);

            return Json(new { success = resultMessage.Split('|')[0].Equals("E") ? "false" : "true", messages = resultMessage.Split('|')[1] }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Download Data
        public ActionResult DownloadData(object sender, EventArgs e, String param)
        {
            /*Modified By fid.muhajir*/
            string rExtract = string.Empty;
            string filename = "RACK_ADDRESS";
            string fileTemplateLocation = HttpContext.Request.MapPath(fileTemplateDownloadLocation);

            #region Get Data
            var paramDecode = System.Web.Helpers.Json.Decode(param);
            sRackAddressMaster searchModel = new sRackAddressMaster();
            searchModel.RackAddressCode = paramDecode.RackAddressCode;
            searchModel.ZoneCode = paramDecode.ZoneCode;
            searchModel.AisleCode = paramDecode.AisleCode;
            searchModel.RackCode = paramDecode.RackCode;
            searchModel.DeleteFlag = paramDecode.DeleteFlag;

            List<mRackAddressMaster> rows = RackAddressMasterRepository.Instance.SearchData(searchModel);
            #endregion


            #region Checking Template
            FileInfo info = new FileInfo(fileTemplateLocation);
            if (!info.Exists)
            {
                rExtract = "Template|RackAddressMasterDownload";
                return Json(new { success = false, messages = rExtract, status = "File" }, JsonRequestBehavior.AllowGet);
            }

            if (rows==null)
            {
                return Json(new { success = false, messages = "", code = "Data" }, JsonRequestBehavior.AllowGet);
            }

            FileStream ftmp = null;
            try
            {
                ftmp = new FileStream(fileTemplateLocation, FileMode.Open, FileAccess.Read);
            }
            catch
            {
            }
            if (ftmp == null)
            {
                rExtract = "Template|RackAddressMasterDownload";
                return Json(new { success = false, messages = rExtract, status = "Write" }, JsonRequestBehavior.AllowGet);
            }
            #endregion


            #region Create File
            FileStream stream = new FileStream(fileTemplateLocation, FileMode.Open, FileAccess.Read);
            XSSFWorkbook workbook = new XSSFWorkbook(stream);
            XSSFSheet sheet = (XSSFSheet)workbook.GetSheet("Report");

            #region Fill Header
            NPOIUtil.SetCellValueX(sheet, 6, 8, DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
            NPOIUtil.SetCellValueX(sheet, 7, 8, getpE_UserId.Username);

            NPOIUtil.SetCellValueX(sheet, 9, 8, String.IsNullOrEmpty(searchModel.RackAddressCode) ? "ALL" : searchModel.RackAddressCode);
            NPOIUtil.SetCellValueX(sheet, 10, 8, String.IsNullOrEmpty(searchModel.ZoneCode) ? "ALL" : searchModel.ZoneCode);
            NPOIUtil.SetCellValueX(sheet, 11, 8, String.IsNullOrEmpty(searchModel.AisleCode) ? "ALL" : searchModel.AisleCode);
            NPOIUtil.SetCellValueX(sheet, 12, 8, String.IsNullOrEmpty(searchModel.RackCode) ? "ALL" : searchModel.RackCode);
            NPOIUtil.SetCellValueX(sheet, 13, 8, String.IsNullOrEmpty(searchModel.DeleteFlag) ? "ALL" : searchModel.DeleteFlag.Equals("0") ? "N" : "Y");
            #endregion

            #region Fill Grid
            int startRow = 17;
            int rowNo = 1;
            int rowIndex = startRow;
            foreach (mRackAddressMaster row in rows)
            {
                if (rowIndex > startRow)
                {
                    NPOIUtil.copyCellX(sheet, sheet, startRow, startRow, 0, 65, rowIndex, 0);
                }
                NPOIUtil.SetCellValueX(sheet, rowIndex, 1, rowNo);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 3, row.RACK_ADDRESS_CD);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 10, row.ZONE_CD);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 14, row.AISLE_CD);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 18, row.RACK_CD);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 22, row.ROW);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 26, row.COLUMN);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 30, row.LENGTH);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 33, row.WIDTH);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 36, row.HEIGHT);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 39, row.DELETE_FLAG);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 43, row.CREATED_BY);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 47, row.CREATED_DT);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 54, row.CHANGED_BY);
                NPOIUtil.SetCellValueX(sheet, rowIndex, 58, row.CHANGED_DT);

                rowIndex++;
                rowNo++;
            }
            workbook.SetPrintArea(0, 0, 65, 0, rowIndex);
            #endregion

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            stream.Close();

            String dateTime = DateTime.Now.ToString("ddMMyyyyHHmm");
            filename = filename + "_" + dateTime + ".xlsx";

            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            #endregion
            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Upload Data
        private class UploadControlHelper
        {
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                //AllowedFileExtensions = new string[] { ".xlsx" },
                AllowedFileExtensions = new string[] { ".csv" },
                MaxFileSize = 20971520
            };
        }

        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadRackAddressMaster", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            Guid processID = Guid.NewGuid();
            long pid = 0;
            String fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            String tb_t_name = "spex.TB_T_RACK_ADDRESS";
            String processName = "Rack Address Master Upload Batch";
            String resultMessage = String.Empty;

            Lock lc = new Lock();
            int IsLock = lc.is_lock(batchID);
            if (IsLock > 0)
            {
                resultMessage = "E|" + messageError.getMsgText("MSPXS2036ERR");
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                if (RackAddressMasterRepository.Instance.DeleteTempData().Equals("SUCCESS"))
                {
                    Log lg = new Log();
                    string msg = "MSPXS2024INF| INF |" + new MessagesString().getMsgText("MSPXS2024INF").Replace("{0}", processName);
                    pid = lg.createLog(msg, getpE_UserId.Username, processName, pid, moduleID, batchID);

                    lc.CREATED_BY = getpE_UserId.Username;
                    lc.CREATED_DT = DateTime.Now;
                    lc.PROCESS_ID = pid;
                    lc.FUNCTION_ID = batchID;
                    lc.LOCK_REF = batchID;
                    lc.LockFunction(lc);

                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string uploadDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);
                    var pathfile = Path.Combine(uploadDir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    e.UploadedFile.SaveAs(pathfile);

                    String bulkSts = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    if (bulkSts == string.Empty || bulkSts == "SUCCESS")
                    {
                        String processSts = RackAddressMasterRepository.Instance.BackgroundProsessUploadSave(moduleID, batchID, pid, getpE_UserId.Username);
                        if (processSts == "SUCCESS")
                        {
                            resultMessage = "I|" + messageError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2] + "|" + pid.ToString();
                        }
                    }
                    else
                    {
                        resultMessage = "E|" + bulkSts.Split('|')[1];
                    }
                    lc.UnlockFunction(batchID);
                }
                else
                {
                    resultMessage = "E|" + RackAddressMasterRepository.Instance.DeleteTempData();
                }
            }
            else
            {
                resultMessage = "E|File is not .csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
            {
                e.CallbackData = resultMessage;
            }
        }
        #endregion
        #endregion

    }
}
