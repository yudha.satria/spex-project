﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class CaseMasterController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mCaseMaster caseMaster = new mCaseMaster();

        public CaseMasterController()
        {
            Settings.Title = "Case Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
       // public ActionResult CaseGridCallback(string pCASE_TYPE, string pDESCRIPTION)
            //MODIF AGI 2017-08-10
        public ActionResult CaseGridCallback(string pCASE_TYPE, string pDESCRIPTION,string pCASE_GROUPING)
        {
            List<mCaseMaster> model = new List<mCaseMaster>();

            //REMAKS AGI 2017-08-10
            //model = caseMaster.getList(pCASE_TYPE, pDESCRIPTION);
            model = caseMaster.getList(pCASE_TYPE, pDESCRIPTION, pCASE_GROUPING);

            return PartialView("CaseGrid", model);
        }

        //public ActionResult EditCase(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT, string pDESCRIPTION)
        //modif agi 2017-08-10
        public ActionResult EditCase(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT, string pDESCRIPTION, string pCASE_GROUPING)    
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            //string resultMessage = caseMaster.Update(pCASE_TYPE, pCASE_NET_WEIGHT, pCASE_LENGTH, pCASE_WIDTH, pCASE_HEIGHT, pDESCRIPTION, getpE_UserId.Username);
            //modif agi 2017-08-10
            string resultMessage = caseMaster.Update(pCASE_TYPE, pCASE_NET_WEIGHT, pCASE_LENGTH, pCASE_WIDTH, pCASE_HEIGHT, pDESCRIPTION, getpE_UserId.Username, pCASE_GROUPING);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        //public ActionResult AddCase(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT,string pDESCRIPTION)
        //MODIF AGI 2017-08-10
        public ActionResult AddCase(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT, string pDESCRIPTION,string pCASE_GROUPING) 
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
           // string  msg = "";

            string resultMessage = caseMaster.Add(pCASE_TYPE, pCASE_NET_WEIGHT, pCASE_LENGTH, pCASE_WIDTH, pCASE_HEIGHT, pDESCRIPTION, getpE_UserId.Username,pCASE_GROUPING);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteCase(string pCASE_TYPE)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = caseMaster.Delete(pCASE_TYPE, getpE_UserId.Username);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        private string GetDateSQL(string dt)
        {
            if (dt == "01.01.0100")
                return "";

            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime date = new DateTime(year, month, day);

            return date.ToString("yyyy-MM-dd");
        }

        //public void DownloadData_ByParameter(object sender, EventArgs e, string pCASE_TYPE, string pDESCRIPTION)
        //MODIF AGI 2017-07-10
        public void DownloadData_ByParameter(object sender, EventArgs e, string pCASE_TYPE, string pDESCRIPTION,string pCASE_GROUPING)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/CaseMasterDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;
            styleContent3.DataFormat = workbook.CreateDataFormat().GetFormat("#0.0");

            //add agi 2017-08-28
            ICellStyle styleContent4 = workbook.CreateCellStyle();
            styleContent4.VerticalAlignment = VerticalAlignment.Top;
            styleContent4.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent4.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent4.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent4.Alignment = HorizontalAlignment.Right;
            styleContent4.DataFormat = workbook.CreateDataFormat().GetFormat("#0.000");


            ISheet sheet = workbook.GetSheet("Case Master");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "CaseMaster" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(pCASE_TYPE);
            sheet.GetRow(9).GetCell(2).SetCellValue(pDESCRIPTION);
            sheet.GetRow(10).GetCell(2).SetCellValue(pCASE_GROUPING);

            int row = 16;
            int rowNum = 1;
            IRow Hrow;

            List<mCaseMaster> model = new List<mCaseMaster>();

            model = caseMaster.getList(pCASE_TYPE, pDESCRIPTION, pCASE_GROUPING);


            foreach (mCaseMaster result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.CASE_TYPE);
                //remaks agi 2017-08-10                
                //Hrow.CreateCell(3).SetCellValue(result.DESCRIPTION);
                //if (result.CASE_NET_WEIGHT != null)
                //    Hrow.CreateCell(4).SetCellValue((double)result.CASE_NET_WEIGHT);
                //else
                //    Hrow.CreateCell(4).SetCellValue("");
                //if (result.CASE_LENGTH != null)
                //    Hrow.CreateCell(5).SetCellValue((double)result.CASE_LENGTH);
                //else
                //    Hrow.CreateCell(5).SetCellValue("");
                //if (result.CASE_WIDTH != null)
                //    Hrow.CreateCell(6).SetCellValue((double)result.CASE_WIDTH);
                //else
                //    Hrow.CreateCell(6).SetCellValue("");
                //if (result.CASE_HEIGHT != null)
                //    Hrow.CreateCell(7).SetCellValue((double)result.CASE_HEIGHT);
                //else
                //    Hrow.CreateCell(7).SetCellValue("");
                //if (result.MEASUREMENT != null)
                //    Hrow.CreateCell(8).SetCellValue((double)result.MEASUREMENT);
                //else
                //    Hrow.CreateCell(8).SetCellValue("");
                //Hrow.CreateCell(9).SetCellValue(result.CREATED_BY);
                //Hrow.CreateCell(10).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                //Hrow.CreateCell(11).SetCellValue(result.CHANGED_BY);
                //Hrow.CreateCell(12).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                //add agi 2017-08-10
                Hrow.CreateCell(3).SetCellValue(result.CASE_GROUPING);
                Hrow.CreateCell(4).SetCellValue(result.DESCRIPTION);
                if (result.CASE_NET_WEIGHT != null)
                    Hrow.CreateCell(5).SetCellValue((double)result.CASE_NET_WEIGHT);
                else
                    Hrow.CreateCell(5).SetCellValue("");
                if (result.CASE_LENGTH != null)
                    Hrow.CreateCell(6).SetCellValue((double)result.CASE_LENGTH);
                else
                    Hrow.CreateCell(6).SetCellValue("");
                if (result.CASE_WIDTH != null)
                    Hrow.CreateCell(7).SetCellValue((double)result.CASE_WIDTH);
                else
                    Hrow.CreateCell(7).SetCellValue("");
                if (result.CASE_HEIGHT != null)
                    Hrow.CreateCell(8).SetCellValue((double)result.CASE_HEIGHT);
                else
                    Hrow.CreateCell(8).SetCellValue("");
                if (result.MEASUREMENT != null)
                    Hrow.CreateCell(9).SetCellValue((double)result.MEASUREMENT);
                else
                    Hrow.CreateCell(9).SetCellValue("");
                Hrow.CreateCell(10).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(11).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(12).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(13).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));


                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                //remaks agi 2017-08-10
                //Hrow.GetCell(3).CellStyle = styleContent2;
                //Hrow.GetCell(4).CellStyle = styleContent3;
                //Hrow.GetCell(5).CellStyle = styleContent3;
                //Hrow.GetCell(6).CellStyle = styleContent3;
                //Hrow.GetCell(7).CellStyle = styleContent3;
                //Hrow.GetCell(8).CellStyle = styleContent3;
                //Hrow.GetCell(9).CellStyle = styleContent;
                //Hrow.GetCell(10).CellStyle = styleContent2;
                //Hrow.GetCell(11).CellStyle = styleContent;
                //Hrow.GetCell(12).CellStyle = styleContent2;

                //add agi 2017-08-10
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent4;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent;
                Hrow.GetCell(13).CellStyle = styleContent2;

                row++;
                rowNum++;
            }


            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        public void DownloadData_ByTicked(object sender, EventArgs e, string pCASE_KEY, string pCASE_TYPE, string pDESCRIPTION,string pCASE_GROUPING)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/CaseMasterDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;
            styleContent3.DataFormat = workbook.CreateDataFormat().GetFormat("#0.0");

            //add agi 2017-08-28
            ICellStyle styleContent4 = workbook.CreateCellStyle();
            styleContent4.VerticalAlignment = VerticalAlignment.Top;
            styleContent4.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent4.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent4.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent4.Alignment = HorizontalAlignment.Right;
            styleContent4.DataFormat = workbook.CreateDataFormat().GetFormat("#0.000");

            ISheet sheet = workbook.GetSheet("Case Master");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "CaseMaster" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(pCASE_TYPE);
            sheet.GetRow(9).GetCell(2).SetCellValue(pDESCRIPTION);
            sheet.GetRow(10).GetCell(2).SetCellValue(pCASE_GROUPING);
            int row = 16;
            int rowNum = 1;
            IRow Hrow;

            List<mCaseMaster> model = new List<mCaseMaster>();

            model = caseMaster.getDownloadList(pCASE_KEY);


            foreach (mCaseMaster result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.CASE_TYPE);
                //REMAKS AGI 2017-08-10
                //Hrow.CreateCell(3).SetCellValue(result.DESCRIPTION);
                //if (result.CASE_NET_WEIGHT != null)
                //    Hrow.CreateCell(4).SetCellValue((double)result.CASE_NET_WEIGHT);
                //else
                //    Hrow.CreateCell(4).SetCellValue("");
                //if (result.CASE_LENGTH != null)
                //    Hrow.CreateCell(5).SetCellValue((double)result.CASE_LENGTH);
                //else
                //    Hrow.CreateCell(5).SetCellValue("");
                //if (result.CASE_WIDTH != null)
                //    Hrow.CreateCell(6).SetCellValue((double)result.CASE_WIDTH);
                //else
                //    Hrow.CreateCell(6).SetCellValue("");
                //if (result.CASE_HEIGHT != null)
                //    Hrow.CreateCell(7).SetCellValue((double)result.CASE_HEIGHT);
                //else
                //    Hrow.CreateCell(7).SetCellValue("");
                //if (result.MEASUREMENT != null)
                //    Hrow.CreateCell(8).SetCellValue((double)result.MEASUREMENT);
                //else
                //    Hrow.CreateCell(8).SetCellValue("");
                //Hrow.CreateCell(9).SetCellValue(result.CREATED_BY);
                //Hrow.CreateCell(10).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                //Hrow.CreateCell(11).SetCellValue(result.CHANGED_BY);
                //Hrow.CreateCell(12).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                //ADD AGI 2017-08-10
                Hrow.CreateCell(3).SetCellValue(result.DESCRIPTION);
                Hrow.CreateCell(4).SetCellValue(result.DESCRIPTION);
                if (result.CASE_NET_WEIGHT != null)
                    Hrow.CreateCell(5).SetCellValue((double)result.CASE_NET_WEIGHT);
                else
                    Hrow.CreateCell(5).SetCellValue("");
                if (result.CASE_LENGTH != null)
                    Hrow.CreateCell(6).SetCellValue((double)result.CASE_LENGTH);
                else
                    Hrow.CreateCell(6).SetCellValue("");
                if (result.CASE_WIDTH != null)
                    Hrow.CreateCell(7).SetCellValue((double)result.CASE_WIDTH);
                else
                    Hrow.CreateCell(7).SetCellValue("");
                if (result.CASE_HEIGHT != null)
                    Hrow.CreateCell(8).SetCellValue((double)result.CASE_HEIGHT);
                else
                    Hrow.CreateCell(8).SetCellValue("");
                if (result.MEASUREMENT != null)
                    Hrow.CreateCell(9).SetCellValue((double)result.MEASUREMENT);
                else
                    Hrow.CreateCell(9).SetCellValue("");
                Hrow.CreateCell(10).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(11).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(12).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(13).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                //remaks agi 2017-08-10
                //Hrow.GetCell(3).CellStyle = styleContent2;
                //Hrow.GetCell(4).CellStyle = styleContent3;
                //Hrow.GetCell(5).CellStyle = styleContent3;
                //Hrow.GetCell(6).CellStyle = styleContent3;
                //Hrow.GetCell(7).CellStyle = styleContent3;
                //Hrow.GetCell(8).CellStyle = styleContent3;
                //Hrow.GetCell(9).CellStyle = styleContent;
                //Hrow.GetCell(10).CellStyle = styleContent2;
                //Hrow.GetCell(11).CellStyle = styleContent;
                //Hrow.GetCell(12).CellStyle = styleContent2;

                //add agi 2017-08-10
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent3;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent3;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent3;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent;
                Hrow.GetCell(13).CellStyle = styleContent2;
                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
    }
}

