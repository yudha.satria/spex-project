﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;
using NPOI.HSSF.UserModel;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class WarehouseInquiryController : PageController
    {

        //Model Maintenance Warehouse Master
        mWarehouseInquiry ms = new mWarehouseInquiry();
        MessagesString msgError = new MessagesString();


        public string moduleID = "SPX17";
        public string functionID = "SPX170200";

        public WarehouseInquiryController()
        {
            Settings.Title = "Warehouse Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }


        public void GetComboboxStatus()
        {
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            List<mSystemMaster> model = new List<mSystemMaster>();
            model = (new mSystemMaster()).GetComboBoxList("PICKING_LIST_FLAG");
            model.Insert(0, item);

            ViewData["PICKING_LIST_FLAG"] = model;
        }


        public void GetComboboxOrderType()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            model = (new mSystemMaster()).GetComboBoxList("TPCAP_ORDER_TYPE");
            model.Insert(0, item);
            ViewData["ORDER_TYPE"] = model;
        }

        protected override void Startup()
        {

            //Call error message 

            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();

            /*No data found*/
            ViewBag.MSPX00001ERR = msgError.getMsgText("MSPXS2006ERR");
            /*Cannot find template file {0} on folder {1}.*/
            ViewBag.MSPXS2034ERR = msgError.getMsgText("MSPXS2034ERR");
            /*Error writing file = {0}. Reason = {1}.*/
            ViewBag.MSPXS2035ERR = msgError.getMsgText("MSPXS2035ERR");
            /*Are you sure you want to download this data?*/
            ViewBag.MSPXS2021INF = msgError.getMsgText("MSPXS2021INF");

            getpE_UserId = (User)ViewData["User"];
            GetComboboxStatus();
            GetComboboxOrderType();

        }

        public ActionResult WarehouseInquiryCallBack(string pORDER_NO, string pITEM_NO, string pPART_NO, string pORDER_TYPE, string pBUYER_PD, string pORDER_DATE_FROM, string pORDER_DATE_TO, string pSTATUS_FLAG, string Mode)
        {
            List<mWarehouseInquiry> model = new List<mWarehouseInquiry>();
            if (Mode.Equals("Search"))
            {
                pORDER_DATE_FROM = reFormatDate(pORDER_DATE_FROM);
                pORDER_DATE_TO = reFormatDate(pORDER_DATE_TO);
                model = ms.getListWarehouseInquiry(pORDER_NO, pITEM_NO, pPART_NO, pORDER_TYPE, pBUYER_PD, pORDER_DATE_FROM, pORDER_DATE_TO, pSTATUS_FLAG);
            }

            GetComboboxOrderType();
            GetComboboxStatus();
            return PartialView("WarehouseInquiryGrid", model);
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }

            return result;
        }

        public string reFormatDate_Download(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }

            return result;
        }

        #region Function Download
        //function download 
        public ActionResult DownloadWarehouseInquiry(object sender, EventArgs e, string pORDER_NO, string pITEM_NO, string pPART_NO, string pORDER_TYPE, string pBUYER_PD, string pORDER_DATE_FROM, string pORDER_DATE_TO, string pSTATUS_FLAG)
        {
            string rExtract = string.Empty;
            string filename = "Warehouse_Inquiry_Download.xls";
            string directory = "Template";
            string filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);

            List<mWarehouseInquiry> model = new List<mWarehouseInquiry>();
            pORDER_DATE_FROM = reFormatDate(pORDER_DATE_FROM);
            pORDER_DATE_TO = reFormatDate(pORDER_DATE_TO);
            model = ms.getListWarehouseInquiry(pORDER_NO, pITEM_NO, pPART_NO, pORDER_TYPE, pBUYER_PD, pORDER_DATE_FROM, pORDER_DATE_TO, pSTATUS_FLAG);



            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            else if (model.Count() == 0)
            {
                lError = 1;
                status = "Data";
                rExtract = model.Count().ToString();
            }
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1)
                return Json(new { success = false, messages = rExtract, status = status }, JsonRequestBehavior.AllowGet);
            #endregion 

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Warehouse Inquiry");
            string date = DateTime.Now.ToString("ddMMyyyyHHmm");
            filename = "Warehouse_Inquiry_Master_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            int row = 10;
            int rowNum = 1;
            IRow Hrow;


            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.BUYER_PD);
                Hrow.CreateCell(3).SetCellValue(result.TMAP_ORDER_NO);
                Hrow.CreateCell(4).SetCellValue(result.TMAP_ORDER_DATE);
                Hrow.CreateCell(5).SetCellValue(result.TPCAP_ORDER_TYPE);
                Hrow.CreateCell(6).SetCellValue(result.ORDER_TYPE);
                Hrow.CreateCell(7).SetCellValue(result.TRANSP_CD);
                Hrow.CreateCell(8).SetCellValue(result.TMAP_ITEM_NO);
                Hrow.CreateCell(9).SetCellValue(result.TMAP_PART_NO);
                Hrow.CreateCell(10).SetCellValue(result.PART_NAME);
                Hrow.CreateCell(11).SetCellValue(result.TMAP_ORDER_QTY.ToString());
                Hrow.CreateCell(12).SetCellValue(result.ALLOCATION_QTY.ToString());
                Hrow.CreateCell(13).SetCellValue(result.REMAIN_QTY.ToString());
                Hrow.CreateCell(14).SetCellValue(result.RACK_ADDRESS_CD);
                Hrow.CreateCell(15).SetCellValue(result.PICKING_LIST_FLAG);

                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent3;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent3;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            MemoryStream memory = new MemoryStream();
            workbook.Write(memory);
            ftmp.Close();
            Response.BinaryWrite(memory.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Generate

        public ActionResult GeneratePickingList(string pCheckBox, string iListKeyGenerate)
        {
            string pOderNo = "";
            string pPartNO = "";
            string pItemNo = "";
            mWarehouseInquiry WH = new mWarehouseInquiry();
            if (pCheckBox != "true")
            {
                List<mWarehouseInquiry> listDataModel = new List<mWarehouseInquiry>();
                mWarehouseInquiry dataModel = new mWarehouseInquiry();
                var listKeyHeader = iListKeyGenerate.Split(';');
                int i = 0;
                foreach (var lsKeyHeader in listKeyHeader)
                {
                    var listKeyDetail = lsKeyHeader.Split('|');
                    dataModel = new mWarehouseInquiry();
                    dataModel.TMAP_ORDER_NO = listKeyDetail[0].ToString();
                    dataModel.TMAP_PART_NO = listKeyDetail[1].ToString();
                    dataModel.TMAP_ITEM_NO = listKeyDetail[2].ToString();
                    listDataModel.Add(dataModel);
                    if (i == 0)
                    {
                        pOderNo = listKeyDetail[0].ToString();
                        pPartNO = listKeyDetail[1].ToString();
                        pItemNo = listKeyDetail[2].ToString();
                    }
                    else
                    {
                        pOderNo = pOderNo + ";" + listKeyDetail[0].ToString();
                        pPartNO = pPartNO + ";" + listKeyDetail[1].ToString();
                        pItemNo = pItemNo + ";" + listKeyDetail[2].ToString(); 
                    }
                    i++;
                } 
                  
            } 
            string[] r = null;
            //string isError = "N";
            string processName = "PICKING LIST CREATION BATCH";
            string resultMessage = "";
            long pid = 0;
            var success = true;

            Lock L = new Lock();
            string functionIDUpload = "SPX170200";
            int IsLock = L.is_lock(functionIDUpload);
            if (IsLock > 0)
            {
                resultMessage = "E|" + msgError.getMsgText("MSPXS2036ERR");
            }
            else if (IsLock == 0)
            {
                string user_id = getpE_UserId.Username;
                Log lg = new Log();
                string msg = "MSPX00035INF|INF|" + new MessagesString().getMsgText("MSPX00035INF").Replace("{0}", processName);
                pid = lg.createLog(msg, user_id, "Picking List Creation Batch", pid, moduleID, functionIDUpload);

                resultMessage = WH.GeneratePickingList(pCheckBox, pOderNo, pPartNO, pItemNo, pid);
                if (resultMessage == "SUCCES")
                    resultMessage = "I|" + msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2] + "|" + pid.ToString();
            }
            else
            {
                resultMessage = "E|" + resultMessage.Split('|')[1];
                //resultMessage = "E|" + resultMessage.Split('|')[1];
            }
            L.UnlockFunction(functionIDUpload);
            return Json(new { success = success, messages = resultMessage }, JsonRequestBehavior.AllowGet); 

        }

        #endregion


    }
}
