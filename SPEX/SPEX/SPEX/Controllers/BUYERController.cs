﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DevExpress.Web.ASPxGridView;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class BUYERController : PageController
    {
        //
        // GET: /BuyerPD/
        //mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();

        public BUYERController()
        {
            Settings.Title = "Buyer Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //CallbackComboPriceTerm();

            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("BUYER Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        public ActionResult BUYERCallBack(string BUYER_CD, string BUYER_NAME)
        {
            BUYER BYR = new BUYER();
            BYR.BUYER_CD = BUYER_CD;
            BYR.BUYER_NAME = BUYER_NAME;
            List<BUYER> model = new List<BUYER>();
            model = BYR.getBUYER(BYR);

            return PartialView("_BUYERGrid", model);
        }

        protected void gridView_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel)
            {
                e.Visible = false;
            }
        }

        public ActionResult DeleteBUYER(string PARAMS)
        {
            BUYER byr = new BUYER();

            string[] r = null;
            string resultMessage = byr.DeleteData(PARAMS, getpE_UserId.Username);

            r = resultMessage.Split('|');

            if (r[0].ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            
            return null;
        }
        //#endregion 


        public ActionResult AddNewBUYER(string Data)
        {

            string[] r = null;
            BUYER byr = new BUYER();
            byr = System.Web.Helpers.Json.Decode<BUYER>(Data);
            byr.CREATED_BY = getpE_UserId.Username;
            byr.CREATED_DT = DateTime.Now;
            string resultMessage = byr.SaveData(byr);
            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion

        //#region Function Edit -
        public ActionResult UpdateBUYER(string Data)
        {
            string[] r = null;
            BUYER byr = new BUYER();
            byr = System.Web.Helpers.Json.Decode<BUYER>(Data);
            byr.CHANGED_BY = getpE_UserId.Username;
            byr.CHANGED_DT = DateTime.Now;
            string resultMessage = byr.UpdateData(byr);
            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion

        public ActionResult ShowPopUpBuyer(string Mode, string Data)
        {
            BUYER Model = new BUYER();
            Common cmn = new Common();
            ViewData["PortLoadingByAir"] = cmn.GetPortLoadingByAir();
            ViewData["GetPortLoadingBySea"] = cmn.GetPortLoadingBySea();
            ViewData["PORT_DISCHARGE_By_Air"] = cmn.Get_PORT_DISCHARGE_By_Air();
            ViewData["PORT_DISCHARGE_By_Sea"] = cmn.Get_PORT_DISCHARGE_By_Sea();
            if (Mode == "Edit")
            {
                Model = System.Web.Helpers.Json.Decode<BUYER>(Data);
                Model = Model.getBUYER(Model).First();
            }
            return PartialView("_Buyer_Form", Model);
        }

    }
}
