﻿///
/// <author>ho_shin_an@yahoo.com</author>
/// <summary>
/// Toyota .Net Development Kit
/// Copyright (c) Toyota Motor Manufacturing Indonesia, All Right Reserved.
/// </summary>
/// 
// Default Using
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
//Default Toyota Model
using Toyota.Common.Web.Platform;
using Toyota.Common.Web.Platform.Starter.Models;
using Toyota.Common.Credential;
using Toyota.Common.Database;
//Default Devexpress Model
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using NPOI.POIFS.FileSystem;
using NPOI.HPSF;
using System.Web.UI;

namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class PartController : PageController
    {
        /*
         * By default, TDK will finds Login controller when the application authentication enabled.
         * You can override this behaviour at Global.asax as follows:
         *      ApplicationSettings.Instance.Security.LoginController = [Your desired controller name]
         */
        mPart MasterPart = new mPart();

        public PartController()
        {
            Settings.Title = "Part Master";
        }

        public string getpE_PartNo
        {
            get
            {
                if (Session["getpE_PartNo"] != null)
                {
                    return Session["getpE_PartNo"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_PartNo"] = value;
            }
        }

        public string getpE_PartName
        {
            get
            {
                if (Session["getpE_PartName"] != null)
                {
                    return Session["getpE_PartName"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_PartName"] = value;
            }
        }
        
        public string getpE_Model
        {
            get
            {
                if (Session["getpE_Model"] != null)
                {
                    return Session["getpE_Model"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Model"] = value;
            }
        }

        public string getpE_CarFamilyCd
        {
            get
            {
                if (Session["getpE_CarFamilyCd"] != null)
                {
                    return Session["getpE_CarFamilyCd"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_CarFamilyCd"] = value;
            }
        }
        
        public string getpE_PaintType
        {
            get
            {
                if (Session["getpE_PaintType"] != null)
                {
                    return Session["getpE_PaintType"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_PaintType"] = value;
            }
        }
        
        public string getpE_PartSpec
        {
            get
            {
                if (Session["getpE_PartSpec"] != null)
                {
                    return Session["getpE_PartSpec"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_PartSpec"] = value;
            }
        }
        
        public string getpE_TaktTime
        {
            get
            {
                if (Session["getpE_TaktTime"] != null)
                {
                    return Session["getpE_TaktTime"].ToString();
                }
                else return "0";
            }
            set
            {
                Session["getpE_TaktTime"] = value;
            }
        }

        public string getpE_PcsKbn
        {
            get
            {
                if (Session["getpE_PcsKbn"] != null)
                {
                    return Session["getpE_PcsKbn"].ToString();
                }
                else return "0";
            }
            set
            {
                Session["getpE_PcsKbn"] = value;
            }
        }
        
        public string getpE_LatestOrder
        {
            get
            {
                if (Session["getpE_LatestOrder"] != null)
                {
                    return Session["getpE_LatestOrder"].ToString();
                }
                else return "01-01-1753";
            }
            set
            {
                Session["getpE_LatestOrder"] = value;
            }
        }

        public string getpE_Info
        {
            get
            {
                if (Session["getpE_Info"] != null)
                {
                    return Session["getpE_Info"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Info"] = value;
            }
        }

        public ActionResult _PartialAddMaster(string p_UPLOAD)
        {
            ViewBag.UPLOAD = p_UPLOAD;
            return View();
        }

        public ActionResult SPN311PartialUpdateMaster(string p_UPLOAD)
        {
            ViewBag.UPLOAD = p_UPLOAD;
            return View();
        }

        public ActionResult Callbacks()
        {
            return _PartialAddMaster("Callbacks");
        }

        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("I_Browse", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xls" },
                MaxFileSize = 209751520
            };
        }

        public const string UploadDirectory = "Content/FileUploadResult/";
        public const string TemplateFName = "PartMaster";

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string pathfile = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);
                var resultFilePath = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + Path.GetExtension(e.UploadedFile.FileName));
                e.UploadedFile.SaveAs(resultFilePath);
                IUrlResolutionService urlResolver = sender as IUrlResolutionService;
                if (urlResolver != null)
                {
                    e.CallbackData = urlResolver.ResolveClientUrl(resultFilePath);
                }

                string result = EXcelToSQL(resultFilePath);
            }
        }

        public string EXcelToSQL(string path)
        {
            try
            {
                string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\""; // XLS
                //string strConn = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path); // XLSX
                string result;

                DataSet output = new DataSet();
                using (OleDbConnection conn = new OleDbConnection(strConn))
                {
                    conn.Open();
                    DataTable schemaTable = conn.GetOleDbSchemaTable(
                    OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    string Part_No = "";
                    string Part_Name = "";
                    string Car_Family_Name = "";
                    //string Model = "";
                    string Paint_Type = "";
                    string Part_Spec = "";
                    string Takt_Time = "";
                    string PcsKbn = "";
                    string Latest_Order = "";

                    string sheet = "[Sheet1$]";
                    OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + sheet, conn);
                    cmd.CommandType = CommandType.Text;
                    DataTable outputTable = new DataTable(sheet);
                    output.Tables.Add(outputTable);
                    new OleDbDataAdapter(cmd).Fill(outputTable);
                    int x = outputTable.Rows.Count;

                    mProcessID ModProcessID = new mProcessID();
                    string ProcessId = ModProcessID.GetProcessID();

                    for (int i = 0; i < outputTable.Rows.Count; i++)
                    {
                        Part_No = outputTable.Rows[i][0].ToString();
                        Part_Name = outputTable.Rows[i][1].ToString();
                        Car_Family_Name = outputTable.Rows[i][2].ToString();
                        //Model = outputTable.Rows[i][2].ToString();
                        Paint_Type = outputTable.Rows[i][3].ToString();
                        Part_Spec = outputTable.Rows[i][4].ToString();
                        Takt_Time = outputTable.Rows[i][5].ToString();
                        PcsKbn = outputTable.Rows[i][6].ToString();
                        Latest_Order = outputTable.Rows[i][7].ToString();
                        result = MasterPart.SPN311SaveUploadData(Part_No, Part_Name, Car_Family_Name, Paint_Type, Part_Spec, Takt_Time, PcsKbn, Latest_Order, "pid.kevin", ProcessId);
                    }

                    result = MasterPart.SPN311CheckUploadedData("pid.kevin", ProcessId);

                    MasterPart.SPN311DeleteUploadedData(ProcessId);

                    if (result.Contains("successfully"))
                        getpE_Info = "Process UPLOAD PART MASTER finish successfully";
                    else
                        getpE_Info = "Error : please check Process ID " + result;
                }
                return "Conversion Success";
            }
            catch
            {
                return "Conversion Error";
            }
        }

        protected override void Startup()
        {
            List<mPart> model = MasterPart.SPN311GetListMasterPart();
            ViewData["MasterPart"] = model;

            ViewBag.FromDate = DateTime.Now;
        }

        public void SPN311EditData(string pE_PartNo, string pE_PartName, string pE_CarFamilyCd, string pE_PaintType, string pE_PartSpec, string pE_TaktTime, string pE_PcsKbn, string pE_LatestOrder)
        {
            string dtPLatestOrder = "01-01-1753";
            string intTaktTime = "0";
            string intPcsKbn = "0";

            if (!String.IsNullOrEmpty(pE_LatestOrder))
            {
                string strValidFrom = pE_LatestOrder.Substring(5, 2) + "-" + pE_LatestOrder.Substring(8, 2) + "-" + pE_LatestOrder.Substring(0, 4);
                dtPLatestOrder = Convert.ToDateTime(strValidFrom).ToString();
            }
            if (!String.IsNullOrEmpty(pE_TaktTime))
            {
                intTaktTime = Convert.ToInt32(pE_TaktTime).ToString();
            } 
            if (!String.IsNullOrEmpty(pE_LatestOrder))
            {
                intPcsKbn = Convert.ToInt32(pE_PcsKbn).ToString();
            }
            
            getpE_PartNo = pE_PartNo;
            getpE_PartName = pE_PartName;
            getpE_CarFamilyCd = pE_CarFamilyCd;
            getpE_PaintType = pE_PaintType;
            getpE_PartSpec = pE_PartSpec;
            getpE_TaktTime = intTaktTime;
            getpE_PcsKbn = intPcsKbn;
            getpE_LatestOrder = dtPLatestOrder;
        }

        public void SPN311DownloadData(object sender, EventArgs e, string p_Part_No, string p_Car_Family_Cd, string p_Paint_Type)
        {
            List<mPart> model = new List<mPart>();

            if (String.IsNullOrEmpty(p_Part_No.ToString()) && String.IsNullOrEmpty(p_Car_Family_Cd.ToString()) && String.IsNullOrEmpty(p_Paint_Type.ToString()))
            {
                model = MasterPart.SPN311GetListMasterPartSpecific("", "", "");
            }
            else
            {
                model = MasterPart.SPN311GetListMasterPartSpecific(p_Part_No, p_Car_Family_Cd, p_Paint_Type);
            }

            try
            {
                string filename = "";
                string filesTmp = HttpContext.Request.MapPath("~/Template/Part_Master_Download.xls");
                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

                ICellStyle styleContent1 = workbook.CreateCellStyle();
                styleContent1.VerticalAlignment = VerticalAlignment.TOP;
                styleContent1.BorderLeft = BorderStyle.THIN;
                styleContent1.BorderRight = BorderStyle.THIN;
                styleContent1.BorderBottom = BorderStyle.THIN;

                ICellStyle styleContent2 = workbook.CreateCellStyle();
                styleContent2.VerticalAlignment = VerticalAlignment.TOP;
                styleContent2.BorderLeft = BorderStyle.THIN;
                styleContent2.BorderRight = BorderStyle.THIN;
                styleContent2.BorderBottom = BorderStyle.THIN;
                styleContent2.Alignment = HorizontalAlignment.CENTER;

                ISheet sheet = workbook.GetSheet("Part Master");
                string date = DateTime.Now.ToString("dd-MM-yyyy");
                filename = "MasterPart_" + date + ".xls";
                sheet.ForceFormulaRecalculation = true;

                sheet.GetRow(6).GetCell(3).SetCellValue(date);
                sheet.GetRow(7).GetCell(3).SetCellValue("pid.kevin");
                sheet.GetRow(9).GetCell(4).SetCellValue(p_Part_No);
                sheet.GetRow(10).GetCell(4).SetCellValue(p_Car_Family_Cd);
                sheet.GetRow(11).GetCell(4).SetCellValue(p_Paint_Type);

                int row = 16;
                string GetPartNo = string.Empty;
                string GetPartName = string.Empty;
                string GetCarFamilyCd = string.Empty;
                string GetPaintType = string.Empty;
                string GetPartSpec = string.Empty;
                string GetTaktTime = string.Empty;
                string GetPcsKbn = string.Empty;
                int index = 0;
                IRow Hrow;

                foreach (var result in model)
                {
                    index++;

                    Hrow = sheet.CreateRow(row);

                    GetPartNo = result.Part_No;
                    GetPartName = result.Part_Name;
                    GetCarFamilyCd = result.Car_Family_Cd;
                    GetPaintType = result.Paint_Type;
                    GetPartSpec = result.Part_Spec;
                    GetTaktTime = result.Takt_Time.ToString();
                    GetPcsKbn = result.PcsKbn.ToString();

                    //sheet.GetRow(row).GetCell(1).SetCellValue(index.ToString());
                    //sheet.GetRow(row).GetCell(2).SetCellValue(GetPartNo);
                    //sheet.GetRow(row).GetCell(3).SetCellValue(GetPartName);
                    //sheet.GetRow(row).GetCell(4).SetCellValue(GetModel);
                    //sheet.GetRow(row).GetCell(5).SetCellValue(GetPaintType);
                    //sheet.GetRow(row).GetCell(6).SetCellValue(GetPartSpec);
                    //sheet.GetRow(row).GetCell(7).SetCellValue(GetTaktTime);
                    //sheet.GetRow(row).GetCell(8).SetCellValue(GetPcsKbn);

                    //sheet.GetRow(row).GetCell(1).CellStyle = styleContent2;
                    //sheet.GetRow(row).GetCell(2).CellStyle = styleContent2;
                    //sheet.GetRow(row).GetCell(3).CellStyle = styleContent1;
                    //sheet.GetRow(row).GetCell(4).CellStyle = styleContent1;
                    //sheet.GetRow(row).GetCell(5).CellStyle = styleContent1;
                    //sheet.GetRow(row).GetCell(6).CellStyle = styleContent1;
                    //sheet.GetRow(row).GetCell(7).CellStyle = styleContent1;
                    //sheet.GetRow(row).GetCell(8).CellStyle = styleContent1;
                    
                    Hrow.CreateCell(1).SetCellValue(index.ToString());
                    Hrow.CreateCell(2).SetCellValue(GetPartNo);
                    Hrow.CreateCell(3).SetCellValue(GetPartName);
                    Hrow.CreateCell(4).SetCellValue(GetCarFamilyCd);
                    Hrow.CreateCell(5).SetCellValue(GetPaintType);
                    Hrow.CreateCell(6).SetCellValue(GetPartSpec);
                    Hrow.CreateCell(7).SetCellValue(GetTaktTime);
                    Hrow.CreateCell(8).SetCellValue(GetPcsKbn);

                    Hrow.GetCell(1).CellStyle = styleContent2;
                    Hrow.GetCell(2).CellStyle = styleContent2;
                    Hrow.GetCell(3).CellStyle = styleContent1;
                    Hrow.GetCell(4).CellStyle = styleContent1;
                    Hrow.GetCell(5).CellStyle = styleContent1;
                    Hrow.GetCell(6).CellStyle = styleContent1;
                    Hrow.GetCell(7).CellStyle = styleContent1;
                    Hrow.GetCell(8).CellStyle = styleContent1;

                    row++;
                }

                //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
                //    sheet.AutoSizeColumn(i);

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                Response.BinaryWrite(ms.ToArray());
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            }
            catch (Exception err)
            {
                string x = err.ToString();

                x = err.ToString();
            }
        }

        public string SPN311SaveData(string p_Part_No, string p_Part_Name, string p_Car_Family_Cd, string p_Paint_Type, string p_Part_Spec, string p_Takt_Time, string p_PcsKbn, string p_Latest_Order, string p_Created_By)
        {
            return MasterPart.SPN311SaveData(p_Part_No, p_Part_Name, p_Car_Family_Cd, p_Paint_Type, p_Part_Spec, p_Takt_Time, p_PcsKbn, p_Latest_Order, "pid.kevin");
        }

        public string SPN311UpdateData(string p_Part_No, string p_Part_Name, string p_Car_Family_Cd, string p_Paint_Type, string p_Part_Spec, string p_Takt_Time, string p_PcsKbn, string p_Latest_Order, string p_Created_By)
        {
            return MasterPart.SPN311UpdateData(p_Part_No, p_Part_Name, p_Car_Family_Cd, p_Paint_Type, p_Part_Spec, p_Takt_Time, p_PcsKbn, p_Latest_Order, "pid.kevin");
        }

        public string SPN311DeleteData(string p_Part_No)
        {
            return MasterPart.SPN311DeleteData(p_Part_No);
        }

        public string SPN311GetInfo()
        {
            return getpE_Info;
        }

        public ActionResult SPN311EditPopUpUpdate()
        {
            ViewData["SetPartNo"] = getpE_PartNo;
            ViewData["SetPartName"] = getpE_PartName;
            ViewData["SetCarFamilyCd"] = getpE_CarFamilyCd;
            ViewData["SetPaintType"] = getpE_PaintType;
            ViewData["SetPartSpec"] = getpE_PartSpec;
            ViewData["SetTaktTime"] = getpE_TaktTime;
            ViewData["SetPcsKbn"] = getpE_PcsKbn;
            ViewData["SetLatestOrder"] = getpE_LatestOrder != Convert.ToDateTime("01-01-1753").ToString() ? Convert.ToDateTime(getpE_LatestOrder) : DateTime.MinValue;

            return PartialView("_PartialUpdateMaster");
        }

        public ActionResult SPN311EditPopUpAdd()
        {
            ViewData["SetPartNo"] = getpE_PartNo;

            return PartialView("_PartialAddMaster");
        }

        public ActionResult SPN311PartialGridView(string p_Part_No, string p_Car_Family_Cd, string p_Paint_Type)
        {
            List<mPart> model = new List<mPart>();

            if (String.IsNullOrEmpty(p_Part_No.ToString()) && String.IsNullOrEmpty(p_Car_Family_Cd.ToString()) && String.IsNullOrEmpty(p_Paint_Type.ToString()))
            {
                model = MasterPart.SPN311GetListMasterPartSpecific("", "", "");
            }
            else if (p_Car_Family_Cd == "NULLSTATE" && p_Paint_Type == "NULLSTATE")
            {
                model = MasterPart.SPN311GetListMasterPart();
            }
            else
            {
                model = MasterPart.SPN311GetListMasterPartSpecific(p_Part_No, p_Car_Family_Cd, p_Paint_Type);
            }
            return PartialView("_PartialGridView", model);
        }
    }
}
