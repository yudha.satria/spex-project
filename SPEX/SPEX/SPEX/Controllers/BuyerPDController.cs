﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class BuyerPDController : PageController
    {
        //
        // GET: /BuyerPD/
        mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();

        public BuyerPDController()
        {
            Settings.Title = "Buyer PD Master";        
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

         protected override void Startup()
        {
            //CallbackComboPriceTerm();

            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("BuyerPD Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
            ViewData["PRIVILEGEs"] = BuyerPD.GetDataBySystemMaster("General", "PRIVILEGE"); //BuyerPD.GetPrivilege();

        }

         //public ActionResult BuyerPDCallBack(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION)
        //modif agi 2017-08-07
         public ActionResult BuyerPDCallBack(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION, string pDESTINATION_CODE, string pPRIVILEGE, string pCOUNTRY_CD, string pCOUNTRY_CD_DESC, string pCASE_LABEL_CD)     
         {             
             List<mBuyerPD> model = new List<mBuyerPD>();

             {
                 model = BuyerPD.getListBuyerPD
                 (pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION, pDESTINATION_CODE,pPRIVILEGE,pCOUNTRY_CD,pCOUNTRY_CD_DESC,pCASE_LABEL_CD);
             }
             return PartialView("BuyerPDGrid", model);
         }

         //function download      
         //public void DownloadBuyerPD(object sender, EventArgs e, string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION)
        //modif agi 2017-08-07
         public void DownloadBuyerPD(object sender, EventArgs e, string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION, string pDESTINATION_CODE, string pPRIVILEGE, string pCOUNTRY_CD, string pCOUNTRY_CD_DESC, string pCASE_LABEL_CD)     
         {
             string filename = "";
             string filesTmp = HttpContext.Request.MapPath("~/Template/BuyerPD_Code_Download.xls");
             FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

             HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

             ICellStyle styleContent = workbook.CreateCellStyle();
             styleContent.VerticalAlignment = VerticalAlignment.Top;
             styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
             styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
             styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
             styleContent.Alignment = HorizontalAlignment.Left;

             ICellStyle styleContent2 = workbook.CreateCellStyle();
             styleContent2.VerticalAlignment = VerticalAlignment.Top;
             styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
             styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
             styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
             styleContent2.Alignment = HorizontalAlignment.Center;

             ICellStyle styleContent3 = workbook.CreateCellStyle();
             styleContent3.VerticalAlignment = VerticalAlignment.Top;
             styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
             styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
             styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
             styleContent3.Alignment = HorizontalAlignment.Right;

             ISheet sheet = workbook.GetSheet("BuyerPD Code");
             string date = DateTime.Now.ToString("ddMMyyyy");
             filename = "BuyerPD_Code_Master_" + date + ".xls";

             string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

             sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
             sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);
             sheet.GetRow(8).GetCell(2).SetCellValue(pBUYER_PD_CD);
             sheet.GetRow(9).GetCell(2).SetCellValue(pCUSTOMER_NO);
             sheet.GetRow(10).GetCell(2).SetCellValue(pDESCRIPTION);


             int row = 13;
             int rowNum = 1;
             IRow Hrow;

             List<mBuyerPD> model = new List<mBuyerPD>();

             model = BuyerPD.getListBuyerPD
                 (pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION, pDESTINATION_CODE,pPRIVILEGE,pCOUNTRY_CD,pCOUNTRY_CD_DESC,pCASE_LABEL_CD);


             foreach (var result in model)
             {
                 Hrow = sheet.CreateRow(row);

                 Hrow.CreateCell(1).SetCellValue(rowNum);
                 Hrow.CreateCell(2).SetCellValue(result.BUYER_PD_CD);
                 Hrow.CreateCell(3).SetCellValue(result.CUSTOMER_NO);
                 Hrow.CreateCell(4).SetCellValue(result.DESCRIPTION);
                 
                 //Hrow.CreateCell(5).SetCellValue(result.DELETION_FLAG);
                 //Hrow.CreateCell(6).SetCellValue(result.CREATED_BY);
                 //Hrow.CreateCell(7).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                 //Hrow.CreateCell(8).SetCellValue(result.CHANGED_BY);
                 //Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));
                 Hrow.CreateCell(5).SetCellValue(result.DESTINATION_CODE);
                 Hrow.CreateCell(6).SetCellValue(result.COUNTRY_CD);
                 Hrow.CreateCell(7).SetCellValue(result.COUNTRY_CD_DESC);
                 Hrow.CreateCell(8).SetCellValue(result.PRIVILEGE);
                 Hrow.CreateCell(9).SetCellValue(result.CASE_LABEL_CD);
                 Hrow.CreateCell(10).SetCellValue(result.DELETION_FLAG);
                 Hrow.CreateCell(11).SetCellValue(result.CREATED_BY);
                 Hrow.CreateCell(12).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                 Hrow.CreateCell(13).SetCellValue(result.CHANGED_BY);
                 Hrow.CreateCell(14).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                 Hrow.GetCell(1).CellStyle = styleContent3;
                 Hrow.GetCell(2).CellStyle = styleContent2;
                 Hrow.GetCell(3).CellStyle = styleContent2;
                 Hrow.GetCell(4).CellStyle = styleContent;
                 //Hrow.GetCell(5).CellStyle = styleContent2;
                 //Hrow.GetCell(6).CellStyle = styleContent2;
                 //Hrow.GetCell(7).CellStyle = styleContent2;
                 //Hrow.GetCell(8).CellStyle = styleContent2;
                 //Hrow.GetCell(9).CellStyle = styleContent2;
                 Hrow.GetCell(5).CellStyle = styleContent;
                 Hrow.GetCell(6).CellStyle = styleContent;
                 Hrow.GetCell(7).CellStyle = styleContent;
                 Hrow.GetCell(8).CellStyle = styleContent;
                 Hrow.GetCell(9).CellStyle = styleContent;
                 Hrow.GetCell(10).CellStyle = styleContent2;
                 Hrow.GetCell(11).CellStyle = styleContent2;
                 Hrow.GetCell(12).CellStyle = styleContent2;
                 Hrow.GetCell(13).CellStyle = styleContent2;
                 Hrow.GetCell(14).CellStyle = styleContent2;
                 row++;
                 rowNum++;
             }

             MemoryStream ms = new MemoryStream();
             workbook.Write(ms);
             ftmp.Close();
             Response.BinaryWrite(ms.ToArray());
             Response.ContentType = "application/vnd.ms-excel";
             Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
         }

         #region Fucntion Delete Data
         public ActionResult DeleteBuyerPD(string p_BUYER_PD_CD, string p_CHANGED_BY)
         {
             if (getpE_UserId!=null)
             {
                 p_CHANGED_BY = getpE_UserId.Username;
                 //p_CHANGED_BY = "SYSTEM";
                 string[] r = null;
                 string resultMessage = BuyerPD.DeleteData(p_BUYER_PD_CD, p_CHANGED_BY);

                 r = resultMessage.Split('|');
                 if (r[0] == "Error ")
                 {
                     return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                 }
                 else
                 {
                     return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
                 }
             }
             else
             {
                 return Json(new { success = "false", messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
             }
             
             return null;
         }
        #endregion 

         #region Function Add
         //public ActionResult AddNewBuyerPD(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION, string p_CREATED_BY)
        //modif agi 2017-08-07
         public ActionResult AddNewBuyerPD(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION, string pDESTINATION_CODE, string pCOUNTRY_CD, string pCOUNTRY_CD_DESC, string pPRIVILEGE, string pCASE_LABEL_CD)
         {
             if( getpE_UserId!=null)
             {
                 string p_CREATED_BY = getpE_UserId.Username;
                 //p_CREATED_BY = "SYSTEM";
                 string[] r = null;
                 //string resultMessage = BuyerPD.SaveData(pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION, p_CREATED_BY);
                 //modif agi 2017-08-07
                 string resultMessage = BuyerPD.SaveData(pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION, p_CREATED_BY, pDESTINATION_CODE, pCOUNTRY_CD, pCOUNTRY_CD_DESC, pPRIVILEGE, pCASE_LABEL_CD);
                 r = resultMessage.Split('|');
                 if (r[0] == "Error ")
                 {
                     return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                 }
                 else
                 {
                     return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
                 }

             }
             else
             {
                 return Json(new { success = "false", messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
             }
             return null;
         }
        #endregion

        #region Function Edit -
         //public ActionResult UpdateBuyerPD(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION, string pDELETION_FLAG, string pCHANGED_BY)
        //modif agi 2017-08-07
         public ActionResult UpdateBuyerPD(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION, string pDELETION_FLAG, string pDESTINATION_CODE, string pCOUNTRY_CD, string pCOUNTRY_CD_DESC, string pPRIVILEGE, string pCASE_LABEL_CD)     
         {
             if (getpE_UserId!=null)
             {
                 string pCHANGED_BY=string.Empty;
                 pCHANGED_BY = getpE_UserId.Username;
                 //pCHANGED_BY = "SYSTEM";
                 string[] r = null;
                 //string resultMessage = BuyerPD.UpdateData(pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION, pDELETION_FLAG, pCHANGED_BY);
                 //modif agi 2017-08-07
                 string resultMessage = BuyerPD.UpdateData(pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION, pDELETION_FLAG, pCHANGED_BY, pDESTINATION_CODE, pCOUNTRY_CD, pCOUNTRY_CD_DESC, pPRIVILEGE, pCASE_LABEL_CD);

                 r = resultMessage.Split('|');
                 if (r[0] == "Error ")
                 {
                     return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                 }
                 else
                 {
                     return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
                 }
             }
             else
             {
                 return Json(new { success = "false", messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
             }
             return null;
         }
        #endregion
   
        



    }
}
