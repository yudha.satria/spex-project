﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;
using NPOI.HSSF.UserModel;
using SPEX.Models.Combobox;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class SubManifestReceivingInquiryController : PageController
    {

        //Model Maintenance Stock Master
        mSubManifestReceiving ms = new mSubManifestReceiving();
        MessagesString messageError = new MessagesString();

        //Upload
        public string moduleID = "SPX13";
        public string functionID = "SPX130200";
        public const string UploadDirectory = "Content\\FileUploadResult";
        public const string TemplateFName = "UPLOAD_MAINTENANCE_STOCK_MASTER";

        public SubManifestReceivingInquiryController()
        {
            Settings.Title = " Manifest Receiving Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public void getCombobox()
        {
            mCombobox itemNotMandatory = new mCombobox();

            List<mCombobox> ListDeleteFlag = null;
            ListDeleteFlag = ComboboxRepository.Instance.SystemMasterCode("SEND_GR_ADM_FLAG");
            ListDeleteFlag.Insert(0, itemNotMandatory);
            ViewData["SEND_GR_ADM_FLAG"] = ListDeleteFlag;
        }


        protected override void Startup()
        {

            //Call error message
            ViewBag.MSPX00001ERR = messageError.getMSPX00001ERR(" Sub Manifest Receiving Inquiry");
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = messageError.getMSPX00009ERR();
            /*No data found*/
            ViewBag.MSPXS2006ERR = messageError.getMsgText("MSPXS2006ERR");
            /*{0} should not be empty.*/
            ViewBag.MSPXS2005ERR = messageError.getMsgText("MSPXS2005ERR");
            /*A single record must be selected.*/
            ViewBag.MSPXS2017ERR = messageError.getMsgText("MSPXS2017ERR");
            /*Duplication found for {0}.*/
            ViewBag.MSPXS2011ERR = messageError.getMsgText("MSPXS2011ERR");
            /*{0} is not exists in  {1}*/
            ViewBag.MSPXS2010ERR = messageError.getMsgText("MSPXS2010ERR");
            /*Cannot find template file {0} on folder {1}.*/
            ViewBag.MSPXS2034ERR = messageError.getMsgText("MSPXS2034ERR");
            /*Error writing file = {0}. Reason = {1}.*/
            ViewBag.MSPXS2035ERR = messageError.getMsgText("MSPXS2035ERR");
            /*Invalid data type of {0}. Should be {1}.*/
            ViewBag.MSPXS2012ERR = messageError.getMsgText("MSPXS2012ERR");
            /*Are you sure you want to confirm the operation?*/
            ViewBag.MSPXS2018INF = messageError.getMsgText("MSPXS2018INF");
            /*Are you sure you want to abort the operation?*/
            ViewBag.MSPXS2019INF = messageError.getMsgText("MSPXS2019INF");
            /*Are you sure you want to delete the record?*/
            ViewBag.MSPXS2020INF = messageError.getMsgText("MSPXS2020INF");
            /*Are you sure you want to download this data?*/
            ViewBag.MSPXS2021INF = messageError.getMsgText("MSPXS2021INF");



            getpE_UserId = (User)ViewData["User"];
            getCombobox();
        }

        public ActionResult SubManifestReceivingCallBack(string pMANIFEST_NO, string pSUPPLIER_CD, string pTMMIN_ORDER_NO, string pSUB_MANIFEST_NO, string pSUPPLIER_PLANT, string pPART_NO, string pDOCK_CD, string pSUB_SUPPLIER_CD, string pSEND_GR_ADM_FLAG, string pORDER_DATE_FROM, string pORDER_DATE_TO, string pPLAN_RCV_DATE_FROM, string pPLAN_RCV_DATE_TO, string pACTUAL_RCV_DATE_FROM, string pACTUAL_RCV_DATE_TO, string pSUB_SUPPLIER_PLANT, string Mode)
        {
            List<mSubManifestReceiving> model = new List<mSubManifestReceiving>();
            if (Mode.Equals("Search"))
                model = ms.getListSubManifestReceiving(pMANIFEST_NO, pSUPPLIER_CD, pTMMIN_ORDER_NO, pSUB_MANIFEST_NO, pSUPPLIER_PLANT, pPART_NO, pDOCK_CD, pSUB_SUPPLIER_CD, pSEND_GR_ADM_FLAG, pORDER_DATE_FROM, pORDER_DATE_TO, pPLAN_RCV_DATE_FROM, pPLAN_RCV_DATE_TO, pACTUAL_RCV_DATE_FROM, pACTUAL_RCV_DATE_TO, pSUB_SUPPLIER_PLANT);

            return PartialView("SubManifestReceivingInquiryGrid", model);
        }

        //function download 
        public ActionResult DownloadSubManifestReceiving(object sender, EventArgs e, string pMANIFEST_NO, string pSUPPLIER_CD, string pTMMIN_ORDER_NO, string pSUB_MANIFEST_NO, string pSUPPLIER_PLANT, string pPART_NO, string pDOCK_CD, string pSUB_SUPPLIER_CD, string pSEND_GR_ADM_FLAG, string pORDER_DATE_FROM, string pORDER_DATE_TO, string pPLAN_RCV_DATE_FROM, string pPLAN_RCV_DATE_TO, string pACTUAL_RCV_DATE_FROM, string pACTUAL_RCV_DATE_TO, string pSUB_SUPPLIER_PLANT)
        {
            string rExtract = string.Empty;
            string filename = "Sub_Manifest_Receiving_Inquiry_Download.xls";
            string directory = "Template";
            string filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);
            List<mSubManifestReceiving> model = new List<mSubManifestReceiving>();
            model = ms.getListSubManifestReceiving(pMANIFEST_NO, pSUPPLIER_CD, pTMMIN_ORDER_NO, pSUB_MANIFEST_NO, pSUPPLIER_PLANT, pPART_NO, pDOCK_CD, pSUB_SUPPLIER_CD, pSEND_GR_ADM_FLAG, pORDER_DATE_FROM, pORDER_DATE_TO, pPLAN_RCV_DATE_FROM, pPLAN_RCV_DATE_TO, pACTUAL_RCV_DATE_FROM, pACTUAL_RCV_DATE_TO, pSUB_SUPPLIER_PLANT);

            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            else if (model.Count() == 0)
            {
                lError = 1;
                status = "Data";
                rExtract = model.Count().ToString();
            }
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1)
                return Json(new { success = false, messages = rExtract, status = status }, JsonRequestBehavior.AllowGet);
            #endregion
            
            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Sub Manifest Receiving Inquiry");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "Sub_Manifest_receiving_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);


            sheet.GetRow(8).GetCell(2).SetCellValue(String.IsNullOrEmpty(pMANIFEST_NO) ? "ALL" : pMANIFEST_NO);
            sheet.GetRow(9).GetCell(2).SetCellValue(String.IsNullOrEmpty(pSUB_MANIFEST_NO) ? "ALL" : pSUB_MANIFEST_NO);
            sheet.GetRow(10).GetCell(2).SetCellValue(String.IsNullOrEmpty(pDOCK_CD) ? "ALL" : pDOCK_CD);
            sheet.GetRow(11).GetCell(2).SetCellValue(String.IsNullOrEmpty(pSUPPLIER_CD) ? "ALL" : pSUPPLIER_CD);
            sheet.GetRow(12).GetCell(2).SetCellValue(String.IsNullOrEmpty(pSUPPLIER_PLANT) ? "ALL" : pSUPPLIER_PLANT);

            sheet.GetRow(8).GetCell(6).SetCellValue((String.IsNullOrEmpty(pORDER_DATE_FROM) ? "-" : pORDER_DATE_FROM) + " To " + (String.IsNullOrEmpty(pORDER_DATE_TO) ? "-" : pORDER_DATE_TO));
            sheet.GetRow(9).GetCell(6).SetCellValue((String.IsNullOrEmpty(pPLAN_RCV_DATE_FROM) ? "-" : pPLAN_RCV_DATE_FROM) + " To " + (String.IsNullOrEmpty(pPLAN_RCV_DATE_TO) ? "-" : pPLAN_RCV_DATE_TO));
            sheet.GetRow(10).GetCell(6).SetCellValue((String.IsNullOrEmpty(pACTUAL_RCV_DATE_FROM) ? "-" : pACTUAL_RCV_DATE_FROM) + " To " + (String.IsNullOrEmpty(pACTUAL_RCV_DATE_TO) ? "-" : pACTUAL_RCV_DATE_TO));
            sheet.GetRow(11).GetCell(6).SetCellValue(String.IsNullOrEmpty(pSUB_SUPPLIER_CD) ? "ALL" : pSUB_SUPPLIER_CD);
            sheet.GetRow(12).GetCell(6).SetCellValue(String.IsNullOrEmpty(pSUB_SUPPLIER_PLANT) ? "ALL" : pSUB_SUPPLIER_PLANT);

            sheet.GetRow(8).GetCell(10).SetCellValue((String.IsNullOrEmpty(pTMMIN_ORDER_NO) ? "ALL" : pTMMIN_ORDER_NO));
            sheet.GetRow(9).GetCell(10).SetCellValue((String.IsNullOrEmpty(pPART_NO) ? "ALL" : pPART_NO));
            sheet.GetRow(10).GetCell(10).SetCellValue((String.IsNullOrEmpty(pSEND_GR_ADM_FLAG) ? "ALL" : pSEND_GR_ADM_FLAG));


            int row = 16;
            int rowNum = 1;
            IRow Hrow;



            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.MANIFEST_NO);
                Hrow.CreateCell(3).SetCellValue(result.SUB_MANIFEST_NO);
                Hrow.CreateCell(4).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(5).SetCellValue(result.PART_NO);
                Hrow.CreateCell(6).SetCellValue(result.TMAP_PART_NO);
                Hrow.CreateCell(7).SetCellValue(result.MANIFEST_TYPE);
                Hrow.CreateCell(8).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(9).SetCellValue(result.PLANT_CODE);
                Hrow.CreateCell(10).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(11).SetCellValue(result.SUB_SUPPLIER_PLANT);
                Hrow.CreateCell(12).SetCellValue(result.DOCK_CD);
                Hrow.CreateCell(13).SetCellValue(result.SUB_ROUTE_CD);
                Hrow.CreateCell(14).SetCellValue(result.ORDER_RELEASE_DT);
                Hrow.CreateCell(15).SetCellValue(result.ARRIVAL_PLAN_DT);
                Hrow.CreateCell(16).SetCellValue(result.ARRIVAL_ACTUAL_DT);
                Hrow.CreateCell(17).SetCellValue(result.ORDER_QTY);
                Hrow.CreateCell(18).SetCellValue(result.RECEIVE_KANBAN);
                Hrow.CreateCell(19).SetCellValue(result.SEND_GR_ADM_FLAG);
                Hrow.CreateCell(20).SetCellValue(result.RECEIVE_FLAG);


                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent;
                Hrow.GetCell(3).CellStyle = styleContent;
                Hrow.GetCell(4).CellStyle = styleContent;
                Hrow.GetCell(5).CellStyle = styleContent;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent3;
                Hrow.GetCell(18).CellStyle = styleContent3;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent2;
                

                row++;
                rowNum++;
            }

            MemoryStream memory = new MemoryStream();
            workbook.Write(memory);
            ftmp.Close();
            Response.BinaryWrite(memory.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));

            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);
        }


        //function download 
        public ActionResult DownloadSubManifestReceivingDetail(object sender, EventArgs e, string pMANIFEST_NO, string pSUPPLIER_CD, string pTMMIN_ORDER_NO, string pSUB_MANIFEST_NO, string pSUPPLIER_PLANT, string pPART_NO, string pDOCK_CD, string pSUB_SUPPLIER_CD, string pSEND_GR_ADM_FLAG, string pORDER_DATE_FROM, string pORDER_DATE_TO, string pPLAN_RCV_DATE_FROM, string pPLAN_RCV_DATE_TO, string pACTUAL_RCV_DATE_FROM, string pACTUAL_RCV_DATE_TO, string pSUB_SUPPLIER_PLANT)
        {
            string rExtract = string.Empty;
            string filename = "Sub_Manifest_Receiving_Inquiry_Download_Detail.xls";
            string directory = "Template";
            string filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);
            List<mSubManifestReceiving> model = new List<mSubManifestReceiving>();
            model = ms.getListSubManifestReceivingDetail(pMANIFEST_NO, pSUPPLIER_CD, pTMMIN_ORDER_NO, pSUB_MANIFEST_NO, pSUPPLIER_PLANT, pPART_NO, pDOCK_CD, pSUB_SUPPLIER_CD, pSEND_GR_ADM_FLAG, pORDER_DATE_FROM, pORDER_DATE_TO, pPLAN_RCV_DATE_FROM, pPLAN_RCV_DATE_TO, pACTUAL_RCV_DATE_FROM, pACTUAL_RCV_DATE_TO, pSUB_SUPPLIER_PLANT);

            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            else if (model.Count() == 0)
            {
                lError = 1;
                status = "Data";
                rExtract = model.Count().ToString();
            }
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1)
                return Json(new { success = false, messages = rExtract, status = status }, JsonRequestBehavior.AllowGet);
            #endregion
            

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Sub Manifest Receiving Inquiry");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "Sub_Manifest_Receiving_Detail_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            int row = 10;
            int rowNum = 1;
            IRow Hrow;


            
            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.MANIFEST_NO);
                Hrow.CreateCell(3).SetCellValue(result.SUB_MANIFEST_NO);
                Hrow.CreateCell(4).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(5).SetCellValue(result.PART_NO);
                Hrow.CreateCell(6).SetCellValue(result.TMAP_PART_NO);
                Hrow.CreateCell(7).SetCellValue(result.MANIFEST_TYPE);
                Hrow.CreateCell(8).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(9).SetCellValue(result.PLANT_CODE);
                Hrow.CreateCell(10).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(11).SetCellValue(result.SUB_SUPPLIER_PLANT);
                Hrow.CreateCell(12).SetCellValue(result.DOCK_CD);
                Hrow.CreateCell(13).SetCellValue(result.SUB_ROUTE_CD);
                Hrow.CreateCell(14).SetCellValue(result.ORDER_RELEASE_DT);
                Hrow.CreateCell(15).SetCellValue(result.ARRIVAL_PLAN_DT);
                Hrow.CreateCell(16).SetCellValue(result.ARRIVAL_ACTUAL_DT);
                Hrow.CreateCell(17).SetCellValue(result.KANBAN_ID);
                Hrow.CreateCell(18).SetCellValue(result.SEND_GR_ADM_FLAG);
                Hrow.CreateCell(19).SetCellValue(result.RECEIVE_FLAG);



                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent;
                Hrow.GetCell(3).CellStyle = styleContent;
                Hrow.GetCell(4).CellStyle = styleContent;
                Hrow.GetCell(5).CellStyle = styleContent;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent;
                Hrow.GetCell(18).CellStyle = styleContent2;
                Hrow.GetCell(19).CellStyle = styleContent2;

                row++;
                rowNum++;
            }

            MemoryStream memory = new MemoryStream();
            workbook.Write(memory);
            ftmp.Close();
            Response.BinaryWrite(memory.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));

            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult _PopUpLog(string pMANIFEST_NO, string pPART_NO, string pSUB_MANIFEST_NO)
        {
            List<mDetailSubManifestReceiving> model = new List<mDetailSubManifestReceiving>();
            mDetailSubManifestReceiving detailSubManifest = new mDetailSubManifestReceiving();
            model = detailSubManifest.getDetailListSubManifestReceiving(pMANIFEST_NO, pPART_NO, pSUB_MANIFEST_NO);

            return PartialView("SubManifestReceivingInquiryGridDetail", model);
        }

    }
}
