﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;
using NPOI.HSSF.UserModel;
using SPEX.Models.Combobox;
//using Microsoft.Office.Interop.Excel;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class ProblemReportInquiryController : PageController
    {

        //Model Problem Report
        mProblemReportInquiry ms = new mProblemReportInquiry();
        MessagesString messageError = new MessagesString();


        public ProblemReportInquiryController()
        {
            Settings.Title = "Problem Report Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }


        public void GetCombobox()
        {
            mCombobox itemNotMandatory = new mCombobox();
            itemNotMandatory.COMBO_LABEL = "--ALL--";
            itemNotMandatory.COMBO_VALUE = "";

            List<mCombobox> ListProblemOrigin = null;
            ListProblemOrigin = ComboboxRepository.Instance.SystemMasterCode("PROBLEM_ORIGIN");
            ListProblemOrigin.Insert(0, itemNotMandatory);
            ViewData["PROBLEM_ORIGIN"] = ListProblemOrigin;


            List<mCombobox> ListSendStatus = null;
            ListSendStatus = ComboboxRepository.Instance.SystemMasterCode("SEND_STATUS");
            ListSendStatus.Insert(0, itemNotMandatory);
            ViewData["SEND_STATUS"] = ListSendStatus;

            List<mCombobox> ListStatusProblem = null;
            ListStatusProblem = ComboboxRepository.Instance.SystemMasterCode("STATUS_PROBLEM_SHEET");
            ListStatusProblem.Insert(0, itemNotMandatory);
            ViewData["STATUS_PROBLEM_SHEET"] = ListStatusProblem;


            List<mCombobox> ListProblemOriginGrid = null;
            ListProblemOriginGrid = ComboboxRepository.Instance.SystemMasterCode("PROBLEM_ORIGIN");
            ListProblemOriginGrid.Insert(0, itemNotMandatory);
            ViewData["PROBLEM_ORIGIN_GRID"] = ListProblemOriginGrid;


            List<mCombobox> ListProblemCategory = null;
            ListProblemCategory = ComboboxRepository.Instance.GetProblemCategory("");
            ListProblemCategory.Insert(0, itemNotMandatory);
            ViewData["PROBLEM_CATEGORY_GRID"] = ListProblemCategory;
        }

        public ActionResult GetComboboxProblemCategory(string I_PROBLEM_ORIGIN)
        {
            string a = Request.Params["I_PROBLEM_ORIGIN"];
            mCombobox itemNotMandatory = new mCombobox();
            itemNotMandatory.COMBO_LABEL = "--SELECT--";
            itemNotMandatory.COMBO_VALUE = "";
            List<mCombobox> ListProblemOrigin = null;
            //string combo = Request.Params["SelectedCategoryId"];
            ListProblemOrigin = ComboboxRepository.Instance.GetProblemCategory(I_PROBLEM_ORIGIN);
            ListProblemOrigin.Insert(0, itemNotMandatory);
            ViewData["PROBLEM_CATEGORY"] = ListProblemOrigin;
            return PartialView("_PartialCombobox", ListProblemOrigin);
        }


        protected override void Startup()
        {

            //Call error message
            ViewBag.MSPX00001ERR = messageError.getMSPX00001ERR("Problem Report Creation");
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = messageError.getMSPX00009ERR();
            /*No data found*/
            ViewBag.MSPXS2006ERR = messageError.getMsgText("MSPXS2006ERR");
            /*{0} should not be empty.*/
            ViewBag.MSPXS2005ERR = messageError.getMsgText("MSPXS2005ERR");
            /*A single record must be selected.*/
            ViewBag.MSPXS2017ERR = messageError.getMsgText("MSPXS2017ERR");
            /*Duplication found for {0}.*/
            ViewBag.MSPXS2011ERR = messageError.getMsgText("MSPXS2011ERR");
            /*{0} is not exists in  {1}*/
            ViewBag.MSPXS2010ERR = messageError.getMsgText("MSPXS2010ERR");
            /*Cannot find template file {0} on folder {1}.*/
            ViewBag.MSPXS2034ERR = messageError.getMsgText("MSPXS2034ERR");
            /*Error writing file = {0}. Reason = {1}.*/
            ViewBag.MSPXS2035ERR = messageError.getMsgText("MSPXS2035ERR");
            /*Invalid data type of {0}. Should be {1}.*/
            ViewBag.MSPXS2012ERR = messageError.getMsgText("MSPXS2012ERR");
            /*Are you sure you want to confirm the operation?*/
            ViewBag.MSPXS2018INF = messageError.getMsgText("MSPXS2018INF");
            /*Are you sure you want to abort the operation?*/
            ViewBag.MSPXS2019INF = messageError.getMsgText("MSPXS2019INF");
            /*Are you sure you want to delete the record?*/
            ViewBag.MSPXS2020INF = messageError.getMsgText("MSPXS2020INF");
            /*Are you sure you want to download this data?*/
            ViewBag.MSPXS2021INF = messageError.getMsgText("MSPXS2021INF");
            /*Are you sure you want to send the record ?*/
            ViewBag.MSPXS2062ERR = messageError.getMsgText("MSPXS2062ERR");


            getpE_UserId = (User)ViewData["User"];
            GetCombobox();
            //ViewData["PRIVILEGEs"] = BuyerPD.GetDataBySystemMaster("General", "PRIVILEGE"); //BuyerPD.GetPrivilege();

        }

        public ActionResult ProblemReportInquiryCallBack(string Mode, string pPR_CD, string pPROBLEM_DATE_FROM, string pPROBLEM_DATE_TO, string pPART_NO, string pSTATUS_PROBLEM, string pSUPPLIER, string pSUB_SUPPLIER, string pSUPPLIER_CREATOR, string pPROBLEM_ORIGIN, string pPROBLEM_CATEGORY, string pKANBAN_ID, string pSEND_STATUS, string pTMAP_PART_NO)
        {
            List<mProblemReportInquiry> model = new List<mProblemReportInquiry>();
            if (Mode.Equals("Search"))
                model = ms.getListProblemReport(pPR_CD, pPROBLEM_DATE_FROM, pPROBLEM_DATE_TO, pPART_NO, pSTATUS_PROBLEM, pSUPPLIER, pSUB_SUPPLIER, pSUPPLIER_CREATOR, pPROBLEM_ORIGIN, pPROBLEM_CATEGORY, pKANBAN_ID, pSEND_STATUS, pTMAP_PART_NO);

            GetCombobox();
            return PartialView("ProblemReportInquiryGrid", model);
        }


        //function download 
        public ActionResult DownloadProblemReportInquiry(object sender, EventArgs e, string pPR_CD, string pPROBLEM_DATE_FROM, string pPROBLEM_DATE_TO, string pPART_NO, string pSTATUS_PROBLEM, string pSUPPLIER, string pSUB_SUPPLIER, string pSUPPLIER_CREATOR, string pPROBLEM_ORIGIN, string pPROBLEM_CATEGORY, string pKANBAN_ID, string pSEND_STATUS, string pTMAP_PART_NO)
        {
            string rExtract = string.Empty;
            string filename = "Problem_Report_Inquiry_Download.xls";
            string directory = "Template";
            string filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);

            List<mProblemReportInquiry> model = new List<mProblemReportInquiry>();
            model = ms.getListDownloadProblemReport(pPR_CD, pPROBLEM_DATE_FROM, pPROBLEM_DATE_TO, pPART_NO, pSTATUS_PROBLEM, pSUPPLIER, pSUB_SUPPLIER, pSUPPLIER_CREATOR, pPROBLEM_ORIGIN, pPROBLEM_CATEGORY, pKANBAN_ID, pSEND_STATUS, pTMAP_PART_NO);


            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            else if (model.Count() == 0)
            {
                lError = 1;
                status = "Data";
                rExtract = model.Count().ToString();
            }
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1)
                return Json(new { success = false, messages = rExtract, status = status }, JsonRequestBehavior.AllowGet);
            #endregion

           

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
            IFont font = workbook.CreateFont();
            font.FontHeightInPoints = 8;

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;
            styleContent.SetFont(font);


            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;
            styleContent2.SetFont(font);

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;
            styleContent3.SetFont(font);

            ISheet sheet = workbook.GetSheet("Problem Report Inquiry");
            string date = DateTime.Now.ToString("yyyyMMddhhmm");
            filename = "PROBLEM_REPORT_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);


            sheet.GetRow(8).GetCell(2).SetCellValue(String.IsNullOrEmpty(pPR_CD) ? "ALL" : pPR_CD);
            sheet.GetRow(9).GetCell(2).SetCellValue(String.IsNullOrEmpty(pSTATUS_PROBLEM) ? "ALL" : pSTATUS_PROBLEM);
            sheet.GetRow(10).GetCell(2).SetCellValue(String.IsNullOrEmpty(pSUPPLIER_CREATOR) ? "ALL" : pSUPPLIER_CREATOR);
            sheet.GetRow(11).GetCell(2).SetCellValue(String.IsNullOrEmpty(pKANBAN_ID) ? "ALL" : pKANBAN_ID);

            sheet.GetRow(8).GetCell(6).SetCellValue((String.IsNullOrEmpty(pPROBLEM_DATE_FROM) ? "-" : pPROBLEM_DATE_FROM) + " To " + (String.IsNullOrEmpty(pPROBLEM_DATE_TO) ? "-" : pPROBLEM_DATE_TO));
            sheet.GetRow(9).GetCell(6).SetCellValue(String.IsNullOrEmpty(pSUPPLIER) ? "ALL" : pSUPPLIER);
            sheet.GetRow(10).GetCell(6).SetCellValue(String.IsNullOrEmpty(pPROBLEM_ORIGIN) ? "ALL" : pPROBLEM_ORIGIN);
            sheet.GetRow(11).GetCell(6).SetCellValue(String.IsNullOrEmpty(pSEND_STATUS) ? "ALL" : pSEND_STATUS);

            sheet.GetRow(8).GetCell(10).SetCellValue((String.IsNullOrEmpty(pPART_NO) ? "ALL" : pPART_NO));
            sheet.GetRow(9).GetCell(10).SetCellValue((String.IsNullOrEmpty(pSUB_SUPPLIER) ? "ALL" : pSUB_SUPPLIER));
            sheet.GetRow(10).GetCell(10).SetCellValue((String.IsNullOrEmpty(pPROBLEM_CATEGORY) ? "ALL" : pPROBLEM_CATEGORY));
            sheet.GetRow(11).GetCell(10).SetCellValue((String.IsNullOrEmpty(pTMAP_PART_NO) ? "ALL" : pTMAP_PART_NO));


            int row = 15;
            int rowNum = 1;
            IRow Hrow;



            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PR_CD);
                Hrow.CreateCell(3).SetCellValue(result.SUB_MANIFEST_NO);
                Hrow.CreateCell(4).SetCellValue(result.MANIFEST_NO);
                Hrow.CreateCell(5).SetCellValue(result.DOCK_CD);
                Hrow.CreateCell(6).SetCellValue(result.STOCK_FLAG);
                Hrow.CreateCell(7).SetCellValue(result.PART_NO);
                Hrow.CreateCell(8).SetCellValue(result.PART_NAME);
                Hrow.CreateCell(9).SetCellValue(result.KANBAN_ID);
                Hrow.CreateCell(10).SetCellValue(result.PROBLEM_QTY);
                Hrow.CreateCell(11).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(12).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(13).SetCellValue(result.SUPPLIER_NAME);
                Hrow.CreateCell(14).SetCellValue(result.PROBLEM_CATEGORY_CD);
                Hrow.CreateCell(15).SetCellValue(result.PROBLEM_ORIGIN_CD);
                Hrow.CreateCell(16).SetCellValue(result.COST_CENTER);
                Hrow.CreateCell(17).SetCellValue(result.PR_DATE);
                Hrow.CreateCell(18).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(19).SetCellValue(result.STATUS);
                Hrow.CreateCell(20).SetCellValue(result.SEND_STATUS);


                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent;
                Hrow.GetCell(4).CellStyle = styleContent;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent;
                Hrow.GetCell(8).CellStyle = styleContent;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent3;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(14).CellStyle = styleContent;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent3;
                
                row++;
                rowNum++;
            }




            MemoryStream memory = new MemoryStream();
            workbook.Write(memory);
            ftmp.Close();
            
            /* Create Pdf
            //var pathfile = GetPath(reportName);
            //workbook.Write(filesOutput);
            filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);

            System.IO.File.WriteAllBytes(filesTmp, memory.GetBuffer());

            ExportWorkbookToPdf(filesTmp, filesOutput);
            */


            Response.BinaryWrite(memory.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));



            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);
        }



        //function download 
        public ActionResult DownloadProblemReportGi(object sender, EventArgs e, string pPR_CD, string pPROBLEM_DATE_FROM, string pPROBLEM_DATE_TO, string pPART_NO, string pSTATUS_PROBLEM, string pSUPPLIER, string pSUB_SUPPLIER, string pSUPPLIER_CREATOR, string pPROBLEM_ORIGIN, string pPROBLEM_CATEGORY, string pKANBAN_ID, string pSEND_STATUS, string pTMAP_PART_NO)
        {
            string rExtract = string.Empty;
            string filename = "Problem_Report_Gi_To_Scrap_Download.xls";
            string directory = "Template";
            string filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);


            List<mProblemReportInquiry> model = new List<mProblemReportInquiry>();
            model = ms.getListGiProblemReport(pPR_CD, pPROBLEM_DATE_FROM, pPROBLEM_DATE_TO, pPART_NO, pSTATUS_PROBLEM, pSUPPLIER, pSUB_SUPPLIER, pSUPPLIER_CREATOR, pPROBLEM_ORIGIN, pPROBLEM_CATEGORY, pKANBAN_ID, pSEND_STATUS, pTMAP_PART_NO);


            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            else if (model.Count() == 0)
            {
                lError = 1;
                status = "Data";
                rExtract = model.Count().ToString();
            }
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1)
                return Json(new { success = false, messages = rExtract, status = status }, JsonRequestBehavior.AllowGet);
            #endregion



            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
            IFont font = workbook.CreateFont();
            font.FontHeightInPoints = 8;

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;
            styleContent.SetFont(font);


            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;
            styleContent2.SetFont(font);

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;
            styleContent3.SetFont(font);

            ISheet sheet = workbook.GetSheet("Problem Report Inquiry");
            string date = DateTime.Now.ToString("yyyyMMddhhmm");
            filename = "PROBLEM_GI_TO_SCRAP_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);


            sheet.GetRow(8).GetCell(2).SetCellValue(String.IsNullOrEmpty(pPR_CD) ? "ALL" : pPR_CD);
            sheet.GetRow(9).GetCell(2).SetCellValue(String.IsNullOrEmpty(pSTATUS_PROBLEM) ? "ALL" : pSTATUS_PROBLEM);
            sheet.GetRow(10).GetCell(2).SetCellValue(String.IsNullOrEmpty(pSUPPLIER_CREATOR) ? "ALL" : pSUPPLIER_CREATOR);
            sheet.GetRow(11).GetCell(2).SetCellValue(String.IsNullOrEmpty(pKANBAN_ID) ? "ALL" : pKANBAN_ID);

            sheet.GetRow(8).GetCell(6).SetCellValue((String.IsNullOrEmpty(pPROBLEM_DATE_FROM) ? "-" : pPROBLEM_DATE_FROM) + " To " + (String.IsNullOrEmpty(pPROBLEM_DATE_TO) ? "-" : pPROBLEM_DATE_TO));
            sheet.GetRow(9).GetCell(6).SetCellValue(String.IsNullOrEmpty(pSUPPLIER) ? "ALL" : pSUPPLIER);
            sheet.GetRow(10).GetCell(6).SetCellValue(String.IsNullOrEmpty(pPROBLEM_ORIGIN) ? "ALL" : pPROBLEM_ORIGIN);
            sheet.GetRow(11).GetCell(6).SetCellValue(String.IsNullOrEmpty(pSEND_STATUS) ? "ALL" : pSEND_STATUS);

            sheet.GetRow(8).GetCell(10).SetCellValue((String.IsNullOrEmpty(pPART_NO) ? "ALL" : pPART_NO));
            sheet.GetRow(9).GetCell(10).SetCellValue((String.IsNullOrEmpty(pSUB_SUPPLIER) ? "ALL" : pSUB_SUPPLIER));
            sheet.GetRow(10).GetCell(10).SetCellValue((String.IsNullOrEmpty(pPROBLEM_CATEGORY) ? "ALL" : pPROBLEM_CATEGORY));
            sheet.GetRow(11).GetCell(10).SetCellValue((String.IsNullOrEmpty(pTMAP_PART_NO) ? "ALL" : pTMAP_PART_NO));

            int row = 15;
            int rowNum = 1;
            IRow Hrow;



            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.CLIENT_ID);
                Hrow.CreateCell(3).SetCellValue(result.MOVEMENT_TYPE);
                Hrow.CreateCell(4).SetCellValue(result.PLANT_CODE);
                Hrow.CreateCell(5).SetCellValue(result.STORAGE_LOCATION);
                Hrow.CreateCell(6).SetCellValue(result.PR_DATE);
                Hrow.CreateCell(7).SetCellValue(result.POSTING_DATE);
                Hrow.CreateCell(8).SetCellValue(result.DESCRIPTION);
                Hrow.CreateCell(9).SetCellValue(result.BAP_NO);
                Hrow.CreateCell(10).SetCellValue(result.PR_CD);
                Hrow.CreateCell(11).SetCellValue(result.ICS_REASON);
                Hrow.CreateCell(12).SetCellValue(result.COST_CENTER);
                Hrow.CreateCell(13).SetCellValue(result.TMAP_PART_NO);
                Hrow.CreateCell(14).SetCellValue(result.SOURCE_TYPE);
                Hrow.CreateCell(15).SetCellValue(result.PROD_PURPOSE);
                Hrow.CreateCell(16).SetCellValue(result.PROBLEM_QTY);

                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent3;

                row++;
                rowNum++;
            }




            MemoryStream memory = new MemoryStream();
            workbook.Write(memory);
            ftmp.Close();

            Response.BinaryWrite(memory.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));



            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult _PopUpLogKanban(string I_PR_CD, string I_SUB_MANIFEST_NO, string I_PART_NO)
        {
            List<mProblemReportInquiry> model = new List<mProblemReportInquiry>();
            model = ms.getListDetailKanban(I_PR_CD, I_SUB_MANIFEST_NO, I_PART_NO);
            return PartialView("DetailKanbanGrid", model);
        }

        public bool ExportWorkbookToPdf(string workbookPath, string outputPath)
        {

            // If either required string is null or empty, stop and bail out
            if (string.IsNullOrEmpty(workbookPath) || string.IsNullOrEmpty(outputPath))
            {
                return false;
            }

            // Create COM Objects
            Microsoft.Office.Interop.Excel.Application excelApplication;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook;

            // Create new instance of Excel
            excelApplication = new Microsoft.Office.Interop.Excel.Application();

            // Make the process invisible to the user
            excelApplication.ScreenUpdating = false;

            // Make the process silent
            excelApplication.DisplayAlerts = false;

            // Open the workbook that you wish to export to PDF
            excelWorkbook = excelApplication.Workbooks.Open(workbookPath);

            // If the workbook failed to open, stop, clean up, and bail out
            if (excelWorkbook == null)
            {
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;

                return false;
            }

            var exportSuccessful = true;
            try
            {
                // Call Excel's native export function (valid in Office 2007 and Office 2010, AFAIK)
                excelWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, outputPath);
            }
            catch (System.Exception ex)
            {
                // Mark the export as failed for the return value...
                exportSuccessful = false;

                // Do something with any exceptions here, if you wish...
                // MessageBox.Show...        
            }
            finally
            {
                // Close the workbook, quit the Excel, and clean up regardless of the results...
                excelWorkbook.Close();
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;
            }

            // You can use the following method to automatically open the PDF after export if you wish
            // Make sure that the file actually exists first...
            if (System.IO.File.Exists(outputPath))
            {
                System.Diagnostics.Process.Start(outputPath);
            }

            return exportSuccessful;
        }


    }
}
