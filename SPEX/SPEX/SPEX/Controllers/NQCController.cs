﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;
using Toyota.Common.Web.Platform.Starter.Models;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using NPOI.POIFS.FileSystem;
using NPOI.HPSF;
using System.IO;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxGridView;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class NQCController : PageController
    {
        mNQC MNQC = new mNQC();

        public NQCController()
        {

        }

        protected override void Startup()
        {

        }

        public ActionResult SPN231RegulerGridViewPartialView(string ps_ProdMonth, string ps_ParentPartNo, string ps_PartNo, string ps_SourceType)
        {
            List<mNQC> model = MNQC.SPN231NQCRegulerSearch(ps_ProdMonth, ps_ParentPartNo, ps_PartNo, ps_SourceType);

            return PartialView("RegulerGridViewPartialView", model);
        }

        public ActionResult SPN231EmergencyGridViewPartialView(string ps_ProdMonth, string ps_ParentPartNo, string ps_PartNo, string ps_SourceType)
        {
            List<mNQC> model = MNQC.SPN231NQCEmergencySearch(ps_ProdMonth, ps_ParentPartNo, ps_PartNo, ps_SourceType);

            return PartialView("EmergencyGridViewPartialView", model);
        }

        public void _SPN231DownloadNQC(object sender, EventArgs e, string pD_ProdMonth, string pD_ParentPartNo, string pD_PartNo, string pD_SourceType)
        {
            List<mNQC> model = new List<mNQC>();
            model = MNQC.SPN231NQCDownload(pD_ProdMonth, pD_ParentPartNo, pD_PartNo, pD_SourceType);


            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/NQC_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.TOP;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.TOP;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.Alignment = HorizontalAlignment.CENTER;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.TOP;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent3.Alignment = HorizontalAlignment.RIGHT;

            ISheet sheet = workbook.GetSheet("NQC");
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPIN231NQC_" + date + ".xls";
            sheet.ForceFormulaRecalculation = true;

            string dateNow = DateTime.Now.ToString("dd-MM-yyyy");
            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(3).SetCellValue("Sys");
            sheet.GetRow(9).GetCell(4).SetCellValue(pD_ProdMonth);
            sheet.GetRow(10).GetCell(4).SetCellValue(pD_ParentPartNo);
            sheet.GetRow(11).GetCell(4).SetCellValue(pD_PartNo);
            sheet.GetRow(12).GetCell(4).SetCellValue(pD_SourceType);        

            int row = 17;
            int rowNumber = 1;
            string GetProdMonth = string.Empty;
            string GetOrderType = string.Empty;
            string GetParentPartNo = string.Empty;
            string GetParentSourceType = string.Empty;
            string GetParentProd = string.Empty;
            string GetParentPlantCode = string.Empty;
            string GetPartNo = string.Empty;
            string GetSourceType = string.Empty;
            string GetProdPurpose = string.Empty;
            string GetPlantCode = string.Empty;
            string GetSlocCode = string.Empty;
            string GetLineCode = string.Empty;
            string GetQTY_N_1 = string.Empty;
            string GetQTY_N_2 = string.Empty;
            string GetQTY_N_3 = string.Empty;
            string GetQTY_N_4 = string.Empty;
            string GetQTY_N_5 = string.Empty;
            string GetQTY_N_6 = string.Empty;
            string GetQTY_N_7 = string.Empty;
            string GetQTY_N_8 = string.Empty;
            string GetQTY_N_9 = string.Empty;
            string GetQTY_N_10 = string.Empty;
            string GetQTY_N_11 = string.Empty;
            string GetQTY_N_12 = string.Empty;
            string GetQTY_N_13 = string.Empty;
            string GetQTY_N_14 = string.Empty;
            string GetQTY_N_15 = string.Empty;
            string GetQTY_N_16 = string.Empty;
            string GetQTY_N_17 = string.Empty;
            string GetQTY_N_18 = string.Empty;
            string GetQTY_N_19 = string.Empty;
            string GetQTY_N_20 = string.Empty;
            string GetQTY_N_21 = string.Empty;
            string GetQTY_N_22 = string.Empty;
            string GetQTY_N_23 = string.Empty;
            string GetQTY_N_24 = string.Empty;
            string GetQTY_N_25 = string.Empty;
            string GetQTY_N_26 = string.Empty;
            string GetQTY_N_27 = string.Empty;
            string GetQTY_N_28 = string.Empty;
            string GetQTY_N_29 = string.Empty;
            string GetQTY_N_30 = string.Empty;
            string GetQTY_N_31 = string.Empty;
            IRow Hrow;

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                GetProdMonth = result.PROD_MONTH;
                GetOrderType = result.ORDER_TYPE;
                GetParentPartNo = result.PARENT_PART_NO;
                GetParentSourceType = "3";
                GetParentProd = "S";
                GetParentPlantCode = result.PARENT_PLANT_CD;
                GetPartNo = result.PART_NO;
                GetSourceType = result.SOURCE_TYPE;
                GetProdPurpose = result.PROD_PURPOSE;
                GetPlantCode = result.PLANT_CD;
                GetSlocCode = result.SLOC_CD;
                GetLineCode = result.LINE_CD;
                GetQTY_N_1 = result.QTY_N_1.ToString();
                GetQTY_N_2 = result.QTY_N_2.ToString();
                GetQTY_N_3 = result.QTY_N_3.ToString();
                GetQTY_N_4 = result.QTY_N_4.ToString();
                GetQTY_N_5 = result.QTY_N_5.ToString();
                GetQTY_N_6 = result.QTY_N_6.ToString();
                GetQTY_N_7 = result.QTY_N_7.ToString();
                GetQTY_N_8 = result.QTY_N_8.ToString();
                GetQTY_N_9 = result.QTY_N_9.ToString();
                GetQTY_N_10 = result.QTY_N_10.ToString();
                GetQTY_N_11 = result.QTY_N_11.ToString();
                GetQTY_N_12 = result.QTY_N_12.ToString();
                GetQTY_N_13 = result.QTY_N_13.ToString();
                GetQTY_N_14 = result.QTY_N_14.ToString();
                GetQTY_N_15 = result.QTY_N_15.ToString();
                GetQTY_N_16 = result.QTY_N_16.ToString();
                GetQTY_N_17 = result.QTY_N_17.ToString();
                GetQTY_N_18 = result.QTY_N_18.ToString();
                GetQTY_N_19 = result.QTY_N_19.ToString();
                GetQTY_N_20 = result.QTY_N_20.ToString();
                GetQTY_N_21 = result.QTY_N_21.ToString();
                GetQTY_N_22 = result.QTY_N_22.ToString();
                GetQTY_N_23 = result.QTY_N_23.ToString();
                GetQTY_N_24 = result.QTY_N_24.ToString();
                GetQTY_N_25 = result.QTY_N_25.ToString();
                GetQTY_N_26 = result.QTY_N_26.ToString();
                GetQTY_N_27 = result.QTY_N_27.ToString();
                GetQTY_N_28 = result.QTY_N_28.ToString();
                GetQTY_N_29 = result.QTY_N_29.ToString();
                GetQTY_N_30 = result.QTY_N_30.ToString();
                GetQTY_N_31 = result.QTY_N_31.ToString();

                Hrow.CreateCell(1).SetCellValue(rowNumber);
                Hrow.CreateCell(2).SetCellValue(GetProdMonth);
                Hrow.CreateCell(3).SetCellValue(GetOrderType);
                Hrow.CreateCell(4).SetCellValue(GetParentPartNo);
                Hrow.CreateCell(5).SetCellValue(GetParentSourceType);
                Hrow.CreateCell(6).SetCellValue(GetParentProd);
                Hrow.CreateCell(7).SetCellValue(GetParentPlantCode);
                Hrow.CreateCell(8).SetCellValue(GetPartNo);
                Hrow.CreateCell(9).SetCellValue(GetSourceType);
                Hrow.CreateCell(10).SetCellValue(GetProdPurpose);
                Hrow.CreateCell(11).SetCellValue(GetPlantCode);
                Hrow.CreateCell(12).SetCellValue(GetSlocCode);
                Hrow.CreateCell(13).SetCellValue(GetLineCode);
                Hrow.CreateCell(14).SetCellValue(GetQTY_N_1);
                Hrow.CreateCell(15).SetCellValue(GetQTY_N_2);
                Hrow.CreateCell(16).SetCellValue(GetQTY_N_3);
                Hrow.CreateCell(17).SetCellValue(GetQTY_N_4);
                Hrow.CreateCell(18).SetCellValue(GetQTY_N_5);
                Hrow.CreateCell(19).SetCellValue(GetQTY_N_6);
                Hrow.CreateCell(20).SetCellValue(GetQTY_N_7);
                Hrow.CreateCell(21).SetCellValue(GetQTY_N_8);
                Hrow.CreateCell(22).SetCellValue(GetQTY_N_9);
                Hrow.CreateCell(23).SetCellValue(GetQTY_N_10);
                Hrow.CreateCell(24).SetCellValue(GetQTY_N_11);
                Hrow.CreateCell(25).SetCellValue(GetQTY_N_12);
                Hrow.CreateCell(26).SetCellValue(GetQTY_N_13);
                Hrow.CreateCell(27).SetCellValue(GetQTY_N_14);
                Hrow.CreateCell(28).SetCellValue(GetQTY_N_15);
                Hrow.CreateCell(29).SetCellValue(GetQTY_N_16);
                Hrow.CreateCell(30).SetCellValue(GetQTY_N_17);
                Hrow.CreateCell(31).SetCellValue(GetQTY_N_18);
                Hrow.CreateCell(32).SetCellValue(GetQTY_N_19);
                Hrow.CreateCell(33).SetCellValue(GetQTY_N_20);
                Hrow.CreateCell(34).SetCellValue(GetQTY_N_21);
                Hrow.CreateCell(35).SetCellValue(GetQTY_N_22);
                Hrow.CreateCell(36).SetCellValue(GetQTY_N_23);
                Hrow.CreateCell(37).SetCellValue(GetQTY_N_24);
                Hrow.CreateCell(38).SetCellValue(GetQTY_N_25);
                Hrow.CreateCell(39).SetCellValue(GetQTY_N_26);
                Hrow.CreateCell(40).SetCellValue(GetQTY_N_27);
                Hrow.CreateCell(41).SetCellValue(GetQTY_N_28);
                Hrow.CreateCell(42).SetCellValue(GetQTY_N_29);
                Hrow.CreateCell(43).SetCellValue(GetQTY_N_30);
                Hrow.CreateCell(44).SetCellValue(GetQTY_N_31);                 

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent;
                Hrow.GetCell(3).CellStyle = styleContent;
                Hrow.GetCell(4).CellStyle = styleContent;
                Hrow.GetCell(5).CellStyle = styleContent;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent;
                Hrow.GetCell(8).CellStyle = styleContent;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent;
                Hrow.GetCell(12).CellStyle = styleContent;
                Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(14).CellStyle = styleContent3;
                Hrow.GetCell(15).CellStyle = styleContent3;
                Hrow.GetCell(16).CellStyle = styleContent3;
                Hrow.GetCell(17).CellStyle = styleContent3;
                Hrow.GetCell(18).CellStyle = styleContent3;
                Hrow.GetCell(19).CellStyle = styleContent3;
                Hrow.GetCell(20).CellStyle = styleContent3;
                Hrow.GetCell(21).CellStyle = styleContent3;
                Hrow.GetCell(22).CellStyle = styleContent3;
                Hrow.GetCell(23).CellStyle = styleContent3;
                Hrow.GetCell(24).CellStyle = styleContent3;
                Hrow.GetCell(25).CellStyle = styleContent3;
                Hrow.GetCell(26).CellStyle = styleContent3;
                Hrow.GetCell(27).CellStyle = styleContent3;
                Hrow.GetCell(28).CellStyle = styleContent3;
                Hrow.GetCell(29).CellStyle = styleContent3;
                Hrow.GetCell(30).CellStyle = styleContent3;
                Hrow.GetCell(31).CellStyle = styleContent3;
                Hrow.GetCell(32).CellStyle = styleContent3;
                Hrow.GetCell(33).CellStyle = styleContent3;
                Hrow.GetCell(34).CellStyle = styleContent3;
                Hrow.GetCell(35).CellStyle = styleContent3;
                Hrow.GetCell(36).CellStyle = styleContent3;
                Hrow.GetCell(37).CellStyle = styleContent3;
                Hrow.GetCell(38).CellStyle = styleContent3;
                Hrow.GetCell(39).CellStyle = styleContent3;
                Hrow.GetCell(40).CellStyle = styleContent3;
                Hrow.GetCell(41).CellStyle = styleContent3;
                Hrow.GetCell(42).CellStyle = styleContent3;
                Hrow.GetCell(43).CellStyle = styleContent3;
                Hrow.GetCell(44).CellStyle = styleContent3;

                row++;
                rowNumber++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        public ActionResult _SPN232PartialCalculate()
        {
            return PartialView("_PartialCalculate");
        }

        public string SPN232CalculateProcess(string pc_ProdMonth, string pc_ParentPartNo)
        {
            mProcessID ModProcessID = new mProcessID();
            string ProcessId = ModProcessID.GetProcessID();

            string resultMessage = MNQC.SPN232CalculateProcess(pc_ProdMonth, pc_ParentPartNo, ProcessId);

            return resultMessage;
        }

    }
}
