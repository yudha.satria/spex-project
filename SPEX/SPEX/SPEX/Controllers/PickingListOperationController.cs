﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;
using NPOI.HSSF.UserModel; 

namespace SPEX.Controllers
{
    public class PickingListOperationController : PageController
    {

        //Model Maintenance Warehouse Master
        mPickingListOperation ms = new mPickingListOperation();
        MessagesString messageError = new MessagesString();


        public PickingListOperationController()
        {
            Settings.Title = "Picking List Operation";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }


      
       
        protected override void Startup()
        { 
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = messageError.getMSPX00009ERR();

            /*No data found*/
            ViewBag.MSPXS2006ERR = messageError.getMsgText("MSPXS2006ERR");
            /*Are you sure you want to confirm the operation?*/
            ViewBag.MSPXS2018INF = messageError.getMsgText("MSPXS2018INF");
           
            getpE_UserId = (User)ViewData["User"]; 
 
        }


        public ActionResult PickingListOperationSearchData(string pPICKING_LIST_NO, string pPART_NO, string pKANBAN_ID, string pSCAN_METHOD)
        {
            mPickingListOperation model = new mPickingListOperation(); 
            if (getpE_UserId != null)
            {
                string pUSER_LOGIN = getpE_UserId.Username;
                  
                model = ms.getListPickingListOperation(pPICKING_LIST_NO, pPART_NO, pKANBAN_ID, pSCAN_METHOD, pUSER_LOGIN);
                return Json(new { success = true, messages = model }, JsonRequestBehavior.AllowGet); 
            }
            else
            {
                return Json(new { success = false, messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
            } 
        } 

        public ActionResult PickingListOperationCallBack(string pPICKING_LIST_NO, string Mode)
        {
            List<mPickingListOperation> model = new List<mPickingListOperation>();
            if (Mode.Equals("Search")) {  
               // model = ms.getListPickingListOperation(pPICKING_LIST_NO );
            } 
            return PartialView("PickingListOperationGrid", model);
        } 

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2]; 
            }

            return result;
        }       
         

    }
}
