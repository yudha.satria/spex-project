﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class ReadyCargoInquiryController : PageController
    {
        //
        // GET: /ReadyCargoInquiry/
        MessagesString msgError = new MessagesString();
        public ReadyCargoInquiryController()
        {
            Settings.Title = "Ready Cargo Inquiry";
        }
        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }


        protected override void Startup()
        {
            //CallbackComboPriceTerm();

            //Call error message

            getpE_UserId = (User)ViewData["User"];
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Ready Cargo Inquiry");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            ViewData["BUYER"] = GetBuyer();
            ViewData["Invoice_Status"] = GetAllInvoiceStatus();
            ViewData["VANNING_DT_FROM"] = DateTime.Now.AddDays(-31);
            ViewData["VANNING_DT_TO"] = DateTime.Now;
            ViewData["Packing_Company"] = GetPackingCompany();

            ReadyCargoInquiry dat = new ReadyCargoInquiry();
            dat.MODE = "";

            ViewData["MODEL"] = dat;
            string session_name = "ReadyCargoInquirySearch";
            if (getpE_UserId.Username != null)
                session_name += getpE_UserId.Username;

            string p_MODE = GetParameter("p_MODE");
            if (p_MODE != null && Session[session_name] != null)
            {
                ReadyCargoInquiry dat2 = (ReadyCargoInquiry)Session[session_name];
                dat2.MODE = "R";
                ViewData["MODEL"] = dat2;
            }

            Session[session_name] = null;
        }
        public List<string> GetPackingCompany()
        {
            return new ReadyCargoInquiry().GetPackingCompany();
        }
        public List<BUYER> GetBuyer()
        {
            return new ReadyCargoInquiry().GetAllBuyerByBuyerPD();
        }

        public ActionResult GetPD()
        {
            ReadyCargoInquiry RC = new ReadyCargoInquiry();
            List<mPDCode> PDs = new List<mPDCode>();
            string BUYER_CD = string.Empty;
            if (Request.Params["BUYER_CD"] != null)
            {
                BUYER_CD = Request.Params["BUYER_CD"];
                PDs = RC.GetAllPDByBuyerPD(BUYER_CD);
            }
            ViewData["PDs"] = PDs;
            return PartialView("~/Views/ReadyCargoInquiry/_Partial_PD.cshtml");
            //return ViewData["PD_CD"] = RCs;
        }

        public List<ReadyCargoInquiry> GetAllInvoiceStatus()
        {

            return new ReadyCargoInquiry().GetAllInvoiceStatus();
        }

        public ActionResult ReadyCargoInquiryCallBack()
        {
            string BUYER_CD;
            string PD_CD = string.Empty;
            DateTime? VANNING_DT_FROM;
            DateTime? VANNING_DT_TO;
            string Invoice_Status = string.Empty;
            string INVOICE_NO = string.Empty;
            string VANNING_NO = string.Empty;
            DateTime? INVOICE_DT_FROM = null;
            DateTime? INVOICE_DT_TO = null;
            DateTime? ETD_FROM = null;
            DateTime? ETD_TO = null;
            string PACKING_COMPANY = string.Empty;
             //ADD AGI 2017-09-08 Insert field DG Cargo (same as Ready Cargo by Air) AND Searching menu by Case No
            string CASE_NO = string.Empty;


            BUYER_CD = Request.Params["BUYER_CD"];
            PD_CD = Request.Params["PD_CD"];
            VANNING_DT_FROM = Convert.ToDateTime(Request.Params["VANNING_DT_FROM"]);
            VANNING_DT_TO = Convert.ToDateTime(Request.Params["VANNING_DT_TO"]);
            Invoice_Status = Request.Params["Invoice_Status"];
            INVOICE_NO = Request.Params["INVOICE_NO"];
            VANNING_NO = Request.Params["VANNING_NO"];
            if (Request.Params["INVOICE_DT_FROM"] != string.Empty)
            {
                INVOICE_DT_FROM = Convert.ToDateTime(Request.Params["INVOICE_DT_FROM"]);
                INVOICE_DT_TO = Convert.ToDateTime(Request.Params["INVOICE_DT_TO"]);
            }
            if (Request.Params["ETD_FROM"] != string.Empty)
            {
                ETD_FROM = Convert.ToDateTime(Request.Params["ETD_FROM"]);
                ETD_TO = Convert.ToDateTime(Request.Params["ETD_TO"]);
            }
            PACKING_COMPANY = Request.Params["PACKING_COMPANY"];
            //ADD AGI 2017-09-08 Insert field DG Cargo (same as Ready Cargo by Air) AND Searching menu by Case No
            CASE_NO = Request.Params["CASE_NO"];

            string user_id = getpE_UserId.Username;
            List<ReadyCargoInquiry> model = new List<ReadyCargoInquiry>();
            ReadyCargoInquiry readyCargo = new ReadyCargoInquiry();
            
            //model = readyCargo.ReadyCargoInquirySearch(BUYER_CD, PD_CD, VANNING_DT_FROM, VANNING_DT_TO, Invoice_Status, INVOICE_NO, VANNING_NO, INVOICE_DT_FROM, INVOICE_DT_TO, ETD_FROM, ETD_TO, user_id, PACKING_COMPANY);
            //CHANGE AGI 2017-09-08 Insert field DG Cargo (same as Ready Cargo by Air) AND Searching menu by Case No
            model = readyCargo.ReadyCargoInquirySearch(BUYER_CD, PD_CD, VANNING_DT_FROM, VANNING_DT_TO, Invoice_Status, INVOICE_NO, VANNING_NO, INVOICE_DT_FROM, INVOICE_DT_TO, ETD_FROM, ETD_TO, user_id, PACKING_COMPANY,CASE_NO);

            return PartialView("_partial_Grid_ReadyCargo", model);
        }

        public ActionResult _PopUpLog(string BUYER_CD, string PD_CD, string CONTAINER_NO, string VANNING_DT, string PACKING_COMPANY)
        {
            ViewData["SetBUYER_CD"] = BUYER_CD;
            ViewData["SetPD_CD"] = PD_CD;
            ViewData["SetCONTAINER_NO"] = CONTAINER_NO;
            ViewData["SetVANNING_DT"] = Convert.ToDateTime(VANNING_DT);
            ViewData["SetPACKING_COMPANY"] = PACKING_COMPANY;
            List<ReadyCargoCheck> model = new List<ReadyCargoCheck>();
            string user_id = getpE_UserId.Username;
            ReadyCargoCheck readyCargo = new ReadyCargoCheck();
            model = readyCargo.GetReadyCargoCheck(BUYER_CD, PD_CD, CONTAINER_NO, Convert.ToDateTime(VANNING_DT), user_id, PACKING_COMPANY);
            ViewData["ReadyCargoCheck"] = model;
            return PartialView("_partial_Master_Check_ReadyCargo", model);
        }

        public ActionResult Get_Data_Grid_Check_ReadyCargo(string BUYER_CD, string PD_CD, string CONTAINER_NO, string VANNING_DT, string PACKING_COMPANY)
        {
            List<ReadyCargoCheck> model = new List<ReadyCargoCheck>();
            string user_id = getpE_UserId.Username;
            ReadyCargoCheck readyCargo = new ReadyCargoCheck();
            model = readyCargo.GetReadyCargoCheck(BUYER_CD, PD_CD, CONTAINER_NO, Convert.ToDateTime(VANNING_DT), user_id, PACKING_COMPANY);
            ViewData["ReadyCargoCheck"] = model;
            return PartialView("_partial_Grid_Check_ReadyCargo", model);
        }

        public ActionResult CreateInvoice(List<string> KeyList, string p_BUYER_CD, string p_PD_CD, string p_VANNING_DT_FROM,
                        string p_VANNING_DT_TO, string p_INVOICE_STATUS, string p_INVOICE_NO, string p_VANNING_NO,
                        string p_INVOICE_DT_FROM, string p_INVOICE_DT_TO, string p_ETD_FROM, string p_ETD_TO, string p_PACKING_COMPANY)
        {
            string message = string.Empty;
            int error = 0;
            //string URL = string.Empty;
            List<ReadyCargoInquiry> models = new List<ReadyCargoInquiry>();
            ReadyCargoInquiry model = new ReadyCargoInquiry();
            MessagesString Msg = new MessagesString();
            string listpdcode = string.Empty;
            foreach (var key in KeyList)
            {
                var keys = key.Split('|');
                model = new ReadyCargoInquiry();
                //INVOICE_NO;BUYER_CD;PD_CD;CONTAINER_NO;VANNING_DT;VANNING_DT_FROM;VANNING_DT_TO;SEAL_NO;PACKING_COMPANY
                model.BUYER_CD = keys[1].ToString();
                model.PD_CD = keys[2].ToString();
                model.CONTAINER_NO = keys[3].ToString();
                model.VANNING_DT = Convert.ToDateTime(keys[4]);
                model.VANNING_DT_FROM = Convert.ToDateTime(keys[5]);
                model.VANNING_DT_TO = Convert.ToDateTime(keys[6]);
                model.SEAL_NO = keys[7].ToString();
                model.PACKING_COMPANY = keys[8].ToString().ToUpper();
                model.SA = keys[9].ToString().ToUpper();
                model.FA = keys[10].ToString().ToUpper();
                model.ETD_STRING = keys[11].ToString().ToUpper();
                models.Add(model);
            }

            var grouping_packing_company = models.GroupBy(n => new { n.PACKING_COMPANY })
                .Select(g => new
                {
                    g.Key.PACKING_COMPANY
                }).ToList();
            if (grouping_packing_company.Count > 1)
            {
                message = Msg.getMsgText("MSPX00097ERR");//"Error : All selection must have same packing company";
                error = 1;
                return Json(new { success = "false", messages = message, error = error }, JsonRequestBehavior.AllowGet);
            }

            var grouping_pd_cd = models.GroupBy(n => new { n.BUYER_CD, n.PD_CD })
                .Select(g => new
                {
                    g.Key.BUYER_CD,
                    g.Key.PD_CD
                }).ToList();

            if (grouping_pd_cd.Count() > 1)
            {
                model.Check_PD_CD(models, out listpdcode, out error);
                if (error > 0)
                {
                    message = Msg.getMsgText("MSPX00098ERR");
                    message = message.Replace("[0]", listpdcode);
                    return Json(new { success = "false", messages = message }, JsonRequestBehavior.AllowGet);
                }
            }

            var grouping_sa = models.GroupBy(n => new { n.SA })
                .Select(g => new
                {
                    g.Key.SA
                }).ToList();
            if (grouping_sa.Count() > 1)
            {
                message = string.Format(Msg.getMsgText("MSPX00184ERR"), "SA");
                return Json(new { success = "false", messages = message }, JsonRequestBehavior.AllowGet);
            }

            var grouping_fa = models.GroupBy(n => new { n.FA })
                .Select(g => new
                {
                    g.Key.FA
                }).ToList();
            if (grouping_fa.Count() > 1)
            {
                message = string.Format(Msg.getMsgText("MSPX00184ERR"), "FA");
                return Json(new { success = "false", messages = message }, JsonRequestBehavior.AllowGet);
            }

            var grouping_etd = models.GroupBy(n => new { n.ETD_STRING })
                .Select(g => new
                {
                    g.Key.ETD_STRING
                }).ToList();
            if (grouping_etd.Count() > 1)
            {
                message = string.Format(Msg.getMsgText("MSPX00184ERR"), "ETD");
                return Json(new { success = "false", messages = message }, JsonRequestBehavior.AllowGet);
            }


            string session_name = "ReadyCargoInquiry" + getpE_UserId.Username;
            Session[session_name] = models;
            message = model.getUrlCreateInvoice();

            ReadyCargoInquiry searchParam = new ReadyCargoInquiry();

            searchParam.BUYER_CD = p_BUYER_CD;
            searchParam.PD_CD = p_PD_CD;
            searchParam.VANNING_DT_FROM = Convert.ToDateTime(p_VANNING_DT_FROM);
            searchParam.VANNING_DT_TO = Convert.ToDateTime(p_VANNING_DT_TO);
            searchParam.INVOICE_STATUS = p_INVOICE_STATUS;
            searchParam.INVOICE_NO = p_INVOICE_NO;
            searchParam.VANNING_NO = p_VANNING_NO;
            if (p_INVOICE_DT_FROM != string.Empty)
                searchParam.INVOICE_DT_FROM = Convert.ToDateTime(p_INVOICE_DT_FROM);

            if (p_INVOICE_DT_TO != string.Empty)
                searchParam.INVOICE_DT_TO = Convert.ToDateTime(p_INVOICE_DT_TO);

            if (p_ETD_FROM != string.Empty)
                searchParam.ETD_FROM = Convert.ToDateTime(p_ETD_FROM);

            if (p_ETD_TO != string.Empty)
                searchParam.ETD_TO = Convert.ToDateTime(p_ETD_TO);

            searchParam.PACKING_COMPANY = p_PACKING_COMPANY;

            string session_search_name = "ReadyCargoInquirySearch" + getpE_UserId.Username;
            Session[session_search_name] = searchParam;

            return Json(new { success = "true", messages = message }, JsonRequestBehavior.AllowGet);


            return null;
        }
        [HttpPost]
        public ActionResult ExportToXls()
        {
            //string BUYER_CD, string PD_CD, string CONTAINER_NO, string VANNING_DT, string PACKING_COMPANY
            List<ReadyCargoCheck> model = new List<ReadyCargoCheck>();
            //string user_id = getpE_UserId.Username;
            ReadyCargoCheck readyCargo = new ReadyCargoCheck();
            try
            {
                string BUYER_CD;
                string PD_CD = string.Empty;
                string CONTAINER_NO = string.Empty;
                string VANNING_DT;
                string PACKING_COMPANY = string.Empty;
                BUYER_CD = Request.Params["I_BUYER_CD_3"];
                PD_CD = Request.Params["I_PD_CD_3"];
                CONTAINER_NO = Request.Params["I_CONTAINER_NO_3"];
                VANNING_DT = Request.Params["I_VANNING_DT_3"].ToString().Replace(".", "-"); //Convert.ToDateTime(Request.Params["VANNING_DT"]);
                PACKING_COMPANY = Request.Params["I_PACKING_COMPANY_3"];
                string user_id = getpE_UserId.Username;
                model = readyCargo.GetReadyCargoCheck(BUYER_CD, PD_CD, CONTAINER_NO, Convert.ToDateTime(VANNING_DT), user_id, PACKING_COMPANY);
                GridViewExtension.WriteXlsxToResponse(CreateExportGridViewSettings(), model);
                //return Json(new { success = "true", messages = string.Empty }, JsonRequestBehavior.AllowGet);
                return View(model);
            }
            catch (Exception ex)
            {
                //return Json(new { success = "false", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return View(model);
        }

        static GridViewSettings CreateExportGridViewSettings()
        {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "Grid_Check_ReadyCargo";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("LEVEL");
            settings.Columns.Add("LOG");
            settings.Columns.Add("ITEM");

            return settings;
        }

        public ActionResult ViewInvoice(string p_BUYER_CD, string p_PD_CD, string p_VANNING_DT_FROM,
                        string p_VANNING_DT_TO, string p_INVOICE_STATUS, string p_INVOICE_NO, string p_VANNING_NO,
                        string p_INVOICE_DT_FROM, string p_INVOICE_DT_TO, string p_ETD_FROM, string p_ETD_TO, string p_PACKING_COMPANY)
        {
            string message = string.Empty;
            int error = 0;
            //string URL = string.Empty;
            ReadyCargoInquiry model = new ReadyCargoInquiry();

            message = model.getUrlCreateInvoice();

            ReadyCargoInquiry searchParam = new ReadyCargoInquiry();

            searchParam.BUYER_CD = p_BUYER_CD;
            searchParam.PD_CD = p_PD_CD;
            searchParam.VANNING_DT_FROM = Convert.ToDateTime(p_VANNING_DT_FROM);
            searchParam.VANNING_DT_TO = Convert.ToDateTime(p_VANNING_DT_TO);
            searchParam.INVOICE_STATUS = p_INVOICE_STATUS;
            searchParam.INVOICE_NO = p_INVOICE_NO;
            searchParam.VANNING_NO = p_VANNING_NO;
            if (p_INVOICE_DT_FROM != string.Empty)
                searchParam.INVOICE_DT_FROM = Convert.ToDateTime(p_INVOICE_DT_FROM);

            if (p_INVOICE_DT_TO != string.Empty)
                searchParam.INVOICE_DT_TO = Convert.ToDateTime(p_INVOICE_DT_TO);

            if (p_ETD_FROM != string.Empty)
                searchParam.ETD_FROM = Convert.ToDateTime(p_ETD_FROM);

            if (p_ETD_TO != string.Empty)
                searchParam.ETD_TO = Convert.ToDateTime(p_ETD_TO);

            searchParam.PACKING_COMPANY = p_PACKING_COMPANY;

            string session_search_name = "ReadyCargoInquirySearch" + getpE_UserId.Username;
            Session[session_search_name] = searchParam;

            return Json(new { success = "true", messages = message }, JsonRequestBehavior.AllowGet);


            return null;
        }
    }
}
