﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;
namespace SPEX.Controllers
{
    public class VESSEL_SCHEDULEController : PageController
    {
        //
        // GET: /BuyerPD/
        //mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();
        public string moduleID = "F15";
        public string functionID = "F15-009";
        public string location = "VESSEL SCHEDULE";
        Log log = new Log();
        public const string UploadDirectory = "Content\\FileUploadResult";
        public const string TemplateFName = "UPLOAD_VESSEL_SCHEDULE";
        public VESSEL_SCHEDULEController()
        {
            Settings.Title = "Vessel Schedule Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("VESSEL SCHEDULE Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            
            GetBuyyerForCombo();
            ViewData["Packing_Company"] = GetPackingCompany();
            getpE_UserId = (User)ViewData["User"];
            //add agi 2017-10-23
            string range = new mSystemMaster().GetSystemValueList("vessel schedule inquiry", "range day", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            ViewBag.range = Convert.ToInt32(range);
            ViewBag.MSPXV0001ERR = msgError.getMsgText("MSPXV0001ERR").Replace("{0}", range);
        }

        public List<string> GetPackingCompany()
        {

            return new VESSEL_SCHEDULE().GetPackingCompany();
        }

        public void GetBuyyerForCombo()
        {
            string BUYER_CD = string.Empty;
            string BUYER_NAME = string.Empty;
            List<BUYER> model = new List<BUYER>();
            model = GetListBUYER(BUYER_CD, BUYER_NAME);
            ViewData["BUYER"] = model;
        }
        public List<BUYER> GetListBUYER(string BUYER_CD, string BUYER_NAME)
        {

            List<BUYER> model = new List<BUYER>();
            BUYER BYR = new BUYER();
            BYR.BUYER_CD = string.IsNullOrEmpty(BUYER_CD) ? string.Empty : BUYER_CD;
            BYR.BUYER_NAME = string.IsNullOrEmpty(BUYER_CD) ? string.Empty : BUYER_NAME;
            model = BYR.getBUYER(BYR);
            //ViewData["BUYER"] = model;
            return model;
        }

        public ActionResult VESSEL_SCHEDULECallBack(string PACKING_COMPANY, string BUYER_CD, DateTime? VANNING_DT_FROM, DateTime? VANNING_DT_TO, DateTime? ETD_FROM, DateTime? ETD_TO)
        {
            List<VESSEL_SCHEDULE> model = new List<VESSEL_SCHEDULE>();
            model = new VESSEL_SCHEDULE().getVESSEL_SCHEDULE(PACKING_COMPANY, BUYER_CD, VANNING_DT_FROM, VANNING_DT_TO, ETD_FROM, ETD_TO);
            return PartialView("_VESSEL_SCHEDULEGrid", model);
        }

        public ActionResult UploadExcel(HttpPostedFileBase fileexcel)
        {
            DeleteAllOlderFile(HttpContext.Request.MapPath("~/Content/FileUploadResult/"));

            string FileName = fileexcel.FileName;
            //string fileName = Path.Combine(fileexcel.FileName);
            long pid = 0;
            string batchName = "SynchronizeBatch";
            string moduleID = "1";
            string functionID = "10011";
            string subFuncName = "UPLOAD DATA";
            string userBatch = getpE_UserId.Username;
            string location = "VESSEL_SCHEDULE";
            Log log = new Log();
            pid = log.createLog(msgError.getMSPX00005INFForLog(batchName), userBatch, location + "." + subFuncName, 0, moduleID, functionID);
            var path_file = Path.Combine(Server.MapPath("~/FileUpload/RunDown/UploadCustomVESSELScreen/CustomVESSEL"), FileName);
            string customVESSEL_dir = Server.MapPath("~/FileUpload/RunDown/UploadCustomVESSELScreen/CustomVESSEL");
            if (Directory.Exists(customVESSEL_dir))
            {
                fileexcel.SaveAs(path_file);
            }

            return RedirectToAction("");
        }


        // //function download      
        // public void DownloadBuyerPD(object sender, EventArgs e, string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION)
        // {
        //     string filename = "";
        //     string filesTmp = HttpContext.Request.MapPath("~/Template/BuyerPD_Code_Download.xls");
        //     FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

        //     HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

        //     ICellStyle styleContent = workbook.CreateCellStyle();
        //     styleContent.VerticalAlignment = VerticalAlignment.Top;
        //     styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent.Alignment = HorizontalAlignment.Left;

        //     ICellStyle styleContent2 = workbook.CreateCellStyle();
        //     styleContent2.VerticalAlignment = VerticalAlignment.Top;
        //     styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent2.Alignment = HorizontalAlignment.Center;

        //     ICellStyle styleContent3 = workbook.CreateCellStyle();
        //     styleContent3.VerticalAlignment = VerticalAlignment.Top;
        //     styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //     styleContent3.Alignment = HorizontalAlignment.Right;

        //     ISheet sheet = workbook.GetSheet("BuyerPD Code");
        //     string date = DateTime.Now.ToString("ddMMyyyy");
        //     filename = "BuyerPD Code Master" + date + ".xls";

        //     string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

        //     sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
        //     sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);
        //     sheet.GetRow(8).GetCell(2).SetCellValue(pBUYER_PD_CD);
        //     sheet.GetRow(9).GetCell(2).SetCellValue(pCUSTOMER_NO);
        //     sheet.GetRow(10).GetCell(2).SetCellValue(pDESCRIPTION);


        //     int row = 13;
        //     int rowNum = 1;
        //     IRow Hrow;

        //     List<mBuyerPD> model = new List<mBuyerPD>();

        //     model = BuyerPD.getListBuyerPD
        //         (pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION);

        //     foreach (var result in model)
        //     {
        //         Hrow = sheet.CreateRow(row);

        //         Hrow.CreateCell(1).SetCellValue(rowNum);
        //         Hrow.CreateCell(2).SetCellValue(result.BUYER_PD_CD);
        //         Hrow.CreateCell(3).SetCellValue(result.CUSTOMER_NO);
        //         Hrow.CreateCell(4).SetCellValue(result.DESCRIPTION);
        //         Hrow.CreateCell(5).SetCellValue(result.DELETION_FLAG);
        //         Hrow.CreateCell(6).SetCellValue(result.CREATED_BY);
        //         Hrow.CreateCell(7).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
        //         Hrow.CreateCell(8).SetCellValue(result.CHANGED_BY);
        //         Hrow.CreateCell(9).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

        //         Hrow.GetCell(1).CellStyle = styleContent3;
        //         Hrow.GetCell(2).CellStyle = styleContent2;
        //         Hrow.GetCell(3).CellStyle = styleContent2;
        //         Hrow.GetCell(4).CellStyle = styleContent;
        //         Hrow.GetCell(5).CellStyle = styleContent2;
        //         Hrow.GetCell(6).CellStyle = styleContent2;
        //         Hrow.GetCell(7).CellStyle = styleContent2;
        //         Hrow.GetCell(8).CellStyle = styleContent2;
        //         Hrow.GetCell(9).CellStyle = styleContent2;

        //         row++;
        //         rowNum++;
        //     }

        //     MemoryStream ms = new MemoryStream();
        //     workbook.Write(ms);
        //     ftmp.Close();
        //     Response.BinaryWrite(ms.ToArray());
        //     Response.ContentType = "application/vnd.ms-excel";
        //     Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        // }

        // #region Fucntion Delete Data
        public ActionResult DeleteVESSEL_SCHEDULE(List<string> keyValues)
        {

            VESSEL_SCHEDULE Ship = new VESSEL_SCHEDULE();

            string resultMessage = Ship.DeleteDataTBR(keyValues);
            //if (resultMessage == "Error ")
            //{
            //  return Json(new { success = "true",messages = resultMessage }, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            return Json(new { success = "true", messages = resultMessage }, JsonRequestBehavior.AllowGet);
            //}

            return null;
        }
        //#endregion 

        // #region Function Add
        //public ActionResult AddNewVESSEL_AGENT(string SHIP_AGENT_CD, string SHIP_AGENT_NAME, string PIC_NAME, string CONTACT_NO, string DESCR1, string DESCR2, string DESCR3)
        //{
        //    //p_CREATED_BY = getpE_UserId.Username;
        //    //p_CREATED_BY = "SYSTEM";
        //    string[] r = null;
        //    VESSEL_AGENT ship = new VESSEL_AGENT();
        //    ship.SHIP_AGENT_CD = SHIP_AGENT_CD;
        //    ship.SHIP_AGENT_NAME = SHIP_AGENT_NAME;
        //    ship.PIC_NAME = PIC_NAME;
        //    ship.CONTACT_NO = CONTACT_NO;
        //    ship.DESCR1 = DESCR1;
        //    ship.DESCR2 = DESCR2;
        //    ship.DESCR3 = DESCR3;
        //    ship.CREATED_BY = getpE_UserId.Username;
        //    string resultMessage = ship.SaveData(ship);

        //    r = resultMessage.Split('|');
        //    if (r[0] == "Error ")
        //    {
        //        return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
        //    }
        //    return null;
        //}
        //#endregion
        //public void DoWork(object obj)
        //{
        //    // do some work
        //    string[] objs = obj.ToString().Split('|');
        //    long pid=Convert.ToInt64(objs[0]);
        //    string user_id =objs[1].ToString(); //getpE_UserId.Username;
        //    new VESSEL_SCHEDULE().Save_Master_VESSEL_SCHEDULE(Convert.ToInt64(pid), user_id,functionID,moduleID);
        //}

        //#region Function Edit -
        public ActionResult UploadBackgroundProsess()
        {
            string message = string.Empty;
            try
            {
                string user_id = getpE_UserId.Username;
                string subFuncName = "EXcelToSQL";
                long pid = log.createLog(msgError.getMSPX00005INFForLog("UPLOAD VESSEL SCHEDULE"), user_id, location + '.' + subFuncName, 0, moduleID, functionID);
                //Thread worker = new Thread(DoWork);
                //worker.IsBackground = true;
                //worker.SetApartmentState(System.Threading.ApartmentState.STA);
                //worker.Start(pid.ToString()+"|"+user_id);
                //message = "Upload Success, and data will be process. To see progress, please see in log detail with process id:"+pid;

                new VESSEL_SCHEDULE().Save_Master_VESSEL_SCHEDULE(pid, user_id, functionID, moduleID);
                message = msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2];
                return Json(new { success = "true", messages = message + '|' + pid }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "false", messages = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }
        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "Price_Master_Upload";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xlsx" },
                MaxFileSize = 20971520
            };
        }

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            VESSEL_SCHEDULE Ship = new VESSEL_SCHEDULE();

            string[] r = null;
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_VESSEL_SCHEDULE";
            #region createlog
            Guid PROCESS_ID = Guid.NewGuid();
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            string MESSAGE_DETAIL = "";
            long pid = 0;
            //string subFuncName = "EXcelToSQL";
            string isError = "N";
            string processName = "VESSEL SCHEDULE UPLOAD";
            string resultMessage = "";

            int columns;
            int rows;

            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock L = new Lock();

            int IsLock = L.is_lock(functionID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMsgText("MSPX00092ERR");
            }
            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = Ship.DeleteDataTBT();
                if (rExtract == "SUCCES")
                {
                    string user_id = getpE_UserId.Username;

                    L.CREATED_BY = user_id;
                    L.CREATED_DT = DateTime.Now;
                    L.PROCESS_ID = pid;
                    L.FUNCTION_ID = functionID;
                    L.LOCK_REF = functionID;
                    L.LockFunction(L);
                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    //Function untuk nge-extract file excel
                    rExtract = ReadAndInsertExcelData(pathfile);
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                    if (rExtract == string.Empty || rExtract == "SUCCESS")
                    {
                        rExtract = Ship.CheckExistingVESSELSchedule();
                    }
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                //MESSAGE_DETAIL = "File is not .xls";
                //pid = log.createLog(msgError.getMSPX00019ERR("Price"), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                rExtract = "1| File is not .xls OR xlsx";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract;//+ "|" + pid.ToString();
        }

        public void UnLock()
        {
            Lock L = new Lock();
            L.UnlockFunction(functionID);
        }

        public ActionResult IsCheckLock()
        {
            Lock L = new Lock();
            int Lock = L.is_lock(functionID);

            if (Lock == 0)
            {
                return Json(new { success = "true", messages = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string msg = string.Empty;
                msg = msgError.getMsgText("MSPX00088ERR");
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        private void DeleteAllOlderFile(string folderPath)
        {
            IEnumerable<string> allFiles = new List<string>();
            allFiles = System.IO.Directory.EnumerateFiles(folderPath, "*.*");
            foreach (string file in allFiles)
            {
                FileInfo info = new FileInfo(file);
                if (info.CreationTime < DateTime.Now.AddDays(-3))
                    System.IO.File.Delete(file);
            }
        }

        private string ReadAndInsertExcelData(string FileName)
        {
            try
            {
                XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                ISheet sheet = xssfworkbook.GetSheetAt(0);
                int lastrow = sheet.LastRowNum;
                List<VESSEL_SCHEDULE.VESSEL_SCHEDULE_UPLOAD> listVessel = new List<VESSEL_SCHEDULE.VESSEL_SCHEDULE_UPLOAD>();
                int index = 1;

                VESSEL_SCHEDULE.VESSEL_SCHEDULE_UPLOAD vessel;
                for (int i = index; i <= lastrow; i++)
                {
                    vessel = new VESSEL_SCHEDULE.VESSEL_SCHEDULE_UPLOAD();
                    IRow row = sheet.GetRow(i);

                    if (row == null) continue;
                    if (row.GetCell(0) == null && row.GetCell(1) == null && row.GetCell(2) == null && row.GetCell(3) == null) continue;

                    //part no
                    if (row.GetCell(0) == null) vessel.PACKING_COMPANY = "";
                    else if (row.GetCell(0).CellType == CellType.Blank || (row.GetCell(0).CellType == CellType.String && (row.GetCell(0).StringCellValue == null || row.GetCell(0).StringCellValue == "")))
                        vessel.PACKING_COMPANY = "";
                    else if (row.GetCell(0).CellType == CellType.Numeric && (row.GetCell(0).NumericCellValue.ToString() == ""))
                        vessel.PACKING_COMPANY = "";
                    else if (row.GetCell(0).CellType == CellType.String)
                        vessel.PACKING_COMPANY = row.GetCell(0).StringCellValue;
                    else if (row.GetCell(0).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(0);
                        string formatString = row.GetCell(0).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.PACKING_COMPANY = row.GetCell(0).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.PACKING_COMPANY = row.GetCell(0).NumericCellValue.ToString();
                    }
                    else
                        vessel.PACKING_COMPANY = row.GetCell(0).ToString();

                    if (row.GetCell(1) == null) vessel.BUYER_CD = "";
                    else if (row.GetCell(1).CellType == CellType.Blank || (row.GetCell(1).CellType == CellType.String && (row.GetCell(1).StringCellValue == null || row.GetCell(1).StringCellValue == "")))
                        vessel.BUYER_CD = "";
                    else if (row.GetCell(1).CellType == CellType.Numeric && (row.GetCell(1).NumericCellValue.ToString() == ""))
                        vessel.BUYER_CD = "";
                    else if (row.GetCell(1).CellType == CellType.String)
                        vessel.BUYER_CD = row.GetCell(1).StringCellValue;
                    else if (row.GetCell(1).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(1);
                        string formatString = row.GetCell(1).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.BUYER_CD = row.GetCell(1).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.BUYER_CD = row.GetCell(1).NumericCellValue.ToString();
                    }
                    else
                        vessel.BUYER_CD = row.GetCell(1).ToString();

                    if (row.GetCell(2) == null) vessel.FEEDER_VESSEL = "";
                    else if (row.GetCell(2).CellType == CellType.Blank || (row.GetCell(2).CellType == CellType.String && (row.GetCell(2).StringCellValue == null || row.GetCell(2).StringCellValue == "")))
                        vessel.FEEDER_VESSEL = "";
                    else if (row.GetCell(2).CellType == CellType.Numeric && (row.GetCell(2).NumericCellValue.ToString() == ""))
                        vessel.FEEDER_VESSEL = "";
                    else if (row.GetCell(2).CellType == CellType.String)
                        vessel.FEEDER_VESSEL = row.GetCell(2).StringCellValue;
                    else if (row.GetCell(2).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(2);
                        string formatString = row.GetCell(2).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.FEEDER_VESSEL = row.GetCell(2).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.FEEDER_VESSEL = row.GetCell(2).NumericCellValue.ToString();
                    }
                    else
                        vessel.FEEDER_VESSEL = row.GetCell(2).ToString();

                    if (row.GetCell(3) == null) vessel.FEEDER_VOYAGE = "";
                    else if (row.GetCell(3).CellType == CellType.Blank || (row.GetCell(3).CellType == CellType.String && (row.GetCell(3).StringCellValue == null || row.GetCell(3).StringCellValue == "")))
                        vessel.FEEDER_VOYAGE = "";
                    else if (row.GetCell(3).CellType == CellType.Numeric && (row.GetCell(3).NumericCellValue.ToString() == ""))
                        vessel.FEEDER_VOYAGE = "";
                    else if (row.GetCell(3).CellType == CellType.String)
                        vessel.FEEDER_VOYAGE = row.GetCell(3).StringCellValue;
                    else if (row.GetCell(3).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(3);
                        string formatString = row.GetCell(3).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.FEEDER_VOYAGE = row.GetCell(3).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.FEEDER_VOYAGE = row.GetCell(3).NumericCellValue.ToString();
                    }
                    else
                        vessel.FEEDER_VOYAGE = row.GetCell(3).ToString();

                    if (row.GetCell(4) == null) vessel.CONNECT_VESSEL = "";
                    else if (row.GetCell(4).CellType == CellType.Blank || (row.GetCell(4).CellType == CellType.String && (row.GetCell(4).StringCellValue == null || row.GetCell(4).StringCellValue == "")))
                        vessel.CONNECT_VESSEL = "";
                    else if (row.GetCell(4).CellType == CellType.Numeric && (row.GetCell(4).NumericCellValue.ToString() == ""))
                        vessel.CONNECT_VESSEL = "";
                    else if (row.GetCell(4).CellType == CellType.String)
                        vessel.CONNECT_VESSEL = row.GetCell(4).StringCellValue;
                    else if (row.GetCell(4).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(4);
                        string formatString = row.GetCell(4).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.CONNECT_VESSEL = row.GetCell(4).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.CONNECT_VESSEL = row.GetCell(4).NumericCellValue.ToString();
                    }
                    else
                        vessel.CONNECT_VESSEL = row.GetCell(4).ToString();

                    if (row.GetCell(5) == null) vessel.CONNECT_VOYAGE = "";
                    else if (row.GetCell(5).CellType == CellType.Blank || (row.GetCell(5).CellType == CellType.String && (row.GetCell(5).StringCellValue == null || row.GetCell(5).StringCellValue == "")))
                        vessel.CONNECT_VOYAGE = "";
                    else if (row.GetCell(5).CellType == CellType.Numeric && (row.GetCell(5).NumericCellValue.ToString() == ""))
                        vessel.CONNECT_VOYAGE = "";
                    else if (row.GetCell(5).CellType == CellType.String)
                        vessel.CONNECT_VOYAGE = row.GetCell(5).StringCellValue;
                    else if (row.GetCell(5).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(5);
                        string formatString = row.GetCell(5).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.CONNECT_VOYAGE = row.GetCell(5).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.CONNECT_VOYAGE = row.GetCell(5).NumericCellValue.ToString();
                    }
                    else
                        vessel.CONNECT_VOYAGE = row.GetCell(5).ToString();


                    if (row.GetCell(6) == null) vessel.ETD = "";
                    else if (row.GetCell(6).CellType == CellType.Blank || (row.GetCell(6).CellType == CellType.String && (row.GetCell(6).StringCellValue == null || row.GetCell(6).StringCellValue == "")))
                        vessel.ETD = "";
                    else if (row.GetCell(6).CellType == CellType.Numeric && (row.GetCell(6).NumericCellValue.ToString() == ""))
                        vessel.ETD = "";
                    else if (row.GetCell(6).CellType == CellType.String)
                        vessel.ETD = row.GetCell(6).StringCellValue;
                    else if (row.GetCell(6).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(6);
                        string formatString = row.GetCell(6).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.ETD = row.GetCell(6).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.ETD = row.GetCell(6).NumericCellValue.ToString();
                    }
                    else
                        vessel.ETD = row.GetCell(6).ToString();

                    if (row.GetCell(7) == null) vessel.SHIP_AGENT_CD = "";
                    else if (row.GetCell(7).CellType == CellType.Blank || (row.GetCell(7).CellType == CellType.String && (row.GetCell(7).StringCellValue == null || row.GetCell(7).StringCellValue == "")))
                        vessel.SHIP_AGENT_CD = "";
                    else if (row.GetCell(7).CellType == CellType.Numeric && (row.GetCell(7).NumericCellValue.ToString() == ""))
                        vessel.SHIP_AGENT_CD = "";
                    else if (row.GetCell(7).CellType == CellType.String)
                        vessel.SHIP_AGENT_CD = row.GetCell(7).StringCellValue;
                    else if (row.GetCell(7).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(7);
                        string formatString = row.GetCell(7).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.SHIP_AGENT_CD = row.GetCell(7).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.SHIP_AGENT_CD = row.GetCell(7).NumericCellValue.ToString();
                    }
                    else
                        vessel.SHIP_AGENT_CD = row.GetCell(7).ToString();

                    if (row.GetCell(8) == null) vessel.VANNING_DT = "";
                    else if (row.GetCell(8).CellType == CellType.Blank || (row.GetCell(8).CellType == CellType.String && (row.GetCell(8).StringCellValue == null || row.GetCell(8).StringCellValue == "")))
                        vessel.VANNING_DT = "";
                    else if (row.GetCell(8).CellType == CellType.Numeric && (row.GetCell(8).NumericCellValue.ToString() == ""))
                        vessel.VANNING_DT = "";
                    else if (row.GetCell(8).CellType == CellType.String)
                        vessel.VANNING_DT = row.GetCell(8).StringCellValue;
                    else if (row.GetCell(8).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(8);
                        string formatString = row.GetCell(8).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.VANNING_DT = row.GetCell(8).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.VANNING_DT = row.GetCell(8).NumericCellValue.ToString();
                    }
                    else
                        vessel.VANNING_DT = row.GetCell(8).ToString();

                    if (row.GetCell(9) == null) vessel.ETA = "";
                    else if (row.GetCell(9).CellType == CellType.Blank || (row.GetCell(9).CellType == CellType.String && (row.GetCell(9).StringCellValue == null || row.GetCell(9).StringCellValue == "")))
                        vessel.ETA = "";
                    else if (row.GetCell(9).CellType == CellType.Numeric && (row.GetCell(9).NumericCellValue.ToString() == ""))
                        vessel.ETA = "";
                    else if (row.GetCell(9).CellType == CellType.String)
                        vessel.ETA = row.GetCell(9).StringCellValue;
                    else if (row.GetCell(9).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(9);
                        string formatString = row.GetCell(9).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.ETA = row.GetCell(9).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.ETA = row.GetCell(9).NumericCellValue.ToString();
                    }
                    else
                        vessel.ETA = row.GetCell(9).ToString();

                    if (row.GetCell(10) == null) vessel.CONTAINER_PLAN = "";
                    else if (row.GetCell(10).CellType == CellType.Blank || (row.GetCell(10).CellType == CellType.String && (row.GetCell(10).StringCellValue == null || row.GetCell(10).StringCellValue == "")))
                        vessel.CONTAINER_PLAN = "";
                    else if (row.GetCell(10).CellType == CellType.Numeric && (row.GetCell(10).NumericCellValue.ToString() == ""))
                        vessel.CONTAINER_PLAN = "";
                    else if (row.GetCell(10).CellType == CellType.String)
                        vessel.CONTAINER_PLAN = row.GetCell(10).StringCellValue;
                    else if (row.GetCell(10).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(10);
                        string formatString = row.GetCell(10).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.CONTAINER_PLAN = row.GetCell(10).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.CONTAINER_PLAN = row.GetCell(10).NumericCellValue.ToString();
                    }
                    else
                        vessel.CONTAINER_PLAN = row.GetCell(10).ToString();

                    if (row.GetCell(11) == null) vessel.FORWARD_AGENT_CD = "";
                    else if (row.GetCell(11).CellType == CellType.Blank || (row.GetCell(11).CellType == CellType.String && (row.GetCell(11).StringCellValue == null || row.GetCell(11).StringCellValue == "")))
                        vessel.FORWARD_AGENT_CD = "";
                    else if (row.GetCell(11).CellType == CellType.Numeric && (row.GetCell(11).NumericCellValue.ToString() == ""))
                        vessel.FORWARD_AGENT_CD = "";
                    else if (row.GetCell(11).CellType == CellType.String)
                        vessel.FORWARD_AGENT_CD = row.GetCell(11).StringCellValue;
                    else if (row.GetCell(11).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(11);
                        string formatString = row.GetCell(11).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.FORWARD_AGENT_CD = row.GetCell(11).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.FORWARD_AGENT_CD = row.GetCell(11).NumericCellValue.ToString();
                    }
                    else
                        vessel.FORWARD_AGENT_CD = row.GetCell(11).ToString();

                    if (row.GetCell(12) == null) vessel.BOOKING_NO = "";
                    else if (row.GetCell(12).CellType == CellType.Blank || (row.GetCell(12).CellType == CellType.String && (row.GetCell(12).StringCellValue == null || row.GetCell(12).StringCellValue == "")))
                        vessel.BOOKING_NO = "";
                    else if (row.GetCell(12).CellType == CellType.Numeric && (row.GetCell(12).NumericCellValue.ToString() == ""))
                        vessel.BOOKING_NO = "";
                    else if (row.GetCell(12).CellType == CellType.String)
                        vessel.BOOKING_NO = row.GetCell(12).StringCellValue;
                    else if (row.GetCell(12).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(12);
                        string formatString = row.GetCell(12).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.BOOKING_NO = row.GetCell(12).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.BOOKING_NO = row.GetCell(12).NumericCellValue.ToString();
                    }
                    else
                        vessel.BOOKING_NO = row.GetCell(12).ToString();

                    if (row.GetCell(13) == null) vessel.CLOSING_TM = "";
                    else if (row.GetCell(13).CellType == CellType.Blank || (row.GetCell(13).CellType == CellType.String && (row.GetCell(13).StringCellValue == null || row.GetCell(13).StringCellValue == "")))
                        vessel.CLOSING_TM = "";
                    else if (row.GetCell(13).CellType == CellType.Numeric && (row.GetCell(13).NumericCellValue.ToString() == ""))
                        vessel.CLOSING_TM = "";
                    else if (row.GetCell(13).CellType == CellType.String)
                        vessel.CLOSING_TM = row.GetCell(13).StringCellValue;
                    else if (row.GetCell(13).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(13);
                        string formatString = row.GetCell(13).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            vessel.CLOSING_TM = row.GetCell(13).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            vessel.CLOSING_TM = row.GetCell(13).NumericCellValue.ToString();
                    }
                    else
                        vessel.CLOSING_TM = row.GetCell(13).ToString();

                    listVessel.Add(vessel);
                }

                foreach (VESSEL_SCHEDULE.VESSEL_SCHEDULE_UPLOAD data in listVessel)
                {
                    string insertResult = (new VESSEL_SCHEDULE()).InsertTempVessel(data);
                }

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return "1|" + ex.Message;
            }
        }
    }
}
