﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class PackingCompletionByAirController : PageController
    {
        mPackingCompletionByAir packing = new mPackingCompletionByAir();

        public PackingCompletionByAirController()
        {
            Settings.Title = "Packaging Completion [By Air]";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            ViewData["I_CASE_NO"] = packing.GetNewCaseNo();
            if (getpE_UserId != null)
                packing.DeleteTempPart(getpE_UserId.Username);
        }

        [HttpPost]
        public ActionResult GetCaseContentGrid(string p_CASE_NO)
        {
            List<mPackingCompletionByAir> model = new List<mPackingCompletionByAir>();
            string UserID = getpE_UserId.Username;

            if (p_CASE_NO == "")
                ViewData["CASE_NO"] = packing.GetNewCaseNo();

            model = packing.GetCaseContent(p_CASE_NO, UserID);

            return PartialView("CaseContentGrid", model);
        }

        public ActionResult SavePart(string p_CASE_NO, string p_ORDER_NO, string p_ITEM_NO, string p_PART_NO, string p_PART_QTY)
        {
            string UserID = getpE_UserId.Username;

            string resultMessage = packing.SavePart(p_CASE_NO, p_ORDER_NO, p_ITEM_NO, p_PART_NO, GetDecimal(p_PART_QTY), UserID);
            string[] r = null;
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("success"))
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveCase(string p_CASE_NO, string p_CASE_GROSS_WEIGHT, string p_CASE_GROSS_WEIGHT_POINT,
        string p_CASE_NET_WEIGHT, string p_CASE_NET_WEIGHT_POINT, string p_CASE_DANGER_FLAG, string p_CASE_TYPE, string p_CASE_WIDTH,
        string p_CASE_WIDTH_POINT, string p_CASE_HEIGHT, string p_CASE_HEIGHT_POINT, string p_CASE_LENGTH, string p_CASE_LENGTH_POINT)
        {
            string UserID = getpE_UserId.Username;

            string resultMessage = packing.SaveCase(p_CASE_NO, GetDecimal(p_CASE_GROSS_WEIGHT, p_CASE_GROSS_WEIGHT_POINT),
        GetDecimal(p_CASE_NET_WEIGHT, p_CASE_NET_WEIGHT_POINT), p_CASE_DANGER_FLAG, p_CASE_TYPE, GetDecimal(p_CASE_WIDTH, p_CASE_WIDTH_POINT),
        GetDecimal(p_CASE_HEIGHT, p_CASE_HEIGHT_POINT), GetDecimal(p_CASE_LENGTH, p_CASE_LENGTH_POINT), UserID);
            string[] r = null;
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("success"))
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
        }

        private decimal GetDecimal(string strValue)
        {
            decimal returnValue = 0;
            returnValue = Convert.ToDecimal(strValue);
            return returnValue;
        }

        private decimal GetDecimal(string strValue, string strPoint)
        {
            decimal returnValue = 0;
            string newValue = strValue + "." + strPoint;
            returnValue = Convert.ToDecimal(newValue);
            return returnValue;
        }

        public string GetNewCaseNo()
        {
            return packing.GetNewCaseNo();
        }

        public ActionResult DeletePartRow(string p_CASE_NO, string p_ORDER_NO, string p_ITEM_NO, string p_PART_NO)
        {
            string UserID = getpE_UserId.Username;

            string resultMessage = packing.DeleteTempPartRow(p_CASE_NO, p_ORDER_NO, p_ITEM_NO, p_PART_NO, UserID);
            string[] r = null;
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("success"))
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
        }
    }
}
