﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class FORWARDING_AGENTController : PageController
    {
        //
        // GET: /BuyerPD/
        //mBuyerPD BuyerPD = new mBuyerPD();
        MessagesString msgError = new MessagesString();

        public FORWARDING_AGENTController()
        {
            Settings.Title = "Forwarding Agent";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //CallbackComboPriceTerm();

            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Forwarding Agent Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        public ActionResult FORWARDING_AGENTCallBack(string FORWARD_AGENT_CD, string FORWARD_AGENT_NAME, string PIC_NAME)
        {
            if (FORWARD_AGENT_CD != null) 
                if (FORWARD_AGENT_CD.Contains("\""))
            {
                FORWARD_AGENT_CD = "";
                FORWARD_AGENT_NAME = "";
                PIC_NAME = "";
            } 
            
            FORWARDING_AGENT fwd = new FORWARDING_AGENT();
            fwd.FORWARD_AGENT_CD = FORWARD_AGENT_CD;
            fwd.FORWARD_AGENT_NAME = FORWARD_AGENT_NAME;
            fwd.PIC_NAME = PIC_NAME;
            List<FORWARDING_AGENT> model = new List<FORWARDING_AGENT>();
            model = fwd.getFORWARDING_AGENTList(fwd);

            return PartialView("_FORWARDING_AGENTGrid", model);
        }


        public ActionResult DeleteFORWARDING_AGENT(string PARAMS)
        {
            string USER_ID = getpE_UserId.Username;
            ////p_CHANGED_BY = "SYSTEM";
            FORWARDING_AGENT fwd = new FORWARDING_AGENT();
            //fwd.FORWARD_AGENT_CD = FORWARD_AGENT_CD;

            string[] r = null;
            string resultMessage = fwd.DeleteData(PARAMS, USER_ID);

            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error "))
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }

            return null;
        }
        //#endregion 

        // #region Function Add
        public ActionResult AddNewFORWARDING_AGENT(string FORWARD_AGENT_CD, string FORWARD_AGENT_NAME, string PIC_NAME, string CONTACT_NO, string DESCR1, string DESCR2, string DESCR3,
             string MESSRS_1, string MESSRS_2, string MESSRS_3, string TYPE)
        {
            //p_CREATED_BY = getpE_UserId.Username;
            //p_CREATED_BY = "SYSTEM";
            string[] r = null;
            FORWARDING_AGENT fwd = new FORWARDING_AGENT();
            fwd.FORWARD_AGENT_CD = FORWARD_AGENT_CD;
            fwd.FORWARD_AGENT_NAME = FORWARD_AGENT_NAME;
            fwd.PIC_NAME = PIC_NAME;
            fwd.CONTACT_NO = CONTACT_NO;
            fwd.DESCR1 = DESCR1;
            fwd.DESCR2 = DESCR2;
            fwd.DESCR3 = DESCR3;
            fwd.MESSRS_1 = MESSRS_1;
            fwd.MESSRS_2 = MESSRS_2;
            fwd.MESSRS_3 = MESSRS_3;
            fwd.TYPE = TYPE;
            fwd.CREATED_BY = getpE_UserId.Username;
            string resultMessage = fwd.SaveData(fwd);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion

        //#region Function Edit -
        public ActionResult UpdateFORWARDING_AGENT(string FORWARD_AGENT_CD, string FORWARD_AGENT_NAME, string PIC_NAME, string CONTACT_NO, string DESCR1, string DESCR2, string DESCR3,
            string MESSRS_1, string MESSRS_2, string MESSRS_3, string TYPE)
        {
            //string p_CHANGED_BY = getpE_UserId.Username;
            //pCHANGED_BY = "SYSTEM";
            string[] r = null;
            FORWARDING_AGENT fwd = new FORWARDING_AGENT();
            fwd.FORWARD_AGENT_CD = FORWARD_AGENT_CD;
            fwd.FORWARD_AGENT_NAME = FORWARD_AGENT_NAME;
            fwd.PIC_NAME = PIC_NAME;
            fwd.CONTACT_NO = CONTACT_NO;
            fwd.DESCR1 = DESCR1;
            fwd.DESCR2 = DESCR2;
            fwd.DESCR3 = DESCR3;
            fwd.MESSRS_1 = MESSRS_1;
            fwd.MESSRS_2 = MESSRS_2;
            fwd.MESSRS_3 = MESSRS_3;
            fwd.TYPE = TYPE;
            fwd.CHANGED_BY = getpE_UserId.Username;
            string resultMessage = fwd.UpdateData(fwd);

            //string resultMessage = BuyerPD.UpdateData(pBUYER_PD_CD, pCUSTOMER_NO, pDESCRIPTION, pDELETION_FLAG, pCHANGED_BY);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion

        public ActionResult PopUpMaintenance(string p_FORWARD_AGENT_CD, string p_TYPE)
        {
            if (p_FORWARD_AGENT_CD == null)
                p_FORWARD_AGENT_CD = "";

            //if (p_FORWARD_AGENT_CD == "")
            //    ViewData["IsNewData"] = true;
            //else
            //    ViewData["IsNewData"] = false;

            FORWARDING_AGENT fa = new FORWARDING_AGENT();
            FORWARDING_AGENT model = fa.getFORWARDING_AGENT(p_FORWARD_AGENT_CD, p_TYPE);

            return PartialView("ForwardingAgentMaintenace", model);
        }

        public void DownloadData(object sender, EventArgs e, string FORWARD_AGENT_CD, string FORWARD_AGENT_NAME, string PIC_NAME)
        {
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/ForwardingAgentDownload.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            IRow Hrow;

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left; 

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center; 

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right; 

            ISheet sheet = workbook.GetSheet("Forwarding Agent");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "ForwardingAgent" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(FORWARD_AGENT_CD);
            sheet.GetRow(9).GetCell(2).SetCellValue(FORWARD_AGENT_NAME);
            sheet.GetRow(10).GetCell(2).SetCellValue(PIC_NAME);
            
            int row = 16;
            int rowNum = 1;

            FORWARDING_AGENT fwd = new FORWARDING_AGENT();
            fwd.FORWARD_AGENT_CD = FORWARD_AGENT_CD;
            fwd.FORWARD_AGENT_NAME = FORWARD_AGENT_NAME;
            fwd.PIC_NAME = PIC_NAME;
            List<FORWARDING_AGENT> model = new List<FORWARDING_AGENT>();
            model = fwd.getFORWARDING_AGENTList(fwd);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.FORWARD_AGENT_CD);
                Hrow.CreateCell(3).SetCellValue(result.FORWARD_AGENT_NAME);
                Hrow.CreateCell(4).SetCellValue(result.TYPE);
                Hrow.CreateCell(5).SetCellValue(result.PIC_NAME);
                Hrow.CreateCell(6).SetCellValue(result.CONTACT_NO);
                Hrow.CreateCell(7).SetCellValue(result.DESCR1);
                Hrow.CreateCell(8).SetCellValue(result.DESCR2);
                Hrow.CreateCell(9).SetCellValue(result.DESCR3);
                Hrow.CreateCell(10).SetCellValue(result.MESSRS_1);
                Hrow.CreateCell(11).SetCellValue(result.MESSRS_2);
                Hrow.CreateCell(12).SetCellValue(result.MESSRS_3);
                Hrow.CreateCell(13).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(14).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.CREATED_DT));
                Hrow.CreateCell(15).SetCellValue(result.CHANGED_BY);
                if (result.CHANGED_DT == null)
                    Hrow.CreateCell(16).SetCellValue("");
                else
                Hrow.CreateCell(16).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent;
                Hrow.GetCell(2).CellStyle = styleContent;
                Hrow.GetCell(3).CellStyle = styleContent;
                Hrow.GetCell(4).CellStyle = styleContent;
                Hrow.GetCell(5).CellStyle = styleContent;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent;
                Hrow.GetCell(8).CellStyle = styleContent;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent;
                Hrow.GetCell(11).CellStyle = styleContent;
                Hrow.GetCell(12).CellStyle = styleContent;
                Hrow.GetCell(13).CellStyle = styleContent;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent;
                Hrow.GetCell(16).CellStyle = styleContent2;

                row++;
                rowNum++;
            }
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
    }
}
