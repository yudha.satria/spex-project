﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using NPOI.XSSF.UserModel;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using System.Web.UI;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class CancellationAndReorderController : PageController
    {
        mCancellationAndReorder ms = new mCancellationAndReorder();
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        public string batchID = "SPX220200";
        public string batchIDReOrder = "SPX220300";
        public string moduleID = "SPX22";
        public const string UploadDirectory = "Content\\FileUploadResult";
        public const string TemplateFName = "CancellationFile";
        public const string TemplateFNameReorder = "Re-OrderFile";
        
        public CancellationAndReorderController()
        {
            Settings.Title = "Cancellation And Re-Order";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //CallbackOrderType();
            //ViewBag.MSPX00003ERR = msgError.getMSPX00003ERR("Order Date From and Order Date To");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            //ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Cancellation And Re-Order");
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            //ViewBag.MSPX00009INB = msgError.getMSPX00009INB("");
            //ViewBag.MSPX00010INB = msgError.getMSPX00010INB("");


            /*No data found*/
            ViewBag.MSPX00001ERR = msgError.getMsgText("MSPXS2006ERR");
            /*Cannot find template file {0} on folder {1}.*/
            ViewBag.MSPXS2034ERR = msgError.getMsgText("MSPXS2034ERR");
            /*Error writing file = {0}. Reason = {1}.*/
            ViewBag.MSPXS2035ERR = msgError.getMsgText("MSPXS2035ERR");
            /*Are you sure you want to download this data?*/
            ViewBag.MSPXS2021INF = msgError.getMsgText("MSPXS2021INF");

            ViewBag.MSPXS2005ERR = msgError.getMsgText("MSPXS2005ERR");

            getpE_UserId = (User)ViewData["User"];
               
            //ComboOBRStatus();


            ComboStatus();
            ComboSupplier();
            ComboSubSupplier();
            CallbackOrderType(); 
        } 

        public void ComboStatus()
        {
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            List<mSystemMaster> model = new List<mSystemMaster>();
            model = (new mSystemMaster()).GetComboBoxList("CANCELLATION_AND_REORDER_STATUS");
            model.Insert(0, item);
            ViewData["STATUS_FLAG"] = model;
        }
         

        public void ComboSupplier()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            model = ms.GetComboBoxSupplier();
            model.Insert(0, item);
            ViewData["SUPPLIER"] = model;  
        }

        public void ComboSubSupplier()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            model = ms.GetComboBoxSubSupplier();
            model.Insert(0, item);
            ViewData["SUB_SUPPLIER"] = model;  
        }

        public void CallbackOrderType()
        {
            List<SPEX.Models.mCancellationAndReorder.mOrderType> modelOrderType = ms.getListOrderType();
            ViewData["GridOrderType"] = modelOrderType;
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (string.IsNullOrEmpty(dt))
            {
                result = "";
            }
            else if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result; 
        }
 



        [HttpPost, ValidateInput(false)]
        public ActionResult CancellationAndReorderCallBack(string p_TMAP_ORDER_NO, string p_KANBAN_ID, string p_ORDER_TYPE, string p_PART_NO, 
                                                           string p_SUPPLIER, string p_STATUS, string p_MANIFEST_NO, string p_SUB_SUPPLIER, 
                                                           string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_CANCEL_DT, 
                                                           string p_CANCEL_DT_TO, string Mode)
        {
            List<mCancellationAndReorder> model = new List<mCancellationAndReorder>();
            if (Mode.Equals("Search"))
            {
                p_TMAP_ORDER_DT = reFormatDate(p_TMAP_ORDER_DT);
                p_TMAP_ORDER_DT_TO = reFormatDate(p_TMAP_ORDER_DT_TO);
                p_CANCEL_DT = reFormatDate(p_CANCEL_DT);
                p_CANCEL_DT_TO = reFormatDate(p_CANCEL_DT_TO); 
                model = ms.getListCancellationAndReorder( p_TMAP_ORDER_NO, p_KANBAN_ID, p_ORDER_TYPE, p_PART_NO, p_SUPPLIER, p_STATUS, p_MANIFEST_NO, 
                                                          p_SUB_SUPPLIER, p_TMAP_ORDER_DT, p_TMAP_ORDER_DT_TO, p_CANCEL_DT, p_CANCEL_DT_TO);

            }

            ComboSupplier();
            return PartialView("CancellationAndReorderGrid", model);
        }

        #region Download
        public ActionResult DownloadData(object sender, EventArgs e, string p_TMAP_ORDER_NO, string p_KANBAN_ID, string p_ORDER_TYPE, string p_PART_NO,
                                                           string p_SUPPLIER, string p_STATUS, string p_MANIFEST_NO, string p_SUB_SUPPLIER,
                                                           string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_CANCEL_DT,
                                                           string p_CANCEL_DT_TO)
        {
            string rExtract = string.Empty;
            string filename = "Cancellation_and_Re_Order_Download.xls";
            string directory = "Template";
            string filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);
            List<mCancellationAndReorder> model = new List<mCancellationAndReorder>();

            p_TMAP_ORDER_DT = reFormatDate(p_TMAP_ORDER_DT);
            p_TMAP_ORDER_DT_TO = reFormatDate(p_TMAP_ORDER_DT_TO);
            p_CANCEL_DT = reFormatDate(p_CANCEL_DT);
            p_CANCEL_DT_TO = reFormatDate(p_CANCEL_DT_TO);
            model = ms.getListCancellationAndReorder(p_TMAP_ORDER_NO, p_KANBAN_ID, p_ORDER_TYPE, p_PART_NO, p_SUPPLIER, p_STATUS, p_MANIFEST_NO,
                                                         p_SUB_SUPPLIER, p_TMAP_ORDER_DT, p_TMAP_ORDER_DT_TO, p_CANCEL_DT, p_CANCEL_DT_TO);

            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            //else if (model.Count() == 0)
            //{
            //    lError = 1;
            //    status = "Data";
            //    rExtract = model.Count().ToString();
            //}
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1)
                return Json(new { success = false, messages = rExtract, status = status }, JsonRequestBehavior.AllowGet);
            #endregion

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
            ICellStyle styleContent = workbook.CreateCellStyle();
            IFont font = workbook.CreateFont();
            font.FontHeightInPoints = 8;

            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;
            styleContent.SetFont(font);

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;
            styleContent2.SetFont(font);

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;
            styleContent3.SetFont(font);

             

            ISheet sheet = workbook.GetSheet("Cancellation and Re-Order");
            string date = DateTime.Now.ToString("ddMMyyyyHHmm");
            filename = "Cancellation_and_Re_Order_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(String.IsNullOrEmpty(p_TMAP_ORDER_NO) ? "ALL" : p_TMAP_ORDER_NO);
            sheet.GetRow(9).GetCell(2).SetCellValue(String.IsNullOrEmpty(p_PART_NO) ? "ALL" : p_PART_NO);
            sheet.GetRow(10).GetCell(2).SetCellValue(String.IsNullOrEmpty(p_MANIFEST_NO) ? "ALL" : p_MANIFEST_NO);

            sheet.GetRow(8).GetCell(5).SetCellValue(String.IsNullOrEmpty(p_KANBAN_ID) ? "ALL" : p_KANBAN_ID);
            sheet.GetRow(9).GetCell(5).SetCellValue(String.IsNullOrEmpty(p_SUPPLIER) ? "ALL" : p_SUPPLIER);
            sheet.GetRow(10).GetCell(5).SetCellValue(String.IsNullOrEmpty(p_SUB_SUPPLIER) ? "ALL" : p_SUB_SUPPLIER);

            sheet.GetRow(8).GetCell(8).SetCellValue(String.IsNullOrEmpty(p_ORDER_TYPE) ? "ALL" : p_ORDER_TYPE);
            sheet.GetRow(9).GetCell(8).SetCellValue(String.IsNullOrEmpty(p_STATUS) ? "ALL" : p_STATUS);
            sheet.GetRow(10).GetCell(8).SetCellValue((String.IsNullOrEmpty(p_TMAP_ORDER_DT) ? "-" : p_TMAP_ORDER_DT) + " To " + (String.IsNullOrEmpty(p_TMAP_ORDER_DT_TO) ? "-" : p_TMAP_ORDER_DT_TO));
            sheet.GetRow(11).GetCell(8).SetCellValue((String.IsNullOrEmpty(p_CANCEL_DT) ? "-" : p_CANCEL_DT) + " To " + (String.IsNullOrEmpty(p_CANCEL_DT_TO) ? "-" : p_CANCEL_DT_TO));

            int row = 14;
            int rowNum = 1;
            IRow Hrow;

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.TMAP_ORDER_NO);
                Hrow.CreateCell(3).SetCellValue(result.TMAP_PART_NO);
                Hrow.CreateCell(4).SetCellValue(result.TMAP_ITEM_NO);
                Hrow.CreateCell(5).SetCellValue(result.PART_NAME);
                Hrow.CreateCell(6).SetCellValue(result.MANIFEST_NO);
                Hrow.CreateCell(7).SetCellValue(result.TMAP_ITEM_NO);
                Hrow.CreateCell(8).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(9).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(10).SetCellValue(result.TMAP_ORDER_QTY);
                Hrow.CreateCell(11).SetCellValue(result.TMMIN_ORDER_QTY);
                Hrow.CreateCell(12).SetCellValue(result.RECEIVE_QTY);
                Hrow.CreateCell(13).SetCellValue(result.REMAIN_RECEIVE_QTY);
                Hrow.CreateCell(14).SetCellValue(result.SUM_ROC_QTY);
                Hrow.CreateCell(15).SetCellValue(result.CANCEL_DATE);
                Hrow.CreateCell(16).SetCellValue(result.CANCEL_BY);
                Hrow.CreateCell(17).SetCellValue(result.STATUS);

                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent3;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent3;
                Hrow.GetCell(13).CellStyle = styleContent3;
                Hrow.GetCell(14).CellStyle = styleContent3;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                row++;
                rowNum++;
            }
            MemoryStream memory = new MemoryStream();
            workbook.Write(memory);
            ftmp.Close();
            Response.BinaryWrite(memory.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);        

        }





        public ActionResult DownloadDataDetail(object sender, EventArgs e, string p_TMAP_ORDER_NO, string p_KANBAN_ID, string p_ORDER_TYPE, string p_PART_NO,
                                                           string p_SUPPLIER, string p_STATUS, string p_MANIFEST_NO, string p_SUB_SUPPLIER,
                                                           string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_CANCEL_DT, string p_CANCEL_DT_TO)
        {
            string rExtract = string.Empty;
            string filename = "Cancellation_and_Re_Order_Download_Detail.xls";
            string directory = "Template";
            string filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);
            List<mCancellationAndReorder> model = new List<mCancellationAndReorder>();

            p_TMAP_ORDER_DT = reFormatDate(p_TMAP_ORDER_DT);
            p_TMAP_ORDER_DT_TO = reFormatDate(p_TMAP_ORDER_DT_TO);
            p_CANCEL_DT = reFormatDate(p_CANCEL_DT);
            p_CANCEL_DT_TO = reFormatDate(p_CANCEL_DT_TO);
            model = ms.getListCancellationAndReorderDownloadDetail(p_TMAP_ORDER_NO, p_KANBAN_ID, p_ORDER_TYPE, p_PART_NO, p_SUPPLIER, p_STATUS, p_MANIFEST_NO,
                                                         p_SUB_SUPPLIER, p_TMAP_ORDER_DT, p_TMAP_ORDER_DT_TO, p_CANCEL_DT, p_CANCEL_DT_TO);

            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            //else if (model.Count() == 0)
            //{
            //    lError = 1;
            //    status = "Data";
            //    rExtract = model.Count().ToString();
            //}
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1)
                return Json(new { success = false, messages = rExtract, status = status }, JsonRequestBehavior.AllowGet);
            #endregion

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
            ICellStyle styleContent = workbook.CreateCellStyle();
            IFont font = workbook.CreateFont();
            font.FontHeightInPoints = 8;


            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;
            styleContent.SetFont(font);

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;
            styleContent2.SetFont(font);

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;
            styleContent3.SetFont(font);

            ISheet sheet = workbook.GetSheet("Cancellation and Reorder Detail");
            string date = DateTime.Now.ToString("ddMMyyyyHHmm");
            filename = "Cancellation_and_Re_Order_Detail_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(String.IsNullOrEmpty(p_TMAP_ORDER_NO) ? "ALL" : p_TMAP_ORDER_NO);
            sheet.GetRow(9).GetCell(2).SetCellValue(String.IsNullOrEmpty(p_PART_NO) ? "ALL" : p_PART_NO);
            sheet.GetRow(10).GetCell(2).SetCellValue(String.IsNullOrEmpty(p_MANIFEST_NO) ? "ALL" : p_MANIFEST_NO);

            sheet.GetRow(8).GetCell(5).SetCellValue(String.IsNullOrEmpty(p_KANBAN_ID) ? "ALL" : p_KANBAN_ID);
            sheet.GetRow(9).GetCell(5).SetCellValue(String.IsNullOrEmpty(p_SUPPLIER) ? "ALL" : p_SUPPLIER);
            sheet.GetRow(10).GetCell(5).SetCellValue(String.IsNullOrEmpty(p_SUB_SUPPLIER) ? "ALL" : p_SUB_SUPPLIER);

            sheet.GetRow(8).GetCell(8).SetCellValue(String.IsNullOrEmpty(p_ORDER_TYPE) ? "ALL" : p_ORDER_TYPE);
            sheet.GetRow(9).GetCell(8).SetCellValue(String.IsNullOrEmpty(p_STATUS) ? "ALL" : p_STATUS);
            sheet.GetRow(10).GetCell(8).SetCellValue((String.IsNullOrEmpty(p_TMAP_ORDER_DT) ? "-" : p_TMAP_ORDER_DT) + " To " + (String.IsNullOrEmpty(p_TMAP_ORDER_DT_TO) ? "-" : p_TMAP_ORDER_DT_TO));
            sheet.GetRow(11).GetCell(8).SetCellValue((String.IsNullOrEmpty(p_CANCEL_DT) ? "-" : p_CANCEL_DT) + " To " + (String.IsNullOrEmpty(p_CANCEL_DT_TO) ? "-" : p_CANCEL_DT_TO));
            
            int row = 14;
            int rowNum = 1;
            IRow Hrow;

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.TMAP_ORDER_NO);
                Hrow.CreateCell(3).SetCellValue(result.TMAP_PART_NO);
                Hrow.CreateCell(4).SetCellValue(result.TMAP_ITEM_NO);
                Hrow.CreateCell(5).SetCellValue(result.PART_NAME);
                Hrow.CreateCell(6).SetCellValue(result.MANIFEST_NO);
                Hrow.CreateCell(7).SetCellValue(result.TMAP_ITEM_NO);
                Hrow.CreateCell(8).SetCellValue(result.KANBAN_ID);
                Hrow.CreateCell(9).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(10).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(11).SetCellValue(result.TMAP_ORDER_QTY);
                Hrow.CreateCell(12).SetCellValue(result.TMMIN_ORDER_QTY);
                Hrow.CreateCell(13).SetCellValue(result.RECEIVE_QTY);
                Hrow.CreateCell(14).SetCellValue(result.REMAIN_RECEIVE_QTY);
                Hrow.CreateCell(15).SetCellValue(result.SUM_ROC_QTY);
                Hrow.CreateCell(16).SetCellValue(result.CANCEL_DATE);
                Hrow.CreateCell(17).SetCellValue(result.CANCEL_BY);
                Hrow.CreateCell(18).SetCellValue(result.STATUS);

                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent3;
                Hrow.GetCell(13).CellStyle = styleContent3;
                Hrow.GetCell(14).CellStyle = styleContent3;
                Hrow.GetCell(15).CellStyle = styleContent3;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2;
                Hrow.GetCell(18).CellStyle = styleContent2;
                row++;
                rowNum++;
            }
            MemoryStream memory = new MemoryStream();
            workbook.Write(memory);
            ftmp.Close();
            Response.BinaryWrite(memory.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);

        }
            

        

        #endregion

        #region Upload Data
        private class UploadControlHelper
        {
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                //AllowedFileExtensions = new string[] { ".xlsx" },
                AllowedFileExtensions = new string[] { ".csv" },
                MaxFileSize = 20971520
            };
        }

        #region Cancel Order
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadCancel", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            Guid processID = Guid.NewGuid();
            long pid = 0;
            String fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            String tb_t_name = "spex.TB_T_DAILY_ORDER_CANCEL";
            String processName = "Cancellation And Reorder CANCEL UPLOAD";
            String resultMessage = String.Empty;

            Lock lc = new Lock();
            int IsLock = lc.is_lock(batchID);
            if (IsLock > 0)
            {
                resultMessage = "E|" + msgError.getMsgText("MSPXS2036ERR");
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                if (ms.DeleteTempData().Equals("SUCCESS"))
                {
                    Log lg = new Log();
                    string msg = "MSPXS2024INF| INF |" + new MessagesString().getMsgText("MSPXS2024INF").Replace("{0}", processName);
                    pid = lg.createLog(msg, getpE_UserId.Username, processName, pid, moduleID, batchID);

                    lc.CREATED_BY = getpE_UserId.Username;
                    lc.CREATED_DT = DateTime.Now;
                    lc.PROCESS_ID = pid;
                    lc.FUNCTION_ID = batchID;
                    lc.LOCK_REF = batchID;
                    lc.LockFunction(lc);

                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string uploadDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);
                    var pathfile = Path.Combine(uploadDir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    e.UploadedFile.SaveAs(pathfile);

                    String bulkSts = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    if (bulkSts == string.Empty || bulkSts == "SUCCESS")
                    {
                        String processSts = ms.BackgroundProsessUploadSave(moduleID, batchID, pid, getpE_UserId.Username);
                        if (processSts == "SUCCESS")
                        {
                            resultMessage = "I|" + msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2] + "|" + pid.ToString();
                        }
                    }
                    else
                    {
                        resultMessage = "E|" + bulkSts.Split('|')[1];
                    }
                    lc.UnlockFunction(batchID);
                }
                else
                {
                    resultMessage = "E|" + ms.DeleteTempData();
                }
            }
            else
            {
                resultMessage = "E|File is not .csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
            {
                e.CallbackData = resultMessage;
            }
        }
        #endregion

        #region Re-order
        public ActionResult CallbackUploadReorder()
        {
            UploadControlExtension.GetUploadedFiles("UploadReorder", UploadControlHelper.ValidationSettings, uc_FileUploadCompleteReorder);
            return null;
        }

        public void uc_FileUploadCompleteReorder(object sender, FileUploadCompleteEventArgs e)
        {
            Guid processID = Guid.NewGuid();
            long pid = 0;
            String fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            String tb_t_name = "spex.TB_T_REORDER";
            String processName = "Cancellation And Reorder RE-ORDER UPLOAD";
            String resultMessage = String.Empty;

            Lock lc = new Lock();
            int IsLock = lc.is_lock(batchIDReOrder);
            if (IsLock > 0)
            {
                resultMessage = "E|" + msgError.getMsgText("MSPXS2036ERR");
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                if (ms.DeleteTempDataReorder().Equals("SUCCESS"))
                {
                    Log lg = new Log();
                    string msg = "MSPXS2024INF| INF |" + new MessagesString().getMsgText("MSPXS2024INF").Replace("{0}", processName);
                    pid = lg.createLog(msg, getpE_UserId.Username, processName, pid, moduleID, batchIDReOrder);

                    lc.CREATED_BY = getpE_UserId.Username;
                    lc.CREATED_DT = DateTime.Now;
                    lc.PROCESS_ID = pid;
                    lc.FUNCTION_ID = batchIDReOrder;
                    lc.LOCK_REF = batchIDReOrder;
                    lc.LockFunction(lc);

                    string resultFilePath = UploadDirectory + TemplateFNameReorder + Path.GetExtension(e.UploadedFile.FileName);
                    string uploadDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);
                    var pathfile = Path.Combine(uploadDir, TemplateFNameReorder + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    e.UploadedFile.SaveAs(pathfile);

                    String bulkSts = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    if (bulkSts == string.Empty || bulkSts == "SUCCESS")
                    {
                        String processSts = ms.BackgroundProsessUploadSaveReorder(moduleID, batchIDReOrder, pid, getpE_UserId.Username);
                        if (processSts == "SUCCESS")
                        {
                            resultMessage = "I|" + msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2] + "|" + pid.ToString();
                        }
                    }
                    else
                    {
                        resultMessage = "E|" + bulkSts.Split('|')[1];
                    }
                    lc.UnlockFunction(batchIDReOrder);
                }
                else
                {
                    resultMessage = "E|" + ms.DeleteTempDataReorder();
                }
            }
            else
            {
                resultMessage = "E|File is not .csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
            {
                e.CallbackData = resultMessage;
            }
        }
        #endregion

        #endregion


    }
}