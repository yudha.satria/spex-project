﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class PartReceivingController : PageController
    {
        MessagesString msgError = new MessagesString();
        mSystemMaster systemMaster = new mSystemMaster();
        Log log = new Log();

        public PartReceivingController()
        {
            Settings.Title = "Part Receiving";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            List<mSystemMaster> m = systemMaster.GetSystemValueList("TIMEOUT", "PartReceiving", "2");
            List<mSystemMaster> d = systemMaster.GetSystemValueList("TIMEOUT_COLOR", "PartReceiving", "2");
            ViewData["TIMEOUT_DATA"] = "60";
            ViewData["TIMEOUT_COLOR"] = "3000";
            if (m.Count > 1)
            {
                ViewData["TIMEOUT_DATA"] = (Int32.Parse( m[1].SYSTEM_VALUE) * 60).ToString();
            }
            if (d.Count > 1)
            {
                ViewData["TIMEOUT_COLOR"] = (Int32.Parse(d[1].SYSTEM_VALUE) * 1000).ToString();
            }
            getpE_UserId = (User)ViewData["User"];
        }

        public JsonResult keepAlive()
        {
            return Json(new { success = "true" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult doPartReceivingProcess(String KANBAN_ID)
        {
            mPartReceiving PR = new mPartReceiving();
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;
            String result = PR.doPartReceivingProcess(KANBAN_ID, UserID);
            r = result.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                msg = r[1].Trim();
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string data = r[2].Trim();
                msg = r[1].Trim();
                return Json(new { success = "true", messages = msg, data = data }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}

