﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class MarksNumberMasterController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mMarksNumberMaster marksNumber = new mMarksNumberMaster();

        public MarksNumberMasterController()
        {
            Settings.Title = "Marks And Number Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            Common cmn = new Common();
            ViewData["BuyerPDList"] = cmn.GetAllBuyerPDOnly();
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult MarksNumberGridCallback(string pBUYER_PD_CD, string pCOMPANY, string pCITY, string pCOUNTRY, string pMARKS_NUMBER, string pDELETION_FLAG)
        {
            List<mMarksNumberMaster> model = new List<mMarksNumberMaster>();

            model = marksNumber.GetList(pBUYER_PD_CD, pCOMPANY, pCITY, pCOUNTRY, pMARKS_NUMBER, pDELETION_FLAG);

            return PartialView("MarksNumberGrid", model);
        }

        public ActionResult DeleteData(string pBUYER_PD_CD)
        {
            string[] r = null;
            string msg = "";

            string resultMessage = marksNumber.DeleteData(pBUYER_PD_CD, getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditMarksNumber(string pBUYER_PD_CD, string pCOMPANY, string pCITY, string pCOUNTRY, string pMARKS_NUMBER)
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";

            string resultMessage = marksNumber.EditData(pBUYER_PD_CD, pCOMPANY, pCITY, pCOUNTRY, pMARKS_NUMBER, getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;

            if (dt.Contains("."))
            {
                ar = dt.Split('.');
                success = int.TryParse(ar[0].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[2].ToString(), out year);
            }
            else
            {
                ar = dt.Split('-');
                success = int.TryParse(ar[2].ToString(), out day);
                success = int.TryParse(ar[1].ToString(), out month);
                success = int.TryParse(ar[0].ToString(), out year);
            }
            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        public ActionResult UploadBackgroundProsess()
        {
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;

            string resultMessage = marksNumber.UploadMarksNumber(UserID);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = msgError.getMSPX00066INF("Upload Marks Number Master", "<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00065INF("Upload Marks Number Master", "<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }


        private string FunctionID = "F15-050";
        private string ModuleID = "F15";
        public void FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            string rExtract = string.Empty;
            string tb_t_name = "SPEX.TB_T_MARKS_NUMBER";
            #region createlog
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);

            string templateFileName = "TemplateMarksNumberMaster";
            string UploadDirectory = Server.MapPath("~/Content/FileUploadResult");
            long ProcessID = 0;
            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock lockTable = new Lock();

            int IsLock = lockTable.is_lock(FunctionID, out ProcessID);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(ProcessID.ToString());
            }
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = marksNumber.DeleteTempData();
                if (rExtract == "SUCCESS")
                {
                    string resultFilePath = UploadDirectory + templateFileName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, templateFileName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                    rExtract = ReadAndInsertExcelData(pathfile);
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .xls OR xlsx";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        #region Upload function when user click button upload
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, FileUploadComplete);
            return null;
        }
        #endregion

        #region cek format file to Upload  Marks Number
        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplateMarksNumberMaster";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xlsx" },
                MaxFileSize = 20971520
            };
        }
        #endregion

        public void DownloadData(string pBUYER_PD_CD, string pCOMPANY, string pCITY, string pCOUNTRY, string pMARKS_NUMBER, string pDELETION_FLAG)
        {
            try
            {
                string filename = "";
                string filesTmp = HttpContext.Request.MapPath("~/Template/MarksNumberDownload.xlsx");

                FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

                XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

                IDataFormat format = workbook.CreateDataFormat();

                ICellStyle styleDecimal = workbook.CreateCellStyle();
                styleDecimal.DataFormat = HSSFDataFormat.GetBuiltinFormat("##,##0.0####");
                styleDecimal.Alignment = HorizontalAlignment.Center;
                styleDecimal.BorderBottom = BorderStyle.Thin;
                styleDecimal.BorderTop = BorderStyle.Thin;
                styleDecimal.BorderLeft = BorderStyle.Thin;
                styleDecimal.BorderRight = BorderStyle.Thin;

                ICellStyle styleDate1 = workbook.CreateCellStyle();
                styleDate1.DataFormat = HSSFDataFormat.GetBuiltinFormat("dd/MM/yyyy HH:mm:ss");
                styleDate1.Alignment = HorizontalAlignment.Center;
                styleDate1.BorderBottom = BorderStyle.Thin;
                styleDate1.BorderTop = BorderStyle.Thin;
                styleDate1.BorderLeft = BorderStyle.Thin;
                styleDate1.BorderRight = BorderStyle.Thin;

                ICellStyle style1 = workbook.CreateCellStyle();
                style1.VerticalAlignment = VerticalAlignment.Center;
                style1.Alignment = HorizontalAlignment.Center;
                style1.BorderBottom = BorderStyle.Thin;
                style1.BorderTop = BorderStyle.Thin;
                style1.BorderLeft = BorderStyle.Thin;
                style1.BorderRight = BorderStyle.Thin;

                ICellStyle style2 = workbook.CreateCellStyle();
                style2.VerticalAlignment = VerticalAlignment.Center;
                style2.Alignment = HorizontalAlignment.Left;
                style2.BorderBottom = BorderStyle.Thin;
                style2.BorderRight = BorderStyle.Thin;
                style2.BorderLeft = BorderStyle.Thin;
                style2.BorderRight = BorderStyle.None;


                ISheet sheetMain = workbook.GetSheet("PriceMaster");
                string date = DateTime.Now.ToString("ddMMyyyyHHmmss");
                filename = "MarksNumber_" + date + ".xlsx";

                string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

                List<mMarksNumberMaster> models = marksNumber.GetList(pBUYER_PD_CD, pCOMPANY, pCITY, pCOUNTRY, pMARKS_NUMBER, pDELETION_FLAG);
                int startRow = 12;
                IRow Hrow;

                Hrow = sheetMain.GetRow(5);
                Hrow.CreateCell(2).SetCellValue(DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                Hrow.GetCell(2).CellStyle = style2;
                Hrow = sheetMain.GetRow(6);
                Hrow.CreateCell(2).SetCellValue(getpE_UserId.Username);
                Hrow.GetCell(2).CellStyle = style2;

                foreach (mMarksNumberMaster mn in models)
                {
                    Hrow = sheetMain.CreateRow(startRow);
                    //Hrow.CreateCell(0).SetCellValue("");
                    Hrow.CreateCell(1).SetCellValue((startRow - 11).ToString());
                    Hrow.GetCell(1).CellStyle = style1;
                    Hrow.CreateCell(2).SetCellValue(mn.BUYER_PD_CD);
                    Hrow.GetCell(2).CellStyle = style1;
                    Hrow.CreateCell(3).SetCellValue(mn.BYAIR_COMPANY);
                    Hrow.GetCell(3).CellStyle = style1;
                    Hrow.CreateCell(4).SetCellValue(mn.BYAIR_CITY);
                    Hrow.GetCell(4).CellStyle = style1;
                    Hrow.CreateCell(5).SetCellValue(mn.BYAIR_COUNTRY);
                    Hrow.GetCell(5).CellStyle = style1;
                    Hrow.CreateCell(6).SetCellValue(mn.BYAIR_MARKS_NUMBER);
                    Hrow.GetCell(6).CellStyle = style1;
                    Hrow.CreateCell(7).SetCellValue(mn.CREATED_BY);
                    Hrow.GetCell(7).CellStyle = style1;
                    if (mn.CREATED_DT != null)
                    {
                        Hrow.CreateCell(8).SetCellValue(((DateTime)mn.CREATED_DT).ToString("dd.MM.yyyy HH:mm:ss"));
                        Hrow.GetCell(8).CellStyle = style1;
                    }
                    else
                    {
                        Hrow.CreateCell(8).SetCellValue(" ");
                        Hrow.GetCell(8).CellStyle = style1;
                    }
                    Hrow.CreateCell(9).SetCellValue(mn.CHANGED_BY);
                    Hrow.GetCell(9).CellStyle = style1;
                    if (mn.CHANGED_DT != null)
                    {
                        Hrow.CreateCell(10).SetCellValue(((DateTime)mn.CHANGED_DT).ToString("dd.MM.yyyy HH:mm:ss"));
                        Hrow.GetCell(10).CellStyle = style1;
                    }
                    else
                    {
                        Hrow.CreateCell(10).SetCellValue(" ");
                        Hrow.GetCell(10).CellStyle = style1;
                    }

                    startRow++;
                }

                ExcelInsertHeaderImage(workbook, sheetMain, HttpContext.Request.MapPath("~/Content/images/excel_header.png"), 0);

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                ftmp.Close();
                Response.BinaryWrite(ms.ToArray());
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            }
            catch (Exception ex)
            {

            }
        }

        private void ExcelInsertHeaderImage(XSSFWorkbook workbook, ISheet sheet1, string path, int rowDest)
        {
            XSSFDrawing patriarch = (XSSFDrawing)sheet1.CreateDrawingPatriarch();
            XSSFClientAnchor anchor;
            anchor = new XSSFClientAnchor(0, 0, 0, 0, 1, rowDest, 2, rowDest);
            anchor.AnchorType = (AnchorType)2;
            //load the picture and get the picture index in the workbook
            XSSFPicture picture = (XSSFPicture)patriarch.CreatePicture(anchor, ExcelLoadImage(path, workbook));
            //Reset the image to the original size.
            picture.Resize();
            //picture.LineStyle = HSSFPicture.LINESTYLE_DASHDOTGEL;
        }

        public int ExcelLoadImage(string path, XSSFWorkbook workbook)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, (int)file.Length);
            return workbook.AddPicture(buffer, PictureType.JPEG);

        }

        private string ReadAndInsertExcelData(string FileName)
        {
            try
            {
                XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                ISheet sheet = xssfworkbook.GetSheetAt(0);
                int lastrow = sheet.LastRowNum;
                List<mMarksNumberMaster> marksList = new List<mMarksNumberMaster>();
                int index = 1;

                mMarksNumberMaster marks;
                for (int i = index; i <= lastrow; i++)
                {
                    marks = new mMarksNumberMaster();
                    IRow row = sheet.GetRow(i);

                    if (row == null) continue;
                    if (row.GetCell(0) == null && row.GetCell(1) == null && row.GetCell(2) == null && row.GetCell(3) == null && row.GetCell(4) == null) continue;

                    //part no
                    if (row.GetCell(0) == null) marks.BUYER_PD_CD = "";
                    else if (row.GetCell(0).CellType == CellType.Blank || (row.GetCell(0).CellType == CellType.String && (row.GetCell(0).StringCellValue == null || row.GetCell(0).StringCellValue == "")))
                        marks.BUYER_PD_CD = "";
                    else if (row.GetCell(0).CellType == CellType.Numeric && (row.GetCell(0).NumericCellValue.ToString() == ""))
                        marks.BUYER_PD_CD = "";
                    else if (row.GetCell(0).CellType == CellType.String)
                        marks.BUYER_PD_CD = row.GetCell(0).StringCellValue;
                    else if (row.GetCell(0).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(0);
                        string formatString = row.GetCell(0).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            marks.BUYER_PD_CD = row.GetCell(0).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            marks.BUYER_PD_CD = row.GetCell(0).NumericCellValue.ToString();
                    }
                    else
                        marks.BUYER_PD_CD = row.GetCell(0).ToString();

                    //rvc
                    if (row.GetCell(1) == null) marks.BYAIR_COMPANY = "";
                    else if (row.GetCell(1).CellType == CellType.Blank || (row.GetCell(1).CellType == CellType.String && (row.GetCell(1).StringCellValue == null || row.GetCell(1).StringCellValue == "")))
                        marks.BYAIR_COMPANY = "";
                    else if (row.GetCell(1).CellType == CellType.Numeric && (row.GetCell(1).NumericCellValue.ToString() == ""))
                        marks.BYAIR_COMPANY = "";
                    else if (row.GetCell(1).CellType == CellType.String)
                        marks.BYAIR_COMPANY = row.GetCell(1).StringCellValue;
                    else if (row.GetCell(1).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(1);
                        string formatString = row.GetCell(1).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            marks.BYAIR_COMPANY = row.GetCell(1).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            marks.BYAIR_COMPANY = row.GetCell(1).NumericCellValue.ToString();
                    }
                    else
                        marks.BYAIR_COMPANY = row.GetCell(1).ToString();

                    if (row.GetCell(2) == null) marks.BYAIR_CITY = "";
                    else if (row.GetCell(2).CellType == CellType.Blank || (row.GetCell(2).CellType == CellType.String && (row.GetCell(2).StringCellValue == null || row.GetCell(2).StringCellValue == "")))
                        marks.BYAIR_CITY = "";
                    else if (row.GetCell(2).CellType == CellType.Numeric && (row.GetCell(2).NumericCellValue.ToString() == ""))
                        marks.BYAIR_CITY = "";
                    else if (row.GetCell(2).CellType == CellType.String)
                        marks.BYAIR_CITY = row.GetCell(2).StringCellValue;
                    else if (row.GetCell(2).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(2);
                        string formatString = row.GetCell(2).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            marks.BYAIR_CITY = row.GetCell(2).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            marks.BYAIR_CITY = row.GetCell(2).NumericCellValue.ToString();
                    }
                    else
                        marks.BYAIR_CITY = row.GetCell(2).ToString();

                    if (row.GetCell(3) == null) marks.BYAIR_COUNTRY = "";
                    else if (row.GetCell(3).CellType == CellType.Blank || (row.GetCell(3).CellType == CellType.String && (row.GetCell(3).StringCellValue == null || row.GetCell(3).StringCellValue == "")))
                        marks.BYAIR_COUNTRY = "";
                    else if (row.GetCell(3).CellType == CellType.Numeric && (row.GetCell(3).NumericCellValue.ToString() == ""))
                        marks.BYAIR_COUNTRY = "";
                    else if (row.GetCell(3).CellType == CellType.String)
                        marks.BYAIR_COUNTRY = row.GetCell(3).StringCellValue;
                    else if (row.GetCell(3).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(3);
                        string formatString = row.GetCell(3).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            marks.BYAIR_COUNTRY = row.GetCell(3).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            marks.BYAIR_COUNTRY = row.GetCell(3).NumericCellValue.ToString();
                    }
                    else
                        marks.BYAIR_COUNTRY = row.GetCell(3).ToString();

                    if (row.GetCell(4) == null) marks.BYAIR_MARKS_NUMBER = "";
                    else if (row.GetCell(4).CellType == CellType.Blank || (row.GetCell(4).CellType == CellType.String && (row.GetCell(4).StringCellValue == null || row.GetCell(4).StringCellValue == "")))
                        marks.BYAIR_MARKS_NUMBER = "";
                    else if (row.GetCell(4).CellType == CellType.Numeric && (row.GetCell(4).NumericCellValue.ToString() == ""))
                        marks.BYAIR_MARKS_NUMBER = "";
                    else if (row.GetCell(4).CellType == CellType.String)
                        marks.BYAIR_MARKS_NUMBER = row.GetCell(4).StringCellValue;
                    else if (row.GetCell(4).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(4);
                        string formatString = row.GetCell(4).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            marks.BYAIR_MARKS_NUMBER = row.GetCell(4).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            marks.BYAIR_MARKS_NUMBER = row.GetCell(4).NumericCellValue.ToString();
                    }
                    else
                        marks.BYAIR_MARKS_NUMBER = row.GetCell(4).ToString();

                    marksList.Add(marks);
                }

                foreach (mMarksNumberMaster data in marksList)
                {
                    string insertResult = (new mMarksNumberMaster()).InsertTempMarksNumber(data);
                }

                return "0|success";
            }
            catch (Exception ex)
            {
                return "1|" + ex.Message;
            }
        }
    }
}

