﻿using SPEX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class DeviceController : Controller
    {
        //
        // GET: /Default1/
        DeviceUser device = new DeviceUser();
        public ActionResult Index(String Username = "", String Password = "", String IP = "")
        {
            return Json(new { success = "false", messages = "Hello World" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Login(String Username="",String Password = "", String IP="")
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            DeviceUser a = device.doLogin(Username,Password,IP);
            return Json(a, JsonRequestBehavior.AllowGet);
        }

    }
}
