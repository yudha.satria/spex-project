﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class InvoiceByAirController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mInvoice Invoice = new mInvoice();

        public InvoiceByAirController()
        {
            Settings.Title = "Invoice Maintenance Screen";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public ReadyCargoByAirInquiry ReadyCargoByAir
        {
            get
            {
                if (Session["ReadyCargoByAirInquiry" + getpE_UserId.Username] != null)
                    return (ReadyCargoByAirInquiry)Session["ReadyCargoByAirInquiry" + getpE_UserId.Username];
                else return new ReadyCargoByAirInquiry();
            }

        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            if (getpE_UserId != null)
            {
                string p_INVOICE_NO = GetParameter("p_INVOICE_NO");
                if (p_INVOICE_NO == null)
                    ViewData["PREV_LINK"] = "ReadyCargoByAirInquiry";
                else
                    ViewData["PREV_LINK"] = "Invoice";

                GetInvoiceByAir(p_INVOICE_NO);
            }
            else
            {
                mInvoice model = new mInvoice() { ETD = DateTime.Now, INVOICE_DT = DateTime.Now, VANNING_DT = DateTime.Now };
                ViewData["INVOICE_DATA"] = model;
                ViewData["MIN_ETD"] = DateTime.Now;
                ViewData["MIN_VANNING_DT"] = DateTime.Now;
            }
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            ar = dt.Split('.');
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;
            success = int.TryParse(ar[0].ToString(), out day);
            success = int.TryParse(ar[1].ToString(), out month);
            success = int.TryParse(ar[2].ToString(), out year);

            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

        public void GetInvoiceByAir(string p_INVOICE_NO)
        {
            string UserID = getpE_UserId.Username;

            mInvoice model = new mInvoice();
            if (p_INVOICE_NO == null)
            {
                Invoice.DeleteTempByAir(UserID);
                //model = Invoice.getNewInvoiceByAir(ReadyCargoByAir.BUYER_CD, ReadyCargoByAir.PD_CD, ReadyCargoByAir.PACKING_COMPANY, ReadyCargoByAir.CASE_DANGER_FLAG, UserID);
                model = Invoice.getNewInvoiceByAir(ReadyCargoByAir.BUYER_CD, ReadyCargoByAir.PD_CD, ReadyCargoByAir.PACKING_COMPANY, ReadyCargoByAir.CASE_DANGER_FLAG, UserID, ReadyCargoByAir.VANNING_DT_FROM, ReadyCargoByAir.VANNING_DT_TO);
                ViewData["CASE_DATA"] = Invoice.getCaseListByAir(model.INVOICE_NO, ReadyCargoByAir.PACKING_COMPANY, UserID);
            }
            else
            {
                model = Invoice.getInvoiceByAir(p_INVOICE_NO);
            }

            ViewData["MIN_ETD"] = DateTime.Now.AddDays(Invoice.GetDateValidation("ETD_VALIDATION") - 1);
            ViewData["MIN_VANNING_DT"] = DateTime.Now.AddDays(Invoice.GetDateValidation("VANNING_DT_VALIDATION") - 1);
            ViewData["VesselNameList"] = Invoice.GetVesselNameByAir();

            ViewData["INVOICE_DATA"] = model;

        }

        public ActionResult GetPopUpCaseEdit(string p_INVOICE_NO, string p_CASE_NO, string p_BUYER_PD, string p_PACKING_COMPANY, string p_CASE_DANGER_FLAG)
        {
            mInvoice model = Invoice.GetCaseByAir(p_INVOICE_NO, p_CASE_NO, p_PACKING_COMPANY, getpE_UserId.Username);

            return PartialView("~/Views/InvoiceByAir/CaseEdit.cshtml", model);
        }

        public ActionResult GetCaseGrid(string p_INVOICE_NO, string p_BUYER_PD, string p_PACKING_COMPANY, string p_CASE_DANGER_FLAG)
        {
            List<mInvoice> model = Invoice.getCaseListByAir(p_INVOICE_NO, p_PACKING_COMPANY, getpE_UserId.Username);
            return PartialView("CaseGrid", model);
        }

        public ActionResult SubmitInvoice(string p_INVOICE_NO, string p_PACKING_COMPANY)
        {
            if (getpE_UserId != null)
            {
                string UserID = getpE_UserId.Username;
                string resultMessage = "";

                if (ReadyCargoByAir.BUYER_CD != null)
                    resultMessage = Invoice.SaveInvoiceByAir(p_INVOICE_NO, p_PACKING_COMPANY, UserID);
                
                string[] r = null;
                r = resultMessage.Split('|');

                if (r[0].ToString().ToLower().Contains("success"))
                    return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveInvoice(string p_INVOICE_NO, string p_BUYER_PD, string p_PACKING_COMPANY, string p_CASE_DANGER_FLAG,
               string p_VESSEL_NAME, string p_ETD, string p_VANNING_DT, string p_DHL_AWB_NO, string p_FORWARDING_AGENT)
        {
            if (getpE_UserId != null)
            {
                string UserID = getpE_UserId.Username;
                string resultMessage = "";

                if (ReadyCargoByAir.BUYER_CD != null)
                    resultMessage = Invoice.SaveTempInvoiceByAir(p_INVOICE_NO, p_PACKING_COMPANY, p_VESSEL_NAME, GetDate(p_ETD), GetDate(p_VANNING_DT), p_DHL_AWB_NO, p_FORWARDING_AGENT, UserID);
                
                string[] r = null;
                r = resultMessage.Split('|');

                if (r[0].ToString().ToLower().Contains("success"))
                    return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteCase(string p_INVOICE_NO, string p_CASE_NO, string p_BUYER_PD, string p_PACKING_COMPANY, string p_CASE_DANGER_FLAG)
        {
            if (getpE_UserId != null)
            {
                string UserID = getpE_UserId.Username;
                string resultMessage = "";

                if (ReadyCargoByAir.BUYER_CD != null)
                    resultMessage = Invoice.DeleteCaseByAir(p_INVOICE_NO, p_CASE_NO, p_PACKING_COMPANY, UserID);

                string[] r = null;
                r = resultMessage.Split('|');
                var header = Invoice.getTempHeaderInvoiceByAir(p_INVOICE_NO, p_PACKING_COMPANY, UserID);

                if (r[0].ToString().ToLower().Contains("success"))
                    return Json(new
                    {
                        success = "true",
                        messages = r[1],
                        gross_weight = header.GROSS_WEIGHT,
                        net_weight = header.NET_WEIGHT,
                        measurement = header.MEASUREMENT,
                        fob_amount = header.FOB_AMOUNT,
                        total_case = header.TOTAL_CASE,
                        total_qty = header.TOTAL_QTY,
                        total_item = header.TOTAL_ITEM,
                        forwarding_agent = header.FORWARDING_AGENT
                    }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);
        }

        public bool GetDHLValidationProperty(string p_VESSEL_NAME)
        {
            if (p_VESSEL_NAME.ToUpper().Contains("DHL"))
                return Invoice.GetDHLValidation("DHL_AWB_VALIDATION_DHL");
            else
                return Invoice.GetDHLValidation("DHL_AWB_VALIDATION_AIR");
        }

        public ActionResult UpdateCase(string p_INVOICE_NO, string p_BUYER_PD, string p_PACKING_COMPANY, string p_CASE_DANGER_FLAG, string p_CASE_NO,
              string p_CASE_GROSS_WEIGHT, string p_CASE_NET_WEIGHT, string p_CASE_LENGTH, string p_CASE_WIDTH, string p_CASE_HEIGHT)
        {
            if (getpE_UserId != null)
            {
                string UserID = getpE_UserId.Username;
                string resultMessage = "";

                if (ReadyCargoByAir.BUYER_CD != null)
                    resultMessage = Invoice.UpdateCaseByAir(p_INVOICE_NO, p_PACKING_COMPANY, p_CASE_NO, GetDecimal(p_CASE_GROSS_WEIGHT),
                        GetDecimal(p_CASE_NET_WEIGHT), GetDecimal(p_CASE_LENGTH), GetDecimal(p_CASE_WIDTH), GetDecimal(p_CASE_HEIGHT), UserID);

                string[] r = null;
                r = resultMessage.Split('|');
                var header = Invoice.getTempHeaderInvoiceByAir(p_INVOICE_NO, p_PACKING_COMPANY, UserID);

                if (r[0].ToString().ToLower().Contains("success"))
                    return Json(new { success = "true", messages = r[1], gross_weight = header.GROSS_WEIGHT, net_weight = header.NET_WEIGHT, measurement = header.MEASUREMENT, fob_amount = header.FOB_AMOUNT, forwarding_agent = header.FORWARDING_AGENT }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);
        }

        private decimal GetDecimal(string strValue)
        {
            decimal returnValue = 0;
            returnValue = Convert.ToDecimal(strValue);
            return returnValue;
        }

        public List<mInvoice> GetVesselNameByAirList()
        {
            return Invoice.GetVesselNameByAir();
        }
    }
}
