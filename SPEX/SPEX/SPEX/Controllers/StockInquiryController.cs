﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;
using NPOI.HSSF.UserModel;
using System.IO;
using System.Text;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class StockInquiryController : PageController
    {

        //Model Maintenance Stock Master
        mStockInquiry ms = new mStockInquiry();
        MessagesString messageError = new MessagesString();

        //Upload
        public string moduleID = "SPX16";
        public string functionID = "SPX160200";
        public const string UploadDirectory = "Content\\ReferencePartStock";
        public const string TemplateFName = "Reference";

        public StockInquiryController()
        {
            Settings.Title = "Stock Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }


        public void GetCombobox()
        {

        }

        protected override void Startup()
        {

            //Call error message
            ViewBag.MSPX00001ERR = messageError.getMSPX00001ERR("Stock Inquiry");
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = messageError.getMSPX00009ERR();

            /*No data found*/
            ViewBag.MSPXS2006ERR = messageError.getMsgText("MSPXS2006ERR");
            /*{0} should not be empty.*/
            ViewBag.MSPXS2005ERR = messageError.getMsgText("MSPXS2005ERR");
            /*A single record must be selected.*/
            ViewBag.MSPXS2017ERR = messageError.getMsgText("MSPXS2017ERR");
            /*Duplication found for {0}.*/
            ViewBag.MSPXS2011ERR = messageError.getMsgText("MSPXS2011ERR");
            /*{0} is not exists in  {1}*/
            ViewBag.MSPXS2010ERR = messageError.getMsgText("MSPXS2010ERR");
            /*Cannot find template file {0} on folder {1}.*/
            ViewBag.MSPXS2034ERR = messageError.getMsgText("MSPXS2034ERR");
            /*Error writing file = {0}. Reason = {1}.*/
            ViewBag.MSPXS2035ERR = messageError.getMsgText("MSPXS2035ERR");
            /*Invalid data type of {0}. Should be {1}.*/
            ViewBag.MSPXS2012ERR = messageError.getMsgText("MSPXS2012ERR");
            /*Invalid data type of {0}. Should be {1}.*/
            ViewBag.MSPXS2040ERR = messageError.getMsgText("MSPXS2040ERR");
            /*Are you sure you want to confirm the operation?*/
            ViewBag.MSPXS2018INF = messageError.getMsgText("MSPXS2018INF");
            /*Are you sure you want to abort the operation?*/
            ViewBag.MSPXS2019INF = messageError.getMsgText("MSPXS2019INF");
            /*Are you sure you want to delete the record?*/
            ViewBag.MSPXS2020INF = messageError.getMsgText("MSPXS2020INF");
            /*Are you sure you want to download this data?*/
            ViewBag.MSPXS2021INF = messageError.getMsgText("MSPXS2021INF");
            
            getpE_UserId = (User)ViewData["User"];
            GetCombobox();

        }

        public ActionResult StockInquiryCallBack(string pPART_NO, string pRACK_ADDRESS, string Mode)
        {
            List<mStockInquiry> model = new List<mStockInquiry>();
            if (Mode.Equals("Search"))
                model = ms.getListMaintenanceStock(pPART_NO, pRACK_ADDRESS);

            GetCombobox();
            return PartialView("StockInquiryGrid", model);
        }


        #region Function Add
        public ActionResult EditStockInquiry(string pPART_NO, string pPART_NAME, string pSUPPLIER_CD, string pSUB_SUPPLIER_CD, int pSOH, int pACTUAL_STOCK, string pRACK_ADDRESS_CD, string pREFERENCE, String iChangedBy, String iChangedDt)
        {
            if (getpE_UserId != null)
            {
                string p_CREATED_BY = getpE_UserId.Username;
                string[] r = null;
                string resultMessage = ms.UpdateData(pPART_NO,pPART_NAME,pSUPPLIER_CD,pSUB_SUPPLIER_CD,pSOH,pACTUAL_STOCK,pRACK_ADDRESS_CD,pREFERENCE,p_CREATED_BY,iChangedBy,iChangedDt);
                r = resultMessage.Split('|');
                if (r[0] != "I")
                {
                    return Json(new { success = false, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { success = false, messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        #endregion


        //function download 
        public ActionResult DownloadStockInquiry(object sender, EventArgs e, string pPART_NO, string pRACK_ADDRESS)
        {

            string rExtract = string.Empty;
            string filename = "Stock_Inquiry_Download.xls";
            string directory = "Template";
            string filesTmp = HttpContext.Request.MapPath("~/"+directory+"/"+filename);
            List<mStockInquiry> model = new List<mStockInquiry>();
            model = ms.getListMaintenanceStock(pPART_NO, pRACK_ADDRESS);

            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            else if (model.Count() == 0)
            {
                lError = 1;
                status = "Data";
                rExtract = model.Count().ToString();
            }
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1)
                return Json(new { success = false, messages = rExtract, status = status }, JsonRequestBehavior.AllowGet);
            #endregion
            
            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("StockInquiry");
            string date = DateTime.Now.ToString("yyyyMMddHHmm");
            filename = "STOCK_" + date + ".xls";
            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(String.IsNullOrEmpty(pPART_NO) ? "ALL" : pPART_NO);
            sheet.GetRow(9).GetCell(2).SetCellValue(String.IsNullOrEmpty(pRACK_ADDRESS) ? "ALL" : pRACK_ADDRESS);

            int row = 12;
            int rowNum = 1;
            IRow Hrow;

            
            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PART_NO);
                Hrow.CreateCell(3).SetCellValue(result.PART_NAME);
                Hrow.CreateCell(4).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(5).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(6).SetCellValue(result.SOH.ToString());
                Hrow.CreateCell(7).SetCellValue("");
                Hrow.CreateCell(8).SetCellValue(result.RACK_ADDRESS_CD);

                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent;
                Hrow.GetCell(4).CellStyle = styleContent;
                Hrow.GetCell(5).CellStyle = styleContent;
                Hrow.GetCell(6).CellStyle = styleContent3;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent;


                row++;
                rowNum++;
            }

            MemoryStream memory = new MemoryStream();
            workbook.Write(memory);
            ftmp.Close();
            Response.BinaryWrite(memory.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));

            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);
        }


        public void DownloadTemplate(object sender, EventArgs e, string pPART_NO, string pRACK_ADDRESS)
        {

            string filename = "";
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "Stock_Inquiry_" + date + ".csv";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");


            int rowNum = 1;

            List<mStockInquiry> model = new List<mStockInquiry>();
            model = ms.getListMaintenanceStock(pPART_NO, pRACK_ADDRESS);



               StringBuilder sb = new StringBuilder();  
               sb.Append("DATAID");  
               sb.Append(",");  
               sb.Append("PART_NO");  
               sb.Append(",");  
               sb.Append("RACK_ADDRESS");
               sb.Append(",");  
               sb.Append("SOH");
               sb.Append(",");  
               sb.Append("ACTUAL_STOCK");
               sb.AppendLine();  

                foreach (var result in model)
                {
                     sb.Append("D");  
                     sb.Append(",");  
                     sb.Append(result.PART_NO);  
                     sb.Append(",");
                     sb.Append("=\"" + result.RACK_ADDRESS_CD.ToString() + "\"");  
                     sb.Append(",");  
                     sb.Append(result.SOH.ToString());
                     sb.Append(",");  
                     sb.Append("");
                     sb.AppendLine();     
                     rowNum++;
                }



            //Download the CSV file.
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(sb);
            Response.Flush();
            Response.End();
        }


        #region Function Upload
        public ActionResult IsCheckLock()
        {
            Lock L = new Lock();
            int Lock = L.is_lock(functionID);

            if (Lock == 0)
            {
                return Json(new { success = "true", messages = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string msg = string.Empty;
                msg = messageError.getMsgText("MSPX00088ERR");
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }


        public ActionResult CallbackUploadRef()
        {
            UploadControlExtension.GetUploadedFiles("UploadRef", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }
        
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/ReferencePartStock";
            public const string TemplateFName = "Reference";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                //AllowedFileExtensions = new string[] { ".*" },
                MaxFileSize = 20971520
            };
        }

        public class UploadDocumentControlHelper
        {
            public const string UploadDirectory = "Content/ReferencePartStock";
            public const string TemplateFName = "Reference";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".csv" },
                MaxFileSize = 20971520
            };
        }

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
                    string rExtract = string.Empty;
                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);
                    string FileNameUpload = TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName);
                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, FileNameUpload);

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    FileInfo info = new FileInfo(pathfile);
                    if (info.Exists)
                    {
                        rExtract = "I|" + FileNameUpload;
                    }
                    else
                    {
                        rExtract = "E| Error Upload";
                    }
            
            
            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract;//+ "|" + pid.ToString();
        }
        #endregion


        public ActionResult CallbackUploadDocument()
        {
            UploadControlExtension.GetUploadedFiles("UploadAdjustStock", UploadDocumentControlHelper.ValidationSettings, DocumentFileUploadComplete);
            return null;
        }

        public void DocumentFileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            var refFileName = HttpContext.Request.Params["refFileName"];

            mStockInquiry PM = new mStockInquiry();

            string[] r = null;
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_ADJUST_STOCK";

            #region createlog
            Guid PROCESS_ID = Guid.NewGuid();
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            string MESSAGE_DETAIL = "";
            long pid = 0;
            //string subFuncName = "EXcelToSQL";
            string isError = "N";
            string processName = "UPLOAD ADJUST STOCK";
            string resultMessage = "";

            int columns;
            int rows;

            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock L = new Lock();
            string functionIDUpload = "SPX160200";
            int IsLock = L.is_lock(functionIDUpload);
            if (IsLock > 0)
            {
                resultMessage = "E|" + messageError.getMsgText("MSPXS2036ERR");
            }

            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = PM.DeleteDataTBT();
                if (rExtract == "SUCCES")
                {
                    string user_id = getpE_UserId.Username;
                    Log lg = new Log();
                    string msg = "MSPX00035INF|INF|" + new MessagesString().getMsgText("MSPX00035INF").Replace("{0}", processName);
                    pid = lg.createLog(msg, user_id, "Upload Packing Part Master", pid, moduleID, functionIDUpload);
                    L.CREATED_BY = user_id;
                    L.CREATED_DT = DateTime.Now;
                    L.PROCESS_ID = pid;
                    L.FUNCTION_ID = functionIDUpload;
                    L.LOCK_REF = functionIDUpload;
                    L.LockFunction(L);
                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    //Function untuk nge-extract file excel
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                    //rExtract = ReadAndInsertExcelData(pathfile);
                    //''string delimiter=Upload.GetDelimiter(e.UploadedFile.s)
                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    if (rExtract == string.Empty || rExtract == "SUCCESS")
                    {
                        rExtract = PM.BackgroundProcessSave(functionIDUpload, moduleID, user_id, pid, refFileName);
                        if (rExtract == "SUCCES")
                            resultMessage = "I|" + messageError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2] + "|" + pid.ToString();
                    }
                    else
                    {
                        resultMessage = "E|" + resultMessage.Split('|')[1];
                    }
                    L.UnlockFunction(functionIDUpload);

                }
                else
                {
                    rExtract = "E|" + rExtract;
                }
            }
            else
            {
                rExtract = "E|File is not .csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = resultMessage;//+ "|" + pid.ToString();
        }

    
    }
}
