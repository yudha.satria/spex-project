﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class ForwardingAgentAirAssignmentController : PageController
    {
        //
        // GET: /ForwardingAgentAirAssignment/

        MessagesString msgError = new MessagesString();
        Common CMN = new Common();
        public ForwardingAgentAirAssignmentController()
        {
            Settings.Title = "Forwarding Agent Assignment [TRANS By Air]";
        }
        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Ready Cargo Inquiry");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            ViewData["BUYER"] = GetListBuyer();
            ViewData["PACKING_COMPANY"] = GetPackingCompany();
            ViewData["DESTINATION"] = GetDestination();
            ViewData["COURIER_COMPANY"] = GetCOURIER_COMPANY(string.Empty);
            ViewData["FORWARDER"] = GetFORWARDER();
            ViewData["DELETION_STATUS"] = getDELETION_STATUS();
            ViewData["IMPORTER"] = GetIMPORTER();
            ViewData["COUNTRY"] = GetCOUNTRY();
            ViewData["AC_NO_TO_USE"] = GetAC_NO_TO_USE();
            ViewData["BEP"] = GetBEP();
            getpE_UserId = (User)ViewData["User"];
        }
        public List<BUYER> GetListBuyer()
        {
            List<BUYER> Buyers = new List<BUYER>();
            Buyers = CMN.GetAllBuyerByBuyerPD();
            return Buyers;
        }

        public ActionResult GetListPD(string BUYER_CD)
        {
            List<mPDCode> pd = new List<mPDCode>();
            pd = CMN.GetAllPDByBuyerPD(BUYER_CD);
            ViewData["PD"] = pd;
            return PartialView("_Partial_PD");
        }

        public List<string> GetPackingCompany()
        {
            return CMN.GetPackingCompany();
        }

        public List<string> GetDestination()
        {
            return new FORWARDING_AGENT_AIR_ASSIGNMENT().GetDestination();
        }

        public List<string> GetCOURIER_COMPANY(string BEP)
        {
            return new FORWARDING_AGENT_AIR_ASSIGNMENT().GetCOURIER_COMPANY(BEP);
        }

        public List<string> GetFORWARDER()
        {
            return new FORWARDING_AGENT_AIR_ASSIGNMENT().GetFORWARDER();
        }

        public List<string> getDELETION_STATUS()
        {
            return new FORWARDING_AGENT_AIR_ASSIGNMENT().getDELETION_STATUS();
        }

        public List<string> GetIMPORTER()
        {
            return new FORWARDING_AGENT_AIR_ASSIGNMENT().GetIMPORTER();
        }

        public List<string> GetCOUNTRY()
        {
            return new FORWARDING_AGENT_AIR_ASSIGNMENT().GetCOUNTRY();
        }
        public List<string> GetAC_NO_TO_USE()
        {
            return new FORWARDING_AGENT_AIR_ASSIGNMENT().GetAC_NO_TO_USE();
        }

        public List<string> GetBEP()
        {
            return new FORWARDING_AGENT_AIR_ASSIGNMENT().GetBEP();
        }
        [HttpPost]
        public ActionResult GetListForwardingAgentByAir(string Data)
        {
            List<FORWARDING_AGENT_AIR_ASSIGNMENT> FAs = null;
            FORWARDING_AGENT_AIR_ASSIGNMENT Model = new FORWARDING_AGENT_AIR_ASSIGNMENT();
            Model = System.Web.Helpers.Json.Decode<FORWARDING_AGENT_AIR_ASSIGNMENT>(Data);
            FAs = Model.GetListForwardingAgentByAir(Model);
            //var a = ViewData["Model"];
            return PartialView("_Partial_Grid", FAs);
        }

        public ActionResult GetListBEPByFA(string FORWARDER)
        {
            FORWARDING_AGENT_AIR_ASSIGNMENT Model = new FORWARDING_AGENT_AIR_ASSIGNMENT();
            List<string> BEPs = new List<string>();
            BEPs = Model.GetBEPForm(FORWARDER);
            ViewData["BEP"] = BEPs;
            ViewData["BEPSelected"] = BEPs.First();
            ViewBag.BEPSelected = BEPs.First();
            return PartialView("_Partial_BEP");
        }

        public ActionResult _PopUpFA(string Mode, string Keys)
        {
            FORWARDING_AGENT_AIR_ASSIGNMENT Model = new FORWARDING_AGENT_AIR_ASSIGNMENT();
            ViewData["BUYER"] = GetListBuyer();
            ViewData["FORWARDER"] = Model.GetFORWARDERFORM();
            ViewData["PACKING_COMPANY"] = GetPackingCompany();
            ViewBag.Mode = Mode;
            List<string> COURIER_COMPANYs = new List<string>();
            COURIER_COMPANYs = GetCOURIER_COMPANY(Model.BEP);
            ViewData["COURIER_COMPANY"] = COURIER_COMPANYs;
            if (Mode == "Edit")
            {
                ViewData["IS_ADD"] = false;
                Model = System.Web.Helpers.Json.Decode<FORWARDING_AGENT_AIR_ASSIGNMENT>(Keys);
                Model = Model.GetFirstForwardingAgent(Model);
                if (Model.COURIER_COMPANY == null)
                    Model.COURIER_COMPANY = "";
            }
            else if (Mode == "Copy")
            {
                Model = System.Web.Helpers.Json.Decode<FORWARDING_AGENT_AIR_ASSIGNMENT>(Keys);
                Model = Model.GetFirstForwardingAgent(Model);
                if (Model.COURIER_COMPANY == null)
                    Model.COURIER_COMPANY = "";
                ViewData["IS_ADD"] = true; 
                //REMAKS AGI 2016-12-12 REQUEST MR YONI BUG FA BUYER CD AND PD CD MUNCUL SAAT ON LOAD
                //Model.BUYER_CD = "";
                //Model.PD_CD = "";                
            }
            else
            {
                ViewData["IS_ADD"] = true;
                if (Model.COURIER_COMPANY == null)
                    Model.COURIER_COMPANY = "";
            }
            return PartialView("LoadFormFA", Model);
        }

        public ActionResult GetListPD2(string BUYER_CD)
        {
            List<mPDCode> pd = new List<mPDCode>();
            FORWARDING_AGENT_AIR_ASSIGNMENT MODEL = new FORWARDING_AGENT_AIR_ASSIGNMENT();
            pd = CMN.GetAllPDByBuyerPD(BUYER_CD);
            ViewData["PD"] = pd;
            ViewData["IS_ADD"] = false;
            return PartialView("_Partial_PD2");
        }

        public ActionResult GetListRole(string BEP)
        {
            FORWARDING_AGENT_AIR_ASSIGNMENT Model = new FORWARDING_AGENT_AIR_ASSIGNMENT();
            List<string> ROLEs = new List<string>();
            ROLEs = Model.GetRolesByBEP(BEP);
            ViewData["ROLE"] = ROLEs;
            ViewData["ROLESelected"] = ROLEs.First();
            ViewBag.ROLESelected = ROLEs.First();
            return PartialView("_Partial_Role");
        }

        public ActionResult GetCourierCompanyByBEP(string BEP)
        {
            List<string> COURIER_COMPANYs = new List<string>();
            COURIER_COMPANYs = GetCOURIER_COMPANY(BEP);
            ViewData["COURIER_COMPANY"] = COURIER_COMPANYs;
            return PartialView("_Partial_COURIER_COMPANY");
        }

        public ActionResult AddData(FORWARDING_AGENT_AIR_ASSIGNMENT FA)
        {
            FA.CREATED_BY = getpE_UserId.Username;
            FA.CREATED_DT = DateTime.Now;
            //if (FA.FLIGHT_DEPARTURE_STRING != "" || FA.FLIGHT_DEPARTURE_STRING != "0100.01.01")
            //    FA.FLIGHT_DEPARTURE = GetDate(FA.FLIGHT_DEPARTURE_STRING);
            //if (FA.CONNECTING_DEPARTURE_STRING != "" || FA.CONNECTING_DEPARTURE_STRING != "0100.01.01")
            //    FA.CONNECTING_DEPARTURE = GetDate(FA.CONNECTING_DEPARTURE_STRING);

            string Result = FA.AddData(FA);
            string[] Results = Result.Split('|');
            return Json(new { success = Results[0], messages = Results[1] }, JsonRequestBehavior.AllowGet);
            return null;
        }

        public ActionResult DeleteFA(List<FORWARDING_AGENT_AIR_ASSIGNMENT> FAs)
        {
            FORWARDING_AGENT_AIR_ASSIGNMENT Model = new FORWARDING_AGENT_AIR_ASSIGNMENT();
            FAs.Select(c => { c.CHANGED_BY = getpE_UserId.Username; c.CHANGED_DT = DateTime.Now; return c; }).ToList();
            string Message = Model.DeleteData(FAs);
            string[] Results = Message.Split('|');
            return Json(new { success = Results[0], messages = Results[1] }, JsonRequestBehavior.AllowGet);
            return null;
        }

        public ActionResult UpdateData(FORWARDING_AGENT_AIR_ASSIGNMENT FA)
        {
            //FORWARDING_AGENT_AIR_ASSIGNMENT Model = new FORWARDING_AGENT_AIR_ASSIGNMENT();
            FA.CHANGED_BY = getpE_UserId.Username;
            FA.CHANGED_DT = DateTime.Now;
            //if (FA.FLIGHT_DEPARTURE_STRING != "" || FA.FLIGHT_DEPARTURE_STRING != "0100.01.01")
            //    FA.FLIGHT_DEPARTURE = GetDate(FA.FLIGHT_DEPARTURE_STRING);
            //if (FA.CONNECTING_DEPARTURE_STRING != "" || FA.CONNECTING_DEPARTURE_STRING != "0100.01.01")
            //    FA.CONNECTING_DEPARTURE = GetDate(FA.CONNECTING_DEPARTURE_STRING);
            string Message = FA.UpdateData(FA);
            string[] Results = Message.Split('|');
            return Json(new { success = Results[0], messages = Results[1] }, JsonRequestBehavior.AllowGet);
            return null;
        }

        public void DOWMLOADFA(string FA)
        {
            string filename = string.Empty;
            //try
            //{

            string filesTmp = HttpContext.Request.MapPath("~/Template/FORWARDING_AGENT_AIR_ASSIGNMENT_DOWNLOAD.xlsx");

            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            IDataFormat format = workbook.CreateDataFormat();

            ISheet sheetMain = workbook.GetSheet("FA");
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            filename = "FA_Inquiry_" + date + "_" + getpE_UserId.Username + ".xlsx";

            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            List<FORWARDING_AGENT_AIR_ASSIGNMENT> download = new List<FORWARDING_AGENT_AIR_ASSIGNMENT>();
            FORWARDING_AGENT_AIR_ASSIGNMENT Model = new FORWARDING_AGENT_AIR_ASSIGNMENT();
            Model = System.Web.Helpers.Json.Decode<FORWARDING_AGENT_AIR_ASSIGNMENT>(FA);
            string pVERSION = "F";

            download = Model.GetListForwardingAgentByAir(Model);
            GenerateExcelFA(download, sheetMain, workbook, getpE_UserId.Name);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            var att = String.Format("attachment;filename={0}", filename);
            Response.AddHeader("content-disposition", att);
        }

        public void GenerateExcelFA(List<FORWARDING_AGENT_AIR_ASSIGNMENT> download, ISheet sheetMain, XSSFWorkbook workbook, string UserName)
        {
            int row = 10;
            IRow Hrow;
            ICellStyle styleBorder = workbook.CreateCellStyle();
            styleBorder.BorderLeft = BorderStyle.Thin;
            styleBorder.BorderBottom = BorderStyle.Thin;
            styleBorder.BorderRight = BorderStyle.Thin;
            styleBorder.BorderTop = BorderStyle.Thin;


            sheetMain.GetRow(4).GetCell(2).SetCellValue(DateTime.Now.ToString("yyyy-MM-dd"));
            sheetMain.GetRow(5).GetCell(2).SetCellValue(UserName);
            foreach (FORWARDING_AGENT_AIR_ASSIGNMENT FA in download)
            {
                Hrow = sheetMain.CreateRow(row);
                Hrow.CreateCell(0).SetCellValue(FA.BUYER_CD);
                Hrow.GetCell(0).CellStyle = styleBorder;

                Hrow.CreateCell(1).SetCellValue(FA.PD_CD);
                Hrow.GetCell(1).CellStyle = styleBorder;

                Hrow.CreateCell(2).SetCellValue(FA.IMPORTER);
                Hrow.GetCell(2).CellStyle = styleBorder;

                Hrow.CreateCell(3).SetCellValue(FA.DESTINATION);
                Hrow.GetCell(3).CellStyle = styleBorder;

                Hrow.CreateCell(4).SetCellValue(FA.COUNTRY);
                Hrow.GetCell(4).CellStyle = styleBorder;

                Hrow.CreateCell(5).SetCellValue(FA.AC_NO_TO_USE);
                Hrow.GetCell(5).CellStyle = styleBorder;

                Hrow.CreateCell(6).SetCellValue(FA.FORWARDER);
                Hrow.GetCell(6).CellStyle = styleBorder;

                Hrow.CreateCell(7).SetCellValue(FA.BEP);
                Hrow.GetCell(7).CellStyle = styleBorder;

                Hrow.CreateCell(8).SetCellValue(FA.ROLE);
                Hrow.GetCell(8).CellStyle = styleBorder;

                if (FA.RANGE_FROM != null)
                {
                    Hrow.CreateCell(9).SetCellValue((double)FA.RANGE_FROM.Value);
                    Hrow.GetCell(9).CellStyle = styleBorder;


                }
                else
                {
                    Hrow.CreateCell(9).SetCellType(CellType.Blank);
                    Hrow.GetCell(9).CellStyle = styleBorder;
                }
                if (FA.RANGE_TO != null)
                {
                    Hrow.CreateCell(10).SetCellValue((double)FA.RANGE_TO.Value);
                    Hrow.GetCell(10).CellStyle = styleBorder;
                }
                else
                {
                    Hrow.CreateCell(10).SetCellType(CellType.Blank);
                    Hrow.GetCell(10).CellStyle = styleBorder;
                }
                Hrow.CreateCell(11).SetCellValue(FA.COURIER_COMPANY);
                Hrow.GetCell(11).CellStyle = styleBorder;

                Hrow.CreateCell(12).SetCellValue(FA.REMARKS);
                Hrow.GetCell(12).CellStyle = styleBorder;

                Hrow.CreateCell(13).SetCellValue(FA.CREATED_BY);
                Hrow.GetCell(13).CellStyle = styleBorder;


                Hrow.CreateCell(14).SetCellValue(FA.CREATED_DT.Value);
                Hrow.GetCell(14).CellStyle = styleBorder;

                Hrow.CreateCell(15).SetCellValue(FA.DELETION_STATUS);
                Hrow.GetCell(15).CellStyle = styleBorder;

                row++;
            }
        }

        private DateTime GetDate(string dt)
        {
            string[] ar = null;
            ar = dt.Split('.');
            int year = 0;
            int month = 0;
            int day = 0;
            bool success;
            success = int.TryParse(ar[0].ToString(), out day);
            success = int.TryParse(ar[1].ToString(), out month);
            success = int.TryParse(ar[2].ToString(), out year);

            DateTime returnDate = new DateTime(year, month, day);

            return returnDate;
        }

    }
}
