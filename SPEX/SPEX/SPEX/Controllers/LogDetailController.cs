﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.IO;
using System.Drawing;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPopupControl;
using Toyota.Common.Credential;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using Toyota.Common.Web.Platform.Starter.Models;


namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class LogDetailController : PageController
    {
        mLogMonitoring LogMonitoring = new mLogMonitoring();

        public string getpE_ProcessId
        {
            get
            {
                if (Session["getpE_ProcessId"] != null)
                {
                    return Session["getpE_ProcessId"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_ProcessId"] = value;
            }
        }

        public void _SetId(string p_Process_Id)
        {
            getpE_ProcessId = p_Process_Id;

        }

        public ActionResult _LogGrid(string p_Process_Id)
        {
            List<mLogMonitoring> model = LogMonitoring.getListLogMontoringDetail(getpE_ProcessId);
            ViewData["LogData"] = model;

            List<mLogMonitoring> modelH = LogMonitoring.getListLogMontoringHeader(getpE_ProcessId);
            ViewData["D_ProcessId"] = modelH[0].PROCESS_ID;
            ViewData["D_ModuleName"] = modelH[0].MODULE_NAME;
            ViewData["D_FunctionName"] = modelH[0].FUNCTION_NAME;
            ViewData["D_ProcessStatus"] = modelH[0].PROCESS_STATUS;
            //ViewData["D_UserId"] = modelH[0].USER_ID;
            DateTime dt = Convert.ToDateTime(modelH[0].START_DATE);
            ViewData["D_StartDate"] = dt.ToString("dd-MM-yyyy hh:mm:ss");

            return PartialView("_PartialForm");
        }

        public ActionResult Refresh()
        {
            List<mLogMonitoring> model = LogMonitoring.getListLogMontoringDetail(getpE_ProcessId);
            ViewData["LogData"] = model;
            return PartialView("_PartialGridView", ViewData["LogData"]);
        }

        protected override void Startup()
        {
        }
    }
}

