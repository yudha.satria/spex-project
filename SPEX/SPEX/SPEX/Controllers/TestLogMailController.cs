﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using SPEX.Cls;

namespace SPEX.Controllers
{
    public class TestLogMailController : PageController
    {
        Log log = new Log();
        MessagesString mS = new MessagesString();
        public readonly string moduleID = "1";
        public readonly string functionID = "10001";
        public readonly string location = "TestLogMailController";
        public readonly string userBatch = "Agent";

        public TestLogMailController()
        {
           Settings.Title = "Test Log & Mail";
        }

        protected override void Startup()
        {

        }

        public ActionResult _AjaxLogTest() {
            string subFuncName = "_AjaxLogTest";
            string isError = "N";
            string batchName = "Log Test";
            long pid=0;

            #region proses create log
            //Pada saat awal process
            pid = log.createLog(mS.getMSPX00001INB(batchName), userBatch, location + "." + subFuncName, 0, moduleID, functionID);            

            //Pada saat dalam process ada error
            pid = log.createLog(mS.getMSPX00001ERB("P000000001") , userBatch, location + "." + subFuncName, pid, moduleID, functionID);

            //Pada saat dalam process ada error
            pid = log.createLog(mS.getMSPX00002ERB("P000000002"), userBatch, location + "." + subFuncName, pid, moduleID, functionID);

            //Pada saat dalam process ada error
            pid = log.createLog(mS.getMSPX00003ERB("P000000003"), userBatch, location + "." + subFuncName, pid, moduleID, functionID);            

            //Misalkan ada error maka var isError harus di set "Y"
            isError = "Y";

            //Pada saat akhir proses, di cek apakah isError="Y"
            if (isError == "N")
            {
                //log finish tanpa error
                pid = log.createLog(mS.getMSPX00002INB(batchName), userBatch, location + "." + subFuncName, pid, moduleID, functionID);            
            }
            else {
                //log finish dengan error
                pid = log.createLog(mS.getMSPX00003INB(batchName), userBatch, location + "." + subFuncName, pid, moduleID, functionID);
                //kirim notification kalo ada error, panggil class mail
                new MailController().sendNotificationMail("Order Receiving",pid,"PUD").Deliver();
                new MailController().sendNotificationMail("Order Receiving",pid, "PAC").Deliver();
            }
            #endregion

            return Json(new { success = "true", messages = "log created" }, JsonRequestBehavior.AllowGet);
        }
    }
}
