﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SPEX.Cls;
using Toyota.Common.Credential;
using System.IO;
using Toyota.Common.Database;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO.Compression;
using NPOI.SS.Util;
using SPEX.Models;

namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class PEBTAMController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mPEBTAM peb = new mPEBTAM();

        public PEBTAMController()
        {
            Settings.Title = "PEB TAM PACKING";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            //DD.MM.YYYY
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                //MM/DD/YYYY
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PEBCallBack(string p_PEB_SEQ, string p_INVOICE_DT, string p_INVOICE_DT_TO)
        {
            List<mPEBTAM> model = new List<mPEBTAM>();

            p_INVOICE_DT = p_INVOICE_DT == "01.01.0100" ? "" : reFormatDate(p_INVOICE_DT);
            p_INVOICE_DT_TO = p_INVOICE_DT_TO == "01.01.0100" ? "" : reFormatDate(p_INVOICE_DT_TO);

            model = peb.getListPEB(p_PEB_SEQ, p_INVOICE_DT, p_INVOICE_DT_TO);

            return PartialView("PEBGrid", model);
        }

        //Pop Up
        public ActionResult GetPEBDetailGrid(string p_SegNo)
        {
            List<mPEBTAM> model = peb.getListPEBDetail(p_SegNo);
            return PartialView("PEBDetailGrid", model);
        }

        public ActionResult SendParameter(string pPEB_SEQ)
        {

            string batchName = "SendToPEBInterface";
            string moduleID = "4";
            string functionID = "40005";
            string subFuncName = "Send Param";
            string userBatch = getpE_UserId.Username;
            string location = "PEB.SendParameter";

            //starting logmessageError
            long pid = log.createLog(msgError.getMSPX00001INB(batchName), userBatch, location + "." + subFuncName, 0, moduleID, functionID);

            string pParameter = "{&quot;PROCESS_ID&quot;:&quot;" + pid + "&quot;}";

            string pID = pid.ToString();
            string pName = "Sending PEB";
            string pDescription = "Sending Data to PEB Interface";
            string pSubmitter = getpE_UserId.Username;
            string pFunctionName = "Sending PEB Batch";
            string pType = "0";
            string pStatus = "0";
            //for trial
            string pCommand = @"D:\SPIN\Background_Task\Tasks\SPEXToPEBInterface\SPEXToPEBInterface.exe";

            string pStartDate = "0";
            string pEndDate = "0";
            string pPeriodicType = "4";
            string pInterval = null;
            string pExecutionDays = "";
            string pExecutionMonths = "";
            string pTime = "6840000";

            string[] r = null;
            string msg = "";

            string resultMessage = peb.SendData(pID, pPEB_SEQ, pName, pDescription, pSubmitter, pFunctionName, pParameter, pType, pStatus, pCommand, pStartDate, pEndDate, pPeriodicType, pInterval, pExecutionDays, pExecutionMonths, pTime);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                msg = msgError.getMSPX00013INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg + "|" + pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                msg = msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "true", messages = msg + '|' + pID }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PopUpDetail(string p_SeqNo)
        {
            ViewData["SetSeqId"] = p_SeqNo;
            List<mPEBTAM> header3 = peb.getListPEBHeader3(p_SeqNo);
            ViewData["PEBHeader3Data"] = header3;
            List<mPEBTAM> detail = peb.getListPEBDetail(p_SeqNo);
            ViewData["PEBDetailData"] = detail;

            return PartialView("~/Views/PEBTAM/PEBDetail.cshtml");
        }

        public ActionResult GetPEBHeader3Grid(string p_SeqNo)
        {
            List<mPEBTAM> header3 = peb.getListPEBHeader3(p_SeqNo);

            return PartialView("PEBHeader3Grid", header3);
        }

        public void GetContainerSizeListMaster()
        {
            List<mPEBTAM> containerSizeList = peb.getListOfSystemMasterData("SPEX_PEB_INTERFACE_PARAM", "CONTAINER_SIZE_");
            ViewData["DataContainerSize"] = containerSizeList;
        }

        public ActionResult PopUpSegment(string p_SeqNo, string p_InvoiceNo, string p_RegisterNo)
        {
            if (p_RegisterNo == "")
                ViewData["IsNewData"] = true;
            else
                ViewData["IsNewData"] = false;

            mPEBTAM model = peb.getPEBSegment(p_SeqNo);
            List<mPEBTAM> importerCDList = peb.getListOfSystemMasterData("SPEX_PEB_INTERFACE_PARAM", "IMPORTER_CD_");
            ViewData["DataImporterCD"] = importerCDList;
            List<mPEBTAM> destPortCDList = peb.getListOfSystemMasterData("SPEX_PEB_INTERFACE_PARAM", "DEST_PORT_CD_");
            ViewData["DataDestPortCD"] = destPortCDList;
            List<mPEBTAM> containerSizeList = peb.getListOfSystemMasterData("SPEX_PEB_INTERFACE_PARAM", "CONTAINER_SIZE_");
            ViewData["DataContainerSize"] = containerSizeList;

            List<mPEBTAM> listDat = peb.getListPEBContainer(p_RegisterNo);
            if (listDat.Count == 0)
            {
                listDat = new List<mPEBTAM>();
                listDat.Add(new mPEBTAM() { SEQ_NO = "1" });
            }

            ViewData["ContainerList"] = listDat;
            ViewData["ContainerIndex"] = listDat.Count;

            return PartialView("~/Views/PEBTAM/PEBSegment.cshtml", model);
        }

        public ActionResult GetSegmentD(string p_ContainerIndex)
        {
            List<mPEBTAM> containerSizeList = peb.getListOfSystemMasterData("SPEX_PEB_INTERFACE_PARAM", "CONTAINER_SIZE_");
            ViewData["DataContainerSize"] = containerSizeList;
            ViewData["ContainerIndex"] = p_ContainerIndex;
            return PartialView("~/Views/PEBTAM/PEBSegmentD.cshtml");
        }

        public ActionResult SaveSegment(string pPEB_SEQ, string pINVOICE_NO, string pREGISTER_NO, string pREGISTER_DT, string pVOYAGE_NO, string pDEST_PORT_CD, string pIMPORTER_CD, string pINSPECTION_DT, string pINV_CONTAINER_COUNT, string pCONTAINER, string pPACKING_COMPANY)
        {

            string REGISTER_DT = reFormatDate(pREGISTER_DT);
            string INSPECTION_DT = reFormatDate(pINSPECTION_DT);

            string[] r = null;
            string resultMessage = peb.SaveSegmentData(pPEB_SEQ, pINVOICE_NO, pREGISTER_NO, REGISTER_DT,
                      pVOYAGE_NO, pDEST_PORT_CD, pIMPORTER_CD, INSPECTION_DT,
                      pINV_CONTAINER_COUNT, pCONTAINER, pPACKING_COMPANY, getpE_UserId.Username);

            string msg = "";
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = msgError.getMSPX00034INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg + "|" + pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00001INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg + "|" + pID }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult UpdateSegment(string pPEB_SEQ, string pINVOICE_NO, string pREGISTER_NO, string pREGISTER_DT, string pVOYAGE_NO, string pDEST_PORT_CD, string pIMPORTER_CD, string pINSPECTION_DT, string pINV_CONTAINER_COUNT, string pCONTAINER, string pPACKING_COMPANY)
        {

            string REGISTER_DT = reFormatDate(pREGISTER_DT);
            string INSPECTION_DT = reFormatDate(pINSPECTION_DT);

            string[] r = null;
            string resultMessage = peb.UpdateSegmentData(pPEB_SEQ, pINVOICE_NO, pREGISTER_NO, REGISTER_DT,
                      pVOYAGE_NO, pDEST_PORT_CD, pIMPORTER_CD, INSPECTION_DT,
                      pINV_CONTAINER_COUNT, pCONTAINER, pPACKING_COMPANY, getpE_UserId.Username);

            string msg = "";
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = msgError.getMSPX00034INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg + "|" + pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00001INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg + "|" + pID }, JsonRequestBehavior.AllowGet);
            }

        }

        public string reFormatReportDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[0] + ar[1] + ar[2];
            }
            return result;
        }

        public string CheckInvoice(string pInvoice)
        {
            string result = "EXIST";

            mInvoice inv = new mInvoice().getInvoice(pInvoice);

            return inv == null ? "NOT EXIST" : result;
        }

        public ActionResult CheckProcess(string pPEB_SEQ)
        {
            string resultMessage = peb.CheckProcess(pPEB_SEQ);
            string[] r = null;
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteData(string pPEB_SEQ)
        {
            string[] r = null;
            string msg = "";

            string resultMessage = peb.DeleteData(pPEB_SEQ);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
