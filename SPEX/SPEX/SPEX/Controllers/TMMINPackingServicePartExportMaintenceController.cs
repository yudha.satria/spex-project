﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class TMMINPackingServicePartExportMaintenceController : PageController
    {
        //
        // GET: /TMMINPackingServicePartExportMaintence/

        MessagesString msgError = new MessagesString();
        public string moduleID = "F15";
        public string functionID = "F15-025";
        public const string UploadDirectory = "Content\\FileUploadResult";
        public const string TemplateFName = "UPLOAD_PACKING_PART_MASTER";
        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public TMMINPackingServicePartExportMaintenceController()
        {
            Settings.Title = "Service Part Export Maintenance (TMMIN Packing)";
            //Settings.Title = "test";
        }


        protected override void Startup()
        {
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Packing Part Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
            ViewData["PART_TYPE"] = Common.GetPartTypeList();
            ViewData["DG_FLAG"] = Common.GetDangerFlagList();
            List<mSystemMaster> REORDER_FLAGs=new mSystemMaster().GetSystemValueList("REORDER_FLAG", "1;0", "3");
            ViewData["REORDER_FLAGs"] = REORDER_FLAGs.Where(a => a.SYSTEM_VALUE!=string.Empty).OrderByDescending(a=>a.SYSTEM_CD);
        }

        //public ActionResult GetPackingPartMasterCallBack(string PART_NO, string SUPPLIER_CD, string DOCK_CD, string KANBAN_NO, string SUPPLIER_PlANT, string PART_TYPE, string DG_FLAG, string Mode)
        //modif agi 2017-08-11
        public ActionResult GetPackingPartMasterCallBack(string Data, string Mode)
        {
            List<PackingPartMaster> model = new List<PackingPartMaster>();
            var decode = System.Web.Helpers.Json.Decode(Data);
            //PackingPartMaster PM = //System.Web.Helpers.Json.Decode(Data);// JsonConvert.SerializeObject(model);
            PackingPartMaster PM = new PackingPartMaster();
            PM.PART_NO = decode.PART_NO;
            PM.SUPPLIER_CD =decode.SUPPLIER_CD;
            PM.SUB_SUPPLIER_CD = decode.SUB_SUPPLIER_CD;
            PM.PMSP_FLAG = decode.PMSP_FLAG;
            PM.DOCK_CD = decode.DOCK_CD;
            PM.KANBAN_NO = decode.KANBAN_NO;
            PM.SUPPLIER_PLANT = decode.SUPPLIER_PlANT;
            PM.PART_TYPE =decode.PART_TYPE;
            PM.DG_FLAG =decode.DG_FLAG;
            //add agi 2017-08-11
            PM.REORDER_FLAG = decode.REORDER_FLAG;
            PM.COMPANY_CD = decode.COMPANY_CD;
            PM.RCV_PLANT_CD = decode.RCV_PLANT_CD;
            PM.CASE_GROUPING = decode.CASE_GROUPING;
            model = PM.GetData(PM, Mode);
            return PartialView("_PartialGrid", model);
        }

        public ActionResult Edit(string PART_NO, string SUPPLIER_CD, string SUPPLIER_PLANT, string DOCK_CD, string Mode)
        {
            PackingPartMaster model = new PackingPartMaster();

            PackingPartMaster PM = new PackingPartMaster();
            PM.PART_NO = PART_NO;
            PM.SUPPLIER_CD = SUPPLIER_CD;
            PM.DOCK_CD = DOCK_CD;
            PM.KANBAN_NO = string.Empty;
            PM.SUPPLIER_PLANT = SUPPLIER_PLANT;
            var data = PM.GetData(PM, Mode);
            model = data.Count > 0 ? data.FirstOrDefault() : new PackingPartMaster();

            //var json = JsonConvert.SerializeObject(model);
            //Json(new { success = "true", KANBAN_NO = model.KANBAN_NO, PART_NAME=model.PART_NAME,model. }, JsonRequestBehavior.AllowGet);
            return PartialView("MaintenceForm", model);
            //return null;
        }

        public ActionResult Save(PackingPartMaster Model)
        {
            //getpE_UserId = (User)ViewData["User"];
            if (getpE_UserId != null)
            {
                Model.CREATED_DT = DateTime.Now;
                Model.CREATED_BY = getpE_UserId.Username;
                string result = Model.AddData(Model);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
            return null;
        }

        public ActionResult Update(PackingPartMaster Model)
        {
            //getpE_UserId = (User)ViewData["User"];
            if (getpE_UserId != null)
            {
                Model.CHANGED_DT = DateTime.Now;
                Model.CHANGED_BY = getpE_UserId.Username;
                string result = Model.UpdateData(Model);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "error :User not found because session expired" });
            }
            return null;
        }

        public ActionResult IsCheckLock()
        {
            Lock L = new Lock();
            int Lock = L.is_lock(functionID);

            if (Lock == 0)
            {
                return Json(new { success = "true", messages = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string msg = string.Empty;
                msg = msgError.getMsgText("MSPX00088ERR");
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplatePackingPartMaster";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                //AllowedFileExtensions = new string[] { ".xlsx" },
                AllowedFileExtensions = new string[] { ".csv" },
                MaxFileSize = 20971520
            };
        }

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            PackingPartMaster PM = new PackingPartMaster();

            string[] r = null;
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_PACKING_PART";
            #region createlog
            Guid PROCESS_ID = Guid.NewGuid();
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            string MESSAGE_DETAIL = "";
            long pid = 0;
            //string subFuncName = "EXcelToSQL";
            string isError = "N";
            string processName = "UPLOAD PACKING PART MASTER";
            string resultMessage = "";

            int columns;
            int rows;

            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock L = new Lock();
            string functionIDUpload = "F15-025b";
            int IsLock = L.is_lock(functionIDUpload);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMsgText("MSPX00092ERR");
            }

            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = PM.DeleteDataTBT();
                if (rExtract == "SUCCES")
                {
                    string user_id = getpE_UserId.Username;
                    Log lg = new Log();
                    string msg = "MSPX00035INF|INF|" + new MessagesString().getMsgText("MSPX00035INF").Replace("{0}", processName);
                    pid = lg.createLog(msg, user_id, "Upload Packing Part Master", pid, moduleID, functionIDUpload);
                    L.CREATED_BY = user_id;
                    L.CREATED_DT = DateTime.Now;
                    L.PROCESS_ID = pid;
                    L.FUNCTION_ID = functionIDUpload;
                    L.LOCK_REF = functionIDUpload;
                    L.LockFunction(L);
                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    //Function untuk nge-extract file excel
                    //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
                    //rExtract = ReadAndInsertExcelData(pathfile);
                    //''string delimiter=Upload.GetDelimiter(e.UploadedFile.s)
                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    if (rExtract == string.Empty || rExtract == "SUCCESS")
                    {
                        rExtract = PM.BackgroundProsessSave(functionIDUpload, moduleID, user_id, pid);
                        if (rExtract == "SUCCES")
                            //msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2];
                            rExtract = "0|" + msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2] + "|" + pid.ToString();
                    }
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                //MESSAGE_DETAIL = "File is not .xls";
                //pid = log.createLog(msgError.getMSPX00019ERR("Price"), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                rExtract = "1| File is not .xls OR xlsx";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract;//+ "|" + pid.ToString();
        }

        public ActionResult Delete(List<PackingPartMaster> KeyList)
        {
            PackingPartMaster PPM = new PackingPartMaster();
            string resultMessage = string.Empty;
            resultMessage = PPM.delete(KeyList);
            return Json(new { success = "true", messages = resultMessage }, JsonRequestBehavior.AllowGet);
            return null;
        }

        private string ReadAndInsertExcelData(string FileName)
        {
            try
            {
                XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                ISheet sheet = xssfworkbook.GetSheetAt(0);
                int lastrow = sheet.LastRowNum;
                List<PackingPartMaster.PackingPartMasterUpload> partList = new List<PackingPartMaster.PackingPartMasterUpload>();
                int index = 1;

                PackingPartMaster.PackingPartMasterUpload part;
                for (int i = index; i <= lastrow; i++)
                {
                    part = new PackingPartMaster.PackingPartMasterUpload();
                    IRow row = sheet.GetRow(i);

                    if (row == null) continue;
                    if (row.GetCell(0) == null && row.GetCell(1) == null && row.GetCell(2) == null && row.GetCell(3) == null && row.GetCell(4) == null) continue;

                    if (row.GetCell(0) == null) part.DATA_ID = "";
                    else if (row.GetCell(0).CellType == CellType.Blank || (row.GetCell(0).CellType == CellType.String && (row.GetCell(0).StringCellValue == null || row.GetCell(0).StringCellValue == "")))
                        part.DATA_ID = "";
                    else if (row.GetCell(0).CellType == CellType.Numeric && (row.GetCell(0).NumericCellValue.ToString() == ""))
                        part.DATA_ID = "";
                    else if (row.GetCell(0).CellType == CellType.String)
                        part.DATA_ID = row.GetCell(0).StringCellValue;
                    else if (row.GetCell(0).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(0);
                        string formatString = row.GetCell(0).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.DATA_ID = row.GetCell(0).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.DATA_ID = row.GetCell(0).NumericCellValue.ToString();
                    }
                    else
                        part.DATA_ID = row.GetCell(0).ToString();
                    
                    if (part.DATA_ID.ToLower() != "d") continue;

                    if (row.GetCell(1) == null) part.PART_NO = "";
                    else if (row.GetCell(1).CellType == CellType.Blank || (row.GetCell(1).CellType == CellType.String && (row.GetCell(1).StringCellValue == null || row.GetCell(1).StringCellValue == "")))
                        part.PART_NO = "";
                    else if (row.GetCell(1).CellType == CellType.Numeric && (row.GetCell(1).NumericCellValue.ToString() == ""))
                        part.PART_NO = "";
                    else if (row.GetCell(1).CellType == CellType.String)
                        part.PART_NO = row.GetCell(1).StringCellValue;
                    else if (row.GetCell(1).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(1);
                        string formatString = row.GetCell(1).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PART_NO = row.GetCell(1).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PART_NO = row.GetCell(1).NumericCellValue.ToString();
                    }
                    else
                        part.PART_NO = row.GetCell(1).ToString();

                    if (row.GetCell(2) == null) part.SUPPLIER_CD = "";
                    else if (row.GetCell(2).CellType == CellType.Blank || (row.GetCell(2).CellType == CellType.String && (row.GetCell(2).StringCellValue == null || row.GetCell(2).StringCellValue == "")))
                        part.SUPPLIER_CD = "";
                    else if (row.GetCell(2).CellType == CellType.Numeric && (row.GetCell(2).NumericCellValue.ToString() == ""))
                        part.SUPPLIER_CD = "";
                    else if (row.GetCell(2).CellType == CellType.String)
                        part.SUPPLIER_CD = row.GetCell(2).StringCellValue;
                    else if (row.GetCell(2).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(2);
                        string formatString = row.GetCell(2).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.SUPPLIER_CD = row.GetCell(2).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.SUPPLIER_CD = row.GetCell(2).NumericCellValue.ToString();
                    }
                    else
                        part.SUPPLIER_CD = row.GetCell(2).ToString();

                    if (row.GetCell(3) == null) part.SUPPLIER_PLANT = "";
                    else if (row.GetCell(3).CellType == CellType.Blank || (row.GetCell(3).CellType == CellType.String && (row.GetCell(3).StringCellValue == null || row.GetCell(3).StringCellValue == "")))
                        part.SUPPLIER_PLANT = "";
                    else if (row.GetCell(3).CellType == CellType.Numeric && (row.GetCell(3).NumericCellValue.ToString() == ""))
                        part.SUPPLIER_PLANT = "";
                    else if (row.GetCell(3).CellType == CellType.String)
                        part.SUPPLIER_PLANT = row.GetCell(3).StringCellValue;
                    else if (row.GetCell(3).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(3);
                        string formatString = row.GetCell(3).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.SUPPLIER_PLANT = row.GetCell(3).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.SUPPLIER_PLANT = row.GetCell(3).NumericCellValue.ToString();
                    }
                    else
                        part.SUPPLIER_PLANT = row.GetCell(3).ToString();

                    if (row.GetCell(4) == null) part.DOCK_CD = "";
                    else if (row.GetCell(4).CellType == CellType.Blank || (row.GetCell(4).CellType == CellType.String && (row.GetCell(4).StringCellValue == null || row.GetCell(4).StringCellValue == "")))
                        part.DOCK_CD = "";
                    else if (row.GetCell(4).CellType == CellType.Numeric && (row.GetCell(4).NumericCellValue.ToString() == ""))
                        part.DOCK_CD = "";
                    else if (row.GetCell(4).CellType == CellType.String)
                        part.DOCK_CD = row.GetCell(4).StringCellValue;
                    else if (row.GetCell(4).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(4);
                        string formatString = row.GetCell(4).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.DOCK_CD = row.GetCell(4).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.DOCK_CD = row.GetCell(4).NumericCellValue.ToString();
                    }
                    else
                        part.DOCK_CD = row.GetCell(4).ToString();

                    if (row.GetCell(5) == null) part.KANBAN_NO = "";
                    else if (row.GetCell(5).CellType == CellType.Blank || (row.GetCell(5).CellType == CellType.String && (row.GetCell(5).StringCellValue == null || row.GetCell(5).StringCellValue == "")))
                        part.KANBAN_NO = "";
                    else if (row.GetCell(5).CellType == CellType.Numeric && (row.GetCell(5).NumericCellValue.ToString() == ""))
                        part.KANBAN_NO = "";
                    else if (row.GetCell(5).CellType == CellType.String)
                        part.KANBAN_NO = row.GetCell(5).StringCellValue;
                    else if (row.GetCell(5).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(5);
                        string formatString = row.GetCell(5).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.KANBAN_NO = row.GetCell(5).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.KANBAN_NO = row.GetCell(5).NumericCellValue.ToString();
                    }
                    else
                        part.KANBAN_NO = row.GetCell(5).ToString();

                    if (row.GetCell(6) == null) part.PART_NAME = "";
                    else if (row.GetCell(6).CellType == CellType.Blank || (row.GetCell(6).CellType == CellType.String && (row.GetCell(6).StringCellValue == null || row.GetCell(6).StringCellValue == "")))
                        part.PART_NAME = "";
                    else if (row.GetCell(6).CellType == CellType.Numeric && (row.GetCell(6).NumericCellValue.ToString() == ""))
                        part.PART_NAME = "";
                    else if (row.GetCell(6).CellType == CellType.String)
                        part.PART_NAME = row.GetCell(6).StringCellValue;
                    else if (row.GetCell(6).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(6);
                        string formatString = row.GetCell(6).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PART_NAME = row.GetCell(6).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PART_NAME = row.GetCell(6).NumericCellValue.ToString();
                    }
                    else
                        part.PART_NAME = row.GetCell(6).ToString();

                    if (row.GetCell(7) == null) part.KANBAN_QTY = "";
                    else if (row.GetCell(7).CellType == CellType.Blank || (row.GetCell(7).CellType == CellType.String && (row.GetCell(7).StringCellValue == null || row.GetCell(7).StringCellValue == "")))
                        part.KANBAN_QTY = "";
                    else if (row.GetCell(7).CellType == CellType.Numeric && (row.GetCell(7).NumericCellValue.ToString() == ""))
                        part.KANBAN_QTY = "";
                    else if (row.GetCell(7).CellType == CellType.String)
                        part.KANBAN_QTY = row.GetCell(7).StringCellValue;
                    else if (row.GetCell(7).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(7);
                        string formatString = row.GetCell(7).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.KANBAN_QTY = row.GetCell(7).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.KANBAN_QTY = row.GetCell(7).NumericCellValue.ToString();
                    }
                    else
                        part.KANBAN_QTY = row.GetCell(7).ToString();

                    if (row.GetCell(8) == null) part.PART_TYPE = "";
                    else if (row.GetCell(8).CellType == CellType.Blank || (row.GetCell(8).CellType == CellType.String && (row.GetCell(8).StringCellValue == null || row.GetCell(8).StringCellValue == "")))
                        part.PART_TYPE = "";
                    else if (row.GetCell(8).CellType == CellType.Numeric && (row.GetCell(8).NumericCellValue.ToString() == ""))
                        part.PART_TYPE = "";
                    else if (row.GetCell(8).CellType == CellType.String)
                        part.PART_TYPE = row.GetCell(8).StringCellValue;
                    else if (row.GetCell(8).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(8);
                        string formatString = row.GetCell(8).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PART_TYPE = row.GetCell(8).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PART_TYPE = row.GetCell(8).NumericCellValue.ToString();
                    }
                    else
                        part.PART_TYPE = row.GetCell(8).ToString();

                    if (row.GetCell(9) == null) part.PART_NET_WEIGHT = "";
                    else if (row.GetCell(9).CellType == CellType.Blank || (row.GetCell(9).CellType == CellType.String && (row.GetCell(9).StringCellValue == null || row.GetCell(9).StringCellValue == "")))
                        part.PART_NET_WEIGHT = "";
                    else if (row.GetCell(9).CellType == CellType.Numeric && (row.GetCell(9).NumericCellValue.ToString() == ""))
                        part.PART_NET_WEIGHT = "";
                    else if (row.GetCell(9).CellType == CellType.String)
                        part.PART_NET_WEIGHT = row.GetCell(9).StringCellValue;
                    else if (row.GetCell(9).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(9);
                        string formatString = row.GetCell(9).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PART_NET_WEIGHT = row.GetCell(9).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PART_NET_WEIGHT = row.GetCell(9).NumericCellValue.ToString();
                    }
                    else
                        part.PART_NET_WEIGHT = row.GetCell(9).ToString();

                    if (row.GetCell(10) == null) part.PART_GROSS_WEIGHT = "";
                    else if (row.GetCell(10).CellType == CellType.Blank || (row.GetCell(10).CellType == CellType.String && (row.GetCell(10).StringCellValue == null || row.GetCell(10).StringCellValue == "")))
                        part.PART_GROSS_WEIGHT = "";
                    else if (row.GetCell(10).CellType == CellType.Numeric && (row.GetCell(10).NumericCellValue.ToString() == ""))
                        part.PART_GROSS_WEIGHT = "";
                    else if (row.GetCell(10).CellType == CellType.String)
                        part.PART_GROSS_WEIGHT = row.GetCell(10).StringCellValue;
                    else if (row.GetCell(10).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(10);
                        string formatString = row.GetCell(10).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PART_GROSS_WEIGHT = row.GetCell(10).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PART_GROSS_WEIGHT = row.GetCell(10).NumericCellValue.ToString();
                    }
                    else
                        part.PART_GROSS_WEIGHT = row.GetCell(10).ToString();

                    if (row.GetCell(11) == null) part.PART_LENGTH = "";
                    else if (row.GetCell(11).CellType == CellType.Blank || (row.GetCell(11).CellType == CellType.String && (row.GetCell(11).StringCellValue == null || row.GetCell(11).StringCellValue == "")))
                        part.PART_LENGTH = "";
                    else if (row.GetCell(11).CellType == CellType.Numeric && (row.GetCell(11).NumericCellValue.ToString() == ""))
                        part.PART_LENGTH = "";
                    else if (row.GetCell(11).CellType == CellType.String)
                        part.PART_LENGTH = row.GetCell(11).StringCellValue;
                    else if (row.GetCell(11).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(11);
                        string formatString = row.GetCell(11).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PART_LENGTH = row.GetCell(11).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PART_LENGTH = row.GetCell(11).NumericCellValue.ToString();
                    }
                    else
                        part.PART_LENGTH = row.GetCell(11).ToString();

                    if (row.GetCell(12) == null) part.PART_WIDTH = "";
                    else if (row.GetCell(12).CellType == CellType.Blank || (row.GetCell(12).CellType == CellType.String && (row.GetCell(12).StringCellValue == null || row.GetCell(12).StringCellValue == "")))
                        part.PART_WIDTH = "";
                    else if (row.GetCell(12).CellType == CellType.Numeric && (row.GetCell(12).NumericCellValue.ToString() == ""))
                        part.PART_WIDTH = "";
                    else if (row.GetCell(12).CellType == CellType.String)
                        part.PART_WIDTH = row.GetCell(12).StringCellValue;
                    else if (row.GetCell(12).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(12);
                        string formatString = row.GetCell(12).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PART_WIDTH = row.GetCell(12).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PART_WIDTH = row.GetCell(12).NumericCellValue.ToString();
                    }
                    else
                        part.PART_WIDTH = row.GetCell(12).ToString();

                    if (row.GetCell(13) == null) part.PART_HEIGHT = "";
                    else if (row.GetCell(13).CellType == CellType.Blank || (row.GetCell(13).CellType == CellType.String && (row.GetCell(13).StringCellValue == null || row.GetCell(13).StringCellValue == "")))
                        part.PART_HEIGHT = "";
                    else if (row.GetCell(13).CellType == CellType.Numeric && (row.GetCell(13).NumericCellValue.ToString() == ""))
                        part.PART_HEIGHT = "";
                    else if (row.GetCell(13).CellType == CellType.String)
                        part.PART_HEIGHT = row.GetCell(13).StringCellValue;
                    else if (row.GetCell(13).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(13);
                        string formatString = row.GetCell(13).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PART_HEIGHT = row.GetCell(13).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PART_HEIGHT = row.GetCell(13).NumericCellValue.ToString();
                    }
                    else
                        part.PART_HEIGHT = row.GetCell(13).ToString();

                    if (row.GetCell(14) == null) part.DG_FLAG = "";
                    else if (row.GetCell(14).CellType == CellType.Blank || (row.GetCell(14).CellType == CellType.String && (row.GetCell(14).StringCellValue == null || row.GetCell(14).StringCellValue == "")))
                        part.DG_FLAG = "";
                    else if (row.GetCell(14).CellType == CellType.Numeric && (row.GetCell(14).NumericCellValue.ToString() == ""))
                        part.DG_FLAG = "";
                    else if (row.GetCell(14).CellType == CellType.String)
                        part.DG_FLAG = row.GetCell(14).StringCellValue;
                    else if (row.GetCell(14).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(14);
                        string formatString = row.GetCell(14).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.DG_FLAG = row.GetCell(14).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.DG_FLAG = row.GetCell(14).NumericCellValue.ToString();
                    }
                    else
                        part.DG_FLAG = row.GetCell(14).ToString();

                    if (row.GetCell(15) == null) part.COO = "";
                    else if (row.GetCell(15).CellType == CellType.Blank || (row.GetCell(15).CellType == CellType.String && (row.GetCell(15).StringCellValue == null || row.GetCell(15).StringCellValue == "")))
                        part.COO = "";
                    else if (row.GetCell(15).CellType == CellType.Numeric && (row.GetCell(15).NumericCellValue.ToString() == ""))
                        part.COO = "";
                    else if (row.GetCell(15).CellType == CellType.String)
                        part.COO = row.GetCell(15).StringCellValue;
                    else if (row.GetCell(15).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(15);
                        string formatString = row.GetCell(15).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.COO = row.GetCell(15).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.COO = row.GetCell(15).NumericCellValue.ToString();
                    }
                    else
                        part.COO = row.GetCell(15).ToString();

                    if (row.GetCell(16) == null) part.SUBTITUTION_PART_NO = "";
                    else if (row.GetCell(16).CellType == CellType.Blank || (row.GetCell(16).CellType == CellType.String && (row.GetCell(16).StringCellValue == null || row.GetCell(16).StringCellValue == "")))
                        part.SUBTITUTION_PART_NO = "";
                    else if (row.GetCell(16).CellType == CellType.Numeric && (row.GetCell(16).NumericCellValue.ToString() == ""))
                        part.SUBTITUTION_PART_NO = "";
                    else if (row.GetCell(16).CellType == CellType.String)
                        part.SUBTITUTION_PART_NO = row.GetCell(16).StringCellValue;
                    else if (row.GetCell(16).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(16);
                        string formatString = row.GetCell(16).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.SUBTITUTION_PART_NO = row.GetCell(16).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.SUBTITUTION_PART_NO = row.GetCell(16).NumericCellValue.ToString();
                    }
                    else
                        part.SUBTITUTION_PART_NO = row.GetCell(16).ToString();


                    if (row.GetCell(17) == null) part.SUBTITUTION_CD = "";
                    else if (row.GetCell(17).CellType == CellType.Blank || (row.GetCell(17).CellType == CellType.String && (row.GetCell(17).StringCellValue == null || row.GetCell(17).StringCellValue == "")))
                        part.SUBTITUTION_CD = "";
                    else if (row.GetCell(17).CellType == CellType.Numeric && (row.GetCell(17).NumericCellValue.ToString() == ""))
                        part.SUBTITUTION_CD = "";
                    else if (row.GetCell(17).CellType == CellType.String)
                        part.SUBTITUTION_CD = row.GetCell(17).StringCellValue;
                    else if (row.GetCell(17).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(17);
                        string formatString = row.GetCell(17).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.SUBTITUTION_CD = row.GetCell(17).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.SUBTITUTION_CD = row.GetCell(17).NumericCellValue.ToString();
                    }
                    else
                        part.SUBTITUTION_CD = row.GetCell(17).ToString();


                    if (row.GetCell(18) == null) part.OLD_PART_NO = "";
                    else if (row.GetCell(18).CellType == CellType.Blank || (row.GetCell(18).CellType == CellType.String && (row.GetCell(18).StringCellValue == null || row.GetCell(18).StringCellValue == "")))
                        part.OLD_PART_NO = "";
                    else if (row.GetCell(18).CellType == CellType.Numeric && (row.GetCell(18).NumericCellValue.ToString() == ""))
                        part.OLD_PART_NO = "";
                    else if (row.GetCell(18).CellType == CellType.String)
                        part.OLD_PART_NO = row.GetCell(18).StringCellValue;
                    else if (row.GetCell(18).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(18);
                        string formatString = row.GetCell(18).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.OLD_PART_NO = row.GetCell(18).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.OLD_PART_NO = row.GetCell(18).NumericCellValue.ToString();
                    }
                    else
                        part.OLD_PART_NO = row.GetCell(18).ToString();

                    if (row.GetCell(19) == null) part.PCK_TYPE = "";
                    else if (row.GetCell(19).CellType == CellType.Blank || (row.GetCell(19).CellType == CellType.String && (row.GetCell(19).StringCellValue == null || row.GetCell(19).StringCellValue == "")))
                        part.PCK_TYPE = "";
                    else if (row.GetCell(19).CellType == CellType.Numeric && (row.GetCell(19).NumericCellValue.ToString() == ""))
                        part.PCK_TYPE = "";
                    else if (row.GetCell(19).CellType == CellType.String)
                        part.PCK_TYPE = row.GetCell(19).StringCellValue;
                    else if (row.GetCell(19).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(19);
                        string formatString = row.GetCell(19).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PCK_TYPE = row.GetCell(19).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PCK_TYPE = row.GetCell(19).NumericCellValue.ToString();
                    }
                    else
                        part.PCK_TYPE = row.GetCell(19).ToString();

                    if (row.GetCell(20) == null) part.SAFETY_PCS = "";
                    else if (row.GetCell(20).CellType == CellType.Blank || (row.GetCell(20).CellType == CellType.String && (row.GetCell(20).StringCellValue == null || row.GetCell(20).StringCellValue == "")))
                        part.SAFETY_PCS = "";
                    else if (row.GetCell(20).CellType == CellType.Numeric && (row.GetCell(20).NumericCellValue.ToString() == ""))
                        part.SAFETY_PCS = "";
                    else if (row.GetCell(20).CellType == CellType.String)
                        part.SAFETY_PCS = row.GetCell(20).StringCellValue;
                    else if (row.GetCell(20).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(20);
                        string formatString = row.GetCell(20).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.SAFETY_PCS = row.GetCell(20).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.SAFETY_PCS = row.GetCell(20).NumericCellValue.ToString();
                    }
                    else
                        part.SAFETY_PCS = row.GetCell(20).ToString();

                    if (row.GetCell(21) == null) part.PRMCFC = "";
                    else if (row.GetCell(21).CellType == CellType.Blank || (row.GetCell(21).CellType == CellType.String && (row.GetCell(21).StringCellValue == null || row.GetCell(21).StringCellValue == "")))
                        part.PRMCFC = "";
                    else if (row.GetCell(21).CellType == CellType.Numeric && (row.GetCell(21).NumericCellValue.ToString() == ""))
                        part.PRMCFC = "";
                    else if (row.GetCell(21).CellType == CellType.String)
                        part.PRMCFC = row.GetCell(21).StringCellValue;
                    else if (row.GetCell(21).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(21);
                        string formatString = row.GetCell(21).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PRMCFC = row.GetCell(21).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PRMCFC = row.GetCell(21).NumericCellValue.ToString();
                    }
                    else
                        part.PRMCFC = row.GetCell(21).ToString();

                    if (row.GetCell(22) == null) part.PRMPCT = "";
                    else if (row.GetCell(22).CellType == CellType.Blank || (row.GetCell(22).CellType == CellType.String && (row.GetCell(22).StringCellValue == null || row.GetCell(22).StringCellValue == "")))
                        part.PRMPCT = "";
                    else if (row.GetCell(22).CellType == CellType.Numeric && (row.GetCell(22).NumericCellValue.ToString() == ""))
                        part.PRMPCT = "";
                    else if (row.GetCell(22).CellType == CellType.String)
                        part.PRMPCT = row.GetCell(22).StringCellValue;
                    else if (row.GetCell(22).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(22);
                        string formatString = row.GetCell(22).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PRMPCT = row.GetCell(22).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PRMPCT = row.GetCell(22).NumericCellValue.ToString();
                    }
                    else
                        part.PRMPCT = row.GetCell(22).ToString();

                    if (row.GetCell(23) == null) part.PRMMS = "";
                    else if (row.GetCell(23).CellType == CellType.Blank || (row.GetCell(23).CellType == CellType.String && (row.GetCell(23).StringCellValue == null || row.GetCell(23).StringCellValue == "")))
                        part.PRMMS = "";
                    else if (row.GetCell(23).CellType == CellType.Numeric && (row.GetCell(23).NumericCellValue.ToString() == ""))
                        part.PRMMS = "";
                    else if (row.GetCell(23).CellType == CellType.String)
                        part.PRMMS = row.GetCell(23).StringCellValue;
                    else if (row.GetCell(23).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(23);
                        string formatString = row.GetCell(23).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PRMMS = row.GetCell(23).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PRMMS = row.GetCell(23).NumericCellValue.ToString();
                    }
                    else
                        part.PRMMS = row.GetCell(23).ToString();

                    if (row.GetCell(24) == null) part.PRMXS = "";
                    else if (row.GetCell(24).CellType == CellType.Blank || (row.GetCell(24).CellType == CellType.String && (row.GetCell(24).StringCellValue == null || row.GetCell(24).StringCellValue == "")))
                        part.PRMXS = "";
                    else if (row.GetCell(24).CellType == CellType.Numeric && (row.GetCell(24).NumericCellValue.ToString() == ""))
                        part.PRMXS = "";
                    else if (row.GetCell(24).CellType == CellType.String)
                        part.PRMXS = row.GetCell(24).StringCellValue;
                    else if (row.GetCell(24).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(24);
                        string formatString = row.GetCell(24).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PRMXS = row.GetCell(24).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PRMXS = row.GetCell(24).NumericCellValue.ToString();
                    }
                    else
                        part.PRMXS = row.GetCell(24).ToString();

                    if (row.GetCell(25) == null) part.PRMCD = "";
                    else if (row.GetCell(25).CellType == CellType.Blank || (row.GetCell(25).CellType == CellType.String && (row.GetCell(25).StringCellValue == null || row.GetCell(25).StringCellValue == "")))
                        part.PRMCD = "";
                    else if (row.GetCell(25).CellType == CellType.Numeric && (row.GetCell(25).NumericCellValue.ToString() == ""))
                        part.PRMCD = "";
                    else if (row.GetCell(25).CellType == CellType.String)
                        part.PRMCD = row.GetCell(25).StringCellValue;
                    else if (row.GetCell(25).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(25);
                        string formatString = row.GetCell(25).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PRMCD = row.GetCell(25).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PRMCD = row.GetCell(25).NumericCellValue.ToString();
                    }
                    else
                        part.PRMCD = row.GetCell(25).ToString();

                    if (row.GetCell(26) == null) part.LINE_LEAD_TIME = "";
                    else if (row.GetCell(26).CellType == CellType.Blank || (row.GetCell(26).CellType == CellType.String && (row.GetCell(26).StringCellValue == null || row.GetCell(26).StringCellValue == "")))
                        part.LINE_LEAD_TIME = "";
                    else if (row.GetCell(26).CellType == CellType.Numeric && (row.GetCell(26).NumericCellValue.ToString() == ""))
                        part.LINE_LEAD_TIME = "";
                    else if (row.GetCell(26).CellType == CellType.String)
                        part.LINE_LEAD_TIME = row.GetCell(26).StringCellValue;
                    else if (row.GetCell(26).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(26);
                        string formatString = row.GetCell(26).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.LINE_LEAD_TIME = row.GetCell(26).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.LINE_LEAD_TIME = row.GetCell(26).NumericCellValue.ToString();
                    }
                    else
                        part.LINE_LEAD_TIME = row.GetCell(26).ToString();

                    if (row.GetCell(27) == null) part.START_DT = "";
                    else if (row.GetCell(27).CellType == CellType.Blank || (row.GetCell(27).CellType == CellType.String && (row.GetCell(27).StringCellValue == null || row.GetCell(27).StringCellValue == "")))
                        part.START_DT = "";
                    else if (row.GetCell(27).CellType == CellType.Numeric && (row.GetCell(27).NumericCellValue.ToString() == ""))
                        part.START_DT = "";
                    else if (row.GetCell(27).CellType == CellType.String)
                        part.START_DT = row.GetCell(27).StringCellValue;
                    else if (row.GetCell(27).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(27);
                        string formatString = row.GetCell(27).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.START_DT = row.GetCell(27).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.START_DT = row.GetCell(27).NumericCellValue.ToString();
                    }
                    else
                        part.START_DT = row.GetCell(27).ToString();

                    if (row.GetCell(28) == null) part.END_DT = "";
                    else if (row.GetCell(28).CellType == CellType.Blank || (row.GetCell(28).CellType == CellType.String && (row.GetCell(28).StringCellValue == null || row.GetCell(28).StringCellValue == "")))
                        part.END_DT = "";
                    else if (row.GetCell(28).CellType == CellType.Numeric && (row.GetCell(28).NumericCellValue.ToString() == ""))
                        part.END_DT = "";
                    else if (row.GetCell(28).CellType == CellType.String)
                        part.END_DT = row.GetCell(28).StringCellValue;
                    else if (row.GetCell(28).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(28);
                        string formatString = row.GetCell(28).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.END_DT = row.GetCell(28).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.END_DT = row.GetCell(28).NumericCellValue.ToString();
                    }
                    else
                        part.END_DT = row.GetCell(28).ToString();

                    if (row.GetCell(29) == null) part.PLANT = "";
                    else if (row.GetCell(29).CellType == CellType.Blank || (row.GetCell(29).CellType == CellType.String && (row.GetCell(29).StringCellValue == null || row.GetCell(29).StringCellValue == "")))
                        part.PLANT = "";
                    else if (row.GetCell(29).CellType == CellType.Numeric && (row.GetCell(29).NumericCellValue.ToString() == ""))
                        part.PLANT = "";
                    else if (row.GetCell(29).CellType == CellType.String)
                        part.PLANT = row.GetCell(29).StringCellValue;
                    else if (row.GetCell(29).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(29);
                        string formatString = row.GetCell(29).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PLANT = row.GetCell(29).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PLANT = row.GetCell(29).NumericCellValue.ToString();
                    }
                    else
                        part.PLANT = row.GetCell(29).ToString();

                    if (row.GetCell(30) == null) part.SLOC = "";
                    else if (row.GetCell(30).CellType == CellType.Blank || (row.GetCell(30).CellType == CellType.String && (row.GetCell(30).StringCellValue == null || row.GetCell(30).StringCellValue == "")))
                        part.SLOC = "";
                    else if (row.GetCell(30).CellType == CellType.Numeric && (row.GetCell(30).NumericCellValue.ToString() == ""))
                        part.SLOC = "";
                    else if (row.GetCell(30).CellType == CellType.String)
                        part.SLOC = row.GetCell(30).StringCellValue;
                    else if (row.GetCell(30).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(30);
                        string formatString = row.GetCell(30).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.SLOC = row.GetCell(30).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.SLOC = row.GetCell(30).NumericCellValue.ToString();
                    }
                    else
                        part.SLOC = row.GetCell(30).ToString();

                    if (row.GetCell(31) == null) part.PROD_PURPOSE = "";
                    else if (row.GetCell(31).CellType == CellType.Blank || (row.GetCell(31).CellType == CellType.String && (row.GetCell(31).StringCellValue == null || row.GetCell(31).StringCellValue == "")))
                        part.PROD_PURPOSE = "";
                    else if (row.GetCell(31).CellType == CellType.Numeric && (row.GetCell(31).NumericCellValue.ToString() == ""))
                        part.PROD_PURPOSE = "";
                    else if (row.GetCell(31).CellType == CellType.String)
                        part.PROD_PURPOSE = row.GetCell(31).StringCellValue;
                    else if (row.GetCell(31).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(31);
                        string formatString = row.GetCell(31).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.PROD_PURPOSE = row.GetCell(31).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.PROD_PURPOSE = row.GetCell(31).NumericCellValue.ToString();
                    }
                    else
                        part.PROD_PURPOSE = row.GetCell(31).ToString();

                    if (row.GetCell(32) == null) part.SOURCE_TYPE = "";
                    else if (row.GetCell(32).CellType == CellType.Blank || (row.GetCell(32).CellType == CellType.String && (row.GetCell(32).StringCellValue == null || row.GetCell(32).StringCellValue == "")))
                        part.SOURCE_TYPE = "";
                    else if (row.GetCell(32).CellType == CellType.Numeric && (row.GetCell(32).NumericCellValue.ToString() == ""))
                        part.SOURCE_TYPE = "";
                    else if (row.GetCell(32).CellType == CellType.String)
                        part.SOURCE_TYPE = row.GetCell(32).StringCellValue;
                    else if (row.GetCell(32).CellType == CellType.Numeric)
                    {
                        ICell cell = row.GetCell(32);
                        string formatString = row.GetCell(32).CellStyle.GetDataFormatString();

                        //string value = dataFormatter.FormatCellValue(cell);
                        if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                            part.SOURCE_TYPE = row.GetCell(32).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                        else
                            part.SOURCE_TYPE = row.GetCell(32).NumericCellValue.ToString();
                    }
                    else
                        part.SOURCE_TYPE = row.GetCell(32).ToString();


                    partList.Add(part);
                }

                foreach (PackingPartMaster.PackingPartMasterUpload data in partList)
                {
                    string insertResult = (new PackingPartMaster()).InsertTempPartPrice(data);
                }

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return "1|" + ex.Message;
            }
        }

        public ActionResult SinchronizeSupplierName()
        {
            string[] r = null;
            PackingPartMaster PPM = new PackingPartMaster();
            string resultMessage = PPM.SinchronizeSupplierName();

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1], processId = r[2] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public ActionResult SinchronizeSupplierNameGridCek(List<PackingPartMaster> KeyList)
        {
            PackingPartMaster PPM = new PackingPartMaster();
            PackingPartMaster hapusLock = new PackingPartMaster();
            string[] r = null;
            long pid = 0;
            string functionIDUpload = "DS011";
            string processName = "Sinchronize Supplier Name";
            Lock L = new Lock();
            string hapus = string.Empty;

            string user_id = getpE_UserId.Username;
            Log lg = new Log();
            string msg = "MSPX00035INF|INF|" + new MessagesString().getMsgText("MSPX00035INF").Replace("{0}", processName);
            pid = lg.createLog(msg, user_id, "Get Supplier Name from IPPCS to SPEX", pid, "DS011", functionIDUpload);
            L.CREATED_BY = user_id;
            L.CREATED_DT = DateTime.Now;
            L.PROCESS_ID = pid;
            L.FUNCTION_ID = functionIDUpload;
            L.LOCK_REF = functionIDUpload;
            L.LockFunction(L);

            string resultMessage = PPM.SinchronizeSupplierNameGridCek(KeyList, pid);

            
            return Json(new { success = "true", messages = "", processId = pid }, JsonRequestBehavior.AllowGet);
       
            hapus = hapusLock.DeleteLock(functionIDUpload);

            return null;
        }

        //public void DownloadData_ByParameter(object sender, EventArgs e, string PART_NO, string SUPPLIER_CD, string DOCK_CD, string KANBAN_NO, string SUPPLIER_PlANT, string PART_TYPE, string DG_FLAG, string Mode)
        //modif agi 2017-08-11
        public void DownloadData_ByParameter(object sender, EventArgs e, string Data,string Mode)
        {
            var decode = System.Web.Helpers.Json.Decode(Data);
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/PackingPartMasterDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Part Master");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "PackingPartMaster" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(decode.PART_NO);
            sheet.GetRow(9).GetCell(2).SetCellValue(decode.SUPPLIER_CD);
            sheet.GetRow(10).GetCell(2).SetCellValue(decode.DOCK_CD);
            sheet.GetRow(11).GetCell(2).SetCellValue(decode.KANBAN_NO);
            sheet.GetRow(12).GetCell(2).SetCellValue(decode.PART_TYPE);
            sheet.GetRow(13).GetCell(2).SetCellValue(decode.DG_FLAG);
            sheet.GetRow(14).GetCell(2).SetCellValue(decode.REORDER_FLAG);
            //add agi 2017-08-11
            sheet.GetRow(15).GetCell(2).SetCellValue(decode.COMPANY_CD);
            sheet.GetRow(16).GetCell(2).SetCellValue(decode.RCV_PLANT_CD);
            sheet.GetRow(17).GetCell(2).SetCellValue(decode.CASE_GROUPING);
            
            int row = 21;
            int rowNum = 1;
            IRow Hrow;

            List<PackingPartMaster> model = new List<PackingPartMaster>();
            PackingPartMaster PM = new PackingPartMaster();
            PM.PART_NO = decode.PART_NO;
            PM.SUPPLIER_CD = decode.SUPPLIER_CD;
            PM.SUB_SUPPLIER_CD = decode.SUB_SUPPLIER_CD;
            PM.DOCK_CD = decode.DOCK_CD;
            PM.KANBAN_NO = decode.KANBAN_NO;
            PM.SUPPLIER_PLANT = decode.SUPPLIER_PlANT;
            PM.PART_TYPE = decode.PART_TYPE;
            PM.DG_FLAG = decode.DG_FLAG;
            PM.PMSP_FLAG = decode.PMSP_FLAG;
            //add agi 2017-08-11
            PM.REORDER_FLAG = decode.REORDER_FLAG;
            PM.COMPANY_CD = decode.COMPANY_CD;
            PM.RCV_PLANT_CD = decode.RCV_PLANT_CD;
            PM.CASE_GROUPING = decode.CASE_GROUPING;

            model = PM.GetData(PM, Mode);


            foreach (PackingPartMaster result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PART_NO);
                Hrow.CreateCell(3).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(4).SetCellValue(result.SUPPLIER_PLANT);

                Hrow.CreateCell(5).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(6).SetCellValue(result.SUB_SUPPLIER_PLANT);
                Hrow.CreateCell(7).SetCellValue(result.SUP_NAME);
                Hrow.CreateCell(8).SetCellValue(result.MAD);

                Hrow.CreateCell(9).SetCellValue(result.DOCK_CD);
                Hrow.CreateCell(10).SetCellValue(result.SUBTITUTION_PART_NO);
                Hrow.CreateCell(11).SetCellValue(result.SUBTITUTION_CD);
                Hrow.CreateCell(12).SetCellValue(result.OLD_PART_NO);

                //add agi 2017-08-11
                Hrow.CreateCell(13).SetCellValue(result.REORDER_FLAG);
                Hrow.CreateCell(14).SetCellValue(result.COMPANY_CD);
                Hrow.CreateCell(15).SetCellValue(result.RCV_PLANT_CD);
                Hrow.CreateCell(16).SetCellValue(result.CASE_GROUPING);
                Hrow.CreateCell(17).SetCellValue(result.DAD);

                Hrow.CreateCell(18).SetCellValue(result.KANBAN_NO);
                Hrow.CreateCell(19).SetCellValue(result.PART_NAME);
                Hrow.CreateCell(20).SetCellValue(result.PCK_TYPE);
                Hrow.CreateCell(21).SetCellValue(result.KANBAN_QTY);
                Hrow.CreateCell(22).SetCellValue(result.SAFETY_PCS);
                Hrow.CreateCell(23).SetCellValue(result.PRMCFC);
                Hrow.CreateCell(24).SetCellValue(result.PMSP_FLAG);
                Hrow.CreateCell(25).SetCellValue(result.PRMPCT);
                Hrow.CreateCell(26).SetCellValue(result.PRMMS);
                Hrow.CreateCell(27).SetCellValue(result.PRMXS);
                Hrow.CreateCell(28).SetCellValue(result.PRMCD);
                Hrow.CreateCell(29).SetCellValue(result.LINE_LEAD_TIME);
                if (result.START_DT != null)
                    Hrow.CreateCell(30).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.START_DT));
                else
                    Hrow.CreateCell(30).SetCellValue("");
                if (result.END_DT != null)
                    Hrow.CreateCell(31).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.END_DT));
                else
                    Hrow.CreateCell(31).SetCellValue("");
                Hrow.CreateCell(32).SetCellValue(result.PART_TYPE);
                if (result.PART_NET_WEIGHT != null)
                    Hrow.CreateCell(33).SetCellValue((double)result.PART_NET_WEIGHT);
                else
                    Hrow.CreateCell(33).SetCellValue("");
                if (result.PART_GROSS_WEIGHT != null)
                    Hrow.CreateCell(34).SetCellValue((double)result.PART_GROSS_WEIGHT);
                else
                    Hrow.CreateCell(34).SetCellValue("");
                if (result.PART_LENGTH != null)
                    Hrow.CreateCell(35).SetCellValue((double)result.PART_LENGTH);
                else
                    Hrow.CreateCell(35).SetCellValue("");
                if (result.PART_WIDTH != null)
                    Hrow.CreateCell(36).SetCellValue((double)result.PART_WIDTH);
                else
                    Hrow.CreateCell(36).SetCellValue("");
                if (result.PART_HEIGHT != null)
                    Hrow.CreateCell(37).SetCellValue((double)result.PART_HEIGHT);
                else
                    Hrow.CreateCell(37).SetCellValue("");
                Hrow.CreateCell(38).SetCellValue(result.COO);
                Hrow.CreateCell(39).SetCellValue(result.DG_FLAG);
                Hrow.CreateCell(40).SetCellValue(result.PLANT);
                Hrow.CreateCell(41).SetCellValue(result.SLOC);
                Hrow.CreateCell(42).SetCellValue(result.PROD_PURPOSE);
                Hrow.CreateCell(43).SetCellValue(result.SOURCE_TYPE);
                Hrow.CreateCell(44).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(45).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(46).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(47).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));


                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;

                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;

                //add agi 2017-08-11
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent3;

                Hrow.GetCell(18).CellStyle = styleContent3;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent3;
                Hrow.GetCell(21).CellStyle = styleContent3;
                Hrow.GetCell(22).CellStyle = styleContent3;
                Hrow.GetCell(23).CellStyle = styleContent3;
                Hrow.GetCell(24).CellStyle = styleContent3;
                Hrow.GetCell(25).CellStyle = styleContent2;
                Hrow.GetCell(26).CellStyle = styleContent2;
                Hrow.GetCell(27).CellStyle = styleContent2;
                Hrow.GetCell(28).CellStyle = styleContent3;
                Hrow.GetCell(29).CellStyle = styleContent2;
                Hrow.GetCell(30).CellStyle = styleContent3;
                Hrow.GetCell(31).CellStyle = styleContent3;
                Hrow.GetCell(32).CellStyle = styleContent3;
                Hrow.GetCell(33).CellStyle = styleContent2;
                Hrow.GetCell(34).CellStyle = styleContent2;
                Hrow.GetCell(35).CellStyle = styleContent2;
                Hrow.GetCell(36).CellStyle = styleContent2;
                Hrow.GetCell(37).CellStyle = styleContent2;
                Hrow.GetCell(38).CellStyle = styleContent2;
                Hrow.GetCell(39).CellStyle = styleContent2;
                Hrow.GetCell(40).CellStyle = styleContent2;
                Hrow.GetCell(41).CellStyle = styleContent2;
                Hrow.GetCell(42).CellStyle = styleContent2;
                Hrow.GetCell(43).CellStyle = styleContent2;
                Hrow.GetCell(44).CellStyle = styleContent;
                Hrow.GetCell(45).CellStyle = styleContent2;
                Hrow.GetCell(46).CellStyle = styleContent;
                Hrow.GetCell(47).CellStyle = styleContent2;

                row++;
                rowNum++;
            }


            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        //public void DownloadData_ByTicked(object sender, EventArgs e, string PART_NO, string SUPPLIER_CD, string DOCK_CD, string KANBAN_NO, string SUPPLIER_PlANT, string PART_TYPE, string DG_FLAG, string DOWNLOAD_KEY)
        //modif agi 2017-08-11
        public void DownloadData_ByTicked(object sender, EventArgs e, string Data, string DOWNLOAD_KEY)
        {
            var decode = System.Web.Helpers.Json.Decode(Data);
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/PackingPartMasterDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Part Master");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "PackingPartMaster" + date + ".xlsx";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            //remaks agi 2017-09-15
            //sheet.GetRow(8).GetCell(2).SetCellValue(decode.PART_NO);
            //sheet.GetRow(9).GetCell(2).SetCellValue(decode.SUPPLIER_CD);
            //sheet.GetRow(10).GetCell(2).SetCellValue(decode.DOCK_CD);
            //sheet.GetRow(11).GetCell(2).SetCellValue(decode.KANBAN_NO);
            //sheet.GetRow(12).GetCell(2).SetCellValue(decode.PART_TYPE);
            //sheet.GetRow(13).GetCell(2).SetCellValue(decode.DG_FLAG);
            ////add agi 2017-08-11
            //sheet.GetRow(10).GetCell(2).SetCellValue(decode.DOCK_CD);
            //sheet.GetRow(11).GetCell(2).SetCellValue(decode.KANBAN_NO);
            //sheet.GetRow(12).GetCell(2).SetCellValue(decode.PART_TYPE);
            //sheet.GetRow(13).GetCell(2).SetCellValue(decode.CASE_GROUPING);

            //int row = 20;
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            sheet.GetRow(8).GetCell(2).SetCellValue(decode.PART_NO);
            sheet.GetRow(9).GetCell(2).SetCellValue(decode.SUPPLIER_CD);
            sheet.GetRow(10).GetCell(2).SetCellValue(decode.DOCK_CD);
            sheet.GetRow(11).GetCell(2).SetCellValue(decode.KANBAN_NO);
            sheet.GetRow(12).GetCell(2).SetCellValue(decode.PART_TYPE);
            sheet.GetRow(13).GetCell(2).SetCellValue(decode.DG_FLAG);
            sheet.GetRow(14).GetCell(2).SetCellValue(decode.REORDER_FLAG);
            //add agi 2017-08-11
            sheet.GetRow(15).GetCell(2).SetCellValue(decode.COMPANY_CD);
            sheet.GetRow(16).GetCell(2).SetCellValue(decode.RCV_PLANT_CD);
            sheet.GetRow(17).GetCell(2).SetCellValue(decode.CASE_GROUPING);

            int row = 21;
            
            int rowNum = 1;
            IRow Hrow;

            List<PackingPartMaster> model = new List<PackingPartMaster>();
            PackingPartMaster PM = new PackingPartMaster();
            PM.PART_NO = decode.PART_NO;
            PM.SUPPLIER_CD = decode.SUPPLIER_CD;
            PM.SUB_SUPPLIER_CD = decode.SUB_SUPPLIER_CD;
            PM.DOCK_CD = decode.DOCK_CD;
            PM.KANBAN_NO = decode.KANBAN_NO;
            PM.SUPPLIER_PLANT = decode.SUPPLIER_PlANT;
            PM.PART_TYPE = decode.PART_TYPE;
            PM.DG_FLAG = decode.DG_FLAG;
            PM.PMSP_FLAG = decode.PMSP_FLAG;
            PM.DOWNLOAD_KEY = DOWNLOAD_KEY;

            model = PM.Download(PM);


            foreach (PackingPartMaster result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PART_NO);
                Hrow.CreateCell(3).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(4).SetCellValue(result.SUPPLIER_PLANT);
                
                Hrow.CreateCell(5).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(6).SetCellValue(result.SUB_SUPPLIER_PLANT);
                Hrow.CreateCell(7).SetCellValue(result.SUP_NAME);
                Hrow.CreateCell(8).SetCellValue(result.MAD);

                Hrow.CreateCell(9).SetCellValue(result.DOCK_CD);
                Hrow.CreateCell(10).SetCellValue(result.SUBTITUTION_PART_NO);
                Hrow.CreateCell(11).SetCellValue(result.SUBTITUTION_CD);
                Hrow.CreateCell(12).SetCellValue(result.OLD_PART_NO);

                //add agi 2017-08-11
                Hrow.CreateCell(13).SetCellValue(result.REORDER_FLAG);
                Hrow.CreateCell(14).SetCellValue(result.COMPANY_CD);
                Hrow.CreateCell(15).SetCellValue(result.RCV_PLANT_CD);
                Hrow.CreateCell(16).SetCellValue(result.CASE_GROUPING);
                Hrow.CreateCell(17).SetCellValue(result.DAD);

                Hrow.CreateCell(18).SetCellValue(result.KANBAN_NO);
                Hrow.CreateCell(19).SetCellValue(result.PART_NAME);
                Hrow.CreateCell(20).SetCellValue(result.PCK_TYPE);
                Hrow.CreateCell(21).SetCellValue(result.KANBAN_QTY);
                Hrow.CreateCell(22).SetCellValue(result.SAFETY_PCS);
                Hrow.CreateCell(23).SetCellValue(result.PRMCFC);
                Hrow.CreateCell(24).SetCellValue(result.PMSP_FLAG);
                Hrow.CreateCell(25).SetCellValue(result.PRMPCT);
                Hrow.CreateCell(26).SetCellValue(result.PRMMS);
                Hrow.CreateCell(27).SetCellValue(result.PRMXS);
                Hrow.CreateCell(28).SetCellValue(result.PRMCD);
                Hrow.CreateCell(29).SetCellValue(result.LINE_LEAD_TIME);
                if (result.START_DT != null)
                    Hrow.CreateCell(30).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.START_DT));
                else
                    Hrow.CreateCell(30).SetCellValue("");
                if (result.END_DT != null)
                    Hrow.CreateCell(31).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.END_DT));
                else
                    Hrow.CreateCell(31).SetCellValue("");
                Hrow.CreateCell(32).SetCellValue(result.PART_TYPE);
                if (result.PART_NET_WEIGHT != null)
                    Hrow.CreateCell(33).SetCellValue((double)result.PART_NET_WEIGHT);
                else
                    Hrow.CreateCell(33).SetCellValue("");
                if (result.PART_GROSS_WEIGHT != null)
                    Hrow.CreateCell(34).SetCellValue((double)result.PART_GROSS_WEIGHT);
                else
                    Hrow.CreateCell(34).SetCellValue("");
                if (result.PART_LENGTH != null)
                    Hrow.CreateCell(35).SetCellValue((double)result.PART_LENGTH);
                else
                    Hrow.CreateCell(35).SetCellValue("");
                if (result.PART_WIDTH != null)
                    Hrow.CreateCell(36).SetCellValue((double)result.PART_WIDTH);
                else
                    Hrow.CreateCell(36).SetCellValue("");
                if (result.PART_HEIGHT != null)
                    Hrow.CreateCell(37).SetCellValue((double)result.PART_HEIGHT);
                else
                    Hrow.CreateCell(37).SetCellValue("");
                Hrow.CreateCell(38).SetCellValue(result.COO);
                Hrow.CreateCell(39).SetCellValue(result.DG_FLAG);
                Hrow.CreateCell(40).SetCellValue(result.PLANT);
                Hrow.CreateCell(41).SetCellValue(result.SLOC);
                Hrow.CreateCell(42).SetCellValue(result.PROD_PURPOSE);
                Hrow.CreateCell(43).SetCellValue(result.SOURCE_TYPE);
                Hrow.CreateCell(44).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(45).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(46).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(47).SetCellValue(String.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;

                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent2;
                Hrow.GetCell(12).CellStyle = styleContent2;

                //add agi 2017-08-11
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent3;

                Hrow.GetCell(18).CellStyle = styleContent3;
                Hrow.GetCell(19).CellStyle = styleContent2;
                Hrow.GetCell(20).CellStyle = styleContent3;
                Hrow.GetCell(21).CellStyle = styleContent3;
                Hrow.GetCell(22).CellStyle = styleContent3;
                Hrow.GetCell(23).CellStyle = styleContent3;
                Hrow.GetCell(24).CellStyle = styleContent3;
                Hrow.GetCell(25).CellStyle = styleContent2;
                Hrow.GetCell(26).CellStyle = styleContent2;
                Hrow.GetCell(27).CellStyle = styleContent2;
                Hrow.GetCell(28).CellStyle = styleContent3;
                Hrow.GetCell(29).CellStyle = styleContent2;
                Hrow.GetCell(30).CellStyle = styleContent3;
                Hrow.GetCell(31).CellStyle = styleContent3;
                Hrow.GetCell(32).CellStyle = styleContent3;
                Hrow.GetCell(33).CellStyle = styleContent2;
                Hrow.GetCell(34).CellStyle = styleContent2;
                Hrow.GetCell(35).CellStyle = styleContent2;
                Hrow.GetCell(36).CellStyle = styleContent2;
                Hrow.GetCell(37).CellStyle = styleContent2;
                Hrow.GetCell(38).CellStyle = styleContent2;
                Hrow.GetCell(39).CellStyle = styleContent2;
                Hrow.GetCell(40).CellStyle = styleContent2;
                Hrow.GetCell(41).CellStyle = styleContent2;
                Hrow.GetCell(42).CellStyle = styleContent2;
                Hrow.GetCell(43).CellStyle = styleContent2;
                Hrow.GetCell(44).CellStyle = styleContent;
                Hrow.GetCell(45).CellStyle = styleContent2;
                Hrow.GetCell(46).CellStyle = styleContent;
                Hrow.GetCell(47).CellStyle = styleContent2;

                row++;
                rowNum++;
            }


            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
    }
}
