﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class UploadTLMSController : PageController
    {
        MessagesString msgError = new MessagesString();
        public string moduleID = "SPX02";
        public string functionID = "SPX020200";
        public const string UploadDirectory = "Content\\FileUploadResult";
        public const string TemplateFNameOA = "TLMS_Order_Assignment";
        public const string TemplateFNameDD = "TLMS_Driver_Data";
        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public UploadTLMSController()
        {
            Settings.Title = "Upload TLMS Result";
        }

        protected override void Startup()
        {
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("MOP Inquiry");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        public ActionResult GetUploadTLMSCallBack(string Mode)
        {
            List<mUploadTLMS> model = new List<mUploadTLMS>();
            mUploadTLMS uTLMS = new mUploadTLMS();
            if(Mode.Equals("Search"))
                model = uTLMS.GetData(uTLMS, Mode);
            return PartialView("_PartialGrid", model);
        }

        #region Upload Data
        public ActionResult UploadBackgroundProsess(String uploadType, String filename)
        {
            mUploadTLMS uTLMS = new mUploadTLMS();
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;

            string resultMessage = uTLMS.UploadTLMS(UserID, uploadType, filename);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = msgError.getMSPX00304INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00305INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CallbackUpload([ModelBinder(typeof(DevExpressEditorsBinder))] String uploadType)
        {
            if (uploadType.Equals("uOrderAssingment"))
                UploadControlExtension.GetUploadedFiles("UploadTLMS", UploadControlHelper.ValidationSettings, uc_FileUploadOAComplete);
            else
                UploadControlExtension.GetUploadedFiles("UploadTLMS", UploadControlHelper.ValidationSettings, uc_FileUploadDDComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".csv" }
                //remaks agi 2017-12-13
                ,
                MaxFileSize = 2147483647
            };
        }

        public void uc_FileUploadOAComplete(object sender, FileUploadCompleteEventArgs e)
        {
            mUploadTLMS UT = new mUploadTLMS();

            string rExtract = string.Empty;
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            long pid = 0;

            Lock L = new Lock();
            string functionIDUpload = "SPX020300";
            int IsLock = L.is_lock(functionIDUpload, out pid);
            //add agi 2017-12-13 SET ERROR HANDLING MAX SIZE FROM TB_M_SYSTEM
            decimal size_convert = 1024;
            decimal size = 0;
            string value = new mSystemMaster().GetSystemValueList("TLMS", "MAX_SIZE_UPLOAD", "4", ";").FirstOrDefault().SYSTEM_VALUE;
            decimal maxsize = Convert.ToDecimal(value);
            size = ((Convert.ToDecimal(e.UploadedFile.ContentLength)) / size_convert) / size_convert;
            if (size>maxsize)
            {
                rExtract = "3|" + msgError.getMsgText("MSPXTLMS1ERR").Replace("{0}",maxsize.ToString());
            }
            else if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(pid.ToString());
            }

            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = UT.DeleteDataTBTOA();
                if (rExtract == "SUCCESS")
                {
                    string resultFile = TemplateFNameOA + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, resultFile);

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    string tb_t_name = "spex.TB_T_ORDER_ASSIGNMENT_CSV";
                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));

                    rExtract = resultFile;
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        public void uc_FileUploadDDComplete(object sender, FileUploadCompleteEventArgs e)
        {
            mUploadTLMS UT = new mUploadTLMS();

            string rExtract = string.Empty;
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            long pid = 0;

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock L = new Lock();
            string functionIDUpload = "SPX020400";
            int IsLock = L.is_lock(functionIDUpload, out pid);
            decimal size_convert = 1024;
            decimal size = 0;
            decimal maxsize = Convert.ToDecimal(new mSystemMaster().GetSystemValueList("TLMS", "MAX_SIZE_UPLOAD", "4", ";").FirstOrDefault().SYSTEM_VALUE);
            size = ((Convert.ToDecimal(e.UploadedFile.ContentLength)) / size_convert) / size_convert;
            if (size > maxsize)
            {
                rExtract = "3|" + msgError.getMsgText("MSPXTLMS1ERR").Replace("{0}", maxsize.ToString());
            }
            else if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(pid.ToString());
            }
            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = UT.DeleteDataTBTDD();
                if (rExtract == "SUCCESS")
                {
                    string resultFile = TemplateFNameDD + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, resultFile);

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    string tb_t_name = "spex.TB_T_DRIVER_DATA_CSV";
                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    rExtract = resultFile;
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }
        #endregion
    }
}