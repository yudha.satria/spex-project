﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class PriceMasterController : PageController
    {
        Log log = new Log();
        mPriceMaster PriceMaster = new mPriceMaster();
        MessagesString msgError = new MessagesString();

        //Declare variable for log monitoring
        public readonly string moduleID = "5";
        public readonly string functionID = "50001";
        public readonly string location = "PriceMaster";
        //public readonly string userName = "Ulil.Zakiyatunisa";

        public PriceMasterController()
        {
            Settings.Title = "Price Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //Combo box part number
            ComboPartNumber();

            //Declare error mesaage for java script
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Price Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00053ERR = msgError.getMSPX00053ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            ViewBag.MSPX000018ERR = msgError.getMSPX00018ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        //Formating valid from and valid to
        public string reFormatDateSearch(string dt)
        {
            string result = "";

            if (dt != "01.01.0100") result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));

            return result;
        }

        public string reformatCheck(string Cek)
        {
            string result = "";

            if (Cek != "Unchecked") result = (Cek);

            return result;
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            //DD.MM.YYYY
            if (dt != "01.01.0100" && dt != "")
            {
                ar = dt.Split('.');
                //MM/DD/YYYY
                result = ar[2] + "-" + ar[1] + "-" + ar[0];
            }
            return result;
        }


        //Reload data in grid view
        public ActionResult GridViewPriceMaster(string pPartNumber, string pValidFrom, string pValidTo, string pFranchCd, string pLastPrice, string pFCOnly)
        {
            List<mPriceMaster> model = new List<mPriceMaster>();
            pValidFrom = reFormatDate(pValidFrom);

            pValidTo = reFormatDate(pValidTo);

            {
                model = PriceMaster.getListPriceMaster
                (pPartNumber, pValidFrom, pValidTo, pFranchCd, pLastPrice, pFCOnly);
            }
            return PartialView("GridPriceMaster", model);
        }
        //untuk POPUP
        public ActionResult _viewLog()
        {
            return PartialView("DetailViewLog");
        }


        public ActionResult _PartialLogMonitoringGrid()
        {
            return PartialView("PriceMaster");

        }

        //Reload data part number into combobox
        public void ComboPartNumber()
        {
            List<mPriceMaster> model = new List<mPriceMaster>();

            model = PriceMaster.getPartNoCombobox();
            ViewData["PART_NO"] = model;
        }




        public ActionResult SendParameter()
        {

            long pid = 0;
            string batchName = "Interface From SPOT Batch";
            string moduleID = "5";
            string functionID = "50001";
            string subFuncName = "Interface Batch";

            //Link to user logon
            string userBatch = getpE_UserId.Username;

            string location = "Interface From SPOT";

            pid = log.createLog(msgError.getMSPX00001INB(batchName), userBatch, location + "." + subFuncName, 0, moduleID, functionID);

            string pParameter = "{&quot;PROCESS_ID&quot;:&quot;" + pid + "&quot;}";

            string pID = pid.ToString();
            string pName = "Interface From SPOT";
            string pDescription = "Interface from SPOT to Update Master Price";

            //Link to user logon
            string pSubmitter = getpE_UserId.Username;
            string pFunctionName = "Interface From SPOT Batch";
            string pType = "0";
            string pStatus = "0";
            string pCommand = @"D:\SPIN\Background_Task\Tasks\InterfaceFromSPOTtoSPEX\InterfaceFromSPOT.exe";

            string pStartDate = "0";
            string pEndDate = "0";
            string pPeriodicType = "4";
            string pInterval = null;
            string pExecutionDays = "";
            string pExecutionMonths = "";
            string pTime = "6840000";
            string msg;

            string[] r = null;
            int modelResult_Reprocess = PriceMaster.getValidate_Interface();
            if (modelResult_Reprocess <= 0)
            {
                return Json(new { success = "true", messages = msgError.getMSPX00008INB("Interface").Split('|')[2] }, JsonRequestBehavior.AllowGet);
            }

            else
            {

                msg = msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];

                List<mPriceMaster> model = new List<mPriceMaster>();

                string resultMessage = PriceMaster.InsertParam_Interface(pID, pName, pDescription, pSubmitter, pFunctionName, pParameter, pType, pStatus, pCommand, pStartDate, pEndDate, pPeriodicType, pInterval, pExecutionDays, pExecutionMonths, pTime);

                r = resultMessage.Split('|');
                if (r[0] == "Error ")
                {
                    return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = "true", messages = msg + '|' + pID }, JsonRequestBehavior.AllowGet);
                }
            }
            return null;

        }



        #region cek format file to Upload  Price
        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "Price_Master_Upload";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xls" },
                MaxFileSize = 20971520
            };
        }
        #endregion

        #region Extract Excel
        public const string UploadDirectory = "Content/FileUploadResult";
        public const string TemplateFName = "Price_Master_Upload";

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {

            string[] r = null;
            string rExtract = "";
            #region createlog
            Guid PROCESS_ID = Guid.NewGuid();
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            string MESSAGE_DETAIL = "";
            long pid = 0;
            string subFuncName = "EXcelToSQL";
            string isError = "N";
            string processName = "Upload Price";
            string resultMessage = "";

            int columns;
            int rows;

            #endregion

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            pid = log.createLog(msgError.getMSPX00005INFForLog("Price"), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);

            //checking file name
            if (fileNameUpload == "TemplatePriceMaster")
            {
                //MESSAGE_DETAIL = "Nama file Benar";
                if (e.UploadedFile.IsValid)
                {
                    //MESSAGE_DETAIL = "Format upload adalah .xls
                    //MESSAGE_DETAIL = "File yg di upload data nya gak kosong";

                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    //Function untuk nge-extract file excel
                    rExtract = EXcelToSQL(pathfile, pid);

                    #region generate message after upload
                    string[] aExtract = rExtract.Split('|');

                    if (aExtract[0] == "success")
                    {
                        MESSAGE_DETAIL = "Extracting data is success..";

                        //"success| " + totalUpload.ToString() + "|" + totalUploadSuccess.ToString() + "|" + totalUpdated.ToString() + "|" + totalCantUpdated.ToString() + "|" + totalInvalidData.ToString();

                        MESSAGE_DETAIL = "Total record=" + aExtract[1] + ", " + " Total new record=" + aExtract[2] + ", " + "Total updated record=" + aExtract[3] + ", " + " Total record can't updated=" + aExtract[4] + ", Total Invalid data=" + aExtract[5] + ".";


                        rExtract = "Data finished to upload,\n";
                        if (aExtract[1] != " ")
                        {

                            rExtract = rExtract + "See detail in Log Monitoring Screen with Process ID = " + pid;
                            //MESSAGE_DETAIL = "See detail in log window ..";

                        }
                        else
                        {
                            MESSAGE_DETAIL = "Extracting data is success";

                        }

                        if (aExtract[2] != " ")
                        {

                            MESSAGE_DETAIL = "Some data is cant't uploaded.., " + rExtract;

                        }


                    }
                    else if (aExtract[0] == "error")
                    {
                        rExtract = "error : " + aExtract[1];
                        MESSAGE_DETAIL = "Extracting data is failed, " + rExtract;

                    }
                    #endregion

                }
                else
                {
                    //MESSAGE_DETAIL = "File is not .xls";
                    rExtract = msgError.getMSPX00019ERR("TemplatePriceMaster.xls").Split('|')[2].ToString();
                    pid = log.createLog(msgError.getMSPX00019ERR("TemplatePriceMaster.xls"), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                }
            }
            else
            {
                rExtract = msgError.getMSPX00019ERR("TemplatePriceMaster.xls").Split('|')[2].ToString();
                pid = log.createLog(msgError.getMSPX00019ERR("TemplatePriceMaster.xls"), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
            }
            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract + "|" + PROCESS_ID.ToString();
        }

        public string EXcelToSQL(string path, long pid)
        {
            //IPPCS_QAEntities db = new IPPCS_QAEntities();
            int totalUpload = 0;
            int totalUploadSuccess = 0;
            int totalUpdated = 0;
            int totalCantUpdated = 0;
            int totalInvalidData = 0;

            string PartNumber = "";
            string FranchiseCode = "";

            string PartNumbNotExis = "";
            string FranchiseCdNotExis = "";

            string[] ValidFrom2 = null;
            string[] ValidTo2 = null;

            string sValidFrom = "";
            string sValidTo = "";
            string subFuncName = "EXcelToSQL";
            string isError = "N";
            string processName = "Upload Price";
            string resultMessage = "";

            try
            {
                string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1;MAXSCANROWS=0;\"";
                //string strConn = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", path);
                string result;

                DataSet output = new DataSet();
                using (OleDbConnection conn = new OleDbConnection(strConn))
                {
                    conn.Open();
                    DataTable schemaTable = conn.GetOleDbSchemaTable(
                    OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    string PartNo = "";
                    string FranchiseCd = "";
                    decimal Price;
                    string Currency = "";
                    //DateTime ValidFrom;
                    //DateTime ValidTo;
                    string ValidFrom;
                    string ValidTo;
                    string CreatedBy = "";
                    DateTime CreatedDt;
                    string isUsed = "";
                    string MESSAGE = "";


                    string sheet = "[Sheet1$]";
                    OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + sheet, conn);
                    cmd.CommandType = CommandType.Text;
                    DataTable outputTable = new DataTable(sheet);
                    output.Tables.Add(outputTable);
                    new OleDbDataAdapter(cmd).Fill(outputTable);
                    int x = outputTable.Rows.Count;
                    for (int i = 0; i < outputTable.Rows.Count; i++)
                    {
                        PartNumber = "";
                        FranchiseCode = "";
                        if (outputTable.Rows[i][0].ToString() != "" && outputTable.Rows[i][1].ToString() != "")
                        {

                            totalUpload = totalUpload + 1;


                            //remark by ria 20160427
                            //ValidFrom2 = Convert.ToString(outputTable.Rows[i][4]).Split('.');
                            //ValidTo2 = Convert.ToString(outputTable.Rows[i][5]).Split('.');

                            //if (ValidFrom2.Length <= 1)
                            //    pid = log.createLog(msgError.getMSPX00024ERR("Valid From", totalUpload), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                            //else
                            //    sValidFrom = ValidFrom2[1] + "/" + ValidFrom2[0] + "/" + ValidFrom2[2];

                            //if (ValidTo2.Length <= 1)
                            //    pid = log.createLog(msgError.getMSPX00024ERR("Valid From", totalUpload), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                            //else
                            //    sValidTo = ValidTo2[1] + "/" + ValidTo2[0] + "/" + ValidTo2[2];

                            //throw new Exception("Invalid Valid Data From or Valid To format. Use format [dd.MM.yyyy].");


                            PartNo = outputTable.Rows[i][0].ToString();
                            FranchiseCd = outputTable.Rows[i][1].ToString();

                            Price = Convert.ToDecimal(outputTable.Rows[i][2]);
                            Currency = outputTable.Rows[i][3].ToString();
                            //ValidFrom = Convert.ToDateTime(sValidFrom);
                            //ValidTo = Convert.ToDateTime(sValidTo);
                            ValidFrom = outputTable.Rows[i][4].ToString();
                            ValidTo = outputTable.Rows[i][5].ToString();
                            CreatedBy = getpE_UserId.Username;
                            CreatedDt = DateTime.Now;
                            int RowCount = i + 1;

                            if (PartNo == "" || FranchiseCd == "" || Price == null || Currency == null || ValidFrom == null || ValidTo == null)
                            {
                                //Jika ada kolom yang kosong maka catet log dengan messages error MSPX00021ERR 
                                if (PartNo == null | PartNo == "")
                                    pid = log.createLog(msgError.getMSPX00021ERR("Part Number", totalUpload), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                                totalInvalidData = totalInvalidData + 1;
                                if (FranchiseCd == null | FranchiseCd == "")
                                    pid = log.createLog(msgError.getMSPX00021ERR("Franchise Code", totalUpload), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                                totalInvalidData = totalInvalidData + 1;
                                if (Price == null)
                                    pid = log.createLog(msgError.getMSPX00021ERR("Price", totalUpload), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                                totalInvalidData = totalInvalidData + 1;
                                if (Currency == null)
                                    pid = log.createLog(msgError.getMSPX00021ERR("Currency", totalUpload), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                                totalInvalidData = totalInvalidData + 1;
                                if (ValidFrom == "")
                                    pid = log.createLog(msgError.getMSPX00021ERR("Valid From", totalUpload), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                                totalInvalidData = totalInvalidData + 1;
                                if (ValidTo == "")
                                    pid = log.createLog(msgError.getMSPX00021ERR("Valid To", totalUpload), getpE_UserId.Username, location + '.' + subFuncName, 0, moduleID, functionID);
                                totalInvalidData = totalInvalidData + 1;

                            }
                            else
                            {
                                //Jika semua kolom terisi
                                //PartNo = PriceMaster.CekFranchiseCd(PartNo);
                                result = PriceMaster.SaveUploadData(PartNo, FranchiseCd, Price, Currency, ValidFrom, ValidTo, CreatedBy, pid, RowCount);
                            }


                            //cek apakah partno/franchisecd/ valid from sudah terdaftar

                            //FranchiseCode = PriceMaster.getFranchiseCode(FranchiseCd);

                            //salah satu kriteria tanda kalo row itu benar2 ada isinya

                            //if (PartNumber != "")
                            //{
                            //    PriceMaster.SaveUploadData(PartNo, FranchiseCd, Price, ValidFrom, ValidTo, CreatedBy, pid);
                            //}
                            //else
                            //{
                            //    totalInvalidData = totalInvalidData + 1;
                            //    if (PartNumber == "")
                            //    {
                            //        PartNumbNotExis = PartNumbNotExis + PartNo + ";";
                            //    }
                            //}
                            //end cek
                        }
                    }
                }
                return "success| " + totalUpload.ToString() + "|" + totalUploadSuccess.ToString() + "|" + totalUpdated.ToString() + "|" + totalCantUpdated.ToString() + "|" + totalInvalidData.ToString();
            }
            catch (Exception err)
            {
                return "error|" + err.Message.ToString();
            }
        }
        #endregion

        #region Upload function when user click button upload
        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }
        #endregion


        #region Function save when add data
        public ActionResult AddNewPrice(string PartNo, string FranchiseCode, string Price, string Currency, string ValidFrom, string ValidTo, string CreatedBy)
        {
            CreatedBy = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = PriceMaster.SaveData(PartNo, FranchiseCode, Price, Currency, ValidFrom, ValidTo, CreatedBy);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        #endregion



        #region Function update when user click update data
        public ActionResult UpdatePrice(string PartNo, string FranchiseCode, string Price, string Currency, string ValidFrom, string ValidTo, string ChangedBy)
        {
            ChangedBy = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = PriceMaster.UpdateData(PartNo, FranchiseCode, Price, Currency, ValidFrom, ValidTo, ChangedBy);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        #endregion


        #region Function delete when delete data

        public ActionResult DeletePrice(string key)
        //public ActionResult DeletePrice(string PartNumber, string FranchiseCd , string ValidFrom)
        {
            string[] r = null;
            //string resultMessage = PriceMaster.DeleteData(PartNumber,  FranchiseCd,  ValidFrom);
            string resultMessage = "";
            string[] rows;
            string[] cells;

            rows = key.Split(',');
            for (int i = 0; i < rows.Length; i++)
            {
                cells = rows[i].Split('|');

                string PartNo = cells[0];
                string FranchiseCd = cells[1];
                // string Price = cells[2];
                string ValidFrom = cells[3];
                resultMessage = PriceMaster.DeleteData(PartNo, FranchiseCd, ValidFrom);

            }
            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        #endregion


        #region Function download
        //public void _DownloadPrice(object sender, EventArgs e, string D_PartNumber, string D_ValidFrom, string D_ValidTo)
        //{

        //    string filename = "";
        //    string filesTmp = HttpContext.Request.MapPath("~/Template/Price_Master_Download.xls");
        //    FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

        //    HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

        //    ICellStyle styleContent = workbook.CreateCellStyle();
        //    styleContent.VerticalAlignment = VerticalAlignment.Top;
        //    styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //    styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //    styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //    styleContent.Alignment = HorizontalAlignment.Right;

        //    ICellStyle styleContent2 = workbook.CreateCellStyle();
        //    styleContent2.VerticalAlignment = VerticalAlignment.Top;
        //    styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //    styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //    styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //    styleContent2.Alignment = HorizontalAlignment.Center;

        //    ICellStyle styleContent3 = workbook.CreateCellStyle();
        //    styleContent3.VerticalAlignment = VerticalAlignment.Top;
        //    styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
        //    styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
        //    styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
        //    styleContent3.Alignment = HorizontalAlignment.Left;

        //    ISheet sheet = workbook.GetSheet("PriceMaster");
        //    string date = DateTime.Now.ToString("dd.MM.yyyy");
        //    filename = "DownloadPriceMaster_" + date + ".xls";
        //    //string judul = "TMMIN";
        //    string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

        //    sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
        //    sheet.GetRow(7).GetCell(3).SetCellValue(getpE_UserId.Username);
        //    sheet.GetRow(9).GetCell(3).SetCellValue(D_PartNumber);
        //    sheet.GetRow(10).GetCell(3).SetCellValue(D_ValidFrom);
        //    sheet.GetRow(11).GetCell(3).SetCellValue(D_ValidTo);


        //    int row = 18;
        //    int rowNum = 1;
        //    IRow Hrow;

        //    List<mPriceMaster> model = new List<mPriceMaster>();
        //    model = PriceMaster.PriceMasterDownload(D_PartNumber, D_ValidFrom, D_ValidTo);

        //    foreach (var result in model)
        //    {
        //        Hrow = sheet.CreateRow(row);

        //        Hrow.CreateCell(1).SetCellValue(rowNum);
        //        Hrow.CreateCell(2).SetCellValue(result.PART_NO);
        //        Hrow.CreateCell(3).SetCellValue(result.FRANCHISE_CD.ToString());
        //        Hrow.CreateCell(4).SetCellValue(result.PRICE.ToString());
        //        Hrow.CreateCell(5).SetCellValue(result.CURRENCY_CD.ToString());
        //        Hrow.CreateCell(6).SetCellValue(result.VALID_FROM.ToString("dd.MM.yyyy"));
        //        Hrow.CreateCell(7).SetCellValue(result.VALID_TO.ToString("dd.MM.yyyy"));
        //        Hrow.CreateCell(8).SetCellValue(result.CREATED_BY);
        //        Hrow.CreateCell(9).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm}", result.CREATED_DT));
        //        Hrow.CreateCell(10).SetCellValue(result.CHANGED_BY);
        //        Hrow.CreateCell(11).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm}", result.CHANGED_DT));

        //        //sheet.AddMergedRegion(new CellRangeAddress(row, row, 3, 4));
        //        //Hrow.CreateCell(17).SetCellValue(result.TAKT_TIME.ToString());
        //        //Hrow.CreateCell(18).SetCellValue(result.ORDER_DT.ToString("dd-MM-yyyy"));
        //        Hrow.GetCell(1).CellStyle = styleContent2;
        //        Hrow.GetCell(2).CellStyle = styleContent3;
        //        Hrow.GetCell(3).CellStyle = styleContent2;
        //        //Hrow.GetCell(3).IsMergedCell=
        //        Hrow.GetCell(4).CellStyle = styleContent;
        //        Hrow.GetCell(5).CellStyle = styleContent2;
        //        Hrow.GetCell(6).CellStyle = styleContent2;
        //        Hrow.GetCell(7).CellStyle = styleContent2;
        //        Hrow.GetCell(8).CellStyle = styleContent3;
        //        Hrow.GetCell(9).CellStyle = styleContent2;
        //        Hrow.GetCell(10).CellStyle = styleContent3;
        //        Hrow.GetCell(11).CellStyle = styleContent2;
        //        // Hrow.GetCell(6).CellStyle = styleContent;


        //        row++;
        //        rowNum++;
        //    }
        //    //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
        //    //    sheet.AutoSizeColumn(i);

        //    MemoryStream ms = new MemoryStream();
        //    workbook.Write(ms);
        //    ftmp.Close();
        //    Response.BinaryWrite(ms.ToArray());
        //    Response.ContentType = "application/vnd.ms-excel";
        //    Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        //}

        public void _DownloadPrice(object sender, EventArgs e, string D_PartNumber, string D_ValidFrom, string D_ValidTo, string pFranchCd, string pLastPrice, string pFCOnly)
        {
            //List<mPriceMaster> model = new List<mPriceMaster>();
            D_ValidFrom = reFormatDate(D_ValidFrom);
            D_ValidTo = reFormatDate(D_ValidTo);

            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Price_Master_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Right;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;

            ISheet sheet = workbook.GetSheet("PriceMaster");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "DownloadPriceMaster_" + date + ".xls";
            //string judul = "TMMIN";
            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(3).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(9).GetCell(3).SetCellValue(D_PartNumber);
            sheet.GetRow(10).GetCell(3).SetCellValue(D_ValidFrom);
            sheet.GetRow(11).GetCell(3).SetCellValue(D_ValidTo);


            int row = 18;
            int rowNum = 1;
            IRow Hrow;

            List<mPriceMaster> model = new List<mPriceMaster>();
            model = PriceMaster.getListPriceMaster(D_PartNumber, D_ValidFrom, D_ValidTo, pFranchCd, pLastPrice, pFCOnly);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PART_NO);
                Hrow.CreateCell(3).SetCellValue(result.FRANCHISE_CD.ToString());
                Hrow.CreateCell(4).SetCellValue(result.PRICE.ToString());
                Hrow.CreateCell(5).SetCellValue(result.CURRENCY_CD.ToString());
                Hrow.CreateCell(6).SetCellValue(result.VALID_FROM.ToString("dd.MM.yyyy"));
                Hrow.CreateCell(7).SetCellValue(result.VALID_TO.ToString("dd.MM.yyyy"));
                Hrow.CreateCell(8).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(9).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm}", result.CREATED_DT));
                Hrow.CreateCell(10).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(11).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm}", result.CHANGED_DT));

                //sheet.AddMergedRegion(new CellRangeAddress(row, row, 3, 4));
                //Hrow.CreateCell(17).SetCellValue(result.TAKT_TIME.ToString());
                //Hrow.CreateCell(18).SetCellValue(result.ORDER_DT.ToString("dd-MM-yyyy"));
                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent3;
                Hrow.GetCell(3).CellStyle = styleContent2;
                //Hrow.GetCell(3).IsMergedCell=
                Hrow.GetCell(4).CellStyle = styleContent;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent3;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent3;
                Hrow.GetCell(11).CellStyle = styleContent2;
                // Hrow.GetCell(6).CellStyle = styleContent;


                row++;
                rowNum++;
            }
            //for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
            //    sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
        #endregion

        //add by alira.ria 20160425
        public ActionResult SaveValidation(string PartNo, string FranchiseCode, string ValidFrom)
        {
            string[] r = null;
            string resultMessage = PriceMaster.SaveValidation(PartNo, FranchiseCode, ValidFrom);

            r = resultMessage.Split('|');
            if (r[0].ToLower().Contains("error"))
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        #region Function save when kill data
        public ActionResult KillPrice(string PartNo, string FranchiseCode, string Price, string Currency, string ValidFrom, string ValidTo)
        {
            string UserID = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = PriceMaster.KillData(PartNo, FranchiseCode, Price, Currency, ValidFrom, ValidTo, UserID);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        #endregion
    }
}



