﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class PartMasterController : PageController
    {
        mPartMaster PartMaster = new mPartMaster();
        MessagesString alert = new MessagesString();
        public PartMasterController()
        {
            Settings.Title = "Part Master";
        }
        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()

        {
            ViewBag.MSPX00001ERR = alert.getMSPX00001ERR("Part Master");
            getpE_UserId = (User)ViewData["User"];
        }

        //GET DATA TO GRID BY SEARCHING CRITERIA

        public ActionResult GridViewPartMaster(string pPartNo, string pPartName, string pFranchiseCode)
        {
            List<mPartMaster> model = new List<mPartMaster>();
            {
                model = PartMaster.getPartMaster
                (pPartNo, pPartName, pFranchiseCode);
            }
            return PartialView("GridPartMaster", model);
        }

        public void _DownloadPartMaster(object sender, EventArgs e, string D_PartNo, string D_PartName, string D_FranchiseCode)
        {

            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Part_Master_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Right;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Left;


            //INSERT DATA TO EXCEL HEADER :

            ISheet sheet = workbook.GetSheet("PartMaster");
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            filename = "Part_Master_Download_" + date + ".xls";
            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            sheet.GetRow(6).GetCell(3).SetCellValue(dateNow);
            sheet.GetRow(7).GetCell(3).SetCellValue(getpE_UserId.Username);
            sheet.GetRow(9).GetCell(3).SetCellValue(D_PartNo);
            sheet.GetRow(10).GetCell(3).SetCellValue(D_PartName);
            sheet.GetRow(11).GetCell(3).SetCellValue(D_FranchiseCode);

            int row = 17;
            int rowNum = 1;
            IRow Hrow;

            //Insert data from grid to excell
            List<mPartMaster> model = new List<mPartMaster>();
            model = PartMaster.PartMasterDownload(D_PartNo, D_PartName, D_FranchiseCode);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);
                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PART_NO);
                Hrow.CreateCell(3).SetCellValue(result.PART_NAME);
                Hrow.CreateCell(4).SetCellValue(result.FRANCHISE_CD);
                Hrow.CreateCell(5).SetCellValue(result.RETAIL_PRICE);
                Hrow.CreateCell(6).SetCellValue(result.USED_FLAG);
                Hrow.CreateCell(7).SetCellValue(result.DELETION_FLAG);
                Hrow.CreateCell(8).SetCellValue(result.RELEASE_STATUS);
                Hrow.CreateCell(9).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(10).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CREATED_DT));
                Hrow.CreateCell(11).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(12).SetCellValue(string.Format("{0:dd.MM.yyyy hh:mm:ss}", result.CHANGED_DT));

                Hrow.GetCell(1).CellStyle = styleContent2;
                Hrow.GetCell(2).CellStyle = styleContent3;
                Hrow.GetCell(3).CellStyle = styleContent3;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent3;
                Hrow.GetCell(10).CellStyle = styleContent2;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent2;
                
                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
    }


    }

