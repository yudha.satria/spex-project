﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;
using NPOI.HSSF.UserModel;
using SPEX.Models.Combobox;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class ProblemReportCreationController : PageController
    {

        //Model Problem Report
        mProblemReportCreation ms = new mProblemReportCreation();
        MessagesString messageError = new MessagesString();


        public ProblemReportCreationController()
        {
            Settings.Title = "Problem Report Creation";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }


        public void GetCombobox()
        {
            mCombobox itemNotMandatory = new mCombobox();
            itemNotMandatory.COMBO_LABEL = "--SELECT--";
            itemNotMandatory.COMBO_VALUE = "";

            
            List<mCombobox> ListProblemOrigin = null;
            ListProblemOrigin = ComboboxRepository.Instance.SystemMasterCode("PROBLEM_ORIGIN");
            ListProblemOrigin.Insert(0, itemNotMandatory);
            ViewData["PROBLEM_ORIGIN"] = ListProblemOrigin;

            List<mCombobox> ListProblemOriginGrid = null;
            ListProblemOriginGrid = ComboboxRepository.Instance.SystemMasterCode("PROBLEM_ORIGIN");
            ListProblemOriginGrid.Insert(0, itemNotMandatory);
            ViewData["PROBLEM_ORIGIN_GRID"] = ListProblemOriginGrid;


            List<mCombobox> ListProblemCategory = null;
            ListProblemCategory = ComboboxRepository.Instance.GetProblemCategory("");
            ListProblemCategory.Insert(0, itemNotMandatory);
            ViewData["PROBLEM_CATEGORY_GRID"] = ListProblemCategory;
        }

        public ActionResult GetComboboxProblemCategory(string I_PROBLEM_ORIGIN)
        {
            string a = Request.Params["I_PROBLEM_ORIGIN"];
            mCombobox itemNotMandatory = new mCombobox();
            itemNotMandatory.COMBO_LABEL = "--SELECT--";
            itemNotMandatory.COMBO_VALUE = "";
            List<mCombobox> ListProblemOrigin = null;
            //string combo = Request.Params["SelectedCategoryId"];
            ListProblemOrigin = ComboboxRepository.Instance.GetProblemCategory(I_PROBLEM_ORIGIN);
            ListProblemOrigin.Insert(0, itemNotMandatory);
            ViewData["PROBLEM_CATEGORY"] = ListProblemOrigin;
            return PartialView("_PartialCombobox", ListProblemOrigin);
        }


        protected override void Startup()
        {

            //Call error message
            ViewBag.MSPX00001ERR = messageError.getMSPX00001ERR("Problem Report Creation");
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = messageError.getMSPX00009ERR();
            /*No data found*/
            ViewBag.MSPXS2006ERR = messageError.getMsgText("MSPXS2006ERR");
            /*{0} should not be empty.*/
            ViewBag.MSPXS2005ERR = messageError.getMsgText("MSPXS2005ERR");
            /*A single record must be selected.*/
            ViewBag.MSPXS2017ERR = messageError.getMsgText("MSPXS2017ERR");
            /*Duplication found for {0}.*/
            ViewBag.MSPXS2011ERR = messageError.getMsgText("MSPXS2011ERR");
            /*{0} is not exists in  {1}*/
            ViewBag.MSPXS2010ERR = messageError.getMsgText("MSPXS2010ERR");
            /*Cannot find template file {0} on folder {1}.*/
            ViewBag.MSPXS2034ERR = messageError.getMsgText("MSPXS2034ERR");
            /*Error writing file = {0}. Reason = {1}.*/
            ViewBag.MSPXS2035ERR = messageError.getMsgText("MSPXS2035ERR");
            /*Invalid data type of {0}. Should be {1}.*/
            ViewBag.MSPXS2012ERR = messageError.getMsgText("MSPXS2012ERR");
            /*Are you sure you want to confirm the operation?*/
            ViewBag.MSPXS2018INF = messageError.getMsgText("MSPXS2018INF");
            /*Are you sure you want to abort the operation?*/
            ViewBag.MSPXS2019INF = messageError.getMsgText("MSPXS2019INF");
            /*Are you sure you want to delete the record?*/
            ViewBag.MSPXS2020INF = messageError.getMsgText("MSPXS2020INF");
            /*Are you sure you want to download this data?*/
            ViewBag.MSPXS2021INF = messageError.getMsgText("MSPXS2021INF");
            /*Are you sure you want to send the record ?*/
            ViewBag.MSPXS2062ERR = messageError.getMsgText("MSPXS2062ERR");


            getpE_UserId = (User)ViewData["User"];
            GetCombobox();
            //ViewData["PRIVILEGEs"] = BuyerPD.GetDataBySystemMaster("General", "PRIVILEGE"); //BuyerPD.GetPrivilege();

        }

        public ActionResult ProblemReportCreationCallBack(string Mode)
        {
            List<mProblemReportCreation> model = new List<mProblemReportCreation>();
            if (Mode.Equals("Search"))
                model = ms.getListProblemReport();

            GetCombobox();
            return PartialView("ProblemReportCreationGrid", model);
        }


        #region Function Delete Data
        public ActionResult DeleteProblemReportCreation(string pPrimaryKey)
        {
            if (getpE_UserId != null)
            {
                string p_CREATED_BY = getpE_UserId.Username;

                List<mProblemReportCreation> listKey = new List<mProblemReportCreation>();
                var listKeyHeader = pPrimaryKey.Split(';');


                foreach (var lsKey in listKeyHeader)
                {
                    var listKeyDetail = lsKey.Split('|');
                    mProblemReportCreation data = new mProblemReportCreation();
                    data.PR_CD = listKeyDetail[0].ToString();
                    data.KANBAN_ID = listKeyDetail[1].ToString();
                    data.CHANGED_BY = listKeyDetail[2].ToString();
                    data.CHANGED_DT = listKeyDetail[3].ToString();
                    listKey.Add(data);
                }
                string[] r = null;
                string resultMessage = ms.DeleteData(listKey, p_CREATED_BY);
                r = resultMessage.Split('|');
                if (r[0] != "I")
                {
                    return Json(new { success = false, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { success = false, messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
            }


        }
        #endregion

        #region Function Add
        public ActionResult SaveProblemReportCreation(string pPROBLEM_ORIGIN_CD, string pPROBLEM_CATEGORY_CD, string pCOST_CENTER, string pKANBAN_ID)
        {
            if (getpE_UserId != null)
            {
                string p_CREATED_BY = getpE_UserId.Username;
                string[] r = null;
                string resultMessage = ms.SaveData(pPROBLEM_ORIGIN_CD, pPROBLEM_CATEGORY_CD, pCOST_CENTER, pKANBAN_ID, p_CREATED_BY);
                r = resultMessage.Split('|');
                if (r[0] != "I")
                {
                    return Json(new { success = false, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { success = false, messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        #endregion


        #region Function Add
        public ActionResult EditProblemReportCreation(string pPROBLEM_ORIGIN_CD, string pPROBLEM_CATEGORY_CD, string pCOST_CENTER, string pKANBAN_ID, string pPROBLEM_REPORT_CD, int pPROBLEM_QTY, string iChangedBy, string iChangedDt)
        {
            if (getpE_UserId != null)
            {
                string p_CREATED_BY = getpE_UserId.Username;
                string[] r = null;
                string resultMessage = ms.EditData(pPROBLEM_ORIGIN_CD, pPROBLEM_CATEGORY_CD, pCOST_CENTER, pKANBAN_ID, p_CREATED_BY, pPROBLEM_REPORT_CD, pPROBLEM_QTY, iChangedBy, iChangedDt);
                r = resultMessage.Split('|');
                if (r[0] != "I")
                {
                    return Json(new { success = false, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { success = false, messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        #endregion


        #region Function Send To IPPCS
        public ActionResult SendDataToIPPCS()
        {
            string resultMessage = "";
            var success = true;

            if (getpE_UserId != null)
            {
                string p_CREATED_BY = getpE_UserId.Username;
                string[] r = null;
                string result = ms.SendToIPPCS();
                r = result.Split('|');
                string a = r[0];
                if (r[0] == "Success") {
                    resultMessage = "I|" + messageError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + r[2] + "); return false;'>" + r[2] + "</a>").Split('|')[2] + "|" + r[2].ToString();
                
                }

            }
         
            return Json(new { success = success, messages = resultMessage }, JsonRequestBehavior.AllowGet); 
        }
        #endregion

    }
}
