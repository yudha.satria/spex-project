﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Text;
using NPOI.HSSF.UserModel;
using System.Web;

namespace SPEX.Controllers
{
    public class ShipmentInquiryController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        ShipmentInquiry si = new ShipmentInquiry();
        ShipmentInquiryDetailData sid = new ShipmentInquiryDetailData();
        ShipmentInquiryDetailPrice sip = new ShipmentInquiryDetailPrice();

        public ShipmentInquiryController()
        {
            Settings.Title = "Shipment Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (string.IsNullOrEmpty(dt))
            {
                result = "";
            }
            else if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        protected override void Startup()
        {
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
            ViewData["BookingNo"] = si.ComboBoxGetBookingNo();
            ViewData["STATUS"] = si.ComboBoxGetStatus();
            ViewData["FA_SAP"] = si.ComboBoxGetForwardAgentSAP();
            ViewData["SA_SAP"] = si.ComboBoxGetShipAgentSAP();
            ViewData["BUYER"] = si.ComboBoxGetBuyer("", "");
            ViewData["FA"] = si.ComboBoxGetForwardAgent("", "");
            ViewData["SA"] = si.ComboBoxGetShipAgent("", "");
        }

        #region CALLBACK DATA
        [HttpPost, ValidateInput(false)]
        public ActionResult GetShipmentInquiryCallBack(string BookingNo, string BuyerCd, string FA_Search, 
                                                       string ETDFrom, string ETDTo, string SA_Search,
                                                       string ATDFrom, string ATDTo, string Shpmnt_Search,
                                                       string ContainerNo, string FeederVessel, string Status_Search,
                                                       string FAVendorCD, string SAVendorCD, string Completeness_Search)
        {
            if ((ETDFrom == "01.01.0100") || (ETDFrom == "01.01.0001") || (ETDFrom == "01.01.1900"))
                ETDFrom = "";

            if ((ETDTo == "01.01.0100") || (ETDTo == "01.01.0001") || (ETDTo == "01.01.1900"))
                ETDTo = "";

            if ((ATDFrom == "01.01.0100") || (ATDFrom == "01.01.0001") || (ATDFrom == "01.01.1900"))
                ATDFrom = "";

            if ((ATDTo == "01.01.0100") || (ATDTo == "01.01.0001") || (ATDTo == "01.01.1900"))
                ATDTo = "";

            List<ShipmentInquiry> model = new List<ShipmentInquiry>();
            model = si.getShipmentInquiry(BookingNo, BuyerCd, FA_Search, ETDFrom, ETDTo, SA_Search,
                                          ATDFrom, ATDTo, Shpmnt_Search, ContainerNo,
                                          FeederVessel, Status_Search, FAVendorCD, SAVendorCD, Completeness_Search);

            return PartialView("ShipmentInquiryGrid", model);
        }
        #endregion

        #region EDIT ATD
        public ActionResult CheckEditATD(string SHIPMENT_NO)
        {
            if (getpE_UserId != null)
            {
                string result = si.CheckEditATD(SHIPMENT_NO);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }

        public ActionResult SaveEditATD(string ATD, string SHIPMENT_NO, string BOOKING_NO)
        {
            if (getpE_UserId != null)
            {
                if ((ATD == "01.01.0100") || (ATD == "01.01.0001") || (ATD == "01.01.1900"))
                    ATD = "";

                string result = si.EditATD(ATD, SHIPMENT_NO, BOOKING_NO, getpE_UserId.Username);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }
        #endregion

        #region DETAIL POPUP
        public ActionResult getShipmentInquiryDetail(string BookingNo, string ShipmentNo, string InvoiceNo, string PEBNo)
        {
            List<ShipmentInquiryDetailData> data = new List<ShipmentInquiryDetailData>();
            data = sid.GetData(BookingNo, ShipmentNo, InvoiceNo, PEBNo);
            return PartialView("ShipmentInquiryDetGrid", data);
        }

        public ActionResult DetailBooking(string BookingNo, string ShipmentNo)
        {
            List<ShipmentInquiryDetailData> data = new List<ShipmentInquiryDetailData>();
            data = sid.GetData(BookingNo, ShipmentNo, "", "");
            ViewData["BOOKING_NO"] = BookingNo;
            ViewData["SHIPMENT_NO"] = ShipmentNo;
            return PartialView("ShipmentInquiryDet", data);
        }

        public ActionResult getShipmentInquiryDetShip(string BookingNo, string ShipmentNo, string InvoiceNo, string ServiceCode)
        {
            List<ShipmentInquiryDetailPrice> data = new List<ShipmentInquiryDetailPrice>();
            data = sip.GetData(BookingNo, ShipmentNo, InvoiceNo, ServiceCode);
            return PartialView("ShipmentInquiryDetGridShip", data);
        }

        public ActionResult DetailShipment(string BookingNo, string ShipmentNo)
        {
            List<ShipmentInquiryDetailPrice> data = new List<ShipmentInquiryDetailPrice>();
            data = sip.GetData(BookingNo, ShipmentNo, "", "");
            ViewData["BOOKING_NO"] = BookingNo;
            ViewData["SHIPMENT_NO"] = ShipmentNo;
            return PartialView("ShipmentInquiryDetShip", data);
        }
        #endregion

        #region DOWNLOAD HEADER DATA TO CSV
        public void DownloadData(object sender, EventArgs e, string BookingNo, string BuyerCd, string FA_Search,
                                 string ETDFrom, string ETDTo, string SA_Search, string ATDFrom, string ATDTo, 
                                 string Shpmnt_Search, string ContainerNo, string FeederVessel, string Status_Search,
                                 string FAVendorCD, string SAVendorCD, string Completeness_Search)
        {
            string filename = "";
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "Shipment_Inquiry_Report_" + date + ".csv";

            string dateNow = DateTime.Now.ToString();

            if ((ETDFrom == "01.01.0100") || (ETDFrom == "01.01.0001") || (ETDFrom == "01.01.1900"))
                ETDFrom = "";

            if ((ETDTo == "01.01.0100") || (ETDTo == "01.01.0001") || (ETDTo == "01.01.1900"))
                ETDTo = "";

            if ((ATDFrom == "01.01.0100") || (ATDFrom == "01.01.0001") || (ATDFrom == "01.01.1900"))
                ATDFrom = "";

            if ((ATDTo == "01.01.0100") || (ATDTo == "01.01.0001") || (ATDTo == "01.01.1900"))
                ATDTo = "";

            List<ShipmentInquiry> model = new List<ShipmentInquiry>();
            model = si.getShipmentInquiry(BookingNo, BuyerCd, FA_Search, ETDFrom, ETDTo, SA_Search,
                                        ATDFrom, ATDTo, Shpmnt_Search, ContainerNo,
                                        FeederVessel, Status_Search, FAVendorCD, SAVendorCD, Completeness_Search);
            
            StringBuilder gr = new StringBuilder("");

            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol_Download.Count; i++)
                {
                    gr.Append(Quote(CsvCol_Download[i].Text)); gr.Append(CSV_SEP);
                }
                gr.Append(Environment.NewLine);

                foreach (ShipmentInquiry d in model)
                {
                    csvAddLine(gr, new string[] {
                        Equs(d.STATUS),
                        Equs(d.COMPLETENESS),
                        Equs(d.BOOKING_NO),
                        Equs(d.SHIPMENT_NO),
                        Equs(d.BUYER_CD),
                        Equs(d.ETD == null ? "" : d.ETD.ToString("dd.MM.yyyy") == "01.01.0100" 
                                                  || d.ETD.ToString("dd.MM.yyyy") == "01.01.0001" 
                                                  || d.ETD.ToString("dd.MM.yyyy") == "01.01.1900" 
                                                  ? "" : d.ETD.ToString("dd.MM.yyyy")),
                        Equs(d.ATD == null ? "" : d.ATD.ToString("dd.MM.yyyy") == "01.01.0100" 
                                                  || d.ATD.ToString("dd.MM.yyyy") == "01.01.0001" 
                                                  || d.ATD.ToString("dd.MM.yyyy") == "01.01.1900" 
                                                  ? "" : d.ATD.ToString("dd.MM.yyyy")),
                        Equs(d.ATD_ADDED_BY),
                        Equs(d.CONTAINER_QTY),
                        Equs(d.EXCHANGE_RATE),
                        Equs(d.EXCHANGE_RATE_DT == null ? "" : d.EXCHANGE_RATE_DT.ToString("dd.MM.yyyy") == "01.01.0100" 
                                                              || d.EXCHANGE_RATE_DT.ToString("dd.MM.yyyy") == "01.01.0001" 
                                                              || d.EXCHANGE_RATE_DT.ToString("dd.MM.yyyy") == "01.01.1900" 
                                                              ? "" : d.EXCHANGE_RATE_DT.ToString("dd.MM.yyyy")),
                        Equs(d.TOTAL_AMOUNT),
                        Equs(d.APPROVED_DT == null ? "" : d.APPROVED_DT.ToString("dd.MM.yyyy") == "01.01.0100" 
                                                         || d.APPROVED_DT.ToString("dd.MM.yyyy") == "01.01.0001" 
                                                         || d.APPROVED_DT.ToString("dd.MM.yyyy") == "01.01.1900" 
                                                         ? "" : d.APPROVED_DT.ToString("dd.MM.yyyy")),
                        Equs(d.APPROVED_BY),
                        Equs(d.FA),
                        Equs(d.FA_VENDOR_CD),
                        Equs(d.SA),
                        Equs(d.SA_VENDOR_CD),
                        Equs(d.FEEDER_VESSEL),
                        Equs(d.CREATED_BY),
                        Equs(d.CREATED_DT == null ? "" : d.CREATED_DT.ToString("dd.MM.yyyy") == "01.01.0100" 
                                                         || d.CREATED_DT.ToString("dd.MM.yyyy") == "01.01.0001" 
                                                         || d.CREATED_DT.ToString("dd.MM.yyyy") == "01.01.1900" 
                                                         ? "" : d.CREATED_DT.ToString("dd.MM.yyyy hh:mm:ss")),
                        Equs(d.CHANGED_BY),
                        Equs(d.CHANGED_DT == null ? "" : d.CHANGED_DT.ToString("dd.MM.yyyy") == "01.01.0100" 
                                                         || d.CHANGED_DT.ToString("dd.MM.yyyy") == "01.01.0001" 
                                                         || d.CHANGED_DT.ToString("dd.MM.yyyy") == "01.01.1900" 
                                                         ? "" : d.CHANGED_DT.ToString("dd.MM.yyyy hh:mm:ss"))
                   });
                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        private List<CsvColumn> CsvCol_Download = CsvColumn.Parse(
            "Status|C|1|0|STATUS;" +
            "Completeness|C|1|0|COMPLETENESS;" +
            "Booking No|C|1|0|BOOKING_NO;" +
            "Shipment No|C|6|1|SHIPMENT_NO;" +
            "Buyer Code|C|10|0|BUYER_CD;" +
            "ETD|C|10|0|ETD;" +
            "ATD|C|5|0|ATD;" +
            "ATD Added By|C|23|0|ATD_ADDED_BY;" +
            "Container Qty|C|23|1|CONTAINER_QTY;" +
            "Exchange Rate|C|23|0|EXCHANGE_RATE;" +
            "Exchange Rate Date|C|23|0|EXCHANGE_RATE_DT;" +
            "Total Amount|C|23|1|TOTAL_AMOUNT;" +
            "Approved Date|C|5|0|APPROVED_DT;" +
            "Approved By|C|1|0|APPROVED_BY;" +
            "FA|C|1|0|FA;" +
            "FA Vendor CD|C|1|0|FA_VENDOR_CD;" +
            "SA|C|10|0|SA;" +
            "SA VendorCD|C|10|0|SA_VENDOR_CD;" +
            "Feeder Vessel|C|1|0|FEEDER_VESSEL;" +
            "Created By|C|1|0|CREATED_BY;" +
            "Created Date|C|1|0|CREATED_DT;" +
            "Changed By|C|1|0|CHANGED_BY;" +
            "Changed Date|C|1|0|CHANGED_DT;"
        );

        public class CsvColumn
        {
            public string Text { get; set; }
            public string Column { get; set; }
            public string DataType { get; set; }
            public int Len { get; set; }
            public int Mandatory { get; set; }

            public static List<CsvColumn> Parse(string v)
            {
                List<CsvColumn> l = new List<CsvColumn>();

                string[] x = v.Split(';');

                for (int i = 0; i < x.Length; i++)
                {
                    string[] y = x[i].Split('|');
                    CsvColumn me = null;
                    if (y.Length >= 4)
                    {
                        int len = 0;
                        int mandat = 0;
                        int.TryParse(y[2], out len);
                        int.TryParse(y[3], out mandat);
                        me = new CsvColumn()
                        {
                            Text = y[0],
                            DataType = y[1],
                            Len = len,
                            Mandatory = mandat
                        };
                        l.Add(me);
                    }

                    if (y.Length > 4 && me != null)
                    {
                        me.Column = y[4];
                    }
                }
                return l;
            }
        }

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        private string _csv_sep;
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {
                    _csv_sep = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ";";
                }
                return _csv_sep;
            }
        }

        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        private string Equs(string s)
        {
            string separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (s != null)
            {
                if (s.Contains(separator))
                {
                    s = "\"" + s + "\"";
                }
                else
                {
                    s = "=\"" + s + "\"";
                }
            }
            return s;
        }
        #endregion

        #region DOWNLOAD DATA TO XLS
        public void DownloadDataXLS(object sender, EventArgs e, string BookingNo, string BuyerCd, string FA_Search,
                                    string ETDFrom, string ETDTo, string SA_Search, string ATDFrom, string ATDTo,
                                    string Shpmnt_Search, string ContainerNo, string FeederVessel, string Status_Search,
                                    string FAVendorCD, string SAVendorCD, string Completeness_Search)
        {
            string filename = "";
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "Shipment_Inquiry_Report_" + date + ".xls";

            string dateNow = DateTime.Now.ToString();

            if ((ETDFrom == "01.01.0100") || (ETDFrom == "01.01.0001") || (ETDFrom == "01.01.1900"))
                ETDFrom = "";

            if ((ETDTo == "01.01.0100") || (ETDTo == "01.01.0001") || (ETDTo == "01.01.1900"))
                ETDTo = "";

            if ((ATDFrom == "01.01.0100") || (ATDFrom == "01.01.0001") || (ATDFrom == "01.01.1900"))
                ATDFrom = "";

            if ((ATDTo == "01.01.0100") || (ATDTo == "01.01.0001") || (ATDTo == "01.01.1900"))
                ATDTo = "";

            int row = 2;
            IRow Hrow;
            ISheet sheet;
            ISheet sheet2;
            ISheet sheet3;
            HSSFWorkbook workbook = new HSSFWorkbook();

            #region Sheet 1
            List<ShipmentInquiry> model = new List<ShipmentInquiry>();
            model = si.getShipmentInquiry(BookingNo, BuyerCd, FA_Search, ETDFrom, ETDTo, SA_Search,
                                        ATDFrom, ATDTo, Shpmnt_Search, ContainerNo,
                                        FeederVessel, Status_Search, FAVendorCD, SAVendorCD, Completeness_Search);

            sheet = workbook.CreateSheet("Shipment_Header");

            Hrow = sheet.CreateRow(1);
            Hrow.CreateCell(0).SetCellValue("Status");
            Hrow.CreateCell(1).SetCellValue("Completeness");
            Hrow.CreateCell(2).SetCellValue("Booking No");
            Hrow.CreateCell(3).SetCellValue("Shipment No");
            Hrow.CreateCell(4).SetCellValue("Buyer CD");
            Hrow.CreateCell(5).SetCellValue("ETD");
            Hrow.CreateCell(6).SetCellValue("ATD");
            Hrow.CreateCell(7).SetCellValue("ATD Added By");
            Hrow.CreateCell(8).SetCellValue("Total Container per Shipment");
            Hrow.CreateCell(9).SetCellValue("Exchange Rate");
            Hrow.CreateCell(10).SetCellValue("Exchange Rate Date");
            Hrow.CreateCell(11).SetCellValue("Total Amount");
            Hrow.CreateCell(12).SetCellValue("Approved Date");
            Hrow.CreateCell(13).SetCellValue("Approved By");
            Hrow.CreateCell(14).SetCellValue("FA");
            Hrow.CreateCell(15).SetCellValue("FA Vendor CD");
            Hrow.CreateCell(16).SetCellValue("SA");
            Hrow.CreateCell(17).SetCellValue("SA Vendor CD");
            Hrow.CreateCell(18).SetCellValue("Feeder Vessel");
            Hrow.CreateCell(19).SetCellValue("Created By");
            Hrow.CreateCell(20).SetCellValue("Created Date");
            Hrow.CreateCell(21).SetCellValue("Changed By");
            Hrow.CreateCell(22).SetCellValue("Changed Date");

            foreach (var d in model)
            {
                Hrow = sheet.CreateRow(row);
                Hrow.CreateCell(0).SetCellValue(d.STATUS);
                Hrow.CreateCell(1).SetCellValue(d.COMPLETENESS);
                Hrow.CreateCell(2).SetCellValue(d.BOOKING_NO);
                Hrow.CreateCell(3).SetCellValue(d.SHIPMENT_NO);
                Hrow.CreateCell(4).SetCellValue(d.BUYER_CD);
                Hrow.CreateCell(5).SetCellValue(d.ETD_STR);
                Hrow.CreateCell(6).SetCellValue(d.ATD_STR);
                Hrow.CreateCell(7).SetCellValue(d.ATD_ADDED_BY);
                Hrow.CreateCell(8).SetCellValue(d.CONTAINER_QTY);
                Hrow.CreateCell(9).SetCellValue(d.EXCHANGE_RATE);
                Hrow.CreateCell(10).SetCellValue(d.EXCHANGE_RATE_DT_STR);
                Hrow.CreateCell(11).SetCellValue(d.TOTAL_AMOUNT);
                Hrow.CreateCell(12).SetCellValue(d.APPROVED_DT_STR);
                Hrow.CreateCell(13).SetCellValue(d.APPROVED_BY);
                Hrow.CreateCell(14).SetCellValue(d.FA);
                Hrow.CreateCell(15).SetCellValue(d.FA_VENDOR_CD);
                Hrow.CreateCell(16).SetCellValue(d.SA);
                Hrow.CreateCell(17).SetCellValue(d.SA_VENDOR_CD);
                Hrow.CreateCell(18).SetCellValue(d.FEEDER_VESSEL);
                Hrow.CreateCell(19).SetCellValue(d.CREATED_BY);
                Hrow.CreateCell(20).SetCellValue(d.CREATED_DT_STR);
                Hrow.CreateCell(21).SetCellValue(d.CHANGED_BY);
                Hrow.CreateCell(22).SetCellValue(d.CHANGED_DT_STR);

                row++;
            }
            #endregion

            #region Sheet 2
            row = 2;

            List<ShipmentInquiryDetailData> detail = new List<ShipmentInquiryDetailData>();
            detail = sid.GetDataDownload(BookingNo, BuyerCd, FA_Search, ETDFrom, ETDTo, SA_Search,
                                         ATDFrom, ATDTo, Shpmnt_Search, ContainerNo,
                                         FeederVessel, Status_Search, FAVendorCD, SAVendorCD, Completeness_Search);

            sheet2 = workbook.CreateSheet("Shipment_Detail");

            Hrow = sheet2.CreateRow(1);
            Hrow.CreateCell(0).SetCellValue("Completeness");
            Hrow.CreateCell(1).SetCellValue("Shipment No");
            Hrow.CreateCell(2).SetCellValue("Shipping Instr. No");
            Hrow.CreateCell(3).SetCellValue("Invoice No");
            Hrow.CreateCell(4).SetCellValue("Invoice Date");
            Hrow.CreateCell(5).SetCellValue("Container No");
            Hrow.CreateCell(6).SetCellValue("Vendor CD");
            Hrow.CreateCell(7).SetCellValue("Vendor Type");
            Hrow.CreateCell(8).SetCellValue("Total Container per Invoice");
            Hrow.CreateCell(9).SetCellValue("Total Amount");
            Hrow.CreateCell(10).SetCellValue("PEB No");
            Hrow.CreateCell(11).SetCellValue("PEB Date");
            Hrow.CreateCell(12).SetCellValue("Created By");
            Hrow.CreateCell(13).SetCellValue("Created Date");
            Hrow.CreateCell(14).SetCellValue("Changed By");
            Hrow.CreateCell(15).SetCellValue("Changed Date");

            foreach (var d in detail)
            {
                Hrow = sheet2.CreateRow(row);
                Hrow.CreateCell(0).SetCellValue(d.COMPLETENESS);
                Hrow.CreateCell(1).SetCellValue(d.SHIPMENT_NO);
                Hrow.CreateCell(2).SetCellValue(d.SHIPPING_INSTR_NO);
                Hrow.CreateCell(3).SetCellValue(d.INVOICE_NO);
                Hrow.CreateCell(4).SetCellValue(d.INVOICE_DT_STR);
                Hrow.CreateCell(5).SetCellValue(d.CONTAINER_NO);
                Hrow.CreateCell(6).SetCellValue(d.VENDOR_CD);
                Hrow.CreateCell(7).SetCellValue(d.VENDOR_TYPE);
                Hrow.CreateCell(8).SetCellValue(d.TOTAL_QTY);
                Hrow.CreateCell(9).SetCellValue(d.TOTAL_AMOUNT);
                Hrow.CreateCell(10).SetCellValue(d.PEB_NO);
                Hrow.CreateCell(11).SetCellValue(d.PEB_DT_STR);
                Hrow.CreateCell(12).SetCellValue(d.CREATED_BY);
                Hrow.CreateCell(13).SetCellValue(d.CREATED_DT_STR);
                Hrow.CreateCell(14).SetCellValue(d.CHANGED_BY);
                Hrow.CreateCell(15).SetCellValue(d.CHANGED_DT_STR);

                row++;
            }
            #endregion

            #region Sheet 3
            row = 2;

            List<ShipmentInquiryDetailPrice> price = new List<ShipmentInquiryDetailPrice>();
            price = sip.GetDataDownload(BookingNo, BuyerCd, FA_Search, ETDFrom, ETDTo, SA_Search,
                                         ATDFrom, ATDTo, Shpmnt_Search, ContainerNo,
                                         FeederVessel, Status_Search, FAVendorCD, SAVendorCD, Completeness_Search);

            sheet3 = workbook.CreateSheet("Shipment_Price");

            Hrow = sheet3.CreateRow(1);
            Hrow.CreateCell(0).SetCellValue("Completeness");
            Hrow.CreateCell(1).SetCellValue("Shipment No");
            Hrow.CreateCell(2).SetCellValue("Service ID");
            Hrow.CreateCell(3).SetCellValue("Invoice No");
            Hrow.CreateCell(4).SetCellValue("Vendor CD");
            Hrow.CreateCell(5).SetCellValue("Vendor CD SAP");
            Hrow.CreateCell(6).SetCellValue("Vendor Type");
            Hrow.CreateCell(7).SetCellValue("Vendor Name");
            Hrow.CreateCell(8).SetCellValue("Service Code");
            Hrow.CreateCell(9).SetCellValue("Service Type");
            Hrow.CreateCell(10).SetCellValue("Currency");
            Hrow.CreateCell(11).SetCellValue("Container Size");
            Hrow.CreateCell(12).SetCellValue("QTY");
            Hrow.CreateCell(13).SetCellValue("Unit Price");
            Hrow.CreateCell(14).SetCellValue("VAT");
            Hrow.CreateCell(15).SetCellValue("Total Amount");
            Hrow.CreateCell(16).SetCellValue("Additional Info");
            Hrow.CreateCell(17).SetCellValue("Created By");
            Hrow.CreateCell(18).SetCellValue("Created Date");
            Hrow.CreateCell(19).SetCellValue("Changed By");
            Hrow.CreateCell(20).SetCellValue("Changed Date");

            foreach (var d in price)
            {
                Hrow = sheet3.CreateRow(row);
                Hrow.CreateCell(0).SetCellValue(d.COMPLETENESS);
                Hrow.CreateCell(1).SetCellValue(d.SHIPMENT_NO);
                Hrow.CreateCell(2).SetCellValue(d.SERVICE_ID);
                Hrow.CreateCell(3).SetCellValue(d.INVOICE_NO);
                Hrow.CreateCell(4).SetCellValue(d.VENDOR_CD);
                Hrow.CreateCell(5).SetCellValue(d.VENDOR_CD_SAP);
                Hrow.CreateCell(6).SetCellValue(d.VENDOR_TYPE);
                Hrow.CreateCell(7).SetCellValue(d.VENDOR_NAME);
                Hrow.CreateCell(8).SetCellValue(d.SERVICE_CODE);
                Hrow.CreateCell(9).SetCellValue(d.SERVICE_TYPE);
                Hrow.CreateCell(10).SetCellValue(d.CURRENCY);
                Hrow.CreateCell(11).SetCellValue(d.CONT_SIZE);
                Hrow.CreateCell(12).SetCellValue(d.QTY);
                Hrow.CreateCell(13).SetCellValue(d.UNIT_PRICE);
                Hrow.CreateCell(14).SetCellValue(d.VAT);
                Hrow.CreateCell(15).SetCellValue(d.TOTAL_AMOUNT);
                Hrow.CreateCell(16).SetCellValue(d.ADDITIONAL_INFO);
                Hrow.CreateCell(17).SetCellValue(d.CREATED_BY);
                Hrow.CreateCell(18).SetCellValue(d.CREATED_DT_STR);
                Hrow.CreateCell(19).SetCellValue(d.CHANGED_BY);
                Hrow.CreateCell(20).SetCellValue(d.CHANGED_DT_STR);

                row++;
            }
            #endregion

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            //Response.BinaryWrite(ms.ToArray());
            //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/octet-stream";
            Response.Cache.SetCacheability(HttpCacheability.Private);

            Response.Expires = 0;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", filename));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(ms.ToArray());
            //Response.Flush();
            Response.End();
        }
        #endregion

        #region BUTTON ACTION
        public ActionResult CreateService()
        {
            if (getpE_UserId != null)
            {
                string result = si.CreateService(getpE_UserId.Username);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }

        public ActionResult ReprocessService(string ShipmentList, string CheckAll)
        {
            if (getpE_UserId != null)
            {
                string result = si.ReprocessService(ShipmentList, CheckAll, getpE_UserId.Username);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }

        public ActionResult GetExchangeRate(string ShipmentList, string CheckAll)
        {
            if (getpE_UserId != null)
            {
                string result = si.GetExchangeRate(ShipmentList, CheckAll, getpE_UserId.Username);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }

        public ActionResult ApproveService(string ShipmentList, string CheckAll)
        {
            if (getpE_UserId != null)
            {
                string result = si.ApproveService(ShipmentList, CheckAll, getpE_UserId.Username);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }

        public ActionResult CancelApproval(string ShipmentList)
        {
            if (getpE_UserId != null)
            {
                string result = si.CancelApproval(ShipmentList, getpE_UserId.Username);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }

        public ActionResult CancelService(string ShipmentList)
        {
            if (getpE_UserId != null)
            {
                string result = si.CancelService(ShipmentList, getpE_UserId.Username);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }

        public ActionResult SendToEHS(string ShipmentList)
        {
            if (getpE_UserId != null)
            {
                string result = si.SendToEHS(ShipmentList, getpE_UserId.Username);
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }
        #endregion

        #region CHECK CREATION JOB
        public ActionResult CheckJob()
        {
            if (getpE_UserId != null)
            {
                string result = si.CheckJob();
                string[] results = result.Split('|');
                return Json(new { success = results[0], message = results[1] });
            }
            else
            {
                return Json(new { success = "false", message = "Session Expired" });
            }
        }
        #endregion
    }
}

