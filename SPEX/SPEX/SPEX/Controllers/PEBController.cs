﻿using System.Collections.Generic;
using System.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class PEBController : PageController
    {
        MessagesString msgError = new MessagesString();
        Log log = new Log();
        mPEB peb = new mPEB();

        public PEBController()
        {
            //Settings.Title = "PEB TMMIN PACKING";
            Settings.Title = "PEB PROCESS";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            //DD.MM.YYYY
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                //MM/DD/YYYY
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PEBCallBack(string p_REGISTER_NO, string p_REGISTER_DT_FROM, string p_REGISTER_DT_TO, string p_INVOICE_NO)
        {
            List<mPEB> model = new List<mPEB>();

            p_REGISTER_DT_FROM = p_REGISTER_DT_FROM == "01.01.0100" ? "" : reFormatDate(p_REGISTER_DT_FROM);
            p_REGISTER_DT_TO = p_REGISTER_DT_TO == "01.01.0100" ? "" : reFormatDate(p_REGISTER_DT_TO);

            model = peb.getListPEB(p_REGISTER_NO, p_REGISTER_DT_FROM, p_REGISTER_DT_TO, p_INVOICE_NO);

            return PartialView("PEBGrid", model);
        }

        //Pop Up
        public ActionResult GetPEBDetailGrid(string p_SeqNo, string p_InvoiceNo)
        {
            List<mPEB> model = peb.getListPEBDetail(p_SeqNo, p_InvoiceNo);
            return PartialView("PEBDetailGrid", model);
        }

        public ActionResult SendParameter(string pREGISTER_NO)
        {

            string pID = "";
            string[] r = null;
            string msg;
            string resultMessage = peb.SendData(pREGISTER_NO, getpE_UserId.Username);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                pID = r[2];
                msg = msgError.getMSPX00013INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg + "|" + pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                pID = r[2];
                msg = msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "true", messages = msg + '|' + pID }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PopUpDetail(string p_SeqNo, string p_InvoiceNo)
        {
            ViewData["SetSeqId"] = p_SeqNo;
            List<mPEB> header3 = peb.getListPEBHeader3(p_SeqNo, p_InvoiceNo);
            ViewData["PEBHeader3Data"] = header3;
            List<mPEB> detail = peb.getListPEBDetail(p_SeqNo, p_InvoiceNo);
            ViewData["PEBDetailData"] = detail;

            return PartialView("~/Views/PEB/PEBDetail.cshtml");
        }

        public ActionResult GetPEBHeader3Grid(string p_SeqNo, string p_InvoiceNo)
        {
            List<mPEB> header3 = peb.getListPEBHeader3(p_SeqNo, p_InvoiceNo);

            return PartialView("PEBHeader3Grid", header3);
        }

        public void GetContainerSizeListMaster()
        {
            List<mPEB> containerSizeList = peb.getListOfSystemMasterData("SPEX_PEB_INTERFACE_PARAM", "CONTAINER_SIZE_");
            ViewData["DataContainerSize"] = containerSizeList;
        }

        public ActionResult PopUpSegment(string p_SeqNo, string p_InvoiceNo, string p_RegisterNo)
        {
            if (p_RegisterNo == "")
                ViewData["IsNewData"] = true;
            else
                ViewData["IsNewData"] = false;

            mPEB model = peb.getPEBSegment(p_SeqNo, p_InvoiceNo);
            List<mPEB> importerCDList = peb.getListOfSystemMasterData("SPEX_PEB_INTERFACE_PARAM", "IMPORTER_CD_");
            ViewData["DataImporterCD"] = importerCDList;
            List<mPEB> destPortCDList = peb.getListOfSystemMasterData("SPEX_PEB_INTERFACE_PARAM", "DEST_PORT_CD_");
            ViewData["DataDestPortCD"] = destPortCDList;
            List<mPEB> containerSizeList = peb.getListOfSystemMasterData("SPEX_PEB_INTERFACE_PARAM", "CONTAINER_SIZE_");
            ViewData["DataContainerSize"] = containerSizeList;

            //REMARK BY RIA @20160617
            //List<mPEB> listDat = peb.getListPEBContainer(p_RegisterNo);
            //ADD BY RIA @20160617
            List<mPEB> listDat = peb.getListPEBContainer(p_SeqNo, p_InvoiceNo);
            if (listDat.Count == 0)
            {
                listDat = new List<mPEB>();
                listDat.Add(new mPEB() { SEQ_NO = "1" });
            }

            ViewData["ContainerList"] = listDat;
            ViewData["ContainerIndex"] = listDat.Count;

            return PartialView("~/Views/PEB/PEBSegment.cshtml", model);
        }

        public ActionResult GetSegmentD(string p_ContainerIndex)
        {
            List<mPEB> containerSizeList = peb.getListOfSystemMasterData("SPEX_PEB_INTERFACE_PARAM", "CONTAINER_SIZE_");
            ViewData["DataContainerSize"] = containerSizeList;
            ViewData["ContainerIndex"] = p_ContainerIndex;
            return PartialView("~/Views/PEB/PEBSegmentD.cshtml");
        }
        
        public ActionResult SaveSegment(string pPEB_SEQ, string pINVOICE_NO, string pREGISTER_NO, string pREGISTER_DT, string pVOYAGE_NO, string pDEST_PORT_CD, string pIMPORTER_CD, string pINSPECTION_DT, string pINV_CONTAINER_COUNT, string pCONTAINER)
        {

            string REGISTER_DT = reFormatDate(pREGISTER_DT);
            string INSPECTION_DT = reFormatDate(pINSPECTION_DT);

            string[] r = null;
            string resultMessage = peb.SaveSegmentData(pPEB_SEQ, pINVOICE_NO, pREGISTER_NO, REGISTER_DT,
                      pVOYAGE_NO, pDEST_PORT_CD, pIMPORTER_CD, INSPECTION_DT,
                      pINV_CONTAINER_COUNT, pCONTAINER, getpE_UserId.Username);

            string msg = "";
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = msgError.getMSPX00034INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg + "|" + pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00001INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg + "|" + pID }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult UpdateSegment(string pPEB_SEQ, string pINVOICE_NO, string pREGISTER_NO, string pREGISTER_DT, string pVOYAGE_NO, string pDEST_PORT_CD, string pIMPORTER_CD, string pINSPECTION_DT, string pINV_CONTAINER_COUNT, string pCONTAINER)
        {

            string REGISTER_DT = reFormatDate(pREGISTER_DT);
            string INSPECTION_DT = reFormatDate(pINSPECTION_DT);

            string[] r = null;
            string resultMessage = peb.UpdateSegmentData(pPEB_SEQ, pINVOICE_NO, pREGISTER_NO, REGISTER_DT,
                      pVOYAGE_NO, pDEST_PORT_CD, pIMPORTER_CD, INSPECTION_DT,
                      pINV_CONTAINER_COUNT, pCONTAINER, getpE_UserId.Username);

            string msg = "";
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = msgError.getMSPX00034INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg + "|" + pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00001INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg + "|" + pID }, JsonRequestBehavior.AllowGet);
            }

        }

        public string reFormatReportDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[0] + ar[1] + ar[2];
            }
            return result;
        }

        public string CheckInvoice(string pInvoice)
        {
            string result = "EXIST";

            mInvoice inv = new mInvoice().getInvoice(pInvoice);

            return inv == null ? "NOT EXIST" : result;
        }

        public ActionResult CheckProcess(string pPEB_SEQ, string pINVOICE_NO)
        {
            string resultMessage = peb.CheckProcess(pPEB_SEQ, pINVOICE_NO);
            string[] r = null;
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteData(string pREGISTER_NO)
        {
            string[] r = null;
            string msg = "";
            if (getpE_UserId != null)
            {
                //rEMAKS AGI 2017-03-03
                //string resultMessage = peb.DeleteData(pREGISTER_NO);
                string resultMessage = peb.DeleteDataByUserId(pREGISTER_NO, getpE_UserId.Username);
                r = resultMessage.Split('|');
                if (r[0].ToString().ToLower().Contains("error"))
                    return Json(new { success = "false", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = "true", messages = r[1].ToString() }, JsonRequestBehavior.AllowGet); 
            }
            else
            {
                return Json(new { success = "false", messages ="session anda telah habis silahkan login ulang" }, JsonRequestBehavior.AllowGet);
            }
            
        }
    }
}
