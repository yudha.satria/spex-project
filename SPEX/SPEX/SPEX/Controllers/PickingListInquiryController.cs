﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;
using NPOI.HSSF.UserModel;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;

//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class PickingListInquiryController : PageController
    {

        //Model Maintenance Warehouse Master
        mPickingListInquiry ms = new mPickingListInquiry();
            
        MessagesString msgError = new MessagesString();


        public PickingListInquiryController()
        {
            Settings.Title = "Picking List Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }


        public void GetComboboxStatus()
        { 
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            List<mSystemMaster> model = new List<mSystemMaster>();
            model = (new mSystemMaster()).GetComboBoxList("PICKING_LIST_FLAG");
            model.Insert(0, item); 
            ViewData["PICKING_LIST_FLAG"] = model; 
        }


        public void GetComboboxZone()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            model = ms.GetComboBoxZoneList(); 
            model.Insert(0, item);
            ViewData["ZONE_FLAG"] = model; 
        }

        public void GetComboboxPrinter()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--SELECT--";
            model = ms.GetAllPrinter();
            model.Insert(0, item);
            ViewData["PRINTER_FLAG"] = model; 
        }

        public void GetPickingListTime()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--All--";
            model = (new mSystemMaster()).GetComboBoxList("PICKING_LIST_TIME"); 
            model.Insert(0, item);
            ViewData["PICKING_LIST_TIME_FLAG"] = model;
             
        }

        public void GetComboboxTranpCode()
        {
          mSystemMaster item = new mSystemMaster();
          List<mSystemMaster> model = new List<mSystemMaster>();
          item.SYSTEM_CD = "";
          item.SYSTEM_VALUE = "--All--";
          model = ms.GetComboBoxTranpCode();
          model.Insert(0, item);
          ViewData["TRANP_CODE"] = model;
        }

        protected override void Startup()
        {

            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMsgText("MSPXS2006ERR");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR(); 
            /*A single record must be selected.*/
            ViewBag.MSPXS2017ERR = msgError.getMsgText("MSPXS2017ERR");

            /*No data found*/
            ViewBag.MSPX00001ERR = msgError.getMsgText("MSPXS2006ERR");
            /*Cannot find template file {0} on folder {1}.*/
            ViewBag.MSPXS2034ERR = msgError.getMsgText("MSPXS2034ERR");
            /*Error writing file = {0}. Reason = {1}.*/
            ViewBag.MSPXS2035ERR = msgError.getMsgText("MSPXS2035ERR");
            /*Are you sure you want to download this data?*/
            ViewBag.MSPXS2021INF = msgError.getMsgText("MSPXS2021INF"); 

            getpE_UserId = (User)ViewData["User"];
            GetComboboxStatus();
            GetComboboxZone();
            GetComboboxPrinter();
            GetPickingListTime();
            GetComboboxTranpCode();
 
        }

        public ActionResult PickingListInquiryCallBack(string pPICKING_LIST_NO, string pPICKING_LIST_DATE, string pPICKING_LIST_DATE_TO, string pSTATUS, string pZONE, string pPICKING_LIST_TIME, string pTRANSP_CD, string Mode)
        {
            List<mPickingListInquiry> model = new List<mPickingListInquiry>();
            if (Mode.Equals("Search")) {
                pPICKING_LIST_DATE = reFormatDate(pPICKING_LIST_DATE);
                pPICKING_LIST_DATE_TO = reFormatDate(pPICKING_LIST_DATE_TO);
                model = ms.getListPickingListInquiry(pPICKING_LIST_NO, pPICKING_LIST_DATE, pPICKING_LIST_DATE_TO, pSTATUS, pZONE, pPICKING_LIST_TIME, pTRANSP_CD);
            }
            GetComboboxStatus();
            GetComboboxZone();
            GetComboboxPrinter();
            GetPickingListTime();
            GetComboboxTranpCode();

            return PartialView("PickingListInquiryGrid", model);
        }

        public ActionResult CancelData(string pMANIFEST_NO)
        {
            List<mPickingListInquiry> listDataModel = new List<mPickingListInquiry>();
            mPickingListInquiry dataModel = new mPickingListInquiry();
            var listKeyHeader = pMANIFEST_NO.Split(';');
            foreach (var lsKeyHeader in listKeyHeader)
            {
                var listKeyDetail = lsKeyHeader.Split('|');
                dataModel = new mPickingListInquiry(); 
                dataModel.PICKING_LIST_NO = listKeyDetail[0].ToString();
                dataModel.CHANGED_BY = listKeyDetail[1].ToString();
                dataModel.CHANGED_DT = listKeyDetail[2].ToString();  
                listDataModel.Add(dataModel);
            }
            String resultMessage = ms.CancelData(listDataModel); 
            return Json(new { success = resultMessage.Split('|')[0].Equals("E") ? "false" : "true", messages = resultMessage.Split('|')[1] }, JsonRequestBehavior.AllowGet);
          
        }


        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2]; 
            }

            return result;
        }       



        #region Function Download
        //function download 
        public ActionResult DownloadPickingListInquiry(object sender, EventArgs e, string pPICKING_LIST_NO, string pPICKING_LIST_DATE, string pPICKING_LIST_DATE_TO, string pSTATUS, string pZONE, string pPICKING_LIST_TIME, string pTRANSP_CD)
        {
            //GenerateReport("","","","");
            string rExtract = string.Empty;
            string filename = "Picking_List_Inquiry_Download.xls";
            string directory = "Template";
            string filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);

            List<mPickingListInquiry> model = new List<mPickingListInquiry>();
            pPICKING_LIST_DATE = reFormatDate(pPICKING_LIST_DATE);
            pPICKING_LIST_DATE_TO = reFormatDate(pPICKING_LIST_DATE_TO);
            model = ms.getListPickingListInquiryDownload(pPICKING_LIST_NO, pPICKING_LIST_DATE, pPICKING_LIST_DATE_TO, pSTATUS, pZONE, pPICKING_LIST_TIME, pTRANSP_CD);
             
            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            else if (model.Count() == 0)
            {
                lError = 1;
                status = "Data";
                rExtract = model.Count().ToString();
            }
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1)
                return Json(new { success = false, messages = rExtract, status = status }, JsonRequestBehavior.AllowGet);
            #endregion 
        
            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true); 
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("Picking List Inquiry");
            string date = DateTime.Now.ToString("ddMMyyyyHHmm");
            filename = "Picking_List_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);

            int row = 11;
            int rowNum = 1;
            IRow Hrow;

            
            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.PICKING_LIST_NO);
                Hrow.CreateCell(3).SetCellValue(result.ZONE);
                Hrow.CreateCell(4).SetCellValue(result.PICKING_LIST_DATE);
                Hrow.CreateCell(5).SetCellValue(result.PICKING_LIST_TIME);
                Hrow.CreateCell(6).SetCellValue(result.PART_NO);
                Hrow.CreateCell(7).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(8).SetCellValue(result.TMAP_ITEM_NO);
                Hrow.CreateCell(9).SetCellValue(result.RACK_ADDRESS_CD);
                Hrow.CreateCell(10).SetCellValue(result.ORDER_QTY);
                Hrow.CreateCell(11).SetCellValue(result.TMAP_ORDER_QTY);
                Hrow.CreateCell(12).SetCellValue(result.REMAIN_RECEIVE_QTY);
                Hrow.CreateCell(13).SetCellValue(result.ORDER_TYPE_TMMIN);
                Hrow.CreateCell(14).SetCellValue(result.TMAP_ORDER_TYPE);
                Hrow.CreateCell(15).SetCellValue(result.BUYER_PD);
                Hrow.CreateCell(16).SetCellValue(result.TRANSP_CD);
                Hrow.CreateCell(17).SetCellValue(result.STATUS); 

                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent2;
                Hrow.GetCell(3).CellStyle = styleContent2;
                Hrow.GetCell(4).CellStyle = styleContent2;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent2;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent2;
                Hrow.GetCell(10).CellStyle = styleContent3;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent3;
                Hrow.GetCell(13).CellStyle = styleContent2;
                Hrow.GetCell(14).CellStyle = styleContent2;
                Hrow.GetCell(15).CellStyle = styleContent2;
                Hrow.GetCell(16).CellStyle = styleContent2;
                Hrow.GetCell(17).CellStyle = styleContent2; 
                row++;
                rowNum++;
            }

            MemoryStream memory = new MemoryStream();
            workbook.Write(memory);
            ftmp.Close();
            Response.BinaryWrite(memory.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);        
        }
            
        #endregion

        #region _PopUpLog  dan _PopUpLogKanban

        public ActionResult _PopUpLog(string pPICKING_LIST_NO, string pPART_NO, string pORDER_NO)
        {
            List<mPickingListInquiryDetailPart> model = new List<mPickingListInquiryDetailPart>();
            mPickingListInquiryDetailPart detailPart = new mPickingListInquiryDetailPart();
            model = detailPart.getDetailParts(pPICKING_LIST_NO, pPART_NO, pORDER_NO);

            GetComboboxPrinter();
            return PartialView("DetailPart", model);
        }

        public ActionResult _PopUpLogSearch(string pPICKING_LIST_NO, string pPART_NO, string pORDER_NO,string Mode)
        {
            List<mPickingListInquiryDetailPart> model = new List<mPickingListInquiryDetailPart>();
            mPickingListInquiryDetailPart detailPart = new mPickingListInquiryDetailPart();
            if (Mode.Equals("Search"))
            {
                model = detailPart.getDetailParts(pPICKING_LIST_NO, pPART_NO, pORDER_NO);
            }
            GetComboboxPrinter();
            return PartialView("DetailPartGrid", model);
        }

        public ActionResult _PopUpLogKanban(string pPICKING_LIST_NO, string pPART_NO, string pTMAP_ITEM_NO)
        {
            List<mPickingListInquiryDetailKanban> model = new List<mPickingListInquiryDetailKanban>();
            mPickingListInquiryDetailKanban detailKanban = new mPickingListInquiryDetailKanban();
            model = detailKanban.getDetailKanbans(pPICKING_LIST_NO, pPART_NO, pTMAP_ITEM_NO);

            GetComboboxPrinter();
            return PartialView("DetailKanbanGrid", model);
        } 

        #endregion

        #region PDF

        public ActionResult GenaretePDFPickingListKanban(string pMANIFEST_NO, string pCheckBox)
        {
            List<mPickingListInquiry> listDataModel = new List<mPickingListInquiry>();
            mPickingListInquiry dataModel = new mPickingListInquiry();
            List<string> listReportKanban = new List<string>();

            string rExtract = string.Empty;
            string filename = "Data_source_of_Picking_List.trdx";
            string directory = "Report";
            string filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);
            //String source = Server.MapPath("~/Report/Data_source_of_Picking_List.trdx");

            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            //else if (model.Count() == 0)
            //{
            //    lError = 1;
            //    status = "Data";
            //    rExtract = model.Count().ToString();
            //}
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1) 
                return Json(new { success = false, messages = "Cannot find template file "+ filename +" on folder "+ directory+".", status = status }, JsonRequestBehavior.AllowGet);
            #endregion 



            var listKeyHeader = pMANIFEST_NO.Split(';');
            foreach (var lsKeyHeader in listKeyHeader)
            {
                var listKeyDetail = lsKeyHeader.Split('|');
                dataModel = new mPickingListInquiry(); 
                dataModel.PICKING_LIST_NO = listKeyDetail[0].ToString(); 
                listDataModel.Add(dataModel);
            }
            string pPICKING_LIST_NO;
            foreach (mPickingListInquiry dt in listDataModel)
            {
                string name = null;
                pPICKING_LIST_NO = dt.PICKING_LIST_NO;
                name = GenerateReport(pPICKING_LIST_NO);
                if (name != null)
                    listReportKanban.Add(name);

                if (pCheckBox == "true")
                {
                    List<mPickingListInquiryDetailPart> listDetailPart = new List<mPickingListInquiryDetailPart>();
                    mPickingListInquiryDetailPart detailPart = new mPickingListInquiryDetailPart();
                    listDetailPart = detailPart.getDetailParts(dt.PICKING_LIST_NO, "", "");

                    //foreach (var dp in listDetailPart)
                    //{
                        string nameKanban = null;
                        nameKanban = GeneratePickingListKanban_New(dt.PICKING_LIST_NO, "", "","");
                        if (nameKanban != null)
                            listReportKanban.Add(nameKanban); 
                    //} 
                } 

            }
            return Json(new { success = "true", messages = "success", FILE_NAME = listReportKanban }, JsonRequestBehavior.AllowGet);
        }  

        public ActionResult GenaretePDFPartNo(string pMANIFEST_NO)
        {
            List<mPickingListInquiry> listDataModel = new List<mPickingListInquiry>();
            mPickingListInquiry dataModel = new mPickingListInquiry();
            List<string> listReportKanban = new List<string>();



            var listKeyHeader = pMANIFEST_NO.Split(';');
            foreach (var lsKeyHeader in listKeyHeader)
            {
                var listKeyDetail = lsKeyHeader.Split('|');
                dataModel = new mPickingListInquiry();
                //dataModel.MODE = iMode;
                dataModel.PICKING_LIST_NO = listKeyDetail[0].ToString();
                dataModel.PART_NO = listKeyDetail[1].ToString();
                dataModel.TMAP_ITEM_NO = listKeyDetail[3].ToString();
                listDataModel.Add(dataModel);
            }
            string pPICKING_LIST_NO, pPART_NO, pKANBAN_ID;
            foreach (mPickingListInquiry dt in listDataModel)
            {
                pPICKING_LIST_NO = dt.PICKING_LIST_NO;
                List<mPickingListInquiryDetailKanban> listDetailKanban = new List<mPickingListInquiryDetailKanban>();
                mPickingListInquiryDetailKanban detailKanban = new mPickingListInquiryDetailKanban();
                listDetailKanban = detailKanban.getDetailKanbans(dt.PICKING_LIST_NO, dt.PART_NO, dt.TMAP_ITEM_NO);
                string name = null;
                name = GeneratePickingListKanban_New(pPICKING_LIST_NO, dt.PART_NO, "", dt.TMAP_ITEM_NO);
                if (name != null)
                    listReportKanban.Add(name); 
            }
            return Json(new { success = "true", messages = "sukses", FILE_NAME = listReportKanban }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenaretePDFKanban(string pMANIFEST_NO)
        {
            List<mPickingListInquiry> listDataModel = new List<mPickingListInquiry>();
            mPickingListInquiry dataModel = new mPickingListInquiry();
            List<string> listReportKanban = new List<string>();


            string rExtract = string.Empty;
            string filename = "Data_source_of_Picking_List.trdx";
            string directory = "Report";
            string filesTmp = HttpContext.Request.MapPath("~/" + directory + "/" + filename);
            //String source = Server.MapPath("~/Report/Data_source_of_Picking_List.trdx");

            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;
            }
            //else if (model.Count() == 0)
            //{
            //    lError = 1;
            //    status = "Data";
            //    rExtract = model.Count().ToString();
            //}
            else if (info.Exists)
            {
                try { ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read); }
                catch { }
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if (lError == 1)
                return Json(new { success = false, messages = "Cannot find the template file", status = status }, JsonRequestBehavior.AllowGet);
            #endregion 

            var listKeyHeader = pMANIFEST_NO.Split(';');
            foreach (var lsKeyHeader in listKeyHeader)
            {
                var listKeyDetail = lsKeyHeader.Split('|');
                dataModel = new mPickingListInquiry(); 
                dataModel.PICKING_LIST_NO = listKeyDetail[0].ToString();
                dataModel.PART_NO = listKeyDetail[1].ToString();
                dataModel.KANBAN_ID = listKeyDetail[2].ToString() ;
                listDataModel.Add(dataModel);
            }
            string PICKING_LIST_NO = listDataModel.FirstOrDefault().PICKING_LIST_NO;
            string PART_NO = listDataModel.FirstOrDefault().PART_NO;
            string KANBAN_ID = listDataModel.FirstOrDefault().KANBAN_ID;
            string KANBAN_FINAL = "";
            int i=0;

            foreach (var item in listDataModel)
            {
                if (i == 0)
                    KANBAN_FINAL += "#";

                KANBAN_FINAL += item.KANBAN_ID;
            }

            string name = null;
            name = GeneratePickingListKanban_New(PICKING_LIST_NO, PART_NO, KANBAN_FINAL,"");
            if (name != null)
                    listReportKanban.Add(name); 

            return Json(new { success = "true", messages = "sukses", FILE_NAME = listReportKanban }, JsonRequestBehavior.AllowGet);
             
        }

        public string GenaretePDFPickingList_New(string pPICKING_LIST_NO)
        { 
            try
            {
                string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("ddMMyyyyHHmmssfff");
                mTelerik report; 
                String source = Server.MapPath("~/Report/Picking_List_And_Kanban.trdx");
                FileInfo fileInfo = new FileInfo(source);

                pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
                Directory.CreateDirectory(pdfFolderPath); 

                pdfFilename = "PICKING_LIST_" + date;
                if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                    System.IO.File.Delete(pdfFolderPath + pdfFilename);

                report = new mTelerik(source, pdfFilename);
                //report.AddParameters("PICKING_LIST", "P117000208");
                report.AddParameters("PICKING_LIST", pPICKING_LIST_NO);
                //string path = @"Data Source=10.165.8.80\MSSQLServer17;Initial Catalog=SPEX_DB;Persist Security Info=True;User ID=sa;Password=fid123!!";
                string path = ms.GetTelerikPath();
                report.SetConnectionString(path); 

                using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
                {
                    byte[] info = report.GeneratePDF();
                    fs.Write(info, 0, info.Length);
                } 
                return report.ResultName; 
            }
            catch (Exception ex)
            {
                return null; 
            }
            return null;
        }

        private string GeneratePickingListKanban_New(string pMANIFEST_NO, string pPART_NO, string pKANBAN_ID, string pTMAP_ITEM_NO)
        {
            try
            {
                string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("ddMMyyyyHHmmssfff");
                mTelerik report; 
                String source = Server.MapPath("~/Report/Data_source_of_Picking_List.trdx");
                FileInfo fileInfo = new FileInfo(source);

                pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
                Directory.CreateDirectory(pdfFolderPath);
                 
                pdfFilename = "SL_" + date;
                if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                    System.IO.File.Delete(pdfFolderPath + pdfFilename);

                report = new mTelerik(source, pdfFilename);
                report.AddParameters("MANIFEST_NO", pMANIFEST_NO);
                report.AddParameters("PART_NO", pPART_NO);
                report.AddParameters("KANBAN_ID", pKANBAN_ID);
                report.AddParameters("TMAP_ITEM_NO", pTMAP_ITEM_NO);

                //report.AddParameters("MANIFEST_NO", "P117000004");
                //report.AddParameters("KANBAN_ID", "P11700000400200001");

                //string path = @"Data Source=10.165.8.80\MSSQLServer17;Initial Catalog=SPEX_DB;Persist Security Info=True;User ID=sa;Password=fid123!!";
                string path = ms.GetTelerikPath();
                report.SetConnectionString(path);


                using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
                {
                    byte[] info = report.GeneratePDF();
                    fs.Write(info, 0, info.Length);
                }

                return report.ResultName; 
            }
            catch (Exception ex)
            {
                return null;
                 
            }
            return null;
        }

        public ActionResult GenaretePDFPickingList_2(string pMANIFEST_NO)
        {
            List<mPickingListInquiry> listDataModel = new List<mPickingListInquiry>();
            mPickingListInquiry dataModel = new mPickingListInquiry();
            var listKeyHeader = pMANIFEST_NO.Split(';');
            foreach (var lsKeyHeader in listKeyHeader)
            {
                var listKeyDetail = lsKeyHeader.Split('|');
                dataModel = new mPickingListInquiry(); 
                dataModel.PICKING_LIST_NO = listKeyDetail[0].ToString();
                listDataModel.Add(dataModel);
            }
            string pPICKING_LIST_NO;
            foreach (mPickingListInquiry dt in listDataModel)
            {
                pPICKING_LIST_NO = dt.PICKING_LIST_NO;
                try
                {
                    string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("ddMMyyyy");
                    mTelerik report; 
                    String source = Server.MapPath("~/Report/Picking_List_And_Kanban.trdx");
                    FileInfo fileInfo = new FileInfo(source);

                    pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
                    Directory.CreateDirectory(pdfFolderPath); 

                    pdfFilename = "PICKING_LIST_" + date;
                    if (System.IO.File.Exists(pdfFolderPath + pdfFilename))
                        System.IO.File.Delete(pdfFolderPath + pdfFilename);

                    report = new mTelerik(source, pdfFilename);
                    //report.AddParameters("PICKING_LIST", "P117000208");
                    report.AddParameters("PICKING_LIST", pPICKING_LIST_NO);
                    //string path = @"Data Source=10.165.8.80\MSSQLServer17;Initial Catalog=SPEX_DB;Persist Security Info=True;User ID=sa;Password=fid123!!";
                    string path = ms.GetTelerikPath();
                    report.SetConnectionString(path);


                    using (FileStream fs = System.IO.File.Create(pdfFolderPath + "/" + report.ResultName))
                    {
                        byte[] info = report.GeneratePDF();
                        fs.Write(info, 0, info.Length);
                    }
                    return Json(new { success = "true", messages = "sukses", FILE_NAME = report.ResultName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = "false", messages = ex.Message, FILE_NAME = "" }, JsonRequestBehavior.AllowGet);
                }
            }
                return null;
        }
        #endregion








        #region create pdf pickinglist itextsharp
        public string GenerateReport(string pPickingList)
        {

                //pPickingList = "P119000164";
                mPickingListReport msReport = new mPickingListReport();
                string result = string.Empty;
                List<mPickingListReport> model = new List<mPickingListReport>();
                model = msReport.getListPickingList(pPickingList);


                string pdfFilename, pdfFolderPath = "", date = DateTime.Now.ToString("ddMMyyyyHHmmssfff");
                pdfFolderPath = HttpContext.Request.MapPath("~/Content/ReportResult/");
                Directory.CreateDirectory(pdfFolderPath);
                pdfFilename = "PICKING_LIST_" + date +".pdf";
                


                if (model.Count() > 0) { 
            
            
                    try
                    {
                        iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.A4);
                        BaseFont arial = BaseFont.CreateFont("C:\\Windows\\Fonts\\arial.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                        iTextSharp.text.Font f_15_bold = new iTextSharp.text.Font(arial, 15, iTextSharp.text.Font.BOLD);
                        iTextSharp.text.Font f_8_normal = new iTextSharp.text.Font(arial, 8, iTextSharp.text.Font.NORMAL);
                        iTextSharp.text.Font f_12_normal_bold = new iTextSharp.text.Font(arial, 10, iTextSharp.text.Font.BOLD);
                        iTextSharp.text.Font f_12_normal_underline = new iTextSharp.text.Font(arial, 10, iTextSharp.text.Font.UNDERLINE);

                        Random rnd = new Random();
                        int name = rnd.Next(1, 1000);
                        MemoryStream mstream = new MemoryStream();


                        using (mstream)
                        {
                            PdfWriter writer = PdfWriter.GetInstance(doc, mstream);
                            doc.Open();


                            PdfContentByte content = writer.DirectContent;
                            Barcode128 bar128 = new Barcode128();
                            bar128.Code = pPickingList;
                            Image img128 = bar128.CreateImageWithBarcode(content, null, null);
                            img128.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                            img128.ScaleAbsolute(180f, 30f);



                            #region header pdf
                            PdfPTable tablePickingLeft = new PdfPTable(2);
                            tablePickingLeft.HorizontalAlignment = 0;
                            tablePickingLeft.TotalWidth = 140f;
                            tablePickingLeft.LockedWidth = true;
                            float[] widthsPickingLeft = new float[] { 70f, 70f };
                            tablePickingLeft.SetWidths(widthsPickingLeft);

                            PdfPCell cellHeaderLeft = new PdfPCell();
                            cellHeaderLeft = new PdfPCell(new Phrase("Picking Date", f_8_normal));
                            cellHeaderLeft.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellHeaderLeft.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeaderLeft.FixedHeight = 20f;
                            tablePickingLeft.AddCell(cellHeaderLeft);

                            cellHeaderLeft = new PdfPCell(new Phrase("Picking Time", f_8_normal));
                            cellHeaderLeft.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellHeaderLeft.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeaderLeft.FixedHeight = 20f;
                            tablePickingLeft.AddCell(cellHeaderLeft);



                            cellHeaderLeft = new PdfPCell(new Phrase(model[0].PICKING_DATE, f_12_normal_bold));
                            cellHeaderLeft.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellHeaderLeft.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeaderLeft.FixedHeight = 35f;
                            tablePickingLeft.AddCell(cellHeaderLeft);

                            cellHeaderLeft = new PdfPCell(new Phrase(model[0].PICKING_TIME, f_12_normal_bold));
                            cellHeaderLeft.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellHeaderLeft.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeaderLeft.FixedHeight = 35f;
                            tablePickingLeft.AddCell(cellHeaderLeft);



                            PdfPTable tablePickingRight = new PdfPTable(2);
                            tablePickingRight.HorizontalAlignment = 0;
                            tablePickingRight.TotalWidth = 140f;
                            tablePickingRight.LockedWidth = true;
                            float[] widthsPickingRight = new float[] { 70f, 70f };
                            tablePickingRight.SetWidths(widthsPickingRight);

                            PdfPCell cellHeaderRight = new PdfPCell();
                            cellHeaderRight = new PdfPCell(new Phrase("Dock Code", f_8_normal));
                            cellHeaderRight.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellHeaderRight.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeaderRight.FixedHeight = 20f;
                            tablePickingRight.AddCell(cellHeaderRight);

                            cellHeaderRight = new PdfPCell(new Phrase("Zone", f_8_normal));
                            cellHeaderRight.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellHeaderRight.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeaderRight.FixedHeight = 20f;
                            tablePickingRight.AddCell(cellHeaderRight);



                            cellHeaderRight = new PdfPCell(new Phrase(model[0].DOCK_CD, f_12_normal_bold));
                            cellHeaderRight.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellHeaderRight.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeaderRight.FixedHeight = 35f;
                            tablePickingRight.AddCell(cellHeaderRight);

                            cellHeaderRight = new PdfPCell(new Phrase(model[0].ZONE_CD, f_12_normal_bold));
                            cellHeaderRight.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellHeaderRight.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeaderRight.FixedHeight = 35f;
                            tablePickingRight.AddCell(cellHeaderRight);

                            PdfPTable tableHeader = new PdfPTable(3);
                            tableHeader.HorizontalAlignment = 0;
                            tableHeader.TotalWidth = 500f;
                            tableHeader.LockedWidth = true;
                            float[] widthsHeader = new float[] { 140f, 220f, 140f };
                            tableHeader.SetWidths(widthsHeader);
                        


                            PdfPCell cellHeader = new PdfPCell();
                            cellHeader = new PdfPCell(new Phrase("PICKING LIST", f_15_bold));
                            cellHeader.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellHeader.VerticalAlignment = Element.ALIGN_BOTTOM;
                            cellHeader.Colspan = 3;
                            cellHeader.Border = PdfPCell.NO_BORDER;
                            tableHeader.AddCell(cellHeader);


                            cellHeader = new PdfPCell(tablePickingLeft);
                            cellHeader.HorizontalAlignment = Element.ALIGN_RIGHT;
                            cellHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeader.FixedHeight = 55f;
                            cellHeader.Border = PdfPCell.NO_BORDER;
                            tableHeader.AddCell(cellHeader);

                            cellHeader = new PdfPCell(img128);
                            cellHeader.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeader.Border = PdfPCell.NO_BORDER;
                            cellHeader.FixedHeight = 55f;
                            tableHeader.AddCell(cellHeader);

                            cellHeader = new PdfPCell(tablePickingRight);
                            cellHeader.HorizontalAlignment = Element.ALIGN_LEFT;
                            cellHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeader.FixedHeight = 55f;
                            cellHeader.Border = PdfPCell.NO_BORDER;
                            tableHeader.AddCell(cellHeader);

                            cellHeader = new PdfPCell(new Phrase("Transport : " + model[0].TRANSP_DESC, f_12_normal_bold));
                            cellHeader.HorizontalAlignment = Element.ALIGN_LEFT;
                            cellHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellHeader.FixedHeight = 20f;
                            //cellHeader.Colspan = 3;
                            cellHeader.Border = PdfPCell.NO_BORDER;
                            tableHeader.AddCell(cellHeader); 
                                                      
                            cellHeader = new PdfPCell(new Phrase(Chunk.NEWLINE));
                            cellHeader.Border = PdfPCell.NO_BORDER;
                            cellHeader.Colspan = 3;
                            tableHeader.AddCell(cellHeader);


                            //doc.Add(tableHeader);

                       

                            PdfPTable table = new PdfPTable(9);
                            table.HeaderRows = 3;
                            table.HorizontalAlignment = 0;
                            table.TotalWidth = 500f;
                            table.LockedWidth = true;
                            float[] widths = new float[] { 20f, 86f, 150f, 85f, 35f, 35f, 35f, 27f, 27f };
                            table.SetWidths(widths);

                        
                            PdfPCell cell = new PdfPCell();
                            int i = 1;

                            cell = new PdfPCell(tableHeader);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Border = PdfPCell.NO_BORDER;
                            cell.Colspan = 9;
                            table.AddCell(cell);


                            cell = new PdfPCell(new Phrase("No", f_8_normal));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Rowspan = 2;
                            table.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Part No", f_8_normal));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Rowspan = 2;
                            table.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Part Name", f_8_normal));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Rowspan = 2;
                            table.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Rack Address", f_8_normal));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Rowspan = 2;
                            table.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Picking Qty", f_8_normal));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Rowspan = 2;
                            table.AddCell(cell);


                            cell = new PdfPCell(new Phrase("Pcs / Kanban", f_8_normal));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Rowspan = 2;
                            table.AddCell(cell);


                            cell = new PdfPCell(new Phrase("Total Kanban", f_8_normal));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Rowspan = 2;
                            table.AddCell(cell);


                            cell = new PdfPCell(new Phrase("Confirmation", f_8_normal));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Colspan = 2;
                            table.AddCell(cell);

                            cell = new PdfPCell(new Phrase("OK", f_8_normal));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            table.AddCell(cell);

                            cell = new PdfPCell(new Phrase("NG", f_8_normal));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            table.AddCell(cell);
                            #endregion

                            foreach (var row in model)
                            {
                                cell = new PdfPCell(new Phrase("" + i, f_8_normal));
                                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 22f;
                                table.AddCell(cell);

                                cell = new PdfPCell(new Phrase(row.PART_NO, f_8_normal));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 22f;
                                table.AddCell(cell);

                                cell = new PdfPCell(new Phrase(row.PART_NAME, f_8_normal));
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 22f;
                                table.AddCell(cell);

                                cell = new PdfPCell(new Phrase(row.RACK_ADDRESS_CD, f_8_normal));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 22f;
                                table.AddCell(cell);

                                cell = new PdfPCell(new Phrase(row.ORDER_QTY, f_8_normal));
                                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 22f;
                                table.AddCell(cell);


                                cell = new PdfPCell(new Phrase(row.QTY_PER_CONTAINER, f_8_normal));
                                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 22f;
                                table.AddCell(cell);


                                cell = new PdfPCell(new Phrase(row.TOTAL_KANBAN, f_8_normal));
                                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 22f;
                                table.AddCell(cell);

                                table.AddCell("");
                                table.AddCell("");
                                i++;
                            }
                            doc.Add(table);


                            doc.Close();
                            writer.Close();

                            byte[] bytes = mstream.ToArray();

                            //pdfFolderPath + "/" + report.ResultName

                            System.IO.File.WriteAllBytes(pdfFolderPath + "/" + pdfFilename , mstream.GetBuffer());

                            
                            mstream.Close();
                            //Response.Clear();
                            //Response.ContentType = "application/pdf";
                            //Response.AddHeader("Content-Disposition", "attachment; filename=" + pdfFilename + ".pdf");
                            //Response.ContentType = "application/pdf";
                            //Response.Buffer = true;
                            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            //Response.BinaryWrite(bytes);
                            //Response.Flush();
                            //Response.End();

                        }
                        return pdfFilename;
                    }
                       
                    catch (Exception x)
                    {
                        return null;
                        //result = "System Error: " + x.Message.ToString();
                    }
            }

           return null;
        }        
        #endregion
    }
}
