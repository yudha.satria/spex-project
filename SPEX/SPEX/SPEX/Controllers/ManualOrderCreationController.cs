﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using NPOI.XSSF.UserModel;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using System.Web.UI;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class ManualOrderCreationController : PageController
    {
        mManualOrderCreation ms = new mManualOrderCreation();
        MessagesString msgError = new MessagesString();

        //Upload
        public string moduleID = "SPX24";
        public string functionID = "SPX240200";
        public const string UploadDirectory = "Content\\FileUploadResult";
        public const string TemplateFName = "UPLOAD_MANUAL_ORDER_MASTER";

        
        public ManualOrderCreationController()
        {
            Settings.Title = "Manual Order Creation";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        { 
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR(); 
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR(); 


            /*No data found*/
            ViewBag.MSPX00001ERR = msgError.getMsgText("MSPXS2006ERR");
            /*Cannot find template file {0} on folder {1}.*/
            ViewBag.MSPXS2034ERR = msgError.getMsgText("MSPXS2034ERR");
            /*Error writing file = {0}. Reason = {1}.*/
            ViewBag.MSPXS2035ERR = msgError.getMsgText("MSPXS2035ERR");
            /*Are you sure you want to download this data?*/
            ViewBag.MSPXS2021INF = msgError.getMsgText("MSPXS2021INF"); 
            /*{0} should not be empty.*/
            ViewBag.MSPXS2005ERR = msgError.getMsgText("MSPXS2005ERR");
            /*A single record must be selected.*/
            ViewBag.MSPXS2017ERR = msgError.getMsgText("MSPXS2017ERR");
            /*Duplication found for {0}.*/
            ViewBag.MSPXS2011ERR = msgError.getMsgText("MSPXS2011ERR");
            /*{0} is not exists in  {1}*/
            ViewBag.MSPXS2010ERR = msgError.getMsgText("MSPXS2010ERR"); 
            /*Invalid data type of {0}. Should be {1}.*/
            ViewBag.MSPXS2012ERR = msgError.getMsgText("MSPXS2012ERR");
            /*Are you sure you want to confirm the operation?*/
            ViewBag.MSPXS2018INF = msgError.getMsgText("MSPXS2018INF");
            /*Are you sure you want to abort the operation?*/
            ViewBag.MSPXS2019INF = msgError.getMsgText("MSPXS2019INF");
            /*Are you sure you want to delete the record?*/
            ViewBag.MSPXS2020INF = msgError.getMsgText("MSPXS2020INF");
            /*{0} must be selected.*/
            ViewBag.MSPXS2007ERR = msgError.getMsgText("MSPXS2007ERR");
            /*{0} not match with {1}.*/
            ViewBag.MSPXS2032ERR = msgError.getMsgText("MSPXS2032ERR");
            /*Please select the data that you want to {0}.*/
            ViewBag.MSPX00002INF = msgError.getMsgText("MSPX00002INF");
            /*No order data to {0}*/
            ViewBag.MSPX00008INB = msgError.getMsgText("MSPX00008INB"); 

            getpE_UserId = (User)ViewData["User"];
            ComboManualOrderType();
        } 

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (string.IsNullOrEmpty(dt))
            {
                result = "";
            }
            else if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
            }
            return result; 
        } 

        [HttpPost, ValidateInput(false)] 
        public ActionResult ManualOrderCreationCallBack(  )
        { 
            string user_id = getpE_UserId.Username;
            List<mManualOrderCreation> model = new List<mManualOrderCreation>();
            model = ms.getListManualOrderCreation(user_id); 
            return PartialView("ManualOrderCreationGrid", model);
        }


        #region Function Get Part Name
        public ActionResult GetPartName(string p_PART_NO)
        {
            if (getpE_UserId != null)
            {
                string[] r = null;
                mMaintenanceStockMaster resultData = ms.GetPartName(p_PART_NO);
                if (resultData == null)
                {
                    return Json(new { success = false, result = resultData }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, result = resultData }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
            }

            return null;
        }
        #endregion
         

        #region Combobox

        public void ComboSupplier()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--Select--";
            model = ms.GetComboBoxSupplier();
            model.Insert(0, item);
            ViewData["SUPPLIER"] = model;
        }

        public ActionResult GetComboSubSupplier(string I_SUPPLIER)
        {
            List<mSystemMaster> ListSubSupplier = null;
            if (I_SUPPLIER != "")
            {
                string[] ar = null;
                ar = I_SUPPLIER.Split('-');
                string SupCD = ar[0];
                string PlanCD = ar[1];


                mSystemMaster itemNotMandatory = new mSystemMaster();
                itemNotMandatory.SYSTEM_CD = "";
                itemNotMandatory.SYSTEM_VALUE = "--SELECT--";
                //string combo = Request.Params["SelectedCategoryId"];
                ListSubSupplier = ms.GetComboBoxSubSupplier(SupCD, PlanCD);
                ListSubSupplier.Insert(0, itemNotMandatory);
                ViewData["SUB_SUPPLIER"] = ListSubSupplier;
            } 
            return PartialView("_PartialComboboxSubSupplier", ListSubSupplier); 
        }

        public ActionResult GetComboPartNo(string I_SUPPLIER, string I_SUB_SUPPLIER)
        {
            List<mSystemMaster> ListPartNo = null;
            if (I_SUPPLIER != "" || I_SUB_SUPPLIER != "")
            {
                string[] ar = null;
                ar = I_SUPPLIER.Split('-');
                string SupCD = ar[0];
                string PlanCD = ar[1];
                string SubSupCD = string.Empty;
                string SubPlanCD = string.Empty;
                if (I_SUB_SUPPLIER != "")
                {
                    string[] arr = null;
                    arr = I_SUB_SUPPLIER.Split('-');
                     SubSupCD = arr[0];
                     SubPlanCD = arr[1];

                }

                mSystemMaster itemNotMandatory = new mSystemMaster();
                itemNotMandatory.SYSTEM_CD = "";
                itemNotMandatory.SYSTEM_VALUE = "--SELECT--";
                //string combo = Request.Params["SelectedCategoryId"];
                ListPartNo = ms.GetComboBoxPartNo(SupCD, PlanCD, SubSupCD, SubPlanCD);
                ListPartNo.Insert(0, itemNotMandatory);
                ViewData["PART_NO"] = ListPartNo;
            }
            return PartialView("_PartialComboboxPartNo", ListPartNo);
        }


        public void ComboDock()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            //item.SYSTEM_CD = "";
            //item.SYSTEM_VALUE = "--Select--";
            model = ms.GetComboBoxDock();
            model.Insert(0, item);
            ViewData["DOCK_CD"] = model;
        }


        public void ComboManualOrderType()
        {
            mSystemMaster item = new mSystemMaster();
            List<mSystemMaster> model = new List<mSystemMaster>();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--Select--";
            model = ms.GetComboBoxMOType();
            model.Insert(0, item);

            ViewData["MANUAL_ORDER_TYPE"] = model;
        }

        #endregion

        #region Pop UP


        public ActionResult _PopUpAdd( )
        {  
            ComboSupplier();
            //ComboSubSupplier();
            ComboDock();
            return PartialView("DetailAdd");
        }

        #endregion

        #region Function Add
        public ActionResult SaveNewManualOrder(string pMANUAL_ORDER_TYPE, string pSUPPLIER, string pSUPPLIER_PLANT, string pSUB_SUPPLIER, 
                               string pSUB_SUPPLIER_PLANT, string pDOCK, string pPART_NO, decimal pPIECES_KANBAN, decimal pKANBAN_QTY, decimal pPIECES_QTY)
        {
            if (getpE_UserId != null)
            {
                string pCREATED_BY = getpE_UserId.Username;
                string[] r = null;
                string resultMessage = ms.SaveData(pMANUAL_ORDER_TYPE, pSUPPLIER, pSUPPLIER_PLANT, pSUB_SUPPLIER,  pSUB_SUPPLIER_PLANT, pDOCK, 
                                                   pPART_NO, pPIECES_KANBAN, pKANBAN_QTY, pPIECES_QTY, pCREATED_BY);
                r = resultMessage.Split('|');
                if (r[0] != "I")
                {
                    return Json(new { success = false, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { success = false, messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        #endregion

        #region Function Delete
        public ActionResult DeleteManualOrder(string MANUAL_ORDER_ID)
        { 
            if (getpE_UserId != null)
            {
                string[] r = null;
                string resultMessage = ms.DeleteDataManualOrder(MANUAL_ORDER_ID);
                r = resultMessage.Split('|');
                if (r[0] != "I")
                {
                    return Json(new { success = false, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { success = false, messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        #endregion


        #region Function Cancel
        public ActionResult CancelManualOrder()
        {
            string pCREATED_BY = getpE_UserId.Username;
            if (getpE_UserId != null)
            {
                string[] r = null;
                string resultMessage = ms.CancelDataManualOrder(pCREATED_BY);
                r = resultMessage.Split('|');
                if (r[0] != "I")
                {
                    return Json(new { success = false, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, messages = r[1] }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { success = false, messages = "session expired, please re login" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        #endregion
         


        #region Function Upload
        public ActionResult IsCheckLock()
        {
            Lock L = new Lock();
            int Lock = L.is_lock(functionID);

            if (Lock == 0)
            {
                return Json(new { success = "true", messages = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string msg = string.Empty;
                msg = msgError.getMsgText("MSPX00088ERR");
                return Json(new { success = "false", messages = msg }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadButton", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            //public const string TemplateFName = "TemplatePackingPartMaster";// ???
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                //AllowedFileExtensions = new string[] { ".xlsx" },
                AllowedFileExtensions = new string[] { ".csv" },
                MaxFileSize = 20971520
            };
        }

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            mManualOrderCreation PM = new mManualOrderCreation();

            string[] r = null;
            string rExtract = string.Empty;
            string tb_t_name = "spex.TB_T_MANUAL_ORDER";

            #region createlog
            Guid PROCESS_ID = Guid.NewGuid();
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            string MESSAGE_DETAIL = "";
            long pid = 0;
            //string subFuncName = "EXcelToSQL";
            string isError = "N";
            string processName = "UPLOAD MANUAL ORDER CREATION MASTER";
            string resultMessage = "";

            int columns;
            int rows;

            #endregion

            // MESSAGE_DETAIL = "Starting proces Manual Order Creation Master";
            Lock L = new Lock();
            string functionIDUpload = "SPX240300";
            int IsLock = L.is_lock(functionIDUpload);
            if (IsLock > 0)
            {
                resultMessage = "E|" + msgError.getMsgText("MSPXS2036ERR");
            }

            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = PM.DeleteDataTBT();
                if (rExtract == "SUCCES")
                {
                    string user_id = getpE_UserId.Username;
                    Log lg = new Log();
                    string msg = "MSPXS2024INF|INF|" + new MessagesString().getMsgText("MSPXS2024INF").Replace("{0}", processName);
                    pid = lg.createLog(msg, user_id, "Manual Order Creation Batch", pid, moduleID, functionIDUpload);
                    L.CREATED_BY = user_id;
                    L.CREATED_DT = DateTime.Now;
                    L.PROCESS_ID = pid;
                    L.FUNCTION_ID = functionIDUpload;
                    L.LOCK_REF = functionIDUpload;
                    L.LockFunction(L);
                    string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    if (rExtract == string.Empty || rExtract == "SUCCESS")
                    {
                        rExtract = PM.BackgroundProcessSave(functionIDUpload, moduleID, user_id, pid);
                        if (rExtract == "SUCCES")
                            resultMessage = "I|" + msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2] + "|" + pid.ToString();
                    }
                    else
                    {
                        resultMessage = "E|" + rExtract.Split('|')[1]; 
                    }
                    L.UnlockFunction(functionIDUpload);

                }
                else
                {
                    rExtract = "E|" + rExtract;
                }
            }
            else
            {
                rExtract = "E|File is not .csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = resultMessage; 
        }

        #endregion


        #region Function Submit 
 
        public ActionResult SubmitProcess()
        {
            mManualOrderCreation PM = new mManualOrderCreation();

            string[] r = null;
            string rExtract = string.Empty;

            #region createlog
            Guid PROCESS_ID = Guid.NewGuid();
            string MESSAGE_DETAIL = "";
            long pid = 0;
            var success = true;
            string isError = "N";
            string processName = "MANUAL ORDER SUBMIT PROCESS";
            string resultMessage = "";

            #endregion

            // MESSAGE_DETAIL = "Starting proces MANUAL ORDER SUBMIT PROCESS";
            Lock L = new Lock();
            string functionIDSubmit = "SPX240200";
            int IsLock = L.is_lock(functionIDSubmit);
            if (IsLock > 0)
            {
                resultMessage = "E|" + msgError.getMsgText("MSPXS2036ERR");
            }

            //checking file 
            else if (IsLock == 0)
            {
                string user_id = getpE_UserId.Username;
                Log lg = new Log();
                string msg = "MSPXS2024INF|INF|" + new MessagesString().getMsgText("MSPXS2024INF").Replace("{0}", processName);
                pid = lg.createLog(msg, user_id, "Manual Order Submit Batch", pid, moduleID, functionIDSubmit);
                L.CREATED_BY = user_id;
                L.CREATED_DT = DateTime.Now;
                L.PROCESS_ID = pid;
                L.FUNCTION_ID = functionIDSubmit;
                L.LOCK_REF = functionIDSubmit;
                L.LockFunction(L);

                if (rExtract == string.Empty || rExtract == "SUCCESS")
                {
                    rExtract = PM.BackgroundSubmitProcess(functionIDSubmit, moduleID, user_id, pid);
                    if (rExtract == "SUCCES")
                        resultMessage = "I|" + msgError.getMSPX00009INB("<a href='#' onclick='MessageShow(" + pid + "); return false;'>" + pid + "</a>").Split('|')[2] + "|" + pid.ToString();
                }
                else
                {
                    resultMessage = "E|" + rExtract.Split('|')[1];
                }
                L.UnlockFunction(functionIDSubmit);

            }
            return Json(new { success = success, messages = resultMessage }, JsonRequestBehavior.AllowGet);

        }

        #endregion

    }
}