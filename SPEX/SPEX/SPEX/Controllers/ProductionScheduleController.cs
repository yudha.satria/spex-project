﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;
using Toyota.Common.Web.Platform.Starter.Models;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using NPOI.POIFS.FileSystem;
using NPOI.HPSF;
using System.IO;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxGridView;
using System.Web.UI.WebControls;
using System.Web.UI;



namespace Toyota.Common.Web.Platform.Starter.Controllers
{
    public class ProductionScheduleController : PageController
    {
        mProductionSchedule ProductionSchedule = new mProductionSchedule();

        public ProductionScheduleController()
        {
           Settings.Title = "Production Schedule";
        }
        protected override void Startup()
        {
            ComboBoxPartial_Plant();
            ComboBoxPartial_line();

        }
        public string getpE_ProdMonth
        {
            get
            {
                if (Session["getpE_ProdMonth"] != null)
                {
                    return Session["getpE_ProdMonth"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_ProdMonth"] = value;
            }
        }
        public string getpE_LineCode
        {
            get
            {
                if (Session["getpE_LineCode"] != null)
                {
                    return Session["getpE_LineCode"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_LineCode"] = value;
            }
        }
        public string getpE_Day1
        {
            get
            {
                if (Session["getpE_Day1"] != null)
                {
                    return Session["getpE_Day1"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day1"] = value;
            }
        }
        public string getpE_Day2
        {
            get
            {
                if (Session["getpE_Day2"] != null)
                {
                    return Session["getpE_Day2"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day2"] = value;
            }
        }
        public string getpE_Day3
        {
            get
            {
                if (Session["getpE_Day3"] != null)
                {
                    return Session["getpE_Day3"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day3"] = value;
            }
        }
        public string getpE_Day4
        {
            get
            {
                if (Session["getpE_Day4"] != null)
                {
                    return Session["getpE_Day4"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day4"] = value;
            }
        }
        public string getpE_Day5
        {
            get
            {
                if (Session["getpE_Day5"] != null)
                {
                    return Session["getpE_Day5"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day5"] = value;
            }
        }
        public string getpE_Day6
        {
            get
            {
                if (Session["getpE_Day6"] != null)
                {
                    return Session["getpE_Day6"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day6"] = value;
            }
        }
        public string getpE_Day7
        {
            get
            {
                if (Session["getpE_Day7"] != null)
                {
                    return Session["getpE_Day7"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day7"] = value;
            }
        }
        public string getpE_Day8
        {
            get
            {
                if (Session["getpE_Day8"] != null)
                {
                    return Session["getpE_Day8"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day8"] = value;
            }
        }
        public string getpE_Day9
        {
            get
            {
                if (Session["getpE_Day9"] != null)
                {
                    return Session["getpE_Day9"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day9"] = value;
            }
        }
        public string getpE_Day10
        {
            get
            {
                if (Session["getpE_Day10"] != null)
                {
                    return Session["getpE_Day10"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day10"] = value;
            }
        }
        public string getpE_Day11
        {
            get
            {
                if (Session["getpE_Day11"] != null)
                {
                    return Session["getpE_Day11"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day11"] = value;
            }
        }
        public string getpE_Day12
        {
            get
            {
                if (Session["getpE_Day12"] != null)
                {
                    return Session["getpE_Day12"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day12"] = value;
            }
        }
        public string getpE_Day13
        {
            get
            {
                if (Session["getpE_Day13"] != null)
                {
                    return Session["getpE_Day13"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day13"] = value;
            }
        }
        public string getpE_Day14
        {
            get
            {
                if (Session["getpE_Day14"] != null)
                {
                    return Session["getpE_Day14"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day14"] = value;
            }
        }
        public string getpE_Day15
        {
            get
            {
                if (Session["getpE_Day15"] != null)
                {
                    return Session["getpE_Day15"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day15"] = value;
            }
        }
        public string getpE_Day16
        {
            get
            {
                if (Session["getpE_Day16"] != null)
                {
                    return Session["getpE_Day16"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day16"] = value;
            }
        }
        public string getpE_Day17
        {
            get
            {
                if (Session["getpE_Day17"] != null)
                {
                    return Session["getpE_Day17"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day17"] = value;
            }
        }
        public string getpE_Day18
        {
            get
            {
                if (Session["getpE_Day18"] != null)
                {
                    return Session["getpE_Day18"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day18"] = value;
            }
        }
        public string getpE_Day19
        {
            get
            {
                if (Session["getpE_Day19"] != null)
                {
                    return Session["getpE_Day19"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day19"] = value;
            }
        }
        public string getpE_Day20
        {
            get
            {
                if (Session["getpE_Day20"] != null)
                {
                    return Session["getpE_Day20"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day20"] = value;
            }
        }
        public string getpE_Day21
        {
            get
            {
                if (Session["getpE_Day21"] != null)
                {
                    return Session["getpE_Day21"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day21"] = value;
            }
        }
        public string getpE_Day22
        {
            get
            {
                if (Session["getpE_Day22"] != null)
                {
                    return Session["getpE_Day22"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day22"] = value;
            }
        }
        public string getpE_Day23
        {
            get
            {
                if (Session["getpE_Day23"] != null)
                {
                    return Session["getpE_Day23"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day23"] = value;
            }
        }
        public string getpE_Day24
        {
            get
            {
                if (Session["getpE_Day24"] != null)
                {
                    return Session["getpE_Day24"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day24"] = value;
            }
        }
        public string getpE_Day25
        {
            get
            {
                if (Session["getpE_Day25"] != null)
                {
                    return Session["getpE_Day25"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day25"] = value;
            }
        }
        public string getpE_Day26
        {
            get
            {
                if (Session["getpE_Day26"] != null)
                {
                    return Session["getpE_Day26"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day26"] = value;
            }
        }
        public string getpE_Day27
        {
            get
            {
                if (Session["getpE_Day27"] != null)
                {
                    return Session["getpE_Day27"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day27"] = value;
            }
        }
        public string getpE_Day28
        {
            get
            {
                if (Session["getpE_Day28"] != null)
                {
                    return Session["getpE_Day28"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day28"] = value;
            }
        }
        public string getpE_Day29
        {
            get
            {
                if (Session["getpE_Day29"] != null)
                {
                    return Session["getpE_Day29"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day29"] = value;
            }
        }
        public string getpE_Day30
        {
            get
            {
                if (Session["getpE_Day30"] != null)
                {
                    return Session["getpE_Day30"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day30"] = value;
            }
        }
        public string getpE_Day31
        {
            get
            {
                if (Session["getpE_Day31"] != null)
                {
                    return Session["getpE_Day31"].ToString();
                }
                else return null;
            }
            set
            {
                Session["getpE_Day31"] = value;
            }
        }
        public string get_Message_Upload
        {
            get
            {
                if (Session["get_Message_Upload"] != null)
                {
                    return Session["get_Message_Upload"].ToString();
                }
                else return null;
            }
            set
            {
                Session["get_Message_Upload"] = value;
            }
        }


        public ActionResult GridViewProductionSchedule_cari(string p_PROD_MONTH, string p_PLANT_CD, string p_LINE_CD)
        {
            List<mProductionSchedule> model = new List<mProductionSchedule>();

            {
                model = ProductionSchedule.getListProductionSchedule_cari
                (p_PROD_MONTH, p_PLANT_CD, p_LINE_CD);
            }

            //foreach (var list in model)
            //{
            //    ViewData["MessageSearch"] = list.MSG_TEXT;
            //}
            return PartialView("GridViewProductionSchedule_cari", model);
        }
        public void ComboBoxPartial_Plant()
        {
            List<mProductionSchedule> model = new List<mProductionSchedule>();

            model = ProductionSchedule.getListComboboxPlant();
            ViewData["PLANT_CD"] = model;
        }
        public void ComboBoxPartial_line()
        {
            List<mProductionSchedule> model = new List<mProductionSchedule>();

            model = ProductionSchedule.getListComboboxLine();
            ViewData["LINE_CD"] = model;
        }
        //panggil popup
        public ActionResult _AddPopUp()
        {
            ComboBoxPartial_line();
            return PartialView("ProductionSchedule_modal");
            
        }
        
        public string SaveDataAdd(string p_PROD_MONTH, string p_LINE_CD, string p_TIME_D_1, string p_TIME_D_2, string p_TIME_D_3,string p_TIME_D_4, string p_TIME_D_5, string p_TIME_D_6, string p_TIME_D_7, string p_TIME_D_8,string p_TIME_D_9, string p_TIME_D_10, string p_TIME_D_11, string p_TIME_D_12, string p_TIME_D_13,string p_TIME_D_14, string p_TIME_D_15, string p_TIME_D_16, string p_TIME_D_17, string p_TIME_D_18,string p_TIME_D_19, string p_TIME_D_20, string p_TIME_D_21, string p_TIME_D_22, string p_TIME_D_23,string p_TIME_D_24, string p_TIME_D_25, string p_TIME_D_26, string p_TIME_D_27, string p_TIME_D_28,string p_TIME_D_29, string p_TIME_D_30, string p_TIME_D_31)
        {
            return ProductionSchedule.SaveDataAdd(p_PROD_MONTH, p_LINE_CD, p_TIME_D_1, p_TIME_D_2, p_TIME_D_3,
                p_TIME_D_4, p_TIME_D_5, p_TIME_D_6, p_TIME_D_7, p_TIME_D_8, p_TIME_D_9, p_TIME_D_10, p_TIME_D_11, p_TIME_D_12,
                p_TIME_D_13, p_TIME_D_14, p_TIME_D_15, p_TIME_D_16, p_TIME_D_17, p_TIME_D_18, p_TIME_D_19, p_TIME_D_20,
                p_TIME_D_21, p_TIME_D_22, p_TIME_D_23, p_TIME_D_24, p_TIME_D_25, p_TIME_D_26, p_TIME_D_27, p_TIME_D_28,
                p_TIME_D_29, p_TIME_D_30, p_TIME_D_31);
          
        }
        public string SPN211DeleteData(string pd_List)
        {
            string resultMessage = ProductionSchedule.SPN211DeleteData(pd_List);

            return resultMessage;
        }
        //untuk edit prepare
        public void SPN211EditData(string pE_ProdMonth, string pE_LineCode, string pE_Day1, string pE_Day2, string pE_Day3, string pE_Day4,string pE_Day5, string pE_Day6, string pE_Day7, string pE_Day8, string pE_Day9, string pE_Day10, string pE_Day11,string pE_Day12, string pE_Day13, string pE_Day14, string pE_Day15, string pE_Day16, string pE_Day17, string pE_Day18,string pE_Day19, string pE_Day20, string pE_Day21, string pE_Day22, string pE_Day23, string pE_Day24, string pE_Day25,string pE_Day26, string pE_Day27, string pE_Day28, string pE_Day29, string pE_Day30, string pE_Day31)
        {
            //string dtPLatestOrder = "01-01-1753";
            //string intTaktTime = "0";
            //string intday1 = "0";

            ////if (!String.IsNullOrEmpty(pE_LatestOrder))
            ////{
            ////    string strValidFrom = pE_LatestOrder.Substring(5, 2) + "-" + pE_LatestOrder.Substring(8, 2) + "-" + pE_LatestOrder.Substring(0, 4);
            ////    dtPLatestOrder = Convert.ToDateTime(strValidFrom).ToString();
            ////}
            ////if (!String.IsNullOrEmpty(pE_TaktTime))
            ////{
            ////    intTaktTime = Convert.ToInt32(pE_TaktTime).ToString();
            ////}
            //if (!String.IsNullOrEmpty(pE_Day1))
            //{
            //    intday1 = Convert.ToDecimal(pE_Day1).ToString();
            //}

            getpE_ProdMonth = pE_ProdMonth;
            getpE_LineCode = pE_LineCode;
            getpE_Day1 = pE_Day1;
            getpE_Day2 = pE_Day2;
            getpE_Day3 = pE_Day3;
            getpE_Day4 = pE_Day4;
            getpE_Day5 = pE_Day5;
            getpE_Day6 = pE_Day6;
            getpE_Day7 = pE_Day7;
            getpE_Day8 = pE_Day8;
            getpE_Day9 = pE_Day9;
            getpE_Day10 = pE_Day10;
            getpE_Day11 = pE_Day11;
            getpE_Day12 = pE_Day12;
            getpE_Day13 = pE_Day13;
            getpE_Day14 = pE_Day14;
            getpE_Day15 = pE_Day15;
            getpE_Day16 = pE_Day16;
            getpE_Day17 = pE_Day17;
            getpE_Day18 = pE_Day18;
            getpE_Day19 = pE_Day19;
            getpE_Day20 = pE_Day20;
            getpE_Day21 = pE_Day21;
            getpE_Day22 = pE_Day22;
            getpE_Day23 = pE_Day23;
            getpE_Day24 = pE_Day24;
            getpE_Day25 = pE_Day25;
            getpE_Day26 = pE_Day26;
            getpE_Day27 = pE_Day27;
            getpE_Day28 = pE_Day28;
            getpE_Day29 = pE_Day29;
            getpE_Day30 = pE_Day30;
            getpE_Day31 = pE_Day31;
        }
        //pake sp bisa untuk select data pop up edit di view action
        public ActionResult SPN211EditPopUpUpdate()
        {
            ViewData["SetDateFrom2"] = getpE_ProdMonth;
            ViewData["SetLinecode2"] = getpE_LineCode;
            ViewData["Setday1"] = getpE_Day1;
            ViewData["Setday2"] = getpE_Day2;
            ViewData["Setday3"] = getpE_Day3;
            ViewData["Setday4"] = getpE_Day4;
            ViewData["Setday5"] = getpE_Day5;
            ViewData["Setday6"] = getpE_Day6;
            ViewData["Setday7"] = getpE_Day7;
            ViewData["Setday8"] = getpE_Day8;
            ViewData["Setday9"] = getpE_Day9;
            ViewData["Setday10"] = getpE_Day10;
            ViewData["Setday11"] = getpE_Day11;
            ViewData["Setday12"] = getpE_Day12;
            ViewData["Setday13"] = getpE_Day13;
            ViewData["Setday14"] = getpE_Day14;
            ViewData["Setday15"] = getpE_Day15;
            ViewData["Setday16"] = getpE_Day16;
            ViewData["Setday17"] = getpE_Day17;
            ViewData["Setday18"] = getpE_Day18;
            ViewData["Setday19"] = getpE_Day19;
            ViewData["Setday20"] = getpE_Day20;
            ViewData["Setday21"] = getpE_Day21;
            ViewData["Setday22"] = getpE_Day22;
            ViewData["Setday23"] = getpE_Day23;
            ViewData["Setday24"] = getpE_Day24;
            ViewData["Setday25"] = getpE_Day25;
            ViewData["Setday26"] = getpE_Day26;
            ViewData["Setday27"] = getpE_Day27;
            ViewData["Setday28"] = getpE_Day28;
            ViewData["Setday29"] = getpE_Day29;
            ViewData["Setday30"] = getpE_Day30;
            ViewData["Setday31"] = getpE_Day31;


            return PartialView("ProductionSchedule_Editmodal");
        }
        public string SPN211SaveUpdateData(string p_PROD_MONTH, string p_LINE_CD, string p_TIME_D_1, string p_TIME_D_2, string p_TIME_D_3, string p_TIME_D_4, string p_TIME_D_5, string p_TIME_D_6, string p_TIME_D_7, string p_TIME_D_8, string p_TIME_D_9, string p_TIME_D_10, string p_TIME_D_11, string p_TIME_D_12, string p_TIME_D_13, string p_TIME_D_14, string p_TIME_D_15, string p_TIME_D_16, string p_TIME_D_17, string p_TIME_D_18, string p_TIME_D_19, string p_TIME_D_20, string p_TIME_D_21, string p_TIME_D_22, string p_TIME_D_23, string p_TIME_D_24, string p_TIME_D_25, string p_TIME_D_26, string p_TIME_D_27, string p_TIME_D_28, string p_TIME_D_29, string p_TIME_D_30, string p_TIME_D_31)
        {
            return ProductionSchedule.SPN211SaveUpdateData(p_PROD_MONTH, p_LINE_CD, p_TIME_D_1, p_TIME_D_2, p_TIME_D_3,
                p_TIME_D_4, p_TIME_D_5, p_TIME_D_6, p_TIME_D_7, p_TIME_D_8, p_TIME_D_9, p_TIME_D_10, p_TIME_D_11, p_TIME_D_12,
                p_TIME_D_13, p_TIME_D_14, p_TIME_D_15, p_TIME_D_16, p_TIME_D_17, p_TIME_D_18, p_TIME_D_19, p_TIME_D_20,
                p_TIME_D_21, p_TIME_D_22, p_TIME_D_23, p_TIME_D_24, p_TIME_D_25, p_TIME_D_26, p_TIME_D_27, p_TIME_D_28,
                p_TIME_D_29, p_TIME_D_30, p_TIME_D_31);
          
        }
        public void _DownloadSource211ProductionSchedule(object sender, EventArgs e, string p_PROD_MONTH, string p_PLANT_CD, string p_LINE_CD)
        {

            List<mProductionSchedule> model = new List<mProductionSchedule>();
            model = ProductionSchedule.getListProductionSchedule_cari
            (p_PROD_MONTH, p_PLANT_CD, p_LINE_CD);


            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/Production_Schedule_Inquiry_Download.xls");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.TOP;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.TOP;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            styleContent2.Alignment = HorizontalAlignment.CENTER;

            ISheet sheet = workbook.GetSheet("Prod Schedule Inquiry");
            string date = DateTime.Now.ToString("dd-MM-yyyy");
            filename = "ProductionScheduleInquiry_" + date + ".xls";
            sheet.ForceFormulaRecalculation = true;


            sheet.GetRow(6).GetCell(3).SetCellValue(date);
            sheet.GetRow(7).GetCell(3).SetCellValue("Reyza");
            sheet.GetRow(9).GetCell(4).SetCellValue(p_PROD_MONTH);
            sheet.GetRow(10).GetCell(4).SetCellValue(p_PLANT_CD);
            sheet.GetRow(11).GetCell(4).SetCellValue(p_LINE_CD);




            int row = 17;
            string NOROW = string.Empty;
            string PRODMONTH = string.Empty;
            string PLANTCD = string.Empty;
            string LINECD = string.Empty;
            string DAY1 = string.Empty;
            string DAY2 = string.Empty;
            string DAY3 = string.Empty;
            string DAY4 = string.Empty;
            string DAY5 = string.Empty;
            string DAY6 = string.Empty;
            string DAY7 = string.Empty;
            string DAY8 = string.Empty;
            string DAY9 = string.Empty;
            string DAY10 = string.Empty;
            string DAY11 = string.Empty;
            string DAY12 = string.Empty;
            string DAY13 = string.Empty;
            string DAY14 = string.Empty;
            string DAY15 = string.Empty;
            string DAY16 = string.Empty;
            string DAY17 = string.Empty;
            string DAY18 = string.Empty;
            string DAY19 = string.Empty;
            string DAY20 = string.Empty;
            string DAY21 = string.Empty;
            string DAY22 = string.Empty;
            string DAY23 = string.Empty;
            string DAY24 = string.Empty;
            string DAY25 = string.Empty;
            string DAY26 = string.Empty;
            string DAY27 = string.Empty;
            string DAY28 = string.Empty;
            string DAY29 = string.Empty;
            string DAY30 = string.Empty;
            string DAY31 = string.Empty;
            

            foreach (var result in model)
            {
                NOROW = result.NOROW;
                PRODMONTH = result.PROD_MONTH;
                PLANTCD = result.PLANT_CD;
                LINECD = result.LINE_CD;
                DAY1 = result.TIME_D_1;
                DAY2 = result.TIME_D_2;
                DAY3 = result.TIME_D_3;
                DAY4 = result.TIME_D_4;
                DAY5 = result.TIME_D_5;
                DAY6 = result.TIME_D_6;
                DAY7 = result.TIME_D_7;
                DAY8 = result.TIME_D_8;
                DAY9 = result.TIME_D_9;
                DAY10 = result.TIME_D_10;
                DAY11 = result.TIME_D_11;
                DAY12 = result.TIME_D_12;
                DAY13 = result.TIME_D_13;
                DAY14 = result.TIME_D_14;
                DAY15 = result.TIME_D_15;
                DAY16 = result.TIME_D_16;
                DAY17 = result.TIME_D_17;
                DAY18 = result.TIME_D_18;
                DAY19 = result.TIME_D_19;
                DAY20 = result.TIME_D_20;
                DAY21 = result.TIME_D_21;
                DAY22 = result.TIME_D_22;
                DAY23 = result.TIME_D_23;
                DAY24 = result.TIME_D_24;
                DAY25 = result.TIME_D_25;
                DAY26 = result.TIME_D_26;
                DAY27 = result.TIME_D_27;
                DAY28 = result.TIME_D_28;
                DAY29 = result.TIME_D_29;
                DAY30 = result.TIME_D_30;
                DAY31 = result.TIME_D_31;


                sheet.GetRow(row).GetCell(1).SetCellValue(NOROW);
                sheet.GetRow(row).GetCell(2).SetCellValue(PRODMONTH);
                sheet.GetRow(row).GetCell(3).SetCellValue(PLANTCD);
                sheet.GetRow(row).GetCell(4).SetCellValue(LINECD);
                sheet.GetRow(row).GetCell(5).SetCellValue(DAY1);
                sheet.GetRow(row).GetCell(6).SetCellValue(DAY2);
                sheet.GetRow(row).GetCell(7).SetCellValue(DAY3);
                sheet.GetRow(row).GetCell(8).SetCellValue(DAY4);
                sheet.GetRow(row).GetCell(9).SetCellValue(DAY5);
                sheet.GetRow(row).GetCell(10).SetCellValue(DAY6);
                sheet.GetRow(row).GetCell(11).SetCellValue(DAY7);
                sheet.GetRow(row).GetCell(12).SetCellValue(DAY8);
                sheet.GetRow(row).GetCell(13).SetCellValue(DAY9);
                sheet.GetRow(row).GetCell(14).SetCellValue(DAY10);
                sheet.GetRow(row).GetCell(15).SetCellValue(DAY11);
                sheet.GetRow(row).GetCell(16).SetCellValue(DAY12);
                sheet.GetRow(row).GetCell(17).SetCellValue(DAY13);
                sheet.GetRow(row).GetCell(18).SetCellValue(DAY14);
                sheet.GetRow(row).GetCell(19).SetCellValue(DAY15);
                sheet.GetRow(row).GetCell(20).SetCellValue(DAY16);
                sheet.GetRow(row).GetCell(21).SetCellValue(DAY17);
                sheet.GetRow(row).GetCell(22).SetCellValue(DAY18);
                sheet.GetRow(row).GetCell(23).SetCellValue(DAY19);
                sheet.GetRow(row).GetCell(24).SetCellValue(DAY20);
                sheet.GetRow(row).GetCell(25).SetCellValue(DAY21);
                sheet.GetRow(row).GetCell(26).SetCellValue(DAY22);
                sheet.GetRow(row).GetCell(27).SetCellValue(DAY23);
                sheet.GetRow(row).GetCell(28).SetCellValue(DAY24);
                sheet.GetRow(row).GetCell(29).SetCellValue(DAY25);
                sheet.GetRow(row).GetCell(30).SetCellValue(DAY26);
                sheet.GetRow(row).GetCell(31).SetCellValue(DAY27);
                sheet.GetRow(row).GetCell(32).SetCellValue(DAY28);
                sheet.GetRow(row).GetCell(33).SetCellValue(DAY29);
                sheet.GetRow(row).GetCell(34).SetCellValue(DAY30);
                sheet.GetRow(row).GetCell(35).SetCellValue(DAY31);
                

                sheet.GetRow(row).GetCell(1).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(2).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(3).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(4).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(5).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(6).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(7).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(8).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(9).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(10).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(11).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(12).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(13).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(14).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(15).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(16).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(17).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(18).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(19).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(20).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(21).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(22).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(23).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(24).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(25).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(26).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(27).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(28).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(29).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(30).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(31).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(32).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(33).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(34).CellStyle = styleContent2;
                sheet.GetRow(row).GetCell(35).CellStyle = styleContent2;
               
                row++;
            }

            for (var i = 0; i < sheet.GetRow(0).LastCellNum; i++)
                sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }
        
        //untuk upload
        public ActionResult SPN212CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadProdScheInqury", UploadControlHelper.ValidationSettings, SPN212CallbackUploaduc_FileUploadComplete);
            return null;
        }
        public class UploadControlHelper
        {
            public const string ThumbnailFormat = "Thumbnail{0}{1}";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xls" },
                MaxFileSize = 209751520
            };
        }

        public void SPN212CallbackUploaduc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                var resultFilePath = HttpContext.Request.MapPath("~/Content/FileUploadResult/ProductionScheduleInquiry_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xls");
                e.UploadedFile.SaveAs(resultFilePath);
                IUrlResolutionService urlResolver = sender as IUrlResolutionService;
                if (urlResolver != null)
                {
                    e.CallbackData = urlResolver.ResolveClientUrl(resultFilePath);
                }

                string result = SPN212CallbackUploadExcelToSQL(resultFilePath);
            }
        }
        public string SPN212CallbackUploadExcelToSQL(string resultFilePath)
        {
            try
            {
                FileStream ftmp = new FileStream(resultFilePath, FileMode.Open, FileAccess.Read);

                HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
                ISheet sheet = workbook.GetSheet("ProdScheduleInquiry");

                //string CreatedDT = DateTime.Now.ToString("MM-dd-yyyy");
                //string CreatedBy = "Sys";

                int row = 2;
                mProcessID ModProcessID = new mProcessID();
                string ProcessId = ModProcessID.GetProcessID();
                //kalau kosong cell nya masih error
                for (int i = 2; i <= sheet.LastRowNum; i++)
                {
                    #region
                    //string p_PROD_MONTH = sheet.GetRow(row).GetCell(0).StringCellValue;
                    //string p_LINE_CD = sheet.GetRow(row).GetCell(1).StringCellValue;
                    //string p_TIME_D_1 = sheet.GetRow(row).GetCell(2).StringCellValue;
                    //string p_TIME_D_2 = sheet.GetRow(row).GetCell(3).StringCellValue;
                    //string p_TIME_D_3 = sheet.GetRow(row).GetCell(4).StringCellValue;
                    //string p_TIME_D_4 = sheet.GetRow(row).GetCell(5).StringCellValue;
                    //string p_TIME_D_5 = sheet.GetRow(row).GetCell(6).StringCellValue;
                    //string p_TIME_D_6 = sheet.GetRow(row).GetCell(7).StringCellValue;
                    //string p_TIME_D_7 = sheet.GetRow(row).GetCell(8).StringCellValue;
                    //string p_TIME_D_8 = sheet.GetRow(row).GetCell(9).StringCellValue;
                    //string p_TIME_D_9 = sheet.GetRow(row).GetCell(10).StringCellValue;
                    //string p_TIME_D_10 = sheet.GetRow(row).GetCell(11).StringCellValue;
                    //string p_TIME_D_11 = sheet.GetRow(row).GetCell(12).StringCellValue;
                    //string p_TIME_D_12 = sheet.GetRow(row).GetCell(13).StringCellValue;
                    //string p_TIME_D_13 = sheet.GetRow(row).GetCell(14).StringCellValue;
                    //string p_TIME_D_14 = sheet.GetRow(row).GetCell(15).StringCellValue;
                    //string p_TIME_D_15 = sheet.GetRow(row).GetCell(16).StringCellValue;
                    //string p_TIME_D_16 = sheet.GetRow(row).GetCell(17).StringCellValue;
                    //string p_TIME_D_17 = sheet.GetRow(row).GetCell(18).StringCellValue;
                    //string p_TIME_D_18 = sheet.GetRow(row).GetCell(19).StringCellValue;
                    //string p_TIME_D_19 = sheet.GetRow(row).GetCell(20).StringCellValue;
                    //string p_TIME_D_20 = sheet.GetRow(row).GetCell(21).StringCellValue;
                    //string p_TIME_D_21 = sheet.GetRow(row).GetCell(22).StringCellValue;
                    //string p_TIME_D_22 = sheet.GetRow(row).GetCell(23).StringCellValue;
                    //string p_TIME_D_23 = sheet.GetRow(row).GetCell(24).StringCellValue;
                    //string p_TIME_D_24 = sheet.GetRow(row).GetCell(25).StringCellValue;
                    //string p_TIME_D_25 = sheet.GetRow(row).GetCell(26).StringCellValue;
                    //string p_TIME_D_26 = sheet.GetRow(row).GetCell(27).StringCellValue;
                    //string p_TIME_D_27 = sheet.GetRow(row).GetCell(28).StringCellValue;
                    //string p_TIME_D_28 = sheet.GetRow(row).GetCell(29).StringCellValue;
                    //string p_TIME_D_29 = sheet.GetRow(row).GetCell(30).StringCellValue;
                    //string p_TIME_D_30 = sheet.GetRow(row).GetCell(31).StringCellValue;
                    //string p_TIME_D_31 = sheet.GetRow(row).GetCell(32).StringCellValue;

                    //if (String.IsNullOrEmpty(PartNo) && String.IsNullOrEmpty(LineCd) && String.IsNullOrEmpty(PlantCd) && String.IsNullOrEmpty(SlocCd) && String.IsNullOrEmpty(ValidFrom) && String.IsNullOrEmpty(ValidTo))
                    //{
                    //    break;
                    //}
                    #endregion
                    string p_PROD_MONTH = string.Empty;
                    string p_LINE_CD = string.Empty;
                    string p_TIME_D_1 = string.Empty;
                    string p_TIME_D_2 = string.Empty;
                    string p_TIME_D_3 = string.Empty;
                    string p_TIME_D_4 = string.Empty;
                    string p_TIME_D_5 = string.Empty;
                    string p_TIME_D_6 = string.Empty;
                    string p_TIME_D_7 = string.Empty;
                    string p_TIME_D_8 = string.Empty;
                    string p_TIME_D_9 = string.Empty;
                    string p_TIME_D_10 = string.Empty;
                    string p_TIME_D_11 = string.Empty;
                    string p_TIME_D_12 = string.Empty;
                    string p_TIME_D_13 = string.Empty;
                    string p_TIME_D_14 = string.Empty;
                    string p_TIME_D_15 = string.Empty;
                    string p_TIME_D_16 = string.Empty;
                    string p_TIME_D_17 = string.Empty;
                    string p_TIME_D_18 = string.Empty;
                    string p_TIME_D_19 = string.Empty;
                    string p_TIME_D_20 = string.Empty;
                    string p_TIME_D_21 = string.Empty;
                    string p_TIME_D_22 = string.Empty;
                    string p_TIME_D_23 = string.Empty;
                    string p_TIME_D_24 = string.Empty;
                    string p_TIME_D_25 = string.Empty;
                    string p_TIME_D_26 = string.Empty;
                    string p_TIME_D_27 = string.Empty;
                    string p_TIME_D_28 = string.Empty;
                    string p_TIME_D_29 = string.Empty;
                    string p_TIME_D_30 = string.Empty;
                    string p_TIME_D_31 = string.Empty;

                    ICell PRODMONTHCELL = sheet.GetRow(row).GetCell(0);
                    ICell LINECDCELL = sheet.GetRow(row).GetCell(1);
                    ICell TIME_D1CELL = sheet.GetRow(row).GetCell(2);
                    ICell TIME_D2CELL = sheet.GetRow(row).GetCell(3);
                    ICell TIME_D3CELL = sheet.GetRow(row).GetCell(4);
                    ICell TIME_D4CELL = sheet.GetRow(row).GetCell(5);
                    ICell TIME_D5CELL = sheet.GetRow(row).GetCell(6);
                    ICell TIME_D6CELL = sheet.GetRow(row).GetCell(7);
                    ICell TIME_D7CELL = sheet.GetRow(row).GetCell(8);
                    ICell TIME_D8CELL = sheet.GetRow(row).GetCell(9);
                    ICell TIME_D9CELL = sheet.GetRow(row).GetCell(10);
                    ICell TIME_D10CELL = sheet.GetRow(row).GetCell(11);
                    ICell TIME_D11CELL = sheet.GetRow(row).GetCell(12);
                    ICell TIME_D12CELL = sheet.GetRow(row).GetCell(13);
                    ICell TIME_D13CELL = sheet.GetRow(row).GetCell(14);
                    ICell TIME_D14CELL = sheet.GetRow(row).GetCell(15);
                    ICell TIME_D15CELL = sheet.GetRow(row).GetCell(16);
                    ICell TIME_D16CELL = sheet.GetRow(row).GetCell(17);
                    ICell TIME_D17CELL = sheet.GetRow(row).GetCell(18);
                    ICell TIME_D18CELL = sheet.GetRow(row).GetCell(19);
                    ICell TIME_D19CELL = sheet.GetRow(row).GetCell(20);
                    ICell TIME_D20CELL = sheet.GetRow(row).GetCell(21);
                    ICell TIME_D21CELL = sheet.GetRow(row).GetCell(22);
                    ICell TIME_D22CELL = sheet.GetRow(row).GetCell(23);
                    ICell TIME_D23CELL = sheet.GetRow(row).GetCell(24);
                    ICell TIME_D24CELL = sheet.GetRow(row).GetCell(25);
                    ICell TIME_D25CELL = sheet.GetRow(row).GetCell(26);
                    ICell TIME_D26CELL = sheet.GetRow(row).GetCell(27);
                    ICell TIME_D27CELL = sheet.GetRow(row).GetCell(28);
                    ICell TIME_D28CELL = sheet.GetRow(row).GetCell(29);
                    ICell TIME_D29CELL = sheet.GetRow(row).GetCell(30);
                    ICell TIME_D30CELL = sheet.GetRow(row).GetCell(31);
                    ICell TIME_D31CELL = sheet.GetRow(row).GetCell(32);

                    #region validasi jika ada data diexcel
                    if (PRODMONTHCELL != null)
                    {
                        if (PRODMONTHCELL.CellType != CellType.BLANK)
                        {
                            p_PROD_MONTH = sheet.GetRow(row).GetCell(0).StringCellValue;
                        }
                    }

                    if (LINECDCELL != null)
                    {
                        if (LINECDCELL.CellType != CellType.BLANK)
                        {
                            p_LINE_CD = sheet.GetRow(row).GetCell(1).StringCellValue;
                        }
                    }

                    if (TIME_D1CELL != null)
                    {
                        if (TIME_D1CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_1 = sheet.GetRow(row).GetCell(2).StringCellValue;
                        }
                    }

                    if (TIME_D2CELL != null)
                    {
                        if (TIME_D2CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_2 = sheet.GetRow(row).GetCell(3).StringCellValue;
                        }
                    }
                    if (TIME_D3CELL != null)
                    {
                        if (TIME_D3CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_3 = sheet.GetRow(row).GetCell(4).StringCellValue;
                        }
                    }
                    if (TIME_D4CELL != null)
                    {
                        if (TIME_D4CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_4 = sheet.GetRow(row).GetCell(5).StringCellValue;
                        }
                    }

                    if (TIME_D5CELL != null)
                    {
                        if (TIME_D5CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_5 = sheet.GetRow(row).GetCell(6).StringCellValue;
                        }
                    }

                    if (TIME_D6CELL != null)
                    {
                        if (TIME_D6CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_6 = sheet.GetRow(row).GetCell(7).StringCellValue;
                        }
                    }

                    if (TIME_D7CELL != null)
                    {
                        if (TIME_D7CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_7 = sheet.GetRow(row).GetCell(8).StringCellValue;
                        }
                    }

                    if (TIME_D8CELL != null)
                    {
                        if (TIME_D8CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_8 = sheet.GetRow(row).GetCell(9).StringCellValue;
                        }
                    }

                    if (TIME_D9CELL != null)
                    {
                        if (TIME_D9CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_9 = sheet.GetRow(row).GetCell(10).StringCellValue;
                        }
                    }

                    if (TIME_D10CELL != null)
                    {
                        if (TIME_D10CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_10 = sheet.GetRow(row).GetCell(11).StringCellValue;
                        }
                    }

                    if (TIME_D11CELL != null)
                    {
                        if (TIME_D11CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_11 = sheet.GetRow(row).GetCell(12).StringCellValue;
                        }
                    }
                    if (TIME_D12CELL != null)
                    {
                        if (TIME_D12CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_12 = sheet.GetRow(row).GetCell(13).StringCellValue;
                        }
                    }
                    if (TIME_D13CELL != null)
                    {
                        if (TIME_D13CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_13 = sheet.GetRow(row).GetCell(14).StringCellValue;
                        }
                    }

                    if (TIME_D14CELL != null)
                    {
                        if (TIME_D14CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_14 = sheet.GetRow(row).GetCell(15).StringCellValue;
                        }
                    }

                    if (TIME_D15CELL != null)
                    {
                        if (TIME_D15CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_15 = sheet.GetRow(row).GetCell(16).StringCellValue;
                        }
                    }

                    if (TIME_D16CELL != null)
                    {
                        if (TIME_D16CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_16 = sheet.GetRow(row).GetCell(17).StringCellValue;
                        }
                    }
                    if (TIME_D17CELL != null)
                    {
                        if (TIME_D17CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_17 = sheet.GetRow(row).GetCell(18).StringCellValue;
                        }
                    }

                    if (TIME_D18CELL != null)
                    {
                        if (TIME_D18CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_18 = sheet.GetRow(row).GetCell(19).StringCellValue;
                        }
                    }

                    if (TIME_D19CELL != null)
                    {
                        if (TIME_D19CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_19 = sheet.GetRow(row).GetCell(20).StringCellValue;
                        }
                    }

                   

                    if (TIME_D20CELL != null)
                    {
                        if (TIME_D20CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_20 = sheet.GetRow(row).GetCell(21).StringCellValue;
                        }
                    }

                    if (TIME_D21CELL != null)
                    {
                        if (TIME_D21CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_21 = sheet.GetRow(row).GetCell(22).StringCellValue;
                        }
                    }

                    if (TIME_D22CELL != null)
                    {
                        if (TIME_D22CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_22 = sheet.GetRow(row).GetCell(23).StringCellValue;
                        }
                    }

                    if (TIME_D23CELL != null)
                    {
                        if (TIME_D23CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_23 = sheet.GetRow(row).GetCell(24).StringCellValue;
                        }
                    }

                    if (TIME_D24CELL != null)
                    {
                        if (TIME_D24CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_24 = sheet.GetRow(row).GetCell(25).StringCellValue;
                        }
                    }

                   

                    if (TIME_D25CELL != null)
                    {
                        if (TIME_D25CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_25 = sheet.GetRow(row).GetCell(26).StringCellValue;
                        }
                    }
                    if (TIME_D26CELL != null)
                    {
                        if (TIME_D26CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_26 = sheet.GetRow(row).GetCell(27).StringCellValue;
                        }
                    }
                    if (TIME_D27CELL != null)
                    {
                        if (TIME_D27CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_27 = sheet.GetRow(row).GetCell(28).StringCellValue;
                        }
                    }
                    if (TIME_D28CELL != null)
                    {
                        if (TIME_D28CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_28 = sheet.GetRow(row).GetCell(29).StringCellValue;
                        }
                    }

                    if (TIME_D29CELL != null)
                    {
                        if (TIME_D29CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_29 = sheet.GetRow(row).GetCell(30).StringCellValue;
                        }
                    }
                    if (TIME_D30CELL != null)
                    {
                        if (TIME_D30CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_30 = sheet.GetRow(row).GetCell(31).StringCellValue;
                        }
                    }
                    if (TIME_D31CELL != null)
                    {
                        if (TIME_D31CELL.CellType != CellType.BLANK)
                        {
                            p_TIME_D_31 = sheet.GetRow(row).GetCell(32).StringCellValue;
                        }
                    }
                    #endregion
                   ProductionSchedule.SPN212CallbackUploadExcelToSQL(p_PROD_MONTH, p_LINE_CD, p_TIME_D_1, p_TIME_D_2, p_TIME_D_3, p_TIME_D_4, p_TIME_D_5, p_TIME_D_6, p_TIME_D_7, p_TIME_D_8, p_TIME_D_9, p_TIME_D_10, p_TIME_D_11, p_TIME_D_12, p_TIME_D_13, p_TIME_D_14, p_TIME_D_15, p_TIME_D_16, p_TIME_D_17, p_TIME_D_18, p_TIME_D_19, p_TIME_D_20, p_TIME_D_21, p_TIME_D_22, p_TIME_D_23, p_TIME_D_24, p_TIME_D_25, p_TIME_D_26, p_TIME_D_27, p_TIME_D_28, p_TIME_D_29, p_TIME_D_30, p_TIME_D_31, ProcessId);
                    row++;
                }

                get_Message_Upload = ProductionSchedule.SPN212SaveUploadExcel(ProcessId);

                if (get_Message_Upload == "Process UPLOAD PRODUCTION SCHEDULE finish successfully")
                {
                    return get_Message_Upload;
                }
                else
                {
                    return get_Message_Upload = "Error : please check Process ID " + ProcessId;
                }
            }
            catch (Exception err)
            {
                return get_Message_Upload = "Error | " + Convert.ToString(err.Message);
            }
        }
        public string SPN212GetInfo()
        {

            return get_Message_Upload;
        }
    }

}

    










