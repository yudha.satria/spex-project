﻿///
/// <author>lufty.abdillah@gmail.com</author>
/// <summary>
/// Toyota .Net Development Kit
/// Copyright (c) Toyota Motor Manufacturing Indonesia, All Right Reserved.
/// </summary>
/// 

using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    /*
     * By default, TDK will finds Login controller when the application authentication enabled.
     * You can override this behaviour at Global.asax as follows:
     *      ApplicationSettings.Instance.Security.LoginController = [Your desired controller name]
     */
    public class LoginController : LoginPageController
    {
    }
}