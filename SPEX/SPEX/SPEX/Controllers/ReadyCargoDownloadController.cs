﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using NPOI.XSSF.UserModel;
using System.Web.UI.WebControls;
using System.Web.UI;
using Ionic.Zip;
using System.Text;

namespace SPEX.Controllers
{
    public class ReadyCargoDownloadController : PageController
    {

        Log log = new Log();
        //mBackOrder BackOrder = new mBackOrder();
        MessagesString msgError = new MessagesString();
        public ReadyCargoDownloadController()
        {
            Settings.Title = "Ready Cargo Download";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("ready Cargo");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            ViewBag.MSPX000018ERR = msgError.getMSPX00018ERR();
            
            ReadyCargoDownload RG = new ReadyCargoDownload();
            ViewBag.MSPXMSGDIIFERENTDATE_PACKING_DATE = RG.GetMessageDiferrentDatePackingrDate();
            ViewBag.MSPXMSGDIIFERENTDATE_ORDER_DATE = RG.GetMessageDiferrentDateOrderDate();
            ViewBag.MSPXDIFERRENTDATE = RG.GetDiferrentDate();
            //getpE_UserId = (User)ViewData["User"];
            ViewData["Packing_Company"] =  RG.GetPackingCompany();

        }


        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                result = ar[1] + "/" + ar[0] + "/" + ar[2];
                //result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));
            }

            return result;
        }

        public ActionResult ReadyCargoSearchCalback(string Data)
        {
            ReadyCargoDownload cargo = System.Web.Helpers.Json.Decode<ReadyCargoDownload>(Data);
            List<ReadyCargoDownload> Model = new List<ReadyCargoDownload>();
            Model = cargo.GetData(cargo);
            return PartialView("_PartialGrid", Model);
        }

        public void Download(string Data)
        {

            ReadyCargoDownload cargo = System.Web.Helpers.Json.Decode<ReadyCargoDownload>(Data);
            List<ReadyCargoDownload> RCDs = new List<ReadyCargoDownload>();
            RCDs = cargo.GetData(cargo);

            string date = DateTime.Now.ToString("ddMMyyyy");
            string filename = "ReadyCargoDownload_" + date + ".xlsx";
            string directoryname = "ReadyCargoDownload_" + date + "/";
            string path = Server.MapPath("~/Download/");
            string filename_zip = "ReadyCargoDownload_" + date + ".zip";
            Directory.CreateDirectory(HttpContext.Request.MapPath("~/Download/" + directoryname));

            string filesTmp = HttpContext.Request.MapPath("~/Template/TemplateReadyCargoDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);
            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);
            ISheet sheet = workbook.GetSheet("Sheet1");
            

            if (cargo.ORDER_DT_FROM!=null)
                sheet.GetRow(2).GetCell(0).SetCellValue(String.Format("{0:dd.MM.yyyy}", cargo.ORDER_DT_FROM));
            if (cargo.ORDER_DT_TO != null)
                sheet.GetRow(2).GetCell(1).SetCellValue(String.Format("{0:dd.MM.yyyy}", cargo.ORDER_DT_TO));
            if (cargo.PACKING_DT_FROM != null)
                sheet.GetRow(5).GetCell(0).SetCellValue(String.Format("{0:dd.MM.yyyy}", cargo.PACKING_DT_FROM));
            if (cargo.PACKING_DT_TO!= null)
                sheet.GetRow(5).GetCell(1).SetCellValue(String.Format("{0:dd.MM.yyyy}", cargo.PACKING_DT_TO));
            sheet.GetRow(7).GetCell(1).SetCellValue( cargo.PACKING_COMPANY);
            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");

            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            int row = 13;
            IRow Hrow;
            foreach (ReadyCargoDownload result in RCDs)
            {
                Hrow = sheet.CreateRow(row);                
                Hrow.CreateCell(0).SetCellValue(result.DESTINATION);
                Hrow.CreateCell(1).SetCellValue(result.TRANSPORT);
                Hrow.CreateCell(2).SetCellValue(result.ORDER_NO);
                Hrow.CreateCell(3).SetCellValue(result.CASE_NO);
                Hrow.CreateCell(4).SetCellValue(result.ITEM_NO);
                Hrow.CreateCell(5).SetCellValue(result.PART_NO);
                Hrow.CreateCell(6).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.PACKAGE_DATE));
                Hrow.CreateCell(7).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.VANNING_DATE));
                Hrow.CreateCell(8).SetCellValue(result.CONTAINER_NO);
                Hrow.CreateCell(9).SetCellValue(result.QTY);
                Hrow.CreateCell(10).SetCellValue(result.INVOICE_NO);
                Hrow.CreateCell(11).SetCellValue(String.Format("{0:dd.MM.yyyy}", result.ETD));
                Hrow.CreateCell(12).SetCellValue(result.PACKING_COMPANY);

                //Hrow.GetCell(0).CellStyle = styleContent;
                //Hrow.GetCell(1).CellStyle = styleContent;
                //Hrow.GetCell(2).CellStyle = styleContent;
                //Hrow.GetCell(3).CellStyle = styleContent;
                //Hrow.GetCell(4).CellStyle = styleContent;
                //Hrow.GetCell(5).CellStyle = styleContent;
                //Hrow.GetCell(6).CellStyle = styleContent2;
                //Hrow.GetCell(7).CellStyle = styleContent2;
                //Hrow.GetCell(8).CellStyle = styleContent;
                //Hrow.GetCell(9).CellStyle = styleContent;
                //Hrow.GetCell(10).CellStyle = styleContent;
                //Hrow.GetCell(11).CellStyle = styleContent2;
                //Hrow.GetCell(12).CellStyle = styleContent;
                row++;
            }
            //FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);
            //MemoryStream ms = new MemoryStream();
            //workbook.Write(ms);
            //ftmp.Close();
            //Response.BinaryWrite(ms.ToArray());
            //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
           
            FileStream fs = new FileStream(HttpContext.Request.MapPath(path + directoryname) + filename , FileMode.CreateNew, FileAccess.ReadWrite);
            workbook.Write(fs);
            fs.Close();

            using (ZipFile zip = new ZipFile())
            {
                zip.AddDirectory(HttpContext.Request.MapPath("~/Download/" + directoryname));

                MemoryStream output = new MemoryStream();
                zip.Save(output);
                Response.BinaryWrite(output.ToArray());
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment;filename=" + filename_zip);
            }

            if (Directory.Exists(HttpContext.Request.MapPath("~/Download/" + directoryname)))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(HttpContext.Request.MapPath("~/Download/" + directoryname));
                FileInfo[] fileInfos = dirInfo.GetFiles();
                foreach (FileInfo fileInfo in fileInfos)
                {
                    if (System.IO.File.Exists(fileInfo.FullName))
                        System.IO.File.Delete(fileInfo.FullName);
                }
                Directory.Delete(HttpContext.Request.MapPath("~/Download/" + directoryname));
            }

        }

        public void ExportData(string Data)
        {
            ReadyCargoDownload cargo = System.Web.Helpers.Json.Decode<ReadyCargoDownload>(Data);
            List<ReadyCargoDownload> RCDs = new List<ReadyCargoDownload>();
            RCDs = cargo.GetData(cargo);
            //GridView gv = new GridView();
            //gv.DataSource = RCDs;
            //gv.DataBind();


            string path = Server.MapPath("~/Download/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string date = DateTime.Now.ToString("ddMMyyyy");
            string filename = "ReadyCargoDownload_" + date + ".csv";
            string filename_zip = "ReadyCargoDownload_" + date + ".zip";
            string directoryname = "ReadyCargoDownload_" + date+"/";
            Directory.CreateDirectory(HttpContext.Request.MapPath("~/Download/" + directoryname));

            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", "Destination", "Transport", "Order No", "Case No", "Item No", "Part No", "Package Date", "Vanning Date", "Container No", "Qty", "Invoice No", "Shipment Date (ETD)","Packing Company") + Environment.NewLine);

            foreach (ReadyCargoDownload d in RCDs)
            {
                sb.Append(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", "=\"" + d.DESTINATION + "\"", "=\"" + d.TRANSPORT + "\"", "=\"" + d.ORDER_NO + "\"", "=\"" + d.CASE_NO + "\"", "=\"" + d.ITEM_NO + "\"", "=\"" + d.PART_NO + "\"", String.Format("{0:dd.MM.yyyy}", d.PACKAGE_DATE), String.Format("{0:dd.MM.yyyy}", d.VANNING_DATE), "=\"" + d.CONTAINER_NO + "\"", d.QTY, "=\"" + d.INVOICE_NO + "\"", String.Format("{0:dd.MM.yyyy}", d.ETD),d.PACKING_COMPANY) + Environment.NewLine);
            }
            byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());
            System.IO.File.WriteAllBytes(path + directoryname + filename, bytes);
         

           // System.IO.File.AppendAllText(path + directoryname + filename, query);

            //using (StringWriter textWriter = new StringWriter())
            //{
            //    using(Html32TextWriter htmlTextWriter = new Html32TextWriter(textWriter))
            //    {
            //        StreamWriter writer = System.IO.File.AppendText(path + directoryname + filename);
            //        gv.RenderControl(htmlTextWriter);
            //        writer.WriteLine(textWriter.ToString());
            //        writer.Close();
            //    }
            //}
            //       
            //using (StringWriter sw = new StringWriter())
            //{
            //    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
            //    {
            //        StreamWriter writer = System.IO.File.AppendText(path + directoryname + filename);
            //        gv.RenderControl(hw);
            //        writer.WriteLine(sw.ToString());
            //        writer.Close();
            //    }
            //}

            //System.IO.StringWriter sw = new System.IO.StringWriter();
            //System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);

            //// Render grid view control.
            //gv.RenderControl(htw);

            //// Write the rendered content to a file.
            //string renderedGridView = sw.ToString();
            //System.IO.File.WriteAllText(path + directoryname + filename, renderedGridView);
            //var wb = new XLWorkbook();
            //var ws = wb.Worksheets.Add("Data_Test_Worksheet");
            //ws.Cell(1, 1).InsertData(newList);
            //wb.SaveAs(path + directoryname + filename, renderedGridView));


            using (ZipFile zip = new ZipFile())
            {
                zip.AddDirectory(HttpContext.Request.MapPath("~/Download/" + directoryname));

                MemoryStream output = new MemoryStream();
                zip.Save(output);
                Response.BinaryWrite(output.ToArray());
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment;filename=" + filename_zip);
            }

            if (Directory.Exists(HttpContext.Request.MapPath("~/Download/" + directoryname)))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(HttpContext.Request.MapPath("~/Download/" + directoryname));
                FileInfo[] fileInfos = dirInfo.GetFiles();
                foreach (FileInfo fileInfo in fileInfos)
                {
                    if (System.IO.File.Exists(fileInfo.FullName))
                        System.IO.File.Delete(fileInfo.FullName);
                }
                Directory.Delete(HttpContext.Request.MapPath("~/Download/" + directoryname));
            }

        }
    }
}
