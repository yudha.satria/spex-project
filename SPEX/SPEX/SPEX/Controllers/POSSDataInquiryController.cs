﻿using System.Collections.Generic;
using System.Web.Mvc;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class POSSDataInquiryController : PageController
    {
        mPOSSDataInquiry possData = new mPOSSDataInquiry();

        public POSSDataInquiryController()
        {
            Settings.Title = "POSS Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                    return (User)Session["getpE_UserId"];
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            getpE_UserId = (User)ViewData["User"];
            ViewData["PackingCompanyList"] = (new mInvoice()).GetPackingCompany();
        }

        public string reFormatDate(string dt)
        {
            string result = "";
            string[] ar = null;
            //DD.MM.YYYY
            if (dt != "01.01.0100")
            {
                ar = dt.Split('.');
                //MM/DD/YYYY
                result = ar[2] + "-" + ar[1] + "-" + ar[0];
            }
            else
                result = "";
            return result;
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult POSSGridCallBack(string pPOSS_DT_FROM, string pPOSS_DT_TO, string pPACKING_COMPANY, string pTMAP_SEND,
            string pORDER_NO, string pPART_NO, string pITEM_NO)
        {
            List<mPOSSDataInquiry> model = new List<mPOSSDataInquiry>();

            model = possData.GetList(reFormatDate(pPOSS_DT_FROM), reFormatDate(pPOSS_DT_TO), pTMAP_SEND, pPACKING_COMPANY,
             pORDER_NO, pPART_NO, pITEM_NO);

            return PartialView("POSSDataGrid", model);
        }
    }
}
