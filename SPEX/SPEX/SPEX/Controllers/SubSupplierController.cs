﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class SubSupplierController : PageController
    {
        //
        // GET: /BuyerPD/
        MessagesString msgError = new MessagesString();

        public SubSupplierController()
        {
            Settings.Title = "Sub Supplier Master";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        protected override void Startup()
        {
            //Call error message
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("Sub Supplier Master");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
        }

        public ActionResult SubSupplierCallBack(string MAIN_SUPP_CD, string SUB_SUPP_CD, string MAIN_PLANT_CD, string SUB_PLANT_CD)
        {
            //string MAIN_SUPP_CD = Request.Params["MAIN_SUPP_CD"];
            //string SUB_SUPP_CD = Request.Params["SUB_SUPP_CD"];

            SubSupplier SS = new SubSupplier();
            SS.MAIN_SUPP_CD = MAIN_SUPP_CD;
            SS.MAIN_PLANT_CD = MAIN_PLANT_CD;
            SS.SUB_SUPP_CD = SUB_SUPP_CD;
            SS.SUB_PLANT_CD = SUB_PLANT_CD;
            List<SubSupplier> model = new List<SubSupplier>();
            model = SS.getSubSupplier(SS);

            return PartialView("_SubSupplierGrid", model);
        }

        public ActionResult DeleteSubSUpplier(string SUB_SUPPLIER_LIST)
        {
            SubSupplier SS = new SubSupplier();
            string resultMessage = string.Empty;
            string error = string.Empty;
            try
            {
                resultMessage = SS.DeleteData(SUB_SUPPLIER_LIST);
            }
            catch (Exception ex)
            {
                resultMessage = "Error";
                error = ex.Message;
            }

            if (resultMessage == "Error")
            {
                return Json(new { success = "false", messages = error }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = resultMessage }, JsonRequestBehavior.AllowGet);
            }

            return null;
        }
        //#endregion 

        // #region Function Add
        public ActionResult AddNewSubSupplier(string MAIN_SUPP_CD, string MAIN_PLANT_CD, string SUB_SUPP_CD, string SUB_PLANT_CD, string SUPP_NAME)
        {
            string[] r = null;
            SubSupplier SS = new SubSupplier();
            SS.MAIN_SUPP_CD = MAIN_SUPP_CD;
            SS.MAIN_PLANT_CD = MAIN_PLANT_CD;
            SS.SUB_SUPP_CD = SUB_SUPP_CD;
            SS.SUB_PLANT_CD = SUB_PLANT_CD;
            SS.SUPP_NAME = SUPP_NAME;
            SS.CREATED_BY = getpE_UserId.Username;
            string resultMessage = SS.SaveData(SS);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion

        //#region Function Edit -
        public ActionResult UpdateSubSupplier(string MAIN_SUPP_CD, string MAIN_PLANT_CD, string SUB_SUPP_CD, string SUB_PLANT_CD, string SUPP_NAME)
        {
            string[] r = null;
            SubSupplier SS = new SubSupplier();
            SS.MAIN_SUPP_CD = MAIN_SUPP_CD;
            SS.MAIN_PLANT_CD = MAIN_PLANT_CD;
            SS.SUB_SUPP_CD = SUB_SUPP_CD;
            SS.SUB_PLANT_CD = SUB_PLANT_CD;
            SS.SUPP_NAME = SUPP_NAME;
            SS.CHANGED_BY = getpE_UserId.Username;
            string resultMessage = SS.UpdateData(SS);
            
            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion

        //#region Function Edit -
        public ActionResult Sinchronize()
        {
            string[] r = null;
            SubSupplier SS = new SubSupplier();
            string resultMessage = SS.Sinchronize();

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1], processId = r[2] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        //#endregion
    }
}
