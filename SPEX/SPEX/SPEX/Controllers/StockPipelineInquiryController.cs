﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;
using NPOI.HSSF.UserModel;
using SPEX.Models.Combobox;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Controllers
{
    public class StockPipelineInquiryController : PageController
    {

        //Model Maintenance Stock Master
        mStockPipelineInquiry ms = new mStockPipelineInquiry();
        MessagesString messageError = new MessagesString();

        //Upload
        public string moduleID = "SPX18";
        public string functionID = "SPX180200";
        public const string UploadDirectory = "Content\\FileUploadResult";
        public const string TemplateFName = "UPLOAD_MAINTENANCE_STOCK_MASTER";

        public StockPipelineInquiryController()
        {
            Settings.Title = "Stock Transaction Inquiry";
        }

        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }


        public void GetCombobox()
        {
            
            mCombobox itemNotMandatory = new mCombobox();
            itemNotMandatory.COMBO_LABEL = "--All--";
            itemNotMandatory.COMBO_VALUE = "";

            List<mCombobox> ListPipeLineFlag = null;
            ListPipeLineFlag = ComboboxRepository.Instance.SystemMasterCode("STOCK_PIPELINE_INQUIRY_FLAG");
            ListPipeLineFlag.Insert(0, itemNotMandatory);
            ViewData["STOCK_PIPELINE_INQUIRY_FLAG"] = ListPipeLineFlag;

            List<mCombobox> ListShift = null;
            ListShift = ComboboxRepository.Instance.SystemMasterCode("SHIFT");
            ListShift.Insert(0, itemNotMandatory);
            ViewData["SHIFT"] = ListShift;

            List<mCombobox> ListSupplier = null;
            ListSupplier = ComboboxRepository.Instance.GetSupplier("SUPPLIER");
            ListSupplier.Insert(0, itemNotMandatory);
            ViewData["SUPPLIER_CD"] = ListSupplier;

            List<mCombobox> ListSubSupplier = null;
            ListSubSupplier = ComboboxRepository.Instance.GetSupplier("SUB_SUPPLIER");
            ListSubSupplier.Insert(0, itemNotMandatory);
            ViewData["SUB_SUPPLIER_CD"] = ListSubSupplier; 
        }

        public void getSubSupplier() {
            mCombobox itemNotMandatory = new mCombobox();

            List<mCombobox> ListSubSupplier = null;
            ListSubSupplier = ComboboxRepository.Instance.GetSupplier("SUB_SUPPLIER");
            ListSubSupplier.Insert(0, itemNotMandatory);
            ViewData["SUB_SUPPLIER_CD"] = ListSubSupplier;         
        }

        protected override void Startup()
        {

            //Call error message
            ViewBag.MSPX00001ERR = messageError.getMSPX00001ERR("Maintenance Stock Master");
            ViewBag.MSPX00006ERR = messageError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = messageError.getMSPX00009ERR();
            /*No data found*/
            ViewBag.MSPXS2006ERR = messageError.getMsgText("MSPXS2006ERR");
            /*{0} should not be empty.*/
            ViewBag.MSPXS2005ERR = messageError.getMsgText("MSPXS2005ERR");
            /*A single record must be selected.*/
            ViewBag.MSPXS2017ERR = messageError.getMsgText("MSPXS2017ERR");
            /*Duplication found for {0}.*/
            ViewBag.MSPXS2011ERR = messageError.getMsgText("MSPXS2011ERR");
            /*{0} is not exists in  {1}*/
            ViewBag.MSPXS2010ERR = messageError.getMsgText("MSPXS2010ERR");
            /*Cannot find template file {0} on folder {1}.*/
            ViewBag.MSPXS2034ERR = messageError.getMsgText("MSPXS2034ERR");
            /*Error writing file = {0}. Reason = {1}.*/
            ViewBag.MSPXS2035ERR = messageError.getMsgText("MSPXS2035ERR");
            /*Invalid data type of {0}. Should be {1}.*/
            ViewBag.MSPXS2012ERR = messageError.getMsgText("MSPXS2012ERR");
            /*Are you sure you want to confirm the operation?*/
            ViewBag.MSPXS2018INF = messageError.getMsgText("MSPXS2018INF");
            /*Are you sure you want to abort the operation?*/
            ViewBag.MSPXS2019INF = messageError.getMsgText("MSPXS2019INF");
            /*Are you sure you want to delete the record?*/
            ViewBag.MSPXS2020INF = messageError.getMsgText("MSPXS2020INF");
            /*Are you sure you want to download this data?*/
            ViewBag.MSPXS2021INF = messageError.getMsgText("MSPXS2021INF");
            /*Cannot find reference file {0} on folder {1}.*/
            ViewBag.MSPXS2054ERR = messageError.getMsgText("MSPXS2054ERR");
            getpE_UserId = (User)ViewData["User"];
            GetCombobox();
            //ViewData["PRIVILEGEs"] = BuyerPD.GetDataBySystemMaster("General", "PRIVILEGE"); //BuyerPD.GetPrivilege();

        }

        public ActionResult StockPipelineInquiryCallBack(string pPART_NO, string pFLAG, string pSHIFT, string pSUPPLIER, string pSUB_SUPPLIER, string pDATE_FROM, string pDATE_TO, string Mode)
        {
            List<mStockPipelineInquiry> model = new List<mStockPipelineInquiry>();
            if (Mode.Equals("Search"))
                model = ms.getListPipeLineInquiry(pPART_NO, pFLAG, pSHIFT, pSUPPLIER, pSUB_SUPPLIER, pDATE_FROM, pDATE_TO);

            GetCombobox();
            return PartialView("StockPipelineInquiryGrid", model);
        }


        //function download 
        public ActionResult DownloadStockPipelineInquiry(object sender, EventArgs e, string pPART_NO, string pFLAG, string pSHIFT, string pSUPPLIER, string pSUB_SUPPLIER, string pDATE_FROM, string pDATE_TO)
        {
            string rExtract = string.Empty;
            string filename = "StockPipelineInquiry.xls";
            string directory = "Template";
            string filesTmp = HttpContext.Request.MapPath("~/"+directory+"/"+filename);
            List<mStockPipelineInquiry> model = new List<mStockPipelineInquiry>();
            model = ms.getListPipeLineInquiry(pPART_NO, pFLAG, pSHIFT, pSUPPLIER, pSUB_SUPPLIER, pDATE_FROM, pDATE_TO);


            #region ValidationChecking
            FileStream ftmp = null;
            int lError = 0;
            string status = string.Empty;
            FileInfo info = new FileInfo(filesTmp);
            if (!info.Exists)
            {
                lError = 1;
                status = "File";
                rExtract = directory + "|" + filename;    
            }else if(model.Count() == 0){
                lError = 1;
                status = "Data";
                rExtract = model.Count().ToString();
            }else if (info.Exists)
            {
                try{ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);}
                catch{}
                if (ftmp == null)
                {
                    lError = 1;
                    status = "Write";
                    rExtract = directory + "|" + filename;
                }
            }
            if(lError == 1)
                return Json(new { success = false, messages = rExtract, status = status }, JsonRequestBehavior.AllowGet);
            #endregion

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp, true);
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;

            ISheet sheet = workbook.GetSheet("StockPipelineInquiry");
            string date = DateTime.Now.ToString("ddMMyyyy");
            filename = "Stock_Pipeline_" + date + ".xls";

            string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            sheet.GetRow(5).GetCell(2).SetCellValue(dateNow);
            sheet.GetRow(6).GetCell(2).SetCellValue(getpE_UserId.Username);
            
            model = ms.getListPipeLineInquiry(pPART_NO, pFLAG, pSHIFT, pSUPPLIER, pSUB_SUPPLIER, pDATE_FROM, pDATE_TO);


            sheet.GetRow(8).GetCell(2).SetCellValue(String.IsNullOrEmpty(pPART_NO) ? "ALL" : pPART_NO);
            sheet.GetRow(9).GetCell(2).SetCellValue(String.IsNullOrEmpty(pSUPPLIER) ? "ALL" : pSUPPLIER);
            sheet.GetRow(10).GetCell(2).SetCellValue(String.IsNullOrEmpty(pSUB_SUPPLIER) ? "ALL" : pSUB_SUPPLIER);

            sheet.GetRow(8).GetCell(6).SetCellValue((String.IsNullOrEmpty(pDATE_FROM) ? "-" : pDATE_FROM) + " To " + (String.IsNullOrEmpty(pDATE_TO) ? "-" : pDATE_TO));
            sheet.GetRow(9).GetCell(6).SetCellValue(String.IsNullOrEmpty(pFLAG) ? "ALL" : pFLAG);
            sheet.GetRow(10).GetCell(6).SetCellValue(String.IsNullOrEmpty(pSHIFT) ? "ALL" : pSHIFT);


            int row = 14;
            int rowNum = 1;
            IRow Hrow;



            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);
                
                Hrow.CreateCell(1).SetCellValue(rowNum);
                Hrow.CreateCell(2).SetCellValue(result.DATE.ToString());
                Hrow.CreateCell(3).SetCellValue(result.SEQ);
                Hrow.CreateCell(4).SetCellValue(result.PART_NO);
                Hrow.CreateCell(5).SetCellValue(result.SHIFT);
                Hrow.CreateCell(6).SetCellValue(result.PART_NAME.ToString());
                Hrow.CreateCell(7).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(8).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(9).SetCellValue(result.SUP_NAME);
                Hrow.CreateCell(10).SetCellValue(result.BEGIN_STOCK);
                Hrow.CreateCell(11).SetCellValue(result.ADJUST.ToString());
                Hrow.CreateCell(12).SetCellValue(result.BINNING.ToString());
                Hrow.CreateCell(13).SetCellValue(result.PICKING.ToString());
                Hrow.CreateCell(14).SetCellValue(result.END_STOCK);
                Hrow.CreateCell(15).SetCellValue(result.REF_FILE);
                


                Hrow.GetCell(1).CellStyle = styleContent3;
                Hrow.GetCell(2).CellStyle = styleContent;
                Hrow.GetCell(3).CellStyle = styleContent3;
                Hrow.GetCell(4).CellStyle = styleContent;
                Hrow.GetCell(5).CellStyle = styleContent2;
                Hrow.GetCell(6).CellStyle = styleContent;
                Hrow.GetCell(7).CellStyle = styleContent2;
                Hrow.GetCell(8).CellStyle = styleContent2;
                Hrow.GetCell(9).CellStyle = styleContent;
                Hrow.GetCell(10).CellStyle = styleContent3;
                Hrow.GetCell(11).CellStyle = styleContent3;
                Hrow.GetCell(12).CellStyle = styleContent3;
                Hrow.GetCell(13).CellStyle = styleContent3;
                Hrow.GetCell(14).CellStyle = styleContent3;
                Hrow.GetCell(15).CellStyle = styleContent;


                row++;
                rowNum++;
            }

            MemoryStream memory = new MemoryStream();
            workbook.Write(memory);
            ftmp.Close();
            Response.BinaryWrite(memory.ToArray());
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));

            return Json(new { success = true, messages = "SUCCESS" }, JsonRequestBehavior.AllowGet);
        }


    }
}
