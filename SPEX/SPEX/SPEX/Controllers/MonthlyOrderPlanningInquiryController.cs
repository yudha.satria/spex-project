﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.Mvc;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SPEX.Cls;
using SPEX.Models;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace SPEX.Controllers
{
    public class MonthlyOrderPlanningInquiryController : PageController
    {
        MessagesString msgError = new MessagesString();
        public string moduleID = "SPX01";
        public string functionID = "SPX010100";
        public const string UploadDirectory = "Content\\FileUploadResult";
        public const string TemplateFName = "MOP_Inquiry";
        //add agi 2017-11-30
        public const string tb_t_revise_name = "spex.TB_T_LPOP_SPEX ";
        public User getpE_UserId
        {
            get
            {
                if (Session["getpE_UserId"] != null)
                {
                    return (User)Session["getpE_UserId"];
                }
                else return null;
            }
            set
            {
                Session["getpE_UserId"] = value;
            }
        }

        public MonthlyOrderPlanningInquiryController()
        {
            Settings.Title = "Montly Order Planning Inquiry";
        }

        protected override void Startup()
        {
            ViewBag.MSPX00001ERR = msgError.getMSPX00001ERR("MOP Inquiry");
            ViewBag.MSPX00006ERR = msgError.getMSPX00006ERR();
            ViewBag.MSPX00009ERR = msgError.getMSPX00009ERR();
            getpE_UserId = (User)ViewData["User"];
            ComboSupplier();
            ComboSubSupplier();
            ComboTiming();
            ComboSendSts();
            ComboPMSPFlag();
            ComboReorder();
        }
        
        #region Get ComboBox Data
        public void ComboSupplier()
        {
            List<string> model = new List<string>();

            model = Common.GetSupplierCDPlantList();
            //model = model.Select(str => str.Replace("-", ":")).ToList();
            model[0] = "--ALL--";
            ViewData["MOP_SUPPLIER"] = model;
        }
        public void ComboSubSupplier()
        {
            List<string> model = new List<string>();

            model = Common.GetSubSupplierCDPlantList();
            //model = model.Select(str => str.Replace("-", ":")).ToList();
            model[0] = "--ALL--";
            ViewData["MOP_SUB_SUPPLIER"] = model;
        }

        public void ComboTiming()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--ALL--";

            model = (new mSystemMaster()).GetComboBoxList("MOP_TIMING");
            model.ForEach(i => {
                i.SYSTEM_VALUE = i.SYSTEM_CD + ":" + i.SYSTEM_VALUE;
            });
            model.Insert(0, item);
            ViewData["MOP_TIMING"] = model;
        }

        public void ComboSendSts()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--ALL--";

            model = (new mSystemMaster()).GetComboBoxList("MOP_SEND_STS").OrderByDescending(i => i.SYSTEM_VALUE).ToList();
            model.Insert(0, item);
            ViewData["MOP_SEND_STS"] = model;
        }
        public void ComboPMSPFlag()
        {
            List<mSystemMaster> model = new List<mSystemMaster>();
            mSystemMaster item = new mSystemMaster();
            item.SYSTEM_CD = "";
            item.SYSTEM_VALUE = "--ALL--";

            model = (new mSystemMaster()).GetComboBoxList("MOP_PMSP_FLAG").OrderByDescending(i => i.SYSTEM_VALUE).ToList();
            model.Insert(0, item);
            ViewData["MOP_PMSP_FLAG"] = model;
        }

        public void ComboReorder()
        {
            List<String> model = new List<String>();
            mMOPInquiry MOP = new mMOPInquiry();

            model = MOP.GetReorderFlagList();
            model[0] = "--ALL--";
            ViewData["MOP_REORDER"] = model;
        }
        #endregion

        public ActionResult GetMOPInquiryCallBack(string MONTH, string PART_NUMBER, string SUPPLIER_CD, string SUPPLIER_PLANT, string SUB_SUPPLIER_CD, string SUB_SUPPLIER_PLANT, string PMSP_FLAG, string REORDER_FLAG_SEARCH, string TIMING, string SEND_STATUS, string Mode)
        {
            List<mMOPInquiry> model = new List<mMOPInquiry>();
            mMOPInquiry MOP = new mMOPInquiry();
            MOP.PACK_MONTH = MONTH;
            MOP.PART_NO = PART_NUMBER;
            MOP.SUPPLIER_CD = SUPPLIER_CD;
            MOP.SUPPLIER_PLANT = SUPPLIER_PLANT;
            MOP.SUB_SUPPLIER_CD = SUB_SUPPLIER_CD;
            MOP.SUB_SUPPLIER_PLANT = SUB_SUPPLIER_PLANT;
            MOP.PMSP_FLAG = PMSP_FLAG;
            MOP.REORDER_FLAG = REORDER_FLAG_SEARCH;
            MOP.VERS = TIMING;
            MOP.SEND_FLAG = SEND_STATUS;

            model = MOP.GetData(MOP, Mode);
            return PartialView("_PartialGrid", model);
        }

        public ActionResult UpdateMOPInquiry(string p_PACK_MONTH, string p_PART_NO, string p_SUPPLIER_CD, string p_SUPPLIER_PLANT, string p_DOCK_CD, string p_N_VOLUME, string p_N_1_VOLUME, string p_N_2_VOLUME, string p_N_3_VOLUME)
        {
            mMOPInquiry MOP = new mMOPInquiry();
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string UserID = getpE_UserId.Username;
            string[] r = null;
            string resultMessage = MOP.UpdateData(p_PACK_MONTH, p_PART_NO, p_SUPPLIER_CD, p_SUPPLIER_PLANT, p_DOCK_CD, p_N_VOLUME, p_N_1_VOLUME, p_N_2_VOLUME, p_N_3_VOLUME, UserID);

            r = resultMessage.Split('|');
            if (r[0] == "Error ")
            {
                return Json(new { success = "false", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "true", messages = r[1] }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public JsonResult CalcMOPInquiry()
        {
            mMOPInquiry MOP = new mMOPInquiry();
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;
            String result = MOP.CalcMOPInquiry(UserID);
            r = result.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2].Trim();
                msg = msgError.getMSPX00300INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2].Trim();
                msg = msgError.getMSPX00301INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult MOPSendToIPPCS()
        {
            mMOPInquiry MOP = new mMOPInquiry();
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;
            String result = MOP.MOPSendToIPPCS(UserID);
            r = result.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2].Trim();
                msg = msgError.getMSPX00302INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2].Trim();
                msg = msgError.getMSPX00303INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CallbackUpload()
        {
            UploadControlExtension.GetUploadedFiles("UploadMOP", UploadControlHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public class UploadControlHelper
        {
            public const string UploadDirectory = "Content/FileUploadResult";
            public const string TemplateFName = "TemplatePackingPartMaster";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xlsx" },
                MaxFileSize = 20971520
            };
        }

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            mMOPInquiry MOP = new mMOPInquiry();
            String rExtract = String.Empty;
         
            string resultFilePath = UploadDirectory + TemplateFName + Path.GetExtension(e.UploadedFile.FileName);
            string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

            //lokasi path di server untuk file yang di upload
            var pathfile = Path.Combine(updir, TemplateFName + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName));

            //save file upload sesuai lokasi path di server
            e.UploadedFile.SaveAs(pathfile);

            //Function untuk nge-extract file excel
            //rExtract = Upload.EXcelToSQL(pathfile, tb_t_name);
            rExtract = ReadExcelData(pathfile);
           
            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract;
        }

        private string ReadExcelData(string FileName)
        {
            try
            {
                XSSFWorkbook xssfworkbook = new XSSFWorkbook(FileName);
                ISheet sheet = xssfworkbook.GetSheetAt(0);
                int lastrow = sheet.LastRowNum;
                string partList = String.Empty;
                int index = 1;

                String part;
                if (lastrow > 0)
                {
                    for (int i = index; i <= lastrow; i++)
                    {
                        part = String.Empty;
                        IRow row = sheet.GetRow(i);

                        if (row == null) continue;
                        if (row.GetCell(0) == null) continue;

                        if (row.GetCell(0) == null) part = "";
                        else if (row.GetCell(0).CellType == CellType.Blank || (row.GetCell(0).CellType == CellType.String && (row.GetCell(0).StringCellValue == null || row.GetCell(0).StringCellValue == "")))
                            part = "";
                        else if (row.GetCell(0).CellType == CellType.Numeric && (row.GetCell(0).NumericCellValue.ToString() == ""))
                            part = "";
                        else if (row.GetCell(0).CellType == CellType.String)
                            part = row.GetCell(0).StringCellValue;
                        else if (row.GetCell(0).CellType == CellType.Numeric)
                        {
                            ICell cell = row.GetCell(0);
                            string formatString = row.GetCell(0).CellStyle.GetDataFormatString();

                            //string value = dataFormatter.FormatCellValue(cell);
                            if (formatString.ToLower().Contains("y") || formatString.ToLower().Contains("d") || formatString.ToLower().Contains("m"))
                                part = row.GetCell(0).DateCellValue.ToString(Common.GetNewDateFormatFromExcel(formatString));
                            else
                                part = row.GetCell(0).NumericCellValue.ToString();
                        }
                        else
                            part = row.GetCell(0).ToString();

                        partList += part + (part.Trim() != "" ? (i == lastrow ? "" : ";") : "");
                    }
                }
                else
                    partList = "null";

                return partList;
            }
            catch (Exception ex)
            {
                return  "1|" + ex.Message;
            }
        }

        public void Download(string p_MONTH
            , string p_REORDER
            , string p_SUPPLIER
            , string p_S_PLANT_CD
            , string p_SUB_SUPPLIER_CD
            , string p_SUB_SUPPLIER_PLANT
            , string p_PMSP_FLAG
            , string p_TIMMING
            , string p_PART_NO
            , string p_SEND_STATUS)
        {
            mMOPInquiryDownload MOP = new mMOPInquiryDownload();
            string filename = "";
            string filesTmp = HttpContext.Request.MapPath("~/Template/MOPDownload.xlsx");
            FileStream ftmp = new FileStream(filesTmp, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(ftmp);

            //setting style
            ICellStyle styleContent = workbook.CreateCellStyle();
            styleContent.VerticalAlignment = VerticalAlignment.Top;
            styleContent.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent.Alignment = HorizontalAlignment.Left;
            styleContent.WrapText = true;

            ICellStyle styleContent2 = workbook.CreateCellStyle();
            styleContent2.VerticalAlignment = VerticalAlignment.Top;
            styleContent2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent2.Alignment = HorizontalAlignment.Center;
            styleContent.WrapText = true;

            ICellStyle styleContent3 = workbook.CreateCellStyle();
            styleContent3.VerticalAlignment = VerticalAlignment.Top;
            styleContent3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            styleContent3.Alignment = HorizontalAlignment.Right;
            styleContent.WrapText = true;

            ISheet sheet = workbook.GetSheet("MOP");
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPEX_MOP_" + date + ".xlsx";

            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            string dateNow = DateTime.Now.ToString();

            sheet.GetRow(3).GetCell(4).SetCellValue(": " + p_MONTH);
            sheet.GetRow(3).GetCell(10).SetCellValue(": " + ((p_SUPPLIER + "-" + p_S_PLANT_CD).Equals("-") ? "ALL" : (p_SUPPLIER + "-" + p_S_PLANT_CD)));
            sheet.GetRow(3).GetCell(16).SetCellValue(": " + (p_TIMMING.Equals("F") ? "FIRM" : p_TIMMING.Equals("T") ? "TENTATIVE" : "ALL"));
            sheet.GetRow(3).GetCell(23).SetCellValue(": " + (p_PART_NO.Equals(String.Empty) ? "ALL" : p_PART_NO));
            sheet.GetRow(3).GetCell(30).SetCellValue(": " + dateNow);
            sheet.GetRow(4).GetCell(4).SetCellValue(": " + (p_REORDER.Equals("0") ? "N" : p_REORDER.Equals("1") ? "Y" : "ALL"));
            sheet.GetRow(4).GetCell(10).SetCellValue(": " + ((p_SUB_SUPPLIER_CD + "-" + p_SUB_SUPPLIER_PLANT).Equals("-") ? "ALL" : (p_SUB_SUPPLIER_CD + "-" + p_SUB_SUPPLIER_PLANT)));
            sheet.GetRow(4).GetCell(16).SetCellValue(": " + (p_PMSP_FLAG.Equals("0") ? "N" : p_PMSP_FLAG.Equals("1") ? "Y" : "ALL"));
            sheet.GetRow(4).GetCell(23).SetCellValue(": " + (p_SEND_STATUS.Equals("0") ? "N" : p_SEND_STATUS.Equals("1") ? "Y" : "ALL"));
            sheet.GetRow(4).GetCell(30).SetCellValue(": " + getpE_UserId.Username);

            int row = 7;
            int rowNum = 1;
            IRow Hrow;

            List<mMOPInquiryDownload> model = new List<mMOPInquiryDownload>();

            model = MOP.getDownload(p_MONTH, p_REORDER, p_SUPPLIER, p_S_PLANT_CD, p_SUB_SUPPLIER_CD, p_SUB_SUPPLIER_PLANT, p_PMSP_FLAG, p_TIMMING, p_PART_NO, p_SEND_STATUS);

            foreach (var result in model)
            {
                Hrow = sheet.CreateRow(row);

                Hrow.CreateCell(0).SetCellValue(rowNum);
                Hrow.CreateCell(1).SetCellValue(result.PACK_MONTH);
                Hrow.CreateCell(2).SetCellValue(result.PART_NO);
                Hrow.CreateCell(3).SetCellValue(result.PART_NAME);
                Hrow.CreateCell(4).SetCellValue(result.SUPPLIER_CD);
                Hrow.CreateCell(5).SetCellValue(result.S_PLANT_CD);
                
                Hrow.CreateCell(6).SetCellValue(result.SUB_SUPPLIER_CD);
                Hrow.CreateCell(7).SetCellValue(result.SUB_SUPPLIER_PLANT);
                Hrow.CreateCell(8).SetCellValue(result.SUPPLIER_NAME);
                Hrow.CreateCell(9).SetCellValue(result.PMSP_FLAG);

                Hrow.CreateCell(10).SetCellValue(result.N_VOLUME.ToString());
                Hrow.CreateCell(11).SetCellValue(result.N_FREQ.ToString());
                Hrow.CreateCell(12).SetCellValue(result.N_1_VOLUME.ToString());
                Hrow.CreateCell(13).SetCellValue(result.N_1_FREQ.ToString());
                Hrow.CreateCell(14).SetCellValue(result.N_2_VOLUME.ToString());
                Hrow.CreateCell(15).SetCellValue(result.N_2_FREQ.ToString());
                Hrow.CreateCell(16).SetCellValue(result.N_3_VOLUME.ToString());
                Hrow.CreateCell(17).SetCellValue(result.N_3_FREQ.ToString());

                Hrow.CreateCell(18).SetCellValue(result.N_D01.ToString());
                Hrow.CreateCell(19).SetCellValue(result.N_D02.ToString());
                Hrow.CreateCell(20).SetCellValue(result.N_D03.ToString());
                Hrow.CreateCell(21).SetCellValue(result.N_D04.ToString());
                Hrow.CreateCell(22).SetCellValue(result.N_D05.ToString());
                Hrow.CreateCell(23).SetCellValue(result.N_D06.ToString());
                Hrow.CreateCell(24).SetCellValue(result.N_D07.ToString());
                Hrow.CreateCell(25).SetCellValue(result.N_D08.ToString());
                Hrow.CreateCell(26).SetCellValue(result.N_D09.ToString());
                Hrow.CreateCell(27).SetCellValue(result.N_D10.ToString());
                Hrow.CreateCell(28).SetCellValue(result.N_D11.ToString());
                Hrow.CreateCell(29).SetCellValue(result.N_D12.ToString());
                Hrow.CreateCell(30).SetCellValue(result.N_D13.ToString());
                Hrow.CreateCell(31).SetCellValue(result.N_D14.ToString());
                Hrow.CreateCell(32).SetCellValue(result.N_D15.ToString());
                Hrow.CreateCell(33).SetCellValue(result.N_D16.ToString());
                Hrow.CreateCell(34).SetCellValue(result.N_D17.ToString());
                Hrow.CreateCell(35).SetCellValue(result.N_D18.ToString());
                Hrow.CreateCell(36).SetCellValue(result.N_D19.ToString());
                Hrow.CreateCell(37).SetCellValue(result.N_D20.ToString());
                Hrow.CreateCell(38).SetCellValue(result.N_D21.ToString());
                Hrow.CreateCell(39).SetCellValue(result.N_D22.ToString());
                Hrow.CreateCell(40).SetCellValue(result.N_D23.ToString());
                Hrow.CreateCell(41).SetCellValue(result.N_D24.ToString());
                Hrow.CreateCell(42).SetCellValue(result.N_D25.ToString());
                Hrow.CreateCell(43).SetCellValue(result.N_D26.ToString());
                Hrow.CreateCell(44).SetCellValue(result.N_D27.ToString());
                Hrow.CreateCell(45).SetCellValue(result.N_D28.ToString());
                Hrow.CreateCell(46).SetCellValue(result.N_D29.ToString());
                Hrow.CreateCell(47).SetCellValue(result.N_D30.ToString());
                Hrow.CreateCell(48).SetCellValue(result.N_D31.ToString());

                Hrow.CreateCell(49).SetCellValue(result.N_1_D01.ToString());
                Hrow.CreateCell(50).SetCellValue(result.N_1_D02.ToString());
                Hrow.CreateCell(51).SetCellValue(result.N_1_D03.ToString());
                Hrow.CreateCell(52).SetCellValue(result.N_1_D04.ToString());
                Hrow.CreateCell(53).SetCellValue(result.N_1_D05.ToString());
                Hrow.CreateCell(54).SetCellValue(result.N_1_D06.ToString());
                Hrow.CreateCell(55).SetCellValue(result.N_1_D07.ToString());
                Hrow.CreateCell(56).SetCellValue(result.N_1_D08.ToString());
                Hrow.CreateCell(57).SetCellValue(result.N_1_D09.ToString());
                Hrow.CreateCell(58).SetCellValue(result.N_1_D10.ToString());
                Hrow.CreateCell(59).SetCellValue(result.N_1_D11.ToString());
                Hrow.CreateCell(60).SetCellValue(result.N_1_D12.ToString());
                Hrow.CreateCell(61).SetCellValue(result.N_1_D13.ToString());
                Hrow.CreateCell(62).SetCellValue(result.N_1_D14.ToString());
                Hrow.CreateCell(63).SetCellValue(result.N_1_D15.ToString());
                Hrow.CreateCell(64).SetCellValue(result.N_1_D16.ToString());
                Hrow.CreateCell(65).SetCellValue(result.N_1_D17.ToString());
                Hrow.CreateCell(66).SetCellValue(result.N_1_D18.ToString());
                Hrow.CreateCell(67).SetCellValue(result.N_1_D19.ToString());
                Hrow.CreateCell(68).SetCellValue(result.N_1_D20.ToString());
                Hrow.CreateCell(69).SetCellValue(result.N_1_D21.ToString());
                Hrow.CreateCell(70).SetCellValue(result.N_1_D22.ToString());
                Hrow.CreateCell(71).SetCellValue(result.N_1_D23.ToString());
                Hrow.CreateCell(72).SetCellValue(result.N_1_D24.ToString());
                Hrow.CreateCell(73).SetCellValue(result.N_1_D25.ToString());
                Hrow.CreateCell(74).SetCellValue(result.N_1_D26.ToString());
                Hrow.CreateCell(75).SetCellValue(result.N_1_D27.ToString());
                Hrow.CreateCell(76).SetCellValue(result.N_1_D28.ToString());
                Hrow.CreateCell(77).SetCellValue(result.N_1_D29.ToString());
                Hrow.CreateCell(78).SetCellValue(result.N_1_D30.ToString());
                Hrow.CreateCell(79).SetCellValue(result.N_1_D31.ToString());

                Hrow.CreateCell(80).SetCellValue(result.R_PLANT_CD);
                Hrow.CreateCell(81).SetCellValue(result.DOCK_CD);
                Hrow.CreateCell(82).SetCellValue(result.ORD_TYPE);
                Hrow.CreateCell(83).SetCellValue(result.ORDER_TYPE);
                Hrow.CreateCell(84).SetCellValue(result.KANBAN_NUMBER);
                Hrow.CreateCell(85).SetCellValue(result.CREATED_BY);
                Hrow.CreateCell(86).SetCellValue(String.Format("{0:yyyyMMdd}", result.CREATED_DT));
                Hrow.CreateCell(87).SetCellValue(result.CHANGED_BY);
                Hrow.CreateCell(88).SetCellValue(String.Format("{0:yyyyMMdd}", result.CHANGED_DT));
                Hrow.CreateCell(89).SetCellValue(result.REORDER_FLAG);
                Hrow.CreateCell(90).SetCellValue(result.ADJUST_FLAG);
                Hrow.CreateCell(91).SetCellValue(result.ADJUST_BY);
                Hrow.CreateCell(92).SetCellValue(String.Format("{0:yyyyMMdd}", result.ADJUST_DT));

                for (int i = 0; i <= 92; i++)
                {
                    Hrow.GetCell(i).CellStyle = styleContent;
                }

                row++;
                rowNum++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            ftmp.Close();
            Response.BinaryWrite(ms.ToArray());
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
        }

        #region Export to CSV
        public void DownloadNQC(string p_MONTH
            , string p_SUPPLIER
            , string p_SUB_SUPPLIER
            , string p_PMSP_FLAG
            , string p_TIMMING
            , string p_PART_NO
            , string p_SEND_STATUS)
        {
            string filename = "";
            mMOPInquiryDownload MOP = new mMOPInquiryDownload();
            string date = DateTime.Now.ToString("yyyyMMddhhmmss");
            filename = "SPEX_MOP_NQC_" + date + ".csv";

            //string dateNow = DateTime.Now.ToString("dd.MM.yyyy");
            string dateNow = DateTime.Now.ToString();

            List<mMOPInquiryDownload> model = new List<mMOPInquiryDownload>();

            var p_SUPPLIER_CD = "";
            var p_S_PLANT_CD = "";

            if (!p_SUPPLIER.Equals(""))
            {
                p_SUPPLIER_CD = p_SUPPLIER.Split('-')[0];
                p_S_PLANT_CD = p_SUPPLIER.Split('-')[1];
            }

            var p_SUB_SUPPLIER_CD = "";
            var p_SUB_SUPPLIER_PLANT = "";

            if (!p_SUB_SUPPLIER.Equals(""))
            {
                p_SUB_SUPPLIER_CD = p_SUB_SUPPLIER.Split('-')[0];
                p_SUB_SUPPLIER_PLANT = p_SUB_SUPPLIER.Split('-')[1];
            }

            model = MOP.getDownloadNQC(p_MONTH, p_SUPPLIER_CD, p_S_PLANT_CD, p_SUB_SUPPLIER_CD, p_SUB_SUPPLIER_PLANT, p_PMSP_FLAG, p_TIMMING, p_PART_NO, p_SEND_STATUS);

            StringBuilder gr = new StringBuilder("");

            if (model.Count > 0)
            {
                for (int i = 0; i < CsvCol.Count; i++)
                {
                    gr.Append(Quote(CsvCol[i].Text)); gr.Append(CSV_SEP);
                }

                gr.Append(Environment.NewLine);

                foreach (mMOPInquiryDownload d in model)
                {
					string vPackMonth = "";
					int vYear;
					int vMonth;

					string v29 = "";
					string v30 = "";
					string v31 = "";

					vPackMonth = d.PACK_MONTH;
					vYear = Convert.ToInt16(vPackMonth.Substring(0,4));
					vMonth = Convert.ToInt16(vPackMonth.Substring(4,2));
					int days = DateTime.DaysInMonth(vYear, vMonth);

					if (days < 31)
					{
						v31 = "-";
						if (days < 30)
						{
							v30 = "-";
							if (days < 29)
							{
								v29 = "-";								
							} else {
								v29 = d.N_D29.ToString("0.#");
							}
						} else {
							v29 = d.N_D29.ToString("0.#");
							v30 = d.N_D30.ToString("0.#");
						}
					} else {
						v29 = d.N_D29.ToString("0.#");
						v30 = d.N_D30.ToString("0.#");
						v31 = d.N_D31.ToString("0.#");
					}

                    csvAddLine(gr, new string[] {
                    Equs(d.UPDATE_FLAG), 
                    Equs(d.SUPPLIER_CD), 
                    Equs(d.S_PLANT_CD),
                    Equs(d.S_DOCK_CD),
                    Equs(d.COMP_CD), 
                    Equs(d.R_PLANT_CD),
                    Equs(d.DOCK_CD), 
                    Equs(d.PART_NO),
                    Equs(d.ID_LINE), 
                    Equs(d.PACK_MONTH),
                    d.N_D01.ToString("0.#"), 
                    d.N_D02.ToString("0.#"),
                    d.N_D03.ToString("0.#"),
                    d.N_D04.ToString("0.#"),
                    d.N_D05.ToString("0.#"),
                    d.N_D06.ToString("0.#"),
                    d.N_D07.ToString("0.#"),
                    d.N_D08.ToString("0.#"),
                    d.N_D09.ToString("0.#"),
                    d.N_D10.ToString("0.#"),
                    d.N_D11.ToString("0.#"),
                    d.N_D12.ToString("0.#"),
                    d.N_D13.ToString("0.#"),
                    d.N_D14.ToString("0.#"),
                    d.N_D15.ToString("0.#"),
                    d.N_D16.ToString("0.#"),
                    d.N_D17.ToString("0.#"),
                    d.N_D18.ToString("0.#"),
                    d.N_D19.ToString("0.#"),
                    d.N_D20.ToString("0.#"),
                    d.N_D21.ToString("0.#"),
                    d.N_D22.ToString("0.#"),
                    d.N_D23.ToString("0.#"),
                    d.N_D24.ToString("0.#"),
                    d.N_D25.ToString("0.#"),
                    d.N_D26.ToString("0.#"),
                    d.N_D27.ToString("0.#"),
                    d.N_D28.ToString("0.#"),
                    v29,
                    v30,
                    v31});
                }
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", filename));
            Response.Write(gr.ToString());
            Response.End();
        }

        private string _csv_sep;
        private string CSV_SEP
        {
            get
            {
                if (string.IsNullOrEmpty(_csv_sep))
                {
                    _csv_sep = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    if (string.IsNullOrEmpty(_csv_sep)) _csv_sep = ";";
                }
                return _csv_sep;
            }
        }

        private List<CsvColumn> CsvCol = CsvColumn.Parse(
            "UpdateFlag|C|1|0|UPDATE_FLAG;" +
            "Supplier Logistics Point Code(*)|C|6|1|SUPPLIER_CD;" +
            "Supplier Plant Code|C|10|0|S_PLANT_CD;" +
            "Supplier Dock Code|C|5|0|S_DOCK_CD;" +
            "Receiving Company Logistics Point Code(*)|C|23|0|COMP_CD;" +
            "Receiving Company Plant Code|C|23|1|R_PLANT_CD;" +
            "Receiving Company Dock Code|C|5|0|DOCK_CD;" +
            "PartNo(*)|C|1|0|PART_NO;" +
            "IDLine|C|2|1|ID_LINE;" +
            "YM(*)|C|40|0|PACK_MONTH;" +
            "1st|C|10|0|N_D01;" +
            "2nd|C|4|0|N_D02;" +
            "3rd|C|4|0|N_D03;" +
            "4th|C|16|1|N_D04;" +
            "5th|C|16|1|N_D05;" +
            "6th|C|16|1|N_D06;" +
            "7th|C|16|1|N_D07;" +
            "8th|C|16|1|N_D08;" +
            "9th|C|16|1|N_D09;" +
            "10th|C|16|1|N_D10;" +
            "11th|C|16|1|N_D11;" +
            "12th|C|16|1|N_D12;" +
            "13th|C|16|1|N_D13;" +
            "14th|C|16|1|N_D14;" +
            "15th|C|16|1|N_D15;" +
            "16th|C|16|1|N_D16;" +
            "17th|C|16|1|N_D17;" +
            "18th|C|16|1|N_D18;" +
            "19th|C|16|1|N_D19;" +
            "20th|C|16|1|N_D20;" +
            "21st|C|16|1|N_D21;" +
            "22nd|C|16|1|N_D22;" +
            "23rd|C|16|1|N_D23;" +
            "24th|C|16|1|N_D24;" +
            "25th|C|16|1|N_D25;" +
            "26th|C|16|1|N_D26;" +
            "27th|C|16|1|N_D27;" +
            "28th|C|16|1|N_D28;" +
            "29th|C|16|1|N_D29;" +
            "30th|C|16|1|N_D30;" +
            "31st|C|16|1|N_D31;");

        public void csvAddLine(StringBuilder b, string[] values)
        {
            string SEP = CSV_SEP;
            for (int i = 0; i < values.Length - 1; i++)
            {
                b.Append(values[i]); b.Append(SEP);
            }
            b.Append(values[values.Length - 1]);
            b.Append(Environment.NewLine);
        }

        private string Quote(string s)
        {
            return s != null ? "\"" + s + "\"" : s;
        }

        /// <summary>
        /// put Equal Sign in front and quote char to prevent 'numerization' by Excel
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string Equs(string s)
        {
            string separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (s != null)
            {
                if (s.Contains(separator))
                {
                    s = "\"" + s + "\"";
                }
                else
                {
                    s = "=\"" + s + "\"";
                }
            }
            return s;
        }

        public class CsvColumn
        {
            public string Text { get; set; }
            public string Column { get; set; }
            public string DataType { get; set; }
            public int Len { get; set; }
            public int Mandatory { get; set; }

            public static List<CsvColumn> Parse(string v)
            {
                List<CsvColumn> l = new List<CsvColumn>();

                string[] x = v.Split(';');


                for (int i = 0; i < x.Length; i++)
                {
                    string[] y = x[i].Split('|');
                    CsvColumn me = null;
                    if (y.Length >= 4)
                    {
                        int len = 0;
                        int mandat = 0;
                        int.TryParse(y[2], out len);
                        int.TryParse(y[3], out mandat);
                        me = new CsvColumn()
                        {
                            Text = y[0],
                            DataType = y[1],
                            Len = len,
                            Mandatory = mandat
                        };
                        l.Add(me);
                    }

                    if (y.Length > 4 && me != null)
                    {
                        me.Column = y[4];
                    }
                }
                return l;
            }
        }
        #endregion

        //add agi 2017-11-30
        public ActionResult CallbackUploadRevise()
        {
            UploadControlExtension.GetUploadedFiles("UploadMOPRevise", UploadControlHelperRevise.ValidationSettings, uc_FileUploadReviseComplete);
            return null;
        }

        public class UploadControlHelperRevise
        {
            //public const string UploadDirectory = "Content/FileUploadResult";
            //public const string TemplateFName = "TemplatePackingPartMaster";
            public static readonly ValidationSettings ValidationSettings = new ValidationSettings
            {
                AllowedFileExtensions = new string[] { ".csv" },
                MaxFileSize = 1120971520
            };
        }

        public void uc_FileUploadReviseComplete(object sender, FileUploadCompleteEventArgs e)
        {
            mMOPInquiry MOP = new mMOPInquiry();
            string rExtract = string.Empty;
            string fileNameUpload = Path.GetFileNameWithoutExtension(e.UploadedFile.FileName);
            long pid = 0;

            // MESSAGE_DETAIL = "Starting proces upload Price Master";
            Lock L = new Lock();
            string functionIDUpload = "SPX020400";
            int IsLock = L.is_lock(functionIDUpload, out pid);
            if (IsLock > 0)
            {
                rExtract = "3|" + msgError.getMSPX00112ERR(pid.ToString());
            }

            //checking file 
            else if (e.UploadedFile.IsValid && IsLock == 0)
            {
                rExtract = MOP.DeleteDataTBT();
                if (rExtract == "SUCCESS")
                {
                    string resultFile = fileNameUpload + "-" + string.Format("{0:ddmmyyyyhhmmss}", DateTime.Now) + "-" + getpE_UserId.Username + Path.GetExtension(e.UploadedFile.FileName);
                    string updir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, UploadDirectory);

                    //lokasi path di server untuk file yang di upload
                    var pathfile = Path.Combine(updir, resultFile);

                    //save file upload sesuai lokasi path di server
                    e.UploadedFile.SaveAs(pathfile);

                    //string tb_t_name = "spex.TB_T_DRIVER_DATA_CSV";
                    rExtract = Upload.BulkCopyCSV(pathfile, tb_t_revise_name, Convert.ToChar(Upload.GetDelimiter(e.UploadedFile.FileContent)));
                    rExtract = resultFile;
                }
                else
                {
                    rExtract = "1|" + rExtract;
                }
            }
            else
            {
                rExtract = "1| File is not .csv";
            }

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
                e.CallbackData = rExtract == "" ? "0|" : rExtract;
        }

        public ActionResult UploadReviseBackgroundProsess( String filename)
        {
            mMOPInquiry MOP = new mMOPInquiry();
            if (getpE_UserId == null)
                return Json(new { success = "false", messages = "Please re-login" }, JsonRequestBehavior.AllowGet);

            string[] r = null;
            string msg = "";
            string UserID = getpE_UserId.Username;

            string resultMessage = MOP.UploadReviseBackgroundProcess(UserID);
            r = resultMessage.Split('|');
            if (r[0].ToString().ToLower().Contains("error"))
            {
                string pID = r[2];
                msg = msgError.getMSPX00304INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>").Split('|')[2];
                return Json(new { success = "false", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string pID = r[2];
                msg = msgError.getMSPX00305INF("<a href='#' onclick='MessageShow(" + pID + "); return false;'>" + pID + "</a>");
                return Json(new { success = "true", messages = msg, process_id = pID }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}