﻿EXEC [spex].[SP_VESSEL_SCHED_INSERT_TEMP]
	@PACKING_COMPANY
    ,@BUYER_CD
    ,@FEEDER_VESSEL 
    ,@FEEDER_VOYAGE_NO 
    ,@CONNECTING_VESSEL 
    ,@CONNECT_VOYAGE_NO 
    ,@ETD_DATE 
    ,@SA 
    ,@VANNING_DATE 
    ,@ETA_DATE 
    ,@CONTAINER_PLAN 
    ,@FA 
    ,@BOOKING_NUMBER 
    ,@CLOSING_TIME 