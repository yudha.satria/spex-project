﻿Declare @@SQL_TEXT varchar(max),
		@@PART_NO varchar(max)= @0,
		@@PART_NAME varchar(max)= @1,
		@@FRANCHISE_CD varchar(max)= @2
		
set @@SQL_TEXT ='SELECT  
	   [PART_NO]
      ,[PART_NAME]
      ,[FRANCHISE_CD]
	  ,[RETAIL_PRICE]
      ,[USED_FLAG]
      ,[DELETION_FLAG]
      ,[RELEASE_STATUS]
      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[CHANGED_BY]
      ,[CHANGED_DT]
		--remaks agi 2017-01-06
		--FROM [SPIN].[spex].[TB_M_TOPAS_PART]
		FROM [spex].[TB_M_TOPAS_PART]
			 WHERE  1 = 1'

IF ( @@PART_NO <> '' ) 
	BEGIN 
	  SET @@SQL_TEXT = @@SQL_TEXT + ' AND PART_NO LIKE ''%' + @@PART_NO + '%'''
	END

IF ( @@PART_NAME <> '' ) 
	BEGIN 
	  SET @@SQL_TEXT = @@SQL_TEXT + ' AND PART_NAME LIKE ''%' + @@PART_NAME + '%'''
	END
	
IF ( @@FRANCHISE_CD <> '' ) 
	BEGIN 
	  SET @@SQL_TEXT = @@SQL_TEXT + ' AND FRANCHISE_CD LIKE ''%' + @@FRANCHISE_CD + '%'''
	END
	  
execute (@@SQL_TEXT)