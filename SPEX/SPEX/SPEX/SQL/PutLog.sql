﻿DECLARE @@PID BIGINT = @p_pid;
DECLARE @@tempTable TABLE(PROCESS_ID BIGINT); 

INSERT @@tempTable
EXEC	[spex].[SP_PUTLOG] @p_messages,
	@p_user,
	@p_location, 	
	@@PID OUTPUT ,
	@p_msg_id, 
	@p_msg_type,
	@p_module_id, 
	@p_function_id,
	@p_process_status

SELECT PROCESS_ID FROM @@tempTable;
