﻿--exec GetReadyCargoInquiry 
--@pBUYER_CD, 
--@pPD_CD,
--@pVANNING_DT_FROM,
--@pVANNING_DT_TO,
--@pInvoice_Status,
--@pINVOICE_NO,
--@pVANNING_NO,
--@pINVOICE_DT_FROM,
--@pINVOICE_DT_TO,

--@pETD_FROM,
--@pETD_TO,
--@pPACKING_COMPANY,
--@puser_id

--CHANGE AGI 2017-09-08  Insert field DG Cargo (same as Ready Cargo by Air) AND Searching menu by Case No

EXEC SPEX.[GetReadyCargoInquiry_2]
@pBUYER_CD, 
@pPD_CD,
@pVANNING_DT_FROM,
@pVANNING_DT_TO,
@pInvoice_Status,
@pINVOICE_NO,
@pVANNING_NO,
@pINVOICE_DT_FROM,
@pINVOICE_DT_TO,
@pETD_FROM,
@pETD_TO,
@pPACKING_COMPANY,
@puser_id,
@pCASE_NO