﻿INSERT INTO [spex].[TB_T_PART_LABEL_UPLOAD]
           ([DATA_ID]
           ,[PART_NO]
           ,[QTY]
           ,[PART_NAME]
		   ,[BATCH_ID])
     VALUES
           (@DATA_ID
           ,@PART_NO
           ,@QTY
           ,@PART_NAME
		   ,@BATCH_ID)