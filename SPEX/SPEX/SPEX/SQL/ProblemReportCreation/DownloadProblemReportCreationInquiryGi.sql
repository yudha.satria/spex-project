﻿EXEC [spex].[SP_PROBLEM_REPORT_INQUIRY_GI_DOWNLOAD]
	 @I_PR_CD
	,@I_PROBLEM_DATE_FROM
	,@I_PROBLEM_DATE_TO
	,@I_PART_NO
	,@I_STATUS_PROBLEM
	,@I_SUPPLIER
	,@I_SUB_SUPPLIER
	,@I_SUPPLIER_CREATOR
	,@I_PROBLEM_ORIGIN
	,@I_PROBLEM_CATEGORY
	,@I_KANBAN_ID
	,@I_SEND_STATUS
	,@I_TMAP_PART_NO