﻿SELECT
'' EmptyField,
u.USER_LOGIN UserLogin, 
u.USER_NAME UserName, 
u.USER_PASWD UserPassword,
u.PACKING_COMPANY PackingCompany,
u.COMPANY_PLANT_CD CompanyPlantCode,
u.PLANT_LINE_CD PlantLineCode,
u.TRANSPORT_CD TransportCode,
u.DEVICE_NW_IDENT DeviceNWIdent,
u.LOGIN_STS LoginStatus,
u.POSTION_CD Position,
u.USER_PASWD Password,
convert(date,u.CREATED_DT) CreatedDate,
u.CREATED_BY CreatedBy,
convert(date,u.CHANGET_DT) ChangedDate,
u.CHANGED_BY ChangedBy,
'XXXXX' UserPaswd
FROM spex.TB_M_DEVICE_USER u
where 1=1
and (u.USER_LOGIN = @UserLogin or @UserLogin is null)
and (u.USER_NAME = @UserName or @UserName is null)
and (u.PACKING_COMPANY = @PackingCompany or @PackingCompany is null)
and (u.COMPANY_PLANT_CD = @CompanyPlantCode or @CompanyPlantCode is null)
and (u.PLANT_LINE_CD = @PlantLineCode or @PlantLineCode is null)