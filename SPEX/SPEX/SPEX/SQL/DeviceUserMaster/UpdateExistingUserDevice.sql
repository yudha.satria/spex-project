﻿UPDATE spex.TB_M_DEVICE_USER
SET USER_NAME = @UserName
,USER_PASWD = @Password
,PACKING_COMPANY = @PackingCompany
,COMPANY_PLANT_CD = @CompanyPlantCode
,PLANT_LINE_CD = @PlantLineCode
,TRANSPORT_CD = @TransportCode
,DEVICE_NW_IDENT = @DeviceNWIdent
,POSTION_CD = @PositionCode
,CHANGED_BY = @UserId
,CHANGET_DT = SYSDATETIME()
,LOGIN_STS='N'
where USER_LOGIN = @UserLogin