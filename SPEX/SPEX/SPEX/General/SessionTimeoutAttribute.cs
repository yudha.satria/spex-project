﻿using System.Web;
using System.Web.Mvc;

namespace SPEX.General
{
    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            if (HttpContext.Current.Session == null)
            {
                filterContext.Result = new RedirectResult("~/spex/Login");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}