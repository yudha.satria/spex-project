﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : SERVICE PARTS EXPORT (SPEX)
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : DeviceUserMaster
 * Function Name    : Device User Master
 * Function Group   : 
 * Program Id       : DeviceUserMaster
 * Program Name     : SPEXMandatory
 * Program Type     : Common Mandatory Validation
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID.Yusuf
 * Version          : 01.00.00
 * Creation Date    : 05/09/2017 09:35:40
 * 

 *
 * Copyright(C) 2017 - . All Rights Reserved                                                                                              
 *************************************************************************************************/

using SPEX.Models.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SPEX.General.Validation
{
    public class SPEXMandatory : ValidationAttribute
    {
        public string aliasName { get; set; }

        public override string FormatErrorMessage(string name)
        {
            string messages = MessagesRepository.GetMessageById("MSPX00003ERR");
            return messages.Replace("{0}", aliasName);
        }

        public override bool IsValid(object value)
        {
            return value != null && value.ToString() != "";
        }
    }
}