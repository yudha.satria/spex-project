﻿Declare @@SQL_TEXT nvarchar(max),
		@@SYSTEM_TYPE varchar(max)=@0,
		@@SYSTEM_CD varchar(max)= @1
		
set @@SQL_TEXT ='SELECT  
	   [SYSTEM_TYPE]
      ,[SYSTEM_CD]
	  ,[SYSTEM_VALUE]
		FROM [TB_M_SYSTEM]
			 WHERE  1 = 1 '

IF ( @@SYSTEM_TYPE <> '' ) 
	BEGIN 
	  SET @@SQL_TEXT = @@SQL_TEXT + ' AND SYSTEM_TYPE LIKE ''%' + @@SYSTEM_TYPE + '%'''
	END

IF ( @@SYSTEM_CD <> '' ) 
	BEGIN 
	  SET @@SQL_TEXT = @@SQL_TEXT + ' AND SYSTEM_CD LIKE ''%' + @@SYSTEM_CD + '%'''
	END
	
	  
execute (@@SQL_TEXT) 