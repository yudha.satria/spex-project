﻿EXEC [spex].[SP_SHIP_INSTRUCTION_CONFIRM_TENTATIVE_SAVE]
	@BUYER_CD,
	@ETD,
	@VANNING_DT_FROM, 
	@VANNING_DT_TO,
	@PACKING_COMPANY,
	@VERSION,
	@SHIP_INSTRUCTION_NO,
	@SHIPPER,
	@CONSIGNEE, 
	@NOTIFY_PARTY,
	@CONTAINER_GRADE,
	@USER_ID