/**
 * Created by lufty on 2/17/14.
 */
(function($) {
    $.fn.tdkPageScrollTo = function(options) {
        var settings = $.extend({
            DisableEyeCandy: false,
            ImageBasePath: "images",
            ImageExtension: "png",
            Mode: "vertical",
            ScrollToBottomText: "Scroll to bottom",
            ScrollToTopText: "Scroll to top"
        }, options);

        return this.each(function() {
            var body = $("body");
            var html = $("html");
            if($.browser.mozilla) {
                html.css("overflow", "hidden");
                html.css("height", "100%");
                body.css("overflow", "auto");
                body.css("height", "100%");
            }

            var container = $(document.createElement("div"));
            container.addClass("tdk-ui-page-scrollto");
            if(settings.DisableEyeCandy) {
                container.addClass("disable-eye-candy");
            } else {
                container.addClass("enable-eye-candy");
            }
            body.append(container);

            var table = $(document.createElement("table"));
            container.append(table);
            var tableRow = $(document.createElement("tr"));
            table.append(tableRow);
            var tableColumn = $(document.createElement("td"));
            tableColumn.addClass("scrollto-text-column");
            var scrollText = $(document.createElement("span"));
            tableColumn.append(scrollText);
            tableRow.append(tableColumn);
            if(settings.Mode == "vertical") {
                scrollText.text(settings.ScrollToBottomText);
            } else if(settings.Mode == "vertical") {
                scrollText.text(settings.ScrollToTopText);
            }
            var tableColumn = $(document.createElement("td"));
            tableColumn.addClass("scrollto-icon-column");
            tableRow.append(tableColumn);
            var icon = $(document.createElement("img"));
            icon.addClass("scrollto-icon");
            icon.attr("src", settings.ImageBasePath + "/scroll-page-down." + settings.ImageExtension);
            tableColumn.append(icon);

            container.css("top", ($(window).height() / 3) - (icon.height() / 2));
            container.css("left", -(container.width() - icon.width()));
            container.hover(function() {
                if(settings.DisableEyeCandy) {
                    container.css("left", 0);
                } else {
                    container.animate({
                        left: 0
                    }, "fast", function() {});
                }
            }, function() {
                if(settings.DisableEyeCandy) {
                    container.css("left", -(container.width() - icon.width()));
                } else {
                    container.animate({
                        left: -(container.width() - icon.width())
                    }, "fast", function() {});
                }
            });

            container.click(function(e) {
                var animationPeriod = "slow";
                if(settings.DisableEyeCandy) {
                    animationPeriod = 0;
                }
                if(body.scrollTop() >= $(document).height() - $(window).height()) {
                    var scrollable = $("body");
                    if($.browser.mozilla) {
                        scrollable = $("body, html");
                    }

                    scrollable.animate({
                        scrollTop: 0
                    }, animationPeriod, "swing", function() {
                        icon.attr("src", settings.ImageBasePath + "/scroll-page-down." + settings.ImageExtension);
                        scrollText.text(settings.ScrollToBottomText);
                    });
                } else {
                    var scrollable = $("body");
                    if($.browser.mozilla) {
                        scrollable = $("body, html");
                    }

                    scrollable.animate({
                        scrollTop: $(document).height()
                    }, animationPeriod, "swing", function() {
                        icon.attr("src", settings.ImageBasePath + "/scroll-page-up." + settings.ImageExtension);
                        scrollText.text(settings.ScrollToTopText);
                    });
                }
            });
        });
    };
})(jQuery)