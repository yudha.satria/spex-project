///* 
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */

//(function ($) {
//    $.fn.tdkWebdeskMenubar = function (options) {
//        var settings = $.extend({
//            ImageBaseUrl: "img",
//            HomeIcon: "home.png",
//            LogoutIcon: "logout.png",
//            SearchIcon: "search.png",
//            MenuUpIcon: "menu-up.png",
//            MenuDownIcon: "menu-down.png",
//            TimestampIcon: "timestamp.png",
//            UserIcon: "user.png",
//            ActionIcon: " ",
//            UserPicture: "user-picture.png",
//            NavigationLogo: "navigation-logo.png",
//            NavigationLeftScrollIcon: "navscroll-left.png",
//            NavigationRightScrollIcon: "navscroll-right.png",
//            Title: "Put your page title here, even if it's long",
//            MaxPrimaryAction: 7,
//            ActionColumnCount: 5,
//            NavigationRowCount: 3,
//            NavigationList: "navigation-list",
//            NavigationScrollSpeed: 150,
//            ActionList: "action-list",
//            UserName: "Anonymous",
//            UserDescription: {
//                Name: "Goofy Rhamidar",
//                Position: "SectionHead",
//                Email: "Goofdar@toyota.co.id",
//                Roles: new Array(
//                        "Elvis Administrator",
//                        "IPPCS Administrator",
//                        "TMS Administrator",
//                        "IPSS Administrator",
//                        "IPMS User"
//                )
//            },
//            Time: "00:00",
//            Date: "January, 11 1983",
////            HomeCallback: function () { alert("Home icon clicked"); },
//            //            LogoutCallback: function () { alert("Logout icon clicked"); },
////            Time: "00:00",
////            Date: "January, 11 1983",
//            HomeCallback: function () { window.location = "/Home"; },
//            LogoutCallback: function () { window.location = "/Login/logout"; },
//            SearchCallback: function (key) {
//                var resultArray = new Array();
//                for (var i = 0; i < 20; i++) {
//                    resultArray[i] = "<a href='http://www.google.com'>Result " + i + "</a>";
//                }
//                return resultArray;
//            }
//        }, options);

//        settings.HomeIcon = settings.ImageBaseUrl + "/" + settings.HomeIcon;
//        settings.ActionIcon = settings.ImageBaseUrl + "/" + settings.ActionIcon;
//        settings.LogoutIcon = settings.ImageBaseUrl + "/" + settings.LogoutIcon;
//        settings.SearchIcon = settings.ImageBaseUrl + "/" + settings.SearchIcon;
//        settings.MenuUpIcon = settings.ImageBaseUrl + "/" + settings.MenuUpIcon;
//        settings.MenuDownIcon = settings.ImageBaseUrl + "/" + settings.MenuDownIcon;
//        settings.TimestampIcon = settings.ImageBaseUrl + "/" + settings.TimestampIcon;
//        settings.UserIcon = settings.ImageBaseUrl + "/" + settings.UserIcon;
//        settings.UserPicture = settings.ImageBaseUrl + "/" + settings.UserPicture;
//        settings.NavigationLogo = settings.ImageBaseUrl + "/" + settings.NavigationLogo;
//        settings.NavigationLeftScrollIcon = settings.ImageBaseUrl + "/" + settings.NavigationLeftScrollIcon;
//        settings.NavigationRightScrollIcon = settings.ImageBaseUrl + "/" + settings.NavigationRightScrollIcon;

//        var createIcon = function (className, container, icon, text) {
//            if ((text !== null) && (text !== undefined)) {
//                container.append("<div id='" + className + "' class='icon " + className + "'><img/><span>" + text + "</span></div>");
//            } else {
//                container.append("<div id='" + className + "' class='icon " + className + "'><img/></div>");
//            }

//            var wrapper = container.children("#" + className + ":first");
//            var image = wrapper.children("img:first");
//            image.addClass("image");
//            image.attr("alt", "");
//            image.attr("src", icon);
//            image.attr("width", "18px");

//            wrapper.click(function () {
//                var icon = $(this);
//                if (icon.hasClass("pressed")) {
//                    icon.removeClass("pressed");
//                } else {
//                    icon.addClass("pressed");
//                }
//            });

//            return wrapper;
//        };

//        var createSeparator = function (leftComponent, rightComponent) {
//            leftComponent.addClass("separator-left");
//            rightComponent.addClass("separator-right");
//        };
//        var registerButtonPanelClickEvent = function (menubar, button, panel, openingCallback, closingCallback, openedCallback, closedCallback) {
//            button.click(function () {
//                var top = parseInt(panel.css("top"), 10);
//                if (top < 0) {
//                    $("body").find(".webdesk-menubar-button-panel").each(function () {
//                        var panel = $(this);
//                        panel.animate({
//                            top: -menubar.height() - panel.height()
//                        }, "fast", "swing", function () { }); // wah ini, callbacknya gak kehandle, pake registry aja deh, catat semua callbacknya
//                    });

//                    menubar.find(".icon").each(function () {
//                        var icon = $(this);
//                        if (icon.attr("id") !== button.attr("id")) {
//                            icon.removeClass("pressed");
//                        }
//                    });

//                    if ((openingCallback !== undefined) && (typeof (openingCallback) === "function")) {
//                        openingCallback();
//                    }
//                    panel.animate({
//                        top: menubar.height()
//                    }, "fast", "swing", function () {
//                        if ((openedCallback !== undefined) && (typeof (openedCallback) === "function")) {
//                            openedCallback();
//                        }
//                    });
//                } else {
//                    if ((closingCallback !== undefined) && (typeof (closingCallback) === "function")) {
//                        closingCallback();
//                    }
//                    panel.animate({
//                        top: -menubar.height() - panel.height()
//                    }, "fast", "swing", function () {
//                        if ((closedCallback !== undefined) && (typeof (closedCallback) === "function")) {
//                            closedCallback();
//                        }
//                    });
//                }
//            });
//        };

//        var createButtonPanel = function (parent, browserWindow, icon, className) {
//            parent.append("<div class='tdk-ui " + className + " webdesk-menubar-button-panel round'></div>");
//            var panel = parent.children("." + className + ":first");
//            panel.css("right", 10);
//            /*if ((embedToButton !== undefined) && (embedToButton !== null) && embedToButton) {
//            icon.append(panel);
//            panel.css("position", "absolute");
//            }*/
//            return panel;
//        };

//        return this.each(function () {
//            var element = $(this);
//            var body = $("body");
//            var browserWindow = $(window);
//            element.addClass("tdk-ui webdesk-menubar");
////            element.width(body.outerWidth(true));

//            element.append("<div class='left'></div>");
//            element.append("<div class='right'></div>");

//            var left = element.children(".left:first");
//            left.height(element.innerHeight());
//            var right = element.children(".right:first");
//            right.height(element.innerHeight());

//            var home = createIcon("home", left, settings.HomeIcon, null);
//            home.click(function () {
//                if (typeof (settings.HomeCallback) === "function") {
//                    settings.HomeCallback();
//                }
//            });

//            left.append("<div class='title'></div>");
//            var title = left.children(".title:first");
//            title.text(settings.Title);

//            var action = createIcon("action-slider", right, settings.ActionIcon, null);
//            var actionPanel = createButtonPanel(body, browserWindow, action, "webdesk-menubar-action");
//            actionPanel.append("<div class='wrapper'><ul/></div>");
//            var actionList = actionPanel.find("ul:first");
//            var definedActions = element.find("#" + settings.ActionList).first();
//            actionList.append(definedActions.html());
//            definedActions.remove();
//            actionList.find("img").attr("height", 24);
//            actionPanel.css("top", -actionPanel.height());

//            registerButtonPanelClickEvent(
//                    element, action, actionPanel,
//                    function () { }, function () { },
//                    function () { }, function () { }
//            );

//            var primaryActions = createIcon("actions", right, settings.ActionIcon, "Actions");
//            primaryActions.empty();
//            var primaryActionCount = 0;
//            actionList.find("img").each(function () {
//                if (primaryActionCount < settings.MaxPrimaryAction) {
//                    primaryActions.append("<div><img src='" + $(this).attr("src") + "'/></div>");
//                    primaryActionCount++;
//                }
//            });

//            var menu = createIcon("menu", right, settings.MenuDownIcon, "Menu");
//            var menuPanel = createButtonPanel(body, browserWindow, menu, "webdesk-menubar-menu-slider");
//            menuPanel.append("<div class='navlogo'><img alt='' width='200' src='" + settings.NavigationLogo + "'/></div>");
//            menuPanel.append("<div class='navitem'/>");
//            var menuItemWrapper = menuPanel.children(".navitem:first");
//            menuItemWrapper.append("<div class='content'><div class='viewport'><table></table></div></div>");
//            var menuContent = menuItemWrapper.children(".content:first");
//            menuItemWrapper.append("<div class='scroller'></div>");
//            var menuScroller = menuItemWrapper.children(".scroller:first");
//            menuScroller.append("<div class='left-scroll'><img alt='' width='24' src='" + settings.NavigationLeftScrollIcon + "'/></div>");
//            menuScroller.append("<div class='right-scroll'><img alt='' width='24' src='" + settings.NavigationRightScrollIcon + "'/></div>");
//            var menuViewPort = menuItemWrapper.find(".viewport:first");

//            var navigationTable = menuViewPort.children("table:first");
//            navigationTable.append("<tr/>");
//            var navigationTableRow = navigationTable.find("tr:first");
//            var navigationTableData;
//            var navigationList = element.find("#" + settings.NavigationList + ":first");
//            menuViewPort.append(navigationList);
//            var navigationListItems = navigationList.children("li");
//            var maxMenuColumnWidth = 0;
//            var navigationColumnList;
//            navigationListItems.each(function () {
//                var item = $(this);
//                item.addClass("menu-item");
//                if (item.width() > maxMenuColumnWidth) {
//                    maxMenuColumnWidth = item.width();
//                }
//                navigationTableRow.append("<td class='menu-column'/>");
//                navigationTableData = navigationTableRow.children("td").last();
//                navigationTableData.append("<ul class='menu-list'/>");
//                navigationColumnList = navigationTableData.children("ul:first");
//                navigationColumnList.append(item);

//                item.children("img").attr("width", 32);
//                var children = item.children("ul:first");
//                if (children.size() > 0) {
//                    item.addClass("has-children");
//                    navigationTableData.addClass("has-children");
//                    children.addClass("submenu-list");
//                    children.children("li").each(function () {
//                        var subitem = $(this);
//                        subitem.addClass("submenu-item");
//                        subitem.children("img").attr("width", 24);
//                        var subchildren = subitem.children("ul:first");
//                        if (subchildren.size() > 0) {
//                            subitem.addClass("has-children");
//                            subchildren.addClass("subsubmenu-list");
//                            subchildren.children("li").each(function () {
//                                var subsubitem = $(this);
//                                subsubitem.addClass("subsubmenu-item");
//                                subsubitem.children("img").attr("width", 24);
//                            });
//                        } else {
//                            subitem.addClass("leaf");
//                        }
//                    });
//                } else {
//                    item.addClass("leaf");
//                    item.append("<ul class='submenu-list'/>");
//                    navigationTableData.addClass("leaf");
//                }
//            });

//            navigationListItems.each(function () {
//                var item = $(this);
//                item.width(maxMenuColumnWidth);
//            });
//            navigationList.remove();

//            var navColumnCount = menuViewPort.find("tr:first").find("td").size();
//            registerButtonPanelClickEvent(
//                element, menu, menuPanel,
//                function () { }, function () { },
//                function () {
//                    var image = menu.children("img:first");
//                    image.attr("src", settings.MenuUpIcon);
//                }, function () {
//                    var image = menu.children("img:first");
//                    image.attr("src", settings.MenuDownIcon);
//                }
//            );
//            menuContent.height(navigationTable.outerHeight(true));
//            menuPanel.css("top", -menuPanel.height());
//            //            menuPanel.css("top", 30);

//            var navLeftScroll = menuScroller.children(".left-scroll:first");
//            var navRightScroll = menuScroller.children(".right-scroll:first");
//            var leftScrollTreshold = maxMenuColumnWidth * (navColumnCount - 2);
//            navRightScroll.click(function (e) {
//                e.stopPropagation();
//                var left = parseInt(menuViewPort.css("left"), 10);
//                if (left > -leftScrollTreshold) {
//                    menuViewPort.animate({
//                        left: left - maxMenuColumnWidth
//                    }, settings.NavigationScrollSpeed, "swing");
//                }
//            });
//            navLeftScroll.click(function (e) {
//                e.stopPropagation();
//                var left = parseInt(menuViewPort.css("left"), 10);
//                if (left < 0) {
//                    var leftValue = 0;
//                    if (Math.abs(left) >= maxMenuColumnWidth) {
//                        leftValue = left + maxMenuColumnWidth;
//                    }

//                    menuViewPort.animate({
//                        left: leftValue
//                    }, settings.NavigationScrollSpeed, "swing");
//                }
//            });

//            var user = createIcon("user", right, settings.UserIcon, settings.UserName);
//            var userPanel = createButtonPanel(body, browserWindow, user, "webdesk-menubar-user");
//            userPanel.append("<div class='picture section'><img alt='' src='" + settings.UserPicture + "'/></div>");
//            userPanel.append(
//                    "<div class='description section'>" +
//                    "   <table></table>" +
//                    "</div>"
//            );
//            var userDescriptionTable = userPanel.children(".description").find("table:first");
//            var userDescriptionTableRowValue;
//            var userDescriptionTableRowValueList;
//            $.each(settings.UserDescription, function (key, value) {
//                userDescriptionTable.append(
//                        "<tr>" +
//                        "   <td class='key'>" + key + "</td>" +
//                        "   <td class='value'></td>" +
//                        "</tr>"
//                );
//                userDescriptionTableRowValue = userDescriptionTable.find("tr:last").children("td.value:last");
//                if (value instanceof Array) {
//                    userDescriptionTableRowValue.append("<ul></ul>");
//                    userDescriptionTableRowValueList = userDescriptionTableRowValue.children("ul:first");
//                    $(value).each(function (index) {
//                        userDescriptionTableRowValueList.append("<li>" + value[index] + "</li>");
//                    });
//                } else {
//                    userDescriptionTableRowValue.append(value);
//                }
//            });
//            userPanel.css("top", -userPanel.height() * 2); // sementara dikali dua dulu, tp kenapa ya ? tinggi tabelnya gak kehitung kali ya ...
//            registerButtonPanelClickEvent(
//                    element, user, userPanel,
//                    function () { }, function () { },
//                    function () { }, function () { }
//            );
//            var timestampPanel = createButtonPanel(body, browserWindow, timestamp, "webdesk-menubar-time");
//            //begin
//            //created by fid.joshua - 12-02-2014
//            //set date now
//            var digitalClock = "";
//            function startTime() {
//                var today = new Date();
//                var h = today.getHours();
//                var m = today.getMinutes();
//                var s = today.getSeconds();
//                m = checkTime(m);
//                s = checkTime(s);
//                digitalClock = h + ":" + m;
//                t = setTimeout(function () { startTime() }, 500);
//            }

//            function checkTime(i) {
//                if (i < 10) {
//                    i = "0" + i;
//                }
//                return i;
//            }
//            window.onload = startTime();
//            var timestamp = createIcon("timestamp", right, settings.TimestampIcon, digitalClock);

//            //end
//            //var timestamp = createIcon("timestamp", right, settings.TimestampIcon, settings.Time);
//            var timestampPanel = createButtonPanel(body, browserWindow, timestamp, "webdesk-menubar-time");
//            //begin
//            //created by fid.joshua - 12-02-2014
//            //set date now
//            var currentTime = new Date();
//            var year = currentTime.getFullYear();
//            var month = currentTime.getMonth() + 1;
//            var day = currentTime.getDate();

//            if (month == "1") {
//                month = "Jan";
//            }
//            else if (month == "2") {
//                month = "Feb";
//            }
//            else if (month == "3") {
//                month = "Mar";
//            }
//            else if (month == "4") {
//                month = "Apr";
//            }
//            else if (month == "5") {
//                month = "May";
//            }
//            else if (month == "6") {
//                month = "Jun";
//            }
//            else if (month == "7") {
//                month = "Jul";
//            }
//            else if (month == "8") {
//                month = "Aug";
//            }
//            else if (month == "9") {
//                month = "Sep";
//            }
//            else if (month == "10") {
//                month = "Oct";
//            }
//            else if (month == "11") {
//                month = "Nov";
//            }
//            else if (month == "12") {
//                month = "Dec";
//            }
//            timestampPanel.append("<div class='date section'>" + month + ", " + day + " " + year + "</div>");
//            //end
//            //timestampPanel.append("<div class='date section'>" + settings.Date + "</div>");
//            registerButtonPanelClickEvent(
//                    element, timestamp, timestampPanel,
//                    function () { }, function () { },
//                    function () { }, function () { }
//            );


////            var user = createIcon("user", right, settings.UserIcon, settings.UserName);
////            var userPanel = createButtonPanel(body, browserWindow, user, "webdesk-menubar-user");
////            userPanel.append("<div class='picture section'><img alt='' src='" + settings.UserPicture + "'/></div>");
////            userPanel.append(
////                    "<div class='description section'>" +
////                    "   <table></table>" +
////                    "</div>"
////            );
////            var userDescriptionTable = userPanel.children(".description").find("table:first");
////            var userDescriptionTableRowValue;
////            var userDescriptionTableRowValueList;
////            $.each(settings.UserDescription, function (key, value) {
////                userDescriptionTable.append(
////                        "<tr>" +
////                        "   <td class='key'>" + key + "</td>" +
////                        "   <td class='value'></td>" +
////                        "</tr>"
////                );
////                userDescriptionTableRowValue = userDescriptionTable.find("tr:last").children("td.value:last");
////                if (value instanceof Array) {
////                    userDescriptionTableRowValue.append("<ul></ul>");
////                    userDescriptionTableRowValueList = userDescriptionTableRowValue.children("ul:first");
////                    $(value).each(function (index) {
////                        userDescriptionTableRowValueList.append("<li>" + value[index] + "</li>");
////                    });
////                } else {
////                    userDescriptionTableRowValue.append(value);
////                }
////            });
////            userPanel.css("top", -userPanel.height() * 2); // sementara dikali dua dulu, tp kenapa ya ? tinggi tabelnya gak kehitung kali ya ...
////            registerButtonPanelClickEvent(
////                    element, user, userPanel,
////                    function () { }, function () { },
////                    function () { }, function () { }
////            );

////            var timestamp = createIcon("timestamp", right, settings.TimestampIcon, settings.Time);
////            var timestampPanel = createButtonPanel(body, browserWindow, timestamp, "webdesk-menubar-time");
////            timestampPanel.append("<div class='date section'>" + settings.Date + "</div>");
////            registerButtonPanelClickEvent(
////                    element, timestamp, timestampPanel,
////                    function () { }, function () { },
////                    function () { }, function () { }
////            );

//            var search = createIcon("search", right, settings.SearchIcon, null);
//            body.append("<div class='tdk-ui webdesk-menubar-search webdesk-menubar-button-panel'></div>");
//            var searchPanel = createButtonPanel(body, browserWindow, search, "webdesk-menubar-search");
//            searchPanel.removeClass("round");
//            searchPanel.append("<div class='input section'>Search &nbsp;&nbsp; <input type='text'/></div>");
//            searchPanel.append("<div class='result section'></div>");
//            var searchResult = searchPanel.children(".result:first");
//            searchResult.hide();
//            searchResult.append("<ul></ul>");
//            var searchResultList = searchResult.children("ul:first");
//            searchPanel.find("input").change(function () {
//                var text = $(this).val();
//                if (text !== '') {
//                    searchPanel.addClass("round");
//                    var resultList = settings.SearchCallback(text);
//                    if ((resultList !== null) && (resultList !== undefined)) {
//                        $(resultList).each(function (index) {
//                            searchResultList.append("<li>" + resultList[index] + "</li>");
//                        });
//                        searchResult.fadeIn("fast", "swing");
//                    }
//                } else {
//                    searchResult.fadeOut("fast", "swing", function () {
//                        searchPanel.removeClass("round");
//                    });
//                }
//            });
//            registerButtonPanelClickEvent(
//                    element, search, searchPanel,
//                    function () { },
//                    function () {
//                        searchResult.hide();
//                    },
//                    function () { },
//                    function () {
//                        searchPanel.find("input").val("");
//                        //                        searchResultList.html("");
//                    }
//            );

//            var logout = createIcon("logout", right, settings.LogoutIcon, null);
//            logout.click(function () {
//                if (typeof (settings.LogoutCallback) === "function") {
//                    settings.LogoutCallback();
//                }
//            });

//            createSeparator(home, title);
//            createSeparator(primaryActions, menu);
//            createSeparator(menu, user);
//            createSeparator(user, timestamp);
//            createSeparator(timestamp, search);
//            createSeparator(search, logout);
//        });
//    };
//})(jQuery);

/* 
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

(function ($) {
    $.fn.tdkWebdeskMenubar = function (options) {
        var settings = $.extend({
            ImageBaseUrl: "img",
            HomeIcon: "home.png",
            LogoutIcon: "logout.png",
            SearchIcon: "search.png",
            MenuUpIcon: "menu-up.png",
            MenuDownIcon: "menu-down.png",
            TimestampIcon: "timestamp.png",
            UserIcon: "user.png",
            ActionIcon: "action.png",
            UserPicture: "user-picture.png",
            NavigationLogo: "navigation-logo.png",
            NavigationLeftScrollIcon: "navscroll-left.png",
            NavigationRightScrollIcon: "navscroll-right.png",
            Title: "Put your page title here, even if it's long",
            MaxPrimaryAction: 7,
            ActionColumnCount: 5,
            NavigationRowCount: 3,
            NavigationList: "navigation-list",
            NavigationScrollSpeed: 150,
            ActionList: "action-list",
            UserName: "Anonymous",
            UserDescription: {
                Name: "Goofy Rhamidar",
                Position: "SectionHead",
                Email: "Goofdar@toyota.co.id",
                Roles: new Array(
                        "Elvis Administrator",
                        "IPPCS Administrator",
                        "TMS Administrator",
                        "IPSS Administrator",
                        "IPMS User"
                )
            },
            Time: "00:00",
            Date: "January, 11 1983",
            HomeCallback: function () { window.location = "/spex/Home"; /*alert("Home icon clicked");*/ },
            LogoutCallback: function () { window.location = "/spex/Login/Logout"; /*alert("Logout icon clicked");*/ },
            SearchCallback: function (key) {
                var resultArray = new Array();
                for (var i = 0; i < 20; i++) {
                    resultArray[i] = "<a href='http://www.google.com'>Result " + i + "</a>";
                }
                return resultArray;
            }
        }, options);

        settings.HomeIcon = settings.ImageBaseUrl + "/" + settings.HomeIcon;
        settings.ActionIcon = settings.ImageBaseUrl + "/" + settings.ActionIcon;
        settings.LogoutIcon = settings.ImageBaseUrl + "/" + settings.LogoutIcon;
        settings.SearchIcon = settings.ImageBaseUrl + "/" + settings.SearchIcon;
        settings.MenuUpIcon = settings.ImageBaseUrl + "/" + settings.MenuUpIcon;
        settings.MenuDownIcon = settings.ImageBaseUrl + "/" + settings.MenuDownIcon;
        settings.TimestampIcon = settings.ImageBaseUrl + "/" + settings.TimestampIcon;
        settings.UserIcon = settings.ImageBaseUrl + "/" + settings.UserIcon;
        settings.UserPicture = settings.ImageBaseUrl + "/" + settings.UserPicture;
        settings.NavigationLogo = settings.ImageBaseUrl + "/" + settings.NavigationLogo;
        settings.NavigationLeftScrollIcon = settings.ImageBaseUrl + "/" + settings.NavigationLeftScrollIcon;
        settings.NavigationRightScrollIcon = settings.ImageBaseUrl + "/" + settings.NavigationRightScrollIcon;

        var createIcon = function (className, container, icon, text) {
            if ((text !== null) && (text !== undefined)) {
                container.append("<div id='" + className + "' class='icon " + className + "'><img/><span>" + text + "</span></div>");
            } else {
                container.append("<div id='" + className + "' class='icon " + className + "'><img/></div>");
            }

            var wrapper = container.children("#" + className + ":first");
            var image = wrapper.children("img:first");
            image.addClass("image");
            image.attr("alt", "");
            image.attr("src", icon);
            image.attr("width", "18px");

            wrapper.click(function () {
                var icon = $(this);
                if (icon.hasClass("pressed")) {
                    icon.removeClass("pressed");
                } else {
                    icon.addClass("pressed");
                }
            });

            return wrapper;
        };

        var createSeparator = function (leftComponent, rightComponent) {
            leftComponent.addClass("separator-left");
            rightComponent.addClass("separator-right");
        };
        var registerButtonPanelClickEvent = function (menubar, button, panel, openingCallback, closingCallback, openedCallback, closedCallback) {
            button.click(function () {
                var top = parseInt(panel.css("top"), 10);
                if (top < 0) {
                    $("body").find(".webdesk-menubar-button-panel").each(function () {
                        var panel = $(this);
                        panel.animate({
                            top: -menubar.height() - panel.height()
                        }, "fast", "swing", function () { }); // wah ini, callbacknya gak kehandle, pake registry aja deh, catat semua callbacknya
                    });

                    menubar.find(".icon").each(function () {
                        var icon = $(this);
                        if (icon.attr("id") !== button.attr("id")) {
                            icon.removeClass("pressed");
                        }
                    });

                    if ((openingCallback !== undefined) && (typeof (openingCallback) === "function")) {
                        openingCallback();
                    }
                    panel.animate({
                        top: menubar.height()
                    }, "fast", "swing", function () {
                        if ((openedCallback !== undefined) && (typeof (openedCallback) === "function")) {
                            openedCallback();
                        }
                    });
                } else {
                    if ((closingCallback !== undefined) && (typeof (closingCallback) === "function")) {
                        closingCallback();
                    }
                    panel.animate({
                        top: -menubar.height() - panel.height()
                    }, "fast", "swing", function () {
                        if ((closedCallback !== undefined) && (typeof (closedCallback) === "function")) {
                            closedCallback();
                        }
                    });
                }
            });
        };

        var createButtonPanel = function (parent, browserWindow, icon, className) {
            parent.append("<div class='tdk-ui " + className + " webdesk-menubar-button-panel round'></div>");
            var panel = parent.children("." + className + ":first");
            panel.css("right", 10);
            /*if ((embedToButton !== undefined) && (embedToButton !== null) && embedToButton) {
            icon.append(panel);
            panel.css("position", "absolute");
            }*/
            return panel;
        };

        return this.each(function () {
            var element = $(this);
            var body = $("body");
            var browserWindow = $(window);
            element.addClass("tdk-ui webdesk-menubar");
            //            element.width(body.outerWidth(true));

            element.append("<div class='left'></div>");
            element.append("<div class='right'></div>");

            var left = element.children(".left:first");
            left.height(element.innerHeight());
            var right = element.children(".right:first");
            right.height(element.innerHeight());

            var home = createIcon("home", left, settings.HomeIcon, null);
            home.click(function () {
                if (typeof (settings.HomeCallback) === "function") {
                    settings.HomeCallback();
                }
            });

            left.append("<div class='title'></div>");
            var title = left.children(".title:first");
            title.text(settings.Title);

            // Modified by FID)Kevin, 2014-03-21
            // Hide action feature
            //            var action = createIcon("action-slider", right, settings.ActionIcon, null);
            //            var actionPanel = createButtonPanel(body, browserWindow, action, "webdesk-menubar-action");
            //            actionPanel.append("<div class='wrapper'><ul/></div>");
            //            var actionList = actionPanel.find("ul:first");
            //            var definedActions = element.find("#" + settings.ActionList).first();
            //            actionList.append(definedActions.html());
            //            definedActions.remove();
            //            actionList.find("img").attr("height", 24);
            //            actionPanel.css("top", -actionPanel.height());

            //            registerButtonPanelClickEvent(
            //                    element, action, actionPanel,
            //                    function () { }, function () { },
            //                    function () { }, function () { }
            //            );

            //            var primaryActions = createIcon("actions", right, settings.ActionIcon, "Actions");
            //            primaryActions.empty();
            //            var primaryActionCount = 0;
            //            actionList.find("img").each(function () {
            //                if (primaryActionCount < settings.MaxPrimaryAction) {
            //                    primaryActions.append("<div><img src='" + $(this).attr("src") + "'/></div>");
            //                    primaryActionCount++;
            //                }
            //            });

//            var menu = createIcon("menu", right, settings.MenuDownIcon, "Menu");
//            var menuPanel = createButtonPanel(body, browserWindow, menu, "webdesk-menubar-menu-slider");
//            menuPanel.append("<div class='navlogo'><img alt='' width='200' src='" + settings.NavigationLogo + "'/></div>");
//            menuPanel.append("<div class='navitem'/>");
//            var menuItemWrapper = menuPanel.children(".navitem:first");
//            menuItemWrapper.append("<div class='content'><div class='viewport'><table></table></div></div>");
//            var menuContent = menuItemWrapper.children(".content:first");
//            menuItemWrapper.append("<div class='scroller'></div>");
//            var menuScroller = menuItemWrapper.children(".scroller:first");
//            menuScroller.append("<div class='left-scroll'><img alt='' width='24' src='" + settings.NavigationLeftScrollIcon + "'/></div>");
//            menuScroller.append("<div class='right-scroll'><img alt='' width='24' src='" + settings.NavigationRightScrollIcon + "'/></div>");
//            var menuViewPort = menuItemWrapper.find(".viewport:first");

//            var navigationTable = menuViewPort.children("table:first");
//            navigationTable.append("<tr/>");
//            var navigationTableRow = navigationTable.find("tr:first");
//            var navigationTableData;
//            var navigationList = element.find("#" + settings.NavigationList + ":first");
//            menuViewPort.append(navigationList);
//            var navigationListItems = navigationList.children("li");
//            var maxMenuColumnWidth = 0;
//            var navigationColumnList;
//            navigationListItems.each(function () {
//                var item = $(this);
//                item.addClass("menu-item");
//                if (item.width() > maxMenuColumnWidth) {
//                    maxMenuColumnWidth = item.width();
//                }
//                navigationTableRow.append("<td class='menu-column'/>");
//                navigationTableData = navigationTableRow.children("td").last();
//                navigationTableData.append("<ul class='menu-list'/>");
//                navigationColumnList = navigationTableData.children("ul:first");
//                navigationColumnList.append(item);

//                item.children("img").attr("width", 32);
//                var children = item.children("ul:first");
//                if (children.size() > 0) {
//                    item.addClass("has-children");
//                    navigationTableData.addClass("has-children");
//                    children.addClass("submenu-list");
//                    children.children("li").each(function () {
//                        var subitem = $(this);
//                        subitem.addClass("submenu-item");
//                        subitem.children("img").attr("width", 24);
//                        var subchildren = subitem.children("ul:first");
//                        if (subchildren.size() > 0) {
//                            subitem.addClass("has-children");
//                            subchildren.addClass("subsubmenu-list");
//                            subchildren.children("li").each(function () {
//                                var subsubitem = $(this);
//                                subsubitem.addClass("subsubmenu-item");
//                                subsubitem.children("img").attr("width", 24);
//                            });
//                        } else {
//                            subitem.addClass("leaf");
//                        }
//                    });
//                } else {
//                    item.addClass("leaf");
//                    item.append("<ul class='submenu-list'/>");
//                    navigationTableData.addClass("leaf");
//                }
//            });

//            navigationListItems.each(function () {
//                var item = $(this);
//                item.width(maxMenuColumnWidth);
//            });
//            navigationList.remove();

//            var navColumnCount = menuViewPort.find("tr:first").find("td").size();
//            registerButtonPanelClickEvent(
//                element, menu, menuPanel,
//                function () { }, function () { },
//                function () {
//                    var image = menu.children("img:first");
//                    image.attr("src", settings.MenuUpIcon);
//                }, function () {
//                    var image = menu.children("img:first");
//                    image.attr("src", settings.MenuDownIcon);
//                }
//            );
//            menuContent.height(navigationTable.outerHeight(true));
//            menuPanel.css("top", -menuPanel.height());
//            //            menuPanel.css("top", 30);

//            var navLeftScroll = menuScroller.children(".left-scroll:first");
//            var navRightScroll = menuScroller.children(".right-scroll:first");
//            var leftScrollTreshold = maxMenuColumnWidth * (navColumnCount - 2);
//            navRightScroll.click(function (e) {
//                e.stopPropagation();
//                var left = parseInt(menuViewPort.css("left"), 10);
//                if (left > -leftScrollTreshold) {
//                    menuViewPort.animate({
//                        left: left - maxMenuColumnWidth
//                    }, settings.NavigationScrollSpeed, "swing");
//                }
//            });
//            navLeftScroll.click(function (e) {
//                e.stopPropagation();
//                var left = parseInt(menuViewPort.css("left"), 10);
//                if (left < 0) {
//                    var leftValue = 0;
//                    if (Math.abs(left) >= maxMenuColumnWidth) {
//                        leftValue = left + maxMenuColumnWidth;
//                    }

//                    menuViewPort.animate({
//                        left: leftValue
//                    }, settings.NavigationScrollSpeed, "swing");
//                }
//            });

            var user = createIcon("user", right, settings.UserIcon, settings.UserName);
            var userPanel = createButtonPanel(body, browserWindow, user, "webdesk-menubar-user");
            userPanel.append("<div class='picture section'><img alt='' src='" + settings.UserPicture + "'/></div>");
            userPanel.append(
                    "<div class='description section'>" +
                    "   <table></table>" +
                    "</div>"
            );
            var userDescriptionTable = userPanel.children(".description").find("table:first");
            var userDescriptionTableRowValue;
            var userDescriptionTableRowValueList;
            $.each(settings.UserDescription, function (key, value) {
                userDescriptionTable.append(
                        "<tr>" +
                        "   <td class='key'>" + key + "</td>" +
                        "   <td class='value'></td>" +
                        "</tr>"
                );
                userDescriptionTableRowValue = userDescriptionTable.find("tr:last").children("td.value:last");
                if (value instanceof Array) {
                    userDescriptionTableRowValue.append("<ul></ul>");
                    userDescriptionTableRowValueList = userDescriptionTableRowValue.children("ul:first");
                    $(value).each(function (index) {
                        userDescriptionTableRowValueList.append("<li>" + value[index] + "</li>");
                    });
                } else {
                    userDescriptionTableRowValue.append(value);
                }
            });
            userPanel.css("top", -userPanel.height() * 2); // sementara dikali dua dulu, tp kenapa ya ? tinggi tabelnya gak kehitung kali ya ...
            registerButtonPanelClickEvent(
                    element, user, userPanel,
                    function () { }, function () { },
                    function () { }, function () { }
            );
            var timestampPanel = createButtonPanel(body, browserWindow, timestamp, "webdesk-menubar-time");
            //begin
            //created by fid.joshua - 12-02-2014
            //set date now
            var digitalClock = "";
            function startTime() {
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                digitalClock = h + ":" + m;
                t = setTimeout(function () { startTime() }, 500);
            }

            function checkTime(i) {
                if (i < 10) {
                    i = "0" + i;
                }
                return i;
            }
            window.onload = startTime();
            var timestamp = createIcon("timestamp", right, settings.TimestampIcon, digitalClock);

            //end
            //var timestamp = createIcon("timestamp", right, settings.TimestampIcon, settings.Time);
            var timestampPanel = createButtonPanel(body, browserWindow, timestamp, "webdesk-menubar-time");
            //begin
            //created by fid.joshua - 12-02-2014
            //set date now
            var currentTime = new Date();
            var year = currentTime.getFullYear();
            var month = currentTime.getMonth() + 1;
            var day = currentTime.getDate();

            if (month == "1") {
                month = "Jan";
            }
            else if (month == "2") {
                month = "Feb";
            }
            else if (month == "3") {
                month = "Mar";
            }
            else if (month == "4") {
                month = "Apr";
            }
            else if (month == "5") {
                month = "May";
            }
            else if (month == "6") {
                month = "Jun";
            }
            else if (month == "7") {
                month = "Jul";
            }
            else if (month == "8") {
                month = "Aug";
            }
            else if (month == "9") {
                month = "Sep";
            }
            else if (month == "10") {
                month = "Oct";
            }
            else if (month == "11") {
                month = "Nov";
            }
            else if (month == "12") {
                month = "Dec";
            }
            timestampPanel.append("<div class='date section'>" + month + ", " + day + " " + year + "</div>");
            //end
            //timestampPanel.append("<div class='date section'>" + settings.Date + "</div>");
            registerButtonPanelClickEvent(
                    element, timestamp, timestampPanel,
                    function () { }, function () { },
                    function () { }, function () { }
            );

//            var search = createIcon("search", right, settings.SearchIcon, null);
//            body.append("<div class='tdk-ui webdesk-menubar-search webdesk-menubar-button-panel'></div>");
//            var searchPanel = createButtonPanel(body, browserWindow, search, "webdesk-menubar-search");
//            searchPanel.removeClass("round");
//            searchPanel.append("<div class='input section'>Search &nbsp;&nbsp; <input type='text'/></div>");
//            searchPanel.append("<div class='result section'></div>");
//            var searchResult = searchPanel.children(".result:first");
//            searchResult.hide();
//            searchResult.append("<ul></ul>");
//            var searchResultList = searchResult.children("ul:first");
//            searchPanel.find("input").change(function () {
//                var text = $(this).val();
//                if (text !== '') {
//                    searchPanel.addClass("round");
//                    var resultList = settings.SearchCallback(text);
//                    if ((resultList !== null) && (resultList !== undefined)) {
//                        $(resultList).each(function (index) {
//                            searchResultList.append("<li>" + resultList[index] + "</li>");
//                        });
//                        searchResult.fadeIn("fast", "swing");
//                    }
//                } else {
//                    searchResult.fadeOut("fast", "swing", function () {
//                        searchPanel.removeClass("round");
//                    });
//                }
//            });
//            registerButtonPanelClickEvent(
//                    element, search, searchPanel,
//                    function () { },
//                    function () {
//                        searchResult.hide();
//                    },
//                    function () { },
//                    function () {
//                        searchPanel.find("input").val("");
//                        //                        searchResultList.html("");
//                    }
//            );

//            var logout = createIcon("logout", right, settings.LogoutIcon, null);
//            logout.click(function () {
//                if (typeof (settings.LogoutCallback) === "function") {
//                    settings.LogoutCallback();
//                }
//            });
            createSeparator(home, title);
            createSeparator(title, user);
            createSeparator(user, timestamp);
//            createSeparator(timestamp, search);
//            createSeparator(search, logout);
        });
    };
})(jQuery);