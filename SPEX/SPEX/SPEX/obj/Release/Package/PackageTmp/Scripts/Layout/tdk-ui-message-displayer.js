/**
 * Created by lufty on 2/14/14.
 */
(function ($) {
    $.fn.tdkMessageDisplayer = function (options) {

        var SEVERITY_INFO = "info";
        var SEVERITY_WARNING = "warning";
        var SEVERITY_ERROR = "error";

        var settings = $.extend({
            DisableEyeCandy: false,
            ImageBasePath: "images",
            ImageExtension: "png",
            Messages: null
        }, options);

        var CLASS_DISABLE_EYE_CANDY = "disable-eye-candy";
        var CLASS_ENABLE_EYE_CANDY = "enable-eye-candy";

        var initWidthCalculator = function () {
            var body = $("body");
            var widthCalculator = $(document.createElement("div"));
            widthCalculator.addClass("tdk-ui-message-displayer-width-calculator");
            widthCalculator.css("display", "inline-block");
            body.append(widthCalculator);
            widthCalculator.hide();
        };
        var calculateWidth = function (component) {
            var widthCalculator = $("body").children(".tdk-ui-message-displayer-width-calculator").first();
            widthCalculator.empty();
            var maxWidth = widthCalculator.append(component.clone()).outerWidth(true);
            return maxWidth;
        };
        var calculateHeight = function (component) {
            var widthCalculator = $("body").children(".tdk-ui-message-displayer-width-calculator").first();
            widthCalculator.empty();
            var maxHeight = widthCalculator.append(component.clone()).outerHeight(true);
            return maxHeight;
        };
        var disposeWidthCalculator = function () {
            var widthCalculator = $("body").children(".tdk-ui-message-displayer-width-calculator").first();
            widthCalculator.remove();
        };

        return this.each(function () {
            initWidthCalculator();

            var parent = $(this);
            var container = $(document.createElement("div"));
            container.hide();
            container.addClass("container");
            container.addClass("tdk-ui-message-displayer");
            if (settings.DisableEyeCandy) {
                container.addClass(CLASS_DISABLE_EYE_CANDY);
            } else {
                container.addClass(CLASS_ENABLE_EYE_CANDY);
            }
            parent.append(container);

            if ((settings.Messages != null) && (settings.Messages.length > 0)) {
                //                var messageListToggler = $(document.createElement("div"));
                //                messageListToggler.addClass("message-list-toggler");
                //                var messageListTogglerIcon = $(document.createElement("img"))
                //                messageListTogglerIcon.attr("src", settings.ImageBasePath + "/scroller-down." + settings.ImageExtension);
                //                messageListTogglerIcon.addClass("message-list-toggler-icon");
                //                messageListToggler.append(messageListTogglerIcon);
                //                messageListToggler.css("left", (container.width() / 2) - (messageListToggler.width() / 2));
                //                container.append(messageListToggler);
                //                messageListToggler.hide();

                var messageListWrapper = $(document.createElement("div"));
                messageListWrapper.addClass("messages");
                var messageList = $(document.createElement("ul"));
                messageList.addClass("message-list");
                messageListWrapper.append(messageList);
                container.append(messageListWrapper);
                var messageListItem;

                var errorHeadlines = [];
                var warningHeadlines = [];
                var infoHeadlines = [];
                $.each(settings.Messages, function (index, value) {
                    if (value.Severity == SEVERITY_ERROR) {
                        errorHeadlines.push(value);
                    } else if (value.Severity == SEVERITY_WARNING) {
                        warningHeadlines.push(value);
                    } else if (value.Severity == SEVERITY_INFO) {
                        infoHeadlines.push(value);
                    }

                    messageListItem = $(document.createElement("li"));
                    messageListItem.addClass("message-list-item");
                    messageListItem.addClass(value.Severity);
                    messageListItem.html(value.Text);
                    messageList.append(messageListItem);

                    if ((index % 2) == 0) {
                        messageListItem.addClass("even");
                    }
                });

                var messageWrapperMaxHeight = $(window).height() / 2;
                var messageListHeight = calculateHeight(messageList);
                if (messageListHeight > messageWrapperMaxHeight) {
                    messageListWrapper.height(messageWrapperMaxHeight);
                    //                    var scroller = $(document.createElement("div"));
                    //                    scroller.addClass("scroller");
                    //                    container.append(scroller);
                    //
                    //                    var scrollerUp = $(document.createElement("div"));
                    //                    scrollerUp.addClass("scroller-up");
                    //                    scroller.append(scrollerUp);
                    //                    var scrollerUpIcon = $(document.createElement("img"));
                    //                    scrollerUpIcon.addClass("scroller-up-icon");
                    //                    scrollerUpIcon.attr("src", settings.ImageBasePath + "/scroller-up." + settings.ImageExtension);
                    //                    scrollerUp.append(scrollerUpIcon);
                    //
                    //                    var scrollerDown = $(document.createElement("div"));
                    //                    scrollerDown.addClass("scroller-down");
                    //                    scroller.append(scrollerDown);
                    //                    var scrollerDownIcon = $(document.createElement("img"));
                    //                    scrollerDownIcon.addClass("scroller-down-icon");
                    //                    scrollerDownIcon.attr("src", settings.ImageBasePath + "/scroller-down." + settings.ImageExtension);
                    //                    scrollerDown.append(scrollerDownIcon);
                    //
                    //                    scroller.css("top", (messageWrapperMaxHeight / 2) - (calculateHeight(scroller)/2));
                    //                    var SCROLLING_DELTA = 100;
                    //                    scrollerDown.click(function() {
                    //                        var top = parseInt(messageList.css("top"), 10);
                    //                        if(top > -(Math.abs(messageList.height() - messageWrapperMaxHeight))) {
                    //                            messageList.animate({
                    //                                top: "-=" + SCROLLING_DELTA + "px"
                    //                            }, "fast", function() {});
                    //                        }
                    //                    });
                }
                messageListWrapper.hide();

                var headlineMessages = [];
                var headlineSeverity;
                if (errorHeadlines.length > 0) {
                    headlineMessages = errorHeadlines;
                    headlineSeverity = SEVERITY_ERROR;
                } else if (warningHeadlines.length > 0) {
                    headlineMessages = warningHeadlines;
                    headlineSeverity = SEVERITY_WARNING;
                } else if (infoHeadlines.length > 0) {
                    headlineMessages = infoHeadlines;
                    headlineSeverity = SEVERITY_INFO;
                }

                var headline = $(document.createElement("div"));
                headline.addClass("headline");
                container.addClass(headlineSeverity);
                container.append(headline);
                var headlineTable = $(document.createElement("table"));
                headlineTable.addClass("headline-table");
                var headlineRow = $(document.createElement("tr"));
                headlineRow.addClass("headline-table-row");
                headlineTable.append(headlineRow);
                headline.append(headlineTable);

                var headlineColumn = $(document.createElement("td"));
                headlineColumn.addClass("headline-table-column icon");
                var headlineIcon = $(document.createElement("img"));
                headlineIcon.addClass("headline-icon");
                headlineIcon.attr("src", settings.ImageBasePath + "/headline-" + headlineSeverity + "." + settings.ImageExtension);
                headlineColumn.append(headlineIcon);
                headlineRow.append(headlineColumn);

                headlineColumn = $(document.createElement("td"));
                headlineColumn.addClass("headline-table-column text");
                var headlineText = $(document.createElement("span"));
                headlineText.addClass("headline-text")
                // Modified by FID)Kevin, 2014-03-21
				// Change hyperlink color
                headlineText.html(headlineMessages[0].Text.replace("<a", "<a style='color: #FFFFFF'"));
                headlineColumn.append(headlineText);
                headlineRow.append(headlineColumn);

                if (headlineMessages.length > 1) {
                    headlineColumn = $(document.createElement("td"));
                    headlineColumn.addClass("headline-table-column toggler");
                    var headlineToggler = $(document.createElement("img"));
                    headlineToggler.addClass("headline-toggler");
                    headlineToggler.attr("src", settings.ImageBasePath + "/headline-toggler." + settings.ImageExtension);
                    headlineColumn.append(headlineToggler);
                    headlineRow.append(headlineColumn);
                    headlineColumn.click(function (e) {
                        e.stopPropagation();
                        if (settings.DisableEyeCandy) {
                            headline.hide();
                            messageListWrapper.show();
                        } else {
                            container.animate({
                                bottom: -container.outerHeight(true)
                            }, "fast", function () {
                                headline.hide();
                                messageListWrapper.show();
                                container.animate({
                                    bottom: 0
                                }, "fast", function () { });
                            });
                        }
                    });
                }

                // Modified by FID)Kevin, 2014-03-21
                // Enable hide function with 1 item
                $("html").click(function () {
                    if (settings.DisableEyeCandy) {
                        if (messageListWrapper.is(":visible")) {
                            messageListWrapper.hide();
                            headline.show();
                        } else {
                            container.hide();
                        }
                    } else {
                        if (messageListWrapper.is(":visible")) {
                            container.animate({
                                bottom: -container.outerHeight(true)
                            }, "fast", function () {
                                messageListWrapper.hide();
                                headline.show();
                                container.animate({
                                    bottom: 0
                                }, "fast", function () { });
                            });
                        } else {
                            container.fadeOut("fast");
                        }
                    }
                });

                container.click(function (e) {
                    e.stopPropagation();
                });
            }

            if (settings.Messages != null) {
                if (settings.DisableEyeCandy) {
                    container.show();
                } else {
                    container.effect("bounce");
                }
            }

            disposeWidthCalculator();
        });
    };
})(jQuery)