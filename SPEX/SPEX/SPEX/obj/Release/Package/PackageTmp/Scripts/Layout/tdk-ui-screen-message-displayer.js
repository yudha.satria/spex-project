/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
    $.fn.tdkScreenMessageDisplayer = function (options) {
        var settings = $.extend({
            Height: 30,
            DetailText: "details",
            ImageBaseUrl: "../img",
            InfoIcon: "info.png",
            WarningIcon: "warning.png",
            ErrorIcon: "error.png"
        }, options);

        settings.InfoIcon = settings.ImageBaseUrl + "/" + settings.InfoIcon;
        settings.WarningIcon = settings.ImageBaseUrl + "/" + settings.WarningIcon;
        settings.ErrorIcon = settings.ImageBaseUrl + "/" + settings.ErrorIcon;

        return this.each(function () {
            var element = $(this);
            element.hide();
            element.addClass("tdk-common-ui-screen-message-displayer");
            element.css("position", "fixed");
            element.css("bottom", 0);
            element.css("left", 0);
            element.css("width", "100%");
            element.height(settings.Height);
            element.css("line-height", settings.Height + "px");
            element.css("z-index", 100000);
            element.css("overflow", "hidden");

            element.append("<div class='header'></div>");
            element.append("<div class='content'></div>");

            var messageList = element.children("ul:first");
            var counter = 0;
            var messageItems = messageList.children("li");
            messageItems.each(function () {
                var item = $(this);
                if ((counter % 2) === 0) {
                    item.addClass("even");
                } else {
                    item.addClass("odd");
                }
                counter++;
            });
            var headerClass = "info";
            if (messageItems.length > 0) {
                var firstItem = messageItems.first();
                if (firstItem.hasClass("warning")) {
                    headerClass = "warning";
                } else if (firstItem.hasClass("error")) {
                    headerClass = "error";
                }
            }

            var header = element.children(".header:first");
            header.css("position", "relative");
            header.css("z-index", 10000);
            header.height(settings.Height);
            header.append("<div class='snippet'></div><div class='toggler'></div>");
            var snippet = header.children(".snippet:first");
            snippet.css("float", "left");
            snippet.height(settings.Height);
            snippet.append("<div class='summary'></div>");
            snippet.append("<span>" + firstItem.html() + "</span>");

            var summary = snippet.children(".summary:first");
            summary.hide();
            summary.append("<span class='title'>Summary : </span> <div class='info'/><div class='warning'/><div class='error'/>");
            var info = summary.children(".info:first");
            var warning = summary.children(".warning:first");
            var error = summary.children(".error:first");

            var messageCount = messageList.children("li").size();
            var warningMessageCount = messageList.children("li.warning").size();
            var errorMessageCount = messageList.children("li.error").size();
            error.append(errorMessageCount);
            warning.append(warningMessageCount);
            info.append(messageCount - warningMessageCount - errorMessageCount);


            snippet.addClass(headerClass);
            header.addClass(headerClass);
            var snippetText = snippet.children("span:first");

            snippet.css("width", "85%");
            snippet.css("overflow", "hidden");
            var toggler = header.children(".toggler");
            toggler.append("expand");
            toggler.css("float", "right");
            toggler.css("display", "inline-block");
            toggler.addClass(headerClass);
            if (messageItems.length <= 1) {
                toggler.hide();
            }

            var content = element.children(".content:first");
            content.css("position", "absolute");
            content.css("z-index", 9000);
            content.css("overflow", "auto");
            content.append(messageList);

            toggler.mouseenter(function (e) {
                toggler.addClass("hover");
            });
            toggler.mouseleave(function (e) {
                toggler.removeClass("hover");
            });
            toggler.mousedown(function (e) {
                toggler.removeClass("hover");
                toggler.addClass("pressed");
            });
            toggler.mouseup(function (e) {
                toggler.removeClass("pressed");
                toggler.addClass("hover");

                var elementHeight = element.height();
                if (elementHeight <= settings.Height) {
                    element.animate({
                        height: content.height() + settings.Height
                    }, 250, "swing", function () {
                        toggler.removeClass("collapsed");
                        toggler.addClass("expanded");
                        toggler.text("collapse");
                        summary.show();
                        snippetText.hide();
                        snippet.removeClass(headerClass);
                        snippet.addClass("summary");

                        /* hack kecil sementara aja, ini ada bug */
                        snippet.css("background-image", "none");
                    });
                } else {
                    element.animate({
                        height: settings.Height
                    }, 100, "swing", function () {
                        toggler.text("expand");
                        element.effect({
                            effect: "bounce",
                            easing: "swing",
                            duration: 300,
                            complete: function () {
                                toggler.addClass("collapsed");
                                toggler.removeClass("expanded");
                                summary.hide();
                                snippetText.show();
                                snippet.addClass(headerClass);
                                snippet.removeClass("summary");

                                /* hack kecil sementara aja, ini ada bug */
                                if (headerClass === "info") {
                                    snippet.css("background-image", "url('" + settings.InfoIcon + "')");
                                } else if (headerClass === "warning") {
                                    snippet.css("background-image", "url('" + settings.WarningIcon + "')");
                                } else if (headerClass === "error") {
                                    snippet.css("background-image", "url('" + settings.ErrorIcon + "')");
                                }
                            }
                        });
                    });
                }
            });

            $(".tdk-common-ui-screen-message-displayer .header .snippet .summary .info").css("background-image", "url('" + settings.InfoIcon + "')");
            $(".tdk-common-ui-screen-message-displayer .content ul li").css("background-image", "url('" + settings.InfoIcon + "')");
            $(".tdk-common-ui-screen-message-displayer .header .snippet.info").css("background-image", "url('" + settings.InfoIcon + "')");

            $(".tdk-common-ui-screen-message-displayer .header .snippet .summary .warning").css("background-image", "url('" + settings.WarningIcon + "')");
            $(".tdk-common-ui-screen-message-displayer .content ul li.warning").css("background-image", "url('" + settings.WarningIcon + "')");
            $(".tdk-common-ui-screen-message-displayer .header .snippet.warning").css("background-image", "url('" + settings.WarningIcon + "')");

            $(".tdk-common-ui-screen-message-displayer .header .snippet .summary .error").css("background-image", "url('" + settings.ErrorIcon + "')");
            $(".tdk-common-ui-screen-message-displayer .header .snippet.error").css("background-image", "url('" + settings.ErrorIcon + "')");
            $(".tdk-common-ui-screen-message-displayer .content ul li.error").css("background-image", "url('" + settings.ErrorIcon + "')");

            element.effect({
                effect: "bounce",
                easing: "swing",
                duration: 300
            });
        });
    };
})(jQuery);

