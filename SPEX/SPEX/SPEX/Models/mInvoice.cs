﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using SPEX.Cls;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mInvoice
    {
        public string INVOICE_NO { get; set; }
        public DateTime? INVOICE_DT { get; set; }
        public string BUYER_NAME { get; set; }
        public string ADDRESS_01 { get; set; }
        public string ADDRESS_02 { get; set; }
        public string ADDRESS_03 { get; set; }
        public string PORT_OF_LOADING { get; set; }
        public string PORT_OF_DISCHARGE { get; set; }
        public string VESSEL_NAME { get; set; }
        public DateTime? ETD { get; set; }
        public string PAYMENT_TERM_01 { get; set; }
        public string PAYMENT_TERM_02 { get; set; }
        public string SPC_INST_01 { get; set; }
        public string SPC_INST_02 { get; set; }
        public string SPC_INST_03 { get; set; }
        public string SPC_INST_04 { get; set; }
        public string SPC_INST_05 { get; set; }
        public string TOTAL_QTY { get; set; }
        public decimal FOB_AMOUNT { get; set; }
        public decimal FREIGHT { get; set; }
        public decimal INSURANCE_AMOUNT { get; set; }
        public string PRICE_TERM_DESCR { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
        public decimal TOTAL_CASE { get; set; }
        public decimal GROSS_WEIGHT { get; set; }
        public decimal NET_WEIGHT { get; set; }
        public decimal MEASUREMENT { get; set; }
        public string KODE_SERI_KPJ { get; set; }
        public decimal RATE { get; set; }
        public decimal HARGA_JUAL { get; set; }
        public string NAMA_TERANG { get; set; }
        public string JABATAN { get; set; }

        //for detail
        public string SELLER_CODE { get; set; }
        public string BUYER_CODE { get; set; }
        public string BUYER_PD { get; set; }
        public string CURRENCY { get; set; }
        public string CASE_NO { get; set; }
        public string ORDER_NO { get; set; }
        //remaks agi 2016-12-17
        //public decimal ITEM_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public decimal INVOICE_QTY { get; set; }
        public decimal UNIT_PRICE { get; set; }
        public decimal AMOUNT { get; set; }
        public string TAM_INVOICE { get; set; }
        public string PACKING_COMPANY { get; set; }
        public string TRANSPORT_CD { get; set; }
        public string TRANSPORT_CD_VALUE { get; set; }
        public string SEND_INV_TMAP_STATUS { get; set; }
        public string SEND_SHIP_TMAP_STATUS { get; set; }
        public string SEND_INV_TAM_STATUS { get; set; }

        //ADD INVOICE BY SEA @20160509
        //header
        public string VANNING_NO { get; set; }
        public string BUYER_CD { get; set; }
        public decimal TOTAL_CONTAINER { get; set; }
        public decimal TOTAL_ITEM { get; set; }
        public string SEND_GTOPAS_STATUS { get; set; }
        public DateTime? VANNING_DT_FROM { get; set; }
        public DateTime? VANNING_DT_TO { get; set; }

        //detail
        public DateTime VANNING_DT { get; set; }
        public string CONTAINER_NO { get; set; }
        public string SEAL_NO { get; set; }


        //add by air
        public string DHL_AWB_NO { get; set; }
        public string SHIP_INSTRUCTION_NO { get; set; }
        public string DG_CARGO { get; set; }
        public string COUNTRY { get; set; }
        public string TRANS { get; set; }
        public string SPECIAL_INSTRUCTION { get; set; }
        public string SIGNATURE { get; set; }
        public decimal CORRECTION { get; set; }
        public decimal LENGTH { get; set; }
        public decimal WIDTH { get; set; }
        public decimal HEIGHT { get; set; }
        public DateTime? PACKAGE_DT { get; set; }
        public DateTime? ORDER_DT { get; set; }
        public bool IS_VALIDATION { get; set; }

        public string FORWARDING_AGENT { get; set; }

        public string Text { get; set; }
        public string SYSTEM_VALUE { get; set; }
        public string SYSTEM_DESC { get; set; }
        public string PEB_STATUS { get; set; }

        //add by agi 2016-08-11 for finance report
        public string P_INVOICE_DT { get; set; }
        public string INVOICE_DT_TO { get; set; }
        public string INVOICE_NO_TO { get; set; }

        public string BYAIR_FLIGHT { get; set; }
        public DateTime? DEPARTURE_ABOUT_1 { get; set; }
        public string BYAIR_CONNECTING_FLIGHT { get; set; }
        public DateTime? DEPARTURE_ABOUT_2 { get; set; }
        public string FORMD_STATUS { get; set; }

        public string PEB_REGISTER_NO { get; set; }
        public DateTime? PEB_REGISTER_DT { get; set; }
        public string FEEDER_VOYAGE { get; set; }

        //add agi 2017-04-06 For Good Issue Download Detail
        public decimal BYAIR_CASEAMOUNT { get; set; }
        public string HS_NO { get; set; }
        public decimal RVC { get; set; }

        //add agi 2017-09-11
        public string TRANSPORTATION_CD { get; set; }

        public string GetReportDBPath()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("Get_Report_DB_Path");
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault().Text : "";
        }

        //public List<mInvoice> getListInvoice(string pInvoiceNo, string pInvoiceNoTo, string pInvoiceDate, string pInvoiceDateTo,
        //    string p_PACKING_COMPANY, string p_SEND_SHIP_TMAP_STATUS, string p_SEND_INV_TMAP_STATUS, string p_SEND_INV_TAM_STATUS, string p_PEB_STATUS)
         public List<mInvoice> getListInvoice(string pInvoiceNo, string pInvoiceNoTo, string pInvoiceDate, string pInvoiceDateTo,
            string p_PACKING_COMPANY, string p_SEND_SHIP_TMAP_STATUS, string p_SEND_INV_TMAP_STATUS, string p_SEND_INV_TAM_STATUS, 
             string p_PEB_STATUS, string p_TRANSPORTATION_CD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mInvoice>("Invoice_Header_Search",
                new { INVOICE_NO = pInvoiceNo },
                new { INVOICE_NO_TO = pInvoiceNoTo },
                new { INVOICE_DT = pInvoiceDate },
                new { INVOICE_DT_TO = pInvoiceDateTo },
                new { PACKING_COMPANY = p_PACKING_COMPANY },
                new { SEND_SHIP_TMAP_STATUS = p_SEND_SHIP_TMAP_STATUS },
                new { SEND_INV_TMAP_STATUS = p_SEND_INV_TMAP_STATUS },
                new { SEND_INV_TAM_STATUS = p_SEND_INV_TAM_STATUS },
                new { PEB_STATUS = p_PEB_STATUS },
                //add agi 2017-09-2017
                 new { TRANSPORTATION_CD = p_TRANSPORTATION_CD }
                );
            db.Close();

            return l.ToList();
        }

        public List<mInvoice> getListInvoiceDetail(string pInvoiceNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mInvoice>("Invoice_Detail_Search",
                new { INVOICE_NO = pInvoiceNo }
                );
            db.Close();

            return l.ToList();
        }

        public mInvoice getInvoice(string pInvoiceNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_Get",
                new { INVOICE_NO = pInvoiceNo }
                ).ToList();
            db.Close();

            return result.Count > 0 ? result[0] : null;
        }

        public string DeleteData(string pINVOICE_NO)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("Invoice_Delete_Data",
                new
                {
                    INVOICE_NO = pINVOICE_NO
                }
                );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }

            return result;
        }
        //add agi 2017-03-03
        public string DeleteDataByUserId(string pINVOICE_NO,string pUSER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("Invoice_Delete_Data_By_UserId",
                new
                {
                    INVOICE_NO = pINVOICE_NO,
                    USER_ID=pUSER_ID
                }
                );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }

            return result;
        }

        //add by ria @20160510
        public mInvoice getNewInvoiceBySea(string pBuyerCD, string pPackingCompany, string pDetail, string pUserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_GetNewBySea",
                new { BUYER_CD = pBuyerCD },
                new { PACKING_COMPANY = pPackingCompany },
                new { DETAIL_PARAMS = pDetail },
                new { USER_ID = pUserID }
                ).ToList();
            db.Close();

            return result.Count > 0 ? result[0] : null;
        }

        public List<mInvoice> getInvoiceDetailBySea(string pInvoiceNo, string pPackingCompany, string detailParam, string pUserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_GetDetailBySea",
                new { INVOICE_NO = pInvoiceNo },
                new { PACKING_COMPANY = pPackingCompany },
                new { DETAIL_PARAMS = detailParam },
                new { USER_ID = pUserID }
                ).ToList();
            db.Close();

            return result;
        }

        public string SaveInvoiceBySea(string pInvoiceNo, DateTime pInvoiceDate, string pBuyerCD, string pBuyerName, string pVanningNo, string pVesselName, DateTime pETD,
            decimal pTotalContainer, decimal pTotalCase, decimal pTotalItem, decimal pTotalQty, decimal pMeasurement, decimal pFobAmount, decimal pNetWeight,
            decimal pGrossWeight, decimal pFreigt, decimal pInsuranceAmount, string pPackingCompany, string pFeederVoyage, string pDetailParam, string pIsMaintenance, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_SaveInvoiceBySea",
                    new { INVOICE_NO = pInvoiceNo },
                    new { INVOICE_DT = pInvoiceDate },
                    new { BUYER_CD = pBuyerCD },
                    new { BUYER_NAME = pBuyerName },
                    new { VANNING_NO = pVanningNo },
                    new { VESSEL_NAME = pVesselName },
                    new { ETD = pETD },
                    new { TOTAL_CONTAINER = pTotalContainer },
                    new { TOTAL_CASE = pTotalCase },
                    new { TOTAL_ITEM = pTotalItem },
                    new { TOTAL_QTY = pTotalQty },
                    new { MEASUREMENT = pMeasurement },
                    new { FOB_AMOUNT = pFobAmount },
                    new { NET_WEIGHT = pNetWeight },
                    new { GROSS_WEIGHT = pGrossWeight },
                    new { FREIGHT = pFreigt },
                    new { INSURANCE_AMOUNT = pInsuranceAmount },
                    new { PACKING_COMPANY = pPackingCompany },
                    new { FEEDER_VOYAGE = pFeederVoyage },
                    new { DETAIL_PARAM = pDetailParam },
                    new { IS_MAINTENANCE = pIsMaintenance },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public mInvoice getInvoiceBySea(string pInvoiceNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_GetBySea",
                new { INVOICE_NO = pInvoiceNo }
                ).ToList();
            db.Close();

            return result.Count > 0 ? result[0] : null;
        }

        public mInvoice getInvoiceByAir(string pInvoiceNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_GetByAir",
                new { INVOICE_NO = pInvoiceNo }
                ).ToList();
            db.Close();

            return result.Count > 0 ? result[0] : null;
        }

        //Remaks Agi 2016-12-08 add parameter vaning date from to
        //public mInvoice getNewInvoiceByAir(string pBuyerCD, string pPDCD, string pPackingCompany, string pCaseDangerFlag, string pUserID)
        public mInvoice getNewInvoiceByAir(string pBuyerCD, string pPDCD, string pPackingCompany, string pCaseDangerFlag, string pUserID, string pVANNING_DT_FROM, string pVANNING_DT_TO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_GetNewByAir",
                new { BUYER_CD = pBuyerCD },
                new { PD_CD = pPDCD },
                new { PACKING_COMPANY = pPackingCompany },
                new { CASE_DANGER_FLAG = pCaseDangerFlag },
                new { USER_ID = pUserID },
                //add agi 2016-12-08 add parameter vanning date to
                new { VANNING_DT_FROM =Convert.ToDateTime(pVANNING_DT_FROM) },
                new { VANNING_DT_TO = Convert.ToDateTime(pVANNING_DT_TO) }
                ).ToList();
            db.Close();

            return result.Count > 0 ? result[0] : null;
        }

        public List<mInvoice> getCaseListByAir(string pInvoiceNo, string pPackingCompany, string pUserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_GetCaseListByAir",
                new { INVOICE_NO = pInvoiceNo },
                new { PACKING_COMPANY = pPackingCompany },
                new { USER_ID = pUserID }
                ).ToList();
            db.Close();

            return result;
        }

        public string SaveInvoiceByAir(string pInvoiceNo, string pPackingCompany, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_SubmitInvoiceByAir",
                    new { INVOICE_NO = pInvoiceNo },
                    new { PACKING_COMPANY = pPackingCompany },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string SaveTempInvoiceByAir(string pInvoiceNo, string pPackingCompany, string pVesselName, DateTime pETD,
            DateTime pVanningDt, string pDHLAWBNo, string pForwardingAgent, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_SaveInvoiceByAir",
                    new { INVOICE_NO = pInvoiceNo },
                    new { PACKING_COMPANY = pPackingCompany },
                    new { VESSEL_NAME = pVesselName },
                    new { ETD = pETD },
                    new { VANNING_DT = pVanningDt },
                    new { DHL_AWB_NO = pDHLAWBNo },
                    new { FORWARDING_AGENT = pForwardingAgent },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string GetSystemValue(string pSystemCD)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_GetSystemValueByAir",
                    new { SYSTEM_CD = pSystemCD }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.SYSTEM_VALUE == null ? "" : message.SYSTEM_VALUE;
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public string GetSystemValue(string pSystemType, string pSystemCD)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_GetSystemValue",
                    new { SYSTEM_TYPE = pSystemType },
                    new { SYSTEM_CD = pSystemCD }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.SYSTEM_VALUE == null ? "" : message.SYSTEM_VALUE;
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }


        public double GetDateValidation(string pSystemCD)
        {
            return Convert.ToDouble(GetSystemValue(pSystemCD));
        }

        public bool GetDHLValidation(string pSystemCD)
        {
            return GetSystemValue(pSystemCD) == "1" ? true : false;
        }

        public List<mInvoice> GetVesselNameByAir()
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_GetVesselNameListByAir").ToList();
            db.Close();

            return result;
        }

        public mInvoice GetCaseByAir(string pInvoiceNo, string pCaseNo, string pPackingCompany, string pUserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_GetCaseByAir",
                new { INVOICE_NO = pInvoiceNo },
                new { CASE_NO = pCaseNo },
                new { PACKING_COMPANY = pPackingCompany },
                new { USER_ID = pUserID }
                ).ToList();
            db.Close();

            return result.Count > 0 ? result[0] : null;
        }

        public string UpdateCaseByAir(string pInvoiceNo, string pPackingCompany, string pCaseNo,
            decimal pGrossWeight, decimal pNetWeight, decimal pLength, decimal pWidth, decimal pHeight, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_UpdateCaseByAir",
                    new { INVOICE_NO = pInvoiceNo },
                    new { PACKING_COMPANY = pPackingCompany },
                    new { CASE_NO = pCaseNo },
                    new { CASE_GROSS_WEIGHT = pGrossWeight },
                    new { CASE_NET_WEIGHT = pNetWeight },
                    new { CASE_LENGTH = pLength },
                    new { CASE_WIDTH = pWidth },
                    new { CASE_HEIGHT = pHeight },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string DeleteCaseByAir(string pInvoiceNo, string pCaseNo, string pPackingCompany, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_DeleteCaseByAir",
                    new { INVOICE_NO = pInvoiceNo },
                    new { CASE_NO = pCaseNo },
                    new { PACKING_COMPANY = pPackingCompany },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string DeleteTempByAir(string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_DeleteTempByAir",
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public mInvoice getTempHeaderInvoiceByAir(string pInvoiceNo, string pPackingCompany ,string pUserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_GetTempHeaderByAir",
                new { INVOICE_NO = pInvoiceNo },
                new { PACKING_COMPANY = pPackingCompany },
                    new { USER_ID = pUserID }
                ).ToList();
            db.Close();

            return result.Count > 0 ? result[0] : null;
        }

        public string SendInvoiceTMAP(string pInvoiceNos, string pUserID)
        {
            string result = "error |error on send data.";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_SendInvoiceTMAP",
                    new { INVOICE_NO = pInvoiceNos },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string SendShipmentTMAP(string pInvoiceNos, string pUserID)
        {
            string result = "error |error on send data.";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_SendShipmentTMAP",
                    new { INVOICE_NO = pInvoiceNos },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string SendDummyInvoiceTAM(string pUserID)
        {
            string result = "error |error on send data.";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_SendDummyInvoiceTAM",
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string SendLockCheck(string pFunctionID)
        {
            string result = "error|error on check lock.";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_SendLockCheck",
                    new { FUNCTION_ID = pFunctionID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string SendValidation(string pFunctionID, string pInvoiceNo)
        {
            string result = "error|error on check lock.";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_SendValidation",
                    new { FUNCTION_ID = pFunctionID },
                    new { INVOICE_NO = pInvoiceNo }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public List<mInvoice> GetPackingCompany()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_GetPackingCompany").ToList();
            db.Close();

            return result;
        }

        public string CreatePEB(string pInvoiceNo, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_CreatePEB",
                    new { INVOICE_NO = pInvoiceNo },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string CreateFinanceReport(string pInvoiceNo, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_CreateFinanceReport",
                    new { INVOICE_NO = pInvoiceNo },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string CreateFormD(string pInvoiceNo, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.ExecuteScalar<string>("Invoice_CreateFormD",
                    new { INVOICE_NO = pInvoiceNo },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                result = l;
                //foreach (var message in l.ToList())
                //{
                //    result = message.Text;
                //}
            }
            catch (Exception ex)
            {
                Lock L = new Lock();
                L.UnlockFunction("F15-013");
                result = "exception|" + ex.Message + "|pid";
            }
            return result;
        }

        public List<mInvoiceFormD> GetFormD(string pInvoiceNo, string pUserID)
        {
            List<mInvoiceFormD> result = new List<mInvoiceFormD>();
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                result = db.Fetch<mInvoiceFormD>("Invoice_GetFormD",
                    new { INVOICE_NO = pInvoiceNo },
                    new { USER_ID = pUserID }).ToList();

                db.Close();
                return result;
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string UpdateInvoiceByAirHeader(mInvoice invoice)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_UpdateHeaderByAir",
                    new { INVOICE_NO = invoice.INVOICE_NO },
                    new { BYAIR_FLIGHT = invoice.BYAIR_FLIGHT },
                    new { BYAIR_CONNECTING_FLIGHT = invoice.BYAIR_CONNECTING_FLIGHT },
                    new { DEPARTURE_ABOUT_1 = invoice.DEPARTURE_ABOUT_1 },
                    new { DEPARTURE_ABOUT_2 = invoice.DEPARTURE_ABOUT_2 }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string SendPEBData(string pPROCESS_ID, string pREGISTER_NO, string pINVOICE_NO, string pName, string pDescription, string pSubmitter, string pFunctionName, string pParameter, string pType, string pStatus, string pCommand, string pStartDate, string pEndDate, string pPeriodicType, string pInterval, string pExecutionDays, string pExecutionMonths, string pTime)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "Success ";
            var l = db.Fetch<mPEB>("Invoice_SendPEBData",
                new
                {
                    PROCESS_ID = pPROCESS_ID,
                    REGISTER_NO = pREGISTER_NO,
                    INVOICE_NO = pINVOICE_NO,
                    NAME = pName,
                    DESCRIPTION = pDescription,
                    SUBMITTER = pSubmitter,
                    FUNCTION_NAME = pFunctionName,
                    PARAMETER = pParameter,
                    TYPE = pType,
                    STATUS = pStatus,
                    COMMAND = pCommand,
                    START_DATE = pStartDate,
                    END_DATE = pEndDate,
                    PERIODIC_TYPE = pPeriodicType,
                    INTERVAL = pInterval,
                    EXECUTION_DAYS = pExecutionDays,
                    EXECUTION_MONTHS = pExecutionMonths,
                    TIME = pTime
                });
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }
            return result;
        }

        public string SavePEBSegmentData(string pINVOICE_NO, string pREGISTER_NO, DateTime? pREGISTER_DT, DateTime? pINSPECTION_DT, string pINV_CONTAINER_COUNT, DateTime? pETD, string pPACKING_COMPANY, string pUSER_ID)
        {
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                string result = "SUCCESS|Save Success";
                var l = db.Fetch<mPEB>("Invoice_SavePEBSegment",
                    new
                    {
                        INVOICE_NO = pINVOICE_NO,
                        REGISTER_NO = pREGISTER_NO,
                        REGISTER_DT = pREGISTER_DT,
                        INSPECTION_DT = pINSPECTION_DT,
                        INV_CONTAINER_COUNT = pINV_CONTAINER_COUNT,
                        ETD = pETD,
                        PACKING_COMPANY = pPACKING_COMPANY,
                        USER_ID = pUSER_ID
                    }
                    );
                db.Close();

                //result = l.Text;
                foreach (var message in l)
                {
                    result = message.Text;
                }

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR|" + ex.Message;
            }

        }

        public string CancelPEBSegmentData(string pREGISTER_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mPEB>("Invoice_CancelPEBSegment",
                new
                {
                    REGISTER_NO = pREGISTER_NO
                }
                );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }

        public List<mPEB> GetListPEBContainer(string pINVOICE_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("Invoice_GetPEBSegmentContainer",
                new { INVOICE_NO = pINVOICE_NO }
                );
            db.Close();

            return l.ToList();
        }

        public mPEB GetPEBSegment(string pINVOICE_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            mPEB returnValue = new mPEB();
            var l = db.Fetch<mPEB>("Invoice_GetPEBSegment",
                new { INVOICE_NO = pINVOICE_NO }
                );
            foreach (var message in l.ToList())
            {
                returnValue = (mPEB)message;
            }
            db.Close();

            return returnValue;
        }

        public string CreateFormDProd(string pInvoiceNo, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.ExecuteScalar<string>("Invoice_CreateFormDProd",
                    new { INVOICE_NO = pInvoiceNo },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                result = l;
            }
            catch (Exception ex)
            {
                Lock L = new Lock();
                L.UnlockFunction("F15-013");
                result = "exception|" + ex.Message + "|pid";
            }
            return result;
        }

        public mInvoice getInvoiceByAirEdit(string pInvoiceNo, string pUserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("Invoice_GetByAirEdit",
                new { INVOICE_NO = pInvoiceNo },
                new { USER_ID = pUserID }
                ).ToList();
            db.Close();

            return result.Count > 0 ? result[0] : null;
        }

        public string SaveInvoiceByAirEdit(string p_INVOICE_NO, decimal p_NET_WEIGHT, decimal p_GROSS_WEIGHT,
            decimal p_MEASUREMENT, string p_FORWARDING_AGENT, string p_DHL_AWB_NO, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_SubmitInvoiceByAirEdit",
                    new { INVOICE_NO = p_INVOICE_NO },
                    new { GROSS_WEIGHT = p_GROSS_WEIGHT },
                    new { NET_WEIGHT = p_NET_WEIGHT },
                    new { MEASUREMENT = p_MEASUREMENT },
                    new { FORWARDING_AGENT = p_FORWARDING_AGENT },
                    new { DHL_AWB_NO = p_DHL_AWB_NO },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string GetNewFAByAir(string pInvoiceNo, string pPackingCompany, decimal pGrossWeight, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_GetFAByAir",
                    new { INVOICE_NO = pInvoiceNo },
                    new { PACKING_COMPANY = pPackingCompany },
                    new { GROSS_WEIGHT = pGrossWeight },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.FORWARDING_AGENT;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string PEBValidation(string pInvoiceNo)
        {
            string result = "error|error on check lock.";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("Invoice_PEBValidation",
                    new { INVOICE_NO = pInvoiceNo }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

    }

    public class mInvoiceFormD
    {
        public string INVOICE_NO { get; set; }
        public string ROW_NO { get; set; }
        public string FIELD_A { get; set; }
        public string FIELD_B { get; set; }
        public string FIELD_C { get; set; }
        public string FIELD_D { get; set; }
        public string FIELD_E { get; set; }
        public string FIELD_F { get; set; }
        public string FIELD_G { get; set; }
        public string FIELD_H { get; set; }
        public string FIELD_I { get; set; }
        public string FIELD_J { get; set; }
        public string FIELD_K { get; set; }
        public string FIELD_L { get; set; }
        public string FIELD_M { get; set; }
        public string FIELD_N { get; set; }
        public string FIELD_O { get; set; }
        public string FIELD_P { get; set; }
        public string FIELD_Q { get; set; }
        public string FIELD_R { get; set; }
        public string FIELD_S { get; set; }
        public string FIELD_T { get; set; }
        public string FIELD_U { get; set; }
        public string FIELD_V { get; set; }
        public string FIELD_W { get; set; }
        public string FIELD_X { get; set; }
        public string FIELD_Y { get; set; }
        public string FIELD_Z { get; set; }
        public string FIELD_AA { get; set; }
        public string FIELD_AB { get; set; }
        public string FIELD_AC { get; set; }
        public string FIELD_AD { get; set; }
        public string FIELD_AE { get; set; }
        public string FIELD_AF { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}