﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using SPEX.Cls;

namespace SPEX.Models
{
    public class mPackingCompInquiryParam {
        public string TMAP_ORDER_NO { get; set; }
        public string CASE_NO { get; set; }
        public string BUYER_PD { get; set; }
        public string ITEM_NO { get; set; }
        public string TMMIN_ORDER_NO { get; set; }
        public string HANDLING_TYPE { get; set; }
        public string PRIVILEGE { get; set; }
        public string STATUS { get; set; }
        public string PART_NO { get; set; }
        public string TRANSP_CODE { get; set; }
        public string CONTAINER_NO { get; set; }
        public string ORDER_DT_FR { get; set; }
        public string ORDER_DT_TO { get; set; }
        public string PACKING_DT_FR { get; set; }
        public string PACKING_DT_TO { get; set; }
        public string VANNING_DT_FR { get; set; }
        public string VANNING_DT_TO { get; set; }
        //add agi 2017-10-25
        #region process date
        public string PROCESS_DT_FROM { get; set; }
        public string PROCESS_DT_TO{ get; set; }
        #endregion 

    }

    public class mPackingCompInquiryDownload
    {
        public string NO { get; set; }
        public string CASE_NO { get; set; }
        public string CASE_TYPE { get; set; }
        public string PACKING_DT { get; set; }
        public string TMAP_ORDER_NO { get; set; }
        public string TMAP_ITEM_NO { get; set; }
        public string TMAP_PART_NO { get; set; }
        public string PART_NO { get; set; }
        public string PACKING_QTY { get; set; }
        public string TMAP_ORDER_DT { get; set; }
        public string TMMIN_ORDER_NO { get; set; }
        public string TRANSP_CD { get; set; }
        public string BUYER_PD { get; set; }
        public string PRIVILEGE { get; set; }
        public string CASE_GW { get; set; }
        public string CASE_NW { get; set; }
        public string CASE_LENGTH { get; set; }
        public string CASE_WIDTH { get; set; }
        public string CASE_HEIGHT { get; set; }
        public string CONTAINER_NO { get; set; }
        public string VANNING_DT { get; set; }
    }

    public class mPackingCompInquiry
    {
        public string CASE_NO { get; set; }
        public string CASE_TYPE { get; set; }
        public string PACKING_DT { get; set; }
        public string PREPARED_DT { get; set; }
        public string PACKING_QTY { get; set; }
        public string TRANSP_CD { get; set; }
        public string BUYER_PD { get; set; }
        public string PRIVILEGE { get; set; }
        public string HANDLING_TYPE { get; set; }
        public string CASE_GW { get; set; }
        public string CASE_NW { get; set; }
        public string CONTAINER_NO { get; set; }
        public string VANNING_DT { get; set; }
        public string STATUS { get; set; }

        //[R-20170825001 by FID.Arri] Removed history data
        //public string DATA_TYPE { get; set; }
        //[R-20170825001 by FID.Arri] End

        public List<mPackingCompInquiry> GetData(mPackingCompInquiryParam PCI, String Mode)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mPackingCompInquiry> l = db.Fetch<mPackingCompInquiry>("PackingCompInquirySearch",
                new {
                    TMAP_ORDER_NO = PCI.TMAP_ORDER_NO,
                    CASE_NO = PCI.CASE_NO,
                    BUYER_PD = PCI.BUYER_PD,
                    ITEM_NO = PCI.ITEM_NO,
                    TMMIN_ORDER_NO = PCI.TMMIN_ORDER_NO,
                    HANDLING_TYPE = PCI.HANDLING_TYPE,
                    PRIVILEGE = PCI.PRIVILEGE,
                    STATUS = PCI.STATUS,
                    PART_NO = PCI.PART_NO,
                    TRANSP_CODE = PCI.TRANSP_CODE,
                    CONTAINER_NO = PCI.CONTAINER_NO,
                    ORDER_DT_FR = PCI.ORDER_DT_FR,
                    ORDER_DT_TO = PCI.ORDER_DT_TO,
                    PACKING_DT_FR = PCI.PACKING_DT_FR,
                    PACKING_DT_TO = PCI.PACKING_DT_TO,
                    VANNING_DT_FR = PCI.VANNING_DT_FR,
                    VANNING_DT_TO = PCI.VANNING_DT_TO,
                    MODE = Mode
                        //add agi 2017-10-25
                    #region process date
                    ,PROCESS_DT_FROM=PCI.PROCESS_DT_FROM
                    ,PROCESS_DT_TO=PCI.PROCESS_DT_TO
                    #endregion
                }).ToList();
            db.Close();
            return l;
        }

        public List<mPackingCompInquiryDownload> GetDataDownload(mPackingCompInquiryParam PCI)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mPackingCompInquiryDownload> l = db.Fetch<mPackingCompInquiryDownload>("PackingCompInquirySearch",
                new
                {
                    TMAP_ORDER_NO = PCI.TMAP_ORDER_NO,
                    CASE_NO = PCI.CASE_NO,
                    BUYER_PD = PCI.BUYER_PD,
                    ITEM_NO = PCI.ITEM_NO,
                    TMMIN_ORDER_NO = PCI.TMMIN_ORDER_NO,
                    HANDLING_TYPE = PCI.HANDLING_TYPE,
                    PRIVILEGE = PCI.PRIVILEGE,
                    STATUS = PCI.STATUS,
                    PART_NO = PCI.PART_NO,
                    TRANSP_CODE = PCI.TRANSP_CODE,
                    CONTAINER_NO = PCI.CONTAINER_NO,
                    ORDER_DT_FR = PCI.ORDER_DT_FR,
                    ORDER_DT_TO = PCI.ORDER_DT_TO,
                    PACKING_DT_FR = PCI.PACKING_DT_FR,
                    PACKING_DT_TO = PCI.PACKING_DT_TO,
                    VANNING_DT_FR = PCI.VANNING_DT_FR,
                    VANNING_DT_TO = PCI.VANNING_DT_TO,
                    MODE = "Download"
                    //add agi 2017-10-25
                    #region process date
                    ,PROCESS_DT_FROM=PCI.PROCESS_DT_FROM
                    ,PROCESS_DT_TO=PCI.PROCESS_DT_TO
                    #endregion
                }).ToList();
            db.Close();
            return l;
        }

        public List<mPackingCompInquiry> GetDataHistory(mPackingCompInquiryParam PCI, String Mode)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mPackingCompInquiry> l = db.Fetch<mPackingCompInquiry>("PackingCompInquiryHistorySearch",
                new
                {
                    TMAP_ORDER_NO = PCI.TMAP_ORDER_NO,
                    CASE_NO = PCI.CASE_NO,
                    BUYER_PD = PCI.BUYER_PD,
                    ITEM_NO = PCI.ITEM_NO,
                    TMMIN_ORDER_NO = PCI.TMMIN_ORDER_NO,
                    HANDLING_TYPE = PCI.HANDLING_TYPE,
                    PRIVILEGE = PCI.PRIVILEGE,
                    STATUS = PCI.STATUS,
                    PART_NO = PCI.PART_NO,
                    TRANSP_CODE = PCI.TRANSP_CODE,
                    CONTAINER_NO = PCI.CONTAINER_NO,
                    ORDER_DT_FR = PCI.ORDER_DT_FR,
                    ORDER_DT_TO = PCI.ORDER_DT_TO,
                    PACKING_DT_FR = PCI.PACKING_DT_FR,
                    PACKING_DT_TO = PCI.PACKING_DT_TO,
                    VANNING_DT_FR = PCI.VANNING_DT_FR,
                    VANNING_DT_TO = PCI.VANNING_DT_TO,
                    MODE = Mode
                }).ToList();
            db.Close();
            return l;
        }

        public List<String> GetBuyerMasterList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<String>("GetListBuyerMaster",
                  new { }
                  );
            db.Close();

            return l.ToList();
        }
    }

    public class mTransp {
        public string TRANSPORTATION_CD { get; set; }
        public string TRANSPORTATION_DESC { get; set; }

        public List<mTransp> GetTranspList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mTransp>("GetListTransMaster",
                  new { }
                  );
            db.Close();

            return l.ToList();
        }
    }

    public class mPackingCompInquiryDet
    {
        public string DET_KANBAN_ID { get; set; }
        public string DET_CASE_NO { get; set; }
        public string DET_ITEM_NO { get; set; }
        public string DET_TMAP_ORDER_NO { get; set; }
        public string DET_PART_NO { get; set; }
        public string DET_TMAP_PART_NO { get; set; }
        public string DET_ORDER_DATE { get; set; }
        public string DET_TMMIN_ORDER_NO { get; set; }
        public string DET_KANBAN_STATUS { get; set; }
        public string DET_DATA_TYPE { get; set; }

        public List<mPackingCompInquiryDet> GetData(string pCaseNo, string pCaseType, string pPreparedDT, string pKanbanID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mPackingCompInquiryDet> l = db.Fetch<mPackingCompInquiryDet>("PackingCompInquiryDetSearch",
                new
                {
                    CASE_NO = pCaseNo,
                    CASE_TYPE = pCaseType,
                    PREPARE_DT = pPreparedDT,
                    KANBAN_ID = pKanbanID
                }).ToList();
            db.Close();
            return l;
        }

        public List<mPackingCompInquiryDet> GetDataHistory(string pCaseNo, string pCaseType, string pPackingDT, string pKanbanID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mPackingCompInquiryDet> l = db.Fetch<mPackingCompInquiryDet>("PackingCompInquiryDetHistorySearch",
                new
                {
                    CASE_NO = pCaseNo,
                    CASE_TYPE = pCaseType,
                    PACKING_DT = pPackingDT,
                    KANBAN_ID = pKanbanID
                }).ToList();
            db.Close();
            return l;
        }
    }
}