﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mDistributionMaster
    {
        public string DISTRIBUTOR_CD { get; set; }
        public string OLD_DISTRIBUTOR_CD { get; set; }
        public string DISTRIBUTOR_NAME { get; set; }
        public string DISTRIBUTOR_COUNTRY { get; set; }
        public string BUYER_CD { get; set; }
        public string SELLER_CD { get; set; }
        public string VENDOR_CD { get; set; }
        public string PAYMENT_TERM { get; set; }
        public string PRICE_TERM { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string Text {get; set;} 


        //Calling data will be search and loaded (by SQL)
        public List<mDistributionMaster> getListDistributionMaster(string pDistributorCode, string pDistributorName, string pDistributorCountry)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var list = db.Fetch<mDistributionMaster>("GetDistributionMaster",
                new object[] { pDistributorCode, pDistributorName, pDistributorCountry });
            db.Close();
            return list.ToList();
        }

        //Get data for download
        public List<mDistributionMaster> DistributionMasterDownload(string D_DistributorCode, string D_DistributorName, string D_DistributorCountry)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var list = db.Fetch<mDistributionMaster>("GetDistributionMaster",
                new object[] { D_DistributorCode, D_DistributorName, D_DistributorCountry });
            db.Close();
            return list.ToList();
        }

        //Get data for combobox
        public List<mDistributionMaster> getListDistributorCode()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var list = db.Fetch<mDistributionMaster>("GetDistributorCode");
            db.Close();
            return list.ToList();
        }

        //Add Data
        public string SaveData(string pDistributorCode, string pOldDistributorCode, string pDistributorName, string pDistributorCountry, string pBuyerCode, string pSellerCode, string pVendorCode, string pPaymentTerm, string pPriceTerm, string pCREATED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mDistributionMaster>("DistributionMaster_Save", new
                {
                    DISTRIBUTOR_CD = pDistributorCode,
                    OLD_DISTRIBUTOR_CD = pOldDistributorCode,
                    DISTRIBUTOR_NAME = pDistributorName,
                    DISTRIBUTOR_COUNTRY = pDistributorCountry,
                    BUYER_CD = pBuyerCode,
                    SELLER_CD = pSellerCode,
                    VENDOR_CD = pVendorCode,
                    PAYMENT_TERM = pPaymentTerm,
                    PRICE_TERM = pPriceTerm,
                    CREATED_BY = pCREATED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        //Update Data
        public string SaveDataUpdate(string pDistributorCode, string pOldDistributorCode, string pDistributorName, string pDistributorCountry, string pBuyerCode, string pSellerCode, string pVendorCode, string pPaymentTerm, string pPriceTerm, string pCHANGED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mDistributionMaster>("DistributionMaster_Update", new
                {
                    DISTRIBUTOR_CD = pDistributorCode,
                    OLD_DISTRIBUTOR_CD = pOldDistributorCode,
                    DISTRIBUTOR_NAME = pDistributorName,
                    DISTRIBUTOR_COUNTRY = pDistributorCountry,
                    BUYER_CD = pBuyerCode,
                    SELLER_CD = pSellerCode,
                    VENDOR_CD = pVendorCode,
                    PAYMENT_TERM = pPaymentTerm,
                    PRICE_TERM = pPriceTerm,
                    CHANGED_BY = pCHANGED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        //DeleteData
        public string DeleteData(string p_DistributorCode)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                if (p_DistributorCode.Contains(";"))
                {
                    string[] DistributorCodeList = p_DistributorCode.Split(';');
                    for (int i = 0; i < DistributorCodeList.Length; i++)
                    {
                        var l = db.Fetch<mDistributionMaster>("DistributionMaster_Delete", new
                        {
                            DISTRIBUTOR_CD = DistributorCodeList[i],
                            MSG_TEXT = ""
                        });
                        foreach (var message in l)
                        {
                            result = message.Text;
                        }
                    }
                }
                else
                {
                    var l = db.Fetch<mDistributionMaster>("DistributionMaster_Delete", new
                    {
                        DISTRIBUTOR_CD = p_DistributorCode,
                        MSG_TEXT = ""
                    });
                    foreach (var message in l)
                    {
                        result = message.Text;
                    }
                }
                db.Close();

            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }
           

                        

    }
}
