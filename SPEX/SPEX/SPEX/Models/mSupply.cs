﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mSupply
    {
        public string BUYERPD_CD { get; set; }
        public string ORDER_NO { get; set; }
        public DateTime? ORDER_DT { get; set; }
        public string PART_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string ORDER_TYPE { get; set; }
        public string TRANSPORTATION_CD { get; set; }
        public string ORDER_QTY { get; set; }
        public string ACCEPT_QTY { get; set; }
        //public string REJECT_QTY { get; set; }
        public string PROCESS_PART_NO { get; set; }
        public string PROCESS_QTY { get; set; }
        //public string BO_QTY { get; set; }
        //public string ETD_BO { get; set; }
        public string SUPPLY_QTY { get; set; }
        public DateTime? PROCESS_DATE { get; set; }
        public string SUPPLY_INV_NO { get; set; }

        public string SUPPLY_PART_NO { get; set; }
        public string PACKING_QTY { get; set; }//Cargo Ready Qty
        public string SHIPMENT_QTY { get; set; }
        public DateTime? SHIPMENT_DATE { get; set; }
        public string SUPPLY_REMARK { get; set; }


        public string PACKED_QTY { get; set; }
        public DateTime? PROCESS_DT { get; set; }
        public string INV_NO { get; set; }
        public string REMARK { get; set; }


        public string Text { get; set; }



        public List<mSupply> getListSupply(string pOrderNo, string pPartNo, string pItemNo, string pOrderDateFrom, string pOrderDateTo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mSupply> ("ETD_SUPPLY_SEARCH",
                 new { ORDER_NO = pOrderNo },
                  new { PART_NO = pPartNo },
                 
                  new { ITEM_NO = pItemNo },
                  new { ORDER_DT_FROM = pOrderDateFrom },
                  new { ORDER_DT_TO = pOrderDateTo }
                  );
            db.Close();
            return l.ToList();

        }


        public List<mSupply> SupplyDownload(string D_PartNo, string D_OrderNo, string D_ItemNo, string D_OrderDt, string D_OrderDtTo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mSupply>("SupplyDownload", new
            {
                PART_NO = D_PartNo,
                ORDER_NO = D_OrderNo,
                ITEM_NO = D_ItemNo,
                ORDER_DT_FROM = D_OrderDt,
                ORDER_DT_TO = D_OrderDtTo


            });

            if (D_PartNo == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }

        public List<mSupply> SupplyDownloadTicked(string D_KeyDownload)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mSupply>("SupplyDownloadTicked", new
            {
                D_KeyDownload = D_KeyDownload


            });

            if (D_KeyDownload == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }



    }
}
