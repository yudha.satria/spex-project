﻿using System;

namespace SPEX.Models
{
    public abstract class Singleton<TClass, TInterface> where TClass : class, TInterface, new()
    {
        /**Edited**/
        private static volatile TClass instance = new TClass();
        private static object syncRoot = new Object();

        public static TInterface Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new TClass();
                    }
                }
                return instance;
            }
        }
    }
}