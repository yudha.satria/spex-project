﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mOrderInquiry
    {
        //Header
        public string TMAP_ORDER_NO { get; set; }
        public DateTime? TMAP_ORDER_DT { get; set; }
        public DateTime? TMMIN_REC_DT { get; set; }
        public DateTime? TMMIN_SEND_DT { get; set; }
        public DateTime? TMMIN_DELIVERY_PLAN_DT { get; set; }
        public string VALIDATION_FLAG { get; set; }
        public string ADJUST_FLAG { get; set; }
        public int ADJUST_QTY { get; set; }
        public int ACCEPT_QTY { get; set; }
        public string REMARKS { get; set; }
        public string BUYERPD_CD { get; set; }
        public string TRANSPORTATION_CD { get; set; }
        public string ORDER_TYPE { get; set; }
        public string PAYMENT_TERM_CD { get; set; }
        public string PRICE_TERM_CD { get; set; }
        public string CURRENCY_CD { get; set; }

        //detail
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public int ORDER_QTY { get; set; }
        public string ICR_ORDER_EXPORT_H { get; set; }
        public string ICR_ORDER_DT_FORMAT { get; set; }
        //public string ICR_TRANSPORTANTION_CD { get; set; }
        //public string ICR_BO_TYPE { get; set; }
        public string ICR_BO_INSTRUCTION { get; set; }
        public string ICR_ORDER_QTY { get; set; }
        public string ICR_DUPLICATE_ORDER_ITEM { get; set; }
        public string ICR_ORDER_NO { get; set; }
        public string ICR_ITEM_NO { get; set; }
        public string ICH_PAYMENT_TERM_CD { get; set; }
        public string ICH_PRICE_TERM_CD { get; set; }
       // public string ICH_DISTRIBUTION { get; set; }
        public string ICH_PDCODE { get; set; }
        public string ICH_BUYERPD { get; set; }
        public string ICW_PARTFRANCHCDA { get; set; }
        public string ICW_PARTFRANCHCDF { get; set; }
        //public string ICW_PRICE { get; set; }
        public string ICW_ORDER_DT_IN_RANGE { get; set; }

        public string ICR_ORDER_DATA { get; set; }
        public string ICR_TRANSPORTANTION_CD { get; set; }
        public string ICR_BO_TYPE { get; set; }
        public string ICH_TERM_MASTER { get; set; }
        public string ICH_DISTRIBUTION { get; set; }
        public string ICH_BUYER_PD { get; set; }
        public string ICW_PRICE { get; set; }
        public string ICW_FRANCHCD { get; set; }
        
        //order type
        public string BUYER_ORDER_TYPE { get; set; }
        public string ORDER_TYPE_DESCRIPTION { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string Text { get; set; }
        public int TOTAL { get; set; }
        public int TOTAL_WARNING { get; set; }
        public int TOTAL_REPROCESS { get; set; }
        public int TOTAL_ORDERACK { get; set; }
        public int RCV_LINE { get; set; }
        public int RCV_QTY { get; set; }
        public int ACCEPT_LINE { get; set; }
        //public int ACCEPT_QTY { get; set; }
        public int REJECT_LINE { get; set; }
        public int REJECT_QTY { get; set; }
        public int CANCEL_LINE { get; set; }
        public int CANCEL_QTY { get; set; }
        public int HOLD_LINE { get; set; }
        public int HOLD_QTY { get; set; }
        public string HEADER_ATTN { get; set; }
        public string HEADER_TO { get; set; }
        public string HEADER_CC { get; set; }

        public string TMMIN_PACKING { get; set; }
        public string PACKING_COMPANY { get; set; }
        public string PART_TYPE { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_PLANT { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PMSP_FLAG { get; set; }
        public int ALREADY_PACKING_QTY { get; set; }
        public int REMAIN_PACKING_QTY { get; set; }

        //add field agi 2017-07-12
        public string OBR_STATUS { get; set; }
        public string OBR_STATUS_NAME { get; set; }
        //Get data order
        //public List<mOrderInquiry> getListOrderInquiry(string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_TMAP_ORDER_NO, string p_ITEM_NO, string p_PART_NO,
        //    string p_ORDER_TYPE, string p_VALIDATION_FLAG, string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_TMMIN_DEL_PLAN_DT, string p_TMMIN_DEL_PLAN_DT_TO,
        //    string p_SEND_DT, string p_SEND_DT_TO, string p_SEND_TO_TAM, string p_PACKING_COMPANY, string p_PART_TYPE, string p_SUPPLIER)
        public List<mOrderInquiry> getListOrderInquiry(string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_TMAP_ORDER_NO, string p_ITEM_NO, string p_PART_NO,
            string p_ORDER_TYPE, string p_VALIDATION_FLAG, string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_TMMIN_DEL_PLAN_DT, string p_TMMIN_DEL_PLAN_DT_TO,
            string p_SEND_DT, string p_SEND_DT_TO, string p_SEND_TO_TAM, string p_PACKING_COMPANY, string p_PART_TYPE, string p_SUPPLIER, string p_SUB_SUPPLIER, string p_OBR_STATUS)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderInquiry>("OrderInquiry_Search",

                  new { TMAP_ORDER_DT = p_TMAP_ORDER_DT },
                  new { TMAP_ORDER_DT_TO = p_TMAP_ORDER_DT_TO },
                  new { TMAP_ORDER_NO = p_TMAP_ORDER_NO },
                  new { PART_NO = p_PART_NO },
                  new { ITEM_NO = p_ITEM_NO },
                  new { ORDER_TYPE = p_ORDER_TYPE },
                  new { VALIDATION_FLAG = p_VALIDATION_FLAG },
                  new { TMMIN_REC_DT = p_TMMIN_REC_DT },
                  new { TMMIN_REC_DT_TO = p_TMMIN_REC_DT_TO },
                  new { TMMIN_DEL_PLAN_DT = p_TMMIN_DEL_PLAN_DT },
                  new { TMMIN_DEL_PLAN_DT_TO = p_TMMIN_DEL_PLAN_DT_TO },
                  new { SEND_DT = p_SEND_DT },
                  new { SEND_DT_TO = p_SEND_DT_TO },
                  new { SEND_TO_TAM = p_SEND_TO_TAM },
                  new { PACKING_COMPANY = p_PACKING_COMPANY },
                  new { PART_TYPE = p_PART_TYPE },
                  new { SUPPLIER = p_SUPPLIER },
                  new { SUB_SUPPLIER = p_SUB_SUPPLIER },

                  //add agi 2017-07-12
                  new { OBR_STATUS=p_OBR_STATUS}
                  );
            db.Close();
            return l.ToList();
        }

        //Gridlookup OrderType
        public List<mOrderInquiry> getListOrderType()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderInquiry>("OrderType_Select");
            db.Close();
            return l.ToList();
        }

        //Get data download
        public List<mOrderInquiry> getListOrderInquiry_Download(string p_Order)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mOrderInquiry>("OrderInquiry_Download",
                new { p_Order = p_Order }
                  );

            db.Close();
            return l.ToList();
        }

        //Update Data
        public string UpdateData(string p_TMAP_ORDER_NO, string p_ITEM_NO, string p_PART_NO, string p_ORDER_QTY, string p_ADJUST_QTY, string p_ACCEPT_QTY, string p_VALIDATION_FLAG, string p_REMARKS, string p_CHANGED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mOrderInquiry>("OrderInquiry_Update", new
                {
                    TMAP_ORDER_NO = p_TMAP_ORDER_NO,
                    ITEM_NO = p_ITEM_NO,
                    PART_NO = p_PART_NO,
                    ORDER_QTY = p_ORDER_QTY,
                    ADJUST_QTY = p_ADJUST_QTY,
                    ACCEPT_QTY = p_ACCEPT_QTY,
                    VALIDATION_FLAG = p_VALIDATION_FLAG,
                    REMARKS = p_REMARKS,
                    CHANGED_BY = p_CHANGED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        //Insert Parameter to TB_R_BACKGROUNDTASK_REGISTRY
        public string InsertParam(string pID, string pName, string pDescription, string pSubmitter, string pFunctionName, string pParameter, string pType, string pStatus, string pCommand, string pStartDate, string pEndDate, string pPeriodicType, string pInterval, string pExecutionDays, string pExecutionMonths, string pTime)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mOrderInquiry>("OrderInquiry_SendOrder", new
                {
                    ID = pID,
                    NAME = pName,
                    DESCRIPTION = pDescription,
                    SUBMITTER = pSubmitter,
                    FUNCTION_NAME = pFunctionName,
                    PARAMETER = pParameter,
                    TYPE = pType,
                    STATUS = pStatus,
                    COMMAND = pCommand,
                    START_DATE = pStartDate,
                    END_DATE = pEndDate,
                    PERIODIC_TYPE = pPeriodicType,
                    INTERVAL = pInterval,
                    EXECUTION_DAYS = pExecutionDays,
                    EXECUTION_MONTHS = pExecutionMonths,
                    TIME = pTime,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        //Insert Parameter to TB_R_BACKGROUNDTASK_REGISTRY
        public string InsertParam_synchronize(string pID, string pName, string pDescription, string pSubmitter, string pFunctionName, string pParameter, string pType, string pStatus, string pCommand, string pStartDate, string pEndDate, string pPeriodicType, string pInterval, string pExecutionDays, string pExecutionMonths, string pTime)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mOrderInquiry>("OrderInquiry_Synchronize", new
                {
                    ID = pID,
                    NAME = pName,
                    DESCRIPTION = pDescription,
                    SUBMITTER = pSubmitter,
                    FUNCTION_NAME = pFunctionName,
                    PARAMETER = pParameter,
                    TYPE = pType,
                    STATUS = pStatus,
                    COMMAND = pCommand,
                    START_DATE = pStartDate,
                    END_DATE = pEndDate,
                    PERIODIC_TYPE = pPeriodicType,
                    INTERVAL = pInterval,
                    EXECUTION_DAYS = pExecutionDays,
                    EXECUTION_MONTHS = pExecutionMonths,
                    TIME = pTime,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        //Gridlookup OrderType
        public int getValidate()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderInquiry>("OrderSending_Validation_SendDT");
            db.Close();
            return l.ToList()[0].TOTAL;
        }

        public int getValidate_Warning()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderInquiry>("OrderSending_Validation_Warning");
            db.Close();
            return l.ToList()[0].TOTAL_WARNING;
        }
        
        //Reproses validation : update order data yang sudah di maintain
        public int getValidate_Reprocess()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderInquiry>("Reprocess_Validation");
            db.Close();
            return l.ToList()[0].TOTAL_REPROCESS;
        }

        //untuk email notify
        public void SendNotify()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            db.Fetch<mOrderInquiry>("Notify_Email");
            db.Close();
           
        }

        
        public int getValidate_OrderAck()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderInquiry>("OrderAck_Validation");
            db.Close();
            return l.ToList()[0].TOTAL_ORDERACK;
        }

        //send Email
        public int sendOrderACK(string fileName)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            int l = db.Execute("OrderAck_SendMail", new { 
                file_name = fileName
            });
            db.Close();
            return l;
        }

        //Get data order
        public List<mOrderInquiry> getListOrderACK()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderInquiry>("OrderAcknoledgement");
            db.Close();
            return l.ToList();
        }


        //Get data order
        public mOrderInquiry getListHeaderOrderACK()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.SingleOrDefault<mOrderInquiry>("GetListHeaderOrderACK");
            db.Close();
            return l;
        }

        //Insert Parameter and execute job
        public string InsertParamJobGTOPAS( string pUserID)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mOrderInquiry>("OrderInquiry_SendOrderGTOPAS", new
                {
                    USER_ID = pUserID
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }


        public string InsertParamJob(string pUserID)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mOrderInquiry>("OrderInquiry_SendOrderJob", new
                {
                    USER_ID = pUserID
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public string Reprocess(string pUserID)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mOrderInquiry>("OrderInquiry_Reprocess", new
                {
                    USER_ID = pUserID
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public List<OrderInquryDetailPartNo> OrderInquryGetDetailPartNo(mOrderInquiry Order)
        {
            string result = string.Empty;
            List<OrderInquryDetailPartNo> Parts = new List<OrderInquryDetailPartNo>();
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                Parts = db.Fetch<OrderInquryDetailPartNo>("OrderInquiry_GetDetailPartNo", new
                {
                    PART_NO = Order.PART_NO,
                    ITEM_NO = Order.ITEM_NO,
                    TMAP_ORDER_NO = Order.TMAP_ORDER_NO,
                    ORDERQTY = Order.ORDER_QTY,
                    PACKING_COMPANY = Order.PACKING_COMPANY,
                    PART_TYPE = Order.PART_TYPE
                }).ToList(); 
                db.Close();                
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return Parts;
        }

        //public string InsertParamJobGTOPAS(string pUserID, string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_PACKING_COMPANY)
        public string InsertParamJobGTOPAS(string pUserID, string p_TMMIN_REC_DT, string p_TMMIN_REC_DT_TO, string p_PACKING_COMPANY,string p_ORDER_TYPE)    
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                //var list = db.Fetch<mOrderInquiry>("OrderInquiry_SendOrderGTOPAS", new
                var list = db.Fetch<string>("OrderInquiry_SendOrder_Split", new
                {
                    USER_ID = pUserID,
                    TMMIN_REC_DT = p_TMMIN_REC_DT,
                    TMMIN_REC_DT_TO = p_TMMIN_REC_DT_TO,
                    PACKING_COMPANY=p_PACKING_COMPANY,
                    //ADD AGI 2017-07-21
                    ORDER_TYPE=p_ORDER_TYPE
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        //add method agi 2017-12-07
        public List<mOrderInquiry> Get_List_OBR_Status()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderInquiry>("OrderInquiry_Get_List_OBR_Status").ToList();
            return l;
        }
    }
}