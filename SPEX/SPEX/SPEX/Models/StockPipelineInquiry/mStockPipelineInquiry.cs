﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Muhajir Shiddiq
namespace SPEX.Models
{
    public class mStockPipelineInquiry
    {
        public string DATE { get; set; }
        public string FLAG{ get; set; }
        public string SHIFT{ get; set; }
        public int SEQ{ get; set; }
        public string PART_NO{ get; set; }
        public string PART_NAME { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public int QTY { get; set; }
        public int BEGIN_STOCK { get; set; }
        public int END_STOCK { get; set; }
        public string SUP_NAME { get; set; }
        public int ADJUST { get; set; }
        public int BINNING { get; set; }
        public int PICKING { get; set; }
        public string REF_FILE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        
        public string Text { get; set; }



        public List<mStockPipelineInquiry> getListPipeLineInquiry(string pPART_NO, string pFLAG, string pSHIFT,string pSUPPLIER,string pSUB_SUPPLIER,string pDATE_FROM,string pDATE_TO)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mStockPipelineInquiry>("StockPipelineInquiry/SearchStockPipelineInquiry",
                new { PART_NO = pPART_NO },
                new { SUPPLIER_CD =  pSUPPLIER},
                new { SUB_SUPPLIER_CD = pSUB_SUPPLIER},
                new { FLAG = pFLAG},
                new { SHIFT = pSHIFT},
                new { DATE_FROM = pDATE_FROM},
                new { DATE_TO = pDATE_TO });
            db.Close();
            return l.ToList();
        }

    }
}
