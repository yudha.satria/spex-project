﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Muhajir Shiddiq
namespace SPEX.Models
{
    public class mMaintenanceStockMaster
    {
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public decimal KANBAN_QTY { get; set; }
        public string SYSTEM_VALUE { get; set; }
        public string TEMP_RACK_ADDRESS_CD { get; set; }
        public string RACK_ADDRESS_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string DOCK_PRD { get; set; }
        public decimal DAD { get; set; }
        public int MAD { get; set; }
        public decimal ORDER_CYCLE { get; set; }
        public decimal PROCUREMENT_LT { get; set; }
        public decimal RECEIVING_LT { get; set; }
        public decimal ALFA { get; set; }
        public int ROP { get; set; }
        public int MIN_STOCK { get; set; }
        public int MAX_STOCK { get; set; }
        public string PART_DIMENSION { get; set; }
        public string PART_DIMENSION_NAME { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
        
        public string Text { get; set; }



        public List<mMaintenanceStockMaster> getListMaintenanceStock(string pPART_NO, string pRACK_ADDRESS_CD, string pPART_DIMENSION)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMaintenanceStockMaster>("MaintenanceStock/SearchMaintenanceStockMaster",
                  new { PART_NO = pPART_NO },
                  new { RACK_ADDRESS_CD = pRACK_ADDRESS_CD },
                  new { PART_DIMENSION = pPART_DIMENSION });
            db.Close();
            return l.ToList();
        }

        public mMaintenanceStockMaster GetPartName(string pPART_NO)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMaintenanceStockMaster>("MaintenanceStock/GetPartName",
                  new { PART_NO = pPART_NO }
                  );
            db.Close();
            return l.SingleOrDefault();
        }


        public String DeleteData(List<mMaintenanceStockMaster> dataModel, string pCREATED_BY)
        {
            String result = String.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                foreach (mMaintenanceStockMaster dt in dataModel)
                {
                    dynamic args = new
                    {
                        PART_NO = dt.PART_NO,
                        RACK_ADDRESS_CD = dt.RACK_ADDRESS_CD,
                        USER_ID = pCREATED_BY
                    };
                    result = db.SingleOrDefault<String>("MaintenanceStock/DeleteMaintenanceStockMaster", args);
                    if (result.Split('|')[0].Equals("E"))
                    {
                        break;
                    }
                }
                if (result.Split('|')[0].Equals("E"))
                {
                    db.AbortTransaction();
                }
                else
                {
                    db.CommitTransaction();
                }
            }
            catch (Exception e)
            {
                result = "E|" + e.Message;
                db.AbortTransaction();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public mMaintenanceStockMaster GetMadDad(string pPART_NO,string pDOCK_CD)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMaintenanceStockMaster>("MaintenanceStock/GetDadMad",
                  new { PART_NO = pPART_NO },
                  new { DOCK_CD = pDOCK_CD });
            db.Close();
            return l.SingleOrDefault();
        }




        public string SaveData(string pPART_NO, string pPART_NAME, string pRACK_ADDRESS_CD, string pDOCK_CD, string pDOCK_PRD, decimal pDAD, int pMAD, decimal pORDER_CYCLE, decimal pPROCUREMENT_LT, decimal pRECEIVING_LT, decimal pALFA, int pROP, int pMIN_STOCK, int pMAX_STOCK, int pKANBAN_QTY, string pPART_DIMENSION, string pCREATED_BY)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            string l = db.SingleOrDefault<string>("MaintenanceStock/SaveMaintenanceStockMaster",
                    new { MODE           = "add" },        
                    new { PART_NO        = pPART_NO },        
                    new { PART_NAME      = pPART_NAME },
                    new { TEMP_RACK_ADDRESS_CD= pRACK_ADDRESS_CD },
                    new { RACK_ADDRESS_CD= pRACK_ADDRESS_CD },
                    new { DOCK_CD        = pDOCK_CD },
                    new { DOCK_PRD      = pDOCK_PRD },        
                    new { DAD            = pDAD },            
                    new { MAD            = pMAD },            
                    new { ORDER_CYCLE    = pORDER_CYCLE },    
                    new { PROCUREMENT_LT = pPROCUREMENT_LT }, 
                    new { RECEIVING_LT   = pRECEIVING_LT },   
                    new { ALFA           = pALFA },           
                    new { ROP            = pROP },            
                    new { MIN_STOCK      = pMIN_STOCK },      
                    new { MAX_STOCK      = pMAX_STOCK },      
                    new { KANBAN_QTY     = pKANBAN_QTY },     
                    new { PART_DIMENSION = pPART_DIMENSION },
                    new { CREATED_BY = pCREATED_BY },
                    new { CHANGED_BY = "" },
                    new { CHANGED_DT = "" }
                    );
            db.Close();
            return l;
        }

        public string UpdateData(string pPART_NO, string pPART_NAME, string pTEMP_RACK_ADDRESS_CD, string pRACK_ADDRESS_CD, string pDOCK_CD, string pDOCK_PRD, decimal pDAD, int pMAD, decimal pORDER_CYCLE, decimal pPROCUREMENT_LT, decimal pRECEIVING_LT, decimal pALFA, int pROP, int pMIN_STOCK, int pMAX_STOCK, int pKANBAN_QTY, string pPART_DIMENSION, string pCREATED_BY, string iChangedBy, string iChangedDt)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            string l = db.SingleOrDefault<string>("MaintenanceStock/SaveMaintenanceStockMaster",
                    new { MODE = "edit" },        
                    new { PART_NO = pPART_NO },
                    new { PART_NAME = pPART_NAME },
                    new { TEMP_RACK_ADDRESS_CD= pTEMP_RACK_ADDRESS_CD },
                    new { RACK_ADDRESS_CD = pRACK_ADDRESS_CD },
                    new { DOCK_CD = pDOCK_CD },
                    new { DOCK_PRD = pDOCK_PRD },
                    new { DAD = pDAD },
                    new { MAD = pMAD },
                    new { ORDER_CYCLE = pORDER_CYCLE },
                    new { PROCUREMENT_LT = pPROCUREMENT_LT },
                    new { RECEIVING_LT = pRECEIVING_LT },
                    new { ALFA = pALFA },
                    new { ROP = pROP },
                    new { MIN_STOCK = pMIN_STOCK },
                    new { MAX_STOCK = pMAX_STOCK },
                    new { KANBAN_QTY = pKANBAN_QTY },
                    new { PART_DIMENSION = pPART_DIMENSION },
                    new { CREATED_BY = pCREATED_BY },
                    new { CHANGED_BY = iChangedBy },
                    new { CHANGED_DT = iChangedDt }                    
                    );

            db.Close();
            return l;
        }

        public string DeleteDataTBT()
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("MaintenanceStock/DeleteTBTMaintenanceStockMaster");
                    scope.Complete();
                }


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

        public string BackgroundProcessSave(string function_id, string module_id, string user_id, long PID)
        {

            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("MaintenanceStock/MAINTENANCE_STOCK_MASTER_CALL_JOB",
                       new { function_id = function_id },
                       new { module_id = module_id },
                       new { user_id = user_id },
                       new { PID = PID }
                       );
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result = "1|" + ex.Message;
            }
            return result;
        }


    }
}
