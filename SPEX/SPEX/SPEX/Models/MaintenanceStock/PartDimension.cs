﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Collections;

namespace SPEX.Models
{
    public class PartDimension
    {
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }

        public List<PartDimension> GetAllPartDimension()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<PartDimension>("MaintenanceStock/GetPartDimension").ToList();
            db.Close();
            return l;
        }

        public static IEnumerable GetComboPartDimension()
        {
            PartDimension dbContext = new PartDimension();
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<PartDimension>("GetPartDimension").ToList();
            db.Close();
            return l;
        }
    }
}