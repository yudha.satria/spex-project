﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Collections;

namespace SPEX.Models
{
    public class RackAddress
    {
        public string RACK_ADDRESS_CD { get; set; }
        public string DELETE_FLAG { get; set; }

        public List<RackAddress> GetAllRackAddress(string pDELETE_FLAG)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<RackAddress>("MaintenanceStock/GetRackAddress",
                 new { DELETE_FLAG = pDELETE_FLAG}).ToList();
            db.Close();
            
            return l;
        }

    }
}