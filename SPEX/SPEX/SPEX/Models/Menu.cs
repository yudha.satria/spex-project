﻿using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public  class MenuSpex
    {
        public  string MENU_ID {get;set;}
        public  string MENU_TEXT { get; set; }
        public  string NAVIGATE_URL { get; set; }
        public  string PARENT_ID { get; set; }
        public List<MenuSpex> Child { get; set; }
        public bool HasChildren { get; set; }

        public List<MenuSpex>  getMenuByRole(string ROLE_ID)
        {
            List<MenuSpex> menus = new List<MenuSpex>();
            List<MenuSpex> MenuSpexsAll = GetAllMenuSpexByRole(ROLE_ID);
            menus = ConvertMenu(MenuSpexsAll, string.Empty);
            return menus;
        }

        public List<MenuSpex> ConvertMenu(List<MenuSpex> MenuSpexs, string Parent_ID)
        {
            List<MenuSpex> menus = new List<MenuSpex>();
            var parent = MenuSpexs.Where(a => a.PARENT_ID == Parent_ID).ToList();
            foreach (var menuspex in parent)
            {
                MenuSpex menu = new MenuSpex();
                menu.MENU_TEXT = menuspex.MENU_TEXT;
                menu.NAVIGATE_URL = menuspex.NAVIGATE_URL;
                Child= ConvertMenu(MenuSpexs, menuspex.MENU_ID);
                menu.Child = ConvertMenu(MenuSpexs, menuspex.MENU_ID);
                menu.MENU_ID = menuspex.MENU_ID;
                menu.HasChildren = menu.Child.Count() > 0 ? true : false;
                menus.Add(menu);
            }
            //MenuList menulist = menus;
            return menus;
        }

        //public MenuList getChildMenu(List<MenuSpex> MenuSpexs, string Parent_ID)
        //{
        //    MenuList menus = new MenuList();
        //    menus = MenuSpexs.Where(a => a.PARENT_ID = string.Empty).ToList();
        //    foreach (var menuspex in menus)
        //    {
        //        Menu TdkMenu = new Menu();
        //        TdkMenu.Text = menuspex.Text;
        //        TdkMenu.NavigateUrl = menuspex.NavigateUrl;
        //        TdkMenu.Children = getChildMenu(MenuSpexs, menuspex.MENU_ID);
        //    }
        //    return menus;
        //}

        public  List<MenuSpex> GetAllMenuSpexByRole(string ROLE_ID)
        {
            //List<MenuSpex> menu = new List<MenuSpex>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<MenuSpex> l = db.Fetch<MenuSpex>("MenuSpex_Search",
                  new { ROLE_ID = ROLE_ID }
                  ).ToList();
            db.Close();
            return l;
        }
        
        
    }
}