﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPDCode
    {
        public string PD_CD { get; set; }
        public string DESCRIPTION { get; set; }
        public string SHIPMENT_FLAG { get; set; }
        public string ECLUSIVE_FLAG { get; set; }
        public string USED_FLAG { get; set; }
        public string DELETION_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string Text { get; set; }

        public List<mPDCode> getListPDCode(string pPD_CD, string pDESCRIPTION)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPDCode>("PDCode_Search",
                  new { PD_CD = pPD_CD },
                  new { DESCRIPTION = pDESCRIPTION }
                  );
            db.Close();
            return l.ToList();
        }

        //Delete Data
        public string DeleteData(string p_PD_CD, string p_CHANGED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                if (p_PD_CD.Contains(";"))
                {
                    string[] PDcodeList = p_PD_CD.Split(';');
                    for (int i = 0; i < PDcodeList.Length; i++)
                    {
                        var l = db.Fetch<mPDCode>("PDCode_Delete", new
                        {
                            PD_CD = PDcodeList[i],
                            CHANGED_BY = p_CHANGED_BY,
                            MSG_TEXT = ""
                        });
                        foreach (var message in l)
                        { result = message.Text; }
                    }
                }
                else
                {
                    var l = db.Fetch<mPriceTerm>("PDCode_Delete", new
                    {
                        PD_CD = p_PD_CD,
                        CHANGED_BY = p_CHANGED_BY,
                        MSG_TEXT = ""
                    });
                    foreach (var message in l)
                    { result = message.Text; }
                }
                db.Close();
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        //Save Data
        public string SaveData(string p_PD_CD, string p_DESCRIPTION, string p_SHIPMENT_FLAG, string p_EXCLUSIVE_FLAG, string p_CREATED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPDCode>("PDCode_Save", new
                {
                    PD_CD = p_PD_CD,
                    DESCRIPTION = p_DESCRIPTION,
                    SHIPMENT_FLAG = p_SHIPMENT_FLAG,
                    EXCLUSIVE_FLAG = p_EXCLUSIVE_FLAG,
                    CREATED_BY = p_CREATED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        //Update Data
        public string UpdateData(string p_PD_CD, string p_DESCRIPTION, string p_SHIPMENT_FLAG, string p_EXCLUSIVE_FLAG, string p_DELETION_FLAG, string p_CHANGED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPDCode>("PDCode_Update", new
                {
                    PD_CD = p_PD_CD,
                    DESCRIPTION = p_DESCRIPTION,
                    SHIPMENT_FLAG = p_SHIPMENT_FLAG,
                    EXCLUSIVE_FLAG = p_EXCLUSIVE_FLAG,
                    DELETION_FLAG = p_DELETION_FLAG,
                    CHANGED_BY = p_CHANGED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

    }
}