﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPackingCompletionByAir
    {
        public string BUYER_CD { get; set; }
        public string PD_CD { get; set; }
        public string CASE_NO { get; set; }
        public decimal CASE_GROSS_WEIGHT { get; set; }
        public decimal CASE_NET_WEIGHT { get; set; }
        public decimal CASE_MEASUREMENT { get; set; }
        public string CASE_DANGER_FLAG { get; set; }
        public string CASE_TYPE { get; set; }
        public decimal CASE_LENGTH { get; set; }
        public decimal CASE_WIDTH { get; set; }
        public decimal CASE_HEIGHT { get; set; }
        public string TRANSPORT_CD { get; set; }

        public string ORDER_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public decimal PART_QTY { get; set; }
        public string PART_NAME { get; set; }
        public string PACKING_MONTH { get; set; }
        public DateTime? PACKAGE_DATE { get; set; }
        public DateTime? ORDER_DATE { get; set; }

        //additional grid
        public decimal TOTAL_PART_QTY { get; set; }

        public string TEXT { get; set; }
        
        public List<mPackingCompletionByAir> GetCaseContent(string pCaseNo, string pUserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPackingCompletionByAir>("PC_GetCaseContent",
                        new { CASE_NO = pCaseNo },
                        new { USER_ID = pUserID }
                  );
            db.Close();

            return l.ToList();
        }

        public string SavePart(string pCaseNo, string pOrderNo, string pItemNo, string pPartNo, decimal pPartQty,  string pUserID)
        {
            string result = "error ";

            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mShippingInstruction>("PC_SavePart",
                        new { CASE_NO = pCaseNo },
                        new { ORDER_NO = pOrderNo },
                        new { ITEM_NO = pItemNo },
                        new { PART_NO = pPartNo },
                        new { PART_QTY = pPartQty },
                        new { USER_ID = pUserID }
                      );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string SaveCase(string pCaseNo, decimal pGrossWeight, decimal pNetWeight, string pDangerFlag, string pCaseType, decimal pWidth, decimal pHeight,
            decimal pLength, string pUserID)
        {
            string result = "error ";

            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mShippingInstruction>("PC_SaveCase",
                        new { CASE_NO = pCaseNo },
                        new { CASE_GROSS_WEIGHT = pGrossWeight },
                        new { CASE_NET_WEIGHT = pNetWeight },
                        new { CASE_DANGER_FLAG = pDangerFlag },
                        new { CASE_TYPE = pCaseType },
                        new { CASE_LENGTH = pLength },
                        new { CASE_WIDTH = pWidth },
                        new { CASE_HEIGHT = pHeight },
                        new { USER_ID = pUserID }
                      );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string GetNewCaseNo()
        {
            string result = "";

            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mShippingInstruction>("PC_GetNewCaseNo");
                db.Close();

                foreach (var data in l.ToList())
                {
                    result = data.CASE_NO;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string DeleteTempPart(string pUserID)
        {
            string result = "error";

            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mShippingInstruction>("PC_DeleteTempPart",
                    new { USER_ID = pUserID });
                db.Close();

                foreach (var data in l.ToList())
                {
                    result = data.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string DeleteTempPartRow(string pCaseNo, string pOrderNo, string pItemNo, string pPartNo, string pUserID)
        {
            string result = "error ";

            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mShippingInstruction>("PC_DeleteTempPartRow",
                        new { CASE_NO = pCaseNo },
                        new { ORDER_NO = pOrderNo },
                        new { ITEM_NO = pItemNo },
                        new { PART_NO = pPartNo },
                        new { USER_ID = pUserID }
                      );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}