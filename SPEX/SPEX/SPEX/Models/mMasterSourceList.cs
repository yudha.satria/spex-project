﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mSourceList
    {
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string PLANT_CD { get; set; }
        public string LINE_CD { get; set; }
        public string SLOC_CD { get; set; }
        public DateTime VALID_FROM {get; set;}
        public DateTime VALID_TO {get; set;}
        public string CREATED_BY {get; set;}
        public DateTime CREATED_DT {get; set;}
        public string CHANGED_BY {get; set;}
        public DateTime CHANGED_DT { get; set; }
        public string MSG_TEXT { get; set; }
        public string Text { get; set; }

        public List<mSourceList> SPN113MasterSourceListSearch(string ps_PartNo, string ps_PlantCode, string ps_LineCode, string ps_ValidFrom, string ps_ValidTo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            DateTime dtPValidFrom = Convert.ToDateTime("01-01-1753");
            DateTime dtPValidTo = Convert.ToDateTime("01-01-1753");

            if (!String.IsNullOrEmpty(ps_ValidFrom))
            {

                string strValidFrom = ps_ValidFrom.Substring(3, 2) + "-" + ps_ValidFrom.Substring(0, 2) + "-" + ps_ValidFrom.Substring(6, 4);
                dtPValidFrom = Convert.ToDateTime(strValidFrom);
            }

            if (!String.IsNullOrEmpty(ps_ValidTo))
            {

                string strValidTo = ps_ValidTo.Substring(3, 2) + "-" + ps_ValidTo.Substring(0, 2) + "-" + ps_ValidTo.Substring(6, 4);
                dtPValidTo = Convert.ToDateTime(strValidTo);
            }

            var l = db.Fetch<mSourceList>("Master_Source_List_Search", new {
                                            PART_NO = ps_PartNo,
                                            PLANT_CD = ps_PlantCode,
                                            LINE_CD = ps_LineCode,
                                            VALID_FROM = dtPValidFrom,
                                            VALID_TO = dtPValidTo
                                        });
            if (ps_PartNo == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }

        public string SPN113MasterSourceListSaveData(string pi_PartNo, string pi_PlantCode, string pi_LineCode, string pi_SlocCode, string pi_ValidFrom, string pi_ValidTo)
        {
            string result = string.Empty;
            try
            {
                DateTime dtPValidFrom = Convert.ToDateTime("01-01-1753");
                DateTime dtPValidTo = Convert.ToDateTime("01-01-1753");

                if (!String.IsNullOrEmpty(pi_ValidFrom))
                {

                    string strValidFrom = pi_ValidFrom.Substring(3, 2) + "-" + pi_ValidFrom.Substring(0, 2) + "-" + pi_ValidFrom.Substring(6, 4);
                    dtPValidFrom = Convert.ToDateTime(strValidFrom);
                }

                if (!String.IsNullOrEmpty(pi_ValidTo))
                {

                    string strValidTo = pi_ValidTo.Substring(3, 2) + "-" + pi_ValidTo.Substring(0, 2) + "-" + pi_ValidTo.Substring(6, 4);
                    dtPValidTo = Convert.ToDateTime(strValidTo);
                }

                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mSourceList>("Master_Source_List_Save",
                    new { PART_NO = pi_PartNo },
                    new { PLANT_CD = pi_PlantCode },
                    new { LINE_CD = pi_LineCode },
                    new { SLOC_CD = pi_SlocCode },
                    new { VALID_FROM = dtPValidFrom },
                    new { VALID_TO = dtPValidTo },
                    new { CREATED_BY = "Sys" },
                    new { MSG_TEXT = "" }
                );
                db.Close();

                foreach (var message in l)
                {
                    result =  message.MSG_TEXT;
                }
            }
            catch (Exception err)
            {
                result = "error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public string SPN113MasterSourceListEditData(string pE_PartNo, string pE_PlantCode, string pE_LineCode, string pE_SlocCode, string pE_ValidFrom, string pE_ValidTo)
        {
            string result = string.Empty;
            try
            {
                DateTime dtPValidFrom = Convert.ToDateTime("01-01-1753");
                DateTime dtPValidTo = Convert.ToDateTime("01-01-1753");

                if (!String.IsNullOrEmpty(pE_ValidFrom))
                {

                    string strValidFrom = pE_ValidFrom.Substring(3, 2) + "-" + pE_ValidFrom.Substring(0, 2) + "-" + pE_ValidFrom.Substring(6, 4);
                    dtPValidFrom = Convert.ToDateTime(strValidFrom);
                }

                if (!String.IsNullOrEmpty(pE_ValidTo))
                {

                    string strValidTo = pE_ValidTo.Substring(3, 2) + "-" + pE_ValidTo.Substring(0, 2) + "-" + pE_ValidTo.Substring(6, 4);
                    dtPValidTo = Convert.ToDateTime(strValidTo);
                }

                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mSourceList>("Master_Source_List_Edit",
                    new { PART_NO = pE_PartNo },
                    new { PLANT_CD = pE_PlantCode },
                    new { LINE_CD = pE_LineCode },
                    new { SLOC_CD = pE_SlocCode },
                    new { VALID_FROM = dtPValidFrom },
                    new { VALID_TO = dtPValidTo },
                    new { CHANGED_BY = "Sys" },
                    new { MSG_TEXT = "" }
                );
                db.Close();

                foreach (var message in l)
                {
                    result = message.MSG_TEXT;
                }
            }
            catch (Exception err)
            {
                result = "error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public string SPN113MasterSourceListDeleteData(string pd_Part_List)
        {
            string result = string.Empty;
            try
            {

                IDBContext db = DatabaseManager.Instance.GetContext();

                string[] PartList = pd_Part_List.Split(',');
                string paramPartNo = string.Empty;
                string paramLineCd = string.Empty;
                string paramValidFrom = string.Empty;

                for (int i = 0; i < PartList.Length; i++)
                {
                    string [] paramlist = (PartList[i].ToString()).Split('|');
                    
                    paramPartNo = paramlist[0].ToString();
                    paramValidFrom = paramlist[1].ToString();
                    paramLineCd = paramlist[2].ToString();

                    var l = db.Fetch<mSourceList>("Master_Source_List_Delete",
                        new { PART_NO = paramPartNo },
                        new { VALID_FROM = paramValidFrom },
                        new { LINE_CD = paramLineCd },
                        new { CHANGED_BY = "Sys" },
                        new { MSG_TEXT = "" });
                    db.Close();
                    
                    int cekjmlh = PartList.Length - i;
                    if (cekjmlh == 1)
                    {
                        foreach (var message in l)
                        {
                            result = message.MSG_TEXT;
                        }
                    }
                }                
            }
            catch (Exception err)
            {
                result = "error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public string SPN113CallbackUploadExcelToSQL(string PartNo, string ValidFrom, string LineCd, string SlocCd, string PlantCd, string ValidTo, string CreatedBy, string CreatedDT, string ProcessId)
        {

            string result = string.Empty;
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Execute("Master_Source_List_Upload_Temportary_Table",
                    new { PART_NO = PartNo },
                    new { PLANT_CD = PlantCd },
                    new { LINE_CD = LineCd },
                    new { SLOC_CD = SlocCd },
                    new { VALID_FROM = ValidFrom },
                    new { VALID_TO = ValidTo },
                    new { CREATED_BY = CreatedBy },
                    new { CREATED_DT = CreatedDT },
                    new { PROCESS_ID = ProcessId}
                );
                db.Close();

                result = "Successfully";
            }
            catch (Exception err)
            {
                result = "error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public string SPN113SaveUploadExcel(string Created_By, string ProcessId)
        {
            string result = string.Empty;
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mSourceList>("Master_Source_List_Upload_Save", new { PROCESS_ID = ProcessId }, new { CREATED_BY = Created_By });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text.ToString();
                }

                if (result == "Process UPLOAD SOURCE LIST finish successfully")
                {
                    return "Upload Successfully";
                }
                else
                {
                    return result;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }
    }
}