﻿using DevExpress.Web.ASPxUploadControl;
using SPEX.Cls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class Document
    {
        public string Message { get; set; }
        public string DOCNAME { get; set; }
        public decimal DOC_SIZE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string DELETE_LINK_IMAGE { get; set; }
        public string URL_DOC { get; set; }
        public string FileName { get; set; }
        
        //static string GetCallbackData(UploadedFile uploadedFile, string fileUrl)
        //{
        //    string name = uploadedFile.FileName;
        //    long sizeInKilobytes = uploadedFile.ContentLength / 1024;
        //    string sizeText = sizeInKilobytes.ToString() + " KB";

        //    return name + "|" + fileUrl + "|" + sizeText;
        //}


        public void UploadFile(FileUploadCompleteEventArgs File, string FullFileName)
        {
            string FileName = File.UploadedFile.FileName;
            //string FullFileName = Path.Combine(Server.MapPath(UploadDirectory), FileName);
            File.UploadedFile.SaveAs(FullFileName);            
        }



        public void Save(List<Document> Files, string User_Id)
        {
            
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.BeginTransaction();
            foreach (var Fl in Files)
            {
                
                db.Execute("DocumentSave",
                    new
                    {
                        FileName = Fl.DOCNAME,
                        DOC_SIZE = Fl.DOC_SIZE,
                        User_Id = User_Id
                    });
            }
            db.CommitTransaction();
            db.Close();
        }


        public void Save(string File,Decimal DOC_SIZE, string User_Id)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();
                db.Execute("DocumentSave",
                    new
                    {
                        FileName = File,
                        DOC_SIZE = DOC_SIZE,
                        User_Id = User_Id
                    });
            
            db.Close();
        }

        public string GetExistingDocument(string FileName)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string IsExixst = string.Empty;
            IsExixst= db.ExecuteScalar<string>("DocumentGetExistingFile",
                    new
                    {
                        FileName = FileName                        
                    });
            return IsExixst;
        }

        public List<Document> Search(string FileName)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l= db.Fetch<Document>("DocumentSearch",
                    new
                    {
                        FileName = FileName
                    }).ToList();
            return l;
        }

        public string Delete(string FileName)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string IsExixst = string.Empty;
            IsExixst = db.ExecuteScalar<string>("DocumentDelete",
                    new
                    {
                        FileName = FileName
                    });
            return IsExixst;
        }

        public string ExtFileCheck(string Ext)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string Valid = string.Empty;
            Valid = db.ExecuteScalar<string>("DocumentValidExt",
                    new
                    {
                        Ext = Ext
                    });
            return Valid;
        }

        public decimal GetMaxFileSize()
        {
            decimal size = Convert.ToDecimal(new mSystemMaster().GetSystemValueList("DOCUMENT", "DOC_MAXSIZE", "4", ";").FirstOrDefault().SYSTEM_VALUE);
            return size;
        }

        public decimal GetMaxAllFileSize()
        {
            decimal size = Convert.ToDecimal(new mSystemMaster().GetSystemValueList("DOCUMENT", "DOC_TOTALSIZE", "4", ";").FirstOrDefault().SYSTEM_VALUE);
            return size;
        }

        public static readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new mSystemMaster().GetSystemValueList("DOCUMENT", "DOC_FILE_EXTENTION", "4", ";").Select(a => a.SYSTEM_VALUE).ToArray(),//,//new string [] {".xlsx"},
            MaxFileSize = (Convert.ToInt64(new mSystemMaster().GetSystemValueList("DOCUMENT", "DOC_MAXSIZE", "4", ";").FirstOrDefault().SYSTEM_VALUE) * 1024) * 1024,
            MaxFileSizeErrorText = new MessagesString().getMsgText("MSPXDOC02ERR").ToString(),
            NotAllowedFileExtensionErrorText = new MessagesString().getMsgText("MSPXDOC01ERR")
        };

        public decimal GetExistingSize()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            decimal size = 0;
            size = db.ExecuteScalar<decimal>("DocumentSumSize");
            return size;
        }
    }
}