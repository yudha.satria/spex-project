﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Diani Widyaningrum
namespace SPEX.Models
{
    public class mWarehouseInquiry
    {
        public string BUYER_PD { get; set; }
        public string TMAP_ORDER_NO { get; set; }
        public string TMAP_ORDER_DATE { get; set; }
        public string ORDER_TYPE { get; set; }
        public string TRANSP_CD { get; set; }
        public string TMAP_ITEM_NO { get; set; }
        public string TMAP_PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public decimal TMAP_ORDER_QTY { get; set; }
        public decimal REMAIN_QTY { get; set; }
        public string PICKING_LIST_FLAG { get; set; }
        public string ALLOCATION_QTY { get; set; }
        public string RACK_ADDRESS_CD { get; set; }
        public string TPCAP_ORDER_TYPE { get; set; }
        public string TEXT { get; set; }

        public class mResultMessage
        { 
            public string TEXT { get; set; }
            public string pid { get; set; }
        }

        public List<mWarehouseInquiry> getListWarehouseInquiry(string pORDER_NO, string pITEM_NO, string pPART_NO, string pORDER_TYPE, string pBUYER_PD, string pORDER_DATE_FROM, string pORDER_DATE_TO, string pSTATUS_FLAG)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();  
            var l = db.Fetch<mWarehouseInquiry>("SearchWarehouseInquiry",
                    new { ORDER_NO = pORDER_NO },
                    new { ITEM_NO = pITEM_NO },
                    new { PART_NO = pPART_NO },
                    new { ORDER_TYPE = pORDER_TYPE },
                    new { BUYER_PD = pBUYER_PD },
                    new { ORDER_DATE_FROM = pORDER_DATE_FROM },
                    new { ORDER_DATE_TO = pORDER_DATE_TO },
                    new { STATUS_FLAG = pSTATUS_FLAG });
            db.Close();
            return l.ToList();
        }

        public List<mSystemMaster> GetComboBoxList(string systemType)
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetComboBoxfrSystem",
                new { SYSTEM_TYPE = systemType }
            ).ToList();
            db.Close();
            return datas;
        }

        public string GeneratePickingList(string pCheckBox, string pOderNo, string pPartNO, string pItemNo, long process_id)
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("PICKING_LIST_CREATION_BATCH_CALL_JOB",
                    new { process_id = process_id },
                    new { pOrderNo = pOderNo },
                    new { pPartNo = pPartNO },
                    new { pItemNo = pItemNo });
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result = "1|" + ex.Message;
            } 
            return result;
        } 
      
    }
}
