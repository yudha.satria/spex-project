﻿using System;

namespace SPEX.Models
{
    public class History
    {
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ChangedBy { get; set; }
        public DateTime ChangedDate { get; set; }
    }
}