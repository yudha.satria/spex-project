﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mVanningCompletion
    {
        public string PACKING_ID { get; set; }
        public string TMMIN_ORDER_NO { get; set; }
        public string BUYER_CD { get; set; }
        public string PD_CD { get; set; }
        public string PART_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string TMAP_ORDER_NO { get; set; }
        public DateTime? TMAP_ORDER_DT { get; set; }
        public string PART_QTY { get; set; }
        public string CASE_NO { get; set; }
        public string CASE_TYPE { get; set; }
        public string CASE_GROSS_WEIGHT { get; set; }
        public DateTime? PACKING_DT { get; set; }
        public string PACKING_COMPANY { get; set; }
        public string TRANSPORT_CD { get; set; }
        public string DANGER_FLAG { get; set; }
        public string CONTAINER_NO { get; set; }
        public DateTime? VANNING_DT { get; set; }
        public string VANNING_DT_STRING { get; set; }
        public string SEAL_NO { get; set; }
        public string CONTAINER_SIZE { get; set; }
        public string CONTAINER_TYPE { get; set; }
        public string RC_FLAG { get; set; }
        public string VC_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string TEXT { get; set; }

        public class mVanningCompletionUpload
        {
            public string DATA_ID { get; set; }
            public string CONTAINER_NO { get; set; }
            public string SEAL_NO { get; set; }
            public string CONTAINER_SIZE { get; set; }
            public string VANNING_DT { get; set; }
            public string CASE_NO { get; set; }
        }


        public List<mVanningCompletion> getListVanningCompletion(string p_VANNING_DT_FROM, string p_VANNING_DT_TO,
            string p_CONTAINER_NO, string p_SEAL_NO)
        {
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mVanningCompletion>("VanningCompletion_Search",
                        new { VANNING_DT_FROM = p_VANNING_DT_FROM },
                        new { VANNING_DT_TO = p_VANNING_DT_TO },
                        new { CONTAINER_NO = p_CONTAINER_NO },
                        new { SEAL_NO = p_SEAL_NO }
                      );
                db.Close();
                return l.ToList();
            }
            catch (Exception ex)
            {
                return new List<mVanningCompletion>();
            }
        }

        public string Upload(string UserID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mVanningCompletion>("VanningCompletion_Upload",
                             new { USER_ID = UserID }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string DeleteTempData()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mVanningCompletion>("VanningCompletion_DeleteTemp");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public List<mVanningCompletion> DownloadData(string p_KEY_DOWNLOADS, string p_VANNING_DT_FROM, string p_VANNING_DT_TO,
            string p_CONTAINER_NO, string p_SEAL_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mVanningCompletion>("VanningCompletion_Download",
                    new { KEY_DOWNLOADS = p_KEY_DOWNLOADS },
                        new { VANNING_DT_FROM = p_VANNING_DT_FROM },
                        new { VANNING_DT_TO = p_VANNING_DT_TO },
                        new { CONTAINER_NO = p_CONTAINER_NO },
                        new { SEAL_NO = p_SEAL_NO }
                  );
            db.Close();
            return l.ToList();
        }

        public string InsertTemp(mVanningCompletionUpload data)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mVanningCompletion>("VanningCompletion_InsertTemp",
                             new { DATA_ID = data.DATA_ID },
                             new { CONTAINER_NO = data.CONTAINER_NO },
                             new { SEAL_NO = data.SEAL_NO },
                             new { CONTAINER_SIZE = data.CONTAINER_SIZE },
                             new { VANNING_DT = data.VANNING_DT },
                             new { CASE_NO = data.CASE_NO }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

    }
}