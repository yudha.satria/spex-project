﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mDailyOrder
    {
        #region
        //FUNGSI GETHER SETTER - Sama dengan Query Database
        public string Prod_Month { get; set; }
        public string Order_Type { get; set; }
        public string Part_No { get; set; }
        public string Line_Cd { get; set; }
        public string Part_Name { get; set; }
        public string Model { get; set; }
        public string Paint_Type { get; set; }
        public string Part_Spec { get; set; }
        public int Takt_Time { get; set; }
        public int PcsKbn { get; set; }
        public DateTime Order_Dt { get; set; }
        public DateTime Production_Dt { get; set; }
        public string Plant_Cd { get; set; }
        public string Sloc_Cd { get; set; }
        public int Ratio { get; set; }
        public double Time_Usage_1 { get; set; }
        public double Time_Usage_2 { get; set; }
        public double Time_Usage_3 { get; set; }
        public double Time_Usage_4 { get; set; }
        public double Time_Usage_5 { get; set; }
        public double Time_Usage_6 { get; set; }
        public double Time_Usage_7 { get; set; }
        public double Time_Usage_8 { get; set; }
        public double Time_Usage_9 { get; set; }
        public double Time_Usage_10 { get; set; }
        public double Time_Usage_11 { get; set; }
        public double Time_Usage_12 { get; set; }
        public double Time_Usage_13 { get; set; }
        public double Time_Usage_14 { get; set; }
        public double Time_Usage_15 { get; set; }
        public double Time_Usage_16 { get; set; }
        public double Time_Usage_17 { get; set; }
        public double Time_Usage_18 { get; set; }
        public double Time_Usage_19 { get; set; }
        public double Time_Usage_20 { get; set; }
        public double Time_Usage_21 { get; set; }
        public double Time_Usage_22 { get; set; }
        public double Time_Usage_23 { get; set; }
        public double Time_Usage_24 { get; set; }
        public double Time_Usage_25 { get; set; }
        public double Time_Usage_26 { get; set; }
        public double Time_Usage_27 { get; set; }
        public double Time_Usage_28 { get; set; }
        public double Time_Usage_29 { get; set; }
        public double Time_Usage_30 { get; set; }
        public double Time_Usage_31 { get; set; }
        public double Time_Avail_1 { get; set; }
        public double Time_Avail_2 { get; set; }
        public double Time_Avail_3 { get; set; }
        public double Time_Avail_4 { get; set; }
        public double Time_Avail_5 { get; set; }
        public double Time_Avail_6 { get; set; }
        public double Time_Avail_7 { get; set; }
        public double Time_Avail_8 { get; set; }
        public double Time_Avail_9 { get; set; }
        public double Time_Avail_10 { get; set; }
        public double Time_Avail_11 { get; set; }
        public double Time_Avail_12 { get; set; }
        public double Time_Avail_13 { get; set; }
        public double Time_Avail_14 { get; set; }
        public double Time_Avail_15 { get; set; }
        public double Time_Avail_16 { get; set; }
        public double Time_Avail_17 { get; set; }
        public double Time_Avail_18 { get; set; }
        public double Time_Avail_19 { get; set; }
        public double Time_Avail_20 { get; set; }
        public double Time_Avail_21 { get; set; }
        public double Time_Avail_22 { get; set; }
        public double Time_Avail_23 { get; set; }
        public double Time_Avail_24 { get; set; }
        public double Time_Avail_25 { get; set; }
        public double Time_Avail_26 { get; set; }
        public double Time_Avail_27 { get; set; }
        public double Time_Avail_28 { get; set; }
        public double Time_Avail_29 { get; set; }
        public double Time_Avail_30 { get; set; }
        public double Time_Avail_31 { get; set; }
        public double Qty_D_1 { get; set; }
        public double Qty_D_2 { get; set; }
        public double Qty_D_3 { get; set; }
        public double Qty_D_4 { get; set; }
        public double Qty_D_5 { get; set; }
        public double Qty_D_6 { get; set; }
        public double Qty_D_7 { get; set; }
        public double Qty_D_8 { get; set; }
        public double Qty_D_9 { get; set; }
        public double Qty_D_10 { get; set; }
        public double Qty_D_11 { get; set; }
        public double Qty_D_12 { get; set; }
        public double Qty_D_13 { get; set; }
        public double Qty_D_14 { get; set; }
        public double Qty_D_15 { get; set; }
        public double Qty_D_16 { get; set; }
        public double Qty_D_17 { get; set; }
        public double Qty_D_18 { get; set; }
        public double Qty_D_19 { get; set; }
        public double Qty_D_20 { get; set; }
        public double Qty_D_21 { get; set; }
        public double Qty_D_22 { get; set; }
        public double Qty_D_23 { get; set; }
        public double Qty_D_24 { get; set; }
        public double Qty_D_25 { get; set; }
        public double Qty_D_26 { get; set; }
        public double Qty_D_27 { get; set; }
        public double Qty_D_28 { get; set; }
        public double Qty_D_29 { get; set; }
        public double Qty_D_30 { get; set; }
        public double Qty_D_31 { get; set; }
        public string Time_Percent_1 { get; set; }
        public string Time_Percent_2 { get; set; }
        public string Time_Percent_3 { get; set; }
        public string Time_Percent_4 { get; set; }
        public string Time_Percent_5 { get; set; }
        public string Time_Percent_6 { get; set; }
        public string Time_Percent_7 { get; set; }
        public string Time_Percent_8 { get; set; }
        public string Time_Percent_9 { get; set; }
        public string Time_Percent_10 { get; set; }
        public string Time_Percent_11 { get; set; }
        public string Time_Percent_12 { get; set; }
        public string Time_Percent_13 { get; set; }
        public string Time_Percent_14 { get; set; }
        public string Time_Percent_15 { get; set; }
        public string Time_Percent_16 { get; set; }
        public string Time_Percent_17 { get; set; }
        public string Time_Percent_18 { get; set; }
        public string Time_Percent_19 { get; set; }
        public string Time_Percent_20 { get; set; }
        public string Time_Percent_21 { get; set; }
        public string Time_Percent_22 { get; set; }
        public string Time_Percent_23 { get; set; }
        public string Time_Percent_24 { get; set; }
        public string Time_Percent_25 { get; set; }
        public string Time_Percent_26 { get; set; }
        public string Time_Percent_27 { get; set; }
        public string Time_Percent_28 { get; set; }
        public string Time_Percent_29 { get; set; }
        public string Time_Percent_30 { get; set; }
        public string Time_Percent_31 { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Dt { get; set; }
        public string Changed_By { get; set; }
        public DateTime Changed_Dt { get; set; }
        public string Text { get; set; }
#endregion

        public List<mDailyOrder> SPN221GetDailyOrder()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mDailyOrder>("Daily_Order_Select_0");
            db.Close();
            return l.ToList();
        }

        public List<mDailyOrder> SPN221GetDailyOrderSpecific(string p_Prod_Month, string p_Order_Type, string p_Plant_Cd, string p_Line_Cd)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            if (p_Line_Cd == "ALL") p_Line_Cd = "";
            var l = db.Fetch<mDailyOrder>("Daily_Order_Select", new { PROD_MONTH = p_Prod_Month }, new { ORDER_TYPE = p_Order_Type }, new { PLANT_CD = p_Plant_Cd }, new { LINE_CD = p_Line_Cd });
            db.Close();
            return l.ToList();
        }

        public List<mDailyOrder> SPN221GetDailyOrderAvailUsage(string p_Prod_Month, string p_Order_Type, string p_Plant_Cd, string p_Line_Cd)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            if (p_Line_Cd == "ALL") p_Line_Cd = "";
            var l = db.Fetch<mDailyOrder>("Daily_Order_Select_Avail_Usage", new { PROD_MONTH = p_Prod_Month }, new { ORDER_TYPE = p_Order_Type }, new { PLANT_CD = p_Plant_Cd }, new { LINE_CD = p_Line_Cd });
            db.Close();
            return l.ToList();
        }

        public string SPN221SaveDailyOrder(string p_Prod_Month, string p_Order_Type, string p_Part_No, string p_Line_Cd, string p_Order_Dt, string p_Production_Dt, string p_Ratio, string Time_Usage_1, string Time_Usage_2, string Time_Usage_3, string Time_Usage_4, string Time_Usage_5, string Time_Usage_6, string Time_Usage_7, string Time_Usage_8, string Time_Usage_9, string Time_Usage_10, string Time_Usage_11, string Time_Usage_12, string Time_Usage_13, string Time_Usage_14, string Time_Usage_15, string Time_Usage_16, string Time_Usage_17, string Time_Usage_18, string Time_Usage_19, string Time_Usage_20, string Time_Usage_21, string Time_Usage_22, string Time_Usage_23, string Time_Usage_24, string Time_Usage_25, string Time_Usage_26, string Time_Usage_27, string Time_Usage_28, string Time_Usage_29, string Time_Usage_30, string Time_Usage_31, string Time_Avail_1, string Time_Avail_2, string Time_Avail_3, string Time_Avail_4, string Time_Avail_5, string Time_Avail_6, string Time_Avail_7, string Time_Avail_8, string Time_Avail_9, string Time_Avail_10, string Time_Avail_11, string Time_Avail_12, string Time_Avail_13, string Time_Avail_14, string Time_Avail_15, string Time_Avail_16, string Time_Avail_17, string Time_Avail_18, string Time_Avail_19, string Time_Avail_20, string Time_Avail_21, string Time_Avail_22, string Time_Avail_23, string Time_Avail_24, string Time_Avail_25, string Time_Avail_26, string Time_Avail_27, string Time_Avail_28, string Time_Avail_29, string Time_Avail_30, string Time_Avail_31, string Qty_D_1, string Qty_D_2, string Qty_D_3, string Qty_D_4, string Qty_D_5, string Qty_D_6, string Qty_D_7, string Qty_D_8, string Qty_D_9, string Qty_D_10, string Qty_D_11, string Qty_D_12, string Qty_D_13, string Qty_D_14, string Qty_D_15, string Qty_D_16, string Qty_D_17, string Qty_D_18, string Qty_D_19, string Qty_D_20, string Qty_D_21, string Qty_D_22, string Qty_D_23, string Qty_D_24, string Qty_D_25, string Qty_D_26, string Qty_D_27, string Qty_D_28, string Qty_D_29, string Qty_D_30, string Qty_D_31, string p_Created_By)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "";

            var l = db.Fetch<mDailyOrder>("Daily_Order_Save", new { PROD_MONTH = p_Prod_Month }, new { ORDER_TYPE = p_Order_Type }, new { PART_NO = p_Part_No }, new { LINE_CD = p_Line_Cd }, new { ORDER_DT = p_Order_Dt }, new { PRODUCTION_DT = p_Production_Dt }, new { RATIO = p_Ratio }, new { TIME_USAGE_1 = Time_Usage_1 }, new { TIME_USAGE_2 = Time_Usage_2 }, new { TIME_USAGE_3 = Time_Usage_3 }, new { TIME_USAGE_4 = Time_Usage_4 }, new { TIME_USAGE_5 = Time_Usage_5 }, new { TIME_USAGE_6 = Time_Usage_6 }, new { TIME_USAGE_7 = Time_Usage_7 }, new { TIME_USAGE_8 = Time_Usage_8 }, new { TIME_USAGE_9 = Time_Usage_9 }, new { TIME_USAGE_10 = Time_Usage_10 }, new { TIME_USAGE_11 = Time_Usage_11 }, new { TIME_USAGE_12 = Time_Usage_12 }, new { TIME_USAGE_13 = Time_Usage_13 }, new { TIME_USAGE_14 = Time_Usage_14 }, new { TIME_USAGE_15 = Time_Usage_15 }, new { TIME_USAGE_16 = Time_Usage_16 }, new { TIME_USAGE_17 = Time_Usage_17 }, new { TIME_USAGE_18 = Time_Usage_18 }, new { TIME_USAGE_19 = Time_Usage_19 }, new { TIME_USAGE_20 = Time_Usage_20 }, new { TIME_USAGE_21 = Time_Usage_21 }, new { TIME_USAGE_22 = Time_Usage_22 }, new { TIME_USAGE_23 = Time_Usage_23 }, new { TIME_USAGE_24 = Time_Usage_24 }, new { TIME_USAGE_25 = Time_Usage_25 }, new { TIME_USAGE_26 = Time_Usage_26 }, new { TIME_USAGE_27 = Time_Usage_27 }, new { TIME_USAGE_28 = Time_Usage_28 }, new { TIME_USAGE_29 = Time_Usage_29 }, new { TIME_USAGE_30 = Time_Usage_30 }, new { TIME_USAGE_31 = Time_Usage_31 }, new { TIME_AVAIL_1 = Time_Avail_1 }, new { TIME_AVAIL_2 = Time_Avail_2 }, new { TIME_AVAIL_3 = Time_Avail_3 }, new { TIME_AVAIL_4 = Time_Avail_4 }, new { TIME_AVAIL_5 = Time_Avail_5 }, new { TIME_AVAIL_6 = Time_Avail_6 }, new { TIME_AVAIL_7 = Time_Avail_7 }, new { TIME_AVAIL_8 = Time_Avail_8 }, new { TIME_AVAIL_9 = Time_Avail_9 }, new { TIME_AVAIL_10 = Time_Avail_10 }, new { TIME_AVAIL_11 = Time_Avail_11 }, new { TIME_AVAIL_12 = Time_Avail_12 }, new { TIME_AVAIL_13 = Time_Avail_13 }, new { TIME_AVAIL_14 = Time_Avail_14 }, new { TIME_AVAIL_15 = Time_Avail_15 }, new { TIME_AVAIL_16 = Time_Avail_16 }, new { TIME_AVAIL_17 = Time_Avail_17 }, new { TIME_AVAIL_18 = Time_Avail_18 }, new { TIME_AVAIL_19 = Time_Avail_19 }, new { TIME_AVAIL_20 = Time_Avail_20 }, new { TIME_AVAIL_21 = Time_Avail_21 }, new { TIME_AVAIL_22 = Time_Avail_22 }, new { TIME_AVAIL_23 = Time_Avail_23 }, new { TIME_AVAIL_24 = Time_Avail_24 }, new { TIME_AVAIL_25 = Time_Avail_25 }, new { TIME_AVAIL_26 = Time_Avail_26 }, new { TIME_AVAIL_27 = Time_Avail_27 }, new { TIME_AVAIL_28 = Time_Avail_28 }, new { TIME_AVAIL_29 = Time_Avail_29 }, new { TIME_AVAIL_30 = Time_Avail_30 }, new { TIME_AVAIL_31 = Time_Avail_31 }, new { QTY_D_1 = Qty_D_1 }, new { QTY_D_2 = Qty_D_2 }, new { QTY_D_3 = Qty_D_3 }, new { QTY_D_4 = Qty_D_4 }, new { QTY_D_5 = Qty_D_5 }, new { QTY_D_6 = Qty_D_6 }, new { QTY_D_7 = Qty_D_7 }, new { QTY_D_8 = Qty_D_8 }, new { QTY_D_9 = Qty_D_9 }, new { QTY_D_10 = Qty_D_10 }, new { QTY_D_11 = Qty_D_11 }, new { QTY_D_12 = Qty_D_12 }, new { QTY_D_13 = Qty_D_13 }, new { QTY_D_14 = Qty_D_14 }, new { QTY_D_15 = Qty_D_15 }, new { QTY_D_16 = Qty_D_16 }, new { QTY_D_17 = Qty_D_17 }, new { QTY_D_18 = Qty_D_18 }, new { QTY_D_19 = Qty_D_19 }, new { QTY_D_20 = Qty_D_20 }, new { QTY_D_21 = Qty_D_21 }, new { QTY_D_22 = Qty_D_22 }, new { QTY_D_23 = Qty_D_23 }, new { QTY_D_24 = Qty_D_24 }, new { QTY_D_25 = Qty_D_25 }, new { QTY_D_26 = Qty_D_26 }, new { QTY_D_27 = Qty_D_27 }, new { QTY_D_28 = Qty_D_28 }, new { QTY_D_29 = Qty_D_29 }, new { QTY_D_30 = Qty_D_30 }, new { QTY_D_31 = Qty_D_31 }, new { CREATED_BY = p_Created_By });
            db.Close();

            foreach (var message in l)
            {
                result = message.Text.ToString();
            }
            return result;
        }

        public string SPN211DeleteDailyOrder(string p_Prod_Month, string p_Order_Type, string p_Part_No, string p_Line_Cd)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                if (p_Prod_Month.Contains(";"))
                {
                    string[] ProdMonthList = p_Prod_Month.Split(';');
                    string[] OrderTypeList = p_Order_Type.Split(';');
                    string[] PartNoList = p_Part_No.Split(';');
                    string[] LineCdList = p_Line_Cd.Split(';');
                    for (int i = 0; i < PartNoList.Length; i++)
                    {
                        var l = db.Fetch<mDailyOrder>("Daily_Order_Delete", new { PROD_MONTH = ProdMonthList[i] }, new { ORDER_TYPE = OrderTypeList[i] }, new { PART_NO = PartNoList[i] }, new { LINE_CD = LineCdList[i] });
                        foreach (var message in l)
                        {
                            result = result + message.Text;
                        }
                    }
                }
                else
                {
                    var l = db.Fetch<mDailyOrder>("Daily_Order_Delete", new { PROD_MONTH = p_Prod_Month }, new { ORDER_TYPE = p_Order_Type }, new { PART_NO = p_Part_No }, new { LINE_CD = p_Line_Cd });
                    foreach (var message in l)
                    {
                        result = result + message.Text;
                    }
                }
                db.Close();

                if (result.Contains("ERROR"))
                {
                    return "Process DELETE DAILY ORDER finish with error";
                }
                else
                {
                    return "Process DELETE DAILY ORDER finish successfully";
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }
    }
}