﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPOSSInquiry
    {
        public DateTime ORDER_DT { get; set; }
        public string ORDER_NO { get; set; }
        public string PART_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string TMAP_ORDER_QTY { get; set; }
        public string ORI_ORDER_QTY { get; set; }
        public string ADJUST_QTY {get; set;}
        public string ACCEPT_QTY { get; set; }
        public string ORDER_QTY { get; set; }
        public string SUM_PROCESS_QTY { get; set; }
        public DateTime TMMIN_SEND_DT { get; set; }
        public DateTime TMMIN_DELIVERY_PLAN_DT { get; set; }
        public string POSS_STATUS { get; set; }


        //untuk Pop Up
        public DateTime POSS_DATE { get; set; }
        public string CUST_NO { get; set; }
        
        public string INVOICE_NO { get; set; }
        public string INVOICE_ITEM_NO { get; set; }
        public string SEQUENCE { get; set; }
        public string BO_RELEASE_FLAG { get; set; }
        public string PORTION_CODE { get; set; }
        public string RECORD_ID { get; set; }
        public string DATA_ID { get; set; }
        public string BO_CODE { get; set; }
        public string RA_CODE { get; set; }
        public string ORDER_ITEM_NO { get; set; }
        public string PROCESS_PART_NO { get; set; }
        public string ORDER_PART_NO { get; set; }
        public string PROCESS_QTY { get; set; }
        public string UNIT_FOB_PRICE { get; set; }
        public string TARIF_CODE { get; set; }
        public string HS_CODE { get; set; }
        public string DIST_COLUMN { get; set; }
        public DateTime ETD { get; set; }
        public DateTime TMC_PROCESS_DT { get; set; }
        public DateTime PRICE_CALC_DT { get; set; }
        public string FILLER { get; set; }
        public DateTime SEND_DT { get; set; }



        public string Text { get; set; }

        //calling data while search process
        public List<mPOSSInquiry> getListPOSSInquiry(string pOrderDateFrom, string pOrderDateTo, string pOrderNo, string pPartNo, string pItemNo, string pStatusPOSS)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var list = db.Fetch<mPOSSInquiry>("GetPOSSInquiry",
                        new { ORDER_DT_FROM = pOrderDateFrom },
                        new { ORDER_DT_TO = pOrderDateTo },
                        new { ORDER_NO = pOrderNo },
                        new { PART_NO = pPartNo },
                        new { ITEM_NO = pItemNo },
                        new { POSS_STATUS = pStatusPOSS });
            db.Close();
            return list.ToList();
        }


        //SQLnya dibedain
        public List<mPOSSInquiry> getListPOSSDetail(string pU_OrderNo, string pU_ItemNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var list = db.Fetch<mPOSSInquiry>("GetPOSSDetail",
                        new object[] { pU_OrderNo, pU_ItemNo});
            db.Close();
            return list.ToList();

        }

    }
}
