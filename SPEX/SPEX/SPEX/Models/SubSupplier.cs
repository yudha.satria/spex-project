﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class SubSupplier
    {
        public string MAIN_SUPP_CD { get; set; }
        public string MAIN_PLANT_CD { get; set; }
        public string SUB_SUPP_CD { get; set; }
        public string SUB_PLANT_CD { get; set; }
        public string SUPP_NAME { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string Text { get; set; }

        public List<SubSupplier> getSubSupplier(SubSupplier SS)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<SubSupplier> l = db.Fetch<SubSupplier>("SubSupplier_Search",
                  new { MAIN_SUPP_CD = SS.MAIN_SUPP_CD },
                  new { SUB_SUPP_CD = SS.SUB_SUPP_CD },
                  new { MAIN_PLANT_CD = SS.MAIN_PLANT_CD },
                  new { SUB_PLANT_CD = SS.SUB_PLANT_CD }
                  ).ToList();
            db.Close();
            return l;
        }

     //Delete Data
        public string DeleteData(string SUB_SUPPLIER_LIST)
        {
            string result = "DELETE DATA SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            using (TransactionScope scope = new TransactionScope())
            { 
                if (SUB_SUPPLIER_LIST.Contains(";"))
                {
                    string[] arr_SUB_SUPPLIER_LIST = SUB_SUPPLIER_LIST.Split(';');
                    for (int i = 0; i < arr_SUB_SUPPLIER_LIST.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(arr_SUB_SUPPLIER_LIST[i]))
                        {
                            string[] arr_SUB_SUPPLIER_LIST2 = arr_SUB_SUPPLIER_LIST[i].Split('|');
                            var l = db.Fetch<mBuyerPD>("SubSupplier_Delete",
                                new { MAIN_SUPP_CD = arr_SUB_SUPPLIER_LIST2[0] },
                                new { MAIN_PLANT_CD = arr_SUB_SUPPLIER_LIST2[1] },
                                new { SUB_SUPP_CD = arr_SUB_SUPPLIER_LIST2[2] },
                                new { SUB_PLANT_CD = arr_SUB_SUPPLIER_LIST2[3] }
                            );
                        }
                    }
                }
                else
                {
                    string[] arr_SUB_SUPPLIER_LIST = SUB_SUPPLIER_LIST.Split('|');
                    db.Fetch<PORT_LOADING>("SubSupplier_Delete",
                        new { MAIN_SUPP_CD = arr_SUB_SUPPLIER_LIST[0] },
                        new { MAIN_PLANT_CD = arr_SUB_SUPPLIER_LIST[1] },
                        new { SUB_SUPP_CD = arr_SUB_SUPPLIER_LIST[2] },
                        new { SUB_PLANT_CD = arr_SUB_SUPPLIER_LIST[3] }
                    );
                }
                scope.Complete();
            }
            return result;
        }
        
        //Save Data
        public string SaveData(SubSupplier SS)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<SubSupplier>("SubSupplier_Save", new
                {
                    MAIN_SUPP_CD = SS.MAIN_SUPP_CD,
                    MAIN_PLANT_CD = SS.MAIN_PLANT_CD,
                    SUB_SUPP_CD = SS.SUB_SUPP_CD,
                    SUB_PLANT_CD = SS.SUB_PLANT_CD,
                    SUPP_NAME = SS.SUPP_NAME,
                    CREATED_BY = SS.CREATED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }


        ////Update Data
        public string UpdateData(SubSupplier SS)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<SubSupplier>("SubSupplier_Update", new
                {
                    MAIN_SUPP_CD = SS.MAIN_SUPP_CD,
                    MAIN_PLANT_CD = SS.MAIN_PLANT_CD,
                    SUB_SUPP_CD = SS.SUB_SUPP_CD,
                    SUB_PLANT_CD = SS.SUB_PLANT_CD,
                    SUPP_NAME = SS.SUPP_NAME,
                    CHANGED_BY = SS.CHANGED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        ////Update Data
        public string Sinchronize()
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<SubSupplier>("Sinchronize", new
                {
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }


    }
}
