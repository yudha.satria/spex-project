﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Collections;

namespace SPEX.Models
{
    public class Dock
    {
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }

        public List<Dock> GetAllDock()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<Dock>("GetDock").ToList();
            db.Close();
            return l;
        }

    }
}