﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Diani Widyaningrum
namespace SPEX.Models
{
    public class mPickingListOperation
    {
        public String TEXT { get; set; }
        public String MANIFEST_NO { get; set; }
        public String ZONE { get; set; }
        public String PART_NO { get; set; }
        public String RACK_ADDRESS_CD { get; set; }
        public String KANBAN_ID { get; set; }
        public decimal TOTAL_QTY { get; set; }

        public mPickingListOperation getListPickingListOperation(string pPICKING_LIST_NO, string pPART_NO, string pKANBAN_ID, string pSCAN_METHOD, string pUSER_LOGIN)
        
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            IList<mPickingListOperation> l = db.Fetch<mPickingListOperation>("SearchPickingListOperation",
                    new { PICKING_LIST_NO = pPICKING_LIST_NO },
                    new { PART_NO = pPART_NO },
                    new { KANBAN_ID = pKANBAN_ID },
                    new { SCAN_METHOD = pSCAN_METHOD },
                    new { USER_LOGIN = pUSER_LOGIN }
                    );
            db.Close();
            return l.SingleOrDefault();

        } 
    }


}
