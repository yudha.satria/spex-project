﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using System.Transactions;

namespace Toyota.Common.Web.Platform.Starter.Models
{
    public class PartMasterMaintenance
    {
        public List<string> PartType()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<string>("PartMasterMaintenanceGetPartType").ToList();
            db.Close();
            return l;
        }
    }
}
