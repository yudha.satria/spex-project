﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SPEX.Models
{
    public class OrderInquryDetailPartNo
    {
        public int NUM { get; set; }
        public string TMAP_ORDER_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public int? ORDER_QTY { get; set; }
        public decimal? PACKING_QTY { get; set; }
        public string CASE_NO { get; set; }
        public DateTime? PACKING_DT { get; set; }
        public string TMMIN_ORDER_NO { get; set; }
        public string CONTAINER_NO { get; set; }
        public DateTime? VANNING_DT { get; set; }
        public string INVOICE_NO { get; set; }
        public decimal? SHIPMENT_QTY { get; set; }
        public DateTime? ETD { get; set; }

    }
}