﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Web.Mvc;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class ReadyCargoInquiry
    {
        public string BUYER_CD { get; set; }
        public string PD_CD { get; set; }
        public string CONTAINER_NO { get; set; }
        public string SEAL_NO { get; set; }
        public string VANNING_NO { get; set; }
        public DateTime? VANNING_DT { get; set; }
        public DateTime? VANNING_DT_FROM { get; set; }
        public DateTime? VANNING_DT_TO { get; set; }
        public string INVOICE_NO { get; set; }
        public DateTime? INVOICE_DT { get; set; }
        public decimal CASE { get; set; }
        public decimal SIZE { get; set; }
        public decimal ITEM { get; set; }
        public decimal QTY { get; set; }
        public decimal GW { get; set; }
        public decimal NW { get; set; }
        public decimal M3 { get; set; }
        public string SA { get; set; }
        public string FA { get; set; }
        public DateTime? ETD { get; set; }
        public string ETD_STRING { get; set; }
        public string LOG_TYPE { get; set; }
        public string TRANSPORT_CD { get; set; }
        public string TRANSPORT_DESC { get; set; }
        public string PACKING_COMPANY { get; set; }
        public bool SETVISIBLE { get; set; }

        public string SYSTEM_VALUE { get; set; }
        public string SYSTEM_DESC { get; set; }
        
        public string INVOICE_STATUS { get; set; }
        public DateTime? INVOICE_DT_FROM { get; set; }
        public DateTime? INVOICE_DT_TO { get; set; }
        public DateTime? ETD_FROM { get; set; }
        public DateTime? ETD_TO { get; set; }
        
        public string MODE { get; set; }

        //add agi 2017-09-08

        public string DG_CARGO { get; set; }
        public List<BUYER> GetAllBuyerByBuyerPD()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<BUYER> l = db.Fetch<BUYER>("Ready_Cargo_BUYER_Search").ToList();
            db.Close();
            return l;
        }

        public List<mPDCode> GetAllPDByBuyerPD(string BUYER_CD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mPDCode> l = db.Fetch<mPDCode>("Ready_Cargo_PD_Search",new { BUYER_CD=BUYER_CD}).ToList();
                db.Close();
            
            return l;
        }

        public List<ReadyCargoInquiry> GetAllInvoiceStatus()
        {
            List<ReadyCargoInquiry> ListInvoiceStatus = new List<ReadyCargoInquiry>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            ListInvoiceStatus = db.Fetch<ReadyCargoInquiry>("GetAllInvoiceStatus").ToList();
            db.Close();
            return ListInvoiceStatus;
        }

        //public List<ReadyCargoInquiry> ReadyCargoInquirySearch(string pBUYER_CD, string pPD_CD, DateTime? pVANNING_DT_FROM, DateTime? pVANNING_DT_TO, string pInvoice_Status, string pINVOICE_NO, string pVANNING_NO, DateTime? pINVOICE_DT_FROM, DateTime? pINVOICE_DT_TO, DateTime? pETD_FROM, DateTime? pETD_TO, string puser_id, string pPACKING_COMPANY)
        //CHANGE AGI 2017-09-08  Insert field DG Cargo (same as Ready Cargo by Air) AND Searching menu by Case No
        public List<ReadyCargoInquiry> ReadyCargoInquirySearch(string pBUYER_CD, string pPD_CD, DateTime? pVANNING_DT_FROM, DateTime? pVANNING_DT_TO, string pInvoice_Status, string pINVOICE_NO, string pVANNING_NO, DateTime? pINVOICE_DT_FROM, DateTime? pINVOICE_DT_TO, DateTime? pETD_FROM, DateTime? pETD_TO, string puser_id, string pPACKING_COMPANY,string pCASE_NO)
        {
            List<ReadyCargoInquiry> ListReadyCargoInquiry = new List<ReadyCargoInquiry>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            ListReadyCargoInquiry = db.Fetch<ReadyCargoInquiry>("ReadyCargoInquirySearch",
                                                                new { pBUYER_CD = pBUYER_CD },
                                                                new { pPD_CD = pPD_CD },
                                                                new { pVANNING_DT_FROM = pVANNING_DT_FROM },
                                                                new { pVANNING_DT_TO = pVANNING_DT_TO },
                                                                new { pInvoice_Status = pInvoice_Status },
                                                                new { pINVOICE_NO = pINVOICE_NO },
                                                                new { pVANNING_NO = pVANNING_NO },
                                                                new { pINVOICE_DT_FROM = pINVOICE_DT_FROM },
                                                                new { pINVOICE_DT_TO = pINVOICE_DT_TO },
                                                                new { pETD_FROM = pETD_FROM },
                                                                new { pETD_TO = pETD_TO },
                                                                new {pPACKING_COMPANY=pPACKING_COMPANY},
                                                                new { puser_id = puser_id },
                                                                //ADD AGI 2017-09-08 Insert field DG Cargo (same as Ready Cargo by Air) AND Searching menu by Case No
                                                                new { pCASE_NO=pCASE_NO}
                                                                ).ToList();
            
            db.Close();
            return ListReadyCargoInquiry;
        }

        public string getUrlCreateInvoice()
        {
            string url = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            url = db.Fetch<string>("RADYCARGO_GET_URL_CREATE_INVOICE").First();
            db.Close();
            return url;
        }
        public void Check_PD_CD(List<ReadyCargoInquiry> models,out string ErrorMessage,out int ErrorCount)
        {
            string result=string.Empty;
            ErrorCount=0;
            ErrorMessage=string.Empty;
            var grouping_PD_CD= models.GroupBy(n => new {n.BUYER_CD, n.PD_CD})
                .Select(g => new {g.Key.BUYER_CD, g.Key.PD_CD}).ToList();
            foreach (var pd_cd in grouping_PD_CD)
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                result = db.Fetch<string>("READYCARGO_Check_BUYER_PD_CD",
                                                                new { pBUYER_CD = BUYER_CD },
                                                                new { pPD_CD = PD_CD }).First();
                if (result != "Y")
                {
                    ErrorMessage = ErrorMessage == string.Empty ? pd_cd.PD_CD : ErrorMessage + "," +  pd_cd.PD_CD;
                    ErrorCount = ErrorCount + 1;
                }
                db.Close();
            }
        }
        static GridViewSettings CreateExportGridViewSettings()
        {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "gvExport";
            //settings.Width = Unit.Percentage(100);

            //settings.Columns.Add("id");
            //settings.Columns.Add("group");
            //settings.Columns.Add("text");

            return settings;
        }

        public List<string> GetPackingCompany()
        {
            //string url = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("RADYCARGO_GET_GetPackingCompany").ToList();
            db.Close();
            return q;
        }
    }
}