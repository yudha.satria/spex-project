﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Muhajir Shiddiq
namespace SPEX.Models
{
    public class mStockInquiry
    {
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public int KANBAN_QTY { get; set; }
        public string SYSTEM_VALUE { get; set; }
        public string TEMP_RACK_ADDRESS_CD { get; set; }
        public string RACK_ADDRESS_CD { get; set; }
        public string DOCK_CD { get; set; }
        public int DAD { get; set; }
        public int MAD { get; set; }
        public decimal ORDER_CYCLE { get; set; }
        public decimal PROCUREMENT_LT { get; set; }
        public decimal RECEIVING_LT { get; set; }
        public decimal ALFA { get; set; }
        public int ROP { get; set; }
        public decimal SOH { get; set; }
        public string ACTUAL_STOCK { get; set; }
        public string REFERENCE { get; set; }
        public int ADJUST_QTY { get; set; }
        public int MIN_STOCK { get; set; }
        public int MAX_STOCK { get; set; }
        public string PART_DIMENSION { get; set; }
        public string PART_DIMENSION_NAME { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUP_NAME { get; set; }
        public string SUPP_NAME { get; set; }
        public string SUB_SUPPLIER_PLANT { get; set; }
        public string PLANT { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }

        public string Text { get; set; }



        public List<mStockInquiry> getListMaintenanceStock(string pPART_NO, string pRACK_ADDRESS_CD)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mStockInquiry>("StockInquiry/SearchStockInquiry",
                  new { PART_NO = pPART_NO },
                  new { RACK_ADDRESS_CD = pRACK_ADDRESS_CD });
            db.Close();
            return l.ToList();
        }

        public string UpdateData(string pPART_NO, string pPART_NAME, string pSUPPLIER_CD, string pSUB_SUPPLIER_CD, int pSOH, int pACTUAL_STOCK, string pRACK_ADDRESS_CD, string pREFERENCE, string pCREATED_BY, String iChangedBy, String iChangedDt)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            string l = db.SingleOrDefault<string>("StockInquiry/SaveStockInquiry",
                    new { PART_NO = pPART_NO },
                    new { PART_NAME = pPART_NAME },
                    new { SUPPLIER_CD = pSUPPLIER_CD },
                    new { SUB_SUPPLIER_CD = pSUB_SUPPLIER_CD },
                    new { SOH = pSOH },
                    new { ACTUAL_STOCK = pACTUAL_STOCK },
                    new { RACK_ADDRESS_CD = pRACK_ADDRESS_CD },
                    new { REF_FILE = pREFERENCE },
                    new { CREATED_BY = pCREATED_BY },
                    new { CHANGED_BY = iChangedBy },
                    new { CHANGED_DT = iChangedDt }                    
                    );
            db.Close();
            return l;
        }

        public string DeleteDataTBT()
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("StockInquiry/Adjust_Stock_Delete");
                    scope.Complete();
                }


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

        public string BackgroundProcessSave(string function_id, string module_id, string user_id, long PID, string refFileName)
        {

            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("StockInquiry/SP_STOCK_INQUIRY_CALL_JOB",
                       new { function_id = function_id },
                       new { module_id = module_id },
                       new { user_id = user_id },
                       new { PID = PID },
                       new { refFileName = refFileName }
                       );
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result = "1|" + ex.Message;
            }
            return result;
        }


    }
}
