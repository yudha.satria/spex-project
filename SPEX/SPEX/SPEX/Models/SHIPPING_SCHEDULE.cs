﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using NPOI.POIFS.FileSystem;
using NPOI.HPSF;
using System.Threading;
using System.Transactions;


namespace Toyota.Common.Web.Platform.Starter.Models
{
    public class SHIPPING_SCHEDULE
    {
        public	string		BUYER_CD		    {get;set;}
        public string       BUYER_NAME { get; set; }
        //public  BUYER Buyer                     {get;set;}
        public	string		FEEDER_VESSEL		{get;set;}
        public	string		FEEDER_VOYAGE		{get;set;}
        public	string		CONNECT_VESSEL		{get;set;}
        public	string		CONNECT_VOYAGE		{get;set;}
        public	DateTime?	ETA		            {get;set;}
        public  string      SHIP_AGENT_CD       {get;set;}
        public  string      SHIP_AGENT_NAME     {get;set;}
        public	DateTime?	VANNING_DT		    {get;set;}
        public	string		VANNING_DAY		    {get;set;}
        public	DateTime?	ETD		            {get;set;}
        public	string		ETD_DAY		        {get;set;}
        public	int		    CONTAINER_PLAN		{get;set;}
        public  string      FORWARD_AGENT_CD    {get;set;}
        public  string      FORWARD_AGENT_NAME  {get;set;}
        public	string		BOOKING_NO		    {get;set;}
        public	DateTime?	CLOSING_TM		    {get;set;}
        public  string      PORT_LOADING_CD     {get;set;}
        public  string      PORT_LOADING_NAME   {get;set;}
        public	string		CREATED_BY		    {get;set;}
        public	DateTime?	CREATED_DT		    {get;set;}
        public	string		CHANGED_BY		    {get;set;}
        public	DateTime?	CHANGED_DT		    {get;set;}


        public string Text { get; set; }

        public List<SHIPPING_SCHEDULE> getSHIPPING_SCHEDULE(string BUYER_CD, DateTime VANNING_DT_FROM, DateTime VANNING_DT_TO, DateTime ETD_FROM, DateTime ETD_TO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<SHIPPING_SCHEDULE> l = db.Fetch<SHIPPING_SCHEDULE>("SHIPPING_SCHEDULE_Search",
                  new { BUYER_CD = BUYER_CD },
                  new { VANNING_DT_FROM = VANNING_DT_FROM },
                  new { VANNING_DT_TO = VANNING_DT_TO },
                  new { ETD_FROM = ETD_FROM },
                  new { ETD_TO = ETD_TO }
                  ).ToList();
            db.Close();
            return l;
        }

     //Delete Data
        public string DeleteDataTBT()
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Fetch<SHIPPING_SCHEDULE>("Tb_T_ship_Schedule_Delete");
                    scope.Complete();
                }

                
            }
            catch (TransactionAbortedException ex)
            {
                result =  ex.Message;
            }
            catch (ApplicationException ex)
            {
                result =  ex.Message;
            }
            return result;
        }

        public string DeleteDataTBR(List<string> keyValues)
        {
            string result = "DELETE DATA SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (string a in keyValues)
                    {
                        var key = a.Split('|');
                        BUYER_CD = key[0];
                        FEEDER_VESSEL = key[1];
                        FEEDER_VOYAGE = key[2];
                        CONNECT_VESSEL = key[3];
                        CONNECT_VOYAGE = key[4];
                        if (key[5] != string.Empty)
                        {
                            //
                            ETA = DateTime.ParseExact(key[5], "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                        }
                        else
                        {
                            ETA = DateTime.ParseExact("1753-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                        }
                        SHIP_AGENT_CD = key[6];
                        VANNING_DT = DateTime.ParseExact(key[7], "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                        ETD = DateTime.ParseExact(key[8], "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                        db.Fetch<SHIPPING_SCHEDULE>("TB_R_Ship_Schedule_Delete",
                             new { p_BUYER_CD = BUYER_CD },
                             new { p_FEEDER_VESSEL = FEEDER_VESSEL },
                             new { p_FEEDER_VOYAGE = FEEDER_VOYAGE },
                             new { p_CONNECT_VESSEL = CONNECT_VESSEL },
                             new { p_CONNECT_VOYAGE = CONNECT_VOYAGE },
                             new { p_ETA = ETA },
                             new { p_SHIP_AGENT_CD = SHIP_AGENT_CD },
                             new { p_VANNING_DT = VANNING_DT },
                             new { p_ETD = ETD }
                             );
                    }
                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                result =  ex.Message;
            }
            catch (ApplicationException ex)
            {
                result =  ex.Message;
            }
            return result;;
        }
       
        public void Save_Master_SHIPPING_SCHEDULE(long pid ,string user_id)
        {
            string strConnection = DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString;            
            
                //call stored procedure which will run longer time since it calls another remote stored procedure and
                //waits until it's done processing
                SqlConnection conn = new SqlConnection(strConnection);
                conn.Open();
                SqlCommand cmd = new SqlCommand("SAVE_SHIPPING_SCHEDULE", conn);
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@process_id", pid));
                cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
                cmd.ExecuteReader();
            
        }

        public string CheckExistingShipingSchedule()
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<string>("CheckExistingDataInSHIPPING_SCHEDULE");
            db.Close();
            foreach (var message in l)
            { result = message.ToString(); }

            return result;
        }
        
        


    }
}


  