﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : SERVICE PARTS EXPORT (SPEX)
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : DeviceUserMaster
 * Function Name    : Device User Master
 * Function Group   : 
 * Program Id       : DeviceUserMaster
 * Program Name     : DeviceUserMaster
 * Program Type     : Model
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID.Yusuf
 * Version          : 01.00.00
 * Creation Date    : 05/09/2017 09:35:40
 * 

 *
 * Copyright(C) 2017 - . All Rights Reserved                                                                                              
 *************************************************************************************************/

using SPEX.General.Validation;
using SPEX.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SPEX.Models.DeviceUserMaster
{
    public class DeviceUserMaster : SPEXBaseModel
    {
        public DeviceUserMaster() { }
        public DeviceUserMaster(
            string UserLogin,
            string UserPassword,
            string UserName, 
            string PackingCompany,
            string CompanyPlantCode,
            string PlantLineCode,
            string TransportCode,
            string DeviceNWIdent,
            string Position,
            string LoginStatus,
            string CreatedBy,
            string CreatedDate,
            string ChangedBy,
            string ChangedDate,
            string UserPaswd)
        {
            this.UserLogin = UserLogin;
            this.UserPassword = UserPassword;
            this.UserName = UserName;
            this.PackingCompany = PackingCompany;
            this.CompanyPlantCode = CompanyPlantCode;
            this.PlantLineCode = PlantLineCode;
            this.TransportCode = TransportCode;
            this.DeviceNWIdent = DeviceNWIdent;
            this.Position = Position;
            this.LoginStatus = LoginStatus;
            this.CreatedBy = CreatedBy;
            this.ChangedDate = ChangedDate;
            this.CreatedDate = CreatedDate;
            this.ChangedBy = ChangedBy;
            this.UserPaswd = UserPaswd;

        }

        [SPEXMandatory(aliasName = "User Login")]
        public string UserLogin { get; set; }

        [SPEXMandatory(aliasName = "User Pasword")]
        public string UserPassword { get; set; }

        [SPEXMandatory(aliasName = "User Name")]
        public string UserName { get; set; }

        [SPEXMandatory(aliasName = "Packing Company")]
        public string PackingCompany { get; set; }

        [SPEXMandatory(aliasName = "Company Plant Code")]
        public string CompanyPlantCode { get; set; }

        [SPEXMandatory(aliasName = "Plant Line Code")]
        public string PlantLineCode { get; set; }

        [SPEXMandatory(aliasName = "Transport Code")]
        public string TransportCode { get; set; }

        public string DeviceNWIdent { get; set; }

        [SPEXMandatory(aliasName = "Position")]
        public string Position { get; set; }

        public string LoginStatus { get; set; }

        [SPEXMandatory(aliasName = "Password")]
        public string Password { get; set; }

        [SPEXMandatory(aliasName = "Created By")]
        public string CreatedBy { get; set; }

        [SPEXMandatory(aliasName = "Created Date")]
        public string CreatedDate { get; set; }

        [SPEXMandatory(aliasName = "Changed By")]
        public string ChangedBy { get; set; }

        [SPEXMandatory(aliasName = "Changed Date")]
        public string ChangedDate { get; set; }

        [SPEXMandatory(aliasName = "User Paswd")]
        public string UserPaswd { get; set; }


        public string CreatedDtTxt
        {
            get
            {
                return GetTxtDate(CreatedDate);
            }
        }

        public string ChangedDtTxt
        {
            get
            {
                return GetTxtDate(ChangedDate);
            }
        }

        private string GetTxtDate(string d)
        {
            string r = "";
            string[] s = null;
            if (string.IsNullOrEmpty(d))
            {
                r = "";
            }
            else
            {
                s = d.Split(' ');
                s = s[0].Split('/');
                r = s[0] + "." + s[1] + "." + s[2];
            }
            return r;
        }

    }
}