﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : SERVICE PARTS EXPORT (SPEX)
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : DeviceUserMaster
 * Function Name    : Device User Master
 * Function Group   : 
 * Program Id       : DeviceUserMaster
 * Program Name     : DeviceUserMasterRepository
 * Program Type     : Repository (data access object)
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID.Yusuf
 * Version          : 01.00.00
 * Creation Date    : 05/09/2017 09:35:40
 * 

 *
 * Copyright(C) 2017 - . All Rights Reserved                                                                                              
 *************************************************************************************************/

using SPEX.Controllers.DeviceUserMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models.DeviceUserMaster
{
    public static class DeviceUserMasterRepository
    {
        public static List<String> GetCompanyPlantCodeList()
        {
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<String>("DeviceUserMaster/GET_COMPANY_PLANT_CODE_COMBOBOX", new { });
                db.Close();
                return l.ToList();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }  
                return new List<string>();
            }
        }

        public static List<String> GetPackingCompanyList()
        {
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<String>("DeviceUserMaster/GET_PACKING_COMPANY_COMBOBOX", new { });
                db.Close();
                return l.ToList();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }                
                return new List<string>();
            }
        }

        public static List<String> GetPlantLineCodeList()
        {
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<String>("DeviceUserMaster/GET_PLANT_LINE_CODE_COMBOBOX", new { });
                db.Close();
                return l.ToList();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }  
                return new List<string>();
            }
        }

        public static List<String> GetTransportCodeList()
        {
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                //var l = db.Fetch<String>("DeviceUserMaster/GET_COMPANY_PLANT_CODE_COMBOBOX", new { });
                var l = db.Fetch<String>("DeviceUserMaster/GET_TRANSPORT_CODE_COMBOBOX", new { });
                db.Close();
                return l.ToList();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }
                return new List<string>();
            }
        }

        public static List<String> GetPositionList()
        {
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<String>("DeviceUserMaster/GET_POSITION_COMBOBOX", new { });
                db.Close();
                return l.ToList();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }
                return new List<string>();
            }
        }

        public static List<DeviceUserMaster> getList(DeviceUserMasterSearch data)
        {
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<DeviceUserMaster>("DeviceUserMaster/DeviceUserMaster_GetList",
                    new
                    {
                        UserLogin = data.UserLogin
                        ,UserName = data.UserName
                        ,PackingCompany = data.PackingCompany
                        ,CompanyPlantCode = data.CompanyPlantCode
                        ,PlantLineCode = data.PlantLineCode
                    }
                 );
                db.Close();
                return l.ToList();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }
                return new List<DeviceUserMaster>();
            }
            //return getDummyResult();
        }

        public static int DeleteUserDevice(string UserLogin)
        {
            int result = 0;
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                result = db.Execute("DeviceUserMaster/DeleteUserDevice", new { UserLogin = UserLogin });
                db.Close();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return result;
        }

        public static int ResetUserDevice(string UserLogin, string userAppName)
        {
            int result = 0;
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                result = db.Execute("DeviceUserMaster/ResetUserDevice", 
                    new { 
                        UserLogin = UserLogin,
                        DefaultPassword = "toyota123",
                        UserID = userAppName
                    });
                db.Close();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return result;
        }

        public static int AddUserDevice(DeviceUserMasterSearch param, string userAppName)
        {
            int result = 0;
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                result = db.Execute("DeviceUserMaster/AddNewUserDevice",
                    new
                    {
                        UserLogin = param.UserLogin
                        ,UserName = param.UserName
                        ,UserPassword = param.password
                        ,PackingCompany = param.PackingCompany
                        ,CompanyPlantCode = param.CompanyPlantCode
                        ,PlantLineCode = param.PlantLineCode
                        ,TransportCode = param.TransportCode
                        ,DeviceNWIdent = param.DeviceNWIdent
                        ,LoginStatus = "0"
                        ,PositionCode = param.PositionCode
                        ,UserId = userAppName
                    });
                db.Close();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return result;
        }

        public static int UpdateUserDevice(DeviceUserMasterSearch param, string userAppName)
        {
            int result = 0;
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                result = db.Execute("DeviceUserMaster/UpdateExistingUserDevice",
                    new
                    {
                        UserLogin = param.UserLogin,
                        UserName = param.UserName,
                        Password = param.password,
                        PackingCompany = param.PackingCompany,
                        CompanyPlantCode = param.CompanyPlantCode,
                        PlantLineCode = param.PlantLineCode,
                        TransportCode = param.TransportCode,
                        DeviceNWIdent = param.DeviceNWIdent,
                        PositionCode = param.PositionCode,
                        UserId = userAppName
                    });
                db.Close();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return result;
        }

        public static void ForceLogoff(string UserLogin)
        {
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                db.Execute("DeviceUserMaster/ForceLogoffExistingUserDevice",
                    new
                    {
                        UserLogin = UserLogin
                    });
                db.Close();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public static bool ForceLogoffV2(string UserLogin)
        {
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                db.Execute("DeviceUserMaster/ForceLogoffExistingUserDevice",
                    new
                    {
                        UserLogin = UserLogin
                    });
                db.Close();

                return true;
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }
                return false;
            }
        }

        #region dummy static local data
        private static List<DeviceUserMaster> getDummyResult()
        {
            List<DeviceUserMaster> result = new List<DeviceUserMaster>();
            result.Add(new DeviceUserMaster("1", "2", "3", "4", "5", "6", "7", "8", "9","10","11","12","13","14","15"));
            result.Add(new DeviceUserMaster("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"));
            result.Add(new DeviceUserMaster("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"));
            result.Add(new DeviceUserMaster("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"));
            result.Add(new DeviceUserMaster("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"));
            result.Add(new DeviceUserMaster("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"));
            result.Add(new DeviceUserMaster("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"));
            return result;
        }
        #endregion               
    }
}