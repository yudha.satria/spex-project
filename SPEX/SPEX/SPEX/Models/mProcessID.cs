﻿using System;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mProcessID
    {
        //FUNGSI GETHER SETTER - Sama dengan Query Database
        public string Text { get; set; }

        public string GetProcessID()
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mProcessID>("Get_Process_ID");
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }
    }
}