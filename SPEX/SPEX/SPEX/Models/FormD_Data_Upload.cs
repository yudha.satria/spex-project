﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class FormD_Data_Upload
    {

        public class InvoiceHeaderUpload
        {
            public string I9INVN { get; set; }
            public string I9INVD { get; set; }
            public string I9NAME { get; set; }
            public string I9ADR1 { get; set; }
            public string I9ADR2 { get; set; }
            public string I9ADR3 { get; set; }
            public string I9LOAD { get; set; }
            public string I9DISC { get; set; }
            public string I9VESL { get; set; }
            public string I9ETD { get; set; }
            public string I9PAY1 { get; set; }
            public string I9PAY2 { get; set; }
            public string I9SPC1 { get; set; }
            public string I9SPC2 { get; set; }
            public string I9SPC3 { get; set; }
            public string I9SPC4 { get; set; }
            public string I9SPC5 { get; set; }
            public string I9TQTY { get; set; }
            public string I9FOBA { get; set; }
            public string I9FRGA { get; set; }
            public string I9INSA { get; set; }
            public string I9PRCT { get; set; }
            public string I9TOTL { get; set; }
            public string I9TOTC { get; set; }
            public string I9GROS { get; set; }
            public string I9NET { get; set; }
            public string I9MEAS { get; set; }
            public string I9FPSN { get; set; }
            public string I9FPRT { get; set; }
            public string I9FPHJ { get; set; }
            public string NEW_FLAG { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DT { get; set; }
            public string CHANGE_DBY { get; set; }
            public DateTime CHANGE_DT { get; set; }

        }
        

        public class Orderdata
        {
            public string OHTNSO { get; set; }
            public string OHBYR { get; set; }
            public string OHPD { get; set; }
            public string OHORDN { get; set; }
            public string OHORDD { get; set; }
            public string OHTRAN { get; set; }
            public string OHORDT { get; set; }
            public string OHBOIN { get; set; }
            public string OHPAYT { get; set; }
            public string OHPRCT { get; set; }
            public string OHCURR { get; set; }
            public string OHSLR { get; set; }
            public string OHOBYR { get; set; }
            public string OHOPD { get; set; }
            public string OHSPLT { get; set; }
            public string OHTITM { get; set; }
            public string OHTQTY { get; set; }
            public string OHOITM { get; set; }
            public string OHOQTY { get; set; }
            public string OHCRTD { get; set; }
            public string OHLUPD { get; set; }
            public string NEW_FLAG { get; set; }
            public string CREATE_BY { get; set; }
            public string CREATE_DT { get; set; }
            public string CHANGE_DBY { get; set; }
            public string CHANGE_DT { get; set; }

        }

        public class InvoiceDetailUpload
        {
            public string F9INVN { get; set; }
            public string F9INVD { get; set; }
            public string F9SELR { get; set; }
            public string F9BYR { get; set; }
            public string F9PD { get; set; }
            public string F9CURR { get; set; }
            public string F9CASN { get; set; }
            public string F9ORDN { get; set; }
            public string F9ITEM { get; set; }
            public string F9PNO { get; set; }
            public string F9PNME { get; set; }
            public string F9QTY { get; set; }
            public string F9UNIT { get; set; }
            public string F9AMNT { get; set; }
            public string F9TINV { get; set; }
        }

        public string Text { get; set; }

        
        
        //Delete Data
        public string DeleteDataTB_T_ID()
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Fetch<InvoiceDetailUpload>("FormD_Data_Upload_InvoiceDetail_DeleteTemp");
                    scope.Complete();
                }


            }
            catch (TransactionAbortedException ex)
            {
                result = ex.Message;
            }
            catch (ApplicationException ex)
            {
                result = ex.Message;
            }
            return result;
        }

        
        public void Save_InvoiceHeader(long pid, string user_id, string function_id, string module_id)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Execute("FormD_Data_Upload_InvoiceHeader_Insert",
                             new { process_id = pid },
                             new { user_id = user_id },
                             new { function_id = function_id },
                             new { module_id = module_id }
                );
            db.Close();
            //foreach (var message in l)
            //{ result = message.ToString(); }
            //return result;
        }

        public void Save_InvoiceDetail(long pid, string user_id, string function_id, string module_id)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Execute("FormD_Data_Upload_InvoiceDetail_Insert",
                             new { process_id = pid },
                             new { user_id = user_id },
                             new { function_id = function_id },
                             new { module_id = module_id }
                );
            db.Close();
            //foreach (var message in l)
            //{ result = message.ToString(); }
            //return result;
        }

        //public string CheckExistingDataInvoiceHeaderTemp()
        //{
        //    string result = string.Empty;
        //    IDBContext db = DatabaseManager.Instance.GetContext();
        //    var l = db.Fetch<string>("FormD_Data_Upload_CheckExistingDataInvoiceHeaderTemp");
        //    db.Close();
        //    foreach (var message in l)
        //    { result = message.ToString(); }

        //    return result;
        //}

        public List<string> GetPackingCompany()
        {
            //string url = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("GetPackingCompany").ToList();
            db.Close();
            return q;
        }

        public void InsertTempInvoiceHeaderUpload(InvoiceHeaderUpload data, string USER_ID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.Execute("FormD_Data_Upload_InvoiceHeader_InsertTemp",
                             new { I9INVN = data.I9INVN },
                             new { I9INVD = data.I9INVD },
                             new { I9NAME = data.I9NAME },
                             new { I9ADR1 = data.I9ADR1 },
                             new { I9ADR2 = data.I9ADR2 },
                             new { I9ADR3 = data.I9ADR3 },
                             new { I9LOAD = data.I9LOAD },
                             new { I9DISC = data.I9DISC },
                             new { I9VESL = data.I9VESL },
                             new { I9ETD = data.I9ETD },
                             new { I9PAY1 = data.I9PAY1 },
                             new { I9PAY2 = data.I9PAY2 },
                             new { I9SPC1 = data.I9SPC1 },
                             new { I9SPC2 = data.I9SPC2 },
                             new { I9SPC3 = data.I9SPC3 },
                             new { I9SPC4 = data.I9SPC4 },
                             new { I9SPC5 = data.I9SPC5 },
                             new { I9TQTY = data.I9TQTY },
                             new { I9FOBA = data.I9FOBA },
                             new { I9FRGA = data.I9FRGA },
                             new { I9INSA = data.I9INSA },
                             new { I9PRCT = data.I9PRCT },
                             new { I9TOTL = data.I9TOTL },
                             new { I9TOTC = data.I9TOTC },
                             new { I9GROS = data.I9GROS },
                             new { I9NET = data.I9NET },
                             new { I9MEAS = data.I9MEAS },
                             new { I9FPSN = data.I9FPSN },
                             new { I9FPRT = data.I9FPRT },
                             new { I9FPHJ = data.I9FPHJ },
                             new { USER_ID = USER_ID }
                );
            db.Close();
        }
        
        public string DeleteData_TB_T_OD()
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Fetch<VESSEL_SCHEDULE>("FormD_Data_Upload_OrderData_DeleteTemp");
                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                result = ex.Message;
            }
            catch (ApplicationException ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public string DeleteData_TB_T_IH()
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Fetch<VESSEL_SCHEDULE>("FormD_Data_Upload_InvoiceHeader_DeleteTemp");
                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                result = ex.Message;
            }
            catch (ApplicationException ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public void InsertTempOrderDataUpload(Orderdata data, string USER_ID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.Execute("FormD_Data_Upload_OrderData_InsertTemp",
                                new { OHTNSO = data.OHTNSO },
                                new { OHBYR = data.OHBYR },
                                new { OHPD = data.OHPD },
                                new { OHORDN = data.OHORDN },
                                new { OHORDD = data.OHORDD },
                                new { OHTRAN = data.OHTRAN },
                                new { OHORDT = data.OHORDT },
                                new { OHBOIN = data.OHBOIN },
                                new { OHPAYT = data.OHPAYT },
                                new { OHPRCT = data.OHPRCT },
                                new { OHCURR = data.OHCURR },
                                new { OHSLR = data.OHSLR },
                                new { OHOBYR = data.OHOBYR },
                                new { OHOPD = data.OHOPD },
                                new { OHSPLT = data.OHSPLT },
                                new { OHTITM = data.OHTITM },
                                new { OHTQTY = data.OHTQTY },
                                new { OHOITM = data.OHOITM },
                                new { OHOQTY = data.OHOQTY },
                                new { OHCRTD = data.OHCRTD },
                                new { OHLUPD = data.OHLUPD },
                                new { NEW_FLAG = data.NEW_FLAG },
                                new { USER_ID = USER_ID }
                );
            db.Close();
        }

        public void Save_OrderData(long pid, string user_id, string function_id, string module_id)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Execute("FormD_Data_Upload_OrderData_Insert",
                             new { process_id = pid },
                             new { user_id = user_id },
                             new { function_id = function_id },
                             new { module_id = module_id }
                );
            db.Close();
            //foreach (var message in l)
            //{ result = message.ToString(); }
            //return result;
        }

        //public void InsertTempInvoiceHeaderUpload(InvoiceHeaderUpload data, string USER_ID)
        //{
        //    string result = string.Empty;
        //    IDBContext db = DatabaseManager.Instance.GetContext();
        //    db.Execute("FormD_Data_Upload_InvoiceHeader_InsertTemp",
        //                     new { I9INVN = data.I9INVN },
        //                     new { I9INVD = data.I9INVD },
        //                     new { I9NAME = data.I9NAME },
        //                     new { I9ADR1 = data.I9ADR1 },
        //                     new { I9ADR2 = data.I9ADR2 },
        //                     new { I9ADR3 = data.I9ADR3 },
        //                     new { I9LOAD = data.I9LOAD },
        //                     new { I9DISC = data.I9DISC },
        //                     new { I9VESL = data.I9VESL },
        //                     new { I9ETD = data.I9ETD },
        //                     new { I9PAY1 = data.I9PAY1 },
        //                     new { I9PAY2 = data.I9PAY2 },
        //                     new { I9SPC1 = data.I9SPC1 },
        //                     new { I9SPC2 = data.I9SPC2 },
        //                     new { I9SPC3 = data.I9SPC3 },
        //                     new { I9SPC4 = data.I9SPC4 },
        //                     new { I9SPC5 = data.I9SPC5 },
        //                     new { I9TQTY = data.I9TQTY },
        //                     new { I9FOBA = data.I9FOBA },
        //                     new { I9FRGA = data.I9FRGA },
        //                     new { I9INSA = data.I9INSA },
        //                     new { I9PRCT = data.I9PRCT },
        //                     new { I9TOTL = data.I9TOTL },
        //                     new { I9TOTC = data.I9TOTC },
        //                     new { I9GROS = data.I9GROS },
        //                     new { I9NET = data.I9NET },
        //                     new { I9MEAS = data.I9MEAS },
        //                     new { I9FPSN = data.I9FPSN },
        //                     new { I9FPRT = data.I9FPRT },
        //                     new { I9FPHJ = data.I9FPHJ },
        //                     new { USER_ID = USER_ID }
        //        );
        //    db.Close();
        //}

        public void InsertTempInvoiceDetaiUpload(InvoiceDetailUpload data, string USER_ID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.Execute("FormD_Data_Upload_InvoiceDetail_InsertTemp",
                                new {	F9INVN	= data.F9INVN},
                                new {	F9INVD	= data.F9INVD},
                                new {	F9SELR	= data.F9SELR},
                                new {	F9BYR	= data.F9BYR},
                                new {	F9PD	= data.F9PD},
                                new {	F9CURR	= data.F9CURR},
                                new {	F9CASN	= data.F9CASN},
                                new {	F9ORDN	= data.F9ORDN},
                                new {	F9ITEM	= data.F9ITEM},
                                new {	F9PNO	= data.F9PNO},
                                new {	F9PNME	= data.F9PNME},
                                new {	F9QTY	= data.F9QTY},
                                new {	F9UNIT	= data.F9UNIT},
                                new {	F9AMNT	= data.F9AMNT},
                                new {	F9TINV	= data.F9TINV},
                                new { USER_ID = USER_ID }
                );
            db.Close();
        }

    }
}


