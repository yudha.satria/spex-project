﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPOSSDataInquiry
    {
        public string POSS_DATAID { get; set; }
        public string POSSHEAD_LINENO { get; set; }
        public string PACKING_COMPANY { get; set; }
        public string RECORD_ID { get; set; }
        public string DATA_ID { get; set; }
        public string DIST_FD_CD { get; set; }
        public string ORDER_NO { get; set; }
        public string TRANSPORT_CD { get; set; }
        public string PORTION_CD { get; set; }
        public string BO_INSTRUCTION_CD { get; set; }
        public string SENDER_CD { get; set; }
        public string TMAP_SEND { get; set; }
        public DateTime? TMAP_SEND_DT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string POSSLINE_LINENO { get; set; }
        public string BO_CD { get; set; }
        public string RA_CD { get; set; }
        public string ORDER_ITEM_NO { get; set; }
        public string PROCESS_PART_NO { get; set; }
        public string ORDER_PART_NO { get; set; }
        public string PROCESS_QTY { get; set; }
        public string ORDER_QTY { get; set; }
        public string UNIT_FOB_PRICE { get; set; }
        public string TARIF_CD { get; set; }
        public string HS_CD { get; set; }
        public string DIST_COLUMN { get; set; }
        public DateTime? ETD { get; set; }
        public string FINAL_FOB_PRICE { get; set; }

        public List<mPOSSDataInquiry> GetList(string pPOSS_DT_FROM, string pPOSS_DT_TO, string pTMAP_SEND, 
            string pPACKING_COMPANY, string pORDER_NO, string pPART_NO, string pITEM_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPOSSDataInquiry>("POSSDataInquiry_GetList",
                  new { POSS_DT_FROM = pPOSS_DT_FROM },
                  new { POSS_DT_TO = pPOSS_DT_TO },
                  new { TMAP_SEND = pTMAP_SEND },
                  new { PACKING_COMPANY = pPACKING_COMPANY },
                  new { ORDER_NO = pORDER_NO },
                  new { PART_NO = pPART_NO },
                  new { ITEM_NO = pITEM_NO }
                  );
            db.Close();

            return l.ToList();
        }
    }
}