﻿using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mLogMonitoring
    {
        //FUNGSI GETHER SETTER 
        public string rownum { get; set; }
        public string PROCESS_ID { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string MODULE_ID { get; set; }
        public string MODULE_NAME { get; set; }
        public string FUNCTION_ID { get; set; }
        public string FUNCTION_NAME { get; set; }
        public string PROCESS_STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public string READ_FLAG { get; set; }
        public string REMARK { get; set; }
        public string SEQ_NO { get; set; }
        public string ERR_DT { get; set; }
        public string ERR_LOC { get; set; }
        public string MSG_TYPE { get; set; }
        public string MSG_ID { get; set; }
        public string ERR_MSG { get; set; }
        public string SEQUENCE_NUMBER { get; set; }
        public string LOCATION { get; set; }
        public string MESSAGE { get; set; }
  
        //PEMANGGILAN DATA YANG AKAN DI SEARCH DAN DI TAMPILKAN (CEKNYA DI SQL)
        public List<mLogMonitoring> getListLogMontoring(string p_PROCESS_ID, string p_START_DATE, string p_END_DATE, string p_FUNCTION_ID, string p_CREATED_BY)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mLogMonitoring>("getListLogMontoring",

               new { PROCESS_ID = p_PROCESS_ID },
               new { START_DATE = p_START_DATE },
               new { END_DATE = p_END_DATE },
               new { FUNCTION_ID = p_FUNCTION_ID },
               new { CREATED_BY = p_CREATED_BY });
               db.Close();
            return l.ToList();
        }

        public List<mLogMonitoring> getListLogMontoringDetail(string p_PROCESS_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mLogMonitoring>("getListLogMontoringDetail",

               new { PROCESS_ID = p_PROCESS_ID });
            db.Close();
            return l.ToList();
        }

        public List<mLogMonitoring> getListLogMontoringHeader(string p_PROCESS_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mLogMonitoring>("getListLogMontoringHeader",

               new { PROCESS_ID = p_PROCESS_ID });
            db.Close();
            return l.ToList();
        }

        //untuk look up master

        public List<mLogMonitoring> GetListMasterFunction()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mLogMonitoring>("GetListMasterFunction");
            db.Close();
            return l.ToList();

        }
    }
}
