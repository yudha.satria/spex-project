﻿using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mMasterLine
    {
        public string LINE_CD { get; set; }
        public string PLANT_CD { get; set; }
        public string SLOC_CD { get; set; }
        public string DESCRIPTION { get; set; }

        public List<mMasterLine> getListMasterLine()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            mMasterLine AllMasterLine = new mMasterLine();
            AllMasterLine.LINE_CD = "";
            AllMasterLine.PLANT_CD = "";
            AllMasterLine.SLOC_CD = "";
            AllMasterLine.DESCRIPTION = "";

            var l = db.Fetch<mMasterLine>("Master_Line_Lookup");
            db.Close();
            l.Insert(0, AllMasterLine);
            return l.ToList();
        }
    }

    public class mMasterPlant
    {
        public string PLANT_CD { get; set; }
        public string SLOC_CD { get; set; }
        public string DESCRIPTION { get; set; }

        public List<mMasterPlant> getListMasterPlant()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMasterPlant>("Master_Plant_Lookup");
            db.Close();

            mMasterPlant AllMasterPlant = new mMasterPlant();
            AllMasterPlant.PLANT_CD = "";
            AllMasterPlant.SLOC_CD = "";
            AllMasterPlant.DESCRIPTION = "";

            l.Insert(0, AllMasterPlant);
            return l.ToList();
        }
    }

    public class mMasterFunction
    {
        public string FUNCTION_ID { get; set; }
        public string FUNCTION_NAME { get; set; }


        public List<mMasterFunction> getListMasterFunction()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMasterFunction>("SP_GetFunction_Combobox");
            db.Close();
            mMasterFunction AllMasterFunction = new mMasterFunction();
            AllMasterFunction.FUNCTION_ID = "";
            AllMasterFunction.FUNCTION_NAME = "";

            l.Insert(0, AllMasterFunction);
            return l.ToList();

        }
    }

    public class mMDPCreationPartno
    {
        public string PART_NO { get; set; }


        public List<mMDPCreationPartno> getListMDPCreationPartno()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMDPCreationPartno>("SP_spn251Get_Partno_Combobox");
            db.Close();
            return l.ToList();
        }
    }
    public class mMDPCreationLineCD
    {
        public string LINE_CD { get; set; }



        public List<mMDPCreationLineCD> getListMDPCreationLineCD()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMDPCreationLineCD>("SP_spn251Get_Line_Combobox");
            db.Close();
            return l.ToList();
        }
    }

    public class mSourceType
    {
        public string SOURCE_TYPE { get; set; }
        public string DESCRIPTION { get; set; }

        public List<mSourceType> getListSourceType()
        {
            SortedSet<mSourceType> list = new SortedSet<mSourceType>(new PersonComparer());
            list.Add(new mSourceType { SOURCE_TYPE = "", DESCRIPTION = "" });
            list.Add(new mSourceType { SOURCE_TYPE = "1", DESCRIPTION = "Local Outhouse" });
            list.Add(new mSourceType { SOURCE_TYPE = "2", DESCRIPTION = "Import JSP" });
            list.Add(new mSourceType { SOURCE_TYPE = "3", DESCRIPTION = "Local Inhouse" });
            list.Add(new mSourceType { SOURCE_TYPE = "4", DESCRIPTION = "Import MSP" });
            list.Add(new mSourceType { SOURCE_TYPE = "5", DESCRIPTION = "Steel Local" });
            list.Add(new mSourceType { SOURCE_TYPE = "6", DESCRIPTION = "Steel Import" });
            list.Add(new mSourceType { SOURCE_TYPE = "7", DESCRIPTION = "Steel Cutting Order" });

            return list.ToList();
        }
    }

    public class PersonComparer : IComparer<mSourceType>
    {
        public int Compare(mSourceType x, mSourceType y)
        {
            return x.SOURCE_TYPE.CompareTo(y.SOURCE_TYPE);
        }
    }

}