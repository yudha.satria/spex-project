﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mCaseMaster
    {
        public string CASE_TYPE { get; set; }
        public decimal? CASE_NET_WEIGHT { get; set; }
        public decimal? CASE_LENGTH { get; set; }
        public decimal? CASE_WIDTH { get; set; }
        public decimal? CASE_HEIGHT { get; set; }
        public decimal? MEASUREMENT { get; set; }
        public string DESCRIPTION { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string Text { get; set; }

        //ADD AGI 2017-08-10
        public string CASE_GROUPING { get; set; }

        //public List<mCaseMaster> getList(string pCASE_TYPE, string pDESCRIPTION)
        //MODIF AGI 2017-08-10
        public List<mCaseMaster> getList(string pCASE_TYPE, string pDESCRIPTION,string pCASE_GROUPING)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mCaseMaster>("Case_GetList",
                  new { CASE_TYPE = pCASE_TYPE },
                  new { DESCRIPTION = pDESCRIPTION },
                  //ADD AGI 2017-08-10
                  new { CASE_GROUPING = pCASE_GROUPING }
                  );
            db.Close();

            return l.ToList();
        }

        public mCaseMaster getCase(string pCASE_TYPE)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mCaseMaster>("Case_Get",
                  new { CASE_TYPE = pCASE_TYPE }
                  );
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault() : new mCaseMaster();
        }
        //public string Add(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT, string pDESCRIPTION, string pUSER_ID)
        //MODIF AGI 2017-08-10
        public string Add(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT, string pDESCRIPTION, string pUSER_ID,string pCASE_GROUPING ="")
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("Case_Add",
                  new
                  {
                      CASE_TYPE = pCASE_TYPE,
                      CASE_NET_WEIGHT = pCASE_NET_WEIGHT,
                      CASE_LENGTH = pCASE_LENGTH,
                      CASE_WIDTH = pCASE_WIDTH,
                      CASE_HEIGHT = pCASE_HEIGHT,
                      DESCRIPTION = pDESCRIPTION,
                      USER_ID = pUSER_ID,
                      //ADD AGI 2017-08-10
                      CASE_GROUPING = pCASE_GROUPING
                  }
                  );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }
        //public string Update(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT, string pDESCRIPTION, string pUSER_ID)
        //MODIF AGO 2017-08-10
        public string Update(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT, string pDESCRIPTION, string pUSER_ID,string pCASE_GROUPING)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("Case_Update",
                  new
                  {
                      CASE_TYPE = pCASE_TYPE,
                      CASE_NET_WEIGHT = pCASE_NET_WEIGHT,
                      CASE_LENGTH = pCASE_LENGTH,
                      CASE_WIDTH = pCASE_WIDTH,
                      CASE_HEIGHT = pCASE_HEIGHT,
                      DESCRIPTION = pDESCRIPTION,
                      USER_ID = pUSER_ID,
                      //Add AGI 2017-08-10
                      CASE_GROUPING=pCASE_GROUPING
                  }
                  );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }

        public string Delete(string pCASE_TYPE, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("Case_Delete",
                  new
                  {
                      CASE_TYPE = pCASE_TYPE,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }

        public List<mCaseMaster> getDownloadList(string pCASE_TYPE)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mCaseMaster>("Case_Download",
                  new { CASE_TYPE = pCASE_TYPE }
                  );
            db.Close();

            return l.ToList();
        }

    }
}