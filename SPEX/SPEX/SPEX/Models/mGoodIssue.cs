﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mGoodIssue
    {
        public string INVOICE_NO { get; set; }
        public DateTime? INVOICE_DT { get; set; }
        public DateTime? ETD { get; set; }
        public string GOOD_ISSUE_SALES_DOC { get; set; }
        public string GOOD_ISSUE_STATUS { get; set; }
        public DateTime? GOOD_ISSUE_PROCESS_DT { get; set; }
        public DateTime? GOOD_ISSUE_POSTING_DT { get; set; }
        public DateTime? GOOD_ISSUE_PRICING_DT { get; set; }
        public DateTime? GOOD_ISSUE_BASELINE_DT { get; set; }
        public DateTime? GOOD_ISSUE_DOCUMENT_DT { get; set; }
        public decimal FOB_AMOUNT { get; set; }
        public string PACKING_COMPANY { get; set; }
        public string CANCEL_STATUS { get; set; }


        public string ETD_FROM { get; set; }
        public string ETD_TO { get; set; }
        public string INVOICE_NOS { get; set; }
        public string MODE { get; set; }
        public string TEXT { get; set; }
        public string NOTE { get; set; }

        public List<mGoodIssue> getList(string p_ETD_FROM, string p_ETD_TO, string p_INVOICE_DT_FROM, string p_INVOICE_DT_TO, string p_GOOD_ISSUE_STATUS, string p_INVOICE_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mGoodIssue>("GoodIssue_GetList",
                new { ETD_FROM = p_ETD_FROM },
                new { ETD_TO = p_ETD_TO },
                new { INVOICE_DT_FROM = p_INVOICE_DT_FROM },
                new { INVOICE_DT_TO = p_INVOICE_DT_TO },
                new { GOOD_ISSUE_STATUS = p_GOOD_ISSUE_STATUS },
                new { INVOICE_NO = p_INVOICE_NO }
                );
            db.Close();

            return l.ToList();
        }

        public mGoodIssue getGoodIssue(string p_ETD_FROM, string p_ETD_TO, string p_INVOICE_NOS)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mGoodIssue> result = db.Fetch<mGoodIssue>("GoodIssue_GetPosting",
                new { ETD_FROM = p_ETD_FROM },
                new { ETD_TO = p_ETD_TO },
                new { INVOICE_NOS = p_INVOICE_NOS }
                ).ToList();
            db.Close();

            return result.Count > 0 ? result[0] : new mGoodIssue();
        }

        public string SendPosting(string p_ETD_FROM, string p_ETD_TO, string p_INVOICE_DT_FROM, string p_INVOICE_DT_TO, string p_GOOD_ISSUE_STATUS, string p_INVOICE_NO,string p_INVOICE_NO_PARAM, string p_MODE, string pUserID)
        {
            string result = "error |error on posting data.";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mInvoice>("GoodIssue_Posting",
                    new { ETD_FROM = p_ETD_FROM },
                    new { ETD_TO = p_ETD_TO },
                    new { INVOICE_DT_FROM = p_INVOICE_DT_FROM },
                    new { INVOICE_DT_TO = p_INVOICE_DT_TO },
                    new { GOOD_ISSUE_STATUS = p_GOOD_ISSUE_STATUS },
                    new { INVOICE_NO = p_INVOICE_NO },
                    new { INVOICE_NO_PARAM = p_INVOICE_NO_PARAM},
                    new { MODE = p_MODE },
                    new { USER_ID = pUserID }
                    );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }


        public List<mGoodIssue> getDownload(string p_ETD_FROM, string p_ETD_TO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mGoodIssue>("GoodIssue_Download",
                new { ETD_FROM = p_ETD_FROM },
                new { ETD_TO = p_ETD_TO }
                );
            db.Close();

            return l.ToList();
        }

        public List<mInvoice> getDownloadDetail(string p_ETD_FROM, string p_ETD_TO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mInvoice>("GoodIssue_DownloadDetail",
                new { ETD_FROM = p_ETD_FROM },
                new { ETD_TO = p_ETD_TO }
                );
            db.Close();

            return l.ToList();
        }
    }
}