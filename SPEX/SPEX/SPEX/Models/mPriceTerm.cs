﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPriceTerm
    {
        //Get data
        public string PRICE_TERM_CD { get; set; }
        public string DESCRIPTION_1 { get; set; }
        public string DESCRIPTION_2 { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string DELETION_FLAG { get; set; }

        public string Text { get; set; }

        //Search Data
        public List<mPriceTerm> getListPriceTerm (string p_PRICE_TERM_CD, string p_DESCRIPTION1)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPriceTerm>("PriceTerm_Search",
                  new { PRICE_TERM_CD = p_PRICE_TERM_CD },
                  new { DESCRIPTION_1 = p_DESCRIPTION1 }
                  );
            db.Close();
            return l.ToList();
        }

        //Gridlookup OrderType
        public List<mPriceTerm> getComboPriceTerm()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPriceTerm>("PriceTerm_Select");
            db.Close();
            return l.ToList();
        }

        //Save Data
        public string SaveData(string p_PRICE_TERM_CD, string p_DESCRIPTION1, string p_DESCRIPTION2, string p_CREATED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPriceTerm>("PriceTerm_Save", new { 
                    PRICE_TERM_CD = p_PRICE_TERM_CD, 
                    DESCRIPTION_1 = p_DESCRIPTION1 ,
                    DESCRIPTION_2 = p_DESCRIPTION2,
                    CREATED_BY = p_CREATED_BY,
                    MSG_TEXT = ""
                    });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        //Update Data
        public string UpdateData(string p_PRICE_TERM_CD, string p_DESCRIPTION1, string p_DESCRIPTION2, string p_DELETION_FLAG, string p_CHANGED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPriceTerm>("PriceTerm_Update", new
                {
                    PRICE_TERM_CD = p_PRICE_TERM_CD,
                    DESCRIPTION_1 = p_DESCRIPTION1,
                    DESCRIPTION_2 = p_DESCRIPTION2,
                    CHANGED_BY = p_CHANGED_BY,
                    DELETION_FLAG = p_DELETION_FLAG,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        //Delete Data
        public string DeleteData(string p_PRICE_TERM_CD, string p_CHANGED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                if (p_PRICE_TERM_CD.Contains(";"))
                {
                    string[] PriceTermList = p_PRICE_TERM_CD.Split(';');
                    for (int i = 0; i < PriceTermList.Length; i++)
                    {
                        var l = db.Fetch<mPriceTerm>("PriceTerm_Delete", new { 
                            PRICE_TERM_CD = PriceTermList[i],
                            CHANGED_BY = p_CHANGED_BY,
                            MSG_TEXT = ""
                        });
                        foreach (var message in l)
                        { result = message.Text; }
                    }
                }
                else
                {
                    var l = db.Fetch<mPriceTerm>("PriceTerm_Delete", new { 
                        PRICE_TERM_CD = p_PRICE_TERM_CD,
                        CHANGED_BY = p_CHANGED_BY,
                        MSG_TEXT = ""
                    });
                    foreach (var message in l)
                    { result = message.Text; }
                }
                db.Close();
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

    }
}