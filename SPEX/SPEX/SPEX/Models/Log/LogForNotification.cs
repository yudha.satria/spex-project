﻿/**
 * Created By   : ark.yudha
 * Created Dt   : 12.02.2014
 * **/

using System;

namespace SPEX.Models.Log
{
    public class LogForNotification
    {
        public DateTime? CREATED_DATE {get;set;}
        public string LOCATION {get;set;}
        public string MSG_ID {get;set;}
        public string MSG_TYPE {get;set;}
        public string MESSAGE {get;set;}
    }
}