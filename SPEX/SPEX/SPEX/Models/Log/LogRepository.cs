﻿/**
 * Created By   : ark.yudha
 * Created Dt   : 12.02.2014
 * **/

using System.Collections.Generic;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models.Log
{
    public class LogRepository : Ilog
    {
        public long putLog(string p_messages, string p_user, string p_location = "", long p_pid = 0,
            string p_msg_id = "INF", string p_msg_type = "INF", string p_module_id = "",
            string p_function_id = "", int p_process_status = 0)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            
            dynamic args = new
            {
                p_messages=p_messages,
	            p_user=p_user,
	            p_location=p_location, 	
	            p_pid=p_pid,
	            p_msg_id=p_msg_id, 
	            p_msg_type=p_msg_type,
	            p_module_id=p_module_id, 
	            p_function_id=p_function_id,
	            p_process_status=p_process_status
            };

            long r = db.ExecuteScalar<long>("PutLog",args);
            db.Close();
            return r;
        }

        public IList<LogForNotification> getLogForNotification(long p_Process_ID,string p_Mail_Group_CD) { 
            IDBContext db = DatabaseManager.Instance.GetContext();
            dynamic args = new
            {
                p_Process_ID = p_Process_ID,
                p_Mail_Group_CD = p_Mail_Group_CD
            };
            IList<LogForNotification> l = db.Fetch<LogForNotification>("getLogForNotification", args);
            db.Close();
            return l;
        }

        public IList<LogH> getById(long Process_ID)
        {
            return null;
        }
    }
}