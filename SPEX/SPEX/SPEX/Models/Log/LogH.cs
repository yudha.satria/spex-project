﻿using System;

namespace SPEX.Models.Log
{
    public class LogH
    {
        public long PROCESS_ID { get; set; }
        public string FUNCTION_ID { get; set; }
        public string MODULE_ID { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public Int32 PROCESS_STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DATE { get; set; }
    }
}