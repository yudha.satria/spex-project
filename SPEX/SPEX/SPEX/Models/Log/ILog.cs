﻿/**
 * Created By   : ark.yudha
 * Created Dt   : 12.02.2014
 * **/

using System.Collections.Generic;

namespace SPEX.Models.Log
{
    public interface Ilog
    {
        long putLog(string p_messages, string p_user, string p_location = "", long p_pid = 0,
            string p_msg_id = "INF", string p_msg_type = "INF", string p_module_id = "",
            string p_function_id = "", int p_process_status = 0);

        IList<LogForNotification> getLogForNotification(long p_Process_ID, string p_Mail_Group_CD);
        
        IList<LogH> getById(long Process_ID);
    }
}