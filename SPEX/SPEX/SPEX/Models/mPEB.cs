﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPEB
    {
        //PEBPDTX
        public string PEB_SEQ { get; set; }
        public string HS_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public double TOTAL_QTY { get; set; }
        public double PRICE { get; set; }

        //PEBPHD1X
        public string INVOICE_NO { get; set; }
        public DateTime? INVOICE_DT { get; set; }
        public DateTime? ETD { get; set; }
        public string VESSEL { get; set; }

        //PEBPHD2X
        public double FOB_AMOUNT { get; set; }
        public double TOTAL_CASE { get; set; }
        public double TOTAL_GROSS_WEIGHT { get; set; }
        public double TOTAL_NET_WEIGHT { get; set; }
        //total qty

        //PEBPHD3X
        public string CASE_NO { get; set; }

        // system master
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }

        ////
        public string Text { get; set; }
        public string REGISTER_NO { get; set; }

        public string PROCESS_STATUS { get; set; }
        public string SEND_STATUS { get; set; }

        //segment a
        public DateTime? REGISTER_DT { get; set; }
        public string VOYAGE_NO { get; set; }
        public string DEST_PORT_CD { get; set; }
        public string IMPORTER_CD { get; set; }
        public DateTime? INSPECTION_DT { get; set; }
        public string INV_CONTAINER_COUNT { get; set; }

        //container data
        public string CONTAINER_NO { get; set; }
        public string CONTAINER_SEGEL { get; set; }
        public string CONTAINER_SIZE { get; set; }
        public string SEQ_NO { get; set; }

        public string BUYER_NAME { get; set; }
        public string BUYER_ADDRESS { get; set; }
        public string BUYER_COUNTRY { get; set; }
        public string TRANSPORT_CD { get; set; }
        public string OCEAN_VESSEL_CD { get; set; }
        public string VESSEL_FLAG { get; set; }
        public string PORT_OF_LOADING { get; set; }
        public string TRANSIT_PORT { get; set; }
        public string PROVINCE_GOODS { get; set; }
        public string CURRENCY_CD { get; set; }
        public double FREIGHT_AMOUNT { get; set; }
        public double INSURANCE_AMOUNT { get; set; }
        public double INVOICE_AMOUNT { get; set; }
        public string PACKAGE_MERK { get; set; }
        public double GROSS_WEIGHT { get; set; }
        public double NET_WEIGHT { get; set; }
        public double MEASUREMENT { get; set; }
        public string TRANS_MODE_OFC_CD { get; set; }
        public string LOADING_OFFICE_CD { get; set; }
        public string HANGAR_CD { get; set; }
        public string INSPECTION_OFFICE_CD { get; set; }
        public string INV_NON_CONTAINER_COUNT { get; set; }
        public string INV_PART_COUNT { get; set; }
        public string EXPORT_CATEGORY_CD { get; set; }
        public string PABEAN_OFFICE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string RECEIVER_ADDRESS { get; set; }
        public string RECEIVER_COUNTRY { get; set; }
        public string DESTINATION_PORT { get; set; }
        public string PLB_WAREHOUSE { get; set; }
        public string PAYMENT_METHOD { get; set; }
        public string MAKLON_AMOUNT { get; set; }
        public string OTHER_TAX_RECEIPT { get; set; }
        public string VESSEL_NAME2 { get; set; }
        public string VOYAGE_NO2 { get; set; }
        public string VESSEL_FLAG2 { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string PACKING_COMPANY { get; set; }

        public List<mPEB> getListPEB(string pRegisterNo, string pRegisterDateFrom, string pRegisterDateTo, string pInvoiceNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("PEB_Header_Search",
                  new { REGISTER_NO = pRegisterNo },
                  new { REGISTER_DT_FROM = pRegisterDateFrom },
                  new { REGISTER_DT_TO = pRegisterDateTo },
                  new { INVOICE_NO = pInvoiceNo }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mPEB> getListPEBDetail(string pPEBSeq, string pINVOICE_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("PEB_Detail_Search",
                  new { PEB_SEQ = pPEBSeq },
                  new { INVOICE_NO = pINVOICE_NO }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mPEB> getListPEBHeader3(string pPEBSeq, string pINVOICE_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("PEB_Header3_Search",
                  new { PEB_SEQ = pPEBSeq },
                  new { INVOICE_NO = pINVOICE_NO }
                  );
            db.Close();

            return l.ToList();
        }
        
        public List<mPEB> getListOfDownloadData_2(string pPEBSeq)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("PEB_Download_Data_2",
                  new { PEB_SEQ = pPEBSeq }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mPEB> getListOfSystemMasterData(string pSystemType, string pSystemCD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("PEB_Get_System_Master_List",
               new
               {
                   SYSTEM_TYPE = pSystemType,
                   SYSTEM_CD = pSystemCD
               }
                  );
            db.Close();

            return l.ToList();
        }

        public mPEB getMasterData(string pSystemType, string pSystemCd)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("PEB_Get_System_Master",
                  new
                  {
                      SYSTEM_TYPE = pSystemType,
                      SYSTEM_CD = pSystemCd
                  }
                  );
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault() : new mPEB();
        }

        public string SaveSegmentData(string pPEB_SEQ, string pINVOICE_NO, string pREGISTER_NO,
            string pREGISTER_DT, string pVOYAGE_NO, string pDEST_PORT_CD, string pIMPORTER_CD,
            string pINSPECTION_DT, string pINV_CONTAINER_COUNT, string pCONTAINER, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mPEB>("PEB_Save_Segment",
                  new
                  {
                      REGISTER_NO = pREGISTER_NO,
                      REGISTER_DT = pREGISTER_DT,
                      PEB_SEQ = pPEB_SEQ,
                      INVOICE_NO = pINVOICE_NO,
                      VOYAGE_NO = pVOYAGE_NO,
                      DEST_PORT_CD = pDEST_PORT_CD,
                      IMPORTER_CD = pIMPORTER_CD,
                      INSPECTION_DT = pINSPECTION_DT,
                      INV_CONTAINER_COUNT = pINV_CONTAINER_COUNT,
                      CONTAINER = pCONTAINER,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }

        public string UpdateSegmentData(string pPEB_SEQ, string pINVOICE_NO, string pREGISTER_NO,
            string pREGISTER_DT, string pVOYAGE_NO, string pDEST_PORT_CD, string pIMPORTER_CD,
            string pINSPECTION_DT, string pINV_CONTAINER_COUNT, string pCONTAINER, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mPEB>("PEB_Update_Segment",
                  new
                  {
                      REGISTER_NO = pREGISTER_NO,
                      REGISTER_DT = pREGISTER_DT,
                      PEB_SEQ = pPEB_SEQ,
                      INVOICE_NO = pINVOICE_NO,
                      VOYAGE_NO = pVOYAGE_NO,
                      DEST_PORT_CD = pDEST_PORT_CD,
                      IMPORTER_CD = pIMPORTER_CD,
                      INSPECTION_DT = pINSPECTION_DT,
                      INV_CONTAINER_COUNT = pINV_CONTAINER_COUNT,
                      CONTAINER = pCONTAINER,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }

        //public string CheckSegmentData(string pPEB_SEQ, string pINVOICE_NO, string pREGISTER_NO,
        //    string pREGISTER_DT, string pVOYAGE_NO, string pDEST_PORT_CD, string pIMPORTER_CD,
        //    string pINSPECTION_DT, string pINV_CONTAINER_COUNT, string pCONTAINER, string pUSER_ID)
        //{
        //    string result = "FAILED";
        //    IDBContext db = DatabaseManager.Instance.GetContext();

        //    var l = db.Fetch<mPEB>("PEB_Check_Segment",
        //          new
        //          {
        //              REGISTER_NO = pREGISTER_NO,
        //              REGISTER_DT = pREGISTER_DT,
        //              PEB_SEQ = pPEB_SEQ,
        //              INVOICE_NO = pINVOICE_NO,
        //              VOYAGE_NO = pVOYAGE_NO,
        //              DEST_PORT_CD = pDEST_PORT_CD,
        //              IMPORTER_CD = pIMPORTER_CD,
        //              INSPECTION_DT = pINSPECTION_DT,
        //              INV_CONTAINER_COUNT = pINV_CONTAINER_COUNT,
        //              CONTAINER = pCONTAINER,
        //              USER_ID = pUSER_ID
        //          }
        //          );
        //    db.Close();

        //    foreach (var message in l.ToList())
        //    {
        //        result = message.Text;
        //    }

        //    return result;
        //}

        //public string SendValidate(string PROCESS_ID, string REGISTER_NO)
        //{
        //    IDBContext db = DatabaseManager.Instance.GetContext();
        //    string result = "Success ";
        //    var l = db.Fetch<mPEB>("PEB_Send_Validation", new { PROCESS_ID = PROCESS_ID, REGISTER_NO = REGISTER_NO });
        //    db.Close();

        //    foreach (var message in l.ToList())
        //    {
        //        result = message.Text;
        //    }
        //    return result;
        //}

        public string SendData(string pREGISTER_NO, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "Success ";
            var l = db.Fetch<mPEB>("PEB_Send_Data",
                new
                {
                    REGISTER_NO = pREGISTER_NO,
                    USER_ID = pUSER_ID
                });
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }
            return result;
        }

        public string CheckProcess(string pPEB_SEQ, string pINVOICE_NO)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("PEB_Check_Process",
                  new { PEB_SEQ = pPEB_SEQ },
                  new { INVOICE_NO = pINVOICE_NO }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }

            return result;
        }

        public List<mPEB> getListPEBContainer(string pPEB_SEQ, string pINVOICE_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("PEB_Get_Segment_Container",
                  new { PEB_SEQ = pPEB_SEQ },
                  new { INVOICE_NO = pINVOICE_NO }
                  );
            db.Close();

            return l.ToList();
        }

        public mPEB getPEBSegment(string pPEB_SEQ, string pINVOICE_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            mPEB returnValue = new mPEB();
            var l = db.Fetch<mPEB>("PEB_Get_Segment",
                new { PEB_SEQ = pPEB_SEQ },
                new { INVOICE_NO = pINVOICE_NO }
                );
            foreach (var message in l.ToList())
            {
                returnValue = (mPEB)message;
            }
            db.Close();

            return returnValue;
        }


        public string DeleteData(string pREGISTER_NO)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEB>("PEB_Delete_Data",
                  new { REGISTER_NO = pREGISTER_NO }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }

            return result;
        }

        //ADD AGI 2017-03-03
        public string DeleteDataByUserId(string pREGISTER_NO,string pUSERiD)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPEB>("PEB_Delete_DataByUser",
                  new { REGISTER_NO = pREGISTER_NO,
                        USER_ID=pUSERiD
                    }
                  );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.Text;
            }

            return result;
        }
    }
}
