﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mNQC
    {
        public string PROD_MONTH { get; set;}
        public string PARENT_PART_NO {get; set;}
        public string PARENT_PLANT_CD {get; set;}
        public string PART_NO {get; set;}
        public string PROD_PURPOSE {get; set;}
        public string SOURCE_TYPE {get; set;}
        public string PLANT_CD {get; set;}
        public string SLOC_CD {get; set;}
        public string LINE_CD {get; set;}
        public string PART_COLOR_SFX {get; set;}
        public string DOCK_CD {get; set;}
        public decimal QTY_N_1 {get; set;}
        public decimal QTY_N_2 {get; set;}
        public decimal QTY_N_3 {get; set;}
        public decimal QTY_N_4 {get; set;}
        public decimal QTY_N_5 {get; set;}
        public decimal QTY_N_6 {get; set;}
        public decimal QTY_N_7 {get; set;}
        public decimal QTY_N_8 {get; set;}
        public decimal QTY_N_9 {get; set;}
        public decimal QTY_N_10 {get; set;}
        public decimal QTY_N_11 {get; set;}
        public decimal QTY_N_12 {get; set;}
        public decimal QTY_N_13 {get; set;}
        public decimal QTY_N_14 {get; set;}
        public decimal QTY_N_15 {get; set;}
        public decimal QTY_N_16 {get; set;}
        public decimal QTY_N_17 {get; set;}
        public decimal QTY_N_18 {get; set;}
        public decimal QTY_N_19 {get; set;}
        public decimal QTY_N_20 {get; set;}
        public decimal QTY_N_21 {get; set;}
        public decimal QTY_N_22 {get; set;}
        public decimal QTY_N_23 {get; set;}
        public decimal QTY_N_24 {get; set;}
        public decimal QTY_N_25 {get; set;}
        public decimal QTY_N_26 {get; set;}
        public decimal QTY_N_27 {get; set;}
        public decimal QTY_N_28 {get; set;}
        public decimal QTY_N_29 {get; set;}
        public decimal QTY_N_30 {get; set;}
        public decimal QTY_N_31 {get; set;}
        public string CREATED_BY {get; set;}
        public DateTime CREATED_DT {get; set;}
        public string CHANGED_BY {get; set;}
        public DateTime CHANGED_DT {get; set;}
        public string ORDER_TYPE { get; set; }
        public string Text { get; set; }

        public List<mNQC> SPN231NQCRegulerSearch(string ps_ProductionMonth, string ps_ParentPartNo, string ps_PartNo, string ps_SourceType)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mNQC>("NQC_Reguler_Search", new
            {
                                            PROD_MONTH = ps_ProductionMonth,
                                            PARENT_PART_NO = ps_ParentPartNo,
                                            PART_NO = ps_PartNo,
                                            SOURCE_TYPE = ps_SourceType
                                        });

            if (ps_PartNo == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }

        public List<mNQC> SPN231NQCEmergencySearch(string ps_ProductionMonth, string ps_ParentPartNo, string ps_PartNo, string ps_SourceType)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mNQC>("NQC_Emergency_Search", new
            {
                PROD_MONTH = ps_ProductionMonth,
                PARENT_PART_NO = ps_ParentPartNo,
                PART_NO = ps_PartNo,
                SOURCE_TYPE = ps_SourceType
            });

            if (ps_PartNo == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }

        public List<mNQC> SPN231NQCDownload(string ps_ProductionMonth, string ps_ParentPartNo, string ps_PartNo, string ps_SourceType)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mNQC>("NQC_Download", new
            {
                PROD_MONTH = ps_ProductionMonth,
                PARENT_PART_NO = ps_ParentPartNo,
                PART_NO = ps_PartNo,
                SOURCE_TYPE = ps_SourceType
            });

            if (ps_PartNo == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }

        public string SPN232CalculateProcess(string pc_ProdMonth, string pc_ParentPartNo, string ProcessId)
        {
            string result = string.Empty;
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mSourceList>("NQC_Calculate_Process",
                    new { PROD_MONTH = pc_ProdMonth },
                    new { PARENT_PART_NO = pc_ParentPartNo },
                    new { PROCESS_ID = ProcessId },
                    new { CREATED_BY = "Sys" },
                    new { MSG_TEXT = "" }
                );
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "error | " + Convert.ToString(err.Message);
            }

            return result;
        }

    }
}
