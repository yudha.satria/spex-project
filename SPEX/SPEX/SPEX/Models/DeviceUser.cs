﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class DeviceUser
    {
        public string USER_LOGIN { get; set; }
        public string USER_PASWD { get; set; }
        public string USER_NAME { get; set; }
        public string PACKING_COMPANY { get; set; }
        public string COMPANY_PLANT_CD { get; set; }
        public string PLANT_LINE_CD { get; set; }
        public string TRANSPORT_CD { get; set; }
        public string DEVICE_NW_IDENT { get; set; }
        public string LOGIN_STS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        
        public string Text { get; set; }

        public DeviceUser doLogin(string pUSER_LOGIN,string pPassword, string pIp)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<DeviceUser>("DeviceUser_Get",
                  new { USER_LOGIN = pUSER_LOGIN,
                        IP = pIp
                        }
                  );
            db.Close();

            DeviceUser o;
            if (l.Count > 0)
            {
                o = l.ToList().FirstOrDefault();
                if (o.USER_PASWD == pPassword)
                {
                    o.Text = "Success";
                }
                else
                {
                    o.Text = "Error|Wrong Password";
                }
            }
            else
            {
                o = new DeviceUser();
                o.Text = "Error|User Not Found";
            }


            return o;
        }

        public DeviceUser Test(string pUSER_LOGIN, string pPassword, string pIp)
        {
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<DeviceUser>("DeviceUser_Get",
                      new
                      {
                          USER_LOGIN = pUSER_LOGIN,
                          IP = pIp
                      }
                      );
                db.Close();
                return l.Count > 0 ? l.ToList().FirstOrDefault() : new DeviceUser();
            }
            catch (Exception exc)
            {
                DeviceUser a = new DeviceUser();
                a.USER_NAME = exc.Message;
                return a;
            }
        }



    }
}