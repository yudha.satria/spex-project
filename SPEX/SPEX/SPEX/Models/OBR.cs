﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class OBR
    {
        public string OBR_NO { get; set; }
        public DateTime? OBR_DT { get; set; }
        public DateTime? TMAP_ORDER_DT { get; set; }
        public string TMAP_ORDER_NO { get; set; }
        public string TMAP_ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public decimal? ORDER_QTY { get; set; }
        public decimal? CANCEL_QTY { get; set; }
        public decimal? ACCEPT_QTY { get; set; }
        public string OBR_STATUS { get; set; }
        public string PROCESS_BY { get; set; }
        public DateTime? PROCESS_DT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string OBR_STATUS_NAME { get; set; }

        //ADD AGI 2017-11-23
        public string OBR_REASON { get; set; }
        public decimal MAX_DAD { get; set; }
        public decimal MAX_MAD { get; set; }

        public string Text { get; set; }

        //add fid.mario 2018-02-22
        public string BUYER_PD { get; set; }

        public string ObrDtTxt
        {
            get
            {
                return GetTxtDate(OBR_DT);
            }
        }

        public string TmapOrderDtTxt
        {
            get
            {
                return GetTxtDate(TMAP_ORDER_DT);
            }
        }

        private string GetTxtDate(DateTime? d)
        {
            string r = "";
            if (d.HasValue)
            {
                r = d.Value.Day.ToString().PadLeft(2, '0') + "." + d.Value.Month.ToString().PadLeft(2, '0') + "." + d.Value.Year.ToString();
            }
            return r;
        }

        public List<OBR> getList(string pObrNo, string pItemNo, string pStartOrderDt, string pEndOrderDt, string pOrderNo, string pPartNo, string pStatus)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<OBR>("OBR_GetList",
                  new { OBRNO = pObrNo }
                  ,new { ORDERNO = pOrderNo }
                  , new { ITEMNO = pItemNo }
                  , new { PARTNO = pPartNo }
                  , new { STATUS = pStatus }
                  , new { STARTORDERDT = pStartOrderDt }
                  , new { ENDORDERDT = pEndOrderDt }
                  );
            db.Close();

            return l.ToList();
        }

        public OBR getOBR(string pOBRNO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<OBR>("OBR_Get",
                  new { OBRNO = pOBRNO }
                  );
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault() : new OBR();
        }

        public List<mSystemMaster> GetOBRStatusList()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();

            datas = (new mSystemMaster()).GetSystemValueList("OBR_INQUIRY_STATUS", "", "2");
            return datas;
        }

        public string Add(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT, string pDESCRIPTION, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("Case_Add",
                  new
                  {
                      CASE_TYPE = pCASE_TYPE,
                      CASE_NET_WEIGHT = pCASE_NET_WEIGHT,
                      CASE_LENGTH = pCASE_LENGTH,
                      CASE_WIDTH = pCASE_WIDTH,
                      CASE_HEIGHT = pCASE_HEIGHT,
                      DESCRIPTION = pDESCRIPTION,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }

        public string Update(string pOBR_NO, decimal? pCANCEL_QTY, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("OBR_Update",
                  new
                  {
                      OBR_NO = pOBR_NO,
                      CANCEL_QTY = pCANCEL_QTY,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }


        public string UpdateStatus(string pOBR_NOS, string pStatusID, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("OBR_Update_Status",
                  new
                  {
                      OBR_NOS = pOBR_NOS,
                      STATUS_ID = pStatusID,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }


    }
}