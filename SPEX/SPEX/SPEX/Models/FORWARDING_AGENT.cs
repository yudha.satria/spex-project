﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class FORWARDING_AGENT
    {
        public string FORWARD_AGENT_CD { get; set; }
        public string FORWARD_AGENT_NAME { get; set; }
        public string PIC_NAME { get; set; }
        public string CONTACT_NO { get; set; }
        public string DESCR1 { get; set; }
        public string DESCR2 { get; set; }
        public string DESCR3 { get; set; }
        public string MESSRS_1 { get; set; }
        public string MESSRS_2 { get; set; }
        public string MESSRS_3 { get; set; }
        public string TYPE { get; set; }
        //{
        //    get { return TYPE == null ? "AIR" : "SEA"; }
        //    set { TYPE = value; }
        //}
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string Text { get; set; }
        public bool IsNewData
        {
            get
            {
                if (FORWARD_AGENT_CD == null)
                    return true;
                else if (FORWARD_AGENT_CD == "")
                    return true;
                else
                    return false;
            }
        }

        public List<FORWARDING_AGENT> getFORWARDING_AGENTList(FORWARDING_AGENT fwd)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<FORWARDING_AGENT> l = db.Fetch<FORWARDING_AGENT>("FORWARDING_AGENT_Search",
                  new { FORWARD_AGENT_CD = fwd.FORWARD_AGENT_CD },
                  new { FORWARD_AGENT_NAME = fwd.FORWARD_AGENT_NAME },
                  new { PIC_NAME = fwd.PIC_NAME }
                  ).ToList();
            db.Close();
            return l;
        }

        public FORWARDING_AGENT getFORWARDING_AGENT(string pFORWARD_AGENT_CD, string pTYPE)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<FORWARDING_AGENT> l = db.Fetch<FORWARDING_AGENT>("FORWARDING_AGENT_Get",
                  new { FORWARD_AGENT_CD = pFORWARD_AGENT_CD },
                  new { TYPE = pTYPE }
                  ).ToList();
            db.Close();

            return l.Count > 0 ? l.FirstOrDefault() : (new FORWARDING_AGENT() { TYPE = "AIR" });
        }

        //Delete Data
        public string DeleteData(string PARAMS, string USER_ID)
        {
            string result = "DELETE DATA SUCCES";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<FORWARDING_AGENT>("FORWARDING_AGENT_Delete", new
                {
                    PARAMS = PARAMS,
                    USER_ID = USER_ID
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public string SaveData(FORWARDING_AGENT fwd)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<FORWARDING_AGENT>("FORWARDING_AGENT_Save", new
                {
                    FORWARD_AGENT_CD = fwd.FORWARD_AGENT_CD,
                    FORWARD_AGENT_NAME = fwd.FORWARD_AGENT_NAME,
                    PIC_NAME = fwd.PIC_NAME,
                    CONTACT_NO = fwd.CONTACT_NO,
                    DESCR1 = fwd.DESCR1,
                    DESCR2 = fwd.DESCR2,
                    DESCR3 = fwd.DESCR3,
                    MESSRS_1 = fwd.MESSRS_1,
                    MESSRS_2 = fwd.MESSRS_2,
                    MESSRS_3 = fwd.MESSRS_3,
                    TYPE = fwd.TYPE,
                    CREATED_BY = fwd.CREATED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }


        //Update Data
        public string UpdateData(FORWARDING_AGENT fwd)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<FORWARDING_AGENT>("FORWARDING_AGENT_Update", new
                {
                    FORWARD_AGENT_CD = fwd.FORWARD_AGENT_CD,
                    FORWARD_AGENT_NAME = fwd.FORWARD_AGENT_NAME,
                    PIC_NAME = fwd.PIC_NAME,
                    CONTACT_NO = fwd.CONTACT_NO,
                    DESCR1 = fwd.DESCR1,
                    DESCR2 = fwd.DESCR2,
                    DESCR3 = fwd.DESCR3,
                    MESSRS_1 = fwd.MESSRS_1,
                    MESSRS_2 = fwd.MESSRS_2,
                    MESSRS_3 = fwd.MESSRS_3,
                    TYPE = fwd.TYPE,
                    CHANGED_BY = fwd.CHANGED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }


    }
}
