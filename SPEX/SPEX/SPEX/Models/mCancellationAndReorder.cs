﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mCancellationAndReorder
    { 
        public string TMAP_ORDER_NO { get; set; }
        public string TMAP_PART_NO { get; set; }
        public string TMAP_ITEM_NO { get; set; }
        public string PART_NAME { get; set; }
        public string MANIFEST_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string TMAP_ORDER_QTY { get; set; }
        public string TMMIN_ORDER_QTY { get; set; }
        public string RECEIVE_QTY { get; set; }
        public string REMAIN_RECEIVE_QTY { get; set; }
        public string SUM_ROC_QTY { get; set; }
        public string CANCEL_DATE { get; set; }
        public string SUM_ROC_STATUS { get; set; }
        public string STATUS { get; set; }
        public string CANCEL_BY { get; set; }
        public string KANBAN_ID { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }

        /*
        public string PART_TYPE { get; set; }
        public string TMAP_ORDER_DATE { get; set; }
        public string SEND_DATE { get; set; }
        public string RECEIVE_DT { get; set; }
        public string ORDER_TYPE { get; set; }
        public string RECEIVED_STATUS { get; set; }
        public string TMAP_PART_NO { get; set; }
        public string TMAP_ITEM_NO { get; set; }
        */

        public class mOrderType
        {
            public string BUYER_ORDER_TYPE { get; set; }
            public string ORDER_TYPE_DESCRIPTION { get; set; }

        }



        public List<mCancellationAndReorder> getListCancellationAndReorder(string p_TMAP_ORDER_NO, string p_KANBAN_ID, string p_ORDER_TYPE,
                                                                           string p_PART_NO, string p_SUPPLIER, string p_STATUS, string p_MANIFEST_NO,
                                                                           string p_SUB_SUPPLIER, string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO,
                                                                           string p_CANCEL_DT, string p_CANCEL_DT_TO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mCancellationAndReorder>("CancellationAndReorder_Search",
                  new { TMAP_ORDER_NO = p_TMAP_ORDER_NO },
                  new { KANBAN_ID = p_KANBAN_ID },
                  new { ORDER_TYPE = p_ORDER_TYPE },
                  new { PART_NO = p_PART_NO },
                  new { SUPPLIER = p_SUPPLIER },
                  new { STATUS = p_STATUS },
                  new { MANIFEST_NO = p_MANIFEST_NO },
                  new { SUB_SUPPLIER = p_SUB_SUPPLIER },
                  new { TMAP_ORDER_DT = p_TMAP_ORDER_DT },
                  new { TMAP_ORDER_DT_TO = p_TMAP_ORDER_DT_TO },
                  new { CANCEL_DT = p_CANCEL_DT },
                  new { CANCEL_DT_TO = p_CANCEL_DT_TO }
                  );
            db.Close();
            return l.ToList();
        }

        public List<mCancellationAndReorder> getListCancellationAndReorderDownloadDetail(string p_TMAP_ORDER_NO, string p_KANBAN_ID, string p_ORDER_TYPE,
                                                                                           string p_PART_NO, string p_SUPPLIER, string p_STATUS, string p_MANIFEST_NO,
                                                                                           string p_SUB_SUPPLIER, string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO,
                                                                                           string p_CANCEL_DT, string p_CANCEL_DT_TO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mCancellationAndReorder>("CancellationAndReorder_DownloadDetail",
                  new { TMAP_ORDER_NO = p_TMAP_ORDER_NO },
                  new { KANBAN_ID = p_KANBAN_ID },
                  new { ORDER_TYPE = p_ORDER_TYPE },
                  new { PART_NO = p_PART_NO },
                  new { SUPPLIER = p_SUPPLIER },
                  new { STATUS = p_STATUS },
                  new { MANIFEST_NO = p_MANIFEST_NO },
                  new { SUB_SUPPLIER = p_SUB_SUPPLIER },
                  new { TMAP_ORDER_DT = p_TMAP_ORDER_DT },
                  new { TMAP_ORDER_DT_TO = p_TMAP_ORDER_DT_TO },
                  new { CANCEL_DT = p_CANCEL_DT },
                  new { CANCEL_DT_TO = p_CANCEL_DT_TO }
                  );
            db.Close();
            return l.ToList();
        }

    

        /*
        public List<mCancellationAndReorder> getListCancellationAndReorderDownloadDetail(string p_TMAP_ORDER_DT, string p_TMAP_ORDER_DT_TO, string p_TMAP_ORDER_NO,
                                                                         string p_ITEM_NO, string p_PART_NO, string p_ORDER_TYPE, string p_RECEIVE_DT,
                                                                         string p_RECEIVE_DT_TO, string p_DELIVERY_PLAN_DT, string p_DELIVERY_PLAN_DT_TO,
                                                                         string p_SEND_DT, string p_SEND_DT_TO, string p_SEND_TO_TAM, string p_PACKING_COMPANY,
                                                                         string p_PART_TYPE, string p_SUPPLIER, string p_SUB_SUPPLIER, string p_OBR_STATUS, string p_VALIDATION_FLAG)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mCancellationAndReorder>("CancellationAndReorder_DownloadDetail", 
                  new { TMAP_ORDER_DT = p_TMAP_ORDER_DT },
                  new { TMAP_ORDER_DT_TO = p_TMAP_ORDER_DT_TO },
                  new { TMAP_ORDER_NO = p_TMAP_ORDER_NO },
                  new { PART_NO = p_PART_NO },
                  new { ITEM_NO = p_ITEM_NO },
                  new { ORDER_TYPE = p_ORDER_TYPE },
                  new { VALIDATION_FLAG = p_VALIDATION_FLAG },
                  new { RECEIVE_DT = p_RECEIVE_DT },
                  new { RECEIVE_DT_TO = p_RECEIVE_DT_TO },
                  new { DELIVERY_PLAN_DT = p_DELIVERY_PLAN_DT },
                  new { DELIVERY_PLAN_DT_TO = p_DELIVERY_PLAN_DT_TO },
                  new { SEND_DT = p_SEND_DT },
                  new { SEND_DT_TO = p_SEND_DT_TO },
                  new { SEND_TO_TAM = p_SEND_TO_TAM },
                  new { PACKING_COMPANY = p_PACKING_COMPANY },
                  new { PART_TYPE = p_PART_TYPE },
                  new { SUPPLIER = p_SUPPLIER },
                  new { SUB_SUPPLIER = p_SUB_SUPPLIER },
                  new { OBR_STATUS = p_OBR_STATUS }
                  );
            db.Close();
            return l.ToList();
        }

        */
        #region ComboBox
         
       
        public List<mSystemMaster> GetComboBoxSupplier()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetComboBoxSupplier").ToList();
            db.Close();
            return datas;
        }
        public List<mSystemMaster> GetComboBoxSubSupplier()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetComboBoxSubSupplier").ToList();
            db.Close();
            return datas;
        } 
  
        public List<mOrderType> getListOrderType()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderType>("OrderType_Select");
            db.Close();
            return l.ToList();
        }

        #endregion


        #region upload
        #region Cancel Order
        public String DeleteTempData()
        {
            String result = "SUCCESS";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.Execute("CancellationAndReorder/DeleteDataTemporary");
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public String BackgroundProsessUploadSave(String moduleID, String functionID, long PID, String userID)
        {
            String result = "SUCCESS";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MODULE_ID = moduleID,
                    FUNCTION_ID = functionID,
                    PID = PID,
                    USER_ID = userID
                };
                db.Execute("CancellationAndReorder/BackgroundProcessUpload", args);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                db.Close();
            }
            return result;
        }
        #endregion

        #region Reorder
        public String DeleteTempDataReorder()
        {
            String result = "SUCCESS";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.Execute("CancellationAndReorder/DeleteDataTemporaryReorder");
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public String BackgroundProsessUploadSaveReorder(String moduleID, String functionID, long PID, String userID)
        {
            String result = "SUCCESS";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MODULE_ID = moduleID,
                    FUNCTION_ID = functionID,
                    PID = PID,
                    USER_ID = userID
                };
                db.Execute("CancellationAndReorder/BackgroundProcessUploadReorder", args);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                db.Close();
            }
            return result;
        }
        #endregion
        
        #endregion
    }
}