﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform; 

namespace SPEX.Models.MailNotification
{
    public class MailNotificationRepository:IMailNotification
    {
        public IList<MailAddressForNotification> getMailAddressForNotification(string p_Mail_Group_CD, string p_CC_Status) {
            IList<MailAddressForNotification> l = null;
            IDBContext db = DatabaseManager.Instance.GetContext();

            dynamic args = new
            {
                p_Mail_Group_CD = p_Mail_Group_CD,
                p_CC_Status = p_CC_Status
            };

            l = db.Fetch<MailAddressForNotification>("getMailAddressForNotification", args);

            db.Close();
            return l;
        }
    }
}