﻿/**
 * Created By   : ark.yudha
 * Created Dt   : 12.02.2014
 * **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SPEX.Models.MailNotification
{
    public class MailAddressForNotification
    {
        public string MAIL_ADDRESS { get; set; }
    }
}