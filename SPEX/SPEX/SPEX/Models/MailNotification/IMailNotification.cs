﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPEX.Models.MailNotification
{
    public interface IMailNotification
    {
        IList<MailAddressForNotification> getMailAddressForNotification(string p_Mail_Group_CD, string p_CC_Status);
    }
}
