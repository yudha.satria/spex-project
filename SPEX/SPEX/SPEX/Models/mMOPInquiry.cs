﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using SPEX.Cls;

namespace SPEX.Models
{
    public class mMOPInquiry
    {
        public string PACK_MONTH { get; set; }
        public string PART_NO { get; set; }
        public string REORDER_FLAG { get; set; }
        public string DOCK_CD { get; set; }
        public string SUPPLIER { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string SUB_SUPPLIER { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_PLANT { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PMSP_FLAG { get; set; }
        public string N_FREQ_B { get; set; }
        public string N_VOLUME_B { get; set; }
        public string N_DAD_B { get; set; }
        public string N_FREQ { get; set; }
        public string N_VOLUME { get; set; }
        public string N_DAD { get; set; }
        public string N_1_FREQ { get; set; }
        public string N_1_VOLUME { get; set; }
        public string N_1_DAD { get; set; }
        public string N_2_FREQ { get; set; }
        public string N_2_VOLUME { get; set; }
        public string N_2_DAD { get; set; }
        public string N_3_FREQ { get; set; }
        public string N_3_VOLUME { get; set; }
        public string N_3_DAD { get; set; }
        public string VERS { get; set; }
        public string SEND_FLAG { get; set; }
        public string SEND_DT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public List<mMOPInquiry> GetData(mMOPInquiry PM, string Mode)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mMOPInquiry> l = db.Fetch<mMOPInquiry>("MOPInquirySearch",
                  new { PACK_MONTH = PM.PACK_MONTH },
                  new { PART_NO = PM.PART_NO },
                  new { SUPPLIER_CD = PM.SUPPLIER_CD },
                  new { SUPPLIER_PLANT = PM.SUPPLIER_PLANT },
                  new { SUB_SUPPLIER_CD = PM.SUB_SUPPLIER_CD },
                  new { SUB_SUPPLIER_PLANT = PM.SUB_SUPPLIER_PLANT },
                  new { PMSP_FLAG = PM.PMSP_FLAG },
                  new { REORDER_FLAG = PM.REORDER_FLAG },
                  new { VERS = PM.VERS },
                  new { SEND_FLAG = PM.SEND_FLAG },
                  new { Mode = Mode }
                  ).ToList();
            db.Close();
            return l;
        }

        public string UpdateData(string p_PACK_MONTH, string p_PART_NO, string p_SUPPLIER_CD, string p_SUPPLIER_PLANT, string p_DOCK_CD, string p_N_VOLUME, string p_N_1_VOLUME, string p_N_2_VOLUME, string p_N_3_VOLUME, string UserID)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPackingOrder>("MOPInquiryUpdate", new
                {
                    PACK_MONTH = p_PACK_MONTH,
                    PART_NO = p_PART_NO,
                    SUPPLIER_CD = p_SUPPLIER_CD,
                    SUPPLIER_PLANT = p_SUPPLIER_PLANT,
                    DOCK_CD = p_DOCK_CD,
                    N_VOLUME = p_N_VOLUME,
                    N_1_VOLUME = p_N_1_VOLUME,
                    N_2_VOLUME = p_N_2_VOLUME,
                    N_3_VOLUME = p_N_3_VOLUME,
                    USER_ID = UserID
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public List<string> GetReorderFlagList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("GetReorderFlagList").ToList();
            db.Close();
            return q;
        }

        public String CalcMOPInquiry(string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var q = db.SingleOrDefault<String>("MOPInquiryCalculate", new
                {
                    USER_ID = UserID
                }).ToString();
            db.Close();
            return q;
        }

        public String MOPSendToIPPCS(string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.SingleOrDefault<String>("MOPInquirySendToIPPCS", new
            {
                USER_ID = UserID
            }).ToString();
            db.Close();
            return q;
        }

        //add agi 2017012-04
        public string DeleteDataTBT()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPartPriceMaster>("MOPInquiry_DeleteTBT");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public string UploadReviseBackgroundProcess(string UserID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mUploadTLMS>("MOPInquiry_UploadReviseBackgroundProcess",
                             new
                             {
                                 USER_ID = UserID
                             }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

    }

    public class mMOPInquiryDownload
    {
        public string D_ID { get; set; }
        public string VERS { get; set; }
        public string REV_NO { get; set; }
        public string COMP_CD { get; set; }
        public string R_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string S_PLANT_CD { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_PLANT { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PMSP_FLAG { get; set; }
        public string ORD_TYPE { get; set; }
        public string ORDER_TYPE { get; set; }
        public string PACK_MONTH { get; set; }
        public string CAR_CD { get; set; }
        public string REX_CD { get; set; }
        public string SRC { get; set; }
        public string PART_NO { get; set; }
        public string KANBAN_NUMBER { get; set; }
        public decimal N_VOLUME { get; set; }
        public decimal N_FREQ { get; set; }
        public decimal N_DAD { get; set; }
        public decimal N_1_VOLUME { get; set; }
        public decimal N_1_FREQ { get; set; }
        public decimal N_1_DAD { get; set; }
        public decimal N_2_VOLUME { get; set; }
        public decimal N_2_FREQ { get; set; }
        public decimal N_3_VOLUME { get; set; }
        public decimal N_3_FREQ { get; set; }
        public decimal N_D01 { get; set; }
        public decimal N_D02 { get; set; }
        public decimal N_D03 { get; set; }
        public decimal N_D04 { get; set; }
        public decimal N_D05 { get; set; }
        public decimal N_D06 { get; set; }
        public decimal N_D07 { get; set; }
        public decimal N_D08 { get; set; }
        public decimal N_D09 { get; set; }
        public decimal N_D10 { get; set; }
        public decimal N_D11 { get; set; }
        public decimal N_D12 { get; set; }
        public decimal N_D13 { get; set; }
        public decimal N_D14 { get; set; }
        public decimal N_D15 { get; set; }
        public decimal N_D16 { get; set; }
        public decimal N_D17 { get; set; }
        public decimal N_D18 { get; set; }
        public decimal N_D19 { get; set; }
        public decimal N_D20 { get; set; }
        public decimal N_D21 { get; set; }
        public decimal N_D22 { get; set; }
        public decimal N_D23 { get; set; }
        public decimal N_D24 { get; set; }
        public decimal N_D25 { get; set; }
        public decimal N_D26 { get; set; }
        public decimal N_D27 { get; set; }
        public decimal N_D28 { get; set; }
        public decimal N_D29 { get; set; }
        public decimal N_D30 { get; set; }
        public decimal N_D31 { get; set; }
        public decimal N_1_D01 { get; set; }
        public decimal N_1_D02 { get; set; }
        public decimal N_1_D03 { get; set; }
        public decimal N_1_D04 { get; set; }
        public decimal N_1_D05 { get; set; }
        public decimal N_1_D06 { get; set; }
        public decimal N_1_D07 { get; set; }
        public decimal N_1_D08 { get; set; }
        public decimal N_1_D09 { get; set; }
        public decimal N_1_D10 { get; set; }
        public decimal N_1_D11 { get; set; }
        public decimal N_1_D12 { get; set; }
        public decimal N_1_D13 { get; set; }
        public decimal N_1_D14 { get; set; }
        public decimal N_1_D15 { get; set; }
        public decimal N_1_D16 { get; set; }
        public decimal N_1_D17 { get; set; }
        public decimal N_1_D18 { get; set; }
        public decimal N_1_D19 { get; set; }
        public decimal N_1_D20 { get; set; }
        public decimal N_1_D21 { get; set; }
        public decimal N_1_D22 { get; set; }
        public decimal N_1_D23 { get; set; }
        public decimal N_1_D24 { get; set; }
        public decimal N_1_D25 { get; set; }
        public decimal N_1_D26 { get; set; }
        public decimal N_1_D27 { get; set; }
        public decimal N_1_D28 { get; set; }
        public decimal N_1_D29 { get; set; }
        public decimal N_1_D30 { get; set; }
        public decimal N_1_D31 { get; set; }
        public string PART_NAME { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string REORDER_FLAG { get; set; }
        public string ADJUST_FLAG { get; set; }
        public string ADJUST_BY { get; set; }
        public DateTime? ADJUST_DT { get; set; }
        public string WORKING_DAY { get; set; }
        public string SEND_FLAG { get; set; }
        public string SEND_BY { get; set; }
        public DateTime? SEND_DT { get; set; }
        /*NQC*/
        public string UPDATE_FLAG { get; set; }
        public string S_DOCK_CD { get; set; }
        public string COMPANY_CD { get; set; }
        public string ID_LINE { get; set; }

        public List<mMOPInquiryDownload> getDownload(string p_MONTH, string p_REORDER, string p_SUPPLIER, string p_S_PLANT_CD, string p_SUB_SUPPLIER_CD, string p_SUB_SUPPLIER_PLANT, string p_PMSP_FLAG, string p_TIMMING, string p_PART_NO, string p_SEND_STATUS)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMOPInquiryDownload>("MOPInquiryDownload",
                new { MONTH = p_MONTH },
                new { REORDER = p_REORDER },
                new { SUPPLIER = p_SUPPLIER },
                new { S_PLANT_CD = p_S_PLANT_CD },
                new { SUB_SUPPLIER_CD = p_SUB_SUPPLIER_CD },
                new { SUB_SUPPLIER_PLANT = p_SUB_SUPPLIER_PLANT },
                new { PMSP_FLAG = p_PMSP_FLAG },
                new { TIMMING = p_TIMMING },
                new { PART_NO = p_PART_NO },
                new { SEND_STATUS = p_SEND_STATUS }
                );
            db.Close();

            return l.ToList();
        }

        public List<mMOPInquiryDownload> getDownloadNQC(string p_MONTH, string p_SUPPLIER, string p_S_PLANT_CD, string p_SUB_SUPPLIER_CD, string p_SUB_SUPPLIER_PLANT, string p_PMSP_FLAG, string p_TIMMING, string p_PART_NO, string p_SEND_STATUS)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMOPInquiryDownload>("MOPInquiryNQCDownload",
                new { MONTH = p_MONTH },
                new { SUPPLIER = p_SUPPLIER },
                new { S_PLANT_CD = p_S_PLANT_CD },
                new { SUB_SUPPLIER_CD = p_SUB_SUPPLIER_CD },
                new { SUB_SUPPLIER_PLANT = p_SUB_SUPPLIER_PLANT },
                new { PMSP_FLAG = p_PMSP_FLAG },
                new { TIMMING = p_TIMMING },
                new { PART_NO = p_PART_NO },
                new { SEND_STATUS = p_SEND_STATUS }
                );
            db.Close();

            return l.ToList();
        }

    }
}