﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPart
    {
        //FUNGSI GETHER SETTER - Sama dengan Query Database
        public string Part_No { get; set; }
        public string Part_Name { get; set; }
        public string Car_Family_Cd { get; set; }
        public string Model { get; set; }
        public string Paint_Type { get; set; }
        public string Part_Spec { get; set; }
        public int Takt_Time { get; set; }
        public int PcsKbn { get; set; }
        public DateTime Latest_Order { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Dt { get; set; }
        public string Changed_By { get; set; }
        public DateTime Changed_Dt { get; set; }
        public string Deletion_Flag { get; set; }
        public string Text { get; set; }

        public List<mPart> SPN311GetListMasterPartSpecific(string p_Part_No, string p_Car_Family_Cd, string p_Paint_Type)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPart>("Master_Part_Select", new { PART_NO = p_Part_No }, new { CAR_FAMILY_CD = p_Car_Family_Cd }, new { PART_TYPE = p_Paint_Type });
            db.Close();
            return l.ToList();
        }

        public List<mPart> SPN311GetListMasterPart()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPart>("Master_Part_Select_0");
            db.Close();
            return l.ToList();
        }

        public string SPN311SaveData(string p_Part_No, string p_Part_Name, string p_Car_Family_Cd, string p_Paint_Type, string p_Part_Spec, string p_Takt_Time, string p_PcsKbn, string p_Latest_Order, string p_Created_By)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPart>("Master_Part_Save", new { PART_NO = p_Part_No }, new { PART_NAME = p_Part_Name }, new { CAR_FAMILY_CD = p_Car_Family_Cd }, new { PAINT_TYPE = p_Paint_Type }, new { PART_SPEC = p_Part_Spec }, new { TAKT_TIME = p_Takt_Time }, new { PCSKBN = p_PcsKbn }, new { LATEST_ORDER = p_Latest_Order }, new { CREATED_BY = p_Created_By }, new { MSG_TEXT = "" });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public string SPN311UpdateData(string p_Part_No, string p_Part_Name, string p_Car_Family_Cd, string p_Paint_Type, string p_Part_Spec, string p_Takt_Time, string p_PcsKbn, string p_Latest_Order, string p_Created_By)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPart>("Master_Part_Update", new { PART_NO = p_Part_No }, new { PART_NAME = p_Part_Name }, new { CAR_FAMILY_CD = p_Car_Family_Cd }, new { PAINT_TYPE = p_Paint_Type }, new { PART_SPEC = p_Part_Spec }, new { TAKT_TIME = p_Takt_Time }, new { PCSKBN = p_PcsKbn }, new { LATEST_ORDER = p_Latest_Order }, new { CREATED_BY = p_Created_By }, new { MSG_TEXT = "" });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public string SPN311DeleteData(string p_Part_No)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                if (p_Part_No.Contains(";"))
                {
                    string[] PartNoList = p_Part_No.Split(';');
                    for (int i = 0; i < PartNoList.Length; i++)
                    {
                        var l = db.Fetch<mPart>("Master_Part_Delete", new { PART_NO = PartNoList[i] });
                        foreach (var message in l)
                        {
                            result = result + message.Text;
                        }
                    }
                }
                else
                {
                    var l = db.Fetch<mPart>("Master_Part_Delete", new { PART_NO = p_Part_No });
                    foreach (var message in l)
                    {
                        result = result + message.Text;
                    }
                }
                db.Close();

                if(result.Contains("error")){
                    return "Process DELETE PART MASTER finish with error";
                } else {
                    return "Process DELETE PART MASTER finish successfully";
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public string SPN311SaveUploadData(string p_Part_No, string p_Part_Name, string p_Car_Family_Cd, string p_Paint_Type, string p_Part_Spec, string p_Takt_Time, string p_PcsKbn, string p_Latest_Order, string p_Created_By, string p_Process_Id)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                db.Execute("Master_Part_Save_Upload", new { PART_NO = p_Part_No }, new { PART_NAME = p_Part_Name }, new { CAR_FAMILY_CD = p_Car_Family_Cd }, new { PAINT_TYPE = p_Paint_Type }, new { PART_SPEC = p_Part_Spec }, new { TAKT_TIME = p_Takt_Time }, new { PCSKBN = p_PcsKbn }, new { LATEST_ORDER = p_Latest_Order }, new { CREATED_BY = p_Created_By }, new { MSG_TEXT = "" }, new { PROCESS_ID = p_Process_Id });
                db.Close();

                return "Process SAVE UPLOAD PART finish successfully";
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public string SPN311DeleteUploadedData(string p_Process_Id)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Execute("Master_Part_Delete_Upload", new { PROCESS_ID = p_Process_Id });
                db.Close();
                
                return "Process DELETE UPLOADED PART finish successfully";
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public string SPN311CheckUploadedData(string p_Created_By, string ProcessId)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPart>("Master_Part_Save_Upload_Validation", new { PROCESS_ID = ProcessId }, new { CREATED_BY = p_Created_By });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text.ToString();
                }

                if (result == "SUCCESS")
                {
                    return "Process CHECK UPLOAD PART finish successfully";
                }
                else
                {
                    return result;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }
    }
}