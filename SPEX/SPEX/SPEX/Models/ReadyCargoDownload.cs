﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class ReadyCargoDownload
    {
        public string DESTINATION { get; set; }
        public string TRANSPORT_CD { get; set; }
        public string TRANSPORT { get; set; }
        public string ORDER_NO { get; set; }
        public string CASE_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public DateTime PACKAGE_DATE { get; set; }
        public DateTime VANNING_DATE { get; set; }
        public string CONTAINER_NO { get; set; }
        public int QTY { get; set; }
        public string INVOICE_NO { get; set; }
        public DateTime ETD { get; set; }
        public string PACKING_COMPANY { get; set; }

        public DateTime? ORDER_DT_FROM { get; set; }

        public DateTime? ORDER_DT_TO { get; set; }

        public DateTime? PACKING_DT_FROM { get; set; }

        public DateTime? PACKING_DT_TO { get; set; }

        public List<string> GetPackingCompany()
        {
            //string url = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("RADYCARGO_GET_GetPackingCompany").ToList();
            db.Close();
            return q;
        }

        public List<ReadyCargoDownload> GetData(ReadyCargoDownload Cargo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ReadyCargoDownload> q = db.Fetch<ReadyCargoDownload>
                ("READYCARGO_DOWNLOAD_GET_GET_DATA",
                   new { ORDER_DT_FROM = Cargo.ORDER_DT_FROM }, 
                   new { ORDER_DT_TO = Cargo.ORDER_DT_TO }, 
                   new { PACKING_DT_FROM = Cargo.PACKING_DT_FROM },   
                   new { PACKING_DT_TO = Cargo.PACKING_DT_TO },
                   new { PACKING_COMPANY = Cargo.PACKING_COMPANY }
                ).ToList();
            db.Close();
            return q;
        }

        public int GetDiferrentDate()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            int q = db.Fetch<int>
                ("READYCARGO_DOWNLOAD_GetDiferrentDate",
                   new { System_Type = "ReadyCargoDownload" },
                   new { System_CD = "DiferentMonth" }
                ).SingleOrDefault();
            db.Close();
            return q;
        }

        public string GetMessageDiferrentDateOrderDate()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string q = db.Fetch<string>
                ("READYCARGO_DOWNLOAD_GetMessageDiferrentDateOrderDate"
                ).SingleOrDefault();
            db.Close();
            return q;
        }

        public string GetMessageDiferrentDatePackingrDate()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string q = db.Fetch<string>
                ("READYCARGO_DOWNLOAD_GetMessageDiferrentDatePackingDate"
                ).SingleOrDefault();
            db.Close();
            return q;
        }
    }
}
