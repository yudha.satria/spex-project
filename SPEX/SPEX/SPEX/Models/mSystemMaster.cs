﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mSystemMaster
    {
        public string SYSTEM_TYPE { get; set; }
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }
        public string REMARK { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CREAED_BY { get; set; }
        public DateTime? UPDATED_DT { get; set; }
        public string UPDATED_BY { get; set; }

        public List<mSystemMaster> GetSystemValueList(string systemType, string systemCD, string mode, string separator = ";")
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetSystemValueList",
                new { SYSTEM_TYPE = systemType },
                new { SYSTEM_CD = systemCD },
                new { MODE = mode },
                new { SEPARATOR = separator }
                ).ToList();
            db.Close();
            return datas;
        }

        //Added by FID.Arri at 2017-07-12 for MOP Inquiry Screen
        public List<mSystemMaster> GetComboBoxList(string systemType)
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetComboBoxfrSystem",
                new { SYSTEM_TYPE = systemType }
            ).ToList();
            db.Close();
            return datas;
        }
    }
}