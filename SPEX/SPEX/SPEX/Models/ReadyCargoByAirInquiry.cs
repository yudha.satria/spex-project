﻿using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class ReadyCargoByAirInquiry
    {
        public string BUYER_CD { get; set; }
        public string PD_CD { get; set; }
        public string CASE_DANGER_FLAG { get; set; }
        public string PACKING_COMPANY { get; set; }
        public string INVOICE_NO { get; set; }
        public int INCOMPLETE { get; set; }
        public int COMPLETE { get; set; }
        public string User_Name { get; set; }

        //add agi 2016-12-08 add parameter for call invoice 
        public string VANNING_DT_FROM { get; set; }

        public string VANNING_DT_TO { get; set; }

        //ADD AGI 2017-09-06 REQUEST MRS HESTI PENAMABAHAN KOLOM GRID 

        public string VANNING_NO { get; set; }

        public string FORWARDER { get; set; }

        public List<mBuyerPD> GetAllBuyerPD()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mBuyerPD> l = db.Fetch<mBuyerPD>("Ready_Cargo_ByAir_Get_BuyerPD_All").ToList();
            db.Close();
            return l;
        }

        public List<string> GetPackingCompany()
        {
            //string url = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("RADYCARGO_GET_GetPackingCompany").ToList();
            db.Close();
            return q;
        }

        public List<string> GetAllInvoiceReadyCargoByAirStatus()
        {
            List<string> ListInvoiceStatus = new List<string>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            ListInvoiceStatus = db.Fetch<string>("GetAllInvoiceStatusReadyCargoByAirStatus").ToList();
            db.Close();
            return ListInvoiceStatus;
        }

        public List<string> GetDGCargo()
        {
            List<string> ListInvoiceStatus = new List<string>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            ListInvoiceStatus = db.Fetch<string>("ReadyCargoByAirGetDGCargo").ToList();
            db.Close();
            return ListInvoiceStatus;
        }

        //remaks agi 2016-12-07 add parameter search
        //public List<ReadyCargoByAirInquiry> ReadyCargoInquirySearch(string BUYER_PD_CD, string PACKING_COMPANY, string DG_CARGO, string INVOICE_STATUS)
        //public List<ReadyCargoByAirInquiry> ReadyCargoInquirySearch(string BUYER_PD_CD, string PACKING_COMPANY, string DG_CARGO, string INVOICE_STATUS, string VANNING_DT_FROM, string VANNING_DT_TO, string COMPLATE_STATUS)
        //change agi 2017-09-07 cr mrs hesti crf 
        public List<ReadyCargoByAirInquiry> ReadyCargoInquirySearch(string BUYER_PD_CD, string PACKING_COMPANY, string DG_CARGO, string INVOICE_STATUS, string VANNING_DT_FROM, string VANNING_DT_TO, string COMPLATE_STATUS,string CASE_NO)
        {
            //List<ReadyCargoByAirInquiry> RCs = new List<ReadyCargoByAirInquiry>();
            IDBContext db = DatabaseManager.Instance.GetContext();         
            var RCs = db.Fetch<ReadyCargoByAirInquiry>("ReadyCargoByAirsearch",
                                    new { BUYER_PD_CD = BUYER_PD_CD },
                                    new { PACKING_COMPANY = PACKING_COMPANY },
                                    new { DG_CARGO = DG_CARGO },
                                    new { INVOICE_STATUS = INVOICE_STATUS },
                                    //add by agi 2016-12-07 add parameter searching
                                    new { VANNING_DT_FROM = VANNING_DT_FROM },
                                    new { VANNING_DT_TO = VANNING_DT_TO },
                                    new { COMPLATE_STATUS = COMPLATE_STATUS },
                                    //ADD AGI 2017-09-07 REQUEST MRS HESTI
                                    new { CASE_NO = CASE_NO }
                                    ).ToList();
            db.Close();
            return RCs;
        }

        //REMAKS AGI 2016-12-08 ADD PARAMETER REQUEST MR YONI
        //public List<ReadyCargoByAirDetailInquiry> ReadyCargoDetailInquirySearch(string BUYER_CD,string PD_CD, string PACKING_COMPANY, string DG_CARGO,string INVOICE_NO, string COMPLETE_STATUS)
        public List<ReadyCargoByAirDetailInquiry> ReadyCargoDetailInquirySearch(string BUYER_CD, string PD_CD, string PACKING_COMPANY, string DG_CARGO, string INVOICE_NO, string COMPLETE_STATUS, string VANNING_DT_FROM, string VANNING_DT_TO)
        {
            //List<ReadyCargoByAirInquiry> RCs = new List<ReadyCargoByAirInquiry>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            var RCs = db.Fetch<ReadyCargoByAirDetailInquiry>("ReadyCargoByAirDetailSearch",
                                    new { BUYER_CD = BUYER_CD },
                                    new { PD_CD = PD_CD },
                                    new { PACKING_COMPANY = PACKING_COMPANY },
                                    new { DG_CARGO = DG_CARGO },
                                    new {INVOICE_NO=INVOICE_NO},
                                    new { COMPLETE_STATUS = COMPLETE_STATUS },
                                    //add agi 2016-12-08 add parameter 
                                    new { VANNING_DT_FROM = VANNING_DT_FROM },
                                    new { VANNING_DT_TO = VANNING_DT_TO }
                                    ).ToList();
            db.Close();
            return RCs;
        }

        public string getUrlCreateInvoice()
        {
            string url = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            url = db.Fetch<string>("ReadyCargoByAir_GET_URL_CREATE_INVOICE").First();
            db.Close();
            return url;
        }

        //add by agi 2016-12-07 request MR Yoni for invoice searching criteria
        public List<string> GetCompletnesStatus()
        {
            List<string> List = new List<string>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            List = db.Fetch<string>("ReadyCargoByAirGetCompletnesStatus").ToList();
            db.Close();
            return List;
        } 

      
    }
}