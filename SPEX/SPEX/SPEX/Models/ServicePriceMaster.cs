﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class ServicePriceMaster
    {
        public string SERVICE_REGISTRATION { get; set; }
        public string SERVICE_CODE { get; set; }
        public string SERVICE_NAME { get; set; }
        public string SERVICE_TYPE { get; set; }
        public string CONTAINER_SIZE { get; set; }
        public string PRICE { get; set; }
        public string CURRENCY { get; set; }
        public string VAT { get; set; }
        public string COST_CENTER { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
        public string VENDOR_TYPE { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DATE { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DATE { get; set; }

        public string DELETION_FLAG { get; set; }
        public string ERROR_MSG { get; set; }
        public string IS_LATEST { get; set; }

        public List<String> GetServiceId()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<String>("GetServiceId",
                  new { }
                  );
            db.Close();

            return l.ToList();
        }

        public List<String> GetTipe()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<String>("GetType",
                  new { }
                  );
            db.Close();

            return l.ToList();
        }

        public List<string> GetContainerSize()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<string> l = db.Fetch<string>("GetContainerSize"
                  ).ToList();
            db.Close();
            return l;
        }

        public List<string> GetCurrency()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<string> l = db.Fetch<string>("GetCurrency"
                  ).ToList();
            db.Close();
            return l;
        }

        public List<string> GetVendorType()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<String>("GetVendorType", new { });
            db.Close();

            return l.ToList();
        }

        public List<ServicePriceMaster> getServicePriceMaster(string ServiceReg, string ServiceCode, string ServiceName, string ServiceType, 
                                                              string ValidFrom, string ValidTo, string IsLatest, string VendorType, string Vat)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ServicePriceMaster> l = db.Fetch<ServicePriceMaster>("ServicePriceMaster_Search",
                  new { SERVICE_REG = ServiceReg },
                  new { SERVICE_CODE = ServiceCode },
                  new { SERVICE_NAME = ServiceName },
                  new { SERVICE_TYPE = ServiceType },
                  new { VALID_FROM = ValidFrom },
                  new { VALID_TO = ValidTo },
                  new { MODE = "" },
                  new { IS_LATEST = IsLatest },
                  new { VENDOR_TYPE = VendorType },
                  new { VAT = Vat}
                  ).ToList();
            db.Close();
            return l;
        }

        public string DeleteTempData()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            try
            {
                var l = db.Fetch<mPartPriceMaster>("ServicePriceMaster_DeleteTemp");
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public string BulkInsertTempCSVToTempPartPrice(string userid, long processid)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                result = db.ExecuteScalar<string>("ServicePriceMaster_Upload_Validation", new { USER_ID = userid }, new { PROCESS_ID = processid });
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            finally 
            {
                db.Close();
            }

            return result;
        }

        public List<ServicePriceMaster> getServicePriceErrorUpload()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ServicePriceMaster> l = db.Fetch<ServicePriceMaster>("ServicePriceMaster_ErrorUpload").ToList();
            db.Close();
            return l;
        }
    }

    public class ServicePriceMasterEditAdd
    {
        public string SERVICE_REGISTRATION { get; set; }
        public string SERVICE_CODE { get; set; }
        public string SERVICE_NAME { get; set; }
        public string CONTAINER_SIZE { get; set; }
        public string SERVICE_TYPE { get; set; }
        public string PRICE { get; set; }
        public string CURRENCY { get; set; }
        public string VAT { get; set; }
        public string COST_CENTER { get; set; }
        public DateTime VALID_FROM { get; set; }
        public DateTime VALID_TO { get; set; }
        public string VENDOR_TYPE { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DATE { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DATE { get; set; }

        public string DELETION_FLAG { get; set; }

        public DateTime CREATED_DATE2 { get; set; }
        public DateTime CHANGED_DATE2 { get; set; }

        public List<string> Tipe { get { return new ServicePriceMaster().GetTipe(); } }
        public List<string> ContainerSize { get { return new ServicePriceMaster().GetContainerSize(); } }
        public List<string> Curency { get { return new ServicePriceMaster().GetCurrency(); } }
        public List<string> VendorType { get { return new ServicePriceMaster().GetVendorType(); } }

        public List<ServicePriceMasterEditAdd> GetData(ServicePriceMasterEditAdd spm, string mode)
        {
            List<ServicePriceMasterEditAdd> l = new List<ServicePriceMasterEditAdd>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            l = db.Fetch<ServicePriceMasterEditAdd>("ServicePriceMaster_Search",
                  new { SERVICE_REG = spm.SERVICE_REGISTRATION },
                  new { SERVICE_CODE = "" },
                  new { SERVICE_NAME = "" },
                  new { SERVICE_TYPE = "" },
                  new { VALID_FROM = "" },
                  new { VALID_TO = "" },
                  new { MODE = mode },
                  new { IS_LATEST = "false" },
                  new { VENDOR_TYPE = "" },
                  new { VAT = "" }
                ).ToList();
            db.Close();
            return l;
        }

        public string AddData(ServicePriceMasterEditAdd spm)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ServicePriceMaster_Add",
                 new { SERVICE_REG = spm.SERVICE_REGISTRATION },
                 new { SERVICE_CODE = spm.SERVICE_CODE },
                 new { SERVICE_NAME = spm.SERVICE_NAME },
                 new { SERVICE_TYPE = spm.SERVICE_TYPE },
                 new { CONTAINER_SIZE = spm.CONTAINER_SIZE },
                 new { PRICE = spm.PRICE },
                 new { CURRENCY = spm.CURRENCY },
                 new { VAT = spm.VAT },
                 new { COST_CENTER = spm.COST_CENTER },
                 new { VALID_FROM = spm.VALID_FROM },
                 new { VALID_TO = spm.VALID_TO },
                 new { VENDOR_TYPE = spm.VENDOR_TYPE },
                 new { CREATED_BY = spm.CREATED_BY }
                );
            db.Close();
            return result;
        }

        public string GenerateNewServiceID()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ServicePriceMaster_GenerateSID");
            db.Close();
            return result;
        }

        public string UpdateData(ServicePriceMasterEditAdd spm)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ServicePriceMaster_Update",
                 new { SERVICE_REG = spm.SERVICE_REGISTRATION },
                 new { SERVICE_CODE = spm.SERVICE_CODE },
                 new { SERVICE_NAME = spm.SERVICE_NAME },
                 new { SERVICE_TYPE = spm.SERVICE_TYPE },
                 new { CONTAINER_SIZE = spm.CONTAINER_SIZE },
                 new { PRICE = spm.PRICE },
                 new { CURRENCY = spm.CURRENCY },
                 new { VAT = spm.VAT },
                 new { COST_CENTER = spm.COST_CENTER },
                 new { VALID_FROM = spm.VALID_FROM },
                 new { VALID_TO = spm.VALID_TO },
                 new { VENDOR_TYPE = spm.VENDOR_TYPE },
                 new { CHANGED_BY = spm.CHANGED_BY }
                );
            db.Close();
            return result;
        }

        public string Delete(string ServiceReg, string UserId)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = string.Empty;
            result = db.SingleOrDefault<string>("ServicePriceMaster_delete", new { SERVICE_REG = ServiceReg, USER_ID = UserId });
            db.Close();
            return result;
        }
    }
}