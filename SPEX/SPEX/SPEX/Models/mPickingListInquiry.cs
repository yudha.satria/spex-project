﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Diani Widyaningrum
namespace SPEX.Models
{
    public class mPickingListInquiry
    {
        public string PICKING_LIST_NO { get; set; }
        public string ZONE { get; set; }
        public string PICKING_LIST_DATE { get; set; }
        public string PICKING_LIST_TIME { get; set; }
        public string STATUS { get; set; }
        public string TRANSP_CD { get; set; }
        public string TRANSP_DESC { get; set; }
        public string BUYER_PD { get; set; }
        public string TMAP_ORDER_TYPE { get; set; }
        public string ORDER_TYPE_TMMIN { get; set; }
        public string REMAIN_RECEIVE_QTY { get; set; }
        public string TMAP_ORDER_QTY { get; set; }
        public string ORDER_QTY { get; set; }
        public string RACK_ADDRESS_CD { get; set; }
        public string TMAP_ITEM_NO { get; set; }
        public string TMAP_ORDER_NO { get; set; }
        public string PRINT_FLAG { get; set; }
        public string RECEIVE_QTY { get; set; }

        public string PART_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string Text { get; set; }
        public string CANCEL_BY { get; set; }
        public string CANCEL_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
        public string KANBAN_ID { get; set; }
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }



        public List<mPickingListInquiry> getListPickingListInquiry(string pPICKING_LIST_NO, string pPICKING_LIST_DATE, string pPICKING_LIST_DATE_TO, string pSTATUS, string pZONE, string pPICKING_LIST_TIME, string pTRANSP_CD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPickingListInquiry>("SearchPickingListInquiry",
                    new { PICKING_LIST_NO = pPICKING_LIST_NO },
                    new { PICKING_LIST_DATE = pPICKING_LIST_DATE },
                    new { PICKING_LIST_DATE_TO = pPICKING_LIST_DATE_TO },
                    new { STATUS = pSTATUS },
                    new { ZONE = pZONE },
                    new { PICKING_LIST_TIME = pPICKING_LIST_TIME },
                    new { TRANSP_CD = pTRANSP_CD }
                    );
            db.Close();
            return l.ToList();
        }

        public List<mPickingListInquiry> getListPickingListInquiryDownload(string pPICKING_LIST_NO, string pPICKING_LIST_DATE, string pPICKING_LIST_DATE_TO, string pSTATUS, string pZONE, string pPICKING_LIST_TIME, string pTRANSP_CD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPickingListInquiry>("SearchPickingListInquiryDownload",
                    new { PICKING_LIST_NO = pPICKING_LIST_NO },
                    new { PICKING_LIST_DATE = pPICKING_LIST_DATE },
                    new { PICKING_LIST_DATE_TO = pPICKING_LIST_DATE_TO },
                    new { STATUS = pSTATUS },
                    new { ZONE = pZONE },
                    new { PICKING_LIST_TIME = pPICKING_LIST_TIME },
                    new { TRANSP_CD = pTRANSP_CD }
                    );
            db.Close();
            return l.ToList();
        }

        public string GetTelerikPath()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string path = db.SingleOrDefault<string>("Telerik_Path");
            db.Close();
            return path;
        }

        public List<mSystemMaster> GetComboBoxZoneList()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetComboBoxZone").ToList();
            db.Close();
            return datas;
        }
 
        public List<mSystemMaster> GetAllPrinter()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("Printer_GetList").ToList();
            db.Close();
            return datas;
        }

        public static List<string> GetPickingListTime()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var PickingListTime = db.Fetch<string>("GetPickingListTime").ToList();
            db.Close();
            return PickingListTime;
        }

        public List<mSystemMaster> GetComboBoxTranpCode()
        {
          List<mSystemMaster> datas = new List<mSystemMaster>();
          IDBContext db = DatabaseManager.Instance.GetContext();
          datas = db.Fetch<mSystemMaster>("GetComboBoxTranpCode").ToList();
          db.Close();
          return datas;
        }

        public string CancelData(List<mPickingListInquiry> dataModel)
        {
            String result = String.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                foreach (mPickingListInquiry dt in dataModel)
                {
                    dynamic args = new
                    {
                        MANIFEST_NO = dt.PICKING_LIST_NO,
                        CANCEL_BY = dt.CHANGED_BY,
                        CHANGED_BY = dt.CHANGED_BY,
                        CHANGED_DT = dt.CHANGED_DT
                    };
                    result = db.SingleOrDefault<String>("PickingListInquiryCancel", args);
                    if (result.Split('|')[0].Equals("E"))
                    {
                        break;
                    }
                }
                if (result.Split('|')[0].Equals("E"))
                {
                    db.AbortTransaction();
                }
                else
                {
                    db.CommitTransaction();
                }
            }
            catch (Exception e)
            {
                result = "E|" + e.Message;
                db.AbortTransaction();
            }
            finally
            {
                db.Close();
            }
            return result;

        }


    }

    public class mPickingListInquiryDetailPart
    {
        public string PART_NO { get; set; }
        public string TMAP_ORDER_NO { get; set; }
        public string TMAP_ITEM_NO { get; set; }
        public string RACK_ADDRESS_CD { get; set; }
        public string ORDER_QTY { get; set; }
        public string REMAIN_RECEIVE_QTY { get; set; }
        public string TMAP_ORDER_QTY { get; set; }
        public string ORDER_TYPE_TMMIN { get; set; }
        public string TMAP_ORDER_TYPE { get; set; }
        public string BUYER_PD { get; set; }
        public string TRANSP_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }
        public string PART_PICKING_LIST_NO { get; set; }
        public string RECEIVE_QTY { get; set; }

        public string PICKING_LIST_NO { get; set; }
        public string ZONE { get; set; }
        public string PICKING_LIST_DATE { get; set; }
        public string PICKING_LIST_TIME { get; set; }

        public string Text { get; set; }

        public List<mPickingListInquiryDetailPart> getDetailParts(string pPICKING_LIST_NO, string pPART_NO, string pORDER_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPickingListInquiryDetailPart>("SearchPickingListInquiryDetailPart",
                    new { MANIFEST_NO = pPICKING_LIST_NO },
                    new { PART_NO = pPART_NO },
                    new { ORDER_NO = pORDER_NO });
            db.Close();
            return l.ToList();
        }
    }

    public class mPickingListInquiryDetailKanban
    {
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string TMAP_PART_NO { get; set; }
        public string KANBAN_ID { get; set; }
        public string TMAP_ITEM_NO { get; set; }    
        public string MANIFEST_ITEM_NO { get; set; }
        public string PICKING_FLAG { get; set; }
        public string PICKING_FLAG_STATUS { get; set; }

        public string Text { get; set; }

        public List<mPickingListInquiryDetailKanban> getDetailKanbans(string pPICKING_LIST_NO, string pPART_NO, string pTMAP_ITEM_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPickingListInquiryDetailKanban>("SearchPickingListInquiryDetailKanban",
                    new { MANIFEST_NO = pPICKING_LIST_NO },
                    new { PART_NO = pPART_NO },
                    new { TMAP_ITEM_NO = pTMAP_ITEM_NO}  
                    );
            db.Close();
            return l.ToList();
        }

    }


}
