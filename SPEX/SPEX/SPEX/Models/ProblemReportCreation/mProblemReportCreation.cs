﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Muhajir Shiddiq
namespace SPEX.Models
{
    public class mProblemReportCreation
    {
        public string PR_CD { get; set; }
        public string PR_DATE { get; set; }
        public string SUB_MANIFEST_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string KANBAN_ID { get; set; }
        public string PART_NAME { get; set; }
        public int    PROBLEM_QTY { get; set; }
        public string PROBLEM_ORIGIN_CD { get; set; }
        public string PROBLEM_CATEGORY_CD { get; set; }
        public string PROBLEM_SUB_ORIGIN { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string DOCK_CD { get; set; }
        public string COST_CENTER { get; set; }
        public string STATUS { get; set; }
        public string FINISHED_PR_DT { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
        
        public string Text { get; set; }



        public List<mProblemReportCreation> getListProblemReport()
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mProblemReportCreation>("ProblemReportCreation/SearchProblemReportCreation");
            db.Close();
            return l.ToList();
        }

        public string SaveData(string pPROBLEM_ORIGIN_CD, string pPROBLEM_CATEGORY_CD, string pCOST_CENTER, string pKANBAN_ID, string pCREATED_BY)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            string l = db.SingleOrDefault<string>("ProblemReportCreation/SaveProblemReportCreation",
                    new { MODE                  = "add" },
                    new { PROBLEM_ORIGIN_CD     = pPROBLEM_ORIGIN_CD },
                    new { PROBLEM_CATEGORY_CD   = pPROBLEM_CATEGORY_CD },
                    new { COST_CENTER           = pCOST_CENTER },
                    new { KANBAN_ID             = pKANBAN_ID },
                    new { PROBLEM_REPORT_CD     = "" },
                    new { PROBLEM_QTY           = "" },
                    new { CREATED_BY            = pCREATED_BY },
                    new { CHANGED_BY            = pCREATED_BY },
                    new { CHANGED_DT            = "" }          
                    );
            db.Close();
            return l;
        }


        public string EditData(string pPROBLEM_ORIGIN_CD, string pPROBLEM_CATEGORY_CD, string pCOST_CENTER, string pKANBAN_ID, string pCREATED_BY, string pPROBLEM_REPORT_CD, int pPROBLEM_QTY, string iChangedBy, string iChangedDt)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            string l = db.SingleOrDefault<string>("ProblemReportCreation/SaveProblemReportCreation",
                    new { MODE = "edit" },
                    new { PROBLEM_ORIGIN_CD = pPROBLEM_ORIGIN_CD },
                    new { PROBLEM_CATEGORY_CD = pPROBLEM_CATEGORY_CD },
                    new { COST_CENTER = pCOST_CENTER },
                    new { KANBAN_ID = pKANBAN_ID },
                    new { PROBLEM_REPORT_CD = pPROBLEM_REPORT_CD },
                    new { PROBLEM_QTY = pPROBLEM_QTY},
                    new { CREATED_BY = pCREATED_BY },
                    new { CHANGED_BY = iChangedBy },
                    new { CHANGED_DT = iChangedDt }          
                    );
            db.Close();
            return l;
        }

        public String DeleteData(List<mProblemReportCreation> dataModel, string pCREATED_BY)
        {
            String result = String.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                foreach (mProblemReportCreation dt in dataModel)
                {
                    dynamic args = new
                    {
                        MODE = "delete",
                        PROBLEM_REPORT_CD = dt.PR_CD,
                        KANBAN_ID = dt.KANBAN_ID,
                        PROBLEM_ORIGIN_CD = "",
                        PROBLEM_CATEGORY_CD = "", 
                        COST_CENTER = "",
                        PROBLEM_QTY = "",
                        CREATED_BY = pCREATED_BY,
                        CHANGED_BY = dt.CHANGED_BY,
                        CHANGED_DT = dt.CHANGED_DT
                    };
                    result = db.SingleOrDefault<String>("ProblemReportCreation/SaveProblemReportCreation", args);
                    if (result.Split('|')[0].Equals("E"))
                    {
                        break;
                    }
                }
                if (result.Split('|')[0].Equals("E"))
                {
                    db.AbortTransaction();
                }
                else
                {
                    db.CommitTransaction();
                }
            }
            catch (Exception e)
            {
                result = "E|" + e.Message;
                db.AbortTransaction();
            }
            finally
            {
                db.Close();
            }
            return result;
        }


        public string SendToIPPCS()
        {

            IDBContext db = DatabaseManager.Instance.GetContext();
            string l = db.SingleOrDefault<string>("ProblemReportCreation/ProblemReportCreationSendToIppcs");
            db.Close();
            return l;
        }

    }
}
