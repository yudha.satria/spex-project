﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mBuyerPD
    {
        public string BUYER_PD_CD { get; set; }
        public string CUSTOMER_NO { get; set; }
        public string DESCRIPTION { get; set; }
        //add agi 2017-08-07
        public string DESTINATION_CODE { get; set; }
        public string PRIVILEGE { get; set; }
        public string COUNTRY_CD { get; set; }
        public string COUNTRY_CD_DESC { get; set; }
        public string CASE_LABEL_CD { get; set; }

        //end add
        public string USED_FLAG { get; set; }
        public string DELETION_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string Text { get; set; }

        //public List<mBuyerPD> getListBuyerPD(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION)
        //modif agi 2017-08-07
        public List<mBuyerPD> getListBuyerPD(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION, string pDESTINATION_CODE, string pPRIVILEGE, string pCOUNTRY_CD, string pCOUNTRY_CD_DESC, string pCASE_LABEL_CD)
        {
            
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mBuyerPD>("BuyerPD_Search",
                  new { BUYER_PD_CD = pBUYER_PD_CD },
                  new { CUSTOMER_NO = pCUSTOMER_NO },
                  new { DESCRIPTION = pDESCRIPTION },
                  //add agi 2017-08-07
                  new { DESTINATION_CODE = pDESTINATION_CODE },
                  new { PRIVILEGE = pPRIVILEGE },
                  new { COUNTRY_CD = pCOUNTRY_CD },
                  new { COUNTRY_CD_DESC = pCOUNTRY_CD_DESC },
                  new { CASE_LABEL_CD = pCASE_LABEL_CD }
                  );
            db.Close();
            return l.ToList();
        }

     //Delete Data
        public string DeleteData(string p_BUYER_PD_CD, string p_CHANGED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                if (p_BUYER_PD_CD.Contains(";"))
                {
                    string[] BuyerPDList = p_BUYER_PD_CD.Split(';');
                    for (int i = 0; i < BuyerPDList.Length; i++)
                    {
                        var l = db.Fetch<mBuyerPD>("BuyerPD_Delete", new {
                            BUYER_PD_CD = BuyerPDList[i],
                            CHANGED_BY = p_CHANGED_BY,
                            MSG_TEXT = ""
                        });
                        foreach (var message in l)
                        { result = message.Text; }
                    }
                }
                else
                {
                    var l = db.Fetch<mPriceTerm>("BuyerPD_Delete", new {
                        BUYER_PD_CD = p_BUYER_PD_CD,
                        CHANGED_BY = p_CHANGED_BY,
                        MSG_TEXT = ""
                    });
                    foreach (var message in l)
                    { result = message.Text; }
                }
                db.Close();
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        //Save Data
        //public string SaveData(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION, string pCREATED_BY)
        //modif agi 2017-08-07
        public string SaveData(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION, string pCREATED_BY, string pDESTINATION_CODE, string pCOUNTRY_CD, string pCOUNTRY_CD_DESC, string pPRIVILEGE, string pCASE_LABEL_CD)
        {
             
        string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mBuyerPD>("BuyerPD_Save", new
                {
                    BUYER_PD_CD = pBUYER_PD_CD,
                    CUSTOMER_NO = pCUSTOMER_NO,
                    DESCRIPTION = pDESCRIPTION,
                    CREATED_BY = pCREATED_BY,
                    MSG_TEXT = "",
                    //add agi 2017-08-07
                    DESTINATION_CODE = pDESTINATION_CODE,
                    COUNTRY_CD = pCOUNTRY_CD,
                    COUNTRY_CD_DESC = pCOUNTRY_CD_DESC,
                    PRIVILEGE = pPRIVILEGE,
                    CASE_LABEL_CD = pCASE_LABEL_CD
                    //end add
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }


        //Update Data
        //public string UpdateData(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION, string pDELETION_FLAG, string pCHANGED_BY)
    //modif agi 2017-08-07
        public string UpdateData(string pBUYER_PD_CD, string pCUSTOMER_NO, string pDESCRIPTION, string pDELETION_FLAG, string pCHANGED_BY, string pDESTINATION_CODE, string pCOUNTRY_CD, string pCOUNTRY_CD_DESC, string pPRIVILEGE, string pCASE_LABEL_CD) 
        {
                string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mBuyerPD>("BuyerPD_Update", new
                {
                    BUYER_PD_CD = pBUYER_PD_CD,
                    CUSTOMER_NO = pCUSTOMER_NO,
                    DESCRIPTION = pDESCRIPTION,
                    DELETION_FLAG = pDELETION_FLAG,
                    CHANGED_BY = pCHANGED_BY,
                    MSG_TEXT = "",
                    //add agi 2017-08-07
                    DESTINATION_CODE = pDESTINATION_CODE,
                    COUNTRY_CD = pCOUNTRY_CD,
                    COUNTRY_CD_DESC = pCOUNTRY_CD_DESC,
                    PRIVILEGE = pPRIVILEGE,
                    CASE_LABEL_CD = pCASE_LABEL_CD
                    //end add

                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public List<string> GetPrivilege()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<string>("BuyerPD_GetPrevilage").ToList();
            db.Close();
            return l;
        }

        public List<mBuyerPD> GetAllBuyerPD()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mBuyerPD>("BuyerPD_GetAllDescription").ToList();
            db.Close();
            return l;
        }

        //add agi 2017-08-15
        public List<mBuyerPD> GetAllBuyerPDNoDelete()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mBuyerPD>("BuyerPD_GetAllDescriptionNoDelete").ToList();
            db.Close();
            return l;
        }

        public List<mSystemMaster> GetDataBySystemMaster(string System_Type, string System_Code)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<mSystemMaster> result = db.Fetch<mSystemMaster>("BuyerPd_GetSystemMaster",
                 new { System_Type = System_Type },
                 new { System_Code = System_Code }
                ).ToList();
            db.Close();
            return result;
        }
    }
}
