﻿using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mExportOrderHeaderMaster
    {
        public string PD_CD { get; set; }
        public string BUYER_CD { get; set; }
        public string ORDER_NO { get; set; }
        public string ORDER_DT { get; set; }
        public string TRANSPORTATION_CD { get; set; }
        public string ORDER_TYPE { get; set; }
        public string BO_INSTRUCTION_CD { get; set; }
        public string PAYMENT_TERM_CD { get; set; }
        public string CURRENCY_CD { get; set; }
        public string SELLER_CD { get; set; }
        public string TNSO_FILE_CD { get; set; }
        public string PRICE_TERM_CD { get; set; }
        public string ORIGINAL_BUYER_CD { get; set; }

        //Calling data search and loaded (by QUERY)
        public List<mExportOrderHeaderMaster> getListExportOrderHeaderMaster(string pPDCode, string pBuyerCode, string pOrderNo, string pOrderDateFrom, string pOrderDateTo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext ();

            var list = db.Fetch<mExportOrderHeaderMaster>("GetExportOrderHeaderMaster",
                new object[] { pPDCode, pBuyerCode, pOrderNo, pOrderDateFrom, pOrderDateTo });
            db.Close();
            return list.ToList();
        }

        //Get data for download
        public List<mExportOrderHeaderMaster> ExportOrderHeaderDownload(string D_PDCode, string D_BuyerCode, string D_OrderNo, string D_OrderDateFrom, string D_OrderDateTo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var list = db.Fetch<mExportOrderHeaderMaster>("GetExportOrderHeaderMaster",
                new object[] { D_PDCode, D_BuyerCode, D_OrderNo, D_OrderDateFrom, D_OrderDateTo });
            db.Close();
            return list.ToList();
        }


        //Get data for combobox
        public List<mExportOrderHeaderMaster> getListPDCode()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var list = db.Fetch<mExportOrderHeaderMaster>("GetPDCode");
            db.Close();
            return list.ToList();
        }
        //Get data for combobox
        public List<mExportOrderHeaderMaster> getListBuyerCode()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var list = db.Fetch<mExportOrderHeaderMaster>("GetBuyerCode");
            db.Close();
            return list.ToList();
        }
    }
}
