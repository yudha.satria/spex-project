﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class PipeLineInquiry
    {
        public string BUYER_PD { get; set; }
        public string ORDER_TYPE_DESCRIPTION { get; set; }
        public string TRANSPORTATION_CD { get; set; }
        public string TRANSPORTATION_DESC { get; set; }
        public string STATUS_CD { get; set; }
        public string STATUS_DESC { get; set; }
        public string PROCESS_CD { get; set; }
        public string PROCESS_DESC { get; set; }

        public string PK { get; set; }
        public DateTime? TMAP_ORDER_DT { get; set; }
        public string BUYERPD_CD { get; set; }
        public string ORDER_TYPE { get; set; }
        public DateTime? TMMIN_REC_DT { get; set; }
        public string ORDER_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public decimal ACCEPT_QTY { get; set; }

        //Manifest
        public string MF_ARRIVAL_ACTUAL_DT { get; set; }
        public string MF_ARRIVAL_PLAN_DT { get; set; }
        public Decimal MF_RECEIVE_QTY { get; set; }
        public string MF_RECEIVE_STATUS { get; set; }
        public Int32 MF_RECEIVE_DELAY { get; set; }

        public Decimal ALLOCATE_QTY { get; set; }

        public Decimal PICKING_QTY { get; set; }

        //Receive
        public Int32 RECEIVE_QTY { get; set; }
        public string RECEIVE_STATUS { get; set; }
        public Int32 RECEIVE_DELAY { get; set; }
        public string RECEIVE_RECEIVE_DT { get; set; }
        public string RECEIVE_ARRIVAL_PLAN_DT { get; set; }

        //Packing
        public Decimal PACKING_QTY { get; set; }
        public string PACKING_STATUS { get; set; }
        public Int32 PACKING_DELAY { get; set; }

        //Vanning
        public Decimal VANNING_QTY { get; set; }
        public string VANNING_STATUS { get; set; }
        public Int32 VANNING_DELAY { get; set; }


        public Decimal SHIPMENT_QTY { get; set; }
        public string SHIPMENT_STATUS { get; set; }
        public Int32 SHIPMENT_DELAY { get; set; }
        public DateTime? ETD { get; set; }

        public string KANBAN_ID { get; set; }
        public DateTime? MF_RECEIVE_DT { get; set; }
        public string RCV_QTY { get; set; }
        public DateTime? RECEIVE_DT { get; set; }
        public string CASE_NO { get; set; }
        public DateTime? PACKING_DT { get; set; }
        public string CONTAINER_NO { get; set; }
        public DateTime? VANNING_DT { get; set; }
        public string INVOICE_NO { get; set; }
        public DateTime? INVOICE_DT { get; set; }
        public DateTime? SHIPMENT_DT { get; set; }

        public string TMMIN_SEND_DT { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string STOCK_FLAG { get; set; }




        public string ACTUAL_SHIPMENT_DT { get; set; }
        public string ACTUAL_MF_RECEIVE_DT { get; set; }
        public string ACTUAL_RECEIVE_DT { get; set; }
        public string ACTUAL_PACKING_DT { get; set; }
        public string ACTUAL_VANNING_DT { get; set; }

        //2018-12-12
        //public string ACTUAL_ALL_PLAN_DT { get; set; }
        public string ACTUAL_MF_RECEIVE_PLAN_DT { get; set; }
        public string ACTUAL_SHIPMENT_PLAN_DT { get; set; }
        public string ACTUAL_RECEIVE_PLAN_DT { get; set; }
        public string ACTUAL_PACKING_PLAN_DT { get; set; }
        public string ACTUAL_VANNING_PLAN_DT { get; set; }
        public string PICKING_DT { get; set; }

        public string MANIFEST_NO { get; set; }
        public string PACKING_PLAN_DT { get; set; }
        public string VANNING_PLAN_DT { get; set; }
        public string SHIPMENT_PLAN_DT { get; set; }




        //public string TMMINDtTxt
        //{
        //    get
        //    {
        //        return GetTxtDate(TMMIN_SEND_DT);
        //    }
        //}

        public string TmapOrderDtTxt
        {
            get
            {
                return GetTxtDate(TMAP_ORDER_DT);
            }
        }

        public string TmapEtdTxt
        {
            get
            {
                return GetTxtDate(ETD);
            }
        }

        public string TmapRcvDtTxt
        {
            get
            {
                return GetTxtDate(RECEIVE_DT);
            }
        }

        public string TmapPckDtTxt
        {
            get
            {
                return GetTxtDate(PACKING_DT);
            }
        }

        public string TmapVanDtTxt
        {
            get
            {
                return GetTxtDate(VANNING_DT);
            }
        }

        public string TmapInvDtTxt
        {
            get
            {
                return GetTxtDate(INVOICE_DT);
            }
        }

        public string TmapShpDtTxt
        {
            get
            {
                return GetTxtDate(SHIPMENT_DT);
            }
        }

        public string TmapMFRcvDtTxt
        {
            get
            {
                return GetTxtDate(MF_RECEIVE_DT);
            }
        }

        private string GetTxtDate(DateTime? d)
        {
            string r = "";
            if (d.HasValue)
            {
                r = d.Value.Day.ToString().PadLeft(2, '0') + "." + d.Value.Month.ToString().PadLeft(2, '0') + "." + d.Value.Year.ToString();
            }
            return r;
        }

        public List<PipeLineInquiry> getList(string pOrderNo, string pBuyerPdCd, string pOrderDtFrom, string pOrderDtTo, string pItemNo, string pOrderType
            , string pEdtFrom, string pEdtTo, string pPartNo, string pTransportationCd, string pStatus, string pStockFlag, string pTMMINdate, string pTMMINdateTo, string pProcess)
        {
            List<PipeLineInquiry> PI = new List<PipeLineInquiry>();
            /*DataTable dt = new DataTable();
            dt.Columns.Add("ORDER_NO");
            dt.Columns.Add("ITEM_NO");
            dt.Columns.Add("PART_NO");
            dt.Columns.Add("BUYERPD_CD");
            dt.Columns.Add("ORDER_TYPE");
            dt.Columns.Add("TRANSPORTATION_CD");
            dt.Columns.Add("ORDER_DT_FROM");
            dt.Columns.Add("ORDER_DT_TO");
            dt.Columns.Add("ETD_FROM");
            dt.Columns.Add("ETD_TO");
            dt.Columns.Add("STATUS");
            dt.Columns.Add("PROCESS");
           
            DataRow dr = dt.NewRow();
            dr["ORDER_NO"] = pOrderNo;
            dr["ITEM_NO"] = pItemNo;
            dr["PART_NO"] = pPartNo;
            dr["BUYERPD_CD"] = pBuyerPdCd;
            dr["ORDER_TYPE"] = pOrderType;
            dr["TRANSPORTATION_CD"] = pTransportationCd;
            dr["ORDER_DT_FROM"] = pOrderDtFrom;
            dr["ORDER_DT_TO"] = pOrderDtTo;
            dr["ETD_FROM"] = pEdtFrom;
            dr["ETD_TO"] = pEdtTo;
            dr["STATUS"] = pStatus;
            dr["PROCESS"] = pProcess;
            dt.Rows.Add(dr);
            */
            /*IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<PipeLineInquiry>("PipeLineInquiry_GetList",
                  new { ORDER_NO = pOrderNo }
                  , new { ITEM_NO = pItemNo }
                  , new { PART_NO = pPartNo }
                  , new { BUYER_PD = pBuyerPdCd }
                  , new { ORDER_TYPE = pOrderType } 
                  , new { TRANSPORTATION_CD = pTransportationCd }
                  , new { ORDER_DT_FROM = pOrderDtFrom }
                  , new { ORDER_DT_TO = pOrderDtTo }
                  , new { ETD_FROM = pEdtFrom }
                  , new { ETD_TO = pEdtTo }
                  , new { STATUS = pStatus }
                  , new { PROCESS = pProcess }
                  );
            db.Close();
            */
            using (SqlConnection conn = new SqlConnection(DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("[spex].[SP_PIPELINE_INQUIRY_SEARCH]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                SqlParameter p = cmd.Parameters.AddWithValue("@ORDER_NO", pOrderNo);
                p = cmd.Parameters.AddWithValue("@ITEM_NO", pItemNo);
                p = cmd.Parameters.AddWithValue("@PART_NO", pPartNo);
                p = cmd.Parameters.AddWithValue("@BUYER_PD", pBuyerPdCd);
                p = cmd.Parameters.AddWithValue("@ORDER_TYPE", pOrderType);
                p = cmd.Parameters.AddWithValue("@TRANSPORTATION_CD", pTransportationCd);
                p = cmd.Parameters.AddWithValue("@ORDER_DT_FROM", pOrderDtFrom);
                p = cmd.Parameters.AddWithValue("@ORDER_DT_TO", pOrderDtTo);
                p = cmd.Parameters.AddWithValue("@ETD_FROM", pEdtFrom);
                p = cmd.Parameters.AddWithValue("@ETD_TO", pEdtTo);
                p = cmd.Parameters.AddWithValue("@STATUS", pStatus);
                p = cmd.Parameters.AddWithValue("@PROCESS", pProcess);
                p = cmd.Parameters.AddWithValue("@TMMIN_RELEASE_FROM", pTMMINdate);
                p = cmd.Parameters.AddWithValue("@TMMIN_RELEASE_TO", pTMMINdateTo);
                p = cmd.Parameters.AddWithValue("@STOCK_FLAG", pStockFlag);
                p.SqlDbType = SqlDbType.VarChar;
                try
                {
                    conn.Open();
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            PipeLineInquiry newItem = new PipeLineInquiry();
                            if (!dr.IsDBNull(0))
                                newItem.PK = dr.GetString(0);
                            if (!dr.IsDBNull(1))
                                newItem.TMMIN_SEND_DT = dr.GetString(1);
                            if (!dr.IsDBNull(2))
                                newItem.SUPPLIER_CD = dr.GetString(2);
                            if (!dr.IsDBNull(3))
                                newItem.SUB_SUPPLIER_CD = dr.GetString(3);
                            if (!dr.IsDBNull(4))
                                newItem.TMAP_ORDER_DT = (DateTime)dr.GetDateTime(4);
                            if (!dr.IsDBNull(5))
                                newItem.BUYERPD_CD = dr.GetString(5);
                            if (!dr.IsDBNull(6))
                                newItem.ORDER_TYPE = dr.GetString(6);
                            if (!dr.IsDBNull(7))
                                newItem.TRANSPORTATION_CD = dr.GetString(7);
                            if (!dr.IsDBNull(8))
                                newItem.TMMIN_REC_DT = (DateTime)dr.GetDateTime(8);
                            if (!dr.IsDBNull(9))
                                newItem.ORDER_NO = dr.GetString(9);
                            if (!dr.IsDBNull(10))
                                newItem.ITEM_NO = dr.GetString(10);
                            if (!dr.IsDBNull(11))
                                newItem.PART_NO = dr.GetString(11);
                            if (!dr.IsDBNull(12))
                                newItem.ACCEPT_QTY = (Decimal)dr.GetDecimal(12);
                            if (!dr.IsDBNull(13))
                                newItem.MF_RECEIVE_QTY = (Decimal)dr.GetDecimal(13);
                            var A = dr.GetDecimal(14);

                            if (!dr.IsDBNull(14))
                                newItem.MF_RECEIVE_STATUS = dr.GetString(14);
                            if (!dr.IsDBNull(15))
                                newItem.MF_RECEIVE_DELAY = dr.GetInt32(15);
                            if (!dr.IsDBNull(16))
                                newItem.ALLOCATE_QTY = (Decimal)dr.GetDecimal(16);
                            if (!dr.IsDBNull(17))
                                newItem.PICKING_QTY = dr.GetInt32(17);
                            if (!dr.IsDBNull(18))
                                newItem.RECEIVE_QTY = dr.GetInt32(18);
                            if (!dr.IsDBNull(19))
                                newItem.RECEIVE_STATUS = dr.GetString(19);
                            if (!dr.IsDBNull(20))
                                newItem.RECEIVE_DELAY = dr.GetInt32(20);
                            if (!dr.IsDBNull(21))
                                newItem.PACKING_QTY = (Decimal)dr.GetDecimal(21);
                            if (!dr.IsDBNull(22))
                                newItem.PACKING_STATUS = dr.GetString(22);
                            if (!dr.IsDBNull(23))
                                newItem.PACKING_DELAY = dr.GetInt32(23);
                            if (!dr.IsDBNull(24))
                                newItem.VANNING_QTY = (Decimal)dr.GetDecimal(24);
                            if (!dr.IsDBNull(25))
                                newItem.VANNING_STATUS = dr.GetString(25);
                            if (!dr.IsDBNull(26))
                                newItem.VANNING_DELAY = dr.GetInt32(26);
                            if (!dr.IsDBNull(27))
                                newItem.SHIPMENT_QTY = (Decimal)dr.GetDecimal(27);
                            if (!dr.IsDBNull(28))
                                newItem.SHIPMENT_STATUS = dr.GetString(28);
                            if (!dr.IsDBNull(29))
                                newItem.SHIPMENT_DELAY = dr.GetInt32(29);
                            if (!dr.IsDBNull(30))
                                newItem.ETD = (DateTime)dr.GetDateTime(30);
                            PI.Add(newItem);
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return PI;
        }


        //change agi 2017-11-14
        //public string getPIDDownload(string pOrderNo, string pBuyerPdCd, string pOrderDtFrom, string pOrderDtTo, string pItemNo, string pOrderType
        //    , string pEdtFrom, string pEdtTo, string pPartNo, string pTransportationCd, string pStatus, string pProcess)
        public string getPIDDownload(string pOrderNo, string pBuyerPdCd, string pOrderDtFrom, string pOrderDtTo, string pItemNo, string pOrderType
                , string pEdtFrom, string pEdtTo, string pPartNo, string pTransportationCd, string pStatus, string pStockFlag, string pTMMINdate, string pTMMINdateTo, string pProcess, string UserId)
        {
            string pid = "";
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.SetCommandTimeout(900);

            var l = db.Fetch<string>("PipeLineInquiry_GetDownloadByTick",
                  new { ORDER_NO = pOrderNo }
                  , new { ITEM_NO = pItemNo }
                  , new { PART_NO = pPartNo }
                  , new { BUYER_PD = pBuyerPdCd }
                  , new { ORDER_TYPE = pOrderType }
                  , new { TRANSPORTATION_CD = pTransportationCd }
                  , new { ORDER_DT_FROM = pOrderDtFrom }
                  , new { ORDER_DT_TO = pOrderDtTo }
                  , new { ETD_FROM = pEdtFrom }
                  , new { ETD_TO = pEdtTo }
                  , new { STATUS = pStatus }
                  , new { PROCESS = pProcess }
                  , new { USER_ID = UserId }
                  , new { TMMIN_RELEASE_FROM = pTMMINdate }
                  , new { TMMIN_RELEASE_TO = pTMMINdateTo }
                  , new { STOCK_FLAG = pStockFlag }
                  );
            db.Close();
            return l.FirstOrDefault();
        }

        public List<PipeLineInquiryDownloadDetail> getDownloadByPID(string pid)
        {

            List<PipeLineInquiryDownloadDetail> PI = new List<PipeLineInquiryDownloadDetail>();

            using (SqlConnection conn = new SqlConnection(DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("[spex].[SP_PIPELINE_INQUIRY_DOWNLOAD_BY_PID]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                SqlParameter p = cmd.Parameters.AddWithValue("@PROCESS_ID", pid);
                p.SqlDbType = SqlDbType.VarChar;
                try
                {
                    conn.Open();
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            PipeLineInquiryDownloadDetail newItem = new PipeLineInquiryDownloadDetail();

                            if (!dr.IsDBNull(1))
                                newItem.TMAP_ORDER_DT = dr.GetString(1);
                            //if (!dr.IsDBNull(1))
                            //    newItem.ORDER_NO = dr.GetString(2);
                            if (!dr.IsDBNull(3))
                                newItem.BUYERPD_CD = dr.GetString(3);
                            if (!dr.IsDBNull(4))
                                newItem.ORDER_TYPE = dr.GetString(4);
                            if (!dr.IsDBNull(5))
                                newItem.TRANSPORTATION_CD = dr.GetString(5);
                            if (!dr.IsDBNull(7))
                                newItem.ORDER_NO = dr.GetString(7);
                            if (!dr.IsDBNull(8))
                                newItem.ITEM_NO = dr.GetString(8);
                            if (!dr.IsDBNull(9))
                                newItem.PART_NO = dr.GetString(9);
                            if (!dr.IsDBNull(10))
                                newItem.ACCEPT_QTY = (Decimal)dr.GetDecimal(10);
                            if (!dr.IsDBNull(11))
                                newItem.MF_RECEIVE_QTY = (int)dr.GetDecimal(11);
                            if (!dr.IsDBNull(12))
                                newItem.MF_RECEIVE_DT = dr.GetString(12);
                            if (!dr.IsDBNull(13))
                                newItem.ALLOCATE_QTY = (int)dr.GetDecimal(13);
                            if (!dr.IsDBNull(46))
                                newItem.PICKING_DT = dr.GetString(46);  //(DateTime)dr.GetDateTime(46);
                            if (!dr.IsDBNull(14))
                                newItem.PICKING_QTY = (int)dr.GetDecimal(14);
                            if (!dr.IsDBNull(15))
                                newItem.RECEIVE_QTY = (int)dr.GetDecimal(15);
                            if (!dr.IsDBNull(16))
                                newItem.RECEIVE_DT = dr.GetString(16);
                            if (!dr.IsDBNull(17))
                                newItem.CASE_NO = dr.GetString(17);
                            if (!dr.IsDBNull(18))
                                newItem.PACKING_DT = dr.GetString(18);
                            if (!dr.IsDBNull(19))
                                newItem.CONTAINER_NO = dr.GetString(19);
                            if (!dr.IsDBNull(20))
                                newItem.VANNING_DT = dr.GetString(20);
                            if (!dr.IsDBNull(21))
                                newItem.INVOICE_NO = dr.GetString(21);
                            if (!dr.IsDBNull(22))
                                newItem.INVOICE_DT = dr.GetString(22);
                            if (!dr.IsDBNull(23))
                                newItem.SHIPMENT_DT = dr.GetString(23);
                            if (!dr.IsDBNull(24))
                                newItem.KANBAN_ID = dr.GetString(24);
                            if (!dr.IsDBNull(25))
                                newItem.MF_RECEIVE_STATUS = dr.GetString(25);
                            if (!dr.IsDBNull(26))
                                newItem.MF_RECEIVE_DELAY = (int)dr.GetInt32(26);
                            if (!dr.IsDBNull(27))
                                newItem.RECEIVE_STATUS = dr.GetString(27);
                            if (!dr.IsDBNull(28))
                                newItem.RECEIVE_DELAY = (int)dr.GetInt32(28);
                            if (!dr.IsDBNull(29))
                                newItem.PACKING_STATUS = dr.GetString(29);
                            if (!dr.IsDBNull(30))
                                newItem.PACKING_DELAY = (int)dr.GetInt32(30);
                            if (!dr.IsDBNull(31))
                                newItem.VANNING_STATUS = dr.GetString(31);
                            if (!dr.IsDBNull(32))
                                newItem.VANNING_DELAY = (int)dr.GetInt32(32);
                            if (!dr.IsDBNull(33))
                                newItem.SHIPMENT_STATUS = dr.GetString(33);
                            if (!dr.IsDBNull(34))
                                newItem.SHIPMENT_DELAY = (int)dr.GetInt32(34);
                            if (!dr.IsDBNull(35))
                                newItem.ETD = dr.GetString(35);
                            if (!dr.IsDBNull(36))
                                newItem.PACKING_QTY = (decimal)dr.GetInt32(36);
                            if (!dr.IsDBNull(37))
                                newItem.VANNING_QTY = (decimal)dr.GetInt32(37);
                            if (!dr.IsDBNull(38))
                                newItem.SHIPMENT_QTY = (decimal)dr.GetInt32(38);
                            if (!dr.IsDBNull(39))
                                newItem.TMMIN_SEND_DT = dr.GetString(39);
                            if (!dr.IsDBNull(40))
                                newItem.SUPPLIER_CD = dr.GetString(40);
                            if (!dr.IsDBNull(41))
                                newItem.SUB_SUPPLIER_CD = dr.GetString(41);
                            if (!dr.IsDBNull(42))
                                newItem.MANIFEST_NO = dr.GetString(42);
                            if (!dr.IsDBNull(43))
                                newItem.PACKING_PLAN_DT = dr.GetString(43);
                            if (!dr.IsDBNull(44))
                                newItem.VANNING_PLAN_DT = dr.GetString(44);
                            if (!dr.IsDBNull(45))
                                newItem.SHIPMENT_PLAN_DT = dr.GetString(45);
                            if (!dr.IsDBNull(47))
                                newItem.RECEIVE_PLAN_DT = dr.GetString(47);

                            PI.Add(newItem);
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return PI;
        }

        public List<PipeLineInquiry> getDownloadByTick(string pOrderNo, string pBuyerPdCd, string pOrderDtFrom, string pOrderDtTo, string pItemNo, string pOrderType
            , string pEdtFrom, string pEdtTo, string pPartNo, string pTransportationCd, string pStatus, string pStockFlag, string pTMMINdate, string pTMMINdateTo, string pProcess)
        {
            List<PipeLineInquiry> PI = new List<PipeLineInquiry>();
            /* IDBContext db = DatabaseManager.Instance.GetContext();

             var l = db.Fetch<PipeLineInquiry>("PipeLineInquiry_GetDownloadByTick",
                   new { ORDER_NO = pOrderNo }
                   , new { ITEM_NO = pItemNo }
                   , new { PART_NO = pPartNo }
                   , new { BUYER_PD = pBuyerPdCd }
                   , new { ORDER_TYPE = pOrderType }
                   , new { TRANSPORTATION_CD = pTransportationCd }
                   , new { ORDER_DT_FROM = pOrderDtFrom }
                   , new { ORDER_DT_TO = pOrderDtTo }
                   , new { ETD_FROM = pEdtFrom }
                   , new { ETD_TO = pEdtTo }
                   , new { STATUS = pStatus }
                   , new { PROCESS = pProcess }
                   );
             db.Close();
             return l.ToList();*/
            using (SqlConnection conn = new SqlConnection(DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("[spex].[SP_PIPELINE_INQUIRY_DOWNLOAD_BY_TICK]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                SqlParameter p = cmd.Parameters.AddWithValue("@ORDER_NO", pOrderNo);
                p = cmd.Parameters.AddWithValue("@ITEM_NO", pItemNo);
                p = cmd.Parameters.AddWithValue("@PART_NO", pPartNo);
                p = cmd.Parameters.AddWithValue("@BUYER_PD", pBuyerPdCd);
                p = cmd.Parameters.AddWithValue("@ORDER_TYPE", pOrderType);
                p = cmd.Parameters.AddWithValue("@TRANSPORTATION_CD", pTransportationCd);
                p = cmd.Parameters.AddWithValue("@ORDER_DT_FROM", pOrderDtFrom);
                p = cmd.Parameters.AddWithValue("@ORDER_DT_TO", pOrderDtTo);
                p = cmd.Parameters.AddWithValue("@ETD_FROM", pEdtFrom);
                p = cmd.Parameters.AddWithValue("@ETD_TO", pEdtTo);
                p = cmd.Parameters.AddWithValue("@STATUS", pStatus);
                p = cmd.Parameters.AddWithValue("@PROCESS", pProcess);
                p.SqlDbType = SqlDbType.VarChar;
                try
                {
                    conn.Open();
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        //--- Return from procedures is -> doc, docTo, docTembusan, docApprover, docAttachment
                        int i = 0;


                        while (dr.Read())
                        {
                            //FINANCE_REPORT newItem = new FINANCE_REPORT();
                            PipeLineInquiry newItem = new PipeLineInquiry();

                            if (!dr.IsDBNull(1))
                                newItem.TMAP_ORDER_DT = (DateTime)dr.GetDateTime(1);
                            if (!dr.IsDBNull(3))
                                newItem.BUYERPD_CD = dr.GetString(3);
                            if (!dr.IsDBNull(4))
                                newItem.ORDER_TYPE = dr.GetString(4);
                            if (!dr.IsDBNull(5))
                                newItem.TRANSPORTATION_CD = dr.GetString(5);
                            if (!dr.IsDBNull(7))
                                newItem.ORDER_NO = dr.GetString(7);
                            if (!dr.IsDBNull(8))
                                newItem.ITEM_NO = dr.GetString(8);
                            if (!dr.IsDBNull(9))
                                newItem.PART_NO = dr.GetString(9);
                            if (!dr.IsDBNull(10))
                                newItem.ACCEPT_QTY = (Decimal)dr.GetDecimal(10);
                            if (!dr.IsDBNull(20))
                                newItem.KANBAN_ID = dr.GetString(20);
                            if (!dr.IsDBNull(12))
                                newItem.RECEIVE_DT = (DateTime)dr.GetDateTime(12);
                            if (!dr.IsDBNull(13))
                                newItem.CASE_NO = dr.GetString(13);
                            if (!dr.IsDBNull(14))
                                newItem.PACKING_DT = (DateTime)dr.GetDateTime(14);
                            if (!dr.IsDBNull(15))
                                newItem.CONTAINER_NO = dr.GetString(15);
                            if (!dr.IsDBNull(16))
                                newItem.VANNING_DT = (DateTime)dr.GetDateTime(16);
                            if (!dr.IsDBNull(17))
                                newItem.INVOICE_NO = dr.GetString(17);
                            if (!dr.IsDBNull(18))
                                newItem.INVOICE_DT = (DateTime)dr.GetDateTime(18);
                            if (!dr.IsDBNull(19))
                                newItem.SHIPMENT_DT = (DateTime)dr.GetDateTime(19);
                            PI.Add(newItem);
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return PI;
        }

        public List<mSystemMaster> GetOBRStatusList()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();

            datas = (new mSystemMaster()).GetSystemValueList("OBR_STATUS", "", "2");
            return datas;
        }

        public string Add(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT, string pDESCRIPTION, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("Case_Add",
                  new
                  {
                      CASE_TYPE = pCASE_TYPE,
                      CASE_NET_WEIGHT = pCASE_NET_WEIGHT,
                      CASE_LENGTH = pCASE_LENGTH,
                      CASE_WIDTH = pCASE_WIDTH,
                      CASE_HEIGHT = pCASE_HEIGHT,
                      DESCRIPTION = pDESCRIPTION,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }

        public string Update(string pOBR_NO, decimal? pCANCEL_QTY, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("OBR_Update",
                  new
                  {
                      OBR_NO = pOBR_NO,
                      CANCEL_QTY = pCANCEL_QTY,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }


        public string UpdateStatus(string pOBR_NOS, string pStatusID, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("OBR_Update_Status",
                  new
                  {
                      OBR_NOS = pOBR_NOS,
                      STATUS_ID = pStatusID,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }

        public string DeleteTempData()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mVanningCompletion>("VanningCompletion_DeleteTemp");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public String GetRangeOrderDate()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.SingleOrDefault<String>("PipeLineInquiry_GetRangeOrderDate",
                  new { }
                  );
            db.Close();

            return l;
        }

        public List<String> GetBuyerMasterList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<String>("GetListBuyerPD",
                  new { }
                  );
            db.Close();

            return l.ToList();
        }

        public List<PipeLineInquiry> GetTransportationMasterList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<PipeLineInquiry>("PipeLineInquiry_Transportation", new { });
            db.Close();

            return l.ToList();
        }

        public List<String> GetOrderTypeMasterList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<String>("PipeLineInquiry_OrderType",
                  new { }
                  );
            db.Close();

            return l.ToList();
        }

        //public List<PipeLineInquiry> GetStatusList()
        //{
        //    List<PipeLineInquiry> status = new List<PipeLineInquiry>();
        //    status.Add(new PipeLineInquiry { STATUS_CD = "", STATUS_DESC = "" });
        //    status.Add(new PipeLineInquiry { STATUS_CD="0",STATUS_DESC="On Time"});
        //    status.Add(new PipeLineInquiry { STATUS_CD = "1", STATUS_DESC = "In Progress" });
        //    status.Add(new PipeLineInquiry { STATUS_CD = "2", STATUS_DESC = "Delay" });
        //    return status.ToList();
        //}

        //public List<PipeLineInquiry> GetProcessList() {
        //    List<PipeLineInquiry> process = new List<PipeLineInquiry>();
        //    process.Add(new PipeLineInquiry { PROCESS_CD = "", PROCESS_DESC = "" });
        //    process.Add(new PipeLineInquiry { PROCESS_CD = "1", PROCESS_DESC = "Receiving" });
        //    process.Add(new PipeLineInquiry { PROCESS_CD = "2", PROCESS_DESC = "Packing" });
        //    process.Add(new PipeLineInquiry { PROCESS_CD = "3", PROCESS_DESC = "Vanning" });
        //    process.Add(new PipeLineInquiry { PROCESS_CD = "4", PROCESS_DESC = "Shipment" });
        //    return process.ToList();
        //}

        public String checkSystemMaster()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.SingleOrDefault<String>("PipeLineInquiry_checkSystemMaster",
                  new { }
                  );
            db.Close();

            return l;
        }

        public String linkQty(string pOrderNo, string pItemNo, string pPartNo, string linkType)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.SingleOrDefault<String>("PipeLineInquiry_GetDate",
                  new { ORDER_NO = pOrderNo }
                  , new { ITEM_NO = pItemNo }
                  , new { PART_NO = pPartNo }
                  , new { LINK_TYPE = linkType }
                  );
            db.Close();

            return l;
        }

        public List<mSystemMaster> GetComboBoxTranpCode()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetComboBoxTranpCode").ToList();
            db.Close();
            return datas;
        }

        public List<mSystemMaster> GetComboBoxBuyerPD()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetListBuyerPDforPipeLine").ToList();
            db.Close();
            return datas;
        }

        public List<mSystemMaster> GetComboBoxOrderType()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetListBuyerPipeLineOrderType").ToList();
            db.Close();
            return datas;
        }


        public List<PipeLineInquiry> getListPipeLineInquiry(string pOrderNo, string pBuyerPdCd, string pOrderDtFrom, string pOrderDtTo,
                                            string pItemNo, string pOrderType, string pEdtFrom, string pEdtTo, string pPartNo, string pTransportationCd,
                                            string pStatus, string pStockFlag, string pTMMINdate, string pTMMINdateTo, string pProcess)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.SetCommandTimeout(1800);
            var l = db.Fetch<PipeLineInquiry>("SearchPipeLineInquiry",
                    new { ORDER_NO = pOrderNo },
                    new { ITEM_NO = pItemNo },
                    new { PART_NO = pPartNo },
                    new { BUYER_PD = pBuyerPdCd },
                    new { ORDER_TYPE = pOrderType },
                    new { TRANSPORTATION_CD = pTransportationCd },
                    new { ORDER_DT_FROM = pOrderDtFrom },
                    new { ORDER_DT_TO = pOrderDtTo },
                    new { ETD_FROM = pEdtFrom },
                    new { ETD_TO = pEdtTo },
                    new { STATUS = pStatus },
                    new { PROCESS = pProcess },
                    new { TMMIN_RELEASE_FROM = pTMMINdate },
                    new { TMMIN_RELEASE_TO = pTMMINdateTo },
                    new { STOCK_FLAG = pStockFlag });
            db.Close();
            return l.ToList();
        }

    }



    public class PipeLineInquiryDownloadDetail
    {
        public string BUYER_PD { get; set; }
        public string ORDER_TYPE_DESCRIPTION { get; set; }
        public string TRANSPORTATION_CD { get; set; }
        public string TRANSPORTATION_DESC { get; set; }
        public string STATUS_CD { get; set; }
        public string STATUS_DESC { get; set; }
        public string PROCESS_CD { get; set; }
        public string PROCESS_DESC { get; set; }

        public string PK { get; set; }
        public string TMAP_ORDER_DT { get; set; }
        public string BUYERPD_CD { get; set; }
        public string ORDER_TYPE { get; set; }
        public string TMMIN_REC_DT { get; set; }
        public string ORDER_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public decimal ACCEPT_QTY { get; set; }

        //Manifest
        public string MF_ARRIVAL_ACTUAL_DT { get; set; }
        public string MF_ARRIVAL_PLAN_DT { get; set; }
        public Decimal MF_RECEIVE_QTY { get; set; }
        public string MF_RECEIVE_STATUS { get; set; }
        public Int32 MF_RECEIVE_DELAY { get; set; }

        public Decimal ALLOCATE_QTY { get; set; }

        public Decimal PICKING_QTY { get; set; }

        //Receive
        public Int32 RECEIVE_QTY { get; set; }
        public string RECEIVE_STATUS { get; set; }
        public Int32 RECEIVE_DELAY { get; set; }
        public string RECEIVE_RECEIVE_DT { get; set; }
        public string RECEIVE_ARRIVAL_PLAN_DT { get; set; }

        //Packing
        public Decimal PACKING_QTY { get; set; }
        public string PACKING_STATUS { get; set; }
        public Int32 PACKING_DELAY { get; set; }

        //Vanning
        public Decimal VANNING_QTY { get; set; }
        public string VANNING_STATUS { get; set; }
        public Int32 VANNING_DELAY { get; set; }


        public Decimal SHIPMENT_QTY { get; set; }
        public string SHIPMENT_STATUS { get; set; }
        public Int32 SHIPMENT_DELAY { get; set; }
        public string ETD { get; set; }

        public string KANBAN_ID { get; set; }
        public string MF_RECEIVE_DT { get; set; }
        public string RCV_QTY { get; set; }
        public string RECEIVE_DT { get; set; }
        public string CASE_NO { get; set; }
        public string PACKING_DT { get; set; }
        public string CONTAINER_NO { get; set; }
        public string VANNING_DT { get; set; }
        public string INVOICE_NO { get; set; }
        public string INVOICE_DT { get; set; }
        public string SHIPMENT_DT { get; set; }

        public string TMMIN_SEND_DT { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string STOCK_FLAG { get; set; }




        public string ACTUAL_SHIPMENT_DT { get; set; }
        public string ACTUAL_MF_RECEIVE_DT { get; set; }
        public string ACTUAL_RECEIVE_DT { get; set; }
        public string ACTUAL_PACKING_DT { get; set; }
        public string ACTUAL_VANNING_DT { get; set; }

        //2018-12-12
        //public string ACTUAL_ALL_PLAN_DT { get; set; }
        public string ACTUAL_MF_RECEIVE_PLAN_DT { get; set; }
        public string ACTUAL_SHIPMENT_PLAN_DT { get; set; }
        public string ACTUAL_RECEIVE_PLAN_DT { get; set; }
        public string ACTUAL_PACKING_PLAN_DT { get; set; }
        public string ACTUAL_VANNING_PLAN_DT { get; set; }
        public string PICKING_DT { get; set; }

        public string MANIFEST_NO { get; set; }
        public string PACKING_PLAN_DT { get; set; }
        public string VANNING_PLAN_DT { get; set; }
        public string SHIPMENT_PLAN_DT { get; set; }
        public string RECEIVE_PLAN_DT { get; set; }

    }
}