﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPriceMaster
    {
        public string PART_NO { get; set; }
        public string FRANCHISE_CD { get; set; }
        public decimal PRICE { get; set; }
        public DateTime VALID_FROM { get; set; }
        public DateTime VALID_TO { get; set; }
        public string CURRENCY_CD { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string Text { get; set; }

        public int TOTAL_INTERFACE { get; set; }

        public List<mPriceMaster> getPartNoCombobox()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPriceMaster>("PART_NO_COMBOBOX");
            db.Close();
            return l.ToList();

        }

        //public string reFormatDate(string dt)
        //{
        //    string result = "";

        //    if (dt != "01.01.0100") result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));

        //    return result;
        //}




        public List<mPriceMaster> getListPriceMaster(string pPartNumber, string pValidFrom, string pValidTo, string pFranchCd, string pLastPrice, string pFCOnly)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mPriceMaster>("GetPriceMaster", new
            {
                PART_NO = pPartNumber,
                VALID_FROM = pValidFrom,
                VALID_TO = pValidTo,
                FRANCHISE_CD = pFranchCd,
                LAST_PRICE = pLastPrice,
                FC_ONLY = pFCOnly

            });

            if (pPartNumber == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }

        public string getPartNo(string PartNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPriceMaster>("Price_Upload_PartNo");
            db.Close();
            return null;

        }


        public string CekFranchiseCd(string PartNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mPriceMaster>("CheckFranchiseCd", new
            {
                PART_NO = PartNo

            });
            db.Close();
            return null;

        }

        public string getFranchiseCode(string FranchiseCd)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPriceMaster>("Price_Upload_Franchise");
            db.Close();
            return null;

        }



        //public string getPartNomor(string PartNo)
        //{
        //    //string l = getPartNameByCD(p_PART_NO);
        //    string PartName = "";
        //    var l = db.SP_GetPartNameByCD(PartNo).ToList();
        //    if (l.Count > 0)
        //    {
        //        PartName = Convert.ToString(l[0].PART_NO);
        //    }
        //    return PartName;
        //}


        //public string getFranchiseCd(string FranchiseCd)
        //{
        //    var l = db.SP_GetSupplierNameByCD(FranchiseCd).ToList();
        //    if (l.Count > 0)
        //    {
        //        return Convert.ToString(l[0].FRANCHISE_CD);
        //    }
        //    return "";
        //}


        //public List<mPriceMaster> getListPriceMaster(string pPartNumber, string pValidFrom, string pValidTo)
        //{
        //    IDBContext db = DatabaseManager.Instance.GetContext();

        //    var l = db.Fetch<mPriceMaster>("GetPriceMaster",

        //          new { Part_No = pPartNumber },
        //          new { Valid_From = pValidFrom },
        //          new { Valid_To = pValidTo }
        //          );
        //    db.Close();
        //    return l.ToList();
        //}

        public string reFormatDate(string dt)
        {
            string result = "";

            string[] d = dt.Split('.');
            dt = d[2] + "-" + d[1] + "-" + d[0];

            if (dt != "01.01.0100") result = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(dt));

            return result;
        }


        public string SaveData(string PartNo, string FranchiseCode, string Price, string Currency, string ValidFrom, string ValidTo, string CreatedBy)
        {
            string result = "";
            try
            {

                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPriceMaster>("Master_Price_Save", new
                {
                    p_PART_NO = PartNo,
                    p_FRANCHISE_CD = FranchiseCode,
                    p_PRICE = Price,
                    p_CURRENCY = Currency,
                    p_VALID_FROM = reFormatDate(ValidFrom),
                    p_VALID_TO = reFormatDate(ValidTo),
                    //p_VALID_FROM = ValidFrom,
                    //p_VALID_TO = ValidTo,
                    p_CREATED_BY = CreatedBy,
                    //p_CREATED_DT = CreatedDt,

                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }

            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }


        public string UpdateData(string PartNo, string FranchiseCode, string Price, string Currency, string ValidFrom, string ValidTo, string ChangedBy)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPriceMaster>("Master_Price_Update", new
                {
                    p_PART_NO = PartNo,
                    p_FRANCHISE_CD = FranchiseCode,
                    p_PRICE = Price,
                    p_CURRENCY = Currency,
                    p_VALID_FROM = reFormatDate(ValidFrom),
                    p_VALID_TO = reFormatDate(ValidTo),
                    p_CHANGED_BY = ChangedBy,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public string SaveValidation(string PartNo, string FranchiseCode, string ValidFrom)
        {
            string result = "";
            try
            {

                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPriceMaster>("Master_Price_Save_Validation", new
                {
                    p_PART_NO = PartNo,
                    p_FRANCHISE_CD = FranchiseCode,
                    p_VALID_FROM = reFormatDate(ValidFrom)
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }

            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public string DeleteData(string PartNo, string FranchiseCd, string ValidFrom)
        //public string DeleteData(string PartNumber, string FranchiseCd, string ValidFrom)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mPriceMaster>("Master_Price_Delete", new
                {
                    p_PART_NO = PartNo,
                    p_FRANCHISE_CD = FranchiseCd,
                    p_VALID_FROM = ValidFrom,

                    MSG_TEXT = ""
                });
                foreach (var message in l)
                {
                    result = message.Text;
                }


                db.Close();

            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public List<mPriceMaster> PriceMasterDownload(string D_PartNumber, string D_ValidFrom, string D_ValidTo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mPriceMaster>("Master_Price_Download", new
            {
                PART_NO = D_PartNumber,
                VALID_FROM = D_ValidFrom,
                VALID_TO = D_ValidTo

            });

            if (D_PartNumber == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }


        public string SaveUploadData(string PartNo, string FranchiseCd, decimal Price, string Currency, string ValidFrom, string ValidTo, string CreatedBy, long pid, int RowCount)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPriceMaster>("Master_Price_Upload", new
                {
                    p_PART_NO = PartNo,
                    p_FRANCHISE_CD = FranchiseCd,
                    p_PRICE = Price,
                    p_CURRENCY = Currency,
                    p_VALID_FROM = ValidFrom,
                    p_VALID_TO = ValidTo,
                    p_CREATED_BY = CreatedBy,
                    p_pid = pid,
                    p_RowCount = RowCount,
                    //p_VALID_FROM = reFormatDate(ValidFrom),
                    //p_VALID_TO = reFormatDate(ValidTo),
                    //p_CHANGED_BY = ChangedBy,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }



        public int getValidate_Interface()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPriceMaster>("Reprocess_Interface_Validation");
            db.Close();
            return l.ToList()[0].TOTAL_INTERFACE;
        }



        public string InsertParam_Interface(string pID, string pName, string pDescription, string pSubmitter, string pFunctionName, string pParameter, string pType, string pStatus, string pCommand, string pStartDate, string pEndDate, string pPeriodicType, string pInterval, string pExecutionDays, string pExecutionMonths, string pTime)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mPriceMaster>("Interface_Send_Param", new
                {
                    ID = pID,
                    NAME = pName,
                    DESCRIPTION = pDescription,
                    SUBMITTER = pSubmitter,
                    FUNCTION_NAME = pFunctionName,
                    PARAMETER = pParameter,
                    TYPE = pType,
                    STATUS = pStatus,
                    COMMAND = pCommand,
                    START_DATE = pStartDate,
                    END_DATE = pEndDate,
                    PERIODIC_TYPE = pPeriodicType,
                    INTERVAL = pInterval,
                    EXECUTION_DAYS = pExecutionDays,
                    EXECUTION_MONTHS = pExecutionMonths,
                    TIME = pTime,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }


        //add by ria 20160426
        public string KillData(string PartNo, string FranchiseCode, string Price, string Currency, string ValidFrom, string ValidTo, string UserID)
        {
            string result = "";
            try
            {

                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPriceMaster>("Master_Price_Kill", new
                {
                    p_PART_NO = PartNo,
                    p_FRANCHISE_CD = FranchiseCode,
                    p_PRICE = Price,
                    p_CURRENCY = Currency,
                    p_VALID_FROM = reFormatDate(ValidFrom),
                    p_VALID_TO = reFormatDate(ValidTo),
                    p_USER_ID = UserID,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }

            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }


    }
}



//    public string SaveUploadData(string PartNo, string FranchiseCd, string Price, string ValidFrom, string ValidTo)
//    {
//     string result = "";
//        try
//        {
//            IDBContext db = DatabaseManager.Instance.GetContext();
//            db.Execute("Master_Price_Upload", 
//                new { p_PART_NO = PartNo },
//                new {  p_FRANCHISE_CD = FranchiseCd, },
//                new {  p_PRICE = Price, },
//                new {  p_VALID_FROM = ValidFrom, }, 
//                new {  p_VALID_TO = ValidTo, },
//                new { MSG_TEXT = ""  });
//            db.Close();

//            return "Process save uploaded part master finish successfully";
//        }
//        catch (Exception err)
//        {
//            result = "Error | " + Convert.ToString(err.Message);
//        }
//        return result;
//    }
//}


//public string DeleteUploadedData(string p_Process_Id)
//    {
//        string result = "";
//        try
//        {
//            IDBContext db = DatabaseManager.Instance.GetContext();
//            var l = db.Execute("Master_Part_Delete_Upload", new { PROCESS_ID = p_Process_Id });
//            db.Close();

//            return "Process Delete Upload Price finish successfully";
//        }
//        catch (Exception err)
//        {
//            result = "Error | " + Convert.ToString(err.Message);
//        }
//        return result;
//    }

//    public string CheckUploadedData(string p_Created_By, string ProcessId)
//    {
//        string result = "";
//        try
//        {
//            IDBContext db = DatabaseManager.Instance.GetContext();
//            var l = db.Fetch<mPart>("Master_Part_Save_Upload_Validation", new { PROCESS_ID = ProcessId }, new { CREATED_BY = p_Created_By });
//            db.Close();

//            foreach (var message in l)
//            {
//                result = message.Text.ToString();
//            }

//            if (result == "SUCCESS")
//            {
//                return "Process Check Price finish successfully";
//            }
//            else
//            {
//                return result;
//            }
//        }
//        catch (Exception err)
//        {
//            result = "Error | " + Convert.ToString(err.Message);
//        }
//        return result;
//    }
//}
//}




