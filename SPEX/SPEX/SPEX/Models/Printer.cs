﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class Printer
    {
        public string PRINTER_NAME { get; set; }

        public string Text { get; set; }

       

        public List<Printer> getList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<Printer>("Printer_GetList");
            db.Close();

            return l.ToList();
        }

        


    }
}