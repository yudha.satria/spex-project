﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using SPEX.Cls;

namespace SPEX.Models
{
    public class mUploadTLMS
    {
        public string PROCESS_ID { get; set; }
        public string FILE_NAME { get; set; }
        public string FILE_TYPE { get; set; }
        public DateTime? UPLOAD_DATE { get; set; }
        public string STATUS { get; set; }

        public string TEXT { get; set; }

        public List<mUploadTLMS> GetData(mUploadTLMS PM, string Mode)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mUploadTLMS> l = db.Fetch<mUploadTLMS>("TLMS_Search",
                  new { Mode = Mode }
            ).ToList();
            db.Close();
            return l;
        }

        public string UploadTLMS(string UserID, string uploadType, string filename)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mUploadTLMS>("TLMS_Upload",
                             new { 
                                 USER_ID = UserID,
                                 UPLOAD_TYPE = uploadType,
                                 FILENAME = filename
                             }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string DeleteDataTBTDD()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPartPriceMaster>("TLMS_DeleteTBTDD");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public string DeleteDataTBTOA()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPartPriceMaster>("TLMS_DeleteTBTOA");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public string BulkInsertTempCSVToTempDriverData()
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPartPriceMaster>("TLMS_BulkInsert_DriverData"
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string BulkInsertTempCSVToTempOrderAssignment()
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPartPriceMaster>("TLMS_BulkInsert_OrderAssignment"
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }
    }
}