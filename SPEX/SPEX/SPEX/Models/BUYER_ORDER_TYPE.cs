﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class BUYER_ORDER_TYPE_MASTER
    {
        public string BUYER_ORDER_TYPE { get; set; }
        public string BUYER_TRANSPORTATION_CD { get; set; }
        public string ORDER_TYPE_DESCRIPTION { get; set; }
        public string SUPPLY_TYPE { get; set; }
        public string USED_FLAG { get; set; }
        public string DELETION_FLAG { get; set; }
        public int DELIVERY_LEAD_TIME { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }

        public List<string> GetDistinctBUYER_ORDER_TYPE()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<string> l = db.Fetch<string>("BUYER_ORDER_TYPE_GET_DISTINCT_ORDER_TYPE"
                  ).ToList();
            db.Close();
            return l;
        }

    }
}