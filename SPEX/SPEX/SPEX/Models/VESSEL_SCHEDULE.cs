﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class VESSEL_SCHEDULE
    {
        public string BUYER_CD { get; set; }
        public string BUYER_NAME { get; set; }
        //public  BUYER Buyer                     {get;set;}
        public string FEEDER_VESSEL { get; set; }
        public string FEEDER_VOYAGE { get; set; }
        public string CONNECT_VESSEL { get; set; }
        public string CONNECT_VOYAGE { get; set; }
        public DateTime? ETA { get; set; }
        public string SHIP_AGENT_CD { get; set; }
        public string SHIP_AGENT_NAME { get; set; }
        public DateTime? VANNING_DT { get; set; }
        public string VANNING_DAY { get; set; }
        public DateTime? ETD { get; set; }
        public string ETD_DAY { get; set; }
        public int CONTAINER_PLAN { get; set; }
        public string FORWARD_AGENT_CD { get; set; }
        public string FORWARD_AGENT_NAME { get; set; }
        public string BOOKING_NO { get; set; }
        public DateTime? CLOSING_TM { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string PACKING_COMPANY { get; set; }

        public string Text { get; set; }

        public class VESSEL_SCHEDULE_UPLOAD
        {
            public string BUYER_CD { get; set; }
            public string BUYER_NAME { get; set; }
            public string FEEDER_VESSEL { get; set; }
            public string FEEDER_VOYAGE { get; set; }
            public string CONNECT_VESSEL { get; set; }
            public string CONNECT_VOYAGE { get; set; }
            public string ETA { get; set; }
            public string SHIP_AGENT_CD { get; set; }
            public string SHIP_AGENT_NAME { get; set; }
            public string VANNING_DT { get; set; }
            public string VANNING_DAY { get; set; }
            public string ETD { get; set; }
            public string ETD_DAY { get; set; }
            public string CONTAINER_PLAN { get; set; }
            public string FORWARD_AGENT_CD { get; set; }
            public string FORWARD_AGENT_NAME { get; set; }
            public string BOOKING_NO { get; set; }
            public string CLOSING_TM { get; set; }
            public string PACKING_COMPANY { get; set; }
        }

        public List<VESSEL_SCHEDULE> getVESSEL_SCHEDULE(string PACKING_COMPANY, string BUYER_CD, DateTime? VANNING_DT_FROM, DateTime? VANNING_DT_TO, DateTime? ETD_FROM, DateTime? ETD_TO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<VESSEL_SCHEDULE> l = db.Fetch<VESSEL_SCHEDULE>("VESSEL_SCHEDULE_Search",
                  new { PACKING_COMPANY = PACKING_COMPANY },
                  new { BUYER_CD = BUYER_CD },
                  new { VANNING_DT_FROM = VANNING_DT_FROM },
                  new { VANNING_DT_TO = VANNING_DT_TO },
                  new { ETD_FROM = ETD_FROM },
                  new { ETD_TO = ETD_TO }
                  ).ToList();
            db.Close();
            return l;
        }

        //Delete Data
        public string DeleteDataTBT()
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Fetch<VESSEL_SCHEDULE>("Tb_T_VESSEL_Schedule_Delete");
                    scope.Complete();
                }


            }
            catch (TransactionAbortedException ex)
            {
                result = ex.Message;
            }
            catch (ApplicationException ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public string DeleteDataTBR(List<string> keyValues)
        {
            string result = "DELETE DATA SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (string a in keyValues)
                    {
                        var key = a.Split('|');
                        BUYER_CD = key[0];
                        BUYER_CD = key[0];
                        FEEDER_VESSEL = key[1];
                        FEEDER_VOYAGE = key[2];
                        CONNECT_VESSEL = key[3];
                        CONNECT_VOYAGE = key[4];
                        if (key[5] != string.Empty)
                        {
                            //
                            ETA = DateTime.ParseExact(key[5], "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                        }
                        else
                        {
                            ETA = DateTime.ParseExact("1753-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                        }
                        SHIP_AGENT_CD = key[6];
                        VANNING_DT = DateTime.ParseExact(key[7], "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                        ETD = DateTime.ParseExact(key[8], "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                        PACKING_COMPANY = key[9];
                        db.Fetch<VESSEL_SCHEDULE>("TB_R_VESSEL_Schedule_Delete",
                             new { p_BUYER_CD = BUYER_CD },
                             new { p_FEEDER_VESSEL = FEEDER_VESSEL },
                             new { p_FEEDER_VOYAGE = FEEDER_VOYAGE },
                             new { p_CONNECT_VESSEL = CONNECT_VESSEL },
                             new { p_CONNECT_VOYAGE = CONNECT_VOYAGE },
                             new { p_ETA = ETA },
                             new { p_SHIP_AGENT_CD = SHIP_AGENT_CD },
                             new { p_VANNING_DT = VANNING_DT },
                             new { p_ETD = ETD },
                             new { PACKING_COMPANY = PACKING_COMPANY }
                             );
                    }
                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                result = ex.Message;
            }
            catch (ApplicationException ex)
            {
                result = ex.Message;
            }
            return result; ;
        }

        public void Save_Master_VESSEL_SCHEDULE(long pid, string user_id, string function_id, string module_id)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Execute("VESSEL_SCHEDULE_CALL_JOB",
                             new { process_id = pid },
                             new { user_id = user_id },
                             new { function_id = function_id },
                             new { module_id = module_id }
                );
            db.Close();
            //foreach (var message in l)
            //{ result = message.ToString(); }
            //return result;
        }

        public string CheckExistingVESSELSchedule()
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<string>("CheckExistingDataInVESSEL_SCHEDULE");
            db.Close();
            foreach (var message in l)
            { result = message.ToString(); }

            return result;
        }

        public List<string> GetPackingCompany()
        {
            //string url = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("GetPackingCompany").ToList();
            db.Close();
            return q;
        }

        public string InsertTempVessel(VESSEL_SCHEDULE_UPLOAD data)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPartPriceMaster>("VesselSchedule_InsertTemp",
                             new { PACKING_COMPANY = data.PACKING_COMPANY },
                             new { BUYER_CD = data.BUYER_CD },
                             new { FEEDER_VESSEL = data.FEEDER_VESSEL },
                             new { FEEDER_VOYAGE_NO = data.FEEDER_VOYAGE },
                             new { CONNECTING_VESSEL = data.CONNECT_VESSEL },
                             new { CONNECT_VOYAGE_NO = data.CONNECT_VOYAGE },
                             new { ETD_DATE = data.ETD },
                             new { SA = data.SHIP_AGENT_CD },
                             new { VANNING_DATE = data.VANNING_DT },
                             new { ETA_DATE = data.ETA },
                             new { CONTAINER_PLAN = data.CONTAINER_PLAN },
                             new { FA = data.FORWARD_AGENT_CD },
                             new { BOOKING_NUMBER = data.BOOKING_NO },
                             new { CLOSING_TIME = data.CLOSING_TM }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }
    }
}


