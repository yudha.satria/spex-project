﻿namespace SPEX.Models.Messages
{
    public class Messages : History
    {
        public string MSG_ID { get; set; }
        public string MSG_TEXT{get;set;}
        public string MSG_TYPE { get; set; }
    }
}