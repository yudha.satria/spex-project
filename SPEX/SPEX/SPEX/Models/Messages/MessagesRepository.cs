﻿using System.Collections.Generic;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models.Messages
{
    public class MessagesRepository : IMessages
    {
        public IList<Messages> GetById(string p_MSG_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            dynamic args = new
            {
                p_MSG_ID = p_MSG_ID
            };

            IList<Messages> result = db.Fetch<Messages>("GetMMessagesById", args);
            db.Close();
            return result;
        }

        public static string GetMessageById(string p_MSG_ID)
        {
            string result = "";
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();

                dynamic args = new
                {
                    p_MSG_ID = p_MSG_ID
                };

                IList<Messages> messages = db.Fetch<Messages>("GetMMessagesById", args);
                result = messages[0].MSG_TEXT;
                db.Close();
            }
            catch (System.Exception)
            {
                
                throw;
            }
            return result;
        }
    }
}
