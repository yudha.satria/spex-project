﻿using System.Collections.Generic;

namespace SPEX.Models.Messages
{
    public interface IMessages
    {
        IList<Messages> GetById(string MSG_ID);
    }
}
