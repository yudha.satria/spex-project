﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class ManifestRecivingInquiry
    {
        public string MANIFEST_NO { get; set; }
        public string TMAP_ORDER_NO { get; set; }
        public string PART_NO { get; set; }
        public string TMAP_PART_NO { get; set; }
        public string ORDER_TYPE { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string TMAP_ORDER_QTY { get; set; }
        public string RECEIVE_FLAG { get; set; }
        public string TMAP_ORDER_DATE { get; set; }
        public string MANIFEST_TYPE { get; set; }
        public string KANBAN_ID { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_PLANT { get; set; }
        public string ORDER_RELEASE_DT { get; set; }
        public string SEND_PO_ADM_FLAG { get; set; }
        public string SEND_GR_ADM_FLAG { get; set; }

        public List<ManifestRecivingInquiry> getList(string pManifest_No, string pDock_Cd, string pStartTmapOrdDt, string pEndTmapOrdDt, string pSupplier_Cd, string pRcvPlant_Cd, string pTmapOrder_No, string pPart_No, string pSubSupplier_Cd, string pSubSupplier_Plant, string pStartOrdRlsDt, string pEndOrdRlsDt, string pSendOrderADMStatus, string pSendGRADMStatus)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<ManifestRecivingInquiry>("ManifestRecivingInquiry_GetList",
                  new { MANIFEST_NO = pManifest_No }
                  , new { SUPPLIER_CD = pSupplier_Cd }
                  , new { DOCK_CD = pDock_Cd }
                  , new { PART_NO = pPart_No }
                  , new { TMAP_ORDER_DATE_FROM = pStartTmapOrdDt }
                  , new { TMAP_ORDER_DATE_TO = pEndTmapOrdDt }
                  , new { RCV_PLANT_CD = pRcvPlant_Cd }
                  , new { TMAP_ORDER_NO = pTmapOrder_No }

                  , new { SUB_SUPPLIER_CD = pSubSupplier_Cd }
                  , new { SUB_SUPPLIER_PLANT = pSubSupplier_Plant }
                  , new { ORDER_RELEASE_DATE_FROM = pStartOrdRlsDt }
                  , new { ORDER_RELEASE_DATE_TO = pEndOrdRlsDt }
                  , new { SEND_PO_ADM_FLAG = pSendOrderADMStatus }
                  , new { SEND_GR_ADM_FLAG = pSendGRADMStatus }
                  );
            db.Close();

            return l.ToList();
        }

        public ManifestRecivingInquiry getOBR(string pOBRNO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<ManifestRecivingInquiry>("OBR_Get",
                  new { OBRNO = pOBRNO }
                  );
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault() : new ManifestRecivingInquiry();
        }

        public List<mSystemMaster> GetManifestRecivingInquiryStatusList()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();

            datas = (new mSystemMaster()).GetSystemValueList("OBR_STATUS", "", "2");
            return datas;
        }

        public string Add(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT, string pDESCRIPTION, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("Case_Add",
                  new
                  {
                      CASE_TYPE = pCASE_TYPE,
                      CASE_NET_WEIGHT = pCASE_NET_WEIGHT,
                      CASE_LENGTH = pCASE_LENGTH,
                      CASE_WIDTH = pCASE_WIDTH,
                      CASE_HEIGHT = pCASE_HEIGHT,
                      DESCRIPTION = pDESCRIPTION,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }

        public string Update(string pOBR_NO, decimal? pCANCEL_QTY, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("OBR_Update",
                  new
                  {
                      OBR_NO = pOBR_NO,
                      CANCEL_QTY = pCANCEL_QTY,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }


        public string UpdateStatus(string pOBR_NOS, string pStatusID, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("OBR_Update_Status",
                  new
                  {
                      OBR_NOS = pOBR_NOS,
                      STATUS_ID = pStatusID,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }

        public List<ManifestRecivingInquiry> getDetailQty(string pManifestNo, string pPartNo, string pTmapOrderNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<ManifestRecivingInquiry>("ManifestRecivingInquiry_Detail",
                new
                {
                    MANIFEST_NO = pManifestNo,
                    PART_NO = pPartNo,
                    TMAP_ORDER_NO = pTmapOrderNo
                });

            db.Close();

            return l.ToList();
        }

        public List<ManifestRecivingInquiry> getDownload_ByTicked(string pManifest_No, string pDock_Cd, string pStartTmapOrdDt, string pEndTmapOrdDt, string pSupplier_Cd, string pRcvPlant_Cd, string pTmapOrder_No, string pPart_No, string pSubSupplier_Cd, string pSubSupplier_Plant, string pStartOrdRlsDt, string pEndOrdRlsDt, string pSendOrderADMStatus, string pSendGRADMStatus)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<ManifestRecivingInquiry>("ManifestRecivingInquiry_DownloadByTick",
                  new { MANIFEST_NO = pManifest_No }
                  , new { SUPPLIER_CD = pSupplier_Cd }
                  , new { DOCK_CD = pDock_Cd }
                  , new { PART_NO = pPart_No }
                  , new { TMAP_ORDER_DATE_FROM = pStartTmapOrdDt }
                  , new { TMAP_ORDER_DATE_TO = pEndTmapOrdDt }
                  , new { RCV_PLANT_CD = pRcvPlant_Cd }
                  , new { TMAP_ORDER_NO = pTmapOrder_No }

                  , new { SUB_SUPPLIER_CD = pSubSupplier_Cd }
                  , new { SUB_SUPPLIER_PLANT = pSubSupplier_Plant }
                  , new { ORDER_RELEASE_DATE_FROM = pStartOrdRlsDt }
                  , new { ORDER_RELEASE_DATE_TO = pEndOrdRlsDt }
                  , new { SEND_PO_ADM_FLAG = pSendOrderADMStatus }
                  , new { SEND_GR_ADM_FLAG = pSendGRADMStatus }
                  );
            db.Close();

            return l.ToList();
        }

        
    }
}