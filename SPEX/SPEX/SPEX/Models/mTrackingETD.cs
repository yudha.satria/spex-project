﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mTrackingETD
    {
        public string BUYERPD_CD { get; set; }
        public string ORDER_NO { get; set; }
        public DateTime ORDER_DT { get; set; }
        public string PART_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string ORDER_TYPE { get; set; }
        public string TRANSPORTATION_CD { get; set; }
        public string ORDER_QTY { get; set; }
        public string CANCEL_QTY { get; set; }
        public string SUPPLY_PART_NO { get; set; }
        public string CASE_NO { get; set; }
        public DateTime? PACKING_DATE { get; set; }
        public string PACKING_QTY { get; set; }
        public DateTime? ETD_DATE { get; set; }
        public DateTime? ALLOCATION_DT { get; set; }
       
        
        public string REJECT_QTY { get; set; }
        public string PROCESS_PART_NO { get; set; }
        public string PROCESS_QTY { get; set; }
        public string TOTAL_BO_QTY { get; set; }
        public string CALCULATE_PACKED_QTY { get; set; }
        public string CALCULATE_SUPPLY_QTY { get; set; }
        public string OTD { get; set; }
        public DateTime? TMMIN_DELIVERY_PLAN_DT { get; set; }

        public string SUPPLY_QTY { get; set; }
        public string INVOICE_NO { get; set; }

        public string SELLER_INV_NO { get; set; }
        //public string PACKING_QTY { get; set; }//Cargo Ready Qty
        //public string SHIPMENT_QTY { get; set; }
       // public DateTime? SHIPMENT_DATE { get; set; }

        public string SUPPLIED_QTY { get; set; }
        public DateTime? SHIPMENT_DT { get; set; }
        public string REMARK { get; set; }
        public string ORDER_DT_FROM { get; set; }
        public string ORDER_DT_TO { get; set; }
        public string RA_CODE { get; set; }
        public string ACCEPT_QTY { get; set; }

        public DateTime? PICKING_DT { get; set; }
        public string ALLOCATION_QTY { get; set; }

        public string PACKED_QTY { get; set; }
        public DateTime? PACKING_DT { get; set; }
        public DateTime? DATE_ORDER { get; set; }
	    public string INV_NO {get; set;}
	    //public string REMARK {get; set;}

        public string BO_QTY { get; set; }
        public string PENDING_QTY { get; set; }
        public string HY_OUTS_QTY { get; set; }
        public string PO_QTY { get; set; }

	  
	  



        public string Text { get; set; }



        public List<mTrackingETD> getListETDTracking(string pPartNo, string pOrderNo, string pItemNo, string pOrderDt, string pOrderDtTo, string pOTD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mTrackingETD>("ETDSearch",
                  new { PART_NO = pPartNo },
                  new { ORDER_NO = pOrderNo },
                  new { ITEM_NO = pItemNo },
                  new { ORDER_DT_FROM = pOrderDt },
                  new { ORDER_DT_TO = pOrderDtTo },
                  new { OTD = pOTD }
                  );
            db.Close();
            return l.ToList();

        }


        public List<mTrackingETD> ETDDownloadByTicked(string D_KeyDownload)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mTrackingETD>("ETDDownloadTicked", new
            {
                D_KeyDownload = D_KeyDownload

            });

            if (D_KeyDownload == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }


        public List<mTrackingETD> ETDDownload(string D_PartNo, string D_OrderNo, string D_ItemNo, string D_OrderDt, string D_OrderDtTo, string D_OTD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mTrackingETD>("ETDDownload", new
            {
                PART_NO = D_PartNo,
                ORDER_NO = D_OrderNo,
                ITEM_NO = D_ItemNo,
                ORDER_DT_FROM = D_OrderDt,
                ORDER_DT_TO = D_OrderDtTo,
                OTD = D_OTD


            });

            //if (D_PartNo == "NULLSTATE")
            //{
            //    l.Clear();
            //}
            db.Close();
            return l.ToList();
        }

        public List<mTrackingETD> UploadSearch(string pParam)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mTrackingETD>("Search_Upload_ETD",
                  new { PARAM = pParam }
                  
                  );
            db.Close();
            return l.ToList();

        }
        //public string UploadSearch(string BuyerPDCode, string OrderNo, string OrderTanggal, string PartNo, string ItemNo, string OrderType, string Transport, string OrderQty)
        //{
        //    string result = "";
        //    try
        //    {
        //        IDBContext db = DatabaseManager.Instance.GetContext();

        //        var l = db.Fetch<mTrackingETD>("Search_Upload_ETD",
        //            new
        //        {
        //            BUYERPD_CD = BuyerPDCode,
        //            ORDER_NO = OrderNo,
        //            ORDER_DATE = OrderTanggal,
        //            PART_NO = PartNo,
        //            ITEM_NO = ItemNo,
        //            TRANSPORT = Transport,
        //            ORDER_QTY = OrderQty,
        //            ORDER_TYPE = OrderType


        //        });
        //        db.Close();

        //        //result = l;
        //        //foreach (var message in l)
        //        //{
        //        //    result = message.Text;
        //        //}
        //    }
        //    catch (Exception err)
        //    {
        //        result = "Error | " + Convert.ToString(err.Message);
        //    }

        //    return result;


        //}


        //PEMANGGILAN DATA YANG AKAN DI SEARCH DAN DI TAMPILKAN (CEKNYA DI SQL)
       


        public List<mTrackingETD> getListOTD(string pu_ORDER_NO, string pu_PART_NO, string pu_ITEM_NO)
        {


            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mTrackingETD>("SP_ETD_OTD",
                //new object[] {pu_ORDER_NO, pu_PART_NO , pu_ITEM_NO});
                new { ORDER_NO = pu_ORDER_NO },
                  new { PART_NO = pu_PART_NO },
                  new { ITEM_NO = pu_ITEM_NO });

            db.Close();


            return l.ToList();

        }


        public List<mTrackingETD> getListShipment(string pu_ORDER_NO, string pu_PART_NO, string pu_ITEM_NO)
        {


            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mTrackingETD>("ETD_SHIPMENT_POP_UP",
                //new object[] {pu_ORDER_NO, pu_PART_NO , pu_ITEM_NO});
                new { ORDER_NO = pu_ORDER_NO },
                  new { PART_NO = pu_PART_NO },
                  new { ITEM_NO = pu_ITEM_NO }
                   //,new { ORDER_QTY = pu_ORDER_QTY }
                   );

            db.Close();


            return l.ToList();

        }


        public List<mTrackingETD> getListCargo(string pu_ORDER_NO, string pu_PART_NO, string pu_ITEM_NO)
        {


            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mTrackingETD>("ETD_CARGO_POP_UP",
                //new object[] {pu_ORDER_NO, pu_PART_NO , pu_ITEM_NO});
                new { ORDER_NO = pu_ORDER_NO },
                  new { PART_NO = pu_PART_NO },
                  new { ITEM_NO = pu_ITEM_NO }
                   //,new { ORDER_QTY = pu_ORDER_QTY }
                   );

            db.Close();


            return l.ToList();

        }


        public List<mTrackingETD> getListSupply(string pu_ORDER_NO, string pu_PART_NO, string pu_ITEM_NO)
        {


            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mTrackingETD>("ETD_SUPPLY_POP_UP",
                //new object[] {pu_ORDER_NO, pu_PART_NO , pu_ITEM_NO});
                new { ORDER_NO = pu_ORDER_NO },
                  new { PART_NO = pu_PART_NO },
                  new { ITEM_NO = pu_ITEM_NO }
                  //,new { ORDER_QTY = pu_ORDER_QTY }
                  );

            db.Close();


            return l.ToList();

        }


        public List<mTrackingETD> getListBackOrder(string pu_ORDER_NO, string pu_PART_NO)
        {


            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mTrackingETD>("ETD_BO_POP_UP",
                //new object[] {pu_ORDER_NO, pu_PART_NO , pu_ITEM_NO});
                new { ORDER_NO = pu_ORDER_NO },
                  new { PART_NO = pu_PART_NO }
                  //,new { ITEM_NO = pu_ITEM_NO }
                  // ,new { ORDER_QTY = pu_ORDER_QTY }
                   );

            db.Close();


            return l.ToList();

        }


    }
}




