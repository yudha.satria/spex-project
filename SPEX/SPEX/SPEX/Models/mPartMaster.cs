﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPartMaster
    {
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string FRANCHISE_CD { get; set; }
        public string RETAIL_PRICE { get; set; }
        public string USED_FLAG { get; set; }
        public string DELETION_FLAG { get; set; }
        public string RELEASE_STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        //PEMANGGILAN DATA YANG AKAN DI SEARCH DAN DI TAMPILKAN (CEKNYA DI SQL)


        public List<mPartMaster> getPartMaster(string pPartNo, string pPartNane, string pFranchiseCode )
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPartMaster>("GetPartMaster", new object[] { pPartNo, pPartNane, pFranchiseCode });
            db.Close();
            return l.ToList();
        }



    public List<mPartMaster> PartMasterDownload(string D_PartNo, string D_PartName, string D_FranchiseCode)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPartMaster>("DownloadPartMaster",
               new object[] { D_PartNo, D_PartName, D_FranchiseCode });


            //var l = db.Fetch<mPartMaster>("DownloadPartMaster", new
            //{
            //    PART_NO = D_PartNo,
            //    PART_NAME = D_PartName,
            //    FRANCHISE_CD = D_FranchiseCode
            //});

            if (D_PartNo == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }
        

        

    }
}