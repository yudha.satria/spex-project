﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPEBTAM
    {
        //PEBPDTX
        public string PEB_SEQ { get; set; }
        public string HS_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public double TOTAL_QTY { get; set; }
        public double PRICE { get; set; }

        //PEBPHD1X
        public string INVOICE_NO { get; set; }
        public DateTime? INVOICE_DT { get; set; }
        public DateTime? ETD { get; set; }
        public string VESSEL { get; set; }

        //PEBPHD2X
        public double FOB_AMOUNT { get; set; }
        public double TOTAL_CASE { get; set; }
        public double TOTAL_GROSS_WEIGHT { get; set; }
        public double TOTAL_NET_WEIGHT { get; set; }
        //total qty

        //PEBPHD3X
        public string CASE_NO { get; set; }

        // system master
        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }

        ////
        public string Text { get; set; }
        public string REGISTER_NO { get; set; }

        public string PROCESS_STATUS { get; set; }
        public string SEND_STATUS { get; set; }

        //segment a
        public DateTime? REGISTER_DT { get; set; }
        public string VOYAGE_NO { get; set; }
        public string DEST_PORT_CD { get; set; }
        public string IMPORTER_CD { get; set; }
        public DateTime? INSPECTION_DT { get; set; }
        public string INV_CONTAINER_COUNT { get; set; }

        //container data
        public string CONTAINER_NO { get; set; }
        public string CONTAINER_SEGEL { get; set; }
        public string CONTAINER_SIZE { get; set; }
        public string SEQ_NO { get; set; }
        public string PACKING_COMPANY { get; set; }


        public List<mPEBTAM> getListPEB(string pPEBSeq, string pInvoiceDate, string pInvoiceDateTo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEBTAM>("PEBTAM_Header_Search",
                  new { PEB_SEQ = pPEBSeq },
                  new { INVOICE_DT = pInvoiceDate },
                  new { INVOICE_DT_TO = pInvoiceDateTo }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mPEBTAM> getListPEBDetail(string pPEBSeq)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEBTAM>("PEBTAM_Detail_Search",
                  new { PEB_SEQ = pPEBSeq }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mPEBTAM> getListPEBHeader3(string pPEBSeq)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEBTAM>("PEBTAM_Header3_Search",
                  new { PEB_SEQ = pPEBSeq }
                  );
            db.Close();

            return l.ToList();
        }

        //Insert Parameter to TB_R_BACKGROUNDTASK_REGISTRY
        public string InsertParam(string pID, string pName, string pDescription, string pSubmitter, string pFunctionName, string pParameter, string pType, string pStatus, string pCommand, string pStartDate, string pEndDate, string pPeriodicType, string pInterval, string pExecutionDays, string pExecutionMonths, string pTime)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mPEBTAM>("PEBTAM_SendParam", new
                {
                    ID = pID,
                    NAME = pName,
                    DESCRIPTION = pDescription,
                    SUBMITTER = pSubmitter,
                    FUNCTION_NAME = pFunctionName,
                    PARAMETER = pParameter,
                    TYPE = pType,
                    STATUS = pStatus,
                    COMMAND = pCommand,
                    START_DATE = pStartDate,
                    END_DATE = pEndDate,
                    PERIODIC_TYPE = pPeriodicType,
                    INTERVAL = pInterval,
                    EXECUTION_DAYS = pExecutionDays,
                    EXECUTION_MONTHS = pExecutionMonths,
                    TIME = pTime,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public List<mPEBTAM> getListOfDownloadData_2(string pPEBSeq)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEBTAM>("PEBTAM_Download_Data_2",
                  new { PEB_SEQ = pPEBSeq }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mPEBTAM> getListOfSystemMasterData(string pSystemType, string pSystemCD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEBTAM>("PEB_Get_System_Master_List",
               new
               {
                   SYSTEM_TYPE = pSystemType,
                   SYSTEM_CD = pSystemCD
               }
                  );
            db.Close();

            return l.ToList();
        }

        public mPEBTAM getMasterData(string pSystemType, string pSystemCd)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEBTAM>("PEB_Get_System_Master",
                  new
                  {
                      SYSTEM_TYPE = pSystemType,
                      SYSTEM_CD = pSystemCd
                  }
                  );
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault() : new mPEBTAM();
        }

        public string SaveSegmentData(string pPEB_SEQ, string pINVOICE_NO, string pREGISTER_NO,
            string pREGISTER_DT, string pVOYAGE_NO, string pDEST_PORT_CD, string pIMPORTER_CD,
            string pINSPECTION_DT, string pINV_CONTAINER_COUNT, string pCONTAINER,string pPACKING_COMPANY, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mPEBTAM>("PEBTAM_Save_Segment",
                  new
                  {
                      REGISTER_NO = pREGISTER_NO,
                      REGISTER_DT = pREGISTER_DT,
                      PEB_SEQ = pPEB_SEQ,
                      INVOICE_NO = pINVOICE_NO,
                      VOYAGE_NO = pVOYAGE_NO,
                      DEST_PORT_CD = pDEST_PORT_CD,
                      IMPORTER_CD = pIMPORTER_CD,
                      INSPECTION_DT = pINSPECTION_DT,
                      INV_CONTAINER_COUNT = pINV_CONTAINER_COUNT,
                      CONTAINER = pCONTAINER,
                      PACKING_COMPANY = pPACKING_COMPANY,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }

        public string UpdateSegmentData(string pPEB_SEQ, string pINVOICE_NO, string pREGISTER_NO,
            string pREGISTER_DT, string pVOYAGE_NO, string pDEST_PORT_CD, string pIMPORTER_CD,
            string pINSPECTION_DT, string pINV_CONTAINER_COUNT, string pCONTAINER, string pPACKING_COMPANY, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mPEBTAM>("PEBTAM_Update_Segment",
                  new
                  {
                      REGISTER_NO = pREGISTER_NO,
                      REGISTER_DT = pREGISTER_DT,
                      PEB_SEQ = pPEB_SEQ,
                      INVOICE_NO = pINVOICE_NO,
                      VOYAGE_NO = pVOYAGE_NO,
                      DEST_PORT_CD = pDEST_PORT_CD,
                      IMPORTER_CD = pIMPORTER_CD,
                      INSPECTION_DT = pINSPECTION_DT,
                      INV_CONTAINER_COUNT = pINV_CONTAINER_COUNT,
                      CONTAINER = pCONTAINER,
                      PACKING_COMPANY = pPACKING_COMPANY,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }

        public string CheckSegmentData(string pPEB_SEQ, string pINVOICE_NO, string pREGISTER_NO,
            string pREGISTER_DT, string pVOYAGE_NO, string pDEST_PORT_CD, string pIMPORTER_CD,
            string pINSPECTION_DT, string pINV_CONTAINER_COUNT, string pCONTAINER, string pUSER_ID)
        {
            string result = "FAILED";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEBTAM>("PEBTAM_Check_Segment",
                  new
                  {
                      REGISTER_NO = pREGISTER_NO,
                      REGISTER_DT = pREGISTER_DT,
                      PEB_SEQ = pPEB_SEQ,
                      INVOICE_NO = pINVOICE_NO,
                      VOYAGE_NO = pVOYAGE_NO,
                      DEST_PORT_CD = pDEST_PORT_CD,
                      IMPORTER_CD = pIMPORTER_CD,
                      INSPECTION_DT = pINSPECTION_DT,
                      INV_CONTAINER_COUNT = pINV_CONTAINER_COUNT,
                      CONTAINER = pCONTAINER,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }

            return result;
        }

        public string SendValidate(string PROCESS_ID, string REGISTER_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "Success ";
            var l = db.Fetch<mPEBTAM>("PEBTAM_Send_Validation", new { PROCESS_ID = PROCESS_ID, REGISTER_NO = REGISTER_NO });
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }
            return result;
        }

        public string SendData(string pPROCESS_ID, string pPEB_SEQ, string pName, string pDescription, string pSubmitter, string pFunctionName, string pParameter, string pType, string pStatus, string pCommand, string pStartDate, string pEndDate, string pPeriodicType, string pInterval, string pExecutionDays, string pExecutionMonths, string pTime)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "Success ";
            var l = db.Fetch<mPEBTAM>("PEBTAM_Send_Data",
                new
                {
                    PROCESS_ID = pPROCESS_ID,
                    PEB_SEQ = pPEB_SEQ,
                    NAME = pName,
                    DESCRIPTION = pDescription,
                    SUBMITTER = pSubmitter,
                    FUNCTION_NAME = pFunctionName,
                    PARAMETER = pParameter,
                    TYPE = pType,
                    STATUS = pStatus,
                    COMMAND = pCommand,
                    START_DATE = pStartDate,
                    END_DATE = pEndDate,
                    PERIODIC_TYPE = pPeriodicType,
                    INTERVAL = pInterval,
                    EXECUTION_DAYS = pExecutionDays,
                    EXECUTION_MONTHS = pExecutionMonths,
                    TIME = pTime
                });
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }
            return result;
        }

        public string CheckProcess(string pPEB_SEQ)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEBTAM>("PEBTAM_Check_Process",
                  new
                  {
                      PEB_SEQ = pPEB_SEQ
                  }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }

            return result;
        }

        public List<mPEBTAM> getListPEBContainer(string pREGISTER_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEBTAM>("PEBTAM_Get_Segment_Container",
                  new { REGISTER_NO = pREGISTER_NO }
                  );
            db.Close();

            return l.ToList();
        }

        public mPEBTAM getPEBSegment(string pPEB_SEQ)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            mPEBTAM returnValue = new mPEBTAM();
            var l = db.Fetch<mPEBTAM>("PEBTAM_Get_Segment", new { PEB_SEQ = pPEB_SEQ });
            foreach (var message in l.ToList())
            {
                returnValue = (mPEBTAM)message;
            }
            db.Close();

            return returnValue;
        }


        public string DeleteData(string pPEB_SEQ)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPEBTAM>("PEBTAM_Delete_Data",
                  new
                  {
                      PEB_SEQ = pPEB_SEQ
                  }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }

            return result;
        }
    }
}
