﻿using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mOrderTypeConversionMaster
    {
        public string BUYER_ORDER_TYPE { get; set; }
        public string BUYER_TRANSPORTATION_CD { get; set; }
        public string ORDER_TYPE_DESCRIPTION { get; set; }
        public string SUPPLY_TYPE { get; set; }




        //PEMANGGILAN DATA YANG AKAN DI SEARCH DAN DI TAMPILKAN (CEKNYA DI SQL)
        public List<mOrderTypeConversionMaster> getListOrderTypeMaster(string pBuyerType, string pBuyerTrans, string pOrderTypeDescription, string pSupplyType)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderTypeConversionMaster>("GetOrderTypeMaster",
                new object[] { pBuyerType, pBuyerTrans, pOrderTypeDescription, pSupplyType });
            //new { BUYER_ORDER_TYPE = pBuyerType },
            //new { BUYER_TRANSPORTATION_CD = pBuyerTrans },
            //new { SUPPLY_TYPE = pSupplyType },
            //new { ORDER_TYPE_DESCRIPTION = pOrderTypeDescription });
            db.Close();
            return l.ToList();

        }



        public List<mOrderTypeConversionMaster> OrderTypeConvDownload(string D_BuyerOrderType, string D_BuyerTranscd, string D_OrderTypeDescription, string D_SupplyType)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            //var l = db.Fetch<mOrderTypeConversionMaster>("Master_Order_Type_Conv_Download",
            //    new object[] { D_BuyerOrderType, D_BuyerTranscd, D_OrderTypeDescription, D_SupplyType }); 


            var l = db.Fetch<mOrderTypeConversionMaster>("Master_Order_Type_Conv_Download", new
            {
                BUYER_ORDER_TYPE = D_BuyerOrderType,
                BUYER_TRANSPORTATION_CD = D_BuyerTranscd,
                ORDER_TYPE_DESCRIPTION = D_OrderTypeDescription,
                SUPPLY_TYPE = D_SupplyType
            });

            if (D_OrderTypeDescription == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }

        public List<mOrderTypeConversionMaster> getListCombobox()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderTypeConversionMaster>("GETBUYER_COMBOBOX");
            db.Close();
            return l.ToList();

        }
        public List<mOrderTypeConversionMaster> getListCbBuyerTrans()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderTypeConversionMaster>("GETTRANS_COMBOBOX");
            db.Close();
            return l.ToList();

        }
        public List<mOrderTypeConversionMaster> getListCbSupply()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mOrderTypeConversionMaster>("SUPPLY_COMBOBOX");
            db.Close();
            return l.ToList();

        }




    }
}