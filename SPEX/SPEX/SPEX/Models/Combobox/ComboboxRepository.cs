﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models.Combobox
{
    public class ComboboxRepository
    {
        #region Singleton
        private static ComboboxRepository instance = null;
        public static ComboboxRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ComboboxRepository();
                }
                return instance;
            }
        }
        #endregion

        public List<mCombobox> ZoneCode ()
        {
            List<mCombobox> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {

                };
                result = db.Fetch<mCombobox>("Combobox/GetZoneCode", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public List<mCombobox> AisleCode()
        {
            List<mCombobox> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {

                };
                result = db.Fetch<mCombobox>("Combobox/GetAisleCode", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public List<mCombobox> RackCode()
        {
            List<mCombobox> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {

                };
                result = db.Fetch<mCombobox>("Combobox/GetRackCode", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public List<mCombobox> SystemMasterCode(String SystemType)
        {
            List<mCombobox> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    SYSTEM_TYPE = SystemType
                };
                result = db.Fetch<mCombobox>("Combobox/GetSystemMasterCode", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public List<mCombobox> GrAdmFlag(String SystemType)
        {
            List<mCombobox> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    SYSTEM_TYPE = SystemType
                };
                result = db.Fetch<mCombobox>("Combobox/GetSystemMasterCode", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public List<mCombobox> GetSupplier(String SystemType) {
            List<mCombobox> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    TYPE = SystemType
                };
                result = db.Fetch<mCombobox>("Combobox/GetSupplier", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }


        public List<mCombobox> GetProblemCategory(String ProblemOrigin)
        {
            List<mCombobox> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROBLEM_ORIGIN_CD = ProblemOrigin
                };
                result = db.Fetch<mCombobox>("Combobox/GetProblemCategory", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

    }
}