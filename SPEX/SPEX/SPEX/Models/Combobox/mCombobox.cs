﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models.Combobox
{
    public class mCombobox
    {
        public String COMBO_VALUE { get; set; }
        public String COMBO_LABEL { get; set; }
    }
}