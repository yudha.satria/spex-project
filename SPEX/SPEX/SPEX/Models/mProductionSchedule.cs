﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mProductionSchedule
    {
        //FUNGSI GETHER SETTER 
       
        public string PROD_MONTH { get; set; }
        public string LINE_CD { get; set; }
        public string PLANT_CD { get; set; }
        public string SLOC_CD { get; set; }
        public string TIME_D_1 { get; set; }
        public string TIME_D_2 { get; set; }
        public string TIME_D_3 { get; set; }
        public string TIME_D_4 { get; set; }
        public string TIME_D_5 { get; set; }
        public string TIME_D_6 { get; set; }
        public string TIME_D_7 { get; set; }
        public string TIME_D_8 { get; set; }
        public string TIME_D_9 { get; set; }
        public string TIME_D_10 { get; set; }
        public string TIME_D_11 { get; set; }
        public string TIME_D_12 { get; set; }
        public string TIME_D_13 { get; set; }
        public string TIME_D_14 { get; set; }
        public string TIME_D_15 { get; set; }
        public string TIME_D_16 { get; set; }
        public string TIME_D_17 { get; set; }
        public string TIME_D_18 { get; set; }
        public string TIME_D_19 { get; set; }
        public string TIME_D_20 { get; set; }
        public string TIME_D_21 { get; set; }
        public string TIME_D_22 { get; set; }
        public string TIME_D_23 { get; set; }
        public string TIME_D_24 { get; set; }
        public string TIME_D_25 { get; set; }
        public string TIME_D_26 { get; set; }
        public string TIME_D_27 { get; set; }
        public string TIME_D_28 { get; set; }
        public string TIME_D_29 { get; set; }
        public string TIME_D_30 { get; set; }
        public string TIME_D_31 { get; set; }
        public string TEXT { get; set; }
        public string MSG_TEXT { get; set; }
        public string NOROW { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
       
       // public string MSG_TEXT { get; set; }
        
       

               
        //PEMANGGILAN DATA YANG AKAN DI SEARCH DAN DI TAMPILKAN (CEKNYA DI SQL)
        public List<mProductionSchedule> getListProductionSchedule_cari(string p_PROD_MONTH, string p_PLANT_CD, string p_LINE_CD)
        {
         IDBContext db = DatabaseManager.Instance.GetContext();
         string message = string.Empty;
         var l = db.Fetch<mProductionSchedule>("SP_SPN211_Production_Schedule_Inquiry_Screen_Search",

               new { PROD_MONTH = p_PROD_MONTH },
               new { PLANT_CD = p_PLANT_CD },
               new { LINE_CD = p_LINE_CD });
               //new { MSG_TEXT = MSG_TEXT });

                 //foreach (var msg in l)
                 //{
                 //    message = msg.MSG_TEXT;
                 //}

               db.Close();
               return l.ToList();

        }
        public List<mProductionSchedule> getListComboboxPlant()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mProductionSchedule>("Master_Plant_Lookup");
            db.Close();
            return l.ToList();

        }
        public List<mProductionSchedule> getListComboboxLine()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mProductionSchedule>("Master_Line_Lookup");
            db.Close();
            return l.ToList();

        }
        public string SaveDataAdd(string p_PROD_MONTH, string p_LINE_CD, string p_TIME_D_1, string p_TIME_D_2, string p_TIME_D_3,string p_TIME_D_4, string p_TIME_D_5, string p_TIME_D_6, string p_TIME_D_7, string p_TIME_D_8,string p_TIME_D_9, string p_TIME_D_10, string p_TIME_D_11, string p_TIME_D_12, string p_TIME_D_13,string p_TIME_D_14, string p_TIME_D_15, string p_TIME_D_16, string p_TIME_D_17, string p_TIME_D_18,string p_TIME_D_19, string p_TIME_D_20, string p_TIME_D_21, string p_TIME_D_22, string p_TIME_D_23,string p_TIME_D_24, string p_TIME_D_25, string p_TIME_D_26, string p_TIME_D_27, string p_TIME_D_28,string p_TIME_D_29, string p_TIME_D_30, string p_TIME_D_31)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mProductionSchedule>("SP_SPN211_Production_Schedule_Inquiry_Screen_Save", 
                new { PROD_MONTH = p_PROD_MONTH },
                new { LINE_CD = p_LINE_CD },
                new { TIME_D_1 = p_TIME_D_1 },
                new { TIME_D_2 = p_TIME_D_2 },
                new { TIME_D_3 = p_TIME_D_3 },
                new { TIME_D_4 = p_TIME_D_4 },
                new { TIME_D_5 = p_TIME_D_5 },
                new { TIME_D_6 = p_TIME_D_6 },
                new { TIME_D_7 = p_TIME_D_7 },
                new { TIME_D_8 = p_TIME_D_8 },
                new { TIME_D_9 = p_TIME_D_9 },
                new { TIME_D_10 = p_TIME_D_10 },
                new { TIME_D_11 = p_TIME_D_11 },
                new { TIME_D_12 = p_TIME_D_12 },
                new { TIME_D_13 = p_TIME_D_13 },
                new { TIME_D_14 = p_TIME_D_14 },
                new { TIME_D_15 = p_TIME_D_15 },
                new { TIME_D_16 = p_TIME_D_16 },
                new { TIME_D_17 = p_TIME_D_17 },
                new { TIME_D_18 = p_TIME_D_18 },
                new { TIME_D_19 = p_TIME_D_19 },
                new { TIME_D_20 = p_TIME_D_20 },
                new { TIME_D_20 = p_TIME_D_20 },
                new { TIME_D_21 = p_TIME_D_21 },
                new { TIME_D_22 = p_TIME_D_22 },
                new { TIME_D_23 = p_TIME_D_23 },
                new { TIME_D_24 = p_TIME_D_24 },
                new { TIME_D_25 = p_TIME_D_25 },
                new { TIME_D_26 = p_TIME_D_26 },
                new { TIME_D_27 = p_TIME_D_27 },
                new { TIME_D_28 = p_TIME_D_28 },
                new { TIME_D_29 = p_TIME_D_29 },
                new { TIME_D_30 = p_TIME_D_30 },
                new { TIME_D_31 = p_TIME_D_31 },
                new { MSG_TEXT = "" });
                db.Close();

                foreach (var message in l)
                {
                    result = message.TEXT;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }
        public string SPN211DeleteData(string pd_List)
        {
            string result = string.Empty;
            try
            {

                IDBContext db = DatabaseManager.Instance.GetContext();

                string[] PdList = pd_List.Split(',');
                string paramProdMonth = string.Empty;
                string paramLineCd = string.Empty;
                

                for (int i = 0; i < PdList.Length; i++)
                {
                    string[] paramlist = (PdList[i].ToString()).Split('|');

                    paramProdMonth = paramlist[0].ToString();
                    paramLineCd = paramlist[1].ToString();
                    

                    var l = db.Fetch<mProductionSchedule>("SP_SPN211_Production_Schedule_Inquiry_Screen_Delete",
                        new { PROD_MONTH = paramProdMonth },
                        new { LINE_CD = paramLineCd },
                        new { MSG_TEXT = "" });
                    db.Close();

                    int cekjmlh = PdList.Length - i;
                    if (cekjmlh == 1)
                    {
                        foreach (var message in l)
                        {
                            result = message.MSG_TEXT;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                result = "error | " + Convert.ToString(err.Message);
            }

            return result;
        }
        public string SPN211SaveUpdateData(string p_PROD_MONTH, string p_LINE_CD, string p_TIME_D_1, string p_TIME_D_2, string p_TIME_D_3, string p_TIME_D_4, string p_TIME_D_5, string p_TIME_D_6, string p_TIME_D_7, string p_TIME_D_8, string p_TIME_D_9, string p_TIME_D_10, string p_TIME_D_11, string p_TIME_D_12, string p_TIME_D_13, string p_TIME_D_14, string p_TIME_D_15, string p_TIME_D_16, string p_TIME_D_17, string p_TIME_D_18, string p_TIME_D_19, string p_TIME_D_20, string p_TIME_D_21, string p_TIME_D_22, string p_TIME_D_23, string p_TIME_D_24, string p_TIME_D_25, string p_TIME_D_26, string p_TIME_D_27, string p_TIME_D_28, string p_TIME_D_29, string p_TIME_D_30, string p_TIME_D_31)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mProductionSchedule>("SP_SPN211_Production_Schedule_Inquiry_Edit",
                new { PROD_MONTH = p_PROD_MONTH },
                new { LINE_CD = p_LINE_CD },
                new { TIME_D_1 = p_TIME_D_1 },
                new { TIME_D_2 = p_TIME_D_2 },
                new { TIME_D_3 = p_TIME_D_3 },
                new { TIME_D_4 = p_TIME_D_4 },
                new { TIME_D_5 = p_TIME_D_5 },
                new { TIME_D_6 = p_TIME_D_6 },
                new { TIME_D_7 = p_TIME_D_7 },
                new { TIME_D_8 = p_TIME_D_8 },
                new { TIME_D_9 = p_TIME_D_9 },
                new { TIME_D_10 = p_TIME_D_10 },
                new { TIME_D_11 = p_TIME_D_11 },
                new { TIME_D_12 = p_TIME_D_12 },
                new { TIME_D_13 = p_TIME_D_13 },
                new { TIME_D_14 = p_TIME_D_14 },
                new { TIME_D_15 = p_TIME_D_15 },
                new { TIME_D_16 = p_TIME_D_16 },
                new { TIME_D_17 = p_TIME_D_17 },
                new { TIME_D_18 = p_TIME_D_18 },
                new { TIME_D_19 = p_TIME_D_19 },
                new { TIME_D_20 = p_TIME_D_20 },
                new { TIME_D_20 = p_TIME_D_20 },
                new { TIME_D_21 = p_TIME_D_21 },
                new { TIME_D_22 = p_TIME_D_22 },
                new { TIME_D_23 = p_TIME_D_23 },
                new { TIME_D_24 = p_TIME_D_24 },
                new { TIME_D_25 = p_TIME_D_25 },
                new { TIME_D_26 = p_TIME_D_26 },
                new { TIME_D_27 = p_TIME_D_27 },
                new { TIME_D_28 = p_TIME_D_28 },
                new { TIME_D_29 = p_TIME_D_29 },
                new { TIME_D_30 = p_TIME_D_30 },
                new { TIME_D_31 = p_TIME_D_31 },
                new { MSG_TEXT = "" });
                db.Close();

                foreach (var message in l)
                {
                    result = message.TEXT;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }
        public string SPN212CallbackUploadExcelToSQL(string p_PROD_MONTH, string p_LINE_CD, string p_TIME_D_1, string p_TIME_D_2, string p_TIME_D_3,string p_TIME_D_4, string p_TIME_D_5, string p_TIME_D_6, string p_TIME_D_7, string p_TIME_D_8,string p_TIME_D_9, string p_TIME_D_10, string p_TIME_D_11, string p_TIME_D_12, string p_TIME_D_13,string p_TIME_D_14, string p_TIME_D_15, string p_TIME_D_16, string p_TIME_D_17, string p_TIME_D_18,string p_TIME_D_19, string p_TIME_D_20, string p_TIME_D_21, string p_TIME_D_22, string p_TIME_D_23,string p_TIME_D_24, string p_TIME_D_25, string p_TIME_D_26, string p_TIME_D_27, string p_TIME_D_28,string p_TIME_D_29, string p_TIME_D_30, string p_TIME_D_31, string ProcessId)
        {

            string result = string.Empty;
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Execute("SP_SPN212_ProdScheduleInquiry_Upload_Temporary_Table",
                new { PROD_MONTH = p_PROD_MONTH },
                new { LINE_CD = p_LINE_CD },
                new { TIME_D_1 = p_TIME_D_1 },
                new { TIME_D_2 = p_TIME_D_2 },
                new { TIME_D_3 = p_TIME_D_3 },
                new { TIME_D_4 = p_TIME_D_4 },
                new { TIME_D_5 = p_TIME_D_5 },
                new { TIME_D_6 = p_TIME_D_6 },
                new { TIME_D_7 = p_TIME_D_7 },
                new { TIME_D_8 = p_TIME_D_8 },
                new { TIME_D_9 = p_TIME_D_9 },
                new { TIME_D_10 = p_TIME_D_10 },
                new { TIME_D_11 = p_TIME_D_11 },
                new { TIME_D_12 = p_TIME_D_12 },
                new { TIME_D_13 = p_TIME_D_13 },
                new { TIME_D_14 = p_TIME_D_14 },
                new { TIME_D_15 = p_TIME_D_15 },
                new { TIME_D_16 = p_TIME_D_16 },
                new { TIME_D_17 = p_TIME_D_17 },
                new { TIME_D_18 = p_TIME_D_18 },
                new { TIME_D_19 = p_TIME_D_19 },
                new { TIME_D_20 = p_TIME_D_20 },
                new { TIME_D_20 = p_TIME_D_20 },
                new { TIME_D_21 = p_TIME_D_21 },
                new { TIME_D_22 = p_TIME_D_22 },
                new { TIME_D_23 = p_TIME_D_23 },
                new { TIME_D_24 = p_TIME_D_24 },
                new { TIME_D_25 = p_TIME_D_25 },
                new { TIME_D_26 = p_TIME_D_26 },
                new { TIME_D_27 = p_TIME_D_27 },
                new { TIME_D_28 = p_TIME_D_28 },
                new { TIME_D_29 = p_TIME_D_29 },
                new { TIME_D_30 = p_TIME_D_30 },
                new { TIME_D_31 = p_TIME_D_31 },
                new { PROCESS_ID = ProcessId }
                );
                db.Close();

                result = "Successfully";
            }
            catch (Exception err)
            {
                result = "error | " + Convert.ToString(err.Message);
            }

            return result;
        }
        public string SPN212SaveUploadExcel(string ProcessId)
        {
            string result = string.Empty;
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mSourceList>("SP_SPN212_UPLOAD_PRODUCTIONSCHEDULE", 
                new { PROCESS_ID = ProcessId } 
                );
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text.ToString();
                }

                if (result == "Process UPLOAD Production Schedule Inquiry finish successfully")
                {
                    return "Process UPLOAD PRODUCTION SCHEDULE finish successfully";
                }
                else
                {
                    return result;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }
       
    }
}
