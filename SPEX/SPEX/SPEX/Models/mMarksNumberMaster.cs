﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mMarksNumberMaster
    {
        public string BUYER_PD_CD { get; set; }
        public string BYAIR_COMPANY { get; set; }
        public string BYAIR_CITY { get; set; }
        public string BYAIR_COUNTRY { get; set; }
        public string BYAIR_MARKS_NUMBER { get; set; }
        public string DELETION_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string TEXT { get; set; }

        public string AddData(string BUYER_PD_CD, string COMPANY, string CITY, string COUNTRY, string MARKS_NUMBER, string USER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMarksNumberMaster>("MarksNumberMaster_Add",
                  new { BUYER_PD_CD = BUYER_PD_CD },
                  new { BYAIR_COMPANY = COMPANY },
                  new { BYAIR_CITY = CITY },
                  new { BYAIR_COUNTRY = COUNTRY },
                  new { BYAIR_MARKS_NUMBER = MARKS_NUMBER },
                  new { USER_ID = USER_ID }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string EditData(string BUYER_PD_CD, string COMPANY, string CITY, string COUNTRY, string MARKS_NUMBER, string USER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMarksNumberMaster>("MarksNumberMaster_Edit",
                  new { BUYER_PD_CD = BUYER_PD_CD },
                  new { BYAIR_COMPANY = COMPANY },
                  new { BYAIR_CITY = CITY },
                  new { BYAIR_COUNTRY = COUNTRY },
                  new { BYAIR_MARKS_NUMBER = MARKS_NUMBER },
                  new { USER_ID = USER_ID }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string DeleteData(string BUYER_PD_CD, string USER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMarksNumberMaster>("MarksNumberMaster_Delete",
                  new { BUYER_PD_CD = BUYER_PD_CD },
                  new { USER_ID = USER_ID }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string UploadMarksNumber(string UserID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mMarksNumberMaster>("MarksNumberMaster_Upload",
                             new { USER_ID = UserID }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }


        public string DeleteTempData()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mMarksNumberMaster>("MarksNumberMaster_DeleteTemp");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public List<mMarksNumberMaster> GetList(string pBUYER_PD_CD, string pCOMPANY, string pCITY, string pCOUNTRY, string pMARKS_NUMBER, string pDELETION_FLAG)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMarksNumberMaster>("MarksNumberMaster_GetList",
                  new { BUYER_PD_CD = pBUYER_PD_CD },
                  new { BYAIR_COMPANY = pCOMPANY },
                  new { BYAIR_CITY = pCITY },
                  new { BYAIR_COUNTRY = pCOUNTRY },
                  new { BYAIR_MARKS_NUMBER = pMARKS_NUMBER },
                  new { DELETION_FLAG = pDELETION_FLAG }
                  );
            db.Close();

            return l.ToList();
        }

        public string InsertTempMarksNumber(mMarksNumberMaster data)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mMarksNumberMaster>("MarksNumberMaster_InsertTemp",
                             new { BUYER_PD_CD = data.BUYER_PD_CD },
                             new { BYAIR_COMPANY = data.BYAIR_COMPANY },
                             new { BYAIR_CITY = data.BYAIR_CITY },
                             new { BYAIR_COUNTRY = data.BYAIR_COUNTRY },
                             new { BYAIR_MARKS_NUMBER = data.BYAIR_MARKS_NUMBER }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

    }


}