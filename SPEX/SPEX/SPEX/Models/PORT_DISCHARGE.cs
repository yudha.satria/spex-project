﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
namespace SPEX.Models
{
    public class PORT_DISCHARGE
    {
        public string PORT_DISCHARGE_CD { get; set; }
        public string PORT_DISCHARGE_NAME { get; set; }
        //public string PIC_NAME { get; set; }
        //public string CONTACT_NO { get; set; }
        //public string DESCR1 { get; set; }
        //public string DESCR2 { get; set; }
        //public string DESCR3 { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string Text { get; set; }

        public List<PORT_DISCHARGE> getPORT_DISCHARGE(PORT_DISCHARGE PD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<PORT_DISCHARGE> l = db.Fetch<PORT_DISCHARGE>("PORT_DISCHARGE_SEARCH",
                  new { PORT_DISCHARGE_CD = PD.PORT_DISCHARGE_CD }
                  ).ToList();
            db.Close();
            return l;
        }

     ////Delete Data
     //   public string DeleteData(string PORT_LOADING_CD)
     //   {
     //       string result = "DELETE DATA SUCCES";
     //       IDBContext db = DatabaseManager.Instance.GetContext();
     //       using (TransactionScope scope = new TransactionScope())
     //       { 
     //                   if (PORT_LOADING_CD.Contains(";"))
     //                   {
     //                       string[] PORT_LOADING_CD_list = PORT_LOADING_CD.Split(';');
     //                       for (int i = 0; i < PORT_LOADING_CD_list.Length; i++)
     //                       {
     //                           var l = db.Fetch<mBuyerPD>("PORT_LOADING_Delete", new
     //                           {
     //                               PORT_LOADING_CD = PORT_LOADING_CD_list[i],

     //                           });                    
     //                       }
     //                   }
     //                   else
     //                   {
     //                       db.Fetch<PORT_LOADING>("PORT_LOADING_Delete",
     //                       new { PORT_LOADING_CD = PORT_LOADING_CD });
     //                   }
     //                   scope.Complete();
     //       }
     //       return result;
     //   }
        
     //   //Save Data
     //   public string SaveData(PORT_LOADING PL)
     //   {
     //       string result = "";
     //       try
     //       {
     //           IDBContext db = DatabaseManager.Instance.GetContext();
     //           var l = db.Fetch<PORT_LOADING>("PORT_LOADING_Save", new
     //           {
     //               PORT_LOADING_CD = PL.PORT_LOADING_CD,
     //               PORT_LOADING_NAME = PL.PORT_LOADING_NAME,                    
     //               CREATED_BY = PL.CREATED_BY,
     //               MSG_TEXT = ""
     //           });
     //           db.Close();

     //           foreach (var message in l)
     //           {
     //               result = message.Text;
     //           }
     //       }
     //       catch (Exception err)
     //       {
     //           result = "Error | " + Convert.ToString(err.Message);
     //       }

     //       return result;
     //   }


     //   ////Update Data
        public string UpdateData(PORT_DISCHARGE PD)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<PORT_DISCHARGE>("PORT_DISCHARGE_UPDATE", new
                {
                    PORT_DISCHARGE_CD = PD.PORT_DISCHARGE_CD,
                    PORT_DISCHARGE_NAME = PD.PORT_DISCHARGE_NAME,
                    CHANGED_BY = PD.CHANGED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public string DeleteData(string PORT_DISCHARGE_CD)
        {
            string result = "DELETE DATA SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            using (TransactionScope scope = new TransactionScope())
            {
                if (PORT_DISCHARGE_CD.Contains(";"))
                {
                    string[] PORT_DISCHARGE_CD_list = PORT_DISCHARGE_CD.Split(';');
                    for (int i = 0; i < PORT_DISCHARGE_CD_list.Length; i++)
                    {
                        var l = db.Fetch<mBuyerPD>("PORT_DISCHARGE_Delete", new
                        {
                            PORT_DISCHARGE_CD = PORT_DISCHARGE_CD_list[i],

                        });
                    }
                }
                else
                {
                    db.Fetch<PORT_LOADING>("PORT_DISCHARGE_Delete",
                    new { PORT_LOADING_CD = PORT_DISCHARGE_CD });
                }
                scope.Complete();
            }
            return result;
        }

        public string SaveData(PORT_DISCHARGE PG)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<PORT_DISCHARGE>("PORT_DISCHARGE_SAVE", new
                {
                    PORT_DISCHARGE_CD = PG.PORT_DISCHARGE_CD,
                    PORT_DISCHARGE_NAME = PG.PORT_DISCHARGE_NAME,
                    CREATED_BY = PG.CREATED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }


    }
}
