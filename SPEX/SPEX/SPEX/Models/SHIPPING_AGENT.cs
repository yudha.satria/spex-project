﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class SHIPPING_AGENT
    {
        public string SHIP_AGENT_CD { get; set; }
        public string SHIP_AGENT_NAME { get; set; }
        public string PIC_NAME { get; set; }
        public string CONTACT_NO { get; set; }
        public string DESCR1 { get; set; }
        public string DESCR2 { get; set; }
        public string DESCR3 { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string Text { get; set; }

        public List<SHIPPING_AGENT> getSHIPPING_AGENT(SHIPPING_AGENT ship)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<SHIPPING_AGENT> l = db.Fetch<SHIPPING_AGENT>("SHIPPING_AGENT_Search",
                  new { SHIP_AGENT_CD = ship.SHIP_AGENT_CD },
                  new { SHIP_AGENT_NAME = ship.SHIP_AGENT_NAME },
                  new { PIC_NAME = ship.PIC_NAME }
                  ).ToList();
            db.Close();
            return l;
        }

     //Delete Data
        public string DeleteData(string SHIP_AGENT_CD)
        {
            string result = "DELETE DATA SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    if (SHIP_AGENT_CD.Contains(";"))
                    {
                        string[] SHIP_AGENT_CD_list = SHIP_AGENT_CD.Split(';');
                        for (int i = 0; i < SHIP_AGENT_CD_list.Length; i++)
                        {
                            var l = db.Fetch<mBuyerPD>("SHIPPING_AGENT_Delete", new
                            {
                                SHIP_AGENT_CD = SHIP_AGENT_CD_list[i],

                            });
                            //foreach (var message in l)
                            //{ result = message.Text; }
                        }
                    }
                    else
                    {
                        db.Fetch<SHIPPING_AGENT>("SHIPPING_AGENT_Delete",
                        new { SHIP_AGENT_CD = SHIP_AGENT_CD });
                    }
                    scope.Complete();
                }                
            }
            catch (TransactionAbortedException ex)
            {
                result = ex.Message;
            }
            catch (ApplicationException ex)
            {
                result = ex.Message;
            }
            return result;
        }
      
        public string SaveData(SHIPPING_AGENT Ship)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<SHIPPING_AGENT>("SHIPPING_AGENT_Save", new
                {
                    SHIP_AGENT_CD = Ship.SHIP_AGENT_CD,
                    SHIP_AGENT_NAME = Ship.SHIP_AGENT_NAME,
                    PIC_NAME = Ship.PIC_NAME,
                    CONTACT_NO = Ship.CONTACT_NO,
                    DESCR1=Ship.DESCR1,
                    DESCR2=Ship.DESCR2,
                    DESCR3=Ship.DESCR3,
                    CREATED_BY=Ship.CREATED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }


        //Update Data
        public string UpdateData(SHIPPING_AGENT Ship)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<SHIPPING_AGENT>("SHIPPING_AGENT_Update", new
                {
                    SHIP_AGENT_CD = Ship.SHIP_AGENT_CD,
                    SHIP_AGENT_NAME = Ship.SHIP_AGENT_NAME,
                    PIC_NAME = Ship.PIC_NAME,
                    CONTACT_NO = Ship.CONTACT_NO,
                    DESCR1 = Ship.DESCR1,
                    DESCR2 = Ship.DESCR2,
                    DESCR3 = Ship.DESCR3,
                    CHANGED_BY = Ship.CHANGED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }


    }
}
