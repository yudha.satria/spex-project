﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class ServiceMappingMaster
    {
        public string VENDOR_CD { get; set; }
        public string VENDOR_TYPE { get; set; }
        public string VENDOR_NAME { get; set; }
        public string SERVICE_REGISTRATION { get; set; }
        public string SERVICE_CODE { get; set; }
        public string SERVICE_NAME { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DATE { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DATE { get; set; }
        public string ERROR_MSG { get; set; }

        public List<string> CmbVendorCD { get { return new ServiceMappingMaster().GetVendorCode(); } }

        public List<String> GetServiceId()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<String>("GetServiceId", new { } );
            db.Close();

            return l.ToList();
        }

        public List<String> GetVendorCode()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<String>("GetVendorCode", new { } );
            db.Close();

            return l.ToList();
        }

        public List<String> GetVendorType()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<String>("GetVendorType", new { });
            db.Close();

            return l.ToList();
        }

        public List<ServiceMappingMaster> getServiceMappingMaster(string VendorCd, string VendorName, string ServiceCode,
                                                                  string VendorType, string ServiceReg, string ServiceName,
                                                                  string Mode)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ServiceMappingMaster> l = db.Fetch<ServiceMappingMaster>("ServiceMappingMaster_Search",
                    new { VENDOR_CD = VendorCd },
                    new { VENDOR_NAME = VendorName },
                    new { VENDOR_TYPE = VendorType },
                    new { SERVICE_CODE = ServiceCode },
                    new { SERVICE_REG = ServiceReg },
                    new { SERVICE_NAME = ServiceName },
                    new { MODE = Mode}
                  ).ToList();
            db.Close();
            return l;
        }

        public ServiceMappingMaster GetVendorTypeAndName(string VendorCd)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            ServiceMappingMaster l = db.SingleOrDefault<ServiceMappingMaster>("ServiceMappingMaster_GetVendorTypeAndName", new { VENDOR_CD = VendorCd });
            db.Close();

            return l;
        }

        public List<ServiceMappingMasterPrice> getServicePriceMaster(string VendorCd, string VendorType, string Mode)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ServiceMappingMasterPrice> l = db.Fetch<ServiceMappingMasterPrice>("ServiceMappingMaster_SearchSrvPrice",
                  new { VENDOR_CD = VendorCd },
                  new { VENDOR_TYPE = VendorType },
                  new { MODE = Mode }
                  ).ToList();
            db.Close();
            return l;
        }

        public string AddData(ServiceMappingMaster smm)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ServiceMappingMaster_SaveAdd",
                 new { VENDOR_CD = smm.VENDOR_CD },
                 new { VENDOR_TYPE = smm.VENDOR_TYPE },
                 new { VENDOR_NAME = smm.VENDOR_NAME },
                 new { SERVICE_REG = smm.SERVICE_REGISTRATION },
                 new { USER_ID = smm.CREATED_BY }
                );
            db.Close();
            return result;
        }

        public string EditData(ServiceMappingMaster smm)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ServiceMappingMaster_SaveEdit",
                 new { VENDOR_CD = smm.VENDOR_CD },
                 new { VENDOR_TYPE = smm.VENDOR_TYPE },
                 new { VENDOR_NAME = smm.VENDOR_NAME },
                 new { SERVICE_REG = smm.SERVICE_REGISTRATION },
                 new { USER_ID = smm.CREATED_BY }
                );
            db.Close();
            return result;
        }

        public string DeleteData(List<ServiceMappingMaster> smm, string userId)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "";
            StringBuilder sb = new StringBuilder();

            var distinctsmm = smm.Select(m => 
                                            new { 
                                                m.VENDOR_CD
                                            }).Distinct().ToList();

            foreach (var s in distinctsmm)
            {
                sb.Append(s.VENDOR_CD);
                sb.Append("|");
            }

            if (sb.Length > 0) {
                result = db.SingleOrDefault<string>("ServiceMappingMaster_DeleteData",
                     new { VENDOR_CD = sb.ToString() },
                     new { VENDOR_TYPE = "" },
                     new { SERVICE_REG = "" },
                     new { USER_ID = userId }
                );

                if (result != "SUCCESS")
                    result = "false|" + result;
                else
                {
                    if (distinctsmm.Count > 1)
                    {
                        result = "true|Success Delete All Selected Vendor CD and Vendor Type";
                    }
                    else if (distinctsmm.Count == 1)
                    {
                        result = "true|Vendor code : " + sb.ToString().Split('|')[0].Replace(";", "-") + " has been deleted successfully";
                    }
                }
            }
            else {
                result = "false|No Data to be Deleted.";
            }

            db.Close();
            return result;
        }

        public string DeleteTempData()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            try
            {
                var l = db.Fetch<mPartPriceMaster>("ServiceMappingMaster_DeleteTemp");
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public string BulkInsertTempCSVToTempPartPrice(string userid, long processid)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                result = db.ExecuteScalar<string>("ServiceMappingMaster_Upload_Validation", new { USER_ID = userid }, new { PROCESS_ID = processid });
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public List<ServiceMappingMaster> getServiceMappingErrorUpload()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ServiceMappingMaster> l = db.Fetch<ServiceMappingMaster>("ServiceMappingMaster_ErrorUpload").ToList();
            db.Close();
            return l;
        }
    }

    public class ServiceMappingMasterPrice
    {
        public string SERVICE_REGISTRATION { get; set; }
        public string SERVICE_CODE { get; set; }
        public string SERVICE_NAME { get; set; }
        public string SERVICE_TYPE { get; set; }
        public string CONTAINER_SIZE { get; set; }
        public string PRICE { get; set; }
        public string CURRENCY { get; set; }
        public string VAT { get; set; }
        public string COST_CENTER { get; set; }
        public string VENDOR_TYPE { get; set; }
        public string VALID_FROM { get; set; }
        public string VALID_TO { get; set; }
        public Int16 CHECKED_FLAG { get; set; }
    }
}