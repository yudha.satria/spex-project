﻿using SPEX.Cls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPackingOrder
    {
        public string PACKING_ORDER_NO { get; set; }
        public DateTime? PACKING_ORDER_DT { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string TMMIN_ORDER_NO { get; set; }
        public DateTime? TMMIN_ORDER_DT { get; set; }
        public string TMMIN_MANIFEST_NO { get; set; }
        public string TMAP_ORDER_NO { get; set; }
        public DateTime? TMAP_ORDER_DT { get; set; }
        public string ARRIVAL_DOCK_CD { get; set; }
        public string KANBAN_NO { get; set; }
        //remaks agi 2017-02-08 sesuaikan dengan database
        //public double TMMIN_ORDER_QTY { get; set; }
        public double TMMIN_ORDER_QTY { get; set; }
        public double TMAP_ORDER_QTY { get; set; }
        public string ORDER_GENERATE_STATUS { get; set; }
        public DateTime? ORDER_GENERATE_DT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }


        public string PART_NO { get; set; }
        public string PART_NO_DOWNLOAD { get; set; }

        //add agi 2017-02-07
        public string BUYERPD_CD { get; set; }
        public string ADDRESS { get; set; }
        public string IMPORTIR_INFO_1 { get; set; }
        public string PART_BARCODE { get; set; }
        public string PROGRESS_LANE_NUMBER { get; set; }
        public string CONVAYANCE_NO { get; set; }
        //--and add
        
        public int TMMIN_ORDER_NO_DIGITS
        {
            get
            {
                return Common.GetDataMasterSystemInt("PACKING_ORDER", "TMMIN_ORDERNO_DIGITS");
            }
        }

        public string Text { get; set; }

        public List<mPackingOrder> getListPackingOrder(string p_PACKING_ORDER_NO, string p_TMAP_ORDER_NO,
            string p_SUPPLIER, string p_PACKING_ORDER_DT_FROM, string p_PACKING_ORDER_DT_TO, string p_TMMIN_ORDER_NO, string p_ORDER_GENERATE_STATUS)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPackingOrder>("PackingOrder_Search",
                    new { PACKING_ORDER_NO = p_PACKING_ORDER_NO },
                    new { TMAP_ORDER_NO = p_TMAP_ORDER_NO },
                    new { SUPPLIER = p_SUPPLIER },
                    new { PACKING_ORDER_DT_FROM = p_PACKING_ORDER_DT_FROM },
                    new { PACKING_ORDER_DT_TO = p_PACKING_ORDER_DT_TO },
                    new { TMMIN_ORDER_NO = p_TMMIN_ORDER_NO },
                    new { ORDER_GENERATE_STATUS = p_ORDER_GENERATE_STATUS }
                  );
            db.Close();

            return l.ToList();
        }


        public string UpdateData(string p_PACKING_ORDER_NO, string p_TMMIN_ORDER_NO, string UserID)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPackingOrder>("PackingOrder_Update", new
                {
                    PACKING_ORDER_NO = p_PACKING_ORDER_NO,
                    TMMIN_ORDER_NO = p_TMMIN_ORDER_NO,
                    USER_ID = UserID
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public List<mPackingOrder> getPINoList(string p_PACKING_ORDER_NO, string p_TMAP_ORDER_NO, string p_SUPPLIER, string p_PACKING_ORDER_DT_FROM, string p_PACKING_ORDER_DT_TO, string p_TMMIN_ORDER_NO, string p_ORDER_GENERATE_STATUS)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPackingOrder>("PackingOrder_GetPINoList",
                    new { PACKING_ORDER_NO = p_PACKING_ORDER_NO },
                    new { TMAP_ORDER_NO = p_TMAP_ORDER_NO },
                    new { SUPPLIER = p_SUPPLIER },
                    new { PACKING_ORDER_DT_FROM = p_PACKING_ORDER_DT_FROM },
                    new { PACKING_ORDER_DT_TO = p_PACKING_ORDER_DT_TO },
                    new { TMMIN_ORDER_NO = p_TMMIN_ORDER_NO },
                    new { ORDER_GENERATE_STATUS = p_ORDER_GENERATE_STATUS }
                  );
            db.Close();
            return l.ToList();
        }
        
        public List<mPackingOrder> getDetailDownload(string p_PACKING_ORDER_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPackingOrder>("PackingOrder_GetDetailDownload",
                  new { PACKING_ORDER_NO = p_PACKING_ORDER_NO }
                  );
            db.Close();
            return l.ToList();
        }
        

        public List<mPackingOrder> getListPackingOrder_Download(string p_Order)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mPackingOrder>("PackingOrder_Download",
                new { p_Order = p_Order }
                  );

            db.Close();
            return l.ToList();
        }

        public string GenerateToSupplier(string UserID)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPackingOrder>("PackingOrder_GenerateToSupplier",
                    new { USER_ID = UserID }
                );
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public List<mPackingOrder> getPINoGenerateOrderList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPackingOrder>("PackingOrder_GetPINoGenerateOrderList"
                  );
            db.Close();
            return l.ToList();
        }

        //ADD BY AGI 2016-12-06 DETAIL GENERATE
        public List<mPackingOrder> getDetailGenerate(string p_PACKING_ORDER_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPackingOrder>("PackingOrder_GetDetailGenerate",
                  new { PACKING_ORDER_NO = p_PACKING_ORDER_NO }
                  );
            db.Close();
            return l.ToList();
        }

        public List<mPackingOrder> getListPackingOrder_Download_InHouse(List<string> p_PACKING_ORDER_NO)
        {
            List<mPackingOrder> PO = new List<mPackingOrder>();
            DataTable dt = new DataTable();
            dt.Columns.Add("PACKING_ORDER_NO");
            foreach (var a in p_PACKING_ORDER_NO.Distinct())
            {
                DataRow dr = dt.NewRow();
                dr["PACKING_ORDER_NO"] = a;
                dt.Rows.Add(dr);
            }            
            using (SqlConnection conn = new SqlConnection(DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("[spex].[SP_PACKING_ORDER_DOWNLOAD_INHOUSE_APP]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter p = cmd.Parameters.AddWithValue("@PACKING_ORDER_NOS", dt);
                p.SqlDbType = SqlDbType.Structured;
                try
                {
                    conn.Open();
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        //--- Return from procedures is -> doc, docTo, docTembusan, docApprover, docAttachment
                        int i = 0;


                        while (dr.Read())
                        {
                            //FINANCE_REPORT newItem = new FINANCE_REPORT();
                            mPackingOrder newItem = new mPackingOrder();
                            if (!dr.IsDBNull(0)) 
                                newItem.TMAP_ORDER_NO = dr.GetString(0);
                            if (!dr.IsDBNull(1)) 
                                newItem.BUYERPD_CD = dr.GetString(1);
                            if (!dr.IsDBNull(2)) 
                                newItem.PACKING_ORDER_NO = dr.GetString(2);
                            if (!dr.IsDBNull(3))
                                newItem.PACKING_ORDER_DT = dr.GetDateTime(3);
                            if (!dr.IsDBNull(4)) 
                                newItem.TMMIN_ORDER_NO = dr.GetString(4);
                            if (!dr.IsDBNull(5))
                                newItem.TMAP_ORDER_DT = dr.GetDateTime(5);
                            if (!dr.IsDBNull(6)) 
                                newItem.SUPPLIER_CD = dr.GetString(6);
                            if (!dr.IsDBNull(7)) 
                                newItem.SUPPLIER_PLANT = dr.GetString(7);
                            if (!dr.IsDBNull(8)) 
                                newItem.ARRIVAL_DOCK_CD = dr.GetString(8);
                            if (!dr.IsDBNull(9)) 
                                newItem.PART_NO = dr.GetString(9);
                            if (!dr.IsDBNull(10)) 
                                newItem.KANBAN_NO = dr.GetString(10);
                            if (!dr.IsDBNull(11)) 
                                newItem.TMMIN_ORDER_QTY = dr.GetDouble(11);
                            if (!dr.IsDBNull(12)) 
                                newItem.PART_NO_DOWNLOAD = dr.GetString(12);
                            if (!dr.IsDBNull(13)) 
                                newItem.ADDRESS = dr.GetString(13);
                            if (!dr.IsDBNull(14)) 
                                newItem.IMPORTIR_INFO_1 = dr.GetString(14);
                            if (!dr.IsDBNull(15)) 
                                newItem.PART_BARCODE = dr.GetString(15);
                            if (!dr.IsDBNull(16)) 
                                newItem.PROGRESS_LANE_NUMBER = dr.GetString(16);
                            if (!dr.IsDBNull(17)) 
                                newItem.CONVAYANCE_NO = dr.GetString(17);
                            PO.Add(newItem);
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }           
            return PO;
        }

        public List<string> getPackingOrderNo(string p_PACKING_ORDER_NO, string p_TMAP_ORDER_NO, string p_SUPPLIER, string p_PACKING_ORDER_DT_FROM, string p_PACKING_ORDER_DT_TO, string p_TMMIN_ORDER_NO, string p_ORDER_GENERATE_STATUS)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<string>("PackingOrder_GetPINoList",
                    new { PACKING_ORDER_NO = p_PACKING_ORDER_NO },
                    new { TMAP_ORDER_NO = p_TMAP_ORDER_NO },
                    new { SUPPLIER = p_SUPPLIER },
                    new { PACKING_ORDER_DT_FROM = p_PACKING_ORDER_DT_FROM },
                    new { PACKING_ORDER_DT_TO = p_PACKING_ORDER_DT_TO },
                    new { TMMIN_ORDER_NO = p_TMMIN_ORDER_NO },
                    new { ORDER_GENERATE_STATUS = p_ORDER_GENERATE_STATUS }
                  );
            db.Close();
            return l.ToList();
        }

        public List<mPackingOrder> getListPackingOrder_Download_OutHouse(List<string> p_PACKING_ORDER_NO)
        {
            List<mPackingOrder> PO = new List<mPackingOrder>();
            DataTable dt = new DataTable();
            dt.Columns.Add("PACKING_ORDER_NO");
            foreach (var a in p_PACKING_ORDER_NO.Distinct())
            {
                DataRow dr = dt.NewRow();
                dr["PACKING_ORDER_NO"] = a;
                dt.Rows.Add(dr);
            }
            //IDBContext db = DatabaseManager.Instance.GetContext();
            //PO = db.<mPackingOrder>("PackingOrder_InquiryInHouse",
            //      new { PACKING_ORDER_NO = dt }
            //      ).ToList();
            //db.Close();
            using (SqlConnection conn = new SqlConnection(DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString))
            {
                int i = 1;
                SqlCommand cmd = new SqlCommand("[spex].[SP_PACKING_ORDER_DOWNLOAD_OUTHOUSE_APP]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter p = cmd.Parameters.AddWithValue("@PACKING_ORDER_NOS", dt);
                p.SqlDbType = SqlDbType.Structured;
                try
                {
                    conn.Open();
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        //--- Return from procedures is -> doc, docTo, docTembusan, docApprover, docAttachment
                      


                        while (dr.Read())
                        {
                            
                            mPackingOrder newItem = new mPackingOrder();
                            if (!dr.IsDBNull(0)) 
                                newItem.TMAP_ORDER_NO = dr.GetString(0);
                            if (!dr.IsDBNull(1)) 
                                newItem.BUYERPD_CD = dr.GetString(1);
                            if (!dr.IsDBNull(2)) 
                                newItem.PACKING_ORDER_NO = dr.GetString(2);
                            if(!dr.IsDBNull(3)) 
                                newItem.PACKING_ORDER_DT =dr.GetDateTime(3);
                            if (!dr.IsDBNull(4)) 
                                newItem.TMMIN_ORDER_NO = dr.GetString(4);
                            if (!dr.IsDBNull(5)) 
                                newItem.TMAP_ORDER_DT = dr.GetDateTime(5);
                            if (!dr.IsDBNull(6)) 
                                newItem.SUPPLIER_CD = dr.GetString(6);
                            if (!dr.IsDBNull(7)) 
                                newItem.SUPPLIER_PLANT = dr.GetString(7);
                            if (!dr.IsDBNull(8)) 
                                newItem.ARRIVAL_DOCK_CD = dr.GetString(8);
                            if (!dr.IsDBNull(9)) 
                                newItem.PART_NO = dr.GetString(9);
                            if (!dr.IsDBNull(10)) 
                                newItem.KANBAN_NO = dr.GetString(10);
                            if(!dr.IsDBNull(11))
                                newItem.TMMIN_ORDER_QTY = dr.GetDouble(11);
                            if (!dr.IsDBNull(12)) 
                                newItem.PART_NO_DOWNLOAD = dr.GetString(12);
                            if (!dr.IsDBNull(13)) 
                                newItem.ADDRESS = dr.GetString(13);
                            if (!dr.IsDBNull(14)) 
                                newItem.IMPORTIR_INFO_1 = dr.GetString(14);
                            if (!dr.IsDBNull(15)) 
                                newItem.PART_BARCODE = dr.GetString(15);
                            if (!dr.IsDBNull(16)) 
                                newItem.PROGRESS_LANE_NUMBER = dr.GetString(16);
                            if (!dr.IsDBNull(17)) 
                                newItem.CONVAYANCE_NO = dr.GetString(17);
                            PO.Add(newItem);
                            i++;
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message+",row ="+i.ToString());
                }
            }
            return PO;
        }
    }
}