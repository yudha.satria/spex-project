﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Muhajir Shiddiq
namespace SPEX.Models
{
    public class mPickingListReport
    {
        public string ARRIVAL_PLAN_DT { get; set; }
        public string PICKING_DATE { get; set; }
        public string PICKING_TIME { get; set; }
        public string ARRIVAL_ACTUAL_DT { get; set; }
        public string MANIFEST_NO { get; set; }
        public string DOCK_CD { get; set; }
        public string ZONE_CD { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string RACK_ADDRESS_CD { get; set; }
        public string ORDER_QTY { get; set; }
        public string QTY_PER_CONTAINER { get; set; }
        public string TOTAL_KANBAN { get; set; }
        public string TRANSP_DESC { get; set; }
        
        public string Text { get; set; }



        public List<mPickingListReport> getListPickingList(string pPickingListNo)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mPickingListReport>("PickingList/SearchPickingList",
                    new { @PICKING_LIST = pPickingListNo }
            );
            db.Close();
            return l.ToList();
        }

    }
}
