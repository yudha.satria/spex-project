﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mFirmOrder
    {
        public string PROD_MONTH { get; set; }
        public string ORDER_TYPE { get; set; }
        public string PART_NO { get; set; }
        public string LINE_CD { get; set; }
        public string TIMING { get; set; }
        public string PART_NAME { get; set; }
        public string MODEL { get; set; }
        public string PAINT_TYPE {get; set;}
        public string PART_SPEC { get; set; }
        public decimal TAKT_TIME {get; set;}
        public decimal PCSKBN { get; set; }
        public DateTime LATEST_ORDER { get; set; }
        public string PLANT_CD { get; set; }
        public string SLOC_CD { get; set; }
        public decimal QTY_N { get; set; }
        public decimal QTY_N_1 { get; set; }
        public decimal QTY_N_2 { get; set; }
        public decimal QTY_N_3 { get; set; }
        public string CHECK_PART_MASTER { get; set; }
        public string CHECK_LINE_MASTER { get; set; }
        public string CHECK_PART_LIST_MASTER { get; set; }
        public string CHECK_PLANT_MASTER { get; set; }
        public string CHECK_SOURCE_LIST_MASTER { get; set; }
        public string CHECK_SELLING_PRICE { get; set; }
        public DateTime ORDER_DT { get; set; }
        public string FLAG_COMPLETE { get; set; }
        public string FLAG_PROCESS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
        public string MSG_TEXT { get; set; }
        public string Text { get; set; }
        public string Count_Ok { get; set; }
        public string Count_Not_Good { get; set; }

        public List<mFirmOrder> SPN111FirmOrderSearch(string ps_ProductionMonth, string ps_PartNo, string ps_PlantCode, string ps_LineCode, string ps_OrderType)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mFirmOrder>("Firm_Order_Search", new
            {
                                            PROD_MONTH = ps_ProductionMonth,
                                            PART_NO = ps_PartNo,
                                            PLANT_CD = ps_PlantCode,
                                            LINE_CD = ps_LineCode,
                                            ORDER_TYPE = ps_OrderType
                                        });

            if (ps_PartNo == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }

        public List<mFirmOrder> SPN111FirmOrderCountOkNotGood(string ps_ProductionMonth, string ps_PartNo, string ps_PlantCode, string ps_LineCode, string ps_OrderType)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();                    

            var l = db.Fetch<mFirmOrder>("Firm_Order_Count_Ok_Not_Good", new
            {
                PROD_MONTH = ps_ProductionMonth,
                PART_NO = ps_PartNo,
                PLANT_CD = ps_PlantCode,
                LINE_CD = ps_LineCode,
                ORDER_TYPE = ps_OrderType
            });

            db.Close();
            return l.ToList();
        }

        public List<mFirmOrder> SPN111FirmOrderDownload(string pD_ProductionMonth, string pD_PartNo, string pD_PlantCode, string pD_LineCode, string pD_OrderType)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mFirmOrder>("Firm_Order_Download", new
            {
                PROD_MONTH = pD_ProductionMonth,
                PART_NO = pD_PartNo,
                PLANT_CD = pD_PlantCode,
                LINE_CD = pD_LineCode,
                ORDER_TYPE = pD_OrderType
            });
            if (pD_PartNo == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }

        public string SPN111CallbackUploadExcelToSQL(string ProdMonth, string OrderType, string Timing, string PartNo, string PartName,string  Model, string QTY_N, string QTY_N_1, string QTY_N_2, string QTY_N_3, string CreatedBy, string CreatedDT, string ProcessId)
        {

            string result = string.Empty;
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Execute("Firm_Order_Upload_Temportary_Table",
                    new { PROD_MONTH = ProdMonth },
                    new { ORDER_TYPE = OrderType },
                    new { TIMING = Timing },
                    new { PART_NO = PartNo },
                    new { PART_NAME = PartName },
                    new { MODEL = Model },
                    new { QTY_N = QTY_N },
                    new { QTY_N_1 = QTY_N_1 },
                    new { QTY_N_2 = QTY_N_2 },
                    new { QTY_N_3 = QTY_N_3 },
                    new { ORDER_DT = CreatedDT },
                    new { CREATED_BY = "Sys" },
                    new { CREATED_DT = CreatedDT },
                    new { PROCESS_ID = ProcessId }
                );
                db.Close();

                result = "Successfully";
            }
            catch (Exception err)
            {
                result = "error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public string SPN111CheckBasicValidation(string ProcessId, string Created_By)
        {
            string result = string.Empty;
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mFirmOrder>("Firm_Order_Check_Basic_Validation", new { PROCESS_ID = ProcessId }, new { CREATED_BY = Created_By });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text.ToString();
                }

                if (result == "SUCCESS")
                {
                    return "Upload Successfully";
                }
                else
                {
                    return result;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public string SPN111CheckDataValidation(string ProcessId, string ProdMonth, string Created_By)
        {
            string result = string.Empty;
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mFirmOrder>("Firm_Order_Check_Data_Validation", new { PROCESS_ID = ProcessId }, new { PROD_MONTH = ProdMonth }, new { CREATED_BY = Created_By });
                db.Close();
                    
                return result = "Upload Successfully";
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        public string SPN111Confirm(string pC_Firmorder)
        {
            string result = string.Empty;

            try
            {

                IDBContext db = DatabaseManager.Instance.GetContext();

                string[] PartList = pC_Firmorder.Split(',');
                string paramProdMonth = string.Empty;
                string paramOrderType = string.Empty;
                string paramPartNo = string.Empty;
                string paramFlagComplete= string.Empty;

                for (int i = 0; i < PartList.Length; i++)
                {
                    string[] paramlist = (PartList[i].ToString()).Split('|');

                    paramProdMonth = paramlist[0].ToString();
                    paramOrderType = paramlist[1].ToString() == "Reguler" ? "R" : "E";
                    paramPartNo = paramlist[2].ToString();
                    paramFlagComplete = paramlist[3].ToString();

                    if (paramFlagComplete == "Temporary")
                    {

                        var l = db.Fetch<mSourceList>("Firm_Order_Confirm",
                            new { PROD_MONTH = paramProdMonth },
                            new { ORDER_TYPE = paramOrderType },
                            new { PART_NO = paramPartNo },
                            new { CHANGED_BY = "Sys" },
                            new { MSG_TEXT = "" });
                        db.Close();

                        int cekjmlh = PartList.Length - i;
                        if (cekjmlh == 1)
                        {
                            foreach (var message in l)
                            {
                                result = message.MSG_TEXT;
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                result = "error | " + Convert.ToString(err.Message);
            }

            return result;
        }
    }
}
