﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mFileLogMonitoring
    {
        public DateTime? DATE { get; set; }
        public string FILE_NAME{ get; set; }
        public DateTime? VERIFICATION_START_DT { get; set; }
        public DateTime? VERIFICATION_END_DT { get; set; }
        public string VERIFICATION_STATUS { get; set; }
        public string VERIFICATION_PID { get; set; }
        public DateTime? SEND_ORDER_START_DT { get; set; }
        public DateTime? SEND_ORDER_END_DT { get; set; }
        public string SEND_ORDER_STATUS { get; set; }
        public string SEND_ORDER_PID { get; set; }

        public string PROCESS_ID { get; set; }
        public string SEQUENCE_NUMBER { get; set; }
        public string LOCATION { get; set; }
        public string MSG_TYPE { get; set; }
        public string MESSAGE { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }

        //Get data order
        public List<mFileLogMonitoring> getListFileLogMonitoring(string p_DATE_FROM, string p_DATE_TO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mFileLogMonitoring>("FileLogMonitoring_Search",
                                  
                  new { DATE_FROM = p_DATE_FROM },
                  new { DATE_TO = p_DATE_TO }
                  );
            db.Close();
            return l.ToList();
        }

        public List<mFileLogMonitoring> getListFileLogMontoringDetail(string p_PROCESS_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mFileLogMonitoring>("getListLogMontoringDetail",
               new { PROCESS_ID = p_PROCESS_ID }
               );
            db.Close();
            return l.ToList();
        }
    }
}