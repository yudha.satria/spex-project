﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class ReadyCargoCheck
    {
        public string ITEM {get;set;}
        public string LOG { get; set; }
        public string LEVEL { get; set; }

        public List<ReadyCargoCheck> GetReadyCargoCheck(string BUYER_CD, string PD_CD, string CONTAINER_NO, DateTime VANNING_DT, string USER_ID, string PACKING_COMPANY)
        {
            List<ReadyCargoCheck> ListReadyCargoCheck = new List<ReadyCargoCheck>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            ListReadyCargoCheck = db.Fetch<ReadyCargoCheck>("GetReadyCargoCheck",
                                                            new { pBUYER_CD = BUYER_CD },
                                                                new { pPD_CD = PD_CD },
                                                                new { pCONTAINER_NO = CONTAINER_NO },
                                                                new { pVANNING_DT = VANNING_DT },
                                                                new { pPACKING_COMPANY = PACKING_COMPANY },
                                                                new { pUSER_ID = USER_ID }).ToList();
            db.Close();
            return ListReadyCargoCheck;
        }
    }
}