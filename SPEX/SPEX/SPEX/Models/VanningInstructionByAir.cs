﻿using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class VanningInstructionByAir
    {
        public string BUYER_CD { get; set; }
        public string PD_CD { get; set; }
        public string ORIGINAL { get; set; }
        public string COUNTRY { get; set; }
        public string INVOICE_NO { get; set; }
        public string VANNING_NO { get; set; }
        public string SI_NO { get; set; }
        public string ETD { get; set; }
        public string VANNING_DT { get; set; }
        public string FA { get; set; }
        public string CASE { get; set; }
        public string PACKING_COMPANY { get; set; }

        //khusus detail
        public double GROSS_WEIGHT { get; set; }
        public double NET_WEIGHT { get; set; }
        public double MEASUREMENT { get; set; }
        public double LENGTH { get; set; }
        public double WIDTH { get; set; }
        public double HEIGHT { get; set; }
        public double AMOUNT { get; set; }
        public string PACKAGE_DT { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public List<string> GetPackingCompany()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("GetPackingCompany").ToList();
            db.Close();
            return q;
        }

        public List<string> GetOriginal()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("VANNING_INSTRUCTION_ORIGINAL_SEARCH").ToList();
            db.Close();
            return q;
        }

        public List<mInvoice> GetINVOICE()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<mInvoice>("VANNING_INSTRUCTION_INVOICE_SEARCH").ToList();
            db.Close();
            return q;
        }

        public List<string> Get_READY_CARGO_CASE()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("VANNING_INSTRUCTION_READY_CARGO_CASE_SEARCH").ToList();
            db.Close();
            return q;
        }

        public List<string> Get_FA()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("VANNING_INSTRUCTION_FA_SEARCH").ToList();
            db.Close();
            return q;
        }

        public List<VanningInstructionByAir> GetVanningInstructionByAir(string Original, string Vanning_No, string VANNING_DT_FROM, string VANNING_DT_TO, string Complete_Data, string CASE_NO, string INVOICE_NO, string SI_NO, string ETD_FROM, string ETD_TO, string FORWARDING_AGENT, string PACKING_COMPANY)
        {
            List<VanningInstructionByAir> q = new List<VanningInstructionByAir>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            q = db.Fetch<VanningInstructionByAir>("VanningInstructionByAirSearch",
                                                   new { Original = Original },
                                                   new { Vanning_No = Vanning_No },
                                                   new { VANNING_DT_FROM = VANNING_DT_FROM },
                                                   new { VANNING_DT_TO = VANNING_DT_TO },
                                                   new { Complete_Data = Complete_Data },
                                                   new { CASE_NO = CASE_NO },
                                                   new { INVOICE_NO = INVOICE_NO },
                                                   new { SI_NO = SI_NO },
                                                   new { ETD_FROM = ETD_FROM },
                                                   new { ETD_TO = ETD_TO },
                                                   new { FORWARDING_AGENT = FORWARDING_AGENT },
                                                   new { PACKING_COMPANY = PACKING_COMPANY }
                                                   ).ToList();
            return q;
        }

        public List<VanningInstructionByAir> GetVanningInstructionByAirDetail(string Original, string Vanning_No, string VANNING_DT_FROM, string VANNING_DT_TO, string Complete_Data, string CASE_NO, string INVOICE_NO, string SI_NO, string ETD_FROM, string ETD_TO, string FORWARDING_AGENT, string PACKING_COMPANY)
        {
            List<VanningInstructionByAir> q = new List<VanningInstructionByAir>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            q = db.Fetch<VanningInstructionByAir>("VanningInstructionByAirDetailSearch",
                                                   new { Original = Original },
                                                   new { Vanning_No = Vanning_No },
                                                   new { VANNING_DT_FROM = VANNING_DT_FROM },
                                                   new { VANNING_DT_TO = VANNING_DT_TO },
                                                   new { Complete_Data = Complete_Data },
                                                   new { CASE_NO = CASE_NO },
                                                   new { INVOICE_NO = INVOICE_NO },
                                                   new { SI_NO = SI_NO },
                                                   new { ETD_FROM = ETD_FROM },
                                                   new { ETD_TO = ETD_TO },
                                                   new { FORWARDING_AGENT = FORWARDING_AGENT },
                                                   new { PACKING_COMPANY = PACKING_COMPANY }
                                                   ).ToList();
            return q;
        }

        public mShippingInstruction getShippingInstructionDownload(string INV_NO, string ORIGINAL)
        {

            mShippingInstruction q = new mShippingInstruction();
            IDBContext db = DatabaseManager.Instance.GetContext();
            q = db.Fetch<mShippingInstruction>("VanningInstructionSiDownload",
                                                   new { INV_NO = INV_NO },
                                                   new { ORIGINAL = ORIGINAL }).First();
            List<ReadyCargoCase> Cs = new List<ReadyCargoCase>();
            Cs = db.Fetch<ReadyCargoCase>("VANNING_INSTRUCTION_BY_AIR_DOWNLOAD_ATTACHMENT",
                                                   new { INV_NO = INV_NO }).ToList();
            q.AttachmentReadyCargoCase = Cs;
            return q;
        }


        public string GetReportDBPath()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("Get_Report_DB_Path");
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault().Text : "";
        }

        public void getShippingInstructionAttachment(string VANNING_NO, string PICKUP_DATE)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.Execute("VANNING_INSTRUCTION_BY_AIR_UPDATE_PICKUP_DATE",
                                                  new { VANNING_NO = VANNING_NO },
                                                  new { PICKUP_DATE = PICKUP_DATE });
            db.Close();
        }

        //add agi 2017-09-07
        public mInvoice getInvoice(string pInvoiceNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mInvoice> result = db.Fetch<mInvoice>("VanningInstructionByAir_GetInvoice",
                new { INVOICE_NO = pInvoiceNo }
                ).ToList();
            db.Close();

            return result.Count > 0 ? result[0] : null;
        }

        public mInvoice getInvoiceForVanning(string pInvoiceNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<mInvoice> result = db.Fetch<mInvoice>("VanningInstructionByAir_GetInvoiceForVanning",
                new { INVOICE_NO = pInvoiceNo }
                ).ToList();
            db.Close();

            return result.Count > 0 ? result[0] : null;
        }
    }
}