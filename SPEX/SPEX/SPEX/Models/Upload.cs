﻿using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Linq;
//Default Toyota Model
//Default Devexpress Model

namespace SPEX.Models
{
    public static class Upload
    {
        public static string EXcelToSQL(string path,string tb_t_name)
        {
            //String strConnection = "Data Source=.\\SQLEXPRESS;AttachDbFilename='C:\\Users\\Hemant\\documents\\visual studio 2010\\Projects\\CRMdata\\CRMdata\\App_Data\\Database1.mdf';Integrated Security=True;User Instance=True";
            //string excelConnString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1;MAXSCANROWS=0;\"";
            string strConnection = DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString;
            String excelConnString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0\"", path);
            //Create Connection to Excel work book 
            string sheet = "[Sheet1$]";
            string myexceldataquery = "SELECT * FROM " + sheet;
            string resultMessage = string.Empty;
            try
            {
                using (OleDbConnection excelConnection = new OleDbConnection(excelConnString))
                {
                    //Create OleDbCommand to fetch data from Excel 

                    using (OleDbCommand cmd = new OleDbCommand(myexceldataquery, excelConnection))
                    {
                        excelConnection.Open();
                        using (OleDbDataReader dReader = cmd.ExecuteReader())
                        {
                            using (SqlBulkCopy sqlBulk = new SqlBulkCopy(strConnection))
                            {
                                //Give your Destination table name 
                                //try
                                //{
                                sqlBulk.DestinationTableName = tb_t_name;//"spex.TB_T_SHIPPING_SCHEDULE";
                                sqlBulk.WriteToServer(dReader);
                                //resultMessage = "upload in progress";
                                //}
                                //catch ( Exception EX)
                                //{
                                //    resultMessage ="1|"+EX.Message;
                                //}
                            }
                        }
                    }
                }
            }
            catch (Exception EX)
            {
                resultMessage = "1|" + EX.Message;
            }
            return resultMessage;
        }

        public static string EXcelToSQLQuery(string path, string tb_t_name, string query)
        {
            //String strConnection = "Data Source=.\\SQLEXPRESS;AttachDbFilename='C:\\Users\\Hemant\\documents\\visual studio 2010\\Projects\\CRMdata\\CRMdata\\App_Data\\Database1.mdf';Integrated Security=True;User Instance=True";
            //string excelConnString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1;MAXSCANROWS=0;\"";
            string strConnection = DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString;
            String excelConnString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0\"", path);
            //Create Connection to Excel work book 
            //string sheet = "[Sheet1$]";
            //string myexceldataquery = "SELECT * FROM " + sheet;
            string resultMessage = string.Empty;
            try
            {
                using (OleDbConnection excelConnection = new OleDbConnection(excelConnString))
                {
                    //Create OleDbCommand to fetch data from Excel 

                    using (OleDbCommand cmd = new OleDbCommand(query, excelConnection))
                    {
                        excelConnection.Open();
                        using (OleDbDataReader dReader = cmd.ExecuteReader())
                        {
                            using (SqlBulkCopy sqlBulk = new SqlBulkCopy(strConnection))
                            {
                                //Give your Destination table name 
                                //try
                                //{
                                sqlBulk.DestinationTableName = tb_t_name;//"spex.TB_T_SHIPPING_SCHEDULE";
                                sqlBulk.WriteToServer(dReader);
                                //resultMessage = "upload in progress";
                                //}
                                //catch ( Exception EX)
                                //{
                                //    resultMessage ="1|"+EX.Message;
                                //}
                            }
                        }
                        excelConnection.Close();
                    }
                }
            }
            catch (Exception EX)
            {
                resultMessage = "1|" + EX.Message;
            }
            return resultMessage;
        }

        public static string BulkCopyCSV(string filename, string tablename,char delimiter)
        {
            string resultMessage=string.Empty;;
            string connectionString = DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction();
            try
            {
                using (StreamReader file = new StreamReader(filename))
                {
                    CsvReader csv = new CsvReader(file, true, delimiter);
                    //e.UploadedFile.FileContent
                    //FileStream csv = new FileStream(filename, FileMode.CreateNew, FileAccess.ReadWrite);
                    SqlBulkCopy copy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                    copy.DestinationTableName = tablename;
                    
                    copy.WriteToServer(csv);
                    transaction.Commit();
                }
                
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                resultMessage = "1|" + ex.Message;
            }
            finally
            {
                conn.Close();
            }
            return resultMessage;
        }

        public static string GetDelimiter (Stream st)
        {
            string l = string.Empty;
            StreamReader r = new StreamReader(st);

            string CSV_SEP = Cls.Common.GetDataMasterSystem("CSV", "UploadDelimiter");
            IList<char> ListSeparator = CSV_SEP.ToCharArray();
            while ((l = r.ReadLine()) != null)
            {
                //if (linenum > 0)
                //{
                //TextReader reader = File.OpenText("C:\\todo.txt");
                using (TextReader sr = new StringReader(l))
                {
                    CSV_SEP = Detect(sr, 1, ListSeparator).ToString();
                }
                break;
                //}

            }
            return CSV_SEP;
        }

        public static char Detect(TextReader reader, int rowCount, IList<char> separators)
        {
            IList<int> separatorsCount = new int[separators.Count];

            int character;

            int row = 0;

            bool quoted = false;
            bool firstChar = true;

            while (row < rowCount)
            {
                character = reader.Read();

                switch (character)
                {
                    case '"':
                        if (quoted)
                        {
                            if (reader.Peek() != '"') // Value is quoted and 
                                // current character is " and next character is not ".
                                quoted = false;
                            else
                                reader.Read(); // Value is quoted and current and 
                            // next characters are "" - read (skip) peeked qoute.
                        }
                        else
                        {
                            if (firstChar) 	// Set value as quoted only if this quote is the 
                                // first char in the value.
                                quoted = true;
                        }
                        break;
                    case '\n':
                        if (!quoted)
                        {
                            ++row;
                            firstChar = true;
                            continue;
                        }
                        break;
                    case -1:
                        row = rowCount;
                        break;
                    default:
                        if (!quoted)
                        {
                            int index = separators.IndexOf((char)character);
                            if (index != -1)
                            {
                                ++separatorsCount[index];
                                firstChar = true;
                                continue;
                            }
                        }
                        break;
                }

                if (firstChar)
                    firstChar = false;
            }

            int maxCount = separatorsCount.Max();

            return maxCount == 0 ? '\0' : separators[separatorsCount.IndexOf(maxCount)];
        }

    }
}