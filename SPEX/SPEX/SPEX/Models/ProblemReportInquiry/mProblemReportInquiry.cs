﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Muhajir Shiddiq
namespace SPEX.Models
{
    public class mProblemReportInquiry
    {
        public string PR_CD { get; set; }
        public string PR_DATE { get; set; }
        public string SUB_MANIFEST_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string MANIFEST_ITEM_NO { get; set; }
        public string DOCK_CD { get; set; }
        public string STOCK_FLAG { get; set; }
        public string PART_NO { get; set; }
        public string SOURCE_TYPE { get; set; }
        public string PROD_PURPOSE { get; set; }
        public string ICS_REASON { get; set; }
        public string TMAP_PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PROBLEM_CATEGORY_CD { get; set; }
        public string PROBLEM_ORIGIN_CD { get; set; }
        public string PROBLEM_SUB_ORIGIN { get; set; }
        public string COST_CENTER { get; set; }
        public string STATUS { get; set; }
        public string SEND_STATUS { get; set; }
        public string FINISHED_PR_DT { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
        public string PROBLEM_QTY { get; set; }
        public string QTY { get; set; }
        public string KANBAN_ID { get; set; }
        public string NEW_KANBAN_ID { get; set; }
        public string NEW_MANIFEST_NO { get; set; }
        public string Text { get; set; }
        public string CLIENT_ID { get; set; }
        public string MOVEMENT_TYPE { get; set; }
        public string PLANT_CODE { get; set; }
        public string STORAGE_LOCATION { get; set; }
        public string POSTING_DATE { get; set; }
        public string DESCRIPTION { get; set; }
        public string BAP_NO { get; set; }



        public List<mProblemReportInquiry> getListProblemReport(string pPR_CD , string pPROBLEM_DATE_FROM , string pPROBLEM_DATE_TO , string pPART_NO , string pSTATUS_PROBLEM , string pSUPPLIER , string pSUB_SUPPLIER , string pSUPPLIER_CREATOR , string pPROBLEM_ORIGIN , string pPROBLEM_CATEGORY , string pKANBAN_ID , string pSEND_STATUS,string pTMAP_PART_NO)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

           
            var l = db.Fetch<mProblemReportInquiry>("ProblemReportCreation/SearchProblemReportCreationInquiry",                     
                    new { I_PR_CD = pPR_CD },
                    new { I_PROBLEM_DATE_FROM = pPROBLEM_DATE_FROM },
                    new { I_PROBLEM_DATE_TO = pPROBLEM_DATE_TO },
                    new { I_PART_NO = pPART_NO },
                    new { I_STATUS_PROBLEM = pSTATUS_PROBLEM },
                    new { I_SUPPLIER = pSUPPLIER },
                    new { I_SUB_SUPPLIER = pSUB_SUPPLIER },
                    new { I_SUPPLIER_CREATOR = pSUPPLIER_CREATOR },
                    new { I_PROBLEM_ORIGIN = pPROBLEM_ORIGIN },
                    new { I_PROBLEM_CATEGORY = pPROBLEM_CATEGORY },
                    new { I_KANBAN_ID = pKANBAN_ID },
                    new { I_SEND_STATUS = pSEND_STATUS },
                    new { I_TMAP_PART_NO = pTMAP_PART_NO}
            );
            db.Close();
            return l.ToList();
        }


        public List<mProblemReportInquiry> getListDownloadProblemReport(string pPR_CD, string pPROBLEM_DATE_FROM, string pPROBLEM_DATE_TO, string pPART_NO, string pSTATUS_PROBLEM, string pSUPPLIER, string pSUB_SUPPLIER, string pSUPPLIER_CREATOR, string pPROBLEM_ORIGIN, string pPROBLEM_CATEGORY, string pKANBAN_ID, string pSEND_STATUS, string pTMAP_PART_NO)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mProblemReportInquiry>("ProblemReportCreation/DownloadProblemReportCreationInquiry",                     
                    new { I_PR_CD = pPR_CD },
                    new { I_PROBLEM_DATE_FROM = pPROBLEM_DATE_FROM },
                    new { I_PROBLEM_DATE_TO = pPROBLEM_DATE_TO },
                    new { I_PART_NO = pPART_NO },
                    new { I_STATUS_PROBLEM = pSTATUS_PROBLEM },
                    new { I_SUPPLIER = pSUPPLIER },
                    new { I_SUB_SUPPLIER = pSUB_SUPPLIER },
                    new { I_SUPPLIER_CREATOR = pSUPPLIER_CREATOR },
                    new { I_PROBLEM_ORIGIN = pPROBLEM_ORIGIN },
                    new { I_PROBLEM_CATEGORY = pPROBLEM_CATEGORY },
                    new { I_KANBAN_ID = pKANBAN_ID },
                    new { I_SEND_STATUS = pSEND_STATUS },
                    new { I_TMAP_PART_NO = pTMAP_PART_NO }
            );
            db.Close();
            return l.ToList();
        }


        public List<mProblemReportInquiry> getListDetailKanban(string I_PR_CD, string I_SUB_MANIFEST_NO, string I_PART_NO)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mProblemReportInquiry>("ProblemReportCreation/SearchProblemReportCreationInquiryDetail",
                    new { I_PR_CD = I_PR_CD },
                    new { I_SUB_MANIFEST_NO = I_SUB_MANIFEST_NO },
                    new { I_PART_NO = I_PART_NO }
            );
            db.Close();
            return l.ToList();
        }


        public List<mProblemReportInquiry> getListGiProblemReport(string pPR_CD, string pPROBLEM_DATE_FROM, string pPROBLEM_DATE_TO, string pPART_NO, string pSTATUS_PROBLEM, string pSUPPLIER, string pSUB_SUPPLIER, string pSUPPLIER_CREATOR, string pPROBLEM_ORIGIN, string pPROBLEM_CATEGORY, string pKANBAN_ID, string pSEND_STATUS, string pTMAP_PART_NO)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mProblemReportInquiry>("ProblemReportCreation/DownloadProblemReportCreationInquiryGi",
                    new { I_PR_CD = pPR_CD },
                    new { I_PROBLEM_DATE_FROM = pPROBLEM_DATE_FROM },
                    new { I_PROBLEM_DATE_TO = pPROBLEM_DATE_TO },
                    new { I_PART_NO = pPART_NO },
                    new { I_STATUS_PROBLEM = pSTATUS_PROBLEM },
                    new { I_SUPPLIER = pSUPPLIER },
                    new { I_SUB_SUPPLIER = pSUB_SUPPLIER },
                    new { I_SUPPLIER_CREATOR = pSUPPLIER_CREATOR },
                    new { I_PROBLEM_ORIGIN = pPROBLEM_ORIGIN },
                    new { I_PROBLEM_CATEGORY = pPROBLEM_CATEGORY },
                    new { I_KANBAN_ID = pKANBAN_ID },
                    new { I_SEND_STATUS = pSEND_STATUS },
                    new { I_TMAP_PART_NO = pTMAP_PART_NO }
            );
            db.Close();
            return l.ToList();
        }


        

    }
}
