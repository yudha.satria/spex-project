﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using SPEX.Cls;

namespace SPEX.Models
{

    public class PackingPartMaster
    {
        public string PART_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_PLANT { get; set; }
        public string SUP_NAME { get; set; }
        public string MAD { get; set; }
        public string DOCK_CD { get; set; }
        public string SUBTITUTION_PART_NO { get; set; }
        public string SUBTITUTION_CD { get; set; }
        public string OLD_PART_NO { get; set; }
        public string KANBAN_NO { get; set; }
        public string PART_NAME { get; set; }
        public string PCK_TYPE { get; set; }
        public int KANBAN_QTY { get; set; }
        public int SAFETY_PCS { get; set; }
        public string PMSP_FLAG { get; set; }
        public string PRMCFC { get; set; }
        public int PRMPCT { get; set; }
        public int PRMMS { get; set; }
        public int PRMXS { get; set; }
        public string PRMCD { get; set; }
        public int LINE_LEAD_TIME { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public DateTime? START_DT { get; set; }
        public DateTime? END_DT { get; set; }

        public string PART_TYPE { get; set; }
        public decimal? PART_NET_WEIGHT { get; set; }
        public decimal? PART_GROSS_WEIGHT { get; set; }
        public decimal? PART_LENGTH { get; set; }
        public decimal? PART_WIDTH { get; set; }
        public decimal? PART_HEIGHT { get; set; }
        public string COO { get; set; }
        public string DG_FLAG { get; set; }
        public string PLANT { get; set; }
        public string SLOC { get; set; }
        public string PROD_PURPOSE { get; set; }
        public string SOURCE_TYPE { get; set; }
        public string TEXT { get; set; }
        public string DOWNLOAD_KEY { get; set; }

        //add agi 2017-08-11
        public string REORDER_FLAG { get; set; }
        public string COMPANY_CD { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string CASE_GROUPING { get; set; }
        public int DAD { get; set; }

        public string FUNCTION_ID { get; set; }
        public string PROCESS_ID { get; set; }

        public string Teks { get; set; }

        //public string Text { get; set; }

        public List<mSystemMaster> PART_TYPE_LIST { get { return Common.GetPartTypeList(); } }
        public List<mSystemMaster> DG_GARGO_LIST { get { return Common.GetDangerFlagList(); } }
        //add agi 2017-08-11
        public List<mSystemMaster> REORDER_FLAG_LIST
        {
            get
            {
                return (new mSystemMaster()).GetSystemValueList("REORDER_FLAG", "1;0", "3").Where(a => a.SYSTEM_VALUE != string.Empty).OrderByDescending(a => a.SYSTEM_CD).ToList();        
            }
        }

        public class PackingPartMasterUpload
        {
            public string DATA_ID { get; set; }
            public string PART_NO { get; set; }
            public string SUPPLIER_CD { get; set; }
            public string SUPPLIER_PLANT { get; set; }
            public string SUB_SUPPLIER_CD { get; set; }
            public string SUB_SUPPLIER_PLANT { get; set; }
            public string SUPP_NAME { get; set; }
            public string DOCK_CD { get; set; }
            public string SUBTITUTION_PART_NO { get; set; }
            public string SUBTITUTION_CD { get; set; }
            public string OLD_PART_NO { get; set; }
            public string KANBAN_NO { get; set; }
            public string PART_NAME { get; set; }
            public string PCK_TYPE { get; set; }
            public string KANBAN_QTY { get; set; }
            public string SAFETY_PCS { get; set; }
            public string PMSP_FLAG { get; set; }
            public string PRMCFC { get; set; }
            public string PRMPCT { get; set; }
            public string PRMMS { get; set; }
            public string PRMXS { get; set; }
            public string PRMCD { get; set; }
            public string LINE_LEAD_TIME { get; set; }
            public string START_DT { get; set; }
            public string END_DT { get; set; }
            public string PART_TYPE { get; set; }
            public string PART_NET_WEIGHT { get; set; }
            public string PART_GROSS_WEIGHT { get; set; }
            public string PART_LENGTH { get; set; }
            public string PART_WIDTH { get; set; }
            public string PART_HEIGHT { get; set; }
            public string COO { get; set; }
            public string DG_FLAG { get; set; }
            public string PLANT { get; set; }
            public string SLOC { get; set; }
            public string PROD_PURPOSE { get; set; }
            public string SOURCE_TYPE { get; set; }
        }

        public List<PackingPartMaster> GetData(PackingPartMaster PM, string Mode)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<PackingPartMaster> l = db.Fetch<PackingPartMaster>("PackingPartMasterSearch",
                new { PART_NO = Convert.ToString(PM.PART_NO) },
                new { SUPPLIER_CD = Convert.ToString(PM.SUPPLIER_CD) },
                new { SUB_SUPPLIER_CD = Convert.ToString(PM.SUB_SUPPLIER_CD) },
                new { PMSP_FLAG = Convert.ToString(PM.PMSP_FLAG) },
                new { DOCK_CD = Convert.ToString(PM.DOCK_CD) },
                new { KANBAN_NO = Convert.ToString(PM.KANBAN_NO) },
                new { SUPPLIER_PLANT = Convert.ToString(PM.SUPPLIER_PLANT) },
                new { PART_TYPE = Convert.ToString(PM.PART_TYPE) },
                new { DG_FLAG = Convert.ToString(PM.DG_FLAG) },
                new { Mode = Mode },
                new { REORDER_FLAG = Convert.ToString(PM.REORDER_FLAG) },
                new { COMPANY_CD = Convert.ToString(PM.COMPANY_CD) },
                new { RCV_PLANT_CD = Convert.ToString(PM.RCV_PLANT_CD) },
                new { CASE_GROUPING = Convert.ToString(PM.CASE_GROUPING) }
                ).ToList();
            
            db.Close();
            return l;
        }

        public string UpdateData(PackingPartMaster PM)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                var l = db.Fetch<string>("PackingPartMasterUpdate",
                    new { PART_NO = PM.PART_NO },
                    new { SUPPLIER_CD = PM.SUPPLIER_CD },
                    new { SUPPLIER_PLANT = PM.SUPPLIER_PLANT },
                    new { SUB_SUPPLIER_CD = PM.SUB_SUPPLIER_CD },
                    new { SUB_SUPPLIER_PLANT = PM.SUB_SUPPLIER_PLANT },
                    new { DOCK_CD = PM.DOCK_CD },
                    new { SUBTITUTION_PART_NO = PM.SUBTITUTION_PART_NO },
                    new { SUBTITUTION_CD = PM.SUBTITUTION_CD },
                    new { OLD_PART_NO = PM.OLD_PART_NO },
                    new { KANBAN_NO = PM.KANBAN_NO },
                    new { PART_NAME = PM.PART_NAME },
                    new { PCK_TYPE = PM.PCK_TYPE },
                    new { KANBAN_QTY = PM.KANBAN_QTY },
                    new { SAFETY_PCS = PM.SAFETY_PCS },
                    new { PMSP_FLAG = PM.PMSP_FLAG },
                    new { PRMCFC = PM.PRMCFC },
                    new { PRMPCT = PM.PRMPCT },
                    new { PRMMS = PM.PRMMS },
                    new { PRMXS = PM.PRMXS },
                    new { PRMCD = PM.PRMCD },
                    new { LINE_LEAD_TIME = PM.LINE_LEAD_TIME },
                    new { START_DT = PM.START_DT },
                    new { END_DT = PM.END_DT },
                    new { PART_TYPE = PM.PART_TYPE },
                    new { DG_FLAG = PM.DG_FLAG },
                    new { PART_GROSS_WEIGHT = PM.PART_GROSS_WEIGHT },
                    new { PART_NET_WEIGHT = PM.PART_NET_WEIGHT },
                    new { PART_LENGTH = PM.PART_LENGTH },
                    new { PART_WIDTH = PM.PART_WIDTH },
                    new { PART_HEIGHT = PM.PART_HEIGHT },
                    new { COO = PM.COO },
                    new { PLANT = PM.PLANT },
                    new { SLOC = PM.SLOC },
                    new { PROD_PURPOSE = PM.PROD_PURPOSE },
                    new { SOURCE_TYPE = PM.SOURCE_TYPE },
                    new { CHANGED_BY = PM.CHANGED_BY },
                    new { CHANGED_DT = PM.CHANGED_DT },
                    //add agi 2017-08-11
                    new { REORDER_FLAG = PM.REORDER_FLAG },
                    new { COMPANY_CD = PM.COMPANY_CD },
                    new { RCV_PLANT_CD = PM.RCV_PLANT_CD },
                    new { CASE_GROUPING = PM.CASE_GROUPING }   
                    );
                return l.First();
            }
            catch (Exception ex)
            {
                return "false | error:" + ex.Message;
            }
        }

        public string AddData(PackingPartMaster PM)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                var l = db.Fetch<string>("PackingPartMasterAdd",
                    new { PART_NO = PM.PART_NO },
                    new { SUPPLIER_CD = PM.SUPPLIER_CD },
                    new { SUPPLIER_PLANT = PM.SUPPLIER_PLANT },
                    new { SUB_SUPPLIER_CD = PM.SUB_SUPPLIER_CD },
                    new { SUB_SUPPLIER_PLANT = PM.SUB_SUPPLIER_PLANT },
                    new { DOCK_CD = PM.DOCK_CD },
                    new { SUBTITUTION_PART_NO = PM.SUBTITUTION_PART_NO },
                    new { SUBTITUTION_CD = PM.SUBTITUTION_CD },
                    new { OLD_PART_NO = PM.OLD_PART_NO },
                    new { KANBAN_NO = PM.KANBAN_NO },
                    new { PART_NAME = PM.PART_NAME },
                    new { PCK_TYPE = PM.PCK_TYPE },
                    new { KANBAN_QTY = PM.KANBAN_QTY },
                    new { SAFETY_PCS = PM.SAFETY_PCS },
                    new { PMSP_FLAG = PM.PMSP_FLAG },
                    new { PRMCFC = PM.PRMCFC },
                    new { PRMPCT = PM.PRMPCT },
                    new { PRMMS = PM.PRMMS },
                    new { PRMXS = PM.PRMXS },
                    new { PRMCD = PM.PRMCD },
                    new { LINE_LEAD_TIME = PM.LINE_LEAD_TIME },
                    new { PART_TYPE = PM.PART_TYPE },
                    new { DG_FLAG = PM.DG_FLAG },
                    new { PART_GROSS_WEIGHT = PM.PART_GROSS_WEIGHT },
                    new { PART_NET_WEIGHT = PM.PART_NET_WEIGHT },
                    new { PART_LENGTH = PM.PART_LENGTH },
                    new { PART_WIDTH = PM.PART_WIDTH },
                    new { PART_HEIGHT = PM.PART_HEIGHT },
                    new { COO = PM.COO },
                    new { PLANT = PM.PLANT },
                    new { SLOC = PM.SLOC },
                    new { PROD_PURPOSE = PM.PROD_PURPOSE },
                    new { SOURCE_TYPE = PM.SOURCE_TYPE },
                    new { CREATED_BY = PM.CREATED_BY },
                    new { CREATED_DT = PM.CREATED_DT },
                    new { START_DT = PM.START_DT },
                    new { END_DT = PM.END_DT },
                    //add agi 2017-08-11
                    new { REORDER_FLAG = PM.REORDER_FLAG },
                    new { COMPANY_CD = PM.COMPANY_CD },
                    new { RCV_PLANT_CD = PM.RCV_PLANT_CD },
                    new { CASE_GROUPING = PM.CASE_GROUPING }   ,
                    new { DAD=PM.DAD}
                    );
                return l.First();
            }
            catch (Exception ex)
            {
                return "false | error:" + ex.Message;
            }
        }
        public string DeleteDataTBT()
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("PACKING_PART_TEMP_Delete");
                    scope.Complete();
                }


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

        public string BackgroundProsessSave(string function_id, string module_id, string user_id, long PID)
        {

            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("PACKING_PART_master_call_job",
                       new { function_id = function_id },
                       new { module_id = module_id },
                       new { user_id = user_id },
                       new { PID = PID }
                       );
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result = "1|" + ex.Message;
            }
            return result;
        }
        
        public string delete(List<PackingPartMaster> KeyList)
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {

                db.BeginTransaction();// scope = new TransactionScope())

                foreach (PackingPartMaster key in KeyList)
                {
                    db.Execute("PACKING_PART_master_delete",
                   new { PART_NO = key.PART_NO },
                   new { SUPPLIER_CD = key.SUPPLIER_CD },
                   new { SUPPLIER_PLANT = key.SUPPLIER_PLANT },
                   new { DOCK_CD = key.DOCK_CD }
                   );
                }
                db.CommitTransaction();

                db.Close();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public string InsertTempPartPrice(PackingPartMasterUpload data)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<PackingPartMaster>("PACKING_PART_MASTER_InsertTemp",
                        new { DATA_ID = data.DATA_ID },
                        new { PART_NO = data.PART_NO },
                        new { SUPPLIER_CD = data.SUPPLIER_CD },
                        new { SUPPLIER_PLANT = data.SUPPLIER_PLANT },
                        new { DOCK_CD = data.DOCK_CD },
                        new { SUBTITUTION_PART_NO = data.SUBTITUTION_PART_NO },
                        new { SUBTITUTION_CD = data.SUBTITUTION_CD },
                        new { OLD_PART_NO = data.OLD_PART_NO },
                        new { KANBAN_NO = data.KANBAN_NO },
                        new { PART_NAME = data.PART_NAME },
                        new { PCK_TYPE = data.PCK_TYPE },
                        new { KANBAN_QTY = data.KANBAN_QTY },
                        new { SAFETY_PCS = data.SAFETY_PCS },
                        new { PRMCFC = data.PRMCFC },
                        new { PRMPCT = data.PRMPCT },
                        new { PRMMS = data.PRMMS },
                        new { PRMXS = data.PRMXS },
                        new { PRMCD = data.PRMCD },
                        new { LINE_LEAD_TIME = data.LINE_LEAD_TIME },
                        new { START_DT = data.START_DT },
                        new { END_DT = data.END_DT },
                        new { PART_TYPE = data.PART_TYPE },
                        new { DG_FLAG = data.DG_FLAG },
                        new { PART_GROSS_WEIGHT = data.PART_GROSS_WEIGHT },
                        new { PART_NET_WEIGHT = data.PART_NET_WEIGHT },
                        new { PART_LENGTH = data.PART_LENGTH },
                        new { PART_WIDTH = data.PART_WIDTH },
                        new { PART_HEIGHT = data.PART_HEIGHT },
                        new { PLANT = data.PLANT },
                        new { SLOC = data.SLOC },
                        new { PROD_PURPOSE = data.PROD_PURPOSE },
                        new { SOURCE_TYPE = data.SOURCE_TYPE },
                        new { COO = data.COO }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public List<PackingPartMaster> Download(PackingPartMaster packingPart)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<PackingPartMaster> l = db.Fetch<PackingPartMaster>("PackingPartMasterDownload",
                  new { PART_NO = packingPart.PART_NO },
                  new { SUPPLIER_CD = packingPart.SUPPLIER_CD },
                  new { DOCK_CD = packingPart.DOCK_CD },
                  new { KANBAN_NO = packingPart.KANBAN_NO },
                  new { SUPPLIER_PLANT = packingPart.SUPPLIER_PLANT },
                  new { PART_TYPE = packingPart.PART_TYPE },
                  new { DG_FLAG = packingPart.DG_FLAG },
                  new { DOWNLOAD_KEY = packingPart.DOWNLOAD_KEY }
                  ).ToList();
            db.Close();
            return l;
        }

        public string SinchronizeSupplierName()
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<PackingPartMaster>("SinchronizePackingPart", new
                {
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.TEXT;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public string SinchronizeSupplierNameGridCek(List<PackingPartMaster> KeyList, long pid)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                //var l = db.Fetch<PackingPartMaster>("SinchronizePackingPartGridCek", new
                //{
                //});
                //db.Close();

                //foreach (var message in l)
                //{
                //    result = message.TEXT;
                //}
                db.BeginTransaction();// scope = new TransactionScope())

                foreach (PackingPartMaster key in KeyList)
                {
                    db.Execute("SinchronizePackingPartGridCek",
                   new { PART_NO = key.PART_NO },
                   new { SUPPLIER_CD = key.SUPPLIER_CD },
                   new { SUPPLIER_PLANT = key.SUPPLIER_PLANT },
                   new { DOCK_CD = key.DOCK_CD },
                   new { PROCESS_ID = pid }
                   );
                }
                db.CommitTransaction();

                db.Close();
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        public string DeleteLock(string funcId)
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                //var l = db.Fetch<PackingPartMaster>("TBLock_Delete",
                //    new
                //    {
                //        FuncID = funcId
                //    }
                //);
                //db.Close();
                db.Execute("TBLock_Delete",
                new { FUNCTION_ID = funcId }
                );

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }
    }

}