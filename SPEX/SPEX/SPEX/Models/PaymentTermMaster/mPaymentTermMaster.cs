﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models.PaymentTermMaster
{
    public class mPaymentTermMaster 
    {
        public string PAYMENT_TERM_CD { get; set; }
        public string DESCRIPTION_1 { get; set; }
        public string DESCRIPTION_2 { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string Text { get; set; }
        //PEMANGGILAN DATA YANG AKAN DI SEARCH DAN DI TAMPILKAN (CEKNYA DI SQL)


        public List<mPaymentTermMaster> getPaymentTerm(string pPaymentCd,string pDesc1)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPaymentTermMaster>("GetPaymentMaster",
                new object [] {pPaymentCd , pDesc1 } ); 
            db.Close();
            return l.ToList();

        }

        public List<mPaymentTermMaster> GetListPaymentCD()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPaymentTermMaster>("GETCOMBOPAYMENT");
            db.Close();
            return l.ToList();

        }
        //untuk Delete Data
        public string DeleteData(string p_PAYMENT_TERM_CD)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                if (p_PAYMENT_TERM_CD.Contains(";"))
                {
                    string[] PaymentTermList = p_PAYMENT_TERM_CD.Split(';');
                    for (int i = 0; i < PaymentTermList.Length; i++)
                    {
                        var l = db.Fetch<mPaymentTermMaster>("PaymentTermDelete", new
                        {
                            PAYMENT_TERM_CD = PaymentTermList[i],
                            MSG_TEXT = ""
                        });
                        foreach (var message in l)
                        {
                            result = message.Text;
                        }
                    }
                }
                else
                {
                    var l = db.Fetch<mPaymentTermMaster>("PaymentTermDelete", new
                    {
                        PAYMENT_TERM_CD = p_PAYMENT_TERM_CD,
                        MSG_TEXT = ""
                    });
                    foreach (var message in l)
                    {
                        result = message.Text;
                    }
                }
                db.Close();

            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }
        //untuk Edit Data
        public string UpdateData(string p_PAYMENT_TERM_CD, string p_DESCRIPTION1, string p_DESCRIPTION2, string p_CHANGED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPaymentTermMaster>("PaymentTermUpDate", new
                {
                    PAYMENT_TERM_CD = p_PAYMENT_TERM_CD,
                    DESCRIPTION_1 = p_DESCRIPTION1,
                    DESCRIPTION_2 = p_DESCRIPTION2,
                    CHANGED_BY = p_CHANGED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }

        //untuk Save Add new
        public string SaveData(string p_PAYMENT_TERM_CD, string p_DESCRIPTION1, string p_DESCRIPTION2, string p_CREATED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPaymentTermMaster>("PaymenTermSave", new
                {
                    PAYMENT_TERM_CD = p_PAYMENT_TERM_CD,
                    DESCRIPTION_1 = p_DESCRIPTION1,
                    DESCRIPTION_2 = p_DESCRIPTION2,
                    CREATED_BY = p_CREATED_BY,
                    MSG_TEXT = ""
                });
                db.Close();

                foreach (var message in l)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }
        
    }
}