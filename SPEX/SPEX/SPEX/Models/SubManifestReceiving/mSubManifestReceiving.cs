﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Muhajir Shiddiq
namespace SPEX.Models
{
    public class mSubManifestReceiving
    {
        public string MANIFEST_NO { get; set; }
        public string SUB_MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string PART_NO { get; set; }
        public string TMAP_PART_NO { get; set; }
        public string MANIFEST_TYPE { get; set; }
        public string ORDER_TYPE { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string PLANT_CODE { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_PLANT { get; set; }
        public string DOCK_CD { get; set; }
        public string SUB_ROUTE_CD { get; set; }
        public string RECEIVE_FLAG { get; set; }
        public string ORDER_RELEASE_DT { get; set; }
        public string ARRIVAL_PLAN_DT { get; set; }
        public string ARRIVAL_ACTUAL_DT { get; set; }
        public string ORDER_QTY { get; set; }
        public int RECEIVE_QTY { get; set; }
        public int RECEIVE_KANBAN { get; set; }
        public string KANBAN_ID { get; set; }
        public string SEND_GR_ADM_FLAG { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string Text { get; set; }



        public List<mSubManifestReceiving> getListSubManifestReceiving(string pMANIFEST_NO, string pSUPPLIER_CD, string pTMMIN_ORDER_NO, string pSUB_MANIFEST_NO, string pSUPPLIER_PLANT, string pPART_NO, string pDOCK_CD, string pSUB_SUPPLIER_CD, string pSEND_GR_ADM_FLAG, string pORDER_DATE_FROM,string pORDER_DATE_TO,string pPLAN_RCV_DATE_FROM,string pPLAN_RCV_DATE_TO,string pACTUAL_RCV_DATE_FROM,string pACTUAL_RCV_DATE_TO, string pSUB_SUPPLIER_PLANT)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mSubManifestReceiving>("SubManifestReceiving/SearchSubManifestReceivingMaster",
                new { MANIFEST_NO = pMANIFEST_NO },
                new { SUPPLIER_CD = pSUPPLIER_CD },
                new { TMMIN_ORDER_NO = pTMMIN_ORDER_NO },
                new { SUB_MANIFEST_NO = pSUB_MANIFEST_NO },
                new { SUPPLIER_PLANT = pSUPPLIER_PLANT },
                new { PART_NO = pPART_NO },
                new { DOCK_CD = pDOCK_CD },
                new { SUB_SUPPLIER_CD = pSUB_SUPPLIER_CD },
                new { SEND_GR_ADM_FLAG = pSEND_GR_ADM_FLAG },
                new { ORDER_DATE_FROM = pORDER_DATE_FROM },
                new { ORDER_DATE_TO = pORDER_DATE_TO },
                new { PLAN_RCV_DATE_FROM = pPLAN_RCV_DATE_FROM },
                new { PLAN_RCV_DATE_TO = pPLAN_RCV_DATE_TO },
                new { ACTUAL_RCV_DATE_FROM = pACTUAL_RCV_DATE_FROM },
                new { ACTUAL_RCV_DATE_TO = pACTUAL_RCV_DATE_TO },
                new { SUB_SUPPLIER_PLANT = pSUB_SUPPLIER_PLANT }
                );

            db.Close();
            return l.ToList();
        }

        public List<mSubManifestReceiving> getListSubManifestReceivingDetail(string pMANIFEST_NO, string pSUPPLIER_CD, string pTMMIN_ORDER_NO, string pSUB_MANIFEST_NO, string pSUPPLIER_PLANT, string pPART_NO, string pDOCK_CD, string pSUB_SUPPLIER_CD, string pSEND_GR_ADM_FLAG, string pORDER_DATE_FROM, string pORDER_DATE_TO, string pPLAN_RCV_DATE_FROM, string pPLAN_RCV_DATE_TO, string pACTUAL_RCV_DATE_FROM, string pACTUAL_RCV_DATE_TO, string pSUB_SUPPLIER_PLANT)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mSubManifestReceiving>("SubManifestReceiving/SearchSubManifestReceivingMasterDetail",
                new { MANIFEST_NO = pMANIFEST_NO },
                new { SUPPLIER_CD = pSUPPLIER_CD },
                new { TMMIN_ORDER_NO = pTMMIN_ORDER_NO },
                new { SUB_MANIFEST_NO = pSUB_MANIFEST_NO },
                new { SUPPLIER_PLANT = pSUPPLIER_PLANT },
                new { PART_NO = pPART_NO },
                new { DOCK_CD = pDOCK_CD },
                new { SUB_SUPPLIER_CD = pSUB_SUPPLIER_CD },
                new { SEND_GR_ADM_FLAG = pSEND_GR_ADM_FLAG },
                new { ORDER_DATE_FROM = pORDER_DATE_FROM },
                new { ORDER_DATE_TO = pORDER_DATE_TO },
                new { PLAN_RCV_DATE_FROM = pPLAN_RCV_DATE_FROM },
                new { PLAN_RCV_DATE_TO = pPLAN_RCV_DATE_TO },
                new { ACTUAL_RCV_DATE_FROM = pACTUAL_RCV_DATE_FROM },
                new { ACTUAL_RCV_DATE_TO = pACTUAL_RCV_DATE_TO },
                new { SUB_SUPPLIER_PLANT = pSUB_SUPPLIER_PLANT }
                );

            db.Close();
            return l.ToList();
        }


    }
}
