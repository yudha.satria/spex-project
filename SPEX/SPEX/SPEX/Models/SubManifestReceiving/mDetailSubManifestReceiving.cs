﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Muhajir Shiddiq
namespace SPEX.Models
{
    public class mDetailSubManifestReceiving
    {
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string TMAP_PART_NO { get; set; }
        public string KANBAN_ID { get; set; }
        public string RECEIVE_FLAG { get; set; }
        public string RECEIVE_STATUS { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string Text { get; set; }



        public List<mDetailSubManifestReceiving> getDetailListSubManifestReceiving(string pMANIFEST_NO, string pPART_NO, string pSUB_MANIFEST_NO)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mDetailSubManifestReceiving>("SubManifestReceiving/SearchDetailSubManifestReceivingMaster",
                    new { MANIFEST_NO       = pMANIFEST_NO },
                    new { PART_NO           = pPART_NO },
                    new { SUB_MANIFEST_NO   = pSUB_MANIFEST_NO }
                    );

            db.Close();
            return l.ToList();
        }


    }
}
