﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class PartLabelPrinting
    {
        public string PART_NO { get; set; }
        public string SUPPLIER { get; set; }
        public string PART_NAME { get; set; }
        public int KANBAN_QTY { get; set; }
        public int QTY_PRINT { get; set; }
        public string PACK_MONTH { get; set; }
        public string Text { get; set; }
        public string search_text { get; set; }
        public string search_value{get;set;}

        public class mPartLabelUpload
        {
            public string DATA_ID { get; set; }
            public string PART_NO { get; set; }
            public int QTY { get; set; }
            public string PART_NAME { get; set; }
            public string BATCH_ID { get; set; }
        }

        public List<PartLabelPrinting> getList(string pMad, string pSupplier_Cd, string pSupplierPlant_Cd, string pPart_No, string pQty_Print, string pSearch_Mode, string pBatch_Id)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<PartLabelPrinting>("PartLabelPrinting_GetList",
                  new { MAD = pMad }
                  , new { SUPPLIER_CD = pSupplier_Cd }
                  , new { SUPPLIER_PLANT_CD = pSupplierPlant_Cd }
                  , new { PART_NO = pPart_No }
                  , new { QTY_PRINT = pQty_Print }
                  , new { SEARCH_MODE = pSearch_Mode }
                  , new { BATCH_ID = pBatch_Id }
                  );
            db.Close();

            return l.ToList();
        }

        public int AddTempData(mPartLabelUpload param)
        {
            int result = 0;
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                result = db.Execute("PartLabelPrinting_AddTempUpload",
                    new
                    {
                        DATA_ID = param.DATA_ID,
                        PART_NO = param.PART_NO,
                        QTY = param.QTY,
                        PART_NAME = param.PART_NAME,
                        BATCH_ID = param.BATCH_ID
                    });
                db.Close();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return result;
        }

        public int ClearTempData(string BatchId)
        {
            int result = 0;
            IDBContext db = null;
            try
            {
                db = DatabaseManager.Instance.GetContext();
                result = db.Execute("PartLabelPrinting_ClearTempUpload",
                    new
                    {
                        BATCH_ID = BatchId
                    });
                db.Close();
            }
            catch (Exception)
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return result;
        }

        public string ValidateUploadTemp(string BatchId)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("PartLabelPrinting_ValidateUpload",
                  new
                  {
                      BATCH_ID = BatchId
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }


        public String checkMAD(string pMad)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.SingleOrDefault<String>("PartLabelPrinting_CheckMAD",
                  new { MAD = pMad }
                  );
            db.Close();

            return l;

        }

        public String DoPrint(string ids,string printer,string bid)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.SingleOrDefault<String>("PartLabelPrinting_Print",
                  new { IDS = ids},
                  new { PRINTER = printer },
                  new { BATCH_SESSION = bid }
                  );
            db.Close();

            return l;

        }

        public PartLabelPrinting getPLP(string pPart_No)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<PartLabelPrinting>("PartLabelPrinting_Get",
                  new { PART_NO = pPart_No }
                  );
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault() : new PartLabelPrinting();
        }

        public List<mSystemMaster> GetOBRStatusList()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();

            datas = (new mSystemMaster()).GetSystemValueList("OBR_STATUS", "", "2");
            return datas;
        }

        public string Add(string pCASE_TYPE, decimal? pCASE_NET_WEIGHT, decimal? pCASE_LENGTH, decimal? pCASE_WIDTH, decimal? pCASE_HEIGHT, string pDESCRIPTION, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("Case_Add",
                  new
                  {
                      CASE_TYPE = pCASE_TYPE,
                      CASE_NET_WEIGHT = pCASE_NET_WEIGHT,
                      CASE_LENGTH = pCASE_LENGTH,
                      CASE_WIDTH = pCASE_WIDTH,
                      CASE_HEIGHT = pCASE_HEIGHT,
                      DESCRIPTION = pDESCRIPTION,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();

            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }

            return result;

        }

        public string Update(string pOBR_NO, decimal? pCANCEL_QTY, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("OBR_Update",
                  new
                  {
                      OBR_NO = pOBR_NO,
                      CANCEL_QTY = pCANCEL_QTY,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }


        public string UpdateStatus(string pOBR_NOS, string pStatusID, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("OBR_Update_Status",
                  new
                  {
                      OBR_NOS = pOBR_NOS,
                      STATUS_ID = pStatusID,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }

        public string DeleteTempData()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mVanningCompletion>("VanningCompletion_DeleteTemp");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }


        public List<PartLabelPrinting> getSearchBy()
        {
            List<PartLabelPrinting> listSearch = new List<PartLabelPrinting>();

            listSearch.Add(new PartLabelPrinting { search_value = "", search_text = "--Search By--" });
            listSearch.Add(new PartLabelPrinting { search_value = "mad", search_text = "MAD" });
            listSearch.Add(new PartLabelPrinting { search_value = "part_no", search_text = "Part No" });
            listSearch.Add(new PartLabelPrinting { search_value = "supplier", search_text = "Supplier" });

            return listSearch.ToList();
        }

    }

    public class mPLPSupp
    {
        public string SUPP_CD { get; set; }
        public string SUPP_DESC { get; set; }

        public List<mPLPSupp> GetSuppList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPLPSupp>("GetListSuppMaster",
                  new { }
                  );
            db.Close();

            return l.ToList();
        }
    }
}