﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class FORWARDING_AGENT_AIR_ASSIGNMENT
    {
        public string PD_CD { get; set; }
        public string BUYER_CD { get; set; }
        public string IMPORTER { get; set; }
        public string DESTINATION { get; set; }
        public string COUNTRY { get; set; }
        public string AC_NO_TO_USE { get; set; }
        public string FORWARDER { get; set; }
        public string BEP { get; set; }
        public string ROLE { get; set; }
        public decimal? RANGE_FROM { get; set; }
        public decimal? RANGE_TO { get; set; }
        public string COURIER_COMPANY { get; set; }
        public string REMARKS { get; set; }
        public string DELETION_STATUS { get; set; }
        public string PACKING_COMPANY { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        //key for update
        public string OLD_PD_CD { get; set; }
        public string OLD_BUYER_CD { get; set; }
        public string OLD_FORWARDER { get; set; }
        public string OLD_BEP { get; set; }
        public string OLD_ROLE { get; set; }
        public decimal? OLD_RANGE_FROM { get; set; }
        public decimal? OLD_RANGE_TO { get; set; }
        public string OLD_COURIER_COMPANY { get; set; }
        public string OLD_PACKING_COMPANY { get; set; }


        public List<string> GetDestination()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<string>("ForwardingAgentByAirGetDestination").ToList();
            db.Close();
            return l;
        }

        public List<string> GetCOURIER_COMPANY(string BEP)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<string>("ForwardingAgentByAirGetCOURIER_COMPANY"
                                       , new { BEP = BEP }).ToList();
            db.Close();
            return l;
        }

        public List<string> GetFORWARDER()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<string>("ForwardingAgentByAirGetFORWARDER").ToList();
            db.Close();
            return l;
        }

        public List<string> GetFORWARDERFORM()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<string>("ForwardingAgentByAirGetFORWARDERForm").ToList();
            db.Close();
            return l;
        }
        public List<string> getDELETION_STATUS()
        {
            List<string> DELETION_STATUS = new List<string>();
            DELETION_STATUS.Add("Y");
            DELETION_STATUS.Add("N");
            return DELETION_STATUS;
        }

        public List<string> GetIMPORTER()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<string>("ForwardingAgentByAirGetIMPORTER").ToList();
            db.Close();
            return l;
        }

        public List<string> GetCOUNTRY()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<string>("ForwardingAgentByAirGetCOUNTRY").ToList();
            db.Close();
            return l;
        }

        public List<string> GetAC_NO_TO_USE()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<string>("ForwardingAgentByAirGetAC_NO_TO_USE").ToList();
            db.Close();
            return l;
        }

        public List<string> GetBEP()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<string>("ForwardingAgentByAirGetBEP").ToList();
            db.Close();
            return l;
        }

        public List<string> GetBEPForm(string Forwarder)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<string>("ForwardingAgentByAirGetBEPForm"
                                    , new { Forwarder = Forwarder }).ToList();
            db.Close();
            return l;
        }
        public List<FORWARDING_AGENT_AIR_ASSIGNMENT> GetListForwardingAgentByAir(FORWARDING_AGENT_AIR_ASSIGNMENT model)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<FORWARDING_AGENT_AIR_ASSIGNMENT> l = db.Fetch<FORWARDING_AGENT_AIR_ASSIGNMENT>("ForwardingAgentByAirSearch",
                                                new
                                                {
                                                    BUYER_CD = model.BUYER_CD,
                                                    PD_CD = model.PD_CD,
                                                    DESTINATION = model.DESTINATION,
                                                    COURIER_COMPANY = model.COURIER_COMPANY,
                                                    REMARKS = model.REMARKS,
                                                    FORWARDER = model.FORWARDER,
                                                    DELETION_STATUS = model.DELETION_STATUS,
                                                    IMPORTER = model.IMPORTER,
                                                    COUNTRY = model.COUNTRY,
                                                    AC_NO_TO_USE = model.AC_NO_TO_USE,
                                                    BEP = model.BEP,
                                                    PACKING_COMPANY = model.PACKING_COMPANY
                                                }).ToList();
            db.Close();
            return l;
        }

        public List<string> GetRolesByBEP(string BEP)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<string>("ForwardingAgentByAirGetRoleForm"
                                    , new { BEP = BEP }).ToList();
            db.Close();
            return l;
        }

        public string AddData(FORWARDING_AGENT_AIR_ASSIGNMENT FA)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = string.Empty;
            try
            {
                result = db.Fetch<string>("Forwarding_Agent_By_Air_Add"
                                       , new { AC_NO_TO_USE = FA.AC_NO_TO_USE }
                                       , new { BEP = FA.BEP }
                                       , new { BUYER_CD = FA.BUYER_CD }
                                       , new { COUNTRY = FA.COUNTRY }
                                       , new { COURIER_COMPANY = FA.COURIER_COMPANY }
                                       , new { CREATED_BY = FA.CREATED_BY }
                                       , new { CREATED_DT = FA.CREATED_DT }
                                       , new { DESTINATION = FA.DESTINATION }
                                       , new { FORWARDER = FA.FORWARDER }
                                       , new { IMPORTER = FA.IMPORTER }
                                       , new { PACKING_COMPANY = FA.PACKING_COMPANY }
                                       , new { PD_CD = FA.PD_CD }
                                       , new { RANGE_FROM = FA.RANGE_FROM }
                                       , new { RANGE_TO = FA.RANGE_TO }
                                       , new { REMARKS = FA.REMARKS }
                                       , new { ROLE = FA.ROLE }
                                       //, new { FLIGHT = FA.FLIGHT }
                                       //, new { FLIGHT_DEPARTURE = FA.FLIGHT_DEPARTURE }
                                       //, new { CONNECTING_FLIGHT = FA.CONNECTING_FLIGHT }
                                       //, new { CONNECTING_DEPARTURE = FA.CONNECTING_DEPARTURE }
                                       ).First();
            }
            catch (Exception ex)
            {
                result = "false|" + ex.Message;
            }

            db.Close();
            return result;
        }

        public string UpdateData(FORWARDING_AGENT_AIR_ASSIGNMENT FA)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string message = string.Empty;
            try
            {
                message = db.Fetch<string>("Forwarding_Agent_By_Air_Update"
                                        , new { AC_NO_TO_USE = FA.AC_NO_TO_USE }
                                        , new { BEP = FA.BEP }
                                        , new { BEP = FA.BEP }
                                        , new { BUYER_CD = FA.BUYER_CD }
                                        , new { COUNTRY = FA.COUNTRY }
                                        , new { COURIER_COMPANY = FA.COURIER_COMPANY }
                                        , new { CHANGED_BY = FA.CHANGED_BY }
                                        , new { CHANGED_DT = FA.CHANGED_DT }
                                        , new { DESTINATION = FA.DESTINATION }
                                        , new { FORWARDER = FA.FORWARDER }
                                        , new { IMPORTER = FA.IMPORTER }
                                        , new { PACKING_COMPANY = FA.PACKING_COMPANY }
                                        , new { PD_CD = FA.PD_CD }
                                        , new { RANGE_FROM = FA.RANGE_FROM }
                                        , new { RANGE_TO = FA.RANGE_TO }
                                        , new { REMARKS = FA.REMARKS }
                                        , new { ROLE = FA.ROLE }
                                        //, new { FLIGHT = FA.FLIGHT }
                                        //, new { FLIGHT_DEPARTURE = FA.FLIGHT_DEPARTURE }
                                        //, new { CONNECTING_FLIGHT = FA.CONNECTING_FLIGHT }
                                        //, new { CONNECTING_DEPARTURE = FA.CONNECTING_DEPARTURE }
                                        , new { OLD_PD_CD = FA.OLD_PD_CD }
                                        , new { OLD_BUYER_CD = FA.OLD_BUYER_CD }
                                        , new { OLD_FORWARDER = FA.OLD_FORWARDER }
                                        , new { OLD_BEP = FA.OLD_BEP }
                                        , new { OLD_ROLE = FA.OLD_ROLE }
                                        , new { OLD_RANGE_FROM = FA.OLD_RANGE_FROM }
                                        , new { OLD_RANGE_TO = FA.OLD_RANGE_TO }
                                        , new { OLD_COURIER_COMPANY = FA.OLD_COURIER_COMPANY }
                                        , new { OLD_PACKING_COMPANY = FA.OLD_PACKING_COMPANY }
                                        ).First();
                db.Close();
            }
            catch (Exception ex)
            {
                message = "false|" + ex.Message;

            }
            return message;
        }

        public string DeleteData(List<FORWARDING_AGENT_AIR_ASSIGNMENT> FAs)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string message = "true|Delete Data Succes";

            try
            {
                db.BeginTransaction();
                foreach (FORWARDING_AGENT_AIR_ASSIGNMENT FA in FAs)
                {
                    db.Execute("Forwarding_Agent_By_Air_Delete"
                                           , new { BUYER_CD = FA.BUYER_CD }
                                           , new { PD_CD = FA.PD_CD }
                                           , new { FORWARDER = FA.FORWARDER }
                                           , new { BEP = FA.BEP }
                                           , new { ROLE = FA.ROLE }
                                           , new { RANGE_FROM = FA.RANGE_FROM }
                                           , new { RANGE_TO = FA.RANGE_TO }
                                           , new { COURIER_COMPANY = FA.COURIER_COMPANY }
                                           , new { PACKING_COMPANY = FA.PACKING_COMPANY }
                                           , new { CHANGED_BY = FA.CHANGED_BY }
                                           , new { CHANGED_DT = FA.CHANGED_DT }
                                           );
                }
                db.CommitTransaction();
            }
            catch (Exception ex)
            {
                db.AbortTransaction();
                message = "false|" + ex.Message;
            }

            db.Close();
            return message;
        }

        public FORWARDING_AGENT_AIR_ASSIGNMENT GetFirstForwardingAgent(FORWARDING_AGENT_AIR_ASSIGNMENT Key)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            FORWARDING_AGENT_AIR_ASSIGNMENT FA = db.Fetch<FORWARDING_AGENT_AIR_ASSIGNMENT>("ForwardingAgentByAirTop1Search",
                                                new
                                                {
                                                    BUYER_CD = Key.BUYER_CD,
                                                    PD_CD = Key.PD_CD,
                                                    FORWARDER = Key.FORWARDER,
                                                    BEP = Key.BEP,
                                                    ROLE = Key.ROLE,
                                                    RANGE_FROM = Key.RANGE_FROM,
                                                    RANGE_TO = Key.RANGE_TO,
                                                    COURIER_COMPANY = Key.COURIER_COMPANY,
                                                    PACKING_COMPANY = Key.PACKING_COMPANY
                                                }).First();
            db.Close();
            return FA;
        }


    }
}