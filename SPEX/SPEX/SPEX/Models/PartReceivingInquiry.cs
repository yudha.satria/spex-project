﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class PartReceivingInquiry
    {
        public string BUYERPD_CD { get; set; }
        public string TMAP_ORDER_NO { get; set; }
        public string TMMIN_ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string ORDER_DT { get; set; }
        public string PART_NO { get; set; }
        public string KANBAN_ID { get; set; }
        public string TRANSPORTATION_CD { get; set; }
        public string PRIVILEGE { get; set; }
        public string HANDLING_TYPE { get; set; }
        public string PART_STATUS { get; set; }
        public string PLAN_RECEIVE { get; set; }
        public string RECEIVED_DT { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string SUB_SUPPLIER_CD { get; set; }
        public string SUB_SUPPLIER_PLANT { get; set; }

        //, string pStartReleaseDt, string pEndReleaseDt

        //public List<PartReceivingInquiry> getList(string pBuyer_Pd, string pTmap_Order_No, string pTmmin_Order_No, string pManifest_No
        //    , string pPart_No, string pKanban_Id, string pStrDtFrom, string pStrDtTo, string pItemNo)
        public List<PartReceivingInquiry> getList(string pBuyer_Pd, string pTmap_Order_No, string pTmmin_Order_No, string pManifest_No
            , string pPart_No, string pKanban_Id, string pStrDtFrom, string pStrDtTo, string pItemNo, string pStartReleaseDt="", string pEndReleaseDt="", string pSupplier_Code = "", string pSupplier_Plant = "", string pSubSupplier_Code = "", string pSubSupplier_Plant = "")
        {
            
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<PartReceivingInquiry>("PartReceivingInquiry_GetList",
               new { BUYERPD_CD = pBuyer_Pd }
               , new { TMAP_ORDER_NO = pTmap_Order_No }
               , new { TMMIN_ORDER_NO = pTmmin_Order_No }
               , new { KANBAN_ID = pKanban_Id }
               , new { PART_NO = pPart_No }
               , new { MANIFEST_NO = pManifest_No }
               , new { SORTIR_DT_FROM = pStrDtFrom }
               , new { SORTIR_DT_TO = pStrDtTo }
               , new { ITEM_NO = pItemNo }
               //add agi 2017-10-28
               , new { RELEASE_DATE_FROM  = pStartReleaseDt }
               , new { RELEASE_DATE_TO = pEndReleaseDt }

               , new { SUPPLIER_CD = pSupplier_Code }
               , new { SUPPLIER_PLANT = pSupplier_Plant }
               , new { SUB_SUPPLIER_CD = pSubSupplier_Code }
               , new { SUB_SUPPLIER_PLANT = pSubSupplier_Plant }
             );
            db.Close();
            return l.ToList();
        }

        public List<String> GetBuyerMasterList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<String>("GetListBuyerPD",
                  new { }
                  );
            db.Close();

            return l.ToList();
        }

        public string UpdateStatus(string pOBR_NOS, string pStatusID, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("OBR_Update_Status",
                  new
                  {
                      OBR_NOS = pOBR_NOS,
                      STATUS_ID = pStatusID,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }

        public String doUpdateRecvSts(String kanbanID, String UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var q = db.SingleOrDefault<String>("PartReceivingInquiry_UpdtRecv", new
            {
                KANBAN_ID = kanbanID,
                USER_ID = UserID
            }).ToString();
            db.Close();
            return q;
        }
    }
}