﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Linq;
namespace SPEX.Models
{
    
    public class DynamicUpdate
    {
        public List<DynamicQuery> OriginalData { get; set; }
        public List<DynamicQuery> Newdata { get; set; }
    }
    public class DynamicQueryMaster
    {
        public string PROCESS_NAME { get; set; }
        public string CATEGORY { get; set; }
        public string SQL_QUERY1 { get; set; }
        public string EXT_DOWNLOAD_FILE { get; set; }
        public string PROCESS_DESC { get; set; }
        public string ROLES { get; set; }

    }

    public class DynamicQuery
    {
        public string ColumnName { get; set; }
        public string DataType { get; set; }

        public string PROCESS_NAME { get; set; }

        public string Value { get; set; }
        public int NO { get; set; }

        public string CATEGORY_DESCRIPTION { get; set; }

        public string CATEGORY { get; set; }
        public List<DynamicQuery> GetAllQueryByName(string PROCESS_NAME, string ROLE_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var result = db.Fetch<DynamicQuery>("DynamicQuery_GetAllProcessByName",
                new { PROCESS_NAME = PROCESS_NAME, ROLE_ID = ROLE_ID }).ToList();
            db.Close();
            return result;
        }


        public List<DynamicQuery> GetParameterScr(string ProcessName)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var result = db.Fetch<DynamicQuery>("DynamicQuery_GetParameterScr", new { ProcessName = ProcessName }).ToList();
            db.Close();
            return result;
        }

        public void GET_COLUMN_FOR_DOWNLOAD(string ProcessName, out DataTable DT,out List<DynamicQuery> DQs)
        {
            DT= new DataTable();
            IDBContext db = DatabaseManager.Instance.GetContext();
            DQs  = db.Fetch<DynamicQuery>("DynamicQuery_GET_COLUMN_FOR_DOWNLOAD", new { ProcessName = ProcessName }).ToList();
            foreach (DynamicQuery dq in DQs)
            {
                DT.Columns.Add(dq.ColumnName, typeof(string));
            }
            db.Close();
            //return DT;
        }


        public DataTable ConvertParamToDataTable(List<DynamicQuery> ColumnParameter)
        {
            DataTable DT = new DataTable();
            DT.Columns.Add("NO", typeof(int));
            DT.Columns.Add("VALUE", typeof(string));
            DT.Columns.Add("PROCESS_NAME", typeof(string));
            foreach(DynamicQuery DQ in ColumnParameter)
            {
                 DataRow row = DT.NewRow();;
                 row["NO"] = DQ.NO;
                 row["VALUE"] = DQ.Value;
                 row["PROCESS_NAME"] = DQ.PROCESS_NAME;
                 DT.Rows.Add(row);
            }
            return DT;

        }

        public DataTable GET_DATA_FOR_DOWNLOAD(List<DynamicQuery> ColumnParameter)
        {
            DataTable DTDownload = new DataTable();
            List<DynamicQuery> ColumnResult= new List<DynamicQuery>();

            GET_COLUMN_FOR_DOWNLOAD(ColumnParameter.FirstOrDefault().PROCESS_NAME, out DTDownload, out ColumnResult);

            IDBContext db = DatabaseManager.Instance.GetContext();
            using (SqlConnection conn = new SqlConnection(DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SPEX.sp_DYNAMIC_QUERY_GET_DATA_FOR_DOWNLOAD", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter p = cmd.Parameters.AddWithValue("@PARAM", ConvertParamToDataTable(ColumnParameter));
                p.SqlDbType = SqlDbType.Structured;
                try
                {
                    conn.Open();
                    DTDownload.Load(cmd.ExecuteReader());
                    //using (IDataReader dr = cmd.ExecuteReader())
                    //{
                    //    //--- Return from procedures is -> doc, docTo, docTembusan, docApprover, docAttachment
                    //    int i = 0;


                    //    while (dr.Read())
                    //    {
                    //        DataRow row = DTDownload.NewRow();
                    //        foreach(DynamicQuery column in ColumnResult)
                    //        {
                    //            string a = dr.GetFieldType(i).ToString();
                    //            if (a.ToLower().Contains("string"))
                    //            {
                    //                row[i] = dr.GetString(i);
                    //            }
                    //            if (a.ToLower().Contains("decimal"))
                    //            {
                    //                row[i] = dr.GetDecimal(i);
                    //            }
                    //            if (a.ToLower().Contains("float"))
                    //            {
                    //                row[i] = dr.GetFloat(i);
                    //            }
                    //            if (a.ToLower().Contains("date"))
                    //            {
                    //                row[i] = dr.GetDateTime(i);
                    //            }
                    //            if (a.ToLower().Contains("int"))
                    //            {
                    //                row[i] = dr.GetInt32(i);
                    //            }
                                
                    //            i++;
                    //        }
                    //        DTDownload.Rows.Add(dr);
                    //    }
                    //}

                }
                catch
                {

                }
            }
            
            return DTDownload;
        }

        public string GetExtReport(string PROCESS_NAME)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string Ext = string.Empty;
            Ext = db.Fetch<string>("DynamaicQueryGetExtReport", new { PROCESS_NAME = PROCESS_NAME }).FirstOrDefault();
            db.Close();
            return Ext;
        }

        public string Delete(string PROCESS_NAME)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string Ext = string.Empty;
            Ext = db.Fetch<string>("DynamaicQueryDelete", new { PROCESS_NAME = PROCESS_NAME }).FirstOrDefault();
            db.Close();
            return Ext;
        }

        public string DeleteQuery(List<DynamicQuery> ColumnParameter)
        {
            String Message = "";
            DataTable DTDelete = new DataTable();
            List<DynamicQuery> ColumnResult = new List<DynamicQuery>();
           
                    IDBContext db = DatabaseManager.Instance.GetContext();
                    using (SqlConnection conn = new SqlConnection(DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString))
                    {
                        SqlTransaction transaction;
                        SqlCommand cmd = new SqlCommand("SPEX.sp_DYNAMIC_QUERY_DELETE_EXECUTE_QUERY", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter p = cmd.Parameters.AddWithValue("@PARAM", ConvertParamToDataTable(ColumnParameter));
                        p.SqlDbType = SqlDbType.Structured;
                        conn.Open();
                        string trasaction_name = "DeleteQuery" + ColumnParameter.Where(a => a.ColumnName == "USER_ID").FirstOrDefault().Value;
                        transaction = conn.BeginTransaction(trasaction_name);
                        cmd.Transaction = transaction;
                        //SqlTransaction trans = conn.BeginTransaction();
                        try
                        {
                            using (IDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    Message = dr.GetString(0);
                                }

                            }
                            //trans.Commit();
                            transaction.Commit();
                            cmd.Dispose();
                        }
                        catch (Exception ex)
                        {
                            Message = " |"+ex.Message;
                            transaction.Rollback();
                        }
                        conn.Dispose();
                    }
                    if (Message == string.Empty || Message == "" || Message == null)
                    {
                        Message = " |delete data succesfully";
                    }
                
            
            return Message;
        }

        public string AddQuery(List<DynamicQuery> ColumnParameter)
        {
            String Message = "";
            DataTable DTDownload = new DataTable();
            List<DynamicQuery> ColumnResult = new List<DynamicQuery>();
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                using (SqlConnection conn = new SqlConnection(DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("SPEX.sp_DYNAMIC_QUERY_ADD_EXECUTE_QUERY", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter p = cmd.Parameters.AddWithValue("@PARAM", ConvertParamToDataTable(ColumnParameter));
                    p.SqlDbType = SqlDbType.Structured;
                    conn.Open();
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Message = dr.GetString(0);
                        }

                    }
                }
                if (Message == string.Empty || Message == "" || Message == null)
                {
                    Message = "Add data succesfully";
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return Message;
        }

        public DynamicUpdate GetParameterScrUpdate(string ProcessName)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var result = db.Fetch<DynamicQuery>("DynamicQuery_GetParameterScr", new { ProcessName = ProcessName }).ToList();
            List<DynamicQuery> OriData=result.Where(a=>a.ColumnName.ToLower().Contains("@ori_")).ToList();
            List<DynamicQuery> NewData = result.Where(a => a.ColumnName.ToLower().Contains("@new_")).ToList();
            DynamicUpdate DU = new DynamicUpdate();
            DU.OriginalData = OriData;
            DU.Newdata = NewData;
            db.Close();
            return DU;
        }

        public string UpdateQuery(List<DynamicQuery> ColumnParameter)
        {
            String Message = "";
            DataTable DTDownload = new DataTable();
            List<DynamicQuery> ColumnResult = new List<DynamicQuery>();
            using (SqlConnection conn = new SqlConnection(DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString))
            {
                        IDBContext db = DatabaseManager.Instance.GetContext();
                        SqlTransaction transaction ;
                        SqlCommand cmd = new SqlCommand("SPEX.sp_DYNAMIC_QUERY_UPDATE_EXECUTE_QUERY", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter p = cmd.Parameters.AddWithValue("@PARAM", ConvertParamToDataTable(ColumnParameter));
                        p.SqlDbType = SqlDbType.Structured;
                        conn.Open();
                        string trasaction_name = "UpdateQuery" + ColumnParameter.Where(a => a.ColumnName == "USER_ID").FirstOrDefault().Value;
                        transaction = conn.BeginTransaction(trasaction_name);
                        cmd.Transaction = transaction;
                        try
                        {
                            using (IDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    Message = dr.GetString(0);
                                }

                            }
                            transaction.Commit();
                            cmd.Dispose();
                        }
                        catch (Exception ex)
                        {
                            Message = " |" + ex.Message;
                            transaction.Rollback();
                        }

            }
            if (Message == string.Empty || Message == "" || Message == null)
            {
                        Message = "true|Update data succesfully";
            }
            return Message;
        }

        
        public DynamicQueryMaster GetDataDynamicQueryMaster(string Process_Name)
        {
            DynamicQueryMaster DQM = new DynamicQueryMaster();
            //string Message = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            DQM = db.Fetch<DynamicQueryMaster>("DynamicQuery_GetdDataEditPopUp",
                                new
                                {
                                    PROCESS_NAME = Process_Name                                    
                                }).FirstOrDefault();
            db.Close();
            return DQM;
        }

        public string SaveDynamicDocumentMaster(DynamicQueryMaster DQM)
        {
            string Message = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            Message = db.Fetch<string>("DynamicQuery_AddDynamicQueryMaster",
                                new
                                {
                                    PROCESS_NAME = DQM.PROCESS_NAME,
                                    CATEGORY=DQM.CATEGORY,
                                    EXT_DOWNLOAD_FILE=DQM.EXT_DOWNLOAD_FILE,
                                    SQL_QUERY1=DQM.SQL_QUERY1,
                                    PROCESS_DESC=DQM.PROCESS_DESC,
                                    ROLES=DQM.ROLES
                                }).FirstOrDefault();
            db.Close();
            return Message;
        }

        public string UpdateDynamicDocumentMaster(DynamicQueryMaster DQM)
        {
            string Message = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();

            Message = db.Fetch<string>("DynamicQuery_UpdateDynamicQueryMaster",
                                new
                                {
                                    PROCESS_NAME = DQM.PROCESS_NAME,
                                    CATEGORY = DQM.CATEGORY,
                                    EXT_DOWNLOAD_FILE = DQM.EXT_DOWNLOAD_FILE,
                                    SQL_QUERY1 = DQM.SQL_QUERY1,
                                    PROCESS_DESC = DQM.PROCESS_DESC,
                                    ROLES = DQM.ROLES
                                }).FirstOrDefault();
            db.Close();
            return Message;
        }

        public List<string> GetAllRole()
        {
            List<string> Roles = new List<string>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            Roles = db.Fetch<string>("DynamicQuery_GetAllRole").ToList();
            db.Close();
            return Roles;
        }

        public string RolebackDelete(string PID)
        {
            string Message = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                Message = db.Fetch<string>("DynamicQuery_Rolback_Delete",
                                new
                                {
                                    PID = PID
                                }).FirstOrDefault();
                db.CommitTransaction();
            }
            catch(Exception ex)
            {
                Message = "false|" + ex.Message;
                db.AbortTransaction();
            }
            db.Close();
            return Message;

        }

        public string RolebackUpdate(string PID)
        {
            string Message = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                Message = db.Fetch<string>("DynamicQuery_Rolback_Update",
                                new
                                {
                                    PID = PID
                                }).FirstOrDefault();
                db.CommitTransaction();
            }
            catch(Exception ex)
            {
                Message = "false|" + ex.Message;
                db.AbortTransaction();
            }
            db.Close();
            return Message;

        }
    }
    
}
