﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class BUYER
    {
        public string BUYER_CD { get; set; }
        public string PD_CD { get; set; }
        public string BUYER_NAME { get; set; }
        public string CONSIGNEE_LINE1 { get; set; }
        public string CONSIGNEE_LINE2 { get; set; }
        public string CONSIGNEE_LINE3 { get; set; }
        public string CONSIGNEE_LINE4 { get; set; }
        public string CONSIGNEE_LINE5 { get; set; }
        public string NOTIFY_PARTY_LINE1 { get; set; }
        public string NOTIFY_PARTY_LINE2 { get; set; }
        public string NOTIFY_PARTY_LINE3 { get; set; }
        public string NOTIFY_PARTY_LINE4 { get; set; }
        public string NOTIFY_PARTY_LINE5 { get; set; }
        public string PORT_LOADING_CD { get; set; }
        public string PORT_DISCHARGE_CD { get; set; }
        public string BYAIR_PORT_DISCHARGE_CD { get; set; }
        public string ADDRESS1 { get; set; }
        public string ADDRESS2 { get; set; }
        public string ADDRESS3 { get; set; }
        public string ADDRESS4 { get; set; }
        public string PAYMENT_TERM1 { get; set; }
        public string PAYMENT_TERM2 { get; set; }
        public string SPEC_INSTRUCT1 { get; set; }
        public string SPEC_INSTRUCT2 { get; set; }
        public string SPEC_INSTRUCT3 { get; set; }
        public string SPEC_INSTRUCT4 { get; set; }
        public string SPEC_INSTRUCT5 { get; set; }
        public string PRICE_TERM_DESC { get; set; }
        public string BYAIR_COVER_SI1 { get; set; }
        public string BYAIR_COVER_SI2 { get; set; }
        public string BYAIR_COVER_SI3 { get; set; }
        public string BYAIR_FLIGHT_SI { get; set; }
        public string BYAIR_CONNECTING_FLIGHT_SI { get; set; }
        public DateTime? BYAIR_DEPARTURE_DATE_SI { get; set; }
        public string BYAIR_PORT_LOADING_CD { get; set; }
        public string COUNTRY { get; set; }
        public string PEB_IMPORTER_CD { get; set; }
        public string PEB_BUYER_NAME { get; set; }
        public string PEB_BUYER_ADDRESS { get; set; }
        public string FORMD_CONSIGNEE_COMP { get; set; }
        public string FORMD_CONSIGNEE_ADDRESS { get; set; }
        public string FORMD_CONSIGNEE_COUNTRY { get; set; }
        public string FORMD_CONSIGNEE_COUNTRY_CD { get; set; }
        public string FREIGHT_PAYABLE { get; set; }
        public string WAYBILL_REQUIRED { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }


        public string Text { get; set; }

        public List<BUYER> getBUYER(BUYER BYR)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<BUYER> l = db.Fetch<BUYER>("BUYER_Search",
                  new { BUYER_CD = BYR.BUYER_CD },
                  new { BUYER_NAME = BYR.BUYER_NAME }
                  ).ToList();
            db.Close();
            return l;
        }

        //Delete Data
        public string DeleteData(string PARAMS, string USER_ID)
        {
            string result = "Success |Delete Data Success";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                result = db.Fetch<string>("BUYER_Delete", new
                {
                    PARAMS = PARAMS,
                    USER_ID = USER_ID
                }).First();
                db.Close();
            }
            catch (Exception ex)
            {
                result = "Error|" + ex.Message;
            }
            return result;
        }

        //Save Data
        public string SaveData(BUYER BYR)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                result = db.Fetch<string>("BUYER_Save", new
                {
                    ADDRESS1 = BYR.ADDRESS1,
                    ADDRESS2 = BYR.ADDRESS2,
                    ADDRESS3 = BYR.ADDRESS3,
                    ADDRESS4 = BYR.ADDRESS4,
                    BUYER_CD = BYR.BUYER_CD,
                    BUYER_NAME = BYR.BUYER_NAME,
                    BYAIR_PORT_LOADING_CD = BYR.BYAIR_PORT_LOADING_CD,
                    CONSIGNEE_LINE1 = BYR.CONSIGNEE_LINE1,
                    CONSIGNEE_LINE2 = BYR.CONSIGNEE_LINE2,
                    CONSIGNEE_LINE3 = BYR.CONSIGNEE_LINE3,
                    CONSIGNEE_LINE4 = BYR.CONSIGNEE_LINE4,
                    CONSIGNEE_LINE5 = BYR.CONSIGNEE_LINE5,
                    COUNTRY = BYR.COUNTRY,
                    CREATED_BY = BYR.CREATED_BY,
                    CREATED_DT = BYR.CREATED_DT,
                    FORMD_CONSIGNEE_ADDRESS = BYR.FORMD_CONSIGNEE_ADDRESS,
                    FORMD_CONSIGNEE_COMP = BYR.FORMD_CONSIGNEE_COMP,
                    FORMD_CONSIGNEE_COUNTRY = BYR.FORMD_CONSIGNEE_COUNTRY,
                    FORMD_CONSIGNEE_COUNTRY_CD = BYR.FORMD_CONSIGNEE_COUNTRY_CD,
                    PEB_IMPORTER_CD = BYR.PEB_IMPORTER_CD,
                    NOTIFY_PARTY_LINE1 = BYR.NOTIFY_PARTY_LINE1,
                    NOTIFY_PARTY_LINE2 = BYR.NOTIFY_PARTY_LINE2,
                    NOTIFY_PARTY_LINE3 = BYR.NOTIFY_PARTY_LINE3,
                    NOTIFY_PARTY_LINE4 = BYR.NOTIFY_PARTY_LINE4,
                    NOTIFY_PARTY_LINE5 = BYR.NOTIFY_PARTY_LINE5,
                    PAYMENT_TERM1 = BYR.PAYMENT_TERM1,
                    PAYMENT_TERM2 = BYR.PAYMENT_TERM2,
                    PD_CD = BYR.PD_CD,
                    PEB_BUYER_ADDRESS = BYR.PEB_BUYER_ADDRESS,
                    PEB_BUYER_NAME = BYR.PEB_BUYER_NAME,
                    PORT_DISCHARGE_CD = BYR.PORT_DISCHARGE_CD,
                    PORT_LOADING_CD = BYR.PORT_LOADING_CD,
                    PRICE_TERM_DESC = BYR.PRICE_TERM_DESC,
                    SPEC_INSTRUCT1 = BYR.SPEC_INSTRUCT1,
                    SPEC_INSTRUCT2 = BYR.SPEC_INSTRUCT2,
                    SPEC_INSTRUCT3 = BYR.SPEC_INSTRUCT3,
                    SPEC_INSTRUCT4 = BYR.SPEC_INSTRUCT4,
                    SPEC_INSTRUCT5 = BYR.SPEC_INSTRUCT5,
                    BYAIR_PORT_DISCHARGE_CD = BYR.BYAIR_PORT_DISCHARGE_CD,
                    FREIGHT_PAYABLE = BYR.FREIGHT_PAYABLE,
                    WAYBILL_REQUIRED = BYR.WAYBILL_REQUIRED,
                    MSG_TEXT = ""
                }).First();
                db.Close();

            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }


        //Update Data
        public string UpdateData(BUYER BYR)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                result = db.Fetch<string>("BUYER_Update", new
                {
                    ADDRESS1 = BYR.ADDRESS1,
                    ADDRESS2 = BYR.ADDRESS2,
                    ADDRESS3 = BYR.ADDRESS3,
                    ADDRESS4 = BYR.ADDRESS4,
                    BUYER_CD = BYR.BUYER_CD,
                    BUYER_NAME = BYR.BUYER_NAME,
                    BYAIR_PORT_LOADING_CD = BYR.BYAIR_PORT_LOADING_CD,
                    CONSIGNEE_LINE1 = BYR.CONSIGNEE_LINE1,
                    CONSIGNEE_LINE2 = BYR.CONSIGNEE_LINE2,
                    CONSIGNEE_LINE3 = BYR.CONSIGNEE_LINE3,
                    CONSIGNEE_LINE4 = BYR.CONSIGNEE_LINE4,
                    CONSIGNEE_LINE5 = BYR.CONSIGNEE_LINE5,
                    COUNTRY = BYR.COUNTRY,
                    CHANGED_BY = BYR.CHANGED_BY,
                    CHANGED_DT = BYR.CHANGED_DT,
                    FORMD_CONSIGNEE_ADDRESS = BYR.FORMD_CONSIGNEE_ADDRESS,
                    FORMD_CONSIGNEE_COMP = BYR.FORMD_CONSIGNEE_COMP,
                    FORMD_CONSIGNEE_COUNTRY = BYR.FORMD_CONSIGNEE_COUNTRY,
                    FORMD_CONSIGNEE_COUNTRY_CD = BYR.FORMD_CONSIGNEE_COUNTRY_CD,
                    PD_CD = BYR.PD_CD,
                    PEB_IMPORTER_CD = BYR.PEB_IMPORTER_CD,
                    NOTIFY_PARTY_LINE1 = BYR.NOTIFY_PARTY_LINE1,
                    NOTIFY_PARTY_LINE2 = BYR.NOTIFY_PARTY_LINE2,
                    NOTIFY_PARTY_LINE3 = BYR.NOTIFY_PARTY_LINE3,
                    NOTIFY_PARTY_LINE4 = BYR.NOTIFY_PARTY_LINE4,
                    NOTIFY_PARTY_LINE5 = BYR.NOTIFY_PARTY_LINE5,
                    PAYMENT_TERM1 = BYR.PAYMENT_TERM1,
                    PAYMENT_TERM2 = BYR.PAYMENT_TERM2,
                    PEB_BUYER_ADDRESS = BYR.PEB_BUYER_ADDRESS,
                    PEB_BUYER_NAME = BYR.PEB_BUYER_NAME,
                    PORT_DISCHARGE_CD = BYR.PORT_DISCHARGE_CD,
                    PORT_LOADING_CD = BYR.PORT_LOADING_CD,
                    PRICE_TERM_DESC = BYR.PRICE_TERM_DESC,
                    SPEC_INSTRUCT1 = BYR.SPEC_INSTRUCT1,
                    SPEC_INSTRUCT2 = BYR.SPEC_INSTRUCT2,
                    SPEC_INSTRUCT3 = BYR.SPEC_INSTRUCT3,
                    SPEC_INSTRUCT4 = BYR.SPEC_INSTRUCT4,
                    SPEC_INSTRUCT5 = BYR.SPEC_INSTRUCT5,
                    BYAIR_PORT_DISCHARGE_CD = BYR.BYAIR_PORT_DISCHARGE_CD,
                    FREIGHT_PAYABLE = BYR.FREIGHT_PAYABLE,
                    WAYBILL_REQUIRED = BYR.WAYBILL_REQUIRED,
                    MSG_TEXT = ""
                }).First();
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }

            return result;
        }
    }
}
