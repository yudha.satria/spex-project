﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class ShutterMaster
    {
        public string SHUTTER_CODE { get; set; }
        public string TRANSPORTATION { get; set; }
        public string DESTINATION { get; set; }
        public string PRIVILLAGE { get; set; }
        public string HANDLING_TYPE { get; set; }

        public string CASE_GROUPING { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }

        public List<ShutterMaster> GetShutter(ShutterMaster Shutter)
        {
            List<ShutterMaster> ShutterMasters = new List<ShutterMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            ShutterMasters = db.Fetch<ShutterMaster>("ShutterMaster_Search",
                  new { SHUTTER_CODE = Shutter.SHUTTER_CODE },
                  new { TRANSPORTATION = Shutter.TRANSPORTATION },
                  new { DESTINATION = Shutter.DESTINATION },
                  new { PRIVILLAGE = Shutter.PRIVILLAGE },
                  new { HANDLING_TYPE = Shutter.HANDLING_TYPE },
                  new { CASE_GROUPING = Shutter.CASE_GROUPING }
                  ).ToList();
            db.Close();;
            return ShutterMasters;
        }

        public string AddDataShutterMaster(ShutterMaster Shutter,string User_Id)
        {
            string result = string.Empty;
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                result = db.Fetch<string>("ShutterMaster_Add",
                      new { SHUTTER_CODE = Shutter.SHUTTER_CODE },
                      new { TRANSPORTATION = Shutter.TRANSPORTATION },
                      new { DESTINATION = Shutter.DESTINATION },
                      new { PRIVILLAGE = Shutter.PRIVILLAGE },
                      new { HANDLING_TYPE = Shutter.HANDLING_TYPE },
                      new { CASE_GROUPING = Shutter.CASE_GROUPING },
                      new { USER_ID = User_Id }
                      ).First();
                db.Close(); 
            }
            catch (Exception ex)
            {
                result="false|"+ex.Message;
            }
            return result;
        }

        public string UpadteDataShutterMaster(ShutterMaster Shutter, string User_Id)
        {
            string result = string.Empty;
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                result = db.Fetch<string>("ShutterMaster_Update",
                      new { SHUTTER_CODE = Shutter.SHUTTER_CODE },
                      new { TRANSPORTATION = Shutter.TRANSPORTATION },
                      new { DESTINATION = Shutter.DESTINATION },
                      new { PRIVILLAGE = Shutter.PRIVILLAGE },
                      new { HANDLING_TYPE = Shutter.HANDLING_TYPE },
                      new { CASE_GROUPING = Shutter.CASE_GROUPING },
                      new { USER_ID = User_Id }
                      ).First();
                db.Close(); ;
            }
            catch (Exception ex)
            {
                result = "false|" + ex.Message;

            }
            return result;
        }

        public string DeleteDataShutterMaster(List<string> ShutterCodes)
        {
            string result = string.Empty;
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                foreach (string SHUTTER_CODE in ShutterCodes)
                {
                   
                   result = db.Fetch<string>("ShutterMaster_Delete",
                         new { SHUTTER_CODE = SHUTTER_CODE }
                         ).First();                   
                }
                db.Close(); 
            }
            catch (Exception ex)
            {
                result = "false|" + ex.Message;
            }
            return result;
        }

        public List<string> GetCaseGrouping()
        {  
                IDBContext db = DatabaseManager.Instance.GetContext();
                List<string> result = db.Fetch<string>("ShutterMaster_GetCaseGrouping"
                          ).ToList();                
                db.Close();            
                return result;

        }

        public List<mSystemMaster> GetDataBySystemMaster(string System_Type, string System_Code)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<mSystemMaster> result = db.Fetch<mSystemMaster>("ShutterMaster_GetSystemMaster",
                 new { System_Type = System_Type }  ,
                 new { System_Code = System_Code } 
                ).ToList();
            db.Close();
            return result;
        }
    }
}
