﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mCalendarMaster
    {
        public int Year { get; set; }
        public string Shift { get; set; }
        public DateTime date { get; set; }
        public int WorkingDay { get; set; }

        public string Reason { get; set; }
        public List<mCalendarMaster> GetHoliday(string Year, string Shift,string User_id)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mCalendarMaster>("CalendarMaster_GetHoliday",
                  new { Year = Year },
                  new { Shift = Shift },
                  //add agi 2017-08-21
                  new { User_id = User_id }
                  );
            db.Close();
            return l.ToList();
        }

        public string SaveData(string DateFrom, string DateTo, List<string> Shifts, string WorkingDay, string Reason, string Username)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string l = db.Fetch<string>("CalendarMaster_SaveData",
                  new { DateFrom = DateFrom },
                  new { DateTo = DateTo },
                  new { Shifts = String.Join(";", Shifts.ToArray()) },
                  new { WorkingDay = WorkingDay },
                  new { Reason = Reason },
                  new { Username = Username }
                  ).SingleOrDefault();
            db.Close();
            return l;
        }

        public List<mCalendarMaster_SumWorkingDay> GetSumWorkingDay(string Year,string Shift)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mCalendarMaster_SumWorkingDay>("CalendarMaster_GetSumWorkingDay",
                  new { Year = Year },
                  new { Shift = Shift }
                  );
            db.Close();
            return l.ToList();
        }

        public List<string> GetYear()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<String>("CalendarMaster_Getyear");
            db.Close();
            return l.ToList();
        }
    }

    public class mCalendarMaster_SumWorkingDay
    {
        public int SumOfWorkingDay { get; set; }
        public string NameMonth { get; set; }
        public int Month { get; set; }
    }
}