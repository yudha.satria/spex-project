﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : Service Part Export (SPEX)
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : 
 * Function Name    : Rack Address Master
 * Function Group   : Master
 * Program Id       : 
 * Program Name     : Rack Address Master
 * Program Type     : Model
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID.Diman
 * Version          : 01.00.00
 * Creation Date    : 23/10/2018
 *
 * Update history		Re-fix date				Person in charge				Description
 *
 * Copyright(C) 2018 - . All Rights Reserved
 *************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models.RackAddressMaster
{
    public class mRackAddressMaster
    {
        #region Other
        public String MODE { get; set; }
        public String USER_ID { get; set; }
        #endregion

        #region Detail
        public String RACK_ADDRESS_CD { get; set; }
        public String ZONE_CD { get; set; }
        public String AISLE_CD { get; set; }
        public String RACK_CD { get; set; }
        public String ROW { get; set; }
        public String COLUMN { get; set; }
        public String LENGTH { get; set; }
        public String WIDTH { get; set; }
        public String HEIGHT { get; set; }
        public String DELETE_FLAG { get; set; }
        public String CREATED_BY { get; set; }
        public String CREATED_DT { get; set; }
        public String CHANGED_BY { get; set; }
        public String CHANGED_DT { get; set; }
        #endregion
    }
}