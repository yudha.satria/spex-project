﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : Service Part Export (SPEX)
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : 
 * Function Name    : Rack Address Master
 * Function Group   : Master
 * Program Id       : 
 * Program Name     : Rack Address Master
 * Program Type     : Model
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID.Diman
 * Version          : 01.00.00
 * Creation Date    : 23/10/2018
 *
 * Update history		Re-fix date				Person in charge				Description
 *
 * Copyright(C) 2018 - . All Rights Reserved
 *************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models.RackAddressMaster
{
    public class sRackAddressMaster
    {
        #region Header
        public String RackAddressCode { get; set; }
        public String ZoneCode { get; set; }
        public String AisleCode { get; set; }
        public String RackCode { get; set; }
        public String DeleteFlag { get; set; }
        #endregion
    }
}