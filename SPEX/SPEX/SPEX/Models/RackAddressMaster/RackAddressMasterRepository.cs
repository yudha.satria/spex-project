﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : Service Part Export (SPEX)
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : 
 * Function Name    : Rack Address Master
 * Function Group   : Master
 * Program Id       : 
 * Program Name     : Rack Address Master
 * Program Type     : Repository
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID.Diman
 * Version          : 01.00.00
 * Creation Date    : 23/10/2018
 *
 * Update history		Re-fix date				Person in charge				Description
 *
 * Copyright(C) 2018 - . All Rights Reserved
 *************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models.RackAddressMaster
{
    public class RackAddressMasterRepository
    {
        #region Singleton
        private static RackAddressMasterRepository instance = null;
        public static RackAddressMasterRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RackAddressMasterRepository();
                }
                return instance;
            }
        }
        #endregion

        #region C.R.U.D.S
        public List<mRackAddressMaster> SearchData (sRackAddressMaster searchModel)
        {
            List<mRackAddressMaster> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    RACK_ADDRESS_CD = searchModel.RackAddressCode,
                    ZONE_CD = searchModel.ZoneCode,
                    AISLE_CD = searchModel.AisleCode,
                    RACK_CD = searchModel.RackCode,
                    DELETE_FLAG = searchModel.DeleteFlag
                };
                result = db.Fetch<mRackAddressMaster>("RackAddressMaster/SearchData", args);
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public String SaveData (List<mRackAddressMaster> dataModel)
        {
            String result = String.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                foreach(mRackAddressMaster dt in dataModel)
                {
                    dynamic args = new
                    {
                        MODE = dt.MODE,
                        RACK_ADDRESS_CD = dt.RACK_ADDRESS_CD,
                        ZONE_CD = dt.ZONE_CD,
                        AISLE_CD = dt.AISLE_CD,
                        RACK_CD = dt.RACK_CD,
                        ROW = dt.ROW,
                        COLUMN = dt.COLUMN,
                        LENGTH = dt.LENGTH,
                        WIDTH = dt.WIDTH,
                        HEIGHT = dt.HEIGHT,
                        CHANGED_BY = dt.CHANGED_BY,
                        CHANGED_DT = dt.CHANGED_DT,
                        USER_ID = dt.USER_ID
                    };
                    result = db.SingleOrDefault<String>("RackAddressMaster/SaveData", args);
                    if (result.Split('|')[0].Equals("E"))
                    {
                        break;
                    }
                }
                if (result.Split('|')[0].Equals("E"))
                {
                    db.AbortTransaction();
                }
                else
                {
                    db.CommitTransaction();
                }
            }
            catch (Exception e)
            {
                result = "E|" + e.Message;
                db.AbortTransaction();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public String DeleteTempData()
        {
            String result = "SUCCESS";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.Execute("RackAddressMaster/DeleteDataTemporary");
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public String BackgroundProsessUploadSave(String moduleID, String functionID, long PID, String userID)
        {
            String result = "SUCCESS";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MODULE_ID = moduleID,
                    FUNCTION_ID = functionID,
                    PID = PID,
                    USER_ID = userID
                };
                db.Execute("RackAddressMaster/BackgroundProcessUpload", args);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                db.Close();
            }
            return result;
        }
        #endregion
    }
}