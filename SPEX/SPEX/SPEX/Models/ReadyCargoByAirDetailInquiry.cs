﻿using System;

namespace SPEX.Models
{
    public class ReadyCargoByAirDetailInquiry
    {
        public string DESTINATION	{get;set;}
        public string ORDER_NO	{get;set;}
        public string CASE_NO	{get;set;}
        public string ITEM_NO	{get;set;}
        public string PART_NO	{get;set;}
        public string PART_NAME	{get;set;}
        public string PART_QTY	{get;set;}
        public string PRICE	{get;set;}
        public string AMOUNT	{get;set;}
        public string REMAK { get; set; }
        public DateTime VANNING_DATE { get; set; }
	
    }

}