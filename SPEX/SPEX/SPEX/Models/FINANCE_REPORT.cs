﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class FINANCE_REPORT
    {
        public string ID { get; set; }
        public string INVOICE_TO { get; set; }
        public string LOT_CD { get; set; }
        public string CURRENCY { get; set; }
        public string UOM { get; set; }
        public string RECIEVE_BY { get; set; }
        public string CITY { get; set; }
        public string CHECKED { get; set; }
        public string PREPARED { get; set; }
        public string INVOICE_NO { get; set; }
        public string TAX_NO { get; set; }
        public string INVOICE_DT { get; set; }
        public string VANNING_NO { get; set; }
        public string MARKS { get; set; }
        public string ETD { get; set; }
        public double QTY { get; set; }
        public double AMOUNT { get; set; }
        public string CURRDATE { get; set; }
        public string METHD { get; set; }
        //header
        public string HEADER_RECIEVE_BY { get; set; }
        public string HEADER_CHECKED { get; set; }
        public string HEADER_PREPARED { get; set; }

        //add agi 2017-09-05 request mrs hesti
        public double GROSS_WEIGHT { get; set; }
        public double NET_WEIGHT { get; set; }

        public double MEASUREMENT { get; set; }


        public List<FINANCE_REPORT> CreateFinanceReport(string Invoices)
        {
            var db = DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString;
            List<FINANCE_REPORT> l = new List<FINANCE_REPORT>();
            return l;

        }

        public List<FINANCE_REPORT> CreateFinanceReport(DataTable Invoices)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            List<FINANCE_REPORT> results = new List<FINANCE_REPORT>();
            using (SqlConnection conn = new SqlConnection(DatabaseManager.Instance.GetDefaultConnectionDescriptor().ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("[spex].[SP_INVOICE_CREATE_FINANCE_REPORT]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter p = cmd.Parameters.AddWithValue("@INVOICE_NO", Invoices);
                p.SqlDbType = SqlDbType.Structured;
                try
                {
                    conn.Open();
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        //--- Return from procedures is -> doc, docTo, docTembusan, docApprover, docAttachment
                        int i = 0;


                        while (dr.Read())
                        {
                            FINANCE_REPORT newItem = new FINANCE_REPORT();

                            newItem.ID = dr.GetString(0);
                            newItem.INVOICE_TO = dr.GetString(1);
                            newItem.LOT_CD = dr.GetString(2);
                            newItem.CURRENCY = dr.GetString(3);
                            newItem.UOM = dr.GetString(4);
                            newItem.RECIEVE_BY = dr.GetString(5);
                            newItem.CITY = dr.GetString(6);
                            newItem.CHECKED = dr.GetString(7);
                            newItem.PREPARED = dr.GetString(8);
                            newItem.TAX_NO = dr.GetString(9);
                            newItem.VANNING_NO = dr.GetString(10);
                            newItem.INVOICE_NO = dr.GetString(11);
                            newItem.INVOICE_DT = dr.GetString(12);
                            newItem.MARKS = dr.GetString(13);
                            newItem.ETD = dr.GetString(14);
                            newItem.QTY = Convert.ToDouble(dr.GetDecimal(15));
                            newItem.AMOUNT = Convert.ToDouble(dr.GetDecimal(16));
                            newItem.CURRDATE = dr.GetString(17);
                            newItem.METHD = dr.GetString(18);
                            newItem.HEADER_RECIEVE_BY = dr.GetString(19);
                            newItem.HEADER_CHECKED = dr.GetString(20);
                            newItem.HEADER_PREPARED = dr.GetString(21);
                            //add agi 2017-09-05 request mrs hesti
                            newItem.GROSS_WEIGHT = Convert.ToDouble(dr.GetDecimal(22));
                            newItem.NET_WEIGHT = Convert.ToDouble(dr.GetDecimal(23));
                            newItem.MEASUREMENT = Convert.ToDouble(dr.GetDecimal(24));
                            results.Add(newItem);
                        }
                    }

                }
                catch
                {

                }
            }

            return results;
        }

        public DataTable InvTable()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Invoice_no", typeof(string));
            return table;
        }

        public DataTable ConvertListToDatatebleInvoice(List<mInvoice> Invs)
        {
            DataTable table = InvTable();
            foreach (mInvoice data in Invs)
            {
                DataRow dr = table.NewRow();
                dr["Invoice_no"] = data.INVOICE_NO;
                table.Rows.Add(dr);
            }
            return table;
        }
    }
}