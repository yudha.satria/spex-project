﻿using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPartListMaster
    {
        //FUNGSI GETHER SETTER 
        public string PROD_MONTH { get; set; }
        public string PARENT_PART_NO { get; set; }
        public string PARENT_PROD_PURPOSE { get; set; }
        public string PARENT_SOURCE_TYPE { get; set; }
        public string PARENT_PLANT_CD { get; set; }
        public string PART_NO { get; set; }
        public string PROD_PURPOSE { get; set; }
        public string SOURCE_TYPE { get; set; }
        public string PLANT_CD { get; set; }
        public string SLOC_CD { get; set; }
        public string PART_COLOR_SFX { get; set; }
        public string DOCK_CD { get; set; }
        public string QTY_N_1 { get; set; }
        public string QTY_N_2 { get; set; }
        public string QTY_N_3 { get; set; }
        public string QTY_N_4 { get; set; }
        public string QTY_N_5 { get; set; }
        public string QTY_N_6 { get; set; }
        public string QTY_N_7 { get; set; }
        public string QTY_N_8 { get; set; }
        public string QTY_N_9 { get; set; }
        public string QTY_N_10 { get; set; }
        public string QTY_N_11 { get; set; }
        public string QTY_N_12 { get; set; }
        public string QTY_N_13 { get; set; }
        public string QTY_N_14 { get; set; }
        public string QTY_N_15 { get; set; }
        public string QTY_N_16{ get; set; }
        public string QTY_N_17 { get; set; }
        public string QTY_N_18 { get; set; }
        public string QTY_N_19 { get; set; }
        public string QTY_N_20 { get; set; }
        public string QTY_N_21 { get; set; }
        public string QTY_N_22 { get; set; }
        public string QTY_N_23 { get; set; }
        public string QTY_N_24 { get; set; }
        public string QTY_N_25 { get; set; }
        public string QTY_N_26 { get; set; }
        public string QTY_N_27 { get; set; }
        public string QTY_N_28 { get; set; }
        public string QTY_N_29 { get; set; }
        public string QTY_N_30 { get; set; }
        public string QTY_N_31 { get; set; }

               
        //PEMANGGILAN DATA YANG AKAN DI SEARCH DAN DI TAMPILKAN (CEKNYA DI SQL)
        public List<mPartListMaster> getListPartListMaster(string p_PARENT_PART_NO, string p_PART_NO, string p_PROD_MONTH, string p_PROD_MONTH_to)
        {
         IDBContext db = DatabaseManager.Instance.GetContext();

         var l = db.Fetch<mPartListMaster>("SP_SPN117_PARTLISTMASTER_SEARCH",

               new { PARENT_PART_NO = p_PARENT_PART_NO },
               new { PART_NO = p_PART_NO },
               new { PROD_MONTH = p_PROD_MONTH },
               new { PROD_MONTH_TO = p_PROD_MONTH_to });
               db.Close();
            return l.ToList();

        }

       
       
    }
}
