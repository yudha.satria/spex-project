﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using System.Transactions;
using SPEX.Cls;

namespace Toyota.Common.Web.Platform.Starter.Models
{
    public class PackingInstruction
    {
        public decimal PROCESS_ID {get;set;}
        public string PART_NO {get;set;}
        public string PART_NAME {get;set;}
        public string KANBAN_QTY {get;set;}
        public string PART_TYPE {get;set;}
        
        public string InsertReprint(string PartNo,string SUPLIER_PLANT, int Copy,int Oninsert,string UserId,string Modul_Id,string Function_Id)
        {
            int Loop = 0;
            string  l=string.Empty;
            Log lg= new Log();
            long PID=0;
            MessagesString MSG = new MessagesString();
            PID = lg.createLogOnProgress(MSG.getMSG_Spex("MSPXPI001INF", PID), UserId, "LoopInsert", PID, Modul_Id, Function_Id);
            IDBContext db = DatabaseManager.Instance.GetContext();
            //while (Loop < Copy)
            //{                
                               
            //    if (Oninsert + Loop < Copy)
            //    {
                    l = db.Query<string>("PackingInstructionInsertFromReprint",
                   new { PART_NO = PartNo },
                   new { SUPLIER_PLANT = SUPLIER_PLANT },
                   new { COPY = Copy },
                   new { PID = PID },
                   new { USER = UserId },
                   new { LOOP = 1 }//new { LOOP = Loop }
                   ).First();
                    Loop += Oninsert;
            //    }
            //    else
            //    {
            //       l = db.Query<string>("PackingInstructionInsertFromReprint",
            //       new { PART_NO = PartNo },
            //       new { SUPLIER_PLANT = SUPLIER_PLANT },
            //       new { COPY = Copy-Loop },
            //       new { PID = PID },
            //       new { USER = UserId },
            //       new { LOOP = Copy - Loop }
            //       ).First(); 
            //        //Loop += Oninsert;
            //        Loop += (Copy - Loop);
            //    }
            //}
            lg.createLogOnProgress(MSG.getMSG_Spex("MSPXPI003INF", PID), UserId, "LoopInsert", PID, Modul_Id, Function_Id);
            db.Close();
            return l;
        }


        public int GetSettingInsert()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Query<string>("PackingInstructionLoopInsert").First();
            db.Close();
            return Convert.ToInt32(l);
        }
        public string GetReportDBPath()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mShippingInstruction>("Get_Report_DB_Path");
            db.Close();
            return l.Count > 0 ? l.ToList().FirstOrDefault().Text : "";
        }

        public void deletePackingInstructionPartLabelTempByProcessId(long PID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.Execute("PackingInstructionDeletePartLabelTempByProcessId",new {PID=PID});
            db.Close();            
        }

        public void DeleteCaseLabelTemp(long PID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.Execute("DeleteCaseLabelTempByProcessId", new { PID = PID });
            db.Close(); 
        }

    }
}