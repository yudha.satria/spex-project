﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class VanningInquiry
    {
        public string VANNING_LABEL { get; set; }
        public string SHIP_INSTRUCT_NO { get; set; }
        public string CONTAINER_NO { get; set; }
        public string TRANSPORTATION { get; set; }
        public string TRANSPORTATION_CD { get; set; }
        public string DESTINATION { get; set; }
        public DateTime? VANNING_DATE { get; set; }
        public string SEALNO { get; set; }
        public decimal TARE { get; set; }
        public decimal CONTAINER_SIZE { get; set; }
        public string CONTAINER_TYPE { get; set; }
        public decimal NET_WEIGHT { get; set; }
        public decimal CASE_QTY { get; set; }
        public decimal SUM_CASE { get; set; }
        public string DELIVERY_NOTE_NO { get; set; }
        public string VANNING_STS { get; set; }
        public string TRANSPORTATION_DESC { get; set; }
        public string CASE_CLOSED_DT { get; set; }

        public string Text { get; set; }
        public string CASE_NO { get; set; }
        public string PACKING_DATE { get; set; }
        public string COUNTRY_CD_DESC { get; set; }
        public string COUNTRY_CD { get; set; }
        public string VENDOR_NAME { get; set; }

        public string NO { get; set; }
        public string CONT_NO { get; set; }
        public string SEAL_NO { get; set; }
        public string VANN_LABEL { get; set; }
        public string TRANS_DESC { get; set; }
        public string PENGIRIM { get; set; }
        public string NO_URUT { get; set; }
        public string NAMA_BARANG { get; set; }
        public string SATUAN { get; set; }

        public string VaningDtTxt
        {
            get
            {
                return GetTxtDate(VANNING_DATE);
            }
        }

        private string GetTxtDate(DateTime? d)
        {
            string r = "";
            if (d.HasValue)
            {
                r = d.Value.Day.ToString().PadLeft(2, '0') + "." + d.Value.Month.ToString().PadLeft(2, '0') + "." + d.Value.Year.ToString();
            }
            return r;
        }

        public List<VanningInquiry> getList(string pContainerNo, string pSealNo, string pVanningLable, string pVanningLablePrintDtFrom, string pVanningLablePrintDtTo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<VanningInquiry>("VanningInquiry_Search",
                  new { CONTAINER_NO = pContainerNo }
                  , new { SEAL_NO = pSealNo }
                  , new { VANNING_LABLE = pVanningLable }
                  , new { VANNING_PRINT_DT_FROM = pVanningLablePrintDtFrom }
                  , new { VANNING_PRINT_DT_TO = pVanningLablePrintDtTo }
                  );
            db.Close();

            return l.ToList();
        }


        public List<VanningInquiry> getDownload(string pContainerNo, string pSealNo, string pVanningLable, string pVanningLablePrintDtFrom, string pVanningLablePrintDtTo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<VanningInquiry>("VanningInquiry_Download",
                  new { CONTAINER_NO = pContainerNo }
                  , new { SEAL_NO = pSealNo }
                  , new { VANNING_LABLE = pVanningLable }
                  , new { VANNING_PRINT_DT_FROM = pVanningLablePrintDtFrom }
                  , new { VANNING_PRINT_DT_TO = pVanningLablePrintDtTo }
                  );
            db.Close();

            return l.ToList();
        }

        public OBR getOBR(string pOBRNO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<OBR>("OBR_Get",
                  new { OBRNO = pOBRNO }
                  );
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault() : new OBR();
        }

      
        public string UpdateStatus(string pOBR_NOS, string pStatusID, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("OBR_Update_Status",
                  new
                  {
                      OBR_NOS = pOBR_NOS,
                      STATUS_ID = pStatusID,
                      USER_ID = pUSER_ID
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }

        public List<VanningInquiry> getListDestination()
        {
            List<VanningInquiry> datas = new List<VanningInquiry>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            var row = db.Fetch<VanningInquiry>("DestinationMaster_GetLIst").ToList();
            db.Close();
            datas.Add(new VanningInquiry { COUNTRY_CD = "", COUNTRY_CD_DESC = "" });
            foreach (var item in row)
            {
                datas.Add(new VanningInquiry { COUNTRY_CD = item.COUNTRY_CD, COUNTRY_CD_DESC = item.COUNTRY_CD_DESC });
            }


            return datas;
        }

        public List<VanningInquiry> getVanningSTS(string vanningLabel)
        {
            List<VanningInquiry> datas = new List<VanningInquiry>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            var row = db.Fetch<VanningInquiry>("GetVanningSTS",
                new {
                    VANNING_LABEL = vanningLabel
                }
            ).ToList();
            db.Close();
            foreach (var item in row)
            {
                datas.Add(new VanningInquiry { VANNING_STS=item.VANNING_STS});
            }
            return datas;
        }

        public String Cancel(string pVanning_Label, string pTransportation_Code, string pTransportation_Desc, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.SingleOrDefault<String>("VanningInquiry_Cancel",
                  new
                  {
                      TRANSPORTATION_CODE = pTransportation_Code,
                      TRANSPORTATION_DESC=pTransportation_Desc,
                      USER_LOGIN = pUSER_ID,
                      VANNING_LABEL = pVanning_Label
                  }
                  );
            db.Close();

            return l;
        }

        public String CancelContainerNo(string pCase_No, string pVanning_Label, string pContainer_No, string pTransportation_Code, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.SingleOrDefault<String>("VanningInquiry_CancelContainerNo",
                  new
                  {
                      CONTAINER_NO = pContainer_No,
                      VANNING_LABEL = pVanning_Label,
                      CASE_NO = pCase_No,
                      USER_LOGIN = pUSER_ID,
                      TRANSPORTATION_CODE = pTransportation_Code
                  }
                  );
            db.Close();
            return l;
        }

        public string VanningInquireReOpen(string pVanningLabel,string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<mCaseMaster>("VanningInquire_ReOpen",
                  new
                  {
                      VANNING_LABEL = pVanningLabel,
                      USER_LOGIN = pUSER_ID
                  }
                  );
            db.Close();
            //result = l.Text;
            foreach (var message in l)
            {
                result = message.Text;
            }
            return result;
        }

        public List<VanningInquiry> getDetailContainerNo(string pVanningLabel,string pContainerNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
           
            var l = db.Fetch<VanningInquiry>("VanningInquiry_DetailContainerNo",
                new
                {
                    VANNING_LABEL = pVanningLabel,
                    CONTAINER_NO = pContainerNo
                });

            db.Close();

            return l.ToList();
        }

        public String getDataPopUpSiNo(string pSI_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.SingleOrDefault<String>("VanningInquiry_SI_NO",
                  new
                  {
                      SI_NO = pSI_NO
                  }
                  );
            db.Close();
            
          
            return l;
        }

        public String getTransDesc(string pTrans_Cd)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";

            var l = db.SingleOrDefault<String>("VanningInquiry_Transportation",
                  new
                  {
                      TRANSCD = pTrans_Cd
                  }
                  );
            db.Close();

         
            return l;
        }

        public List<VanningInquiry> getDestination()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<VanningInquiry>("VanningInquiry_Destination");
            db.Close();

            return l.ToList();
        }

        public List<VanningInquiry> getContainerType()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<VanningInquiry>("VanningInquiry_ContainerType");
            db.Close();

            return l.ToList();
        }

        public List<VanningInquiry> getVendorName()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.Fetch<VanningInquiry>("VanningInquiry_VendorName");
            db.Close();

            return l.ToList();
        }

        public string SaveAndPrint(string pSI_No, string pTrans_CD, string pTrans_DESC,string pContainer_No, string pDestination,
            string pSeal_No, string pTare, string pContainerType, string pContainerSize, string pVendor_Name, string getpE_UserId,string printers)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.SingleOrDefault<string>("VanningInquiry_Save_And_Print",
                  new
                  {
                      SHIP_INSTRUCT_NO = pSI_No.Trim(),
                      TRANSPORTATION_CODE = pTrans_CD.Trim(),
                      TRANSPORTATION_DESC = pTrans_DESC.Trim(),
                      CONTAINER_NO = pContainer_No.Trim(),
                      DESTINATION = pDestination,
                      SEALNO = pSeal_No.Trim(),
                      TARE = pTare.Trim(),
                      CONTAINER_SIZE = pContainerSize.Trim(),
                      CONTAINER_TYPE = pContainerType.Trim(),
                      VENDOR_NAME = pVendor_Name.Trim(),
                      USER_LOGIN = getpE_UserId.Trim(),
                      PRINTER = printers
                  }
                  );
            db.Close();

            
            return l;

        }

        public String PrintDeliveryNote(string pVanning_Label, string user_login, string printers)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.SingleOrDefault<String>("VanningInquiry_PrintDeliveryNote",
                  new
                  {
                      VANNING_LABEL = pVanning_Label,
                      USER_LOGIN = user_login,
                      PRINTER = printers
                  }
                  );
            db.Close();
            return l;
        }

        public String RePrint(string pVanning_Label, string user_login, string printer)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.SingleOrDefault<String>("VanningInquiry_RePrint",
                  new
                  {
                      VANNING_LABEL = pVanning_Label,
                      USER_LOGIN = user_login,
                      PRINTER = printer
                  }
                  );
            db.Close();
            return l;
        }

        public String getDefaultPrinter()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = "SUCCESS";
            var l = db.SingleOrDefault<String>("VanningInquiry_DefaultPrinter",new{});
            db.Close();
            return l;
        }
    }
}