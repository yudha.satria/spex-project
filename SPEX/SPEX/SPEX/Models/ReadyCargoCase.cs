﻿using System;

namespace SPEX.Models
{
    public class ReadyCargoCase
    {
        public string BUYER_CD { get; set; }
        public string PD_CD { get; set; }
        public string CONTAINER_NO { get; set; }
        public DateTime VANNING_DATE { get; set; }
        public string CASE_NO { get; set; }
        public string CASE_DANGER_FLAG { get; set; }
        public string CASE_TYPE { get; set; }
        public string TRANSPORT_CD { get; set; }
        public DateTime CASE_PACKAGE_DT { get; set; }
        public string CASE_PACKAGE_TYPE { get; set; }
        public string INVOICE_NO { get; set; }
        public DateTime INVOICE_DT { get; set; }        
        public string VANNING_NO { get; set; }
        public string PACKING_COMPANY { get; set; }
        public string SHIPPING_INSTRUCTION_NO { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
        //add by agi 2016-07-13 for attachment download
        public double CASE_GROSS_WEIGHT { get; set; }
        public double CASE_NET_WEIGHT { get; set; }
        public double CASE_MEASUREMENT { get; set; }
        public double CASE_LENGTH { get; set; }
        public double CASE_WIDTH { get; set; }
        public double CASE_HEIGHT { get; set; }
        public int No { get; set; }
    }
}