﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Transactions;

//Created By Diani Widyaningrum
namespace SPEX.Models
{
    public class mPartStockReceivingInquiry
    {
        public string ORDER_DATE { get; set; }
        public string ORDER_NO { get; set; }
        public string MANIFEST_NO { get; set; }
        public string SUB_MANIFEST_NO { get; set; }
        public string KANBAN_ID { get; set; }
        public string ITEM_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_ADDRESS { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string SUB_SUPPLIER_CODE { get; set; } 
        public string PART_STATUS { get; set; }
        public string BINNING_DATE { get; set; }
        public string BINNING_STATUS { get; set; } 
        public string SUB_SUPPLIER_PLANT { get; set; }


        public string Text { get; set; }

        public List<mPartStockReceivingInquiry> getListPartStockReceivingInquiry(string pORDER_NO, string pKANBAN_ID, string pBINNING_DATE, string pBINNING_DATE_TO, string pSUPPLIER_CODE, string pPART_NO, string pRELEASE_DATE, string pRELEASE_DATE_TO, string pSUB_SUPPLIER_CODE, string pPART_ADDRESS, string pSTATUS, string pSUB_MF_NO, string pMANIFEST, string pSUPPLIER_PLANT, string pSUB_SUPPLIER_PLANT)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPartStockReceivingInquiry>("SearchPartStockReceivingInquiry",
                    new { ORDER_NO = pORDER_NO },
                    new { KANBAN_ID = pKANBAN_ID },
                    new { BINNING_DATE = pBINNING_DATE },
                    new { BINNING_DATE_TO = pBINNING_DATE_TO },
                    new { SUPPLIER_CODE = pSUPPLIER_CODE },
                    new { PART_NO = pPART_NO },
                    new { RELEASE_DATE = pRELEASE_DATE },
                    new { RELEASE_DATE_TO = pRELEASE_DATE_TO },
                    new { SUB_SUPPLIER_CODE = pSUB_SUPPLIER_CODE },
                    new { PART_ADDRESS = pPART_ADDRESS },
                    new { STATUS = pSTATUS },
                    new { SUB_MF_NO = pSUB_MF_NO },
                    new { MANIFEST = pMANIFEST },
                    new { SUPPLIER_PLANT = pSUPPLIER_PLANT },
                    new { SUB_SUPPLIER_PLANT = pSUB_SUPPLIER_PLANT });
            db.Close();
            return l.ToList();
        }

        public List<mSystemMaster> GetComboBoxList(string systemType)
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetComboBoxfrSystem",
                new { SYSTEM_TYPE = systemType }
            ).ToList();
            db.Close();
            return datas;
        }
          

    }
}
