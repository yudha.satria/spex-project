﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mShippingInstruction
    {
        //header
        public string SHIP_INSTRUCTION_NO { get; set; }
        public string VERSION { get; set; }
        public string VERSION_DESC
        {
            get
            {
                if (VERSION != null)
                {
                    if (VERSION == "T" || VERSION == "t")
                        return "Tentative";
                    else
                        return "Firm";
                }
                else
                    return "";
            }
        }
        public string BUYER_CD { get; set; }
        public string BUYER_NAME { get; set; }
        public DateTime? VANNING_DT_FROM { get; set; }
        public DateTime? VANNING_DT_TO { get; set; }
        public string SHIPPER { get; set; }
        public string CONSIGNEE { get; set; }
        public string NOTIFY_PARTY { get; set; }
        public string SHIPPING_AGENT_NAME { get; set; }
        public string FORWARD_AGENT_NAME { get; set; }
        public string BOOKING_NO { get; set; }
        public string FEEDER_VESSEL { get; set; }
        public string VOYAGE1 { get; set; }
        public DateTime? SHIPMENT_DT { get; set; }
        public string CONNECT_VESSEL { get; set; }
        public string VOYAGE2 { get; set; }
        public DateTime? ARRIVAL_DT { get; set; }
        public string PORT_LOADING { get; set; }
        public string PORT_DISCHARGE { get; set; }
        public double TOTAL_CASE { get; set; }
        public double TOTAL_PCS { get; set; }
        public double TOTAL_GW { get; set; }
        public double TOTAL_NET { get; set; }
        public double TOTAL_MEASUREMENT { get; set; }
        public string CONTAINER_GRADE { get; set; }
        public double TOTAL_CONTAINER_PLAN { get; set; }
        public double TOTAL_CONTAINER_ACTUAL { get; set; }
        public string LOCATION_CY { get; set; }
        public string FREIGHT_PAYABLE { get; set; }
        public string PREPAID { get; set; }
        public string COLLECT { get; set; }
        public string BL_RELEASE { get; set; }
        public DateTime? ISSUE_DT { get; set; }
        public string APPROVED_BY { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        //FOR DATA
        public string TOTAL_CASE_STRING { get; set; }
        public string TOTAL_PCS_STRING { get; set; }
        public string TOTAL_GW_STRING { get; set; }
        public string TOTAL_MEASUREMENT_STRING { get; set; }

        public decimal TOTAL_NET_WEIGHT { get; set; }
        public decimal TOTAL_GROSS_WEIGHT { get; set; }
        //
        public string Text { get; set; }
        public string BUYER_DESC { get; set; }
        //SI Invoice 
        public string INVOICE_NO { get; set; }
        public string PD_CD { get; set; }
        public DateTime? ETD { get; set; }

        //detail
        public string PEB_NO { get; set; }
        public DateTime? PEB_DT { get; set; }
        public string CONTAINER_NO { get; set; }
        public string SEAL_NO { get; set; }
        //public string INVOICE_NO { get; set; }
        public DateTime? VANNING_DT { get; set; }
        public string CONTAINER_QTYPLAN { get; set; }
        public string CONTAINER_QTYACTUAL { get; set; }


        public string SYSTEM_CD { get; set; }
        public string SYSTEM_VALUE { get; set; }

        //DOWNLOAD
        public string SHIP_AGENT_PIC_NAME { get; set; }

        public string FORWARD_AGENT_PIC_NO { get; set; }
        public string FORWARD_AGENT_CONTACT_NO { get; set; }

        public string XLS_APPROVE_BY_LABEL { get; set; }
        public string XLS_ARRIVAL_LABEL { get; set; }
        public string XLS_ATTN1_LABEL { get; set; }
        public string XLS_ATTN2_LABEL { get; set; }
        public string XLS_BL_RELEASE_LABEL { get; set; }
        public string XLS_BOOKING_NO_LABEL { get; set; }
        public string XLS_COLLECT_LABEL { get; set; }
        public string XLS_CONNECT_VESSEL_LABEL { get; set; }
        public string XLS_CONNECT_VOY_LABEL { get; set; }
        public string XLS_CONSIGNEE_LABEL { get; set; }
        public string XLS_CONTAINER_ACTUAL_LABEL { get; set; }
        public string XLS_CONTAINER_GRADE_LABEL { get; set; }
        public string XLS_CONTAINER_NO_LABEL { get; set; }
        public string XLS_CONTAINER_PLAN_LABEL { get; set; }
        public string XLS_CONTAINER_TOTAL_QTY_LABEL { get; set; }
        public string XLS_CONTAINER_TOTAL_LOCATION_LABEL { get; set; }
        public string XLS_DATE_ISSUE_LABEL { get; set; }
        public string XLS_DESCRIPTION_GOODS_LABEL { get; set; }
        public string XLS_DESCRIPTION_GOODS_UNIT1_LABEL { get; set; }
        public string XLS_DESCRIPTION_GOODS_UNIT2_LABEL { get; set; }
        public string XLS_FAX_NO_LABEL { get; set; }
        public string XLS_FEEDER_VESSEL_LABEL { get; set; }
        public string XLS_FEEDER_VOY_LABEL { get; set; }
        public string XLS_FREIGHT_PAYABLE_LABEL { get; set; }
        public string XLS_GENUINE_PARTS_LABEL { get; set; }
        public string XLS_GROSS_WEIGHT_LABEL { get; set; }
        public string XLS_GROSS_WEIGHT_UNIT_LABEL { get; set; }
        public string XLS_GROSS_WEIGHT_UNIT_DESC1_LABEL { get; set; }
        public string XLS_GROSS_WEIGHT_UNIT_DESC2_LABEL { get; set; }
        public string XLS_HEADER_1_LABEL { get; set; }
        public string XLS_HEADER_2_LABEL { get; set; }
        public string XLS_HEADER_3_LABEL { get; set; }
        public string XLS_INVOICE_NO_LABEL { get; set; }
        public string XLS_LOCATION_CY_LABEL { get; set; }
        public string XLS_LOCATION_CY_DESC_LABEL { get; set; }
        public string XLS_MARKS_NUMBER_DESC1_LABEL { get; set; }
        public string XLS_MARKS_NUMBER_DESC2_LABEL { get; set; }
        public string XLS_MARKS_NUMBERS_LABEL { get; set; }
        public string XLS_MEASUREMENT_LABEL { get; set; }
        public string XLS_MEASUREMENT_UNIT_LABEL { get; set; }
        public string XLS_MESRS_LABEL { get; set; }
        public string XLS_NOTE_1_LABEL { get; set; }
        public string XLS_NOTE_2_LABEL { get; set; }
        public string XLS_NOTE_3_LABEL { get; set; }
        public string XLS_NOTE_4_LABEL { get; set; }
        public string XLS_NOTE_5_LABEL { get; set; }
        public string XLS_NOTED_LABEL { get; set; }
        public string XLS_NOTIFY_PARTY_LABEL { get; set; }
        public string XLS_PEB_NO_LABEL { get; set; }
        public string XLS_PORT_DISCHARGE_LABEL { get; set; }
        public string XLS_PORT_LOADING_LABEL { get; set; }
        public string XLS_PREPAID_LABEL { get; set; }
        public string XLS_SEAL_NO_LABEL { get; set; }
        public string XLS_SHIPMENT_LABEL { get; set; }
        public string XLS_SHIPPING_INSTRUCTION_LABEL { get; set; }
        public string XLS_SHIPPER_LABEL { get; set; }
        public string XLS_SI_NO_LABEL { get; set; }
        public string XLS_TGL_LABEL { get; set; }
        public string XLS_TOTAL_CONTAINER_LABEL { get; set; }
        public string XLS_VANNING_DT_LABEL { get; set; }

        public string XLS_MNOS_DESC1_LABEL { get; set; }
        public string XLS_MNOS_DESC2_LABEL { get; set; }
        public string XLS_MNOS_DESC3_LABEL { get; set; }
        public string XLS_MNOS_DESC4_LABEL { get; set; }

        public string XLS_SEE_ATTACHMENT_LABEL { get; set; }

        public double SEQ_NO { get; set; }
        public bool IS_COMPLETE { get; set; }
        public string CASE_NO { get; set; }

        public string XLS_TOT_CONTAINER_HDR_INVOICE_LABEL { get; set; }
        public string XLS_HEADER_TBL_NO_LABEL { get; set; }
        public string XLS_HEADER_TBL_CONTAINER_LABEL { get; set; }
        public string XLS_HEADER_TBL_SEAL_LABEL { get; set; }
        public string XLS_HEADER_TBL_PEB_NO_LABEL { get; set; }
        public string XLS_HEADER_TBL_PEB_DT_LABEL { get; set; }

        public string XLS_HEADER_TBL_INVOICE_NO_LABEL { get; set; }
        public string XLS_HEADER_TBL_CASE_NO_LABEL { get; set; }
        public string XLS_HEADER_TBL_NW_LABEL { get; set; }
        public string XLS_HEADER_TBL_GW_LABEL { get; set; }
        public string XLS_HEADER_TBL_M3_LABEL { get; set; }
        public string XLS_TOTAL_CASE_LABEL { get; set; }


        public double TOTAL_CONTAINER { get; set; }
        public double TOTAL_PEB { get; set; }

        public double GROSS_WEIGHT { get; set; }
        public double NET_WEIGHT { get; set; }
        public double MEASUREMENT { get; set; }
        public string PACKING_COMPANY { get; set; }

        //add by agi 2016-07-01 si by air (vanning scrn)
        public string BYAIR_DGCARGO { get; set; }
        public string BYAIR_COVER_SI1 { get; set; }
        public string BYAIR_COVER_SI2 { get; set; }
        public string BYAIR_COVER_SI3 { get; set; }
        public string BYAIR_MESSRS_SI1 { get; set; }
        public string BYAIR_MESSRS_SI2 { get; set; }
        public string BYAIR_MESSRS_SI3 { get; set; }
        public string BYAIR_FLIGHT_SI { get; set; }
        public string BYAIR_CONNECTING_FLIGHT_SI { get; set; }
        public DateTime? DEPARTURE_ABOUT { get; set; }
        public DateTime? DEPARTURE_ABOUT2 { get; set; }
        public string BYAIR_TITLE_SI { get; set; }
        public string BYAIR_NOTED_SI1 { get; set; }
        public string BYAIR_NOTED_SI2 { get; set; }
        public string BYAIR_NOTED_SI3 { get; set; }
        public string BYAIR_NOTED_SI4 { get; set; }
        public string BYAIR_NOTED_SI5 { get; set; }
        public string BYAIR_NOTED_SI6 { get; set; }
        public string BYAIR_MARKS_NUMBER { get; set; }
        public string BYAIR_COUNTRY { get; set; }
        public string VANNING_NO { get; set; }
        public string DG { get; set; }
        public string MSDS { get; set; }

        public string COMPANY { get; set; }

        public string SI_NAME { get; set; }
        public string MASTER_INVOICE_NO { get; set; }
        public string PART_OF_INVOICE_NO { get; set; }
        public string MASTER_PD_CD { get; set; }
        public string PART_OF_PD_CD { get; set; }

        public string SHEET_NAME { get; set; }
        public string XLS_SI_NEED_SPLIT_LABEL { get; set; }
        //ADD BY AGI 2016-05-09
        public string WITH_SINO_BYAIR { get; set; }

        public List<ReadyCargoCase> AttachmentReadyCargoCase { get; set; }
        public string GetReportDBPath()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("Get_Report_DB_Path");
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault().Text : "";
        }

        //public List<mShippingInstruction> getListShippingInstruction(string pBuyerCode, DateTime pCreatedDateFrom, DateTime pCreatedDateTo, string pVersion, string pPackingCompany, string pSI_NO)
        
        public List<mShippingInstruction> getListShippingInstruction(string pBuyerCode, DateTime pCreatedDateFrom,
            DateTime pCreatedDateTo, string pVersion, string pPackingCompany, string pSI_NO, DateTime pETD_FROM, DateTime pETD_TO)

        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Search",
                            new { BUYER_CD = pBuyerCode },
                            new { CREATED_DT_FROM = pCreatedDateFrom },
                            new { CREATED_DT_TO = pCreatedDateTo },
                            new { VERSION = pVersion },
                            new { PACKING_COMPANY = pPackingCompany },
                            new { SI_NO = pSI_NO },
                            //add agi 2017-09-18
                            new { pETD_FROM = pETD_FROM },
                            new { pETD_TO = pETD_TO }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mShippingInstruction> getListSIInvoice(string pSHIP_INSTRUCTION_NO)
        //public List<mShippingInstruction> getListSIInvoice()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Search_Invoice",
                            new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO }
                  );
            db.Close();

            if (l == null)
                return new List<mShippingInstruction>();
            else
                return l.ToList();
        }

        public List<mShippingInstruction> getListSICaseDownloadData(string pSHIP_INSTRUCTION_NO, string pVERSION)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Get_Case_Download",
                  new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                  new { VERSION = pVERSION }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mShippingInstruction> getListSIBreakdownDownloadData(string pSHIP_INSTRUCTION_NO, string pVERSION)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Get_Breakdown_Download",
                  new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                  new { VERSION = pVERSION }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mShippingInstruction> getListSIContainerDownloadData(string pSHIP_INSTRUCTION_NO, string pVERSION, int pMODE, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Get_Container_Download",
                  new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                  new { VERSION = pVERSION },
                  new { MODE = pMODE },
                  new { USER_ID = pUSER_ID }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mShippingInstruction> getListSIVanningDownloadData(string pSHIP_INSTRUCTION_NO, string pVERSION)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Get_Vanning_Download",
                  new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                  new { VERSION = pVERSION }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mShippingInstruction> getListSIPEBDownloadData(string pSHIP_INSTRUCTION_NO, string pVERSION)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Get_PEB_Download",
                  new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                  new { VERSION = pVERSION }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mShippingInstruction> getListBuyerCD()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Get_BuyerCDList");
            db.Close();

            return l.ToList();
        }


        public List<mShippingInstruction> getListContainerGrade()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Get_ContainerGradeList");
            db.Close();

            return l.ToList();
        }

        public mShippingInstruction getShippingInstructionDownload(string pSHIP_INSTRUCTION_NO, string pVERSION, string pSHEET_NAME)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Get_Download_Data",
                  new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                  new { VERSION = pVERSION },
                  new { SHEET_NAME = pSHEET_NAME }
                  );
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault() : new mShippingInstruction();
        }

        public mShippingInstruction getShippingInstruction(string pSHIP_INSTRUCTION_NO, string pVERSION)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Get_Data",
                  new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                  new { VERSION = pVERSION }
                  );
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault() : new mShippingInstruction();
        }

        public List<mShippingInstruction> getShippingInstructionDetail(string pSHIP_INSTRUCTION_NO, string pVERSION)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Get_Detail_Data",
                  new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                  new { VERSION = pVERSION }
                  );
            db.Close();

            return l.ToList();
        }

        public mShippingInstruction getNewShippingIntruction(string pBuyerCode, DateTime pVanningDateFrom, DateTime pVanningDateTo, string pVersion, string pPackingCompany)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_GetNewData",
                  new { BUYER_CD = pBuyerCode },
                  new { VANNING_DT_FROM = pVanningDateFrom },
                  new { VANNING_DT_TO = pVanningDateTo },
                  new { VERSION = pVersion },
                  new { PACKING_COMPANY = pPackingCompany }
                  );
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault() : new mShippingInstruction();
        }

        public string DeleteData(string pSHIP_INSTRUCTION_NO, string pVERSION, string pUSER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Delete_Data",
                  new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                  new { VERSION = pVERSION },
                  new { USER_ID = pUSER_ID }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }

            return result;
        }

        public string CheckCreateTent(string pBUYER_CD, DateTime pVANNING_DT_FROM, DateTime pVANNING_DT_TO, string pPACKING_COMPANY)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_Create_Tentative_Validation",
                  new { BUYER_CD = pBUYER_CD },
                  new { VANNING_DT_FROM = pVANNING_DT_FROM },
                  new { VANNING_DT_TO = pVANNING_DT_TO },
                  new { PACKING_COMPANY = pPACKING_COMPANY }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.Text;
            }

            return result;
        }

        public string ConfirmTentSave(string pBUYER_CD, DateTime pVANNING_DT_FROM, DateTime pVANNING_DT_TO, string pPACKING_COMPANY, string pVERSION,
        string pSHIP_INSTRUCTION_NO, string pSHIPPER, string pCONSIGNEE, string pNOTIFY_PARTY, string pCONTAINER_GRADE, string pUserID)
        {
            string result = "error ";

            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mShippingInstruction>("SI_Confirm_Tentative_Save",
                        new { BUYER_CD = pBUYER_CD },
                        new { VANNING_DT_FROM = pVANNING_DT_FROM },
                        new { VANNING_DT_TO = pVANNING_DT_TO },
                        new { PACKING_COMPANY = pPACKING_COMPANY },
                        new { VERSION = pVERSION },
                        new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                        new { SHIPPER = pSHIPPER },
                        new { CONSIGNEE = pCONSIGNEE },
                        new { NOTIFY_PARTY = pNOTIFY_PARTY },
                        new { CONTAINER_GRADE = pCONTAINER_GRADE },
                        new { USER_ID = pUserID }
                      );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string ConfirmFirmSave(string pSHIP_INSTRUCTION_NO, string pVERSION, string pSHIPPER, string pCONSIGNEE, string pNOTIFY_PARTY, string pCONTAINER_GRADE, string pINVOICE_LIST, string pUserID)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mShippingInstruction>("SI_Confirm_Firm_Save",
                        new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                        new { VERSION = pVERSION },
                        new { SHIPPER = pSHIPPER },
                        new { CONSIGNEE = pCONSIGNEE },
                        new { NOTIFY_PARTY = pNOTIFY_PARTY },
                        new { CONTAINER_GRADE = pCONTAINER_GRADE },
                        new { INVOICE_LIST = pINVOICE_LIST },
                        new { USER_ID = pUserID }
                      );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public List<mShippingInstruction> ConfirmFirmSaveWithListResult(long processID, string pSHIP_INSTRUCTION_NO, string pVERSION, string pSHIPPER, string pCONSIGNEE, string pNOTIFY_PARTY, string pCONTAINER_GRADE, string pINVOICE_LIST, string pUserID)
        {
            //string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mShippingInstruction>("SI_Confirm_Firm_Save",
                        new { PROCESS_ID = processID },
                        new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                        new { VERSION = pVERSION },
                        new { SHIPPER = pSHIPPER },
                        new { CONSIGNEE = pCONSIGNEE },
                        new { NOTIFY_PARTY = pNOTIFY_PARTY },
                        new { CONTAINER_GRADE = pCONTAINER_GRADE },
                        new { INVOICE_LIST = pINVOICE_LIST },
                        new { USER_ID = pUserID }
                      );
                db.Close();

                return l.ToList();
            }
            catch (Exception ex)
            {

            }
            return new List<mShippingInstruction>();
        }

        public List<mShippingInstruction> GetListSIFirm(string pBuyerCode, DateTime pVanningDateFrom, DateTime pVanningDateTo,
            string pPackingCompany)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_GetFirmList",
                            new { BUYER_CD = pBuyerCode },
                            new { VANNING_DT_FROM = pVanningDateFrom },
                            new { VANNING_DT_TO = pVanningDateTo },
                            new { PACKING_COMPANY = pPackingCompany }
                  );
            db.Close();

            return l.ToList();
        }

        public List<mShippingInstruction> GetListSIFirmDownload(string pSHIP_INSTRUCTION_NO, string pUSER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_GetFirmListDownload",
                            new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                            new { USER_ID = pUSER_ID }
                  );
            db.Close();

            return l.ToList();
        }

        public bool CheckSINoExist(string pSHIP_INSTRUCTION_NO)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("SI_CheckSINoExist",
                  new { SHIP_INSTRUCTION_NO = pSHIP_INSTRUCTION_NO },
                  new { VERSION = "F" }
                  );
            db.Close();

            return l.Count >= 1 ? true : false;
        }

        public string CreateFirmValidation(string pINVOICE_LIST)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mShippingInstruction>("SI_Create_Firm_Validation",
                        new { INVOICE_LIST = pINVOICE_LIST }
                      );
                db.Close();

                foreach (var message in l.ToList())
                {
                    result = message.Text;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        //add agi 2017-09-11
        public List<mShippingInstruction> GetListRePrint(string SI_NO,string VERSION)
        {
           // string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<mShippingInstruction> result = new List<mShippingInstruction>();
            result = db.Fetch<mShippingInstruction>("SI_GetReprint",
                  new { SI_NO = SI_NO },
                  new { VERSION = VERSION }
                  ).ToList();
            db.Close();

            return result;
        }
    
    }
}
