﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPOSSConfirmation
    {
        public DateTime POSS_DATE { get; set; }
        public string CUST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string INVOICE_NO { get; set; }
        public string INVOICE_ITEM_NO { get; set; }
        public string SEQUENCE { get; set; }
        public string BO_RELEASE_FLAG { get; set; }
        public string PORTION_CODE { get; set; }
        public string RECORD_ID { get; set; }
        public string DATA_ID { get; set; }
        public string BO_CODE { get; set; }
        public string RA_CODE { get; set; }
        public string ORDER_ITEM_NO { get; set; }
        public string PROCESS_PART_NO { get; set; }
        public string ORDER_PART_NO { get; set; }
        public string PROCESS_QTY { get; set; }
        public string ORDER_QTY { get; set; }
        public string UNIT_FOB_PRICE { get; set; }
        public string FINAL_FOB_PRICE { get; set; }
        public string TARIF_CODE { get; set; }
        public string HS_CODE { get; set; }
        public string DIST_COLUMN { get; set; }
        public DateTime? ETD { get; set; }
        public DateTime TMC_PROCESS_DT { get; set; }
        public DateTime PRICE_CALC_DT { get; set; }
        public string FILLER { get; set; }
        public DateTime SEND_DT { get; set; }
        public string ICW_PRICE { get; set; }
        public string ICW_PARTFRANCHCDA { get; set; }
        public string ICW_PARTFRANCHCDF { get; set; }
        public string VALIDATION_STATUS { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public int TOTAL_REPROCESS { get; set; }

        public string Text { get; set; }

        public string VALIDATION_CD { get; set; }
        public string VALIDATION_DESC { get; set; }
        public DateTime? TMAP_SEND_DT { get; set; }
        public string TMAP_SEND { get; set; }
        public string BO_INSTRUCT_CD { get; set; }
        public string BO_INSTRUCT_DESC { get; set; }
        public string PORTION_DESC { get; set; }
        public string BO_DESC { get; set; }
        public string RA_DESC { get; set; }

        public string PACKING_COMPANY { get; set; }
        public string TGA_TGP_FLAG { get; set; }

        //Calling data while searching process
        public List<mPOSSConfirmation> getListPOSSConfirmation(string pPOSSDateFrom, string pPOSSDateTo, string pOrderNo, string pPartNo,
            string pItemNo, string pRACode, string pBOCode, string pPortionCode, string pBOInstrCode, string pSendFlag, string pSendDateFrom,
            string pSendDateTo, string pValidationStatus, string pPackingCompany, string pTGATGPFlag)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var list = db.Fetch<mPOSSConfirmation>("GetPOSSConfirmation",
               new
               {
                   POSS_DATE_FROM = pPOSSDateFrom,
                   POSS_DATE_TO = pPOSSDateTo,
                   ORDER_NO = pOrderNo,
                   PART_NO = pPartNo,
                   ORDER_ITEM_NO = pItemNo,
                   RA_CODE = pRACode,
                   BO_CODE = pBOCode,
                   PORTION_CODE = pPortionCode,
                   BO_INSTRUCT_CD = pBOInstrCode,
                   SEND_FLAG = pSendFlag,
                   TMAP_SEND_DT_FROM = pSendDateFrom,
                   TMAP_SEND_DT_TO = pSendDateTo,
                   VALIDATION_CD = pValidationStatus,
                   PACKING_COMPANY = pPackingCompany,
                   TGA_TGP_FLAG = pTGATGPFlag
               }
                );
            db.Close();
            return list.ToList();

        }

        public List<mPOSSConfirmation> DownloadPOSS(string pPOSSDateFrom, string pPOSSDateTo, string pOrderNo, string pPartNo, string pItemNo, string pRACode, string pBOCode, string pPortionCode, string pBOInstrCode, string pSendFlag, string pSendDateFrom, string pSendDateTo, string pValidationStatus)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var list = db.Fetch<mPOSSConfirmation>("GetPOSSConfirmation",
                        new object[] { pPOSSDateFrom, pPOSSDateTo, pOrderNo, pPartNo, pItemNo, pRACode, pBOCode, pPortionCode, pBOInstrCode, pSendFlag, pSendDateFrom, pSendDateTo, pValidationStatus });
            db.Close();
            return list.ToList();

        }

        //Get Data for Combo Box
        public List<mPOSSConfirmation> getListBOCode()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var list = db.Fetch<mPOSSConfirmation>("GetBOCode");
            db.Close();
            return list.ToList();
        }

        //Get Data for Combo Box RA Code
        public List<mPOSSConfirmation> getListRACode()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var list = db.Fetch<mPOSSConfirmation>("GetRACode");
            db.Close();


            return list.ToList();
        }

        //Get Data for Combo Box RA Code Pop Up
        public List<mPOSSConfirmation> getListPopUpRACode()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var list = db.Fetch<mPOSSConfirmation>("GetRACode");
            db.Close();
            mPOSSConfirmation addlist = new mPOSSConfirmation();
            addlist.RA_CODE = "Select All";
            list.Insert(0, addlist);

            return list.ToList();
        }

        ////Get Data for Combo Box RA Code Pop up
        //public List<mPOSSConfirmation> getListPopUpRACode()
        //{
        //    IDBContext db = DatabaseManager.Instance.GetContext();
        //    var list = db.Fetch<mPOSSConfirmation>("GetRACode");
        //    db.Close();
        //    return list.ToList();
        //}

        //Get Data for Combo Box
        public List<mPOSSConfirmation> getListPortionCode()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var list = db.Fetch<mPOSSConfirmation>("GetPortionCode");
            db.Close();

            mPOSSConfirmation addlist = new mPOSSConfirmation();
            addlist.PORTION_CODE = "";

            list.Insert(0, addlist);
            return list.ToList();
        }

        public int getValidate_Reprocess()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPOSSConfirmation>("Reprocess_POSS_Validation");
            db.Close();
            return l.ToList()[0].TOTAL_REPROCESS;
        }


        //Insert Parameter to TB_R_BACKGROUNDTASK_REGISTRY
        public string InsertParam(string UserID, string PackingCompany)
        {
            string result = "";

            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mPOSSConfirmation>("POSSConfirmation_SendParam", new
                {
                    USER_ID = UserID,
                    PACKING_COMPANY = PackingCompany
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {

                result = "Error | " + Convert.ToString(err.Message);

            }

            return result;
        }

        //Insert Parameter to TB_R_BACKGROUNDTASK_REGISTRY for Re Validate POSS Warning Data 
        public string InsertParam_synchronize(string UserID)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mPOSSConfirmation>("POSS_Synchronize", new
                {
                    USER_ID = UserID
                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }
            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }

        //edit data POSS
        public string UpdateData(string p_POSSDate, string p_CustNo, string p_OrderNo, string p_InvoiceNo,
            string p_InvoiceItem, string p_BOReleaseFlag, string p_PortionCd, string p_OrderPartNo, string p_RACode, string p_ItemNo,
            string p_ProcessPartNo, string p_CHANGED_BY)
        {
            string result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                var list = db.Fetch<mPOSSConfirmation>("POSSConfirmation_Update", new
                {
                    POSS_DATE = p_POSSDate,
                    CUST_NO = p_CustNo,
                    ORDER_NO = p_OrderNo,
                    INVOICE_NO = p_InvoiceNo,
                    INVOICE_ITEM_NO = p_InvoiceItem,
                    BO_RELEASE_FLAG = p_BOReleaseFlag,
                    PORTION_CODE = p_PortionCd,
                    ORDER_PART_NO = p_OrderPartNo,
                    RA_CODE = p_RACode,
                    ORDER_ITEM_NO = p_ItemNo,
                    PROCESS_PART_NO = p_ProcessPartNo,
                    CHANGED_BY = p_CHANGED_BY,
                    MSG_TEXT = ""

                });
                db.Close();

                foreach (var message in list)
                {
                    result = message.Text;
                }

            }
            catch (Exception err)
            {
                result = "Error | " + Convert.ToString(err.Message);
            }
            return result;
        }
    }
}
