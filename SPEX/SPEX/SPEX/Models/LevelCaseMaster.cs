﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class LevelCaseMaster
    {
        public string PART_NO { get; set; }
        public string ORDER_TYPE { get; set; }
        public string BUYER_PD { get; set; }
        public string TRANSP_CD { get; set; }
        public string HANDLING_TYPE { get; set; }
        public string FINISH_CASE_FLAG { get; set; }
        public string DELETION_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        //public DateTime CHANGED_DT { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public List<mSystemMaster> HANDLING_TYPEs { get { return GetDataBySystemMaster("level_case", "HANDLING_TYPE").Where(a=>a.SYSTEM_CD!="ALL").ToList(); } }
        public List<Transportation> Transporations { get { return new Transportation().GetAllTransportation(); } }

        public List<string> OrderTypes { get { return new BUYER_ORDER_TYPE_MASTER().GetDistinctBUYER_ORDER_TYPE(); } }

        public List<mBuyerPD> BuyerPDs { get { return new mBuyerPD().GetAllBuyerPDNoDelete(); } }

        public List<mSystemMaster> Finish_Case_Flags { get { return getFinish_Case_Flags(); } }
        
        public List<mSystemMaster> getFinish_Case_Flags()
        {
            List<mSystemMaster> fcf= GetDataBySystemMaster("level_case", "Finish_Case_Flag");
            fcf = fcf.Where(a => a.SYSTEM_CD != "ALL").ToList();
            return fcf;
        }
        
        public List<LevelCaseMaster> GetData(LevelCaseMaster LC)
        {
            List<LevelCaseMaster> l= new List<LevelCaseMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            l = db.Fetch<LevelCaseMaster>("LevelCaseMaster_Search",
                new { PART_NO = LC.PART_NO },
                new { ORDER_TYPE = LC.ORDER_TYPE },
                new { BUYER_PD = LC.BUYER_PD },
                new { TRANSP_CD = LC.TRANSP_CD },
                new { HANDLING_TYPE = LC.HANDLING_TYPE },
                new { FINISH_CASE_FLAG = LC.FINISH_CASE_FLAG }
                ).ToList();
            db.Close();
            return l;
        }

        public List<mSystemMaster> GetDataBySystemMaster(string System_Type, string System_Code)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<mSystemMaster> result = db.Fetch<mSystemMaster>("LevelCaseMaster_GetSystemMaster",
                 new { System_Type = System_Type },
                 new { System_Code = System_Code }
                ).ToList();
            db.Close();
            return result;
        }

        public string AddData (LevelCaseMaster LC)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("LevelCaseMaster_Add",
                 new { PART_NO = LC.PART_NO },
                 new { ORDER_TYPE = LC.ORDER_TYPE },
                 new { BUYER_PD = LC.BUYER_PD },
                 new { TRANSP_CD = LC.TRANSP_CD },
                 new { HANDLING_TYPE = LC.HANDLING_TYPE },
                 new { FINISH_CASE_FLAG = LC.FINISH_CASE_FLAG },
                 new { DELETION_FLAG = LC.DELETION_FLAG },
                 new { CREATED_BY = LC.CREATED_BY }
                );
            db.Close();
            return result;
        }

        public string UpdateData(LevelCaseMaster LC)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("LevelCaseMaster_Update",
                 new { PART_NO = LC.PART_NO },
                 new { ORDER_TYPE = LC.ORDER_TYPE },
                 new { BUYER_PD = LC.BUYER_PD },
                 new { TRANSP_CD = LC.TRANSP_CD },
                 new { HANDLING_TYPE = LC.HANDLING_TYPE },
                 new { FINISH_CASE_FLAG = LC.FINISH_CASE_FLAG },
                 new { DELETION_FLAG = LC.DELETION_FLAG },
                 new { CHANGED_BY = LC.CHANGED_BY }
                );
            db.Close();
            return result;
        }

        public string Delete(List<LevelCaseMaster> LCs,string UserId)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            string result = string.Empty;
            foreach(LevelCaseMaster LC in LCs)
            {
                result = db.SingleOrDefault<string>("LevelCaseMaster_delete",
                new { PART_NO = LC.PART_NO },
                new { ORDER_TYPE = LC.ORDER_TYPE },
                new { BUYER_PD = LC.BUYER_PD },
                new { TRANSP_CD = LC.TRANSP_CD },
                new { HANDLING_TYPE = LC.HANDLING_TYPE },
                new { FINISH_CASE_FLAG = LC.FINISH_CASE_FLAG },
                new { DELETION_FLAG = LC.DELETION_FLAG },
                new { CHANGED_BY = UserId }
                );
            }              
            db.Close();
            result = result + LCs.Count.ToString() + " rows";
            return result;
        }

        public string DeleteDataTBT()
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("LevelCaseMaster_Delete_Temp");
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

        public string BackgroundProsessSave(string function_id, string module_id, string user_id, long PID)
        {

            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("LevelCaseMaster_call_job",
                       new { function_id = function_id },
                       new { module_id = module_id },
                       new { user_id = user_id },
                       new { PID = PID }
                       );
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result = "1|" + ex.Message;
            }
            return result;
        }


    }
}