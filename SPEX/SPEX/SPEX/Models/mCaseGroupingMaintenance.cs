﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using SPEX.Cls;

namespace SPEX.Models
{
    public class mCaseGroupingMaintenance
    {
        //add fid.mario 2018-02-28 for packing revise
        public class mCancelOrder
        {
            public string DATAID { get; set; }
            public string MANIFEST_NO { get; set; }
            public string TMAP_ORDER_NO { get; set; }
            public string TMAP_ITEM_NO { get; set; }
            public string TMAP_PART_NO { get; set; }
            public string TEXT { get; set; }
        }

        //add fid.mario 2018-02-28 for packing revise
        public string CREATED_BY { get; set; }

        public string PROCESS_ID { get; set; }

        public string DeleteCaseGroupingDataTBT()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPartPriceMaster>("CaseGroupingMaintenance_DeleteCaseGroupingTBT");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public string UploadCaseGroupingBackgroundProcess(string UserID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mUploadTLMS>("CaseGroupingMaintenance_UploadCaseGroupingBackgroundProcess",
                    new
                    {
                        USER_ID = UserID
                    }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string DeleteFinishCaseDataTBT()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPartPriceMaster>("CaseGroupingMaintenance_DeleteFinishCaseTBT");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public string DeleteCancelOrderDataTBT() //add fid.mario 28/02/2018
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPartPriceMaster>("CaseGroupingMaintenance_DeleteCancelOrderTBT");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public string UploadFinishCaseBackgroundProcess(string UserID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mUploadTLMS>("CaseGroupingMaintenance_UploadFinishCaseBackgroundProcess",
                    new
                    {
                        USER_ID = UserID
                    }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string UploadCancelOrderBackgroundProcess(string UserID) //add fid.mario 28/02/2018
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            string l = db.ExecuteScalar<string>("CaseGroupingMaintenance_UploadCancelOrderBackgroundProcess",
                    new { USER_ID = UserID }
                );
            db.Close();
            //foreach (var message in l.ToList())
            //{
            //    result = message.TEXT;
            //}

            return l;
        }

        public string DeleteTempCancelOrder()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPackingCompletion>("CancelOrder_DeleteTemp");
                db.Close();
                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        //add fid.mario 2018-02-28 for packing revise
        public string InsertCancelOrderTemp(mCancelOrder data, string userID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mCancelOrder>("CancelOrder_InsertTemp",
                             new { DATAID = data.DATAID },
                             new { MANIFEST_NO = data.MANIFEST_NO },
                             new { TMAP_ORDER_NO = data.TMAP_ORDER_NO },
                             new { TMAP_ITEM_NO = data.TMAP_ITEM_NO },
                             new { TMAP_PART_NO = data.TMAP_PART_NO },
                             new { CREATED_BY = userID }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }
            return result;
        }
    }
}