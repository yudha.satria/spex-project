﻿using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mHSMaster
    {
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string HS_INDONESIA { get; set; }
        public string HS_PEB { get; set; }
        public string DELETE_FLAG { get; set; }
        public DateTime? DELETE_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string TEXT { get; set; }


        public List<mHSMaster> GetList(string pPART_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mHSMaster>("HSMaster_GetList",
                  new { PART_NO = pPART_NO }
                  );
            db.Close();

            return l.ToList();
        }

        public mHSMaster GetHS(string pPART_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mHSMaster>("HSMaster_Get",
                  new { PART_NO = pPART_NO }
                  ).ToList();
            db.Close();

            return l.Count > 0 ? l[0] : new mHSMaster();
        }

        public string DeleteData(string pPART_NO, string pUSER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mHSMaster>("HSMaster_Delete",
                  new { PART_NO = pPART_NO },
                  new { USER_ID = pUSER_ID }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string UpdateData(string pPART_NO, string pHS_INDONESIA, string pHS_PEB, string pUSER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mHSMaster>("HSMaster_Update",
                  new { PART_NO = pPART_NO },
                  new { HS_INDONESIA = pHS_INDONESIA },
                  new { HS_PEB = pHS_PEB },
                  new { USER_ID = pUSER_ID }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string AddData(string pPART_NO, string pHS_INDONESIA, string pHS_PEB, string pUSER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mHSMaster>("HSMaster_Add",
                  new { PART_NO = pPART_NO },
                  new { HS_INDONESIA = pHS_INDONESIA },
                  new { HS_PEB = pHS_PEB },
                  new { USER_ID = pUSER_ID }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public mHSMaster GetPartName(string pPART_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mHSMaster>("HSMaster_GetPartName",
                  new { PART_NO = pPART_NO }
                  ).ToList();
            db.Close();

            return l.Count > 0 ? l[0] : new mHSMaster();
        }

        //add agi 2017-06-07
        public string DeleteTempData()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPartPriceMaster>("HS_Master_DeleteTemp");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        
        public string UploadHsBackgroundProcess(string UserID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPartPriceMaster>("HSMaster_Upload",
                             new { USER_ID = UserID }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }
        
    }
}