﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mReadyCargo
    {

        public string BUYERPD_CD { get; set; }
        public string ORDER_NO { get; set; }
        public DateTime? ORDER_DT { get; set; }
        public string PART_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string ORDER_TYPE { get; set; }
        public string TRANSPORTATION_CD { get; set; }
        public string ORDER_QTY { get; set; }
        public string ACCEPT_QTY { get; set; }
        
       
        public string PROCESS_QTY { get; set; }
       
        public DateTime? ETD_DATE { get; set; }
        public string SUPPLY_PART_NO { get; set; }
        public string PACKED_QTY { get; set; }//Cargo Ready Qty
        public string SHIPMENT_QTY { get; set; }
        public DateTime? PACKING_DT { get; set; }
        public string CASE_NO { get; set; }

        public string Text { get; set; }



        public List<mReadyCargo> getListReadyCargo(string pPartNo, string pOrderNo, string pItemNo, string pOrderDt, string pOrderDtTo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mReadyCargo>("ReadyCargoSearch",
                  new { PART_NO = pPartNo },
                  new { ORDER_NO = pOrderNo },
                  new { ITEM_NO = pItemNo },
                  new { ORDER_DT_FROM = pOrderDt },
                  new { ORDER_DT_TO = pOrderDtTo }
                  );
            db.Close();
            return l.ToList();

        }


        public List<mReadyCargo> CargoDownload(string D_PartNo, string D_OrderNo, string D_ItemNo, string D_OrderDt, string D_OrderDtTo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mReadyCargo>("ReadyCargoDownload", new
            {
                PART_NO = D_PartNo,
                ORDER_NO = D_OrderNo,
                ITEM_NO = D_ItemNo,
                ORDER_DT_FROM = D_OrderDt,
                ORDER_DT_TO = D_OrderDtTo


            });

            if (D_PartNo == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }

        public List<mReadyCargo> CargoDownloadTicked(string D_KeyDownload)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();


            var l = db.Fetch<mReadyCargo>("ReadyCargoDownloadTicked", new
            {
                D_KeyDownload = D_KeyDownload


            });

            if (D_KeyDownload == "NULLSTATE")
            {
                l.Clear();
            }
            db.Close();
            return l.ToList();
        }





    }
}
