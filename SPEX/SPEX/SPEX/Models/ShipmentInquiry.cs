﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class ShipmentInquiryComboBox
    {
        public string BUYER_CD { get; set; }
        public string BUYER_NAME { get; set; }

        public string FA_CD { get; set; }
        public string FA_NAME { get; set; }
        public string FA_SAP_CD { get; set; }

        public string SA_CD { get; set; }
        public string SA_NAME { get; set; }
        public string SA_SAP_CD { get; set; }
    }

    public class ShipmentInquiry
    {
        public string STATUS { get; set; }
        public string BOOKING_NO { get; set; }
        public string SHIPMENT_NO { get; set; }
        public string BUYER_CD { get; set; }
        public DateTime ETD { get; set; }
        public DateTime ATD { get; set; }
        public string ATD_ADDED_BY { get; set; }
        public string CONTAINER_QTY { get; set; }
        public string EXCHANGE_RATE { get; set; }
        public DateTime EXCHANGE_RATE_DT { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public DateTime APPROVED_DT { get; set; }
        public string APPROVED_BY { get; set; }
        public string FA { get; set; }
        public string FA_VENDOR_CD { get; set; }
        public string SA { get; set; }
        public string SA_VENDOR_CD { get; set; }
        public string FEEDER_VESSEL { get; set; }
        public string COMPLETENESS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }

        public string ETD_STR { get; set; }
        public string ATD_STR { get; set; }
        public string EXCHANGE_RATE_DT_STR { get; set; }
        public string APPROVED_DT_STR { get; set; }
        public string CREATED_DT_STR { get; set; }
        public string CHANGED_DT_STR { get; set; }

        public string ETD_FROM { get; set; }
        public string ETD_TO { get; set; }
        public string ATD_FROM { get; set; }
        public string ATD_TO { get; set; }
        public string CONTAINER_NO { get; set; }
        public string STATUS_SEARCH { get; set; }
        public string FA_SEARCH { get; set; }
        public string SA_SEARCH { get; set; }

        public List<String> ComboBoxGetBookingNo()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<String>("GetBookingNo", new { });
            db.Close();
            return l.ToList();
        }

        public List<String> ComboBoxGetStatus()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<String>("GetShipmentStatus", new { });
            db.Close();
            return l.ToList();
        }

        public List<ShipmentInquiryComboBox> ComboBoxGetBuyer(string CODE, string NAME)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ShipmentInquiryComboBox> l = db.Fetch<ShipmentInquiryComboBox>("BUYERCD_Search",
                    new { BUYYER_CD = CODE },
                    new { BUYER_NAME = NAME }
                  ).ToList();
            db.Close();
            return l;
        }

        public List<ShipmentInquiryComboBox> ComboBoxGetForwardAgent(string CODE, string NAME)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ShipmentInquiryComboBox> l = db.Fetch<ShipmentInquiryComboBox>("FA_Search",
                    new { FA_CD = CODE },
                    new { FA_NAME = NAME }
                  ).ToList();
            db.Close();
            return l;
        }

        public List<ShipmentInquiryComboBox> ComboBoxGetForwardAgentSAP()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ShipmentInquiryComboBox> l = db.Fetch<ShipmentInquiryComboBox>("FA_SAP_Search", new { }).ToList();
            db.Close();
            return l;
        }

        public List<ShipmentInquiryComboBox> ComboBoxGetShipAgent(string CODE, string NAME)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ShipmentInquiryComboBox> l = db.Fetch<ShipmentInquiryComboBox>("SA_Search",
                    new { SA_CD = CODE },
                    new { SA_NAME = NAME }
                  ).ToList();
            db.Close();
            return l;
        }

        public List<ShipmentInquiryComboBox> ComboBoxGetShipAgentSAP()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ShipmentInquiryComboBox> l = db.Fetch<ShipmentInquiryComboBox>("SA_SAP_Search", new { }).ToList();
            db.Close();
            return l;
        }

        public List<ShipmentInquiry> getShipmentInquiry(string BookingNo, string BuyerCd, string FA_Search,
                                                        string ETDFrom, string ETDTo, string SA_Search,
                                                        string ATDFrom, string ATDTo, string Shpmnt_Search,
                                                        string ContainerNo, string FeederVessel, string Status_Search,
                                                        string FAVendorCD, string SAVendorCD, string Completeness_Search)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ShipmentInquiry> l = db.Fetch<ShipmentInquiry>("ShipmentInquiry_Search",
                  new { BOOKING_NO = BookingNo },
                  new { BUYER_CD = BuyerCd },
                  new { FA = FA_Search },
                  new { ETD_FROM = ETDFrom },
                  new { ETD_TO = ETDTo },
                  new { SA = SA_Search },
                  new { ATD_FROM = ATDFrom },
                  new { ATD_TO = ATDTo },
                  new { SHIPMENT_NO = Shpmnt_Search },
                  new { CONTAINER_NO = ContainerNo },
                  new { FEEDER_VESSEL = FeederVessel },
                  new { STATUS = Status_Search },
                  new { FA_VENDOR_CD = FAVendorCD },
                  new { SA_VENDOR_CD = SAVendorCD },
                  new { COMPLETENESS = Completeness_Search }
                  ).ToList();
            db.Close();
            return l;
        }

        public string CheckEditATD(string ShipmentNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ShipmentInquiryCheckEditETD",
                 new { SHIPMENT_NO = ShipmentNo }
                );
            db.Close();
            return result;
        }

        public string EditATD(string editATD, string ShipmentNo, string BookingNo, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ShipmentInquiryEditETD",
                 new { ATD = editATD },
                 new { SHIPMENT_NO = ShipmentNo },
                 new { BOOKING_NO = BookingNo },
                 new { USER_ID = UserID }
                );
            db.Close();
            return result;
        }

        public string CreateService(string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ShipmentInquiryCreateService",
                 new { USER_ID = UserID }
                );
            db.Close();
            return result;
        }

        public string ReprocessService(string ShipmentList, string CheckAll, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ShipmentInquiryReprocessService",
                 new { SHIPMENT_LIST = ShipmentList },
                 new { CHECK_ALL = CheckAll },
                 new { USER_ID = UserID }
                );
            db.Close();
            return result;
        }

        public string GetExchangeRate(string ShipmentList, string CheckAll, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ShipmentInquiryGetExchangeRateService",
                 new { SHIPMENT_LIST = ShipmentList },
                 new { CHECK_ALL = CheckAll },
                 new { USER_ID = UserID }
                );
            db.Close();
            return result;
        }

        public string ApproveService(string ShipmentList, string CheckAll, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ShipmentInquiryApproveService",
                 new { SHIPMENT_LIST = ShipmentList },
                 new { CHECK_ALL = CheckAll },
                 new { USER_ID = UserID }
                );
            db.Close();
            return result;
        }

        public string CancelApproval(string ShipmentList, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ShipmentInquiryCancelApproveService",
                 new { SHIPMENT_LIST = ShipmentList },
                 new { USER_ID = UserID }
                );
            db.Close();
            return result;
        }

        public string CancelService(string ShipmentList, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ShipmentInquiryCancelService",
                 new { SHIPMENT_LIST = ShipmentList },
                 new { USER_ID = UserID }
                );
            db.Close();
            return result;
        }

        public string SendToEHS(string ShipmentList, string UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ShipmentInquirySendToEHS",
                 new { SHIPMENT_LIST = ShipmentList },
                 new { USER_ID = UserID }
                );
            db.Close();
            return result;
        }

        public string CheckJob()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string result = db.SingleOrDefault<string>("ShipmentInquiryCheckJob");
            db.Close();
            return result;
        }
    }

    public class ShipmentInquiryDetailData
    {
        public string INVOICE_NO { get; set; }
        public DateTime INVOICE_DT { get; set; }
        public string CONTAINER_NO { get; set; }
        public string VENDOR_CD { get; set; }
        public string VENDOR_TYPE { get; set; }
        public string TOTAL_QTY { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public string PEB_NO { get; set; }
        public DateTime PEB_DT { get; set; }
        public string COMPLETENESS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }

        public string INVOICE_DT_STR { get; set; }
        public string PEB_DT_STR { get; set; }
        public string CREATED_DT_STR { get; set; }
        public string CHANGED_DT_STR { get; set; }

        public string SHIPMENT_NO { get; set; }
        public string SHIPPING_INSTR_NO { get; set; }

        public List<ShipmentInquiryDetailData> GetData(string BookingNo, string ShipmentNo, string InvoiceNo, string PEBNo)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ShipmentInquiryDetailData> l = db.Fetch<ShipmentInquiryDetailData>("ShipmentInquiryDetSearch",
                    new { BOOKING_NO = BookingNo },
                    new { SHIPMENT_NO = ShipmentNo }, 
                    new { INVOICE_NO = InvoiceNo },
                    new { PEB_NO = PEBNo }
                ).ToList();
            db.Close();
            return l;
        }

        public List<ShipmentInquiryDetailData> GetDataDownload(string BookingNo, string BuyerCd, string FA_Search,
                                                               string ETDFrom, string ETDTo, string SA_Search,
                                                               string ATDFrom, string ATDTo, string Shpmnt_Search,
                                                               string ContainerNo, string FeederVessel, string Status_Search,
                                                               string FAVendorCD, string SAVendorCD, string Completeness_Search)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ShipmentInquiryDetailData> l = db.Fetch<ShipmentInquiryDetailData>("ShipmentInquiryDetDownload",
                    new { BOOKING_NO = BookingNo },
                    new { BUYER_CD = BuyerCd },
                    new { FA = FA_Search },
                    new { ETD_FROM = ETDFrom },
                    new { ETD_TO = ETDTo },
                    new { SA = SA_Search },
                    new { ATD_FROM = ATDFrom },
                    new { ATD_TO = ATDTo },
                    new { SHIPMENT_NO = Shpmnt_Search },
                    new { CONTAINER_NO = ContainerNo },
                    new { FEEDER_VESSEL = FeederVessel },
                    new { STATUS = Status_Search },
                    new { FA_VENDOR_CD = FAVendorCD },
                    new { SA_VENDOR_CD = SAVendorCD },
                    new { COMPLETENESS = Completeness_Search }
                ).ToList();
            db.Close();
            return l;
        }
    }

    public class ShipmentInquiryDetailPrice
    {   
        public string SERVICE_ID { get; set; }
        public string INVOICE_NO { get; set; }
        public string VENDOR_CD { get; set; }
        public string VENDOR_CD_SAP { get; set; }
        public string VENDOR_TYPE { get; set; }
        public string VENDOR_NAME { get; set; }
        public string SERVICE_CODE { get; set; }
        public string SERVICE_TYPE { get; set; }
        public string CURRENCY { get; set; }
        public string CONT_SIZE { get; set; }
        public string QTY { get; set; }
        public string UNIT_PRICE { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public string VAT { get; set; }
        public string ADDITIONAL_INFO { get; set; }
        public string COMPLETENESS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }

        public string CREATED_DT_STR { get; set; }
        public string CHANGED_DT_STR { get; set; }

        public string SHIPMENT_NO { get; set; }
        
        public List<ShipmentInquiryDetailPrice> GetData(string BookingNo, string ShipmentNo, string InvoiceNo, string ServiceCode)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ShipmentInquiryDetailPrice> l = db.Fetch<ShipmentInquiryDetailPrice>("ShipmentInquiryDetShipSearch",
                    new { BOOKING_NO = BookingNo },
                    new { SHIPMENT_NO = ShipmentNo }, 
                    new { INVOICE_NO = InvoiceNo },
                    new { SERVICE_CODE = ServiceCode }
                ).ToList();
            db.Close();
            return l;
        }

        public List<ShipmentInquiryDetailPrice> GetDataDownload(string BookingNo, string BuyerCd, string FA_Search,
                                                                string ETDFrom, string ETDTo, string SA_Search,
                                                                string ATDFrom, string ATDTo, string Shpmnt_Search,
                                                                string ContainerNo, string FeederVessel, string Status_Search,
                                                                string FAVendorCD, string SAVendorCD, string Completeness_Search)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<ShipmentInquiryDetailPrice> l = db.Fetch<ShipmentInquiryDetailPrice>("ShipmentInquiryDetShipDownload",
                    new { BOOKING_NO = BookingNo },
                    new { BUYER_CD = BuyerCd },
                    new { FA = FA_Search },
                    new { ETD_FROM = ETDFrom },
                    new { ETD_TO = ETDTo },
                    new { SA = SA_Search },
                    new { ATD_FROM = ATDFrom },
                    new { ATD_TO = ATDTo },
                    new { SHIPMENT_NO = Shpmnt_Search },
                    new { CONTAINER_NO = ContainerNo },
                    new { FEEDER_VESSEL = FeederVessel },
                    new { STATUS = Status_Search },
                    new { FA_VENDOR_CD = FAVendorCD },
                    new { SA_VENDOR_CD = SAVendorCD },
                    new { COMPLETENESS = Completeness_Search }
                ).ToList();
            db.Close();
            return l;
        }
    }
}