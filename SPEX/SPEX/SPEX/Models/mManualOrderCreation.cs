﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mManualOrderCreation
    {
        public string MANUAL_ORDER_NO { get; set; } 
        public string SUPPLIER { get; set; } 
        public string SUB_SUPPLIER { get; set; } 
        public string DOCK_CD { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }  
        public string PIECES_KANBAN { get; set; }      
        public string KANBAN_QTY { get; set; }
        public string PIECES_QTY { get; set; }
        public string MANUAL_ORDER_TYPE { get; set; }
        public string MANUAL_ORDER_ID { get; set; }
        public string CREATED_BY { get; set; } 
         

        public mMaintenanceStockMaster GetPartName(string pPART_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mMaintenanceStockMaster>("MANUAL_ORDER_GET_PART_DATA",
                  new { PART_NO = pPART_NO }
                  );
            db.Close();
            return l.SingleOrDefault();
        }
        public List<mManualOrderCreation> getListManualOrderCreation(string user_id)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mManualOrderCreation>("ManualOrderCreation_Search",
                    new { CREATED_BY = user_id } 
                );
            db.Close();
            return l.ToList();
        }

        public string SaveData(string pMANUAL_ORDER_TYPE, string pSUPPLIER, string pSUPPLIER_PLANT, string pSUB_SUPPLIER, 
                               string pSUB_SUPPLIER_PLANT, string pDOCK, string pPART_NO, decimal pPIECES_KANBAN, decimal pKANBAN_QTY, decimal pPIECES_QTY,
                                string pCREATED_BY)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();

            string l = db.SingleOrDefault<string>("ManualOrderCreation_Save",
                    new { MODE = "add" }, 
                    new { pMANUAL_ORDER_TYPE = pMANUAL_ORDER_TYPE }, 
                    new { pSUPPLIER = pSUPPLIER },
                    new { pSUPPLIER_PLANT = pSUPPLIER_PLANT },
                    new { pSUB_SUPPLIER = pSUB_SUPPLIER },
                    new { pSUB_SUPPLIER_PLANT = pSUB_SUPPLIER_PLANT },
                    new { pDOCK = pDOCK },
                    new { pPART_NO = pPART_NO },
                    new { pPIECES_KANBAN = pPIECES_KANBAN }, 
                    new { pKANBAN_QTY = pKANBAN_QTY },
                    new { pPIECES_QTY = pPIECES_QTY },
                    new { CREATED_BY = pCREATED_BY }, 
                    new { MANUAL_ORDER_ID = "" }
                    );
            db.Close();
            return l;
        }

        public string DeleteDataManualOrder(string MANUAL_ORDER_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            string l = db.SingleOrDefault<string>("ManualOrderCreation_Save",
                    new { MODE = "delete" }, 
                    new { pMANUAL_ORDER_TYPE = "" },
                    new { pSUPPLIER = "" },
                    new { pSUPPLIER_PLANT = "" },
                    new { pSUB_SUPPLIER = "" },
                    new { pSUB_SUPPLIER_PLANT = "" },
                    new { pDOCK = "" },
                    new { pPART_NO = "" },
                    new { pPIECES_KANBAN = 0 },
                    new { pKANBAN_QTY = 0 },
                    new { pPIECES_QTY = 0 },
                    new { CREATED_BY = "" },
                    new { MANUAL_ORDER_ID = MANUAL_ORDER_ID }
                    );
            db.Close();
            return l;
        }

        public string CancelDataManualOrder(string pCREATED_BY)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            string l = db.SingleOrDefault<string>("ManualOrderCreation_Save",
                    new { MODE = "cancel" }, 
                    new { pMANUAL_ORDER_TYPE = "" },
                    new { pSUPPLIER = "" },
                    new { pSUPPLIER_PLANT = "" },
                    new { pSUB_SUPPLIER = "" },
                    new { pSUB_SUPPLIER_PLANT = "" },
                    new { pDOCK = "" },
                    new { pPART_NO = "" },
                    new { pPIECES_KANBAN = 0 },
                    new { pKANBAN_QTY = 0 },
                    new { pPIECES_QTY = 0 },
                    new { CREATED_BY = pCREATED_BY },
                    new { MANUAL_ORDER_ID = "" }
                    );
            db.Close();
            return l;
        }



        public string DeleteDataTBT()
        {
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("Manual_Order_Delete_TB_T_Manual_Order");
                    scope.Complete();
                }


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }


        public string BackgroundProcessSave(string function_id, string module_id, string user_id, long PID)
        { 
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("MANUAL_ORDER_UPLOAD_MASTER_CALL_JOB",
                       new { function_id = function_id },
                       new { module_id = module_id },
                       new { user_id = user_id },
                       new { PID = PID }
                       );
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result = "1|" + ex.Message;
            }
            return result;
        }

        public string BackgroundSubmitProcess(string function_id, string module_id, string user_id, long PID)
        { 
            string result = "SUCCES";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    db.Execute("MANUAL_ORDER_SUBMIT_CALL_JOB",
                       new { function_id = function_id },
                       new { module_id = module_id },
                       new { user_id = user_id },
                       new { PID = PID }
                       );
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result = "1|" + ex.Message;
            }
            return result;
        }
         

        #region Combobox
        public List<mSystemMaster> GetComboBoxSupplier()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetComboBoxSupplier").ToList();
            db.Close();
            return datas;
        } 

        public List<mSystemMaster> GetComboBoxSubSupplier(String SupCD, string PlanCD)
        {
            List<mSystemMaster> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MAIN_SUPP_CD = SupCD,
                    MAIN_PLANT_CD = PlanCD
                };
                result = db.Fetch<mSystemMaster>("MANUAL_ORDER_GET_SUB_SUPPLIER", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public List<mSystemMaster> GetComboBoxPartNo(String SupCD, String PlanCD, String SubSupCD, String SubPlanCD)
        {
            List<mSystemMaster> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    SUPPLIER_CD = SupCD,
                    SUPPLIER_PLANT = PlanCD,
                    SUB_SUPPLIER_CD = SubSupCD,
                    SUB_SUPPLIER_PLANT = SubPlanCD
                };
                result = db.Fetch<mSystemMaster>("MANUAL_ORDER_GET_PART_NO", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public List<mSystemMaster> GetComboBoxDock()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("GetDock").ToList();
            db.Close();
            return datas;
        }

        public List<mSystemMaster> GetComboBoxMOType()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            datas = db.Fetch<mSystemMaster>("MANUAL_ORDER_GET_MO_TYPE").ToList();
            db.Close();
            return datas;
        }

        #endregion
    }
}