﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class mPackingCompletion
    {
        public string PACKING_ID { get; set; }
        public string TMMIN_ORDER_NO { get; set; }
        public string BUYER_CD { get; set; }
        public string PD_CD { get; set; }
        public string PART_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string TMAP_ORDER_NO { get; set; }
        public DateTime? TMAP_ORDER_DT { get; set; }
        public string PART_QTY { get; set; }
        public string CASE_NO { get; set; }
        public string CASE_TYPE { get; set; }
        public string CASE_GROSS_WEIGHT { get; set; }
        public DateTime? PACKING_DT { get; set; }
        public string PACKING_COMPANY { get; set; }
        public string TRANSPORT_CD { get; set; }
        public string DANGER_FLAG { get; set; }
        public string CONTAINER_NO { get; set; }
        public DateTime? VANNING_DT { get; set; }
        public string SEAL_NO { get; set; }
        public string CONTAINER_SIZE { get; set; }
        public string CONTAINER_TYPE { get; set; }
        public string RC_FLAG { get; set; }
        public string VC_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        public string TEXT { get; set; }

        public class mPackingCompletionUpload
        {
            public string DATA_ID { get; set; }
            public string CASE_NO { get; set; }
            public string CASE_TYPE { get; set; }
            public string PACKING_DT { get; set; }
            public string PART_NO { get; set; }
            public string QTY { get; set; }
            public string TMMIN_ORDER_NO { get; set; }
        }

        //add agi 2016-12-13 for packing revise
        public class mPackingRevise
        {
            public string DATA_ID { get; set; }
            public string CASE_NO { get; set; }
            public string CASE_TYPE { get; set; }
            public string PACKING_DT { get; set; }
            public string PART_NO { get; set; }
            public string QTY { get; set; }
            public string TMMIN_ORDER_NO { get; set; }
            public string MODE { get; set; }
            public string TEXT { get; set; }
        }
        public List<mPackingCompletion> getListPackingCompletion(string p_PACKING_DT_FROM, string p_PACKING_DT_TO,
            string p_TMAP_ORDER_NO, string p_PART_NO, string p_ITEM_NO, string p_CASE_NO)
        {
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                var l = db.Fetch<mPackingCompletion>("PackingCompletion_Search",
                        new { PACKING_DT_FROM = p_PACKING_DT_FROM },
                        new { PACKING_DT_TO = p_PACKING_DT_TO },
                        new { TMAP_ORDER_NO = p_TMAP_ORDER_NO },
                        new { PART_NO = p_PART_NO },
                        new { ITEM_NO = p_ITEM_NO },
                        new { CASE_NO = p_CASE_NO }
                      );
                db.Close();
                return l.ToList();
            }
            catch (Exception ex)
            {
                return new List<mPackingCompletion>();
            }
        }

        public string Upload(string UserID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPackingCompletion>("PackingCompletion_Upload",
                             new { USER_ID = UserID }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string DeleteTempData()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPackingCompletion>("PackingCompletion_DeleteTemp");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public List<mPackingCompletion> DownloadData(string p_KEY_DOWNLOADS, string p_PACKING_DT_FROM, string p_PACKING_DT_TO,
            string p_TMAP_ORDER_NO, string p_PART_NO, string p_ITEM_NO, string p_CASE_NO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPackingCompletion>("PackingCompletion_Download",
                    new { KEY_DOWNLOADS = p_KEY_DOWNLOADS },
                    new { PACKING_DT_FROM = p_PACKING_DT_FROM },
                    new { PACKING_DT_TO = p_PACKING_DT_TO },
                    new { TMAP_ORDER_NO = p_TMAP_ORDER_NO },
                    new { PART_NO = p_PART_NO },
                    new { ITEM_NO = p_ITEM_NO },
                        new { CASE_NO = p_CASE_NO }
                  );
            db.Close();
            return l.ToList();
        }

        public string InsertTemp(mPackingCompletionUpload data)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPackingCompletion>("PackingCompletion_InsertTemp",
                             new { DATA_ID = data.DATA_ID },
                             new { CASE_NO = data.CASE_NO },
                             new { CASE_TYPE = data.CASE_TYPE },
                             new { PACKING_DT = data.PACKING_DT },
                             new { PART_NO = data.PART_NO },
                             new { QTY = data.QTY },
                             new { TMMIN_ORDER_NO = data.TMMIN_ORDER_NO }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        //add agi 2016-12-13 for insert temp packing revise temp
        public string InsertPackingReviseTemp(mPackingRevise data)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPackingRevise>("PackingRevise_InsertTemp",
                             new { DATA_ID = data.DATA_ID },
                             new { CASE_NO = data.CASE_NO },
                             new { CASE_TYPE = data.CASE_TYPE },
                             new { PACKING_DT = data.PACKING_DT },
                             new { PART_NO = data.PART_NO },
                             new { QTY = data.QTY },
                             new { TMMIN_ORDER_NO = data.TMMIN_ORDER_NO },
                             new { MODE=data.MODE}
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }
            return result;
        }

        public string DeleteTempReviseData()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPackingCompletion>("PackingRevise_DeleteTemp");
                db.Close();
                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public string UploadRevise(string UserID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPackingCompletion>("PackingRevise_Upload",
                             new { USER_ID = UserID }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }
    }
}