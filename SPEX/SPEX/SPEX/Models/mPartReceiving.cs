﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Transactions;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using SPEX.Cls;

namespace SPEX.Models
{
    public class mPartReceiving
    {
        public String KANBAN_ID { get; set; }
        public String PART_NO { get; set; }
        public String DESTINATION { get; set; }
        public String TRANSPORTATION { get; set; }
        public String PRIVILEGE { get; set; }
        public String HANDLING_TYPE { get; set; }
        public String PRIORITY { get; set; }
        public String SHUTTER_NO { get; set; }
        public String KEY_POINT { get; set; }

        public String doPartReceivingProcess(String kanbanID, String UserID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var q = db.SingleOrDefault<String>("PartReceivingProcess", new
            {
                KANBAN_ID = kanbanID,
                USER_ID = UserID
            }).ToString();
            db.Close();
            return q;
        }
    }
}