﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Models
{
    public class Transportation
    {
        public string TRANSPORTATION_CD { get; set; }
        public string TRANSPORTATION_DESC { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }

        public List<Transportation> GetAllTransportation()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<Transportation>("Transportation_GetAll").ToList();
            db.Close();
            return l;
        }
    
    }
}