﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using Telerik.Reporting;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;

namespace SPEX.Models
{
    public class mTelerik
    {
        public string SourcePath { get; set; }
        public string ResultName { get; set; }
        public string ConnString { get; set; }
        public string strprintname { get; set; }
        public ReportParameter Parameters { get; set; }

        public Telerik.Reporting.Report Report;

        public mTelerik(string SourcePath, string ResultName)
        {
            this.SourcePath = SourcePath;
            this.ResultName = ResultName + ".pdf";
            Validate();
        }

        public String SetResultName(String ResultName)
        {
            this.ResultName = ResultName + ".pdf";
            return this.ResultName;
        }

        public void Validate()
        {
            Telerik.Reporting.Report rpt;
            XmlReader xmlReader;
            ReportXmlSerializer xmlSerializer;
            XmlReaderSettings settings = new XmlReaderSettings();

            settings.IgnoreWhitespace = true;
            using (xmlReader = XmlReader.Create(SourcePath, settings))
            {
                xmlSerializer = new ReportXmlSerializer();
                rpt = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
            }

            Report = rpt;
        }

        public void SetConnectionString(string ConnectionString)
        {
            Telerik.Reporting.SqlDataSource sqlDataSource = (Telerik.Reporting.SqlDataSource)Report.DataSource;
            sqlDataSource.ConnectionString = ConnectionString;
            Report.DataSource = sqlDataSource;
        }

        public void SetSubConnectionString(string subPath, string ConnectionString)
        {
            foreach (var items in Report.Items)
            {
                foreach (var item in items.Items)
                {
                    if (item is Telerik.Reporting.SubReport)
                    {
                        var subReport = (Telerik.Reporting.SubReport)item;
                        subReport.ReportSource = UpdateSubReportSource(subReport.ReportSource, subPath, ConnectionString);
                        continue;
                    }
                }
            }
        }

        public void AddParameters(string ParamName, string ParamValue)
        {
            Report.ReportParameters[ParamName].Value = ParamValue;
        }

        public byte[] GeneratePDF()
        {
            Telerik.Reporting.IReportDocument myReport = Report;
            Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
            instanceReportSource.ReportDocument = myReport;
            Telerik.Reporting.Processing.ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);
            return result.DocumentBytes;
        }

        public ReportSource UpdateSubReportSource(ReportSource sourceReportSource, string subPath, string ConnectionString)
        {
            var uriReportSource = (UriReportSource)sourceReportSource;
            uriReportSource.Uri = subPath;
            var reportInstance = DeserializeReport(uriReportSource);
            Telerik.Reporting.SqlDataSource sqlDataSource = (Telerik.Reporting.SqlDataSource)reportInstance.DataSource;
            sqlDataSource.ConnectionString = ConnectionString;
            reportInstance.DataSource = sqlDataSource;
            return CreateInstanceReportSource(reportInstance, uriReportSource);
        }

        public Telerik.Reporting.Report DeserializeReport(UriReportSource uriReportSource)
        {
            var settings = new System.Xml.XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            using (var xmlReader = System.Xml.XmlReader.Create(uriReportSource.Uri, settings))
            {
                var xmlSerializer = new Telerik.Reporting.XmlSerialization.ReportXmlSerializer();
                var report = (Telerik.Reporting.Report)xmlSerializer.Deserialize(xmlReader);
                return report;
            }
        }

        ReportSource CreateInstanceReportSource(IReportDocument report, ReportSource originalReportSource)
        {
            var instanceReportSource = new InstanceReportSource { ReportDocument = report };
            instanceReportSource.Parameters.AddRange(originalReportSource.Parameters);
            return instanceReportSource;
        }

        public void SetSubConnectionString(Dictionary<string, string> paramDict, string ConnectionString)
        {
            foreach (var items in Report.Items)
            {
                foreach (var item in items.Items)
                {
                    if (item is Telerik.Reporting.SubReport)
                    {
                        var subReport = (Telerik.Reporting.SubReport)item;
                        foreach (string key in paramDict.Keys)
                        {
                            if (((Telerik.Reporting.UriReportSource)(subReport.ReportSource)).Uri == key)
                                subReport.ReportSource = UpdateSubReportSource(subReport.ReportSource, paramDict[key], ConnectionString);
                            break;
                        }
                    }
                }
            }
        }

        public byte[] GenerateXLSX()
        {
            Telerik.Reporting.IReportDocument myReport = Report;
            Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
            instanceReportSource.ReportDocument = myReport;
            Telerik.Reporting.Processing.ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("XLSX", instanceReportSource, null);
            return result.DocumentBytes;
        }

        public byte[] GenerateXLS()
        {
            Telerik.Reporting.IReportDocument myReport = Report;
            Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
            instanceReportSource.ReportDocument = myReport;
            Telerik.Reporting.Processing.ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("XLS", instanceReportSource, null);
            return result.DocumentBytes;
        }

        public mTelerik(string SourcePath, string ResultName, bool IsExcel)
        {
            this.SourcePath = SourcePath;
            if (IsExcel)
                this.ResultName = ResultName + ".xls";
            else
                this.ResultName = ResultName + ".pdf";
            Validate();
        }

        public String SetResultName(String ResultName, bool IsExcel)
        {
            if (IsExcel)
                this.ResultName = ResultName + ".xls";
            else
                this.ResultName = ResultName + ".pdf";
            return this.ResultName;
        }

        public void SetDataSource(DataTable dataSource)
        {
            Report.DataSource = dataSource;
        }
    }
}