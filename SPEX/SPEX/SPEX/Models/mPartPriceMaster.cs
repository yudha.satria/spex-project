﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Globalization;

namespace SPEX.Models
{
    public class mPartPriceMaster
    {
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public DateTime? VALID_FROM { get; set; }
        public DateTime? VALID_TO { get; set; }
        public string CURRENCY_CD { get; set; }
        public decimal PRICE { get; set; }
        public decimal RVC { get; set; }
        public string RVC_STRING { get; set; }
        public string DELETE_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
                
        public string TEXT { get; set; }

        public class mPartPriceMasterUpload
        {
            public string PART_NO { get; set; }
            public string PART_NAME { get; set; }
            public string VALID_FROM { get; set; }
            public string VALID_TO { get; set; }
            public string CURRENCY_CD { get; set; }
            public string PRICE { get; set; }
            public string RVC { get; set; }
        }

        public List<mPartPriceMaster> GetList(string pPART_NO, string pVALID_FROM, string pVALID_TO, string pLAST_PRICE)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPartPriceMaster>("PartPriceMaster_GetList",
                  new { PART_NO = pPART_NO },
                  new { VALID_FROM = pVALID_FROM },
                  new { VALID_TO = pVALID_TO },
                  new { LAST_PRICE = pLAST_PRICE }
                  );
            db.Close();

            return l.ToList();
        }

        public string DeleteData(string pPART_NO, DateTime pVALID_FROM, string pUSER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPartPriceMaster>("PartPriceMaster_Delete",
                  new { PART_NO = pPART_NO },
                  new { VALID_FROM = pVALID_FROM },
                  new { USER_ID = pUSER_ID }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string AddData(string pPART_NO, string pPRICE, string pCURRENCY_CD, DateTime pVALID_FROM, DateTime pVALID_TO, string pUSER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPartPriceMaster>("PartPriceMaster_Add",
                  new { PART_NO = pPART_NO },
                  new { PRICE = pPRICE },
                  new { CURRENCY_CD = pCURRENCY_CD },
                  new { VALID_FROM = pVALID_FROM },
                  new { VALID_TO = pVALID_TO },
                  new { USER_ID = pUSER_ID }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string EditData(string pPART_NO, DateTime pVALID_FROM, DateTime pVALID_TO, string pUSER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPartPriceMaster>("PartPriceMaster_Edit",
                  new { PART_NO = pPART_NO },
                   new { VALID_FROM = pVALID_FROM },
                  new { VALID_TO = pVALID_TO },
                  new { USER_ID = pUSER_ID }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string GetNewValidFrom(string pPartNo)
        {
            string result = "error ";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();

                //var l = db.Fetch<mPartPriceMaster>("PartPriceMaster_GetValidFrom",
                DateTime l = db.SingleOrDefault<DateTime>("PartPriceMaster_GetValidFrom",
                    new { 
                            PART_NO = pPartNo 
                        }
                    );
                db.Close();
                result = l.Date.ToString("dd.MM.yyyy");
                //result = Convert.To 
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public string UploadPartPrice(string UserID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPartPriceMaster>("PartPriceMaster_Upload",
                             new { USER_ID = UserID }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string DeleteTempData()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPartPriceMaster>("PartPriceMaster_DeleteTemp");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public string EditRVCData(string pPART_NO, DateTime pVALID_FROM, decimal? pRVC, string pUSER_ID)
        {
            string result = "error ";
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPartPriceMaster>("RVCMaster_Edit",
                  new { PART_NO = pPART_NO },
                  new { VALID_FROM = pVALID_FROM },
                  new { RVC = pRVC },
                  new { USER_ID = pUSER_ID }
                  );
            db.Close();

            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public List<mPartPriceMaster> GetListRVC(string pPART_NO, string pVALID_FROM, string pVALID_TO)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPartPriceMaster>("RVCMaster_GetList",
                  new { PART_NO = pPART_NO },
                  new { VALID_FROM = pVALID_FROM },
                  new { VALID_TO = pVALID_TO }
                  );
            db.Close();

            return l.ToList();
        }

        public string UploadRVC(string UserID)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPartPriceMaster>("RVCMaster_Upload",
                             new { USER_ID = UserID }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string DeleteTempRVCData()
        {
            try
            {
                string result = "SUCCESS";
                IDBContext db = DatabaseManager.Instance.GetContext();
                var l = db.Fetch<mPartPriceMaster>("RVCMaster_DeleteTemp");
                db.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public List<mPartPriceMaster> GetRVCDownloadList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mPartPriceMaster>("RVCMaster_GetDownloadList");
            db.Close();

            return l.ToList();
        }

        public string InsertTempRVC(mPartPriceMasterUpload data)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPartPriceMaster>("RVCMaster_InsertTemp",
                             new { PART_NO = data.PART_NO },
                             new { RVC = data.RVC },
                             new { VALID_FROM = data.VALID_FROM }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string InsertTempPartPrice(mPartPriceMasterUpload data)
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPartPriceMaster>("PartPriceMaster_InsertTemp",
                             new { PART_NO = data.PART_NO },
                             new { PRICE = data.PRICE },
                             new { CURRENCY_CD = data.CURRENCY_CD },
                             new { VALID_FROM = data.VALID_FROM },
                             new { VALID_TO = data.VALID_TO }
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }

        public string BulkInsertTempCSVToTempPartPrice()
        {
            string result = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<mPartPriceMaster>("PartPriceMaster_BulkInsert_Temp"                            
                );
            db.Close();
            foreach (var message in l.ToList())
            {
                result = message.TEXT;
            }

            return result;
        }
    }
}