﻿using System;
using System.Collections.Generic;
using SPEX.Models.Messages;

namespace SPEX.Cls
{
    public class MessagesString
    {
        public string getMsgText(string p_MSG_ID)
        {
            string msgText = "";
            IList<Messages> m = MessagesProvider.Instance.GetById(p_MSG_ID);
            if (m.Count > 0) msgText = m[0].MSG_TEXT;
            return msgText;
        }

        public IList<Messages> getMsgTextList(string p_MSG_ID)
        {
            IList<Messages> m = MessagesProvider.Instance.GetById(p_MSG_ID);
            return m;
        }

        #region INFO
        public string getMSPX00001INF(string p_CompletelySuccesFor)
        {
            string msgText = getMsgText("MSPX00001INF");
            string msgFormated = string.Format(msgText, p_CompletelySuccesFor);
            return msgFormated;
        }

        public string getMSPX00012INF(string p_CompletelyErrorFor)
        {
            string msgText = getMsgText("MSPX00012INF");
            string msgFormated = string.Format(msgText, p_CompletelyErrorFor);
            return msgFormated;
        }
        
        public string getMSPX00002INF(string p_PleaseSelectDataFor)
        {
            string msgText = getMsgText("MSPX00002INF");
            string msgFormated = string.Format(msgText, p_PleaseSelectDataFor);
            return msgFormated;
        }
        //MSPX00005INF	Process upload master {0} is started	INF
        public string getMSPX00005INFForLog(string p_UploadNameProcess)
        {
            string msgText = getMsgText("MSPX00005INF");
            string msgFormated = string.Format("MSPX00005INF|" + "INF|" + msgText, p_UploadNameProcess);
            return msgFormated;
        }
        //MSPX00006INF	Process upload master {0} finish successfully	INF
        public string getMSPX00006INF(string p_UploadNameProcess)
        {
            string msgText = getMsgText("MSPX00006INF");
            string msgFormated = "MSPX00006INF|" + "INF|" + string.Format(msgText, p_UploadNameProcess);
            return msgFormated;
        }

        //MSPX00007INF	Process upload master {0} finish with error	INF
        public string getMSPX00007INF(string p_UploadNameProcess)
        {
            string msgText = getMsgText("MSPX00007INF");
            string msgFormated = "MSPX00007INF|" + "INF|" + string.Format(msgText, p_UploadNameProcess);
            return msgFormated;
        }

        //MSPX00010INF	{0} operation successfully performed	INF
        public string getMSPX00010INF(string p_OperationName)
        {
            string msgText = getMsgText("MSPX00010INF");
            string msgFormated = "MSPX00010INF|" + "INF|" + string.Format(msgText, p_OperationName);
            return msgFormated;
        }

        //MSPX00008INF	{0} is a new data	INF
        public string getMSPX00008INF(string p_FieldAndValue)
        {
            string msgText = getMsgText("MSPX00008INF");
            string msgFormated = "MSPX00008INF|" + "INF|" + string.Format(msgText, p_FieldAndValue);
            return msgFormated;
        }

        //--------INFO for upload






        //-----------------------
        #endregion

        #region ERROR
        //Data not found on {0}
        public string getMSPX00001ERR(string p_NotFoundOn)
        {
            string msgText = getMsgText("MSPX00001ERR");
            string msgFormated = string.Format(msgText, p_NotFoundOn);
            return msgFormated;
        }

        //No data found {0}
        public string getMSPX00002ERR(string p_NoDataFound)
        {
            string msgText = getMsgText("MSPX00002ERR");
            string msgFormated = string.Format(msgText, p_NoDataFound);
            return msgFormated;
        }

        //{0} should not be empty
        public string getMSPX00003ERR(string p_FilterCriteriaName)
        {
            string msgText = getMsgText("MSPX00003ERR");
            string msgFormated = string.Format(msgText, p_FilterCriteriaName);
            return msgFormated;
        }

        //Wrong format for {0}, the correct format should be {1} or {2}
        public string getMSPX00004ERR(string p_WrongFormatFor, string p_ShouldBe1, string p_ShouldBe2)
        {
            string msgText = getMsgText("MSPX00004ERR");
            string msgFormated = string.Format(msgText, p_WrongFormatFor, p_ShouldBe1, p_ShouldBe2);
            return msgFormated;
        }

        //Invalid length for {0}, the correct format should be {1} or {2}
        public string getMSPX00005ERR(string p_InvalidLengthFor, string p_ShouldBe1, string p_ShouldBe2)
        {
            string msgText = getMsgText("MSPX00005ERR");
            string msgFormated = string.Format(msgText, p_InvalidLengthFor, p_ShouldBe1, p_ShouldBe2);
            return msgFormated;
        }

        //Please ticked a checkbox
        public string getMSPX00006ERR()
        {
            string msgText = getMsgText("MSPX00006ERR");
            string msgFormated = msgText;
            return msgFormated;
        }

        //Delete error because of concurrency check in {0}
        public string getMSPX00007ERR(string p_concurrency_check_in)
        {
            string msgText = getMsgText("MSPX00007ERR");
            string msgFormated = string.Format(msgText, p_concurrency_check_in);
            return msgFormated;
        }

        //Can not {0} {1} , because the data is has been used in transaction
        public string getMSPX00008ERR(string p_OnProcess, string p_FieldValue)
        {
            string msgText = getMsgText("MSPX00008ERR");
            string msgFormated = string.Format(msgText, p_OnProcess, p_FieldValue);
            return msgFormated;
        }

        //Can not edit data more than one record
        public string getMSPX00009ERR()
        {
            string msgText = getMsgText("MSPX00009ERR");
            string msgFormated = string.Format(msgText);
            return msgFormated;
        }

        // System parameter {0}, {1}  is not found or blank in System Master
        public string getMSPX00010ERR(string p_SystemParameter1, string p_SystemParameter2)
        {
            string msgText = getMsgText("MSPX00010ERR");
            string msgFormated = string.Format(msgText, p_SystemParameter1, p_SystemParameter2);
            return msgFormated;
        }

        //Disable 13-02-2014, ark.yudha
        //Incomplete data input on process {0} at {1} screen
        //public string getMSPX00011ERR(string p_OnProcess, string p_OnScreen)
        //{
        //    string msgText = getMsgText("MSPX00011ERR");
        //    string msgFormated = string.Format(msgText, p_OnProcess, p_OnScreen);
        //    return msgFormated;
        //}

        //{0}Already exist
        public string getMSPX00015ERR(string p_AlreadyExist)
        {
            string msgText = getMsgText("MSPX00015ERR");
            string msgFormated = string.Format(msgText, p_AlreadyExist);
            return msgFormated;
        }

        //
        public string getMSPX00018ERR()
        {
            string msgText = getMsgText("MSPX00018ERR");
            string msgFormated = string.Format(msgText);
            return msgFormated;
        }

        public string getMSPX00112ERR(string processID)
        {
            string msgText = getMsgText("MSPX00112ERR");
            string msgFormated = string.Format(msgText, processID);
            return msgFormated;
        }

        #endregion


        #region error upload
        //MSPX00019ERR	Invalid file name, valid name is {0}
        public string getMSPX00019ERR(string p_FileName)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00019ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_FileName);
            }
            return msgFormated;
        }
        //MSPX00020ERR	Extracting file {0} is failed
        public string getMSPX00020ERR(string p_FileName)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00020ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_FileName);
            }
            return msgFormated;
        }
        //MSPX00021ERR	Incomplete data for {0}, at row {1}
        public string getMSPX00021ERR(string p_MandatoryFieldName, Int32 p_Row)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00021ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_MandatoryFieldName, p_Row);
            }
            return msgFormated;
        }
        //MSPX00022ERR	Data {0} is exist and rejected, at row {1}
        public string getMSPX00022ERR(string p_FieldNameAndValue, Int32 p_Row)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00022ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_FieldNameAndValue, p_Row);
            }
            return msgFormated;
        }
        //MSPX00023ERR	Valid from date is less than existing data, for {0}, at row {1}
        public string getMSPX00023ERR(string p_FieldNameAndValue, Int32 p_Row)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00023ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_FieldNameAndValue, p_Row);
            }
            return msgFormated;
        }
        //MSPX00024ERR	Invalid format date, for {0}, at row {1}   
        public string getMSPX00024ERR(string p_FieldNameAndValue, Int32 p_Row)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00024ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_FieldNameAndValue, p_Row);
            }
            return msgFormated;
        }

        //MSPX00025ERR	{0} is not exist on master {1},  price for {2} is can not created, at row {3}
        public string getMSPX00024ERR(string p_FieldNameMasterAndValue, string p_MasterName, Int32 p_Row)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00024ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_FieldNameMasterAndValue, p_MasterName, p_Row);
            }
            return msgFormated;
        }


        #endregion

        #region FOR BATCH PROCESS
        /**MSPX00001INB	Starting batch process for {0}**/
        public string getMSPX00001INB(string p_BatchName)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00001INB");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_BatchName);
            }
            return msgFormated;
        }

        /**MSPX00002INB	Batch process for {0} is completed successfully**/
        public string getMSPX00002INB(string p_BatchName)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00002INB");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_BatchName);
            }
            return msgFormated;
        }

        /**MSPX00003INB	Batch process for {0} is has finished with error / warning**/
        public string getMSPX00003INB(string p_BatchName)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00003INB");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_BatchName);
            }
            return msgFormated;
        }

        /**MSPX00004INB	{0} data for number {1} has been created**/
        public string getMSPX00004INB(string p_NameOfData, string p_Number)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00004INB");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_NameOfData, p_Number);
            }
            return msgFormated;
        }

        /**MSPX00001ERB	Part number : {0}  not found**/
        public string getMSPX00001ERB(string p_PartNumber)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00001ERB");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_PartNumber);
            }
            return msgFormated;
        }

        /**MSPX00002ERB	Price for part number : {0} is not found**/
        public string getMSPX00002ERB(string p_PartNumber)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00002ERB");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_PartNumber);
            }
            return msgFormated;
        }

        /**MSPX00003ERB	Price for part number : {0} is not valid**/
        public string getMSPX00003ERB(string p_PartNumber)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00003ERB");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, p_PartNumber);
            }
            return msgFormated;
        }

        public string getMSPX00008INB(string to)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00008INB");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, to);

            }
            return msgFormated;
        }

        public string getMSPX00009INB(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00009INB");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }

        public string getMSPX00010INB(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00010INB");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }

        #endregion

        public string getMSPX00034INF(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00034INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }

        public string getMSPX00013INF(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00013INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }

        //add by ria
        //Please ticked a checkbox
        public string getMSPX00053ERR()
        {
            string msgText = getMsgText("MSPX00053ERR");
            string msgFormated = msgText;
            return msgFormated;
        }

        public string getMSPX00045INF(string processID)
        {
            string msgText = getMsgText("MSPX00045INF");
            string msgFormated = string.Format(msgText, processID);
            return msgFormated;
        }

        public string getMSPX00065INF(string ProcessDesc, string ProcessID )
        {
            string msgText = getMsgText("MSPX00065INF");
            string msgFormated = string.Format(msgText, ProcessDesc, ProcessID);
            return msgFormated;
        }

        public string getMSPX00066INF(string ProcessDesc, string ProcessID)
        {
            string msgText = getMsgText("MSPX00066INF");
            string msgFormated = string.Format(msgText, ProcessDesc, ProcessID);
            return msgFormated;
        }

        public string getMSPX00036INF(string processID)
        {
            string msgText = getMsgText("MSPX00036INF");
            string msgFormated = string.Format(msgText, processID);
            return msgFormated;
        }
        public string getMSPX00037INF(string processID)
        {
            string msgText = getMsgText("MSPX00037INF");
            string msgFormated = string.Format(msgText, processID);
            return msgFormated;
        }
        public string getMSPX00038INF(string processID)
        {
            string msgText = getMsgText("MSPX00038INF");
            string msgFormated = string.Format(msgText, processID);
            return msgFormated;
        }
        public string getMSPX00138ERR(string processID)
        {
            string msgText = getMsgText("MSPX00138ERR");
            string msgFormated = string.Format(msgText, processID);
            return msgFormated;
        }

        public string getMSPX00139ERR(string finishDesc)
        {
            string msgText = getMsgText("MSPX00139ERR");
            string msgFormated = string.Format(msgText, finishDesc);
            return msgFormated;
        }

        public string getMSPX00150ERR(long pID)
        {
            string msgText = getMsgText("MSPX00150ERR");
            string msgFormated = string.Format(msgText, pID);
            
            return msgFormated;
        }

        public string getMSPX00035INF(string ProcessDesc)
        {
            string msgText = getMsgText("MSPX00035INF");
            string msgFormated = string.Format(msgText, ProcessDesc);
            
            return msgFormated;
        }

        public string getMSPX00300INF(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00300INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }

        public string getMSPX00301INF(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00301INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }

        public string getMSPX00302INF(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00302INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }

        public string getMSPX00303INF(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00303INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }

        public string getMSPX00304INF(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00304INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }

        public string getMSPX00305INF(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPX00305INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }


        public string getMSPXCG01INF(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXCG01INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }

        public string getMSPXCG02INF(string pID)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXCG02INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }


        //[A-20170825001 By FID.Arri] Added new messages
        #region Messages have been added by FID (Added by FID.Arri)
        #region Messages Error
        public string getMSPXF0001ERR(string pKanbanId)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0001ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pKanbanId);
            }
            return msgFormated;
        }

        public string getMSPXF0002ERR(string pKanbanId)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0002ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pKanbanId);
            }
            return msgFormated;
        }

        public string getMSPXF0003ERR(string pKanbanId)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0003ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pKanbanId);
            }
            return msgFormated;
        }

        public string getMSPXF0004ERR(string pManifestNo, string pKanbanId)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0004ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pManifestNo, pKanbanId);
            }
            return msgFormated;
        }

        public string getMSPXF0005ERR(string pManifestNo)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0005ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pManifestNo);
            }
            return msgFormated;
        }

        public string getMSPXF0006ERR(string pKanbanId)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0006ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pKanbanId);
            }
            return msgFormated;
        }

        public string getMSPXF0007ERR(string pKanbanId)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0007ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pKanbanId);
            }
            return msgFormated;
        }

        public string getMSPXF0008ERR(string pKanbanId)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0008ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pKanbanId);
            }
            return msgFormated;
        }

        public string getMSPXF0009ERR(string pKanbanId)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0009ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pKanbanId);
            }
            return msgFormated;
        }

        public string getMSPXF0010ERR(string pKanbanId)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0010ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pKanbanId);
            }
            return msgFormated;
        }

        public string getMSPXF0011ERR(string pKanbanId)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0011ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pKanbanId);
            }
            return msgFormated;
        }

        public string getMSPXF0012ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0012ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0013ERR(string pFieldName, string pRow)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0013ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pFieldName, pRow);
            }
            return msgFormated;
        }

        public string getMSPXF0014ERR(string pFieldName, string pRow, string pLength)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0014ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pFieldName, pRow, pLength);
            }
            return msgFormated;
        }

        public string getMSPXF0015ERR(string pSuppCode, string pSuppPlant, string pRow)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0015ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pSuppCode, pSuppPlant, pRow);
            }
            return msgFormated;
        }

        public string getMSPXF0016ERR(string pRTEGRPCD, string pRUNSEQ, string pRTEDATE, string pLOGPTCD
            , string pPLANTCD, string pDOCKCD, string pRow)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0016ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pRTEGRPCD, pRUNSEQ
                    , pRTEDATE, pLOGPTCD, pPLANTCD, pDOCKCD, pRow);
            }
            return msgFormated;
        }

        public string getMSPXF0017ERR(string pField, string pRow, string pVldFormat)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0017ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pField, pRow, pVldFormat);
            }
            return msgFormated;
        }

        public string getMSPXF0018ERR(string pRTEGRPCD, string pRUNSEQ, string pRTEDATE, string pPLANTCD, string pRow)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0018ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pRTEGRPCD, pRUNSEQ
                    , pRTEDATE, pPLANTCD, pRow);
            }
            return msgFormated;
        }

        public string getMSPXF0019ERR(string pRow)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0019ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pRow);
            }
            return msgFormated;
        }

        public string getMSPXF0020ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0020ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0021ERR(string pRTEGRPCD, string pRUNSEQ, string pRTEDATE, string pRCVCOMPDOCKCD
            , string pSUPPCD, string pSUPPPLANTCD, string pSUPPDOCKCD, string pORDDATE, string pORDSEQ, string pRow)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0021ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pRTEGRPCD, pRUNSEQ
                    , pRTEDATE, pRCVCOMPDOCKCD, pSUPPCD, pSUPPPLANTCD, pSUPPDOCKCD, pORDDATE, pORDSEQ, pRow);
            }
            return msgFormated;
        }

        public string getMSPXF0022ERR(string pContainerNo)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0022ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pContainerNo);
            }
            return msgFormated;
        }

        public string getMSPXF0023ERR(string pCaseNo)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0023ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pCaseNo);
            }
            return msgFormated;
        }

        public string getMSPXF0024ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0024ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0025ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0025ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0026ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0026ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0027ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0027ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0028ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0028ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0029ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0029ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0030ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0030ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0031ERR(string pErr)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0031ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pErr);
            }
            return msgFormated;
        }

        public string getMSPXF0032ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0032ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0033ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0033ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0034ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0034ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0035ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0035ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0036ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0036ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0037ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0037ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0038ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0038ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0039ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0039ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0040ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0040ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0041ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0041ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0042ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0042ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0043ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0043ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0044ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0044ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0073ERR()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0073ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0999ERR(string pErr)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0999ERR");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pErr);
            }
            return msgFormated;
        }
        #endregion

        #region Messages Information
        public string getMSPXF0001INF(string pKanbanId, string pManifestNo)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0001INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pKanbanId, pManifestNo);
            }
            return msgFormated;
        }

        public string getMSPXF0002INF(string pKanbanId)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0002INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pKanbanId);
            }
            return msgFormated;
        }

        public string getMSPXF0003INF(string pProcessName)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0003INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pProcessName);
            }
            return msgFormated;
        }

        public string getMSPXF0004INF(string pCountUplData, string pCountSuccessUplDat, string pCountFailUplDat)
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0004INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT
                    , pCountUplData, pCountSuccessUplDat, pCountFailUplDat);
            }
            return msgFormated;
        }

        public string getMSPXF0005INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0005INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0006INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0006INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0007INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0007INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0008INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0008INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0009INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0009INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0010INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0010INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0011INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0011INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0012INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0012INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0013INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0013INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0014INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0014INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0015INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0015INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0016INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0016INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }

        public string getMSPXF0017INF()
        {
            IList<Messages> msgTList = getMsgTextList("MSPXF0017INF");
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT);
            }
            return msgFormated;
        }
        #endregion
        #endregion
        //[A-20170825001 By FID.Arri] End

        public string getMSG_Spex(string Msg_id, long pID)
        {
            IList<Messages> msgTList = getMsgTextList(Msg_id);
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, pID);
            }
            return msgFormated;
        }

        //add agi 2017-09-05
        public string getMSG_SpexHref(string Msg_id, string HrefPID)
        {
            IList<Messages> msgTList = getMsgTextList(Msg_id);
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + string.Format(msgTList[0].MSG_TEXT, HrefPID);
            }
            return msgFormated;
        }

        //add agi 2017-10-11
        public string getMSG_For_Log(string Msg_id)
        {
            IList<Messages> msgTList = getMsgTextList(Msg_id);
            string msgFormated = "";
            if (msgTList.Count > 0)
            {
                msgFormated = msgTList[0].MSG_ID + "|" + msgTList[0].MSG_TYPE + "|" + msgTList[0].MSG_TEXT;
            }
            return msgFormated;
        }
    }
}