﻿using System;
using System.Linq;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace SPEX.Cls
{
    public class Lock
    {
        public long PROCESS_ID { get; set; }
        public string FUNCTION_ID { get; set; }
        public string LOCK_REF { get; set; }
        public string REMARKS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }

        public string TEXT { get; set; }

        public string LockFunction(Lock L)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            //Lock list = new Lock();
            //list.FUNCTION_ID = pFUNCTION_ID;
            //list.PROCESS_ID = pPROCESS_ID;
            //list.LOCK_REF = pLOCK_REF;
            //list.CREATED_BY = "SYSTEM";
            //list.CREATED_DT = DateTime.Today;

            db.Execute("LockProcess",
                new { PROCESS_ID = L.PROCESS_ID },
                new { FUNCTION_ID = L.FUNCTION_ID },
                new { LOCK_REF = L.LOCK_REF },
                new { CREATED_BY = L.CREATED_BY },
                new { CREATED_DT = L.CREATED_DT }
                );

            return "";
        }

        public string UnlockFunction(string pFUNCTION_ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            //Lock list = new Lock();
            //list.FUNCTION_ID = pFUNCTION_ID;

            db.Execute("UnlockProcess", new { FUNCTION_ID = pFUNCTION_ID });

            return "";
        }

        public int is_lock(string function_id)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            int result = db.SingleOrDefault<int>("CommonIsLock", new { FUNCTION_ID = function_id });
            return result;
        }

        public int is_lock(string function_id, out long ProcessID)
        {
            ProcessID = 0;
            string text = "";
            IDBContext db = DatabaseManager.Instance.GetContext();
            var l = db.Fetch<Lock>("CommonIsLockWithProcessIDOut",
                              new { FUNCTION_ID = function_id }
                 );
            db.Close();
            foreach (var message in l.ToList())
            {
                ProcessID = message.PROCESS_ID;
                text = message.TEXT;
            }

            return text == "NOT EXIST" ? 0 : 1;
        }

        //public string is_lock(string function_id)
        //{
        //    IDBContext db = DatabaseManager.Instance.GetContext();
        //    int result = db.SingleOrDefault<int>("CommonIsLock", new { FUNCTION_ID = function_id });
        //    string msg = string.Empty;
        //    //write log error if locked
        //    if (result == 1)
        //    {
        //        string msgID = "MPIP00029ERR";
        //        //Message message = db.SingleOrDefault<Message>("MessageGetByID", new { MSG_ID = msgID });
        //        //db.Execute("LogDetailWrite", new { PROCESS_ID = processID }, new { ERR_LOC = "Concurency and Dependency Checking" }, new { MSG_ID = msgID }, new { ERR_MSG = message.MSG_TEXT });

        //    }

        //    return result;
        //}
    }

}