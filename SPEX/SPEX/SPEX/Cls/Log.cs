﻿using SPEX.Models.Log;

namespace SPEX.Cls
{
    public class Log
    {
        public long createLog(string p_messages, string p_user, string p_location, 
            long p_pid,string p_module_id,string p_function_id)
        {
            long Process_ID = 0;
            //format p_messages yang diterima adalah msg_id|msg_type|messages
            string[] aMessages = p_messages.Split('|');

            string p_msg_id=aMessages[0];
            string p_msg_type = aMessages[1];

            Process_ID=LogProvider.Instance.putLog(aMessages[2], p_user, p_location, p_pid, p_msg_id,
                p_msg_type,p_module_id,p_function_id);
            return Process_ID;
        }

        public long createLogOnProgress(string p_messages, string p_user, string p_location,
            long p_pid, string p_module_id, string p_function_id)
        {
            long Process_ID = 0;
            //format p_messages yang diterima adalah msg_id|msg_type|messages
            string[] bMessages = p_messages.Split('|');

            string p_msg_id = bMessages[0];
            string p_msg_type = bMessages[1];

            Process_ID = LogProvider.Instance.putLog(bMessages[2], p_user, p_location, p_pid, p_msg_id,
                p_msg_type, p_module_id, p_function_id,4);
            return Process_ID;
        }

        public long createLogOnError(string p_messages, string p_user, string p_location,
            long p_pid, string p_module_id, string p_function_id)
        {
            long Process_ID = 0;
            //format p_messages yang diterima adalah msg_id|msg_type|messages
            string[] aMessages = p_messages.Split('|');

            string p_msg_id = aMessages[0];
            string p_msg_type = aMessages[1];

            Process_ID = LogProvider.Instance.putLog(aMessages[2], p_user, p_location, p_pid, p_msg_id,p_msg_type, p_module_id, p_function_id,1);
            return Process_ID;
        }
    }
}