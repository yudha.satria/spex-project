using System.Collections.Generic;
using System.Linq;
using SPEX.Models;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
//using DevExpress.Xpo;

namespace SPEX.Cls
{

    public class Common
    {
        public List<mBuyerPD> GetAllBuyerPDOnly()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mBuyerPD> l = db.Fetch<mBuyerPD>("BuyerPD_Search_BuyerPD").ToList();
            db.Close();
            return l;
        }

        public List<BUYER> GetAllBuyerByBuyerPD()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<BUYER> l = db.Fetch<BUYER>("Buyyer_Search_BuyerPD").ToList();
            db.Close();
            return l;
        }

        public List<mPDCode> GetAllPDByBuyerPD(string BUYER_CD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            List<mPDCode> l = db.Fetch<mPDCode>("PD_Search_BuyerPD", new { BUYER_CD = BUYER_CD }).ToList();
            db.Close();

            return l;
        }

        public List<string> GetPackingCompany()
        {
            //string url = string.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("PackingCompanySearch").ToList();
            db.Close();
            return q;
        }

        public static List<mSystemMaster> GetPackingCompanyList()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();

            datas = (new mSystemMaster()).GetSystemValueList("General", "PACKING_COMPANY", "1");
            return datas;
        }

        public List<PORT_LOADING> GetPortLoadingByAir()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<PORT_LOADING> q = db.Fetch<PORT_LOADING>("Get_Port_loading_cd_by_air").ToList();
            db.Close();
            return q;
        }

        public List<PORT_LOADING> GetPortLoadingBySea()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<PORT_LOADING> q = db.Fetch<PORT_LOADING>("Get_Port_loading_cd_by_Sea").ToList();
            db.Close();
            return q;
        }



        public List<PORT_DISCHARGE> Get_PORT_DISCHARGE_By_Air()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<PORT_DISCHARGE> q = db.Fetch<PORT_DISCHARGE>("Get_Port_Discharge_by_air").ToList();
            db.Close();
            return q;
        }

        public List<PORT_DISCHARGE> Get_PORT_DISCHARGE_By_Sea()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            List<PORT_DISCHARGE> q = db.Fetch<PORT_DISCHARGE>("Get_Port_Discharge_by_Sea").ToList();
            db.Close();
            return q;
        }

        public static string GetDataMasterSystem(string SYSTEM_TYPE, string SYSTEM_CD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string q = db.SingleOrDefault<string>("GetValueSytemMaster",
                                                   new { SYSTEM_TYPE = SYSTEM_TYPE },
                                                   new { SYSTEM_CD = SYSTEM_CD }
                                                   );
            db.Close();
            return q;
        }

        public static int GetDataMasterSystemInt(string SYSTEM_TYPE, string SYSTEM_CD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            string q = db.SingleOrDefault<string>("GetValueSytemMaster",
                                                   new { SYSTEM_TYPE = SYSTEM_TYPE },
                                                   new { SYSTEM_CD = SYSTEM_CD }
                                                   );
            db.Close();
            return int.Parse(q);
        }

        public List<mInvoice> GetStatusList()
        {
            List<mInvoice> ListInvoiceStatus = new List<mInvoice>();
            IDBContext db = DatabaseManager.Instance.GetContext();
            ListInvoiceStatus = db.Fetch<mInvoice>("GetAllInvoiceStatus").ToList();
            db.Close();
            return ListInvoiceStatus;
        }

        public List<mSystemMaster> GetValidationStatusList()
        {
            List<mSystemMaster> datas = (new mSystemMaster()).GetSystemValueList("GENERAL", "VALIDATION_CD_", "2");
            return datas;
        }

        public List<mSystemMaster> GetTMMINPackingList()
        {
            List<mSystemMaster> datas = (new mSystemMaster()).GetSystemValueList("GENERAL", "TMMIN_PACKING_", "2");
            return datas;
        }

        public static List<mSystemMaster> GetPartTypeList()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();

            datas = (new mSystemMaster()).GetSystemValueList("SPX_POSS_DEF_VALUE", "POSS_TGP_FLAG", "1");
            return datas;
        }

        public static List<mSystemMaster> GetDangerFlagList()
        {
            List<mSystemMaster> datas = new List<mSystemMaster>();

            datas = (new mSystemMaster()).GetSystemValueList("GENERAL", "DANGER_FLAG", "1");
            return datas;
        }

        public static string GetNewDateFormatFromExcel(string prevFormat)
        {
            string retFormat = "";
            switch (prevFormat)
            {
                case "d/m/yy":
                    retFormat = "dd/MM/yyyy";
                    break;
                case "m/d/yy":
                    retFormat = "MM/dd/yyyy";
                    break;
                case "d/m/y":
                    retFormat = "dd/MM/yy";
                    break;
                case "m/d/y":
                    retFormat = "MM/dd/yy";
                    break;
                case "yyyymmdd":
                    retFormat = "yyyyMMdd";
                    break;
                case "yyyymmdd\\ hhmm":
                    retFormat = "yyyyMMdd HHmm";
                    break;
                default:
                    retFormat = prevFormat;
                    break;
            }

            return retFormat;
        }

        public static List<string> GetSupplierCDPlantList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("GetSupplierCDPlantList").ToList();
            db.Close();
            return q;
        }

        public static List<string> GetSubSupplierCDPlantList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var q = db.Fetch<string>("GetSubSupplierCDPlantList").ToList();
            db.Close();
            return q;
        }

        public static List<string> GetZoneList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var ZONE_CD = db.Fetch<string>("GetComboBoxZone").ToList();
            db.Close();
            return ZONE_CD;
        }
         

        public static string GetTelerikConnectionString()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<mShippingInstruction>("GetTelerikConnection");
            db.Close();

            return l.Count > 0 ? l.ToList().FirstOrDefault().Text : "";
        }

        //add agi 2017-06-09
        public  List<string> GetSystemMasterToList(string SYSTEM_TYPE,string SYSTEM_CODE,string DELIMITER)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            var l = db.Fetch<string>("SystemMaster_GetListValue", new { SYSTEM_TYPE = SYSTEM_TYPE }, new { SYSTEM_CODE = SYSTEM_CODE }, new { DELIMITER = DELIMITER }).ToList();
            db.Close();

            return l;
        }
    }

}