﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Json;
using System.IO;
using SpexMobile.AppCode;

namespace SpexMobile
{
    public partial class Login : Form
    {
        DatabaseHelper db = new DatabaseHelper();
        public bool SuccessLogin = false;
        public Login()
        {
            InitializeComponent();
        }

        public String TextBoxValue // retrieving a value from
        {
            get
            {
                return this.username_txt.Text;
            }
        }


        public void CenterForm( Form theForm)
        {
            theForm.Location = new Point(
                Screen.PrimaryScreen.WorkingArea.Width / 2 - theForm.Width / 2,
                Screen.PrimaryScreen.WorkingArea.Height / 2 - theForm.Height / 2);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {

            try
            {
                var ip = (from a in Dns.GetHostEntry(Dns.GetHostName()).AddressList
                          where a.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork
                          select a).First().ToString();

                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", username_txt.Text));
                param.Add(new DbParam("@USER_PWD", "Text", passwordtxt.Text));
                param.Add(new DbParam("@IP", "Text", ip));
                param.Add(new DbParam("@VALIDASI", "Text", "0"));
                DataTable o = db.GetDataTable("[spex].SP_DEVICE_USER_GET", param);

                if (o.Rows.Count == 0)
                {
                    MessageBox.Show("User Login Tidak Ditemukan");
                }
                else
                {
                    if (o.Rows[0]["RESPONSE"].ToString().Trim() == "SUCCESS")
                    {
                        if (o.Rows[0]["POSTION_CD"].ToString() == "LH" || o.Rows[0]["POSTION_CD"].ToString() == "SH")
                        {
                            SuccessLogin = true;
                            this.DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("Login tidak berhak mengubah mode");
                        }

                    }
                    else
                    {
                        MessageBox.Show(o.Rows[0]["RESPONSE"].ToString().Trim().Replace("Error|", ""));
                    }
                }

                
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error:" + exc.Message);
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}