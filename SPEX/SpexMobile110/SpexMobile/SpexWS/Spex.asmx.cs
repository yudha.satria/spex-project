﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using SpexMobile.AppCode;
using System.Data;

namespace SpexWS
{
    /// <summary>
    /// Summary description for Spex
    /// </summary>
    [WebService(Namespace = "http://spex.qas.toyota.co.id/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Spex : System.Web.Services.WebService
    {
        [WebMethod]
        public string CheckConnection()
        {
            return "ok";
        }
        
        [WebMethod]
        public DataSet RetrieveDataPrc(String procName, string[] refkey,string[] refval)
        {
            DatabaseHelper db = new DatabaseHelper();
            List<DbParam> param = new List<DbParam>();
            int i = 0;
            DbParam par;
            foreach (string s in refkey)
            {
                par = new DbParam(s, "String", refval[i]);
                param.Add(par);
                i++;
            }
            return db.GetDataSet(procName,param);   
        }

        [WebMethod]
        public DataSet RetrieveDataStr(String sql)
        {
            DatabaseHelper db = new DatabaseHelper();
            return db.GetDataSet(sql);
        }

        [WebMethod]
        public void ProcessDataPrc(String procName, string[] refkey, string[] refval)
        {
            DatabaseHelper db = new DatabaseHelper();
            List<DbParam> param = new List<DbParam>();
            int i = 0;
            DbParam par;
            foreach (string s in refkey)
            {
                par = new DbParam(s, "String", refval[i]);
                param.Add(par);
                i++;
            }
            db.ExecuteProcedure(procName, param);
        }

        [WebMethod]
        public void ProcessDataStr(String sql)
        {
            DatabaseHelper db = new DatabaseHelper();
            db.ExecuteSql(sql);
        }
    }
}
