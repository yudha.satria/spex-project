﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SpexMobile.AppCode
{
    class DbParam
    {
        public string Name { get; set; }
        public string DataType { get; set; }
        public string Val { get; set; }
        public DbParam(String pName, String pDataType, String pVal)
        {
            this.Name = pName;
            this.DataType = pDataType;
            this.Val = pVal;
        }
    }
}
