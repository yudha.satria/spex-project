﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Configuration;

namespace SpexMobile.AppCode
{
    class DatabaseHelper
    {
        //string constr = "Data Source=DB1-DEV-SQL2016.toyota.co.id\\DB1DEVSQL2016;Initial Catalog=SPEX_DB;UID=SP3X_DB_DEV;Password=Sp3X_d8_dev;Persist Security Info=false;";
        //string constr = "Data Source=localhost;Initial Catalog=SPEX_DB_DEV;UID=sa;Password=sa;Persist Security Info=false;";
        //string constr = "Data Source=10.165.8.69;Initial Catalog=SPEX_DB_DEV;UID=dms;Password=dms;Persist Security Info=false;";
        //string constr = "Data Source=localhost;Initial Catalog=SPEX_DB_DEV;UID=sa;Password=sa;Persist Security Info=false;";

        string constr = ConfigurationManager.ConnectionStrings["Default"].ToString();
        public bool using_thread = false;
        public SqlConnection open()
        {
            return new SqlConnection(constr);
        }

        public void close(SqlConnection con)
        {
            con.Close();
        }

        public DataSet GetDataSet(String procName, List<DbParam> p)
        {
            SqlConnection con = open();
            SqlCommand cmd = new SqlCommand(procName, con);
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (DbParam par in p)
            {
                cmd.Parameters.AddWithValue(par.Name, par.Val);
            }
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataSet GetDataSet(String Query)
        {
            SqlConnection con = open();
            SqlCommand cmd = new SqlCommand(Query, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public void ExecuteProcedure(String procName, List<DbParam> p)
        {
            SqlConnection con = open();
            SqlCommand cmd = new SqlCommand(procName, con);
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (DbParam par in p)
            {
                cmd.Parameters.AddWithValue(par.Name, par.Val);
            }
            con.Open();
            cmd.CommandTimeout = 0;
            cmd.ExecuteScalar();
            con.Close();
        }

        public void ExecuteSql(String sql)
        {
            SqlConnection con = open();
            SqlCommand cmd = new SqlCommand(sql,con);
            cmd.CommandType = CommandType.Text;
            con.Open();
            cmd.CommandTimeout = 0;
            cmd.ExecuteScalar();
            con.Close();
        }

        public bool IsConnectNetwork()
        {
            return true;
        } 

        public bool IsConnect()
        {
            bool ConnNetWork = IsConnectNetwork();
            if (ConnNetWork)
            {
                SqlConnection con = open();
                try
                {

                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    con.Open();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
