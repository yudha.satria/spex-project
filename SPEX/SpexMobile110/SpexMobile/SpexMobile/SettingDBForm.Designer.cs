﻿namespace SpexMobile
{
    partial class SettingDBForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.serverNameTxt = new System.Windows.Forms.TextBox();
            this.catalogTxt = new System.Windows.Forms.TextBox();
            this.usernameTxt = new System.Windows.Forms.TextBox();
            this.passwordTxt = new System.Windows.Forms.TextBox();
            this.btnCheck = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpDb = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.tbTimeOut = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tpWs = new System.Windows.Forms.TabPage();
            this.urlWsTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tpDb.SuspendLayout();
            this.tpWs.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 20);
            this.label1.Text = "Server Name";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.Text = "Catalog";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 20);
            this.label3.Text = "Username";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 20);
            this.label4.Text = "Password";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(7, 237);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(72, 20);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // serverNameTxt
            // 
            this.serverNameTxt.Location = new System.Drawing.Point(93, 13);
            this.serverNameTxt.Name = "serverNameTxt";
            this.serverNameTxt.Size = new System.Drawing.Size(100, 21);
            this.serverNameTxt.TabIndex = 5;
            // 
            // catalogTxt
            // 
            this.catalogTxt.Location = new System.Drawing.Point(93, 46);
            this.catalogTxt.Name = "catalogTxt";
            this.catalogTxt.Size = new System.Drawing.Size(100, 21);
            this.catalogTxt.TabIndex = 6;
            // 
            // usernameTxt
            // 
            this.usernameTxt.Location = new System.Drawing.Point(93, 80);
            this.usernameTxt.Name = "usernameTxt";
            this.usernameTxt.Size = new System.Drawing.Size(100, 21);
            this.usernameTxt.TabIndex = 7;
            // 
            // passwordTxt
            // 
            this.passwordTxt.Location = new System.Drawing.Point(93, 115);
            this.passwordTxt.Name = "passwordTxt";
            this.passwordTxt.PasswordChar = '*';
            this.passwordTxt.Size = new System.Drawing.Size(100, 21);
            this.passwordTxt.TabIndex = 8;
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(83, 236);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(72, 20);
            this.btnCheck.TabIndex = 9;
            this.btnCheck.Text = "Check";
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // btnHome
            // 
            this.btnHome.Location = new System.Drawing.Point(161, 236);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(72, 20);
            this.btnHome.TabIndex = 10;
            this.btnHome.Text = "Home";
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpDb);
            this.tabControl1.Controls.Add(this.tpWs);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.tabControl1.Location = new System.Drawing.Point(7, 16);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(226, 214);
            this.tabControl1.TabIndex = 15;
            // 
            // tpDb
            // 
            this.tpDb.Controls.Add(this.label7);
            this.tpDb.Controls.Add(this.tbTimeOut);
            this.tpDb.Controls.Add(this.label6);
            this.tpDb.Controls.Add(this.label3);
            this.tpDb.Controls.Add(this.label1);
            this.tpDb.Controls.Add(this.serverNameTxt);
            this.tpDb.Controls.Add(this.passwordTxt);
            this.tpDb.Controls.Add(this.label2);
            this.tpDb.Controls.Add(this.usernameTxt);
            this.tpDb.Controls.Add(this.label4);
            this.tpDb.Controls.Add(this.catalogTxt);
            this.tpDb.Location = new System.Drawing.Point(0, 0);
            this.tpDb.Name = "tpDb";
            this.tpDb.Size = new System.Drawing.Size(226, 191);
            this.tpDb.Text = "Database";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(163, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 20);
            this.label7.Text = "S";
            // 
            // tbTimeOut
            // 
            this.tbTimeOut.Location = new System.Drawing.Point(93, 152);
            this.tbTimeOut.Name = "tbTimeOut";
            this.tbTimeOut.Size = new System.Drawing.Size(64, 21);
            this.tbTimeOut.TabIndex = 13;
            this.tbTimeOut.TextChanged += new System.EventHandler(this.tbTimeOut_TextChanged);
            this.tbTimeOut.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTimeOut_KeyPress);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(3, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 20);
            this.label6.Text = "Timeout DB";
            // 
            // tpWs
            // 
            this.tpWs.Controls.Add(this.urlWsTxt);
            this.tpWs.Controls.Add(this.label5);
            this.tpWs.Location = new System.Drawing.Point(0, 0);
            this.tpWs.Name = "tpWs";
            this.tpWs.Size = new System.Drawing.Size(218, 188);
            this.tpWs.Text = "Web Service";
            // 
            // urlWsTxt
            // 
            this.urlWsTxt.Location = new System.Drawing.Point(76, 13);
            this.urlWsTxt.Name = "urlWsTxt";
            this.urlWsTxt.Size = new System.Drawing.Size(143, 21);
            this.urlWsTxt.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 20);
            this.label5.Text = "URL";
            // 
            // SettingDBForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnHome);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.btnSave);
            this.Name = "SettingDBForm";
            this.Text = "Connection";
            this.Load += new System.EventHandler(this.SettingDBForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tpDb.ResumeLayout(false);
            this.tpWs.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox serverNameTxt;
        private System.Windows.Forms.TextBox catalogTxt;
        private System.Windows.Forms.TextBox usernameTxt;
        private System.Windows.Forms.TextBox passwordTxt;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpDb;
        private System.Windows.Forms.TabPage tpWs;
        private System.Windows.Forms.TextBox urlWsTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbTimeOut;
    }
}