﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;
using System.Threading;

namespace SpexMobile
{
    public partial class GridVanning : Form
    {
        bool pageActive = true;
        public string mode = "";
        public static int page = 1;
        public static int last_page = 1;

        DatabaseHelper db = new DatabaseHelper();
        public GridVanning()
        {
            InitializeComponent();
            initData();
            if (db.using_thread)
            {
                new Thread(CheckConnectionThread).Start();
            }
        }

        public void ChkConPart(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChkConPart), new object[] { value });
                return;
            }
            if (pageActive && (value == "OFFLINE"))
            {
                pageActive = false;
                OffLineInfoForm ph = new OffLineInfoForm();
                this.Hide();
                ph.ShowDialog();
                this.Show();
                pageActive = true;
            }
        }

        void CheckConnectionThread()
        {
            while (true)
            {
                string v = "OFFLINE";
                if (db.IsConnect())
                {
                    v = "ONLINE";
                }
                ChkConPart(v);
                Thread.Sleep(2000);
            }
        }

        public GridVanning(string _mode)
        {
            InitializeComponent();
            mode = _mode;
            initData();
            if (db.using_thread)
            {
                new Thread(CheckConnectionThread).Start();
            }
        }

        private void initData()
        {
            page = 1;
            last_page = 1;
            search_data(page);
            if (mode == "REMAIN")
            {
                titleLbl.Text = "Remaining Case";
            }
            else
            {
                titleLbl.Text = mode + " Case";
            }


            loginTxt.Text = DeviceUser.USER_LOGIN;
            TransportCdTxt.Text = DeviceUser.TRANSPORT_CD_DESC;
            modeLbl.Text = "";//request elmas mode.Substring(0, 4);
        }

        private void homeBtn_Click(object sender, EventArgs e)
        {
            pageActive = false;
            this.DialogResult = DialogResult.OK;
        }

        private void dataGrid1_Click(object sender, EventArgs e)
        {
            try
            {
                
                int row = dataGrid1.CurrentCell.RowNumber;

                if (row >= 0)
                {
                    string vanninglabel = dataGrid1[row, 1].ToString(); //vanning label
                    string vanningstatusdt = dataGrid1[row, 4].ToString(); //vanning status dt

                    if (vanninglabel.Length > 0)
                    {
                        List<DbParam> param = new List<DbParam>();
                        param.Add(new DbParam("@VANNING_LABEL", "Text", vanninglabel));
                        param.Add(new DbParam("@PACKING_COMPANY", "Text", DeviceUser.PACKING_COMPANY));
                        param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                        param.Add(new DbParam("@MODE", "Text", mode));
                        param.Add(new DbParam("@STATUS_DT", "Text", vanningstatusdt));

                        DataTable o = db.GetDataTable("[spex].SP_DEVICE_VANNING_GET_DATA", param);
                        string status = "";
                        bool isRemain = false;
                        if (o.Rows[0]["TEXT"].ToString() == "Success")
                        {

                            if (mode == "REOPEN")
                            {
                                status = "REOP";
                            }
                            else if (mode == "REMAIN")
                            {
                                isRemain = true;
                                status = dataGrid1[row, 3].ToString();
                            }

                            string Destination = o.Rows[0]["DESTINATION"].ToString();
                            string SEALNO = o.Rows[0]["SEALNO"].ToString();
                            string TARE = o.Rows[0]["TARE"].ToString();
                            string CONTAINERNO = o.Rows[0]["CONTAINERNO"].ToString();
                            string QTY = o.Rows[0]["QTY"].ToString();

                            if (isRemain)
                            {
                                DeviceUser.DEVICE_MODE = o.Rows[0]["DEVICE_MODE"].ToString();
                            }

                            VanningProcessForm ph = new VanningProcessForm(vanninglabel, Destination, SEALNO, TARE, CONTAINERNO, QTY,status,isRemain);
                            pageActive = false;
                            this.Hide();
                            ph.ShowDialog();
                            this.Show();
                            //pageActive = true;
                            this.DialogResult = DialogResult.OK;

                        }
                        else
                        {
                            MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                        }
                    }
                    
                    
                }

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            search_data(page);
        }

        private void search_data(int pageX)
        {
            try
            {
                if (db.IsConnect())
                {
                    List<DbParam> param = new List<DbParam>();
                    param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                    param.Add(new DbParam("@MODE", "Text", mode));
                    param.Add(new DbParam("@PAGEVIEW", "Int", pageX.ToString()));

                    DataSet ds = db.GetDataSet("[spex].SP_DEVICE_GET_GRID_VANNING", param);

                    DataTable o = ds.Tables[0];
                    dataGrid1.TableStyles.Clear();
                    DataGridTableStyle tableStyle = new DataGridTableStyle();
                    tableStyle.MappingName = o.TableName;
                    DataColumnCollection myDataColumns = o.Columns;
                    foreach (DataColumn dataColumn in myDataColumns)
                    {
                        dataColumn.ReadOnly = true;
                        DataGridTextBoxColumn tbcName = new DataGridTextBoxColumn();
                        if (dataColumn.ColumnName.Length > 10)
                        {
                            tbcName.Width = 140;
                        }
                        else
                        {
                            tbcName.Width = 80;
                        }
                        tbcName.MappingName = dataColumn.ColumnName;
                        tbcName.HeaderText = dataColumn.ColumnName;
                        tableStyle.GridColumnStyles.Add(tbcName);
                    }
                    dataGrid1.TableStyles.Add(tableStyle);

                    if (o.Rows.Count > 0)
                    {
                        dataGrid1.DataSource = o;
                        dataGrid1.Visible = true;
                        infolbl.Visible = false;
                        label1.Visible = true;
                        pageLbl.Visible = true;
                        firstLbl.Visible = true;
                        NextBtn.Visible = true;
                        prevBtn.Visible = true;
                        LastBtn.Visible = true;


                        DataTable h = ds.Tables[1];
                        last_page = Int32.Parse(h.Rows[0]["JPAGE"].ToString());
                        pageLbl.Text = page.ToString() + "/" + last_page.ToString();

                        prevBtn.Enabled = !(page == 1);
                        NextBtn.Enabled = page != last_page;
                    }
                    else
                    {
                        dataGrid1.Visible = false;
                        infolbl.Visible = true;
                        label1.Visible = false;
                        pageLbl.Visible = false;
                        firstLbl.Visible = false;
                        NextBtn.Visible = false;
                        prevBtn.Visible = false;
                        LastBtn.Visible = false;
                    }
                }
                else
                {
                    MessageBox.Show("Can not contact server for process. Check network connection");
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error:" + exc.Message);
            }
        }

        private void firstLbl_Click(object sender, EventArgs e)
        {
            page = 1;
            search_data(page);
        }

        private void LastBtn_Click(object sender, EventArgs e)
        {
            page = last_page;
            search_data(last_page);
        }

        private void prevBtn_Click(object sender, EventArgs e)
        {
            page = page - 1;
            search_data(page);
        }

        private void NextBtn_Click(object sender, EventArgs e)
        {
            page = page + 1;
            search_data(page);
        }

    }
}