﻿namespace SpexMobile
{
    partial class CheckPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.lPassword = new System.Windows.Forms.Label();
            this.passwordtxt = new System.Windows.Forms.TextBox();
            this.lMessage = new System.Windows.Forms.Label();
            this.bOK = new System.Windows.Forms.Button();
            this.bClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lPassword
            // 
            this.lPassword.Location = new System.Drawing.Point(13, 48);
            this.lPassword.Name = "lPassword";
            this.lPassword.Size = new System.Drawing.Size(103, 20);
            this.lPassword.Text = "Password SH/LH";
            // 
            // passwordtxt
            // 
            this.passwordtxt.Location = new System.Drawing.Point(122, 48);
            this.passwordtxt.Name = "passwordtxt";
            this.passwordtxt.PasswordChar = '*';
            this.passwordtxt.Size = new System.Drawing.Size(100, 21);
            this.passwordtxt.TabIndex = 8;
            // 
            // lMessage
            // 
            this.lMessage.Location = new System.Drawing.Point(13, 16);
            this.lMessage.Name = "lMessage";
            this.lMessage.Size = new System.Drawing.Size(210, 20);
            this.lMessage.Text = "Anda akan menutup message ini ?";
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(122, 76);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(72, 20);
            this.bOK.TabIndex = 10;
            this.bOK.Text = "OK";
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bClose
            // 
            this.bClose.Enabled = false;
            this.bClose.Location = new System.Drawing.Point(150, 229);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(72, 20);
            this.bClose.TabIndex = 13;
            this.bClose.Text = "Close";
            this.bClose.Visible = false;
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            // 
            // CheckPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.ControlBox = false;
            this.Controls.Add(this.bClose);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.lMessage);
            this.Controls.Add(this.lPassword);
            this.Controls.Add(this.passwordtxt);
            this.Menu = this.mainMenu1;
            this.MinimizeBox = false;
            this.Name = "CheckPassword";
            this.Text = "CheckPassword";
            this.DoubleClick += new System.EventHandler(this.CheckPassword_DoubleClick);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lPassword;
        private System.Windows.Forms.TextBox passwordtxt;
        private System.Windows.Forms.Label lMessage;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bClose;
    }
}