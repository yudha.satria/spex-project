﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;
using System.Threading;

namespace SpexMobile
{
    public partial class PartStockForm : Form
    {
        bool pageActive = true;
        DatabaseHelper db = new DatabaseHelper();

        public PartStockForm()
        {
            InitializeComponent();
            if (db.using_thread)
            {
                new Thread(CheckConnectionThread).Start();
            }
            loginTxt.Text = DeviceUser.USER_LOGIN;
            TransportCdTxt.Text = DeviceUser.TRANSPORT_CD_DESC;
        }

        void CheckConnectionThread()
        {
            while (true)
            {
                string v = "OFFLINE";
                if (db.IsConnect())
                {
                    v = "ONLINE";
                }
                ChkConPart(v);
                Thread.Sleep(2000);
            }
        }

        public void ChkConPart(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChkConPart), new object[] { value });
                return;
            }
            if (pageActive && (value == "OFFLINE"))
            {
                pageActive = false;
                OffLineInfoForm ph = new OffLineInfoForm();
                this.Hide();
                ph.ShowDialog();
                this.Show();
                pageActive = true;
            }
        }

        public void scan(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                getKanbanId();
            }
        }

        public void getKanbanId()
        {
            try
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@KANBAN_ID", "Text", inputScan.Text));

                DataTable o = db.GetDataTable("spex.SP_PART_STOCK_RECEIVING_SCAN", param);

                if (o.Rows[0]["TEXT"].ToString() == "SUCCESS")
                {
                    clearScan();
                    inputKanbanId.Text = o.Rows[0]["KANBAN_ID"].ToString();
                    inputPartNo.Text = o.Rows[0]["PART_NO"].ToString();
                    inputPcsKanban.Text = o.Rows[0]["PCS_KANBAN"].ToString();
                    inputZone.Text = o.Rows[0]["ZONE"].ToString();
                    inputRackAddress.Text = o.Rows[0]["RACK_ADDRESS"].ToString();
                    MessageBox.Show(o.Rows[0]["MESSAGE"].ToString());
                }
                else
                {
                    clearScan();
                    clearScreen();
                    MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                }
            }
            catch (Exception exc)
            {
                clearScan();
                MessageBox.Show("Error: " + exc.Message);
            }

        }

        public void clearScan()
        {
            inputScan.Text = "";
        }

        public void clearScreen() {
            inputKanbanId.Text = "";
            inputPartNo.Text = "";
            inputPcsKanban.Text = "";
            inputZone.Text = "";
            inputRackAddress.Text = "";
        }

        private void homeBtn_Click(object sender, EventArgs e)
        {

        }

        private void PartStockForm_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Up))
            {
                // Up
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Down))
            {
                // Down
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Left))
            {
                // Left
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Right))
            {
                // Right
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Enter))
            {
                // Enter
            }

        }

        private void homeBtn_Click_1(object sender, EventArgs e)
        {
            pageActive = false;
            this.DialogResult = DialogResult.OK;
        }




    }
}