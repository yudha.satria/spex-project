﻿namespace SpexMobile
{
    partial class PackingProcessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.loginTxt = new System.Windows.Forms.Label();
            this.TransportCdTxt = new System.Windows.Forms.Label();
            this.instLbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.inputTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.homeBtn = new System.Windows.Forms.Button();
            this.CrippleBtn = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.caseTypeLbl = new System.Windows.Forms.Label();
            this.destinationLbl = new System.Windows.Forms.Label();
            this.transportationLbl = new System.Windows.Forms.Label();
            this.privilageLbl = new System.Windows.Forms.Label();
            this.handlingTypeLbl = new System.Windows.Forms.Label();
            this.qtyLbl = new System.Windows.Forms.Label();
            this.prevModeBtn = new System.Windows.Forms.Button();
            this.modeLbl = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.kanbanIdTxt = new System.Windows.Forms.Label();
            this.preparedDtTxt = new System.Windows.Forms.Label();
            this.caseNoLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 245);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 20);
            this.label2.Text = "Mode :";
            // 
            // loginTxt
            // 
            this.loginTxt.Location = new System.Drawing.Point(81, 245);
            this.loginTxt.Name = "loginTxt";
            this.loginTxt.Size = new System.Drawing.Size(66, 20);
            this.loginTxt.Text = "L";
            this.loginTxt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TransportCdTxt
            // 
            this.TransportCdTxt.Location = new System.Drawing.Point(153, 245);
            this.TransportCdTxt.Name = "TransportCdTxt";
            this.TransportCdTxt.Size = new System.Drawing.Size(60, 20);
            this.TransportCdTxt.Text = "label1";
            this.TransportCdTxt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // instLbl
            // 
            this.instLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.instLbl.Location = new System.Drawing.Point(3, 5);
            this.instLbl.Name = "instLbl";
            this.instLbl.Size = new System.Drawing.Size(234, 13);
            this.instLbl.Text = "Please Scan Case No";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 20);
            this.label1.Text = "Input";
            // 
            // inputTxt
            // 
            this.inputTxt.Location = new System.Drawing.Point(108, 22);
            this.inputTxt.Name = "inputTxt";
            this.inputTxt.Size = new System.Drawing.Size(116, 21);
            this.inputTxt.TabIndex = 15;
            this.inputTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.inputTxt_KeyPress);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(3, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 20);
            this.label3.Text = "Case Type";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(3, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 20);
            this.label4.Text = "Destination";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(3, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 20);
            this.label5.Text = "Transportation";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(3, 169);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 20);
            this.label6.Text = "Priviledge";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label7.Location = new System.Drawing.Point(108, 169);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 20);
            this.label7.Text = "Handling Type";
            // 
            // homeBtn
            // 
            this.homeBtn.Location = new System.Drawing.Point(81, 215);
            this.homeBtn.Name = "homeBtn";
            this.homeBtn.Size = new System.Drawing.Size(55, 20);
            this.homeBtn.TabIndex = 35;
            this.homeBtn.Text = "Home";
            this.homeBtn.Click += new System.EventHandler(this.homeBtn_Click);
            // 
            // CrippleBtn
            // 
            this.CrippleBtn.Location = new System.Drawing.Point(13, 215);
            this.CrippleBtn.Name = "CrippleBtn";
            this.CrippleBtn.Size = new System.Drawing.Size(53, 20);
            this.CrippleBtn.TabIndex = 36;
            this.CrippleBtn.Text = "Cripple";
            this.CrippleBtn.Click += new System.EventHandler(this.CrippleBtn_Click);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(92, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 20);
            this.label8.Text = ":";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label9.Location = new System.Drawing.Point(92, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 20);
            this.label9.Text = ":";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label14.Location = new System.Drawing.Point(3, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 20);
            this.label14.Text = "Case No";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label15.Location = new System.Drawing.Point(3, 189);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 20);
            this.label15.Text = "Item Qty";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label10.Location = new System.Drawing.Point(92, 109);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 20);
            this.label10.Text = ":";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label11.Location = new System.Drawing.Point(92, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(10, 20);
            this.label11.Text = ":";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label12.Location = new System.Drawing.Point(92, 149);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 20);
            this.label12.Text = ":";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label13.Location = new System.Drawing.Point(56, 169);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(10, 20);
            this.label13.Text = ":";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label16.Location = new System.Drawing.Point(182, 169);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(10, 20);
            this.label16.Text = ":";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label17.Location = new System.Drawing.Point(92, 189);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(10, 20);
            this.label17.Text = ":";
            // 
            // caseTypeLbl
            // 
            this.caseTypeLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.caseTypeLbl.Location = new System.Drawing.Point(108, 49);
            this.caseTypeLbl.Name = "caseTypeLbl";
            this.caseTypeLbl.Size = new System.Drawing.Size(116, 20);
            this.caseTypeLbl.Text = "DUMMY";
            // 
            // destinationLbl
            // 
            this.destinationLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.destinationLbl.Location = new System.Drawing.Point(108, 129);
            this.destinationLbl.Name = "destinationLbl";
            this.destinationLbl.Size = new System.Drawing.Size(70, 20);
            this.destinationLbl.Text = "label18";
            // 
            // transportationLbl
            // 
            this.transportationLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.transportationLbl.Location = new System.Drawing.Point(108, 149);
            this.transportationLbl.Name = "transportationLbl";
            this.transportationLbl.Size = new System.Drawing.Size(116, 20);
            this.transportationLbl.Text = "label18";
            // 
            // privilageLbl
            // 
            this.privilageLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.privilageLbl.Location = new System.Drawing.Point(71, 169);
            this.privilageLbl.Name = "privilageLbl";
            this.privilageLbl.Size = new System.Drawing.Size(38, 20);
            this.privilageLbl.Text = "-";
            // 
            // handlingTypeLbl
            // 
            this.handlingTypeLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.handlingTypeLbl.Location = new System.Drawing.Point(198, 169);
            this.handlingTypeLbl.Name = "handlingTypeLbl";
            this.handlingTypeLbl.Size = new System.Drawing.Size(26, 20);
            this.handlingTypeLbl.Text = "-";
            // 
            // qtyLbl
            // 
            this.qtyLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.qtyLbl.Location = new System.Drawing.Point(108, 189);
            this.qtyLbl.Name = "qtyLbl";
            this.qtyLbl.Size = new System.Drawing.Size(116, 20);
            this.qtyLbl.Text = "label18";
            // 
            // prevModeBtn
            // 
            this.prevModeBtn.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.prevModeBtn.Location = new System.Drawing.Point(152, 215);
            this.prevModeBtn.Name = "prevModeBtn";
            this.prevModeBtn.Size = new System.Drawing.Size(72, 20);
            this.prevModeBtn.TabIndex = 82;
            this.prevModeBtn.Text = "Prev Mode";
            this.prevModeBtn.Click += new System.EventHandler(this.prevModeBtn_Click);
            // 
            // modeLbl
            // 
            this.modeLbl.Location = new System.Drawing.Point(42, 245);
            this.modeLbl.Name = "modeLbl";
            this.modeLbl.Size = new System.Drawing.Size(50, 36);
            this.modeLbl.Text = "New";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label18.Location = new System.Drawing.Point(3, 69);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 20);
            this.label18.Text = "Kanban ID";
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label19.Location = new System.Drawing.Point(3, 89);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 20);
            this.label19.Text = "Prepared DT";
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label20.Location = new System.Drawing.Point(92, 69);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(10, 20);
            this.label20.Text = ":";
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label21.Location = new System.Drawing.Point(92, 89);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(10, 20);
            this.label21.Text = ":";
            // 
            // kanbanIdTxt
            // 
            this.kanbanIdTxt.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.kanbanIdTxt.Location = new System.Drawing.Point(108, 69);
            this.kanbanIdTxt.Name = "kanbanIdTxt";
            this.kanbanIdTxt.Size = new System.Drawing.Size(116, 20);
            this.kanbanIdTxt.Text = "DUMMY";
            // 
            // preparedDtTxt
            // 
            this.preparedDtTxt.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.preparedDtTxt.Location = new System.Drawing.Point(108, 89);
            this.preparedDtTxt.Name = "preparedDtTxt";
            this.preparedDtTxt.Size = new System.Drawing.Size(116, 20);
            this.preparedDtTxt.Text = "DUMMY";
            // 
            // caseNoLbl
            // 
            this.caseNoLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.caseNoLbl.Location = new System.Drawing.Point(108, 109);
            this.caseNoLbl.Name = "caseNoLbl";
            this.caseNoLbl.Size = new System.Drawing.Size(116, 20);
            this.caseNoLbl.Text = "label18";
            // 
            // PackingProcessForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.preparedDtTxt);
            this.Controls.Add(this.kanbanIdTxt);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.modeLbl);
            this.Controls.Add(this.prevModeBtn);
            this.Controls.Add(this.qtyLbl);
            this.Controls.Add(this.handlingTypeLbl);
            this.Controls.Add(this.privilageLbl);
            this.Controls.Add(this.transportationLbl);
            this.Controls.Add(this.destinationLbl);
            this.Controls.Add(this.caseNoLbl);
            this.Controls.Add(this.caseTypeLbl);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.CrippleBtn);
            this.Controls.Add(this.homeBtn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.inputTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TransportCdTxt);
            this.Controls.Add(this.loginTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.instLbl);
            this.Name = "PackingProcessForm";
            this.Text = "Packing";
            this.Load += new System.EventHandler(this.PackingProcessForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label loginTxt;
        private System.Windows.Forms.Label TransportCdTxt;
        private System.Windows.Forms.Label instLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox inputTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button homeBtn;
        private System.Windows.Forms.Button CrippleBtn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label caseTypeLbl;
        private System.Windows.Forms.Label destinationLbl;
        private System.Windows.Forms.Label transportationLbl;
        private System.Windows.Forms.Label privilageLbl;
        private System.Windows.Forms.Label handlingTypeLbl;
        private System.Windows.Forms.Label qtyLbl;
        private System.Windows.Forms.Button prevModeBtn;
        private System.Windows.Forms.Label modeLbl;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label kanbanIdTxt;
        private System.Windows.Forms.Label preparedDtTxt;
        private System.Windows.Forms.Label caseNoLbl;
    }
}