﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace SpexMobile
{
    public partial class ConfigHomeForm : Form
    {
        public ConfigHomeForm()
        {
            InitializeComponent();
        }

        private void LogoutBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnDatabase_Click(object sender, EventArgs e)
        {
            SettingDBForm ph = new SettingDBForm();
            this.Hide();
            ph.ShowDialog();
            this.Show();
        }

        private void btnPrinter_Click(object sender, EventArgs e)
        {
            PrinterConfigForm ph = new PrinterConfigForm();
            this.Hide();
            ph.ShowDialog();
            this.Show();
        }

        private void btnTaskMgr_Click(object sender, EventArgs e)
        {
            if (Environment.OSVersion.Platform.ToString() == "WinCE")
            {
                string path;
                path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                path = path.Replace("file:\\", "") + "\\ITaskMgr.exe";
                Process l = new Process();
                l.StartInfo.FileName = path;
                l.Start();
            }
            else
            {
                // Create a new instance
                Process p = new Process();
                p.StartInfo.FileName = "taskmgr";
                p.Start();
            }

        }

    }
}