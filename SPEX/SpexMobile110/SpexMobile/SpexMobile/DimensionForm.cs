﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;

namespace SpexMobile
{
    public partial class DimensionForm : Form
    {
        DatabaseHelper db = new DatabaseHelper();
        public string PreparedDate;
        public string CaseType;
        public string CaseNo;
        public bool SaveSuccess = false;

        public DimensionForm()
        {
            InitializeComponent();
        }

        private void DimensionForm_Load(object sender, EventArgs e)
        {

        }

        

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(lengthintTxt.Text)
                && !string.IsNullOrEmpty(widthintTxt.Text)
                && !string.IsNullOrEmpty(heightintTxt.Text)
                && !string.IsNullOrEmpty(weightintTxt.Text)
                &&!string.IsNullOrEmpty(lengthdecTxt.Text)
                && !string.IsNullOrEmpty(widthdecTxt.Text)
                && !string.IsNullOrEmpty(heightdecTxt.Text)
                && !string.IsNullOrEmpty(weightdecTxt.Text)
                )
            {
                try
                {
                    string l = lengthintTxt.Text + "." + lengthdecTxt.Text;
                    int li = Int32.Parse(lengthintTxt.Text) + Int32.Parse(lengthdecTxt.Text);
                    if (li > 0)
                    {
                        string w = widthintTxt.Text + "." + widthdecTxt.Text;
                        int wi = Int32.Parse(widthintTxt.Text) + Int32.Parse(widthdecTxt.Text);
                        if (wi > 0)
                        {
                            string h = heightintTxt.Text + "." + heightdecTxt.Text;
                            int hi = Int32.Parse(heightintTxt.Text) + Int32.Parse(heightdecTxt.Text);
                            if(hi > 0)
                            {
                                string we = weightintTxt.Text + "." + weightdecTxt.Text;
                                int wei = Int32.Parse(weightintTxt.Text) + Int32.Parse(weightdecTxt.Text);

                                if (wei > 0)
                                {
                                    /* posibly sql injection, change using sp 20171010 bobby
                                    string sql = "UPDATE spex.TB_R_READYCARGO_CASE SET ";
                                    sql += " CASE_LENGTH=" + l;
                                    sql += " ,CASE_WIDTH=" + w;
                                    sql += " ,CASE_HEIGHT=" + h;
                                    sql += " ,CASE_GROSS_WEIGHT=" + we;
                                    sql += " WHERE CASE_TYPE='" + CaseType + "' AND CASE_NO='" + CaseNo + "' AND CREATED_DT='" + PreparedDate + "'";

                                    db.ExecuteSql(sql);
                                     * */

                                    List<DbParam> param = new List<DbParam>();
                                    param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                                    param.Add(new DbParam("@PREPARED_DT", "Text", PreparedDate));
                                    param.Add(new DbParam("@CASE_NO", "Text", CaseNo));
                                    param.Add(new DbParam("@CASE_TYPE", "Text", CaseType.Substring(0,2)));
                                    param.Add(new DbParam("@CASE_LENGTH", "Text", l));
                                    param.Add(new DbParam("@CASE_WIDTH", "Text", w));
                                    param.Add(new DbParam("@CASE_HEIGHT", "Text", h));
                                    param.Add(new DbParam("@CASE_GROSS_WEIGHT", "Text", we));

                                    DataTable o = db.GetDataTable("[spex].SP_DEVICE_PACKING_SAVE_DIMENSION", param);
                                    if (o.Rows[0]["TEXT"].ToString() == "Success")
                                    {
                                        SaveSuccess = true;
                                        this.DialogResult = DialogResult.OK;
                                    }
                                    else
                                    {
                                        MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Weight Can Not 0");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Height Can Not 0");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Width Can Not 0");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Length Can Not 0");
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }


                
            }
            else
            {
                MessageBox.Show("Semua textboxt harus diisi");
            }
            
        }

        private void lengthdecTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        private void lengthintTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void widthintTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void widthdecTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void heightintTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void heightdecTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void weightintTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void weightdecTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}