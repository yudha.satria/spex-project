﻿namespace SpexMobile
{
    partial class DimensionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lengthintTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.widthintTxt = new System.Windows.Forms.TextBox();
            this.heightintTxt = new System.Windows.Forms.TextBox();
            this.weightintTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lengthdecTxt = new System.Windows.Forms.TextBox();
            this.widthdecTxt = new System.Windows.Forms.TextBox();
            this.heightdecTxt = new System.Windows.Forms.TextBox();
            this.weightdecTxt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 20);
            this.label1.Text = "Case dimension input";
            // 
            // lengthintTxt
            // 
            this.lengthintTxt.Location = new System.Drawing.Point(67, 88);
            this.lengthintTxt.MaxLength = 7;
            this.lengthintTxt.Name = "lengthintTxt";
            this.lengthintTxt.Size = new System.Drawing.Size(64, 21);
            this.lengthintTxt.TabIndex = 1;
            this.lengthintTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lengthintTxt_KeyPress);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(4, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 20);
            this.label2.Text = "L";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 20);
            this.label3.Text = "W";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 20);
            this.label4.Text = "H";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(4, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 20);
            this.label5.Text = "Weight";
            // 
            // widthintTxt
            // 
            this.widthintTxt.Location = new System.Drawing.Point(67, 116);
            this.widthintTxt.MaxLength = 7;
            this.widthintTxt.Name = "widthintTxt";
            this.widthintTxt.Size = new System.Drawing.Size(64, 21);
            this.widthintTxt.TabIndex = 9;
            this.widthintTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.widthintTxt_KeyPress);
            // 
            // heightintTxt
            // 
            this.heightintTxt.Location = new System.Drawing.Point(67, 145);
            this.heightintTxt.MaxLength = 7;
            this.heightintTxt.Name = "heightintTxt";
            this.heightintTxt.Size = new System.Drawing.Size(64, 21);
            this.heightintTxt.TabIndex = 10;
            this.heightintTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.heightintTxt_KeyPress);
            // 
            // weightintTxt
            // 
            this.weightintTxt.Location = new System.Drawing.Point(67, 173);
            this.weightintTxt.MaxLength = 7;
            this.weightintTxt.Name = "weightintTxt";
            this.weightintTxt.Size = new System.Drawing.Size(64, 21);
            this.weightintTxt.TabIndex = 11;
            this.weightintTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.weightintTxt_KeyPress);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(48, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 20);
            this.label6.Text = ":";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(48, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 20);
            this.label7.Text = ":";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(48, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 20);
            this.label8.Text = ":";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(48, 145);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 20);
            this.label9.Text = ":";
            // 
            // lengthdecTxt
            // 
            this.lengthdecTxt.Location = new System.Drawing.Point(137, 87);
            this.lengthdecTxt.MaxLength = 1;
            this.lengthdecTxt.Name = "lengthdecTxt";
            this.lengthdecTxt.Size = new System.Drawing.Size(24, 21);
            this.lengthdecTxt.TabIndex = 20;
            this.lengthdecTxt.Text = "0";
            this.lengthdecTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lengthdecTxt_KeyPress);
            // 
            // widthdecTxt
            // 
            this.widthdecTxt.Location = new System.Drawing.Point(137, 116);
            this.widthdecTxt.MaxLength = 1;
            this.widthdecTxt.Name = "widthdecTxt";
            this.widthdecTxt.Size = new System.Drawing.Size(24, 21);
            this.widthdecTxt.TabIndex = 21;
            this.widthdecTxt.Text = "0";
            this.widthdecTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.widthdecTxt_KeyPress);
            // 
            // heightdecTxt
            // 
            this.heightdecTxt.Location = new System.Drawing.Point(137, 145);
            this.heightdecTxt.MaxLength = 1;
            this.heightdecTxt.Name = "heightdecTxt";
            this.heightdecTxt.Size = new System.Drawing.Size(24, 21);
            this.heightdecTxt.TabIndex = 22;
            this.heightdecTxt.Text = "0";
            this.heightdecTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.heightdecTxt_KeyPress);
            // 
            // weightdecTxt
            // 
            this.weightdecTxt.Location = new System.Drawing.Point(137, 173);
            this.weightdecTxt.MaxLength = 1;
            this.weightdecTxt.Name = "weightdecTxt";
            this.weightdecTxt.Size = new System.Drawing.Size(24, 21);
            this.weightdecTxt.TabIndex = 23;
            this.weightdecTxt.Text = "0";
            this.weightdecTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.weightdecTxt_KeyPress);
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(167, 88);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 20);
            this.label10.Text = "cm";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(167, 117);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 20);
            this.label11.Text = "cm";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(167, 145);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 20);
            this.label12.Text = "cm";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(167, 174);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 20);
            this.label13.Text = "gr";
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(4, 224);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(72, 20);
            this.saveBtn.TabIndex = 32;
            this.saveBtn.Text = "Save";
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(82, 224);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(72, 20);
            this.cancelBtn.TabIndex = 33;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // DimensionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.weightdecTxt);
            this.Controls.Add(this.heightdecTxt);
            this.Controls.Add(this.widthdecTxt);
            this.Controls.Add(this.lengthdecTxt);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.weightintTxt);
            this.Controls.Add(this.heightintTxt);
            this.Controls.Add(this.widthintTxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lengthintTxt);
            this.Controls.Add(this.label1);
            this.Name = "DimensionForm";
            this.Text = "DimensionForm";
            this.Load += new System.EventHandler(this.DimensionForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox lengthintTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox widthintTxt;
        private System.Windows.Forms.TextBox heightintTxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox lengthdecTxt;
        private System.Windows.Forms.TextBox widthdecTxt;
        private System.Windows.Forms.TextBox heightdecTxt;
        private System.Windows.Forms.TextBox weightdecTxt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.TextBox weightintTxt;

    }
}