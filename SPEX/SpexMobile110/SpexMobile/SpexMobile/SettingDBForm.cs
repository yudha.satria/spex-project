﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using SpexMobile.AppCode;

namespace SpexMobile
{
    public partial class SettingDBForm : Form
    {
        DatabaseLocalHelper dh = new DatabaseLocalHelper();
        public SettingDBForm()
        {
            InitializeComponent();
            InitData();
        }

        private void InitData()
        {
            serverNameTxt.Text = dh.GetSysData("db_server");
            catalogTxt.Text = dh.GetSysData("db_catalog");
            usernameTxt.Text = dh.GetSysData("db_uid");
            passwordTxt.Text = dh.GetSysData("db_password");
            urlWsTxt.Text = dh.GetSysData("url_ws");
            string conn_using_ws = dh.GetSysData("conn_using_ws");
            tbTimeOut.Text = dh.GetSysData("timeout");
            if (conn_using_ws == "1")
            {
                tabControl1.SelectedIndex = 1;
            }
            else
            {
                tabControl1.SelectedIndex = 0;
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0) //database
            {
                string constr = "Data Source=" + serverNameTxt.Text + ";Initial Catalog=" + catalogTxt.Text + ";UID=" + usernameTxt.Text + ";Password=" + passwordTxt.Text + ";Persist Security Info=false;";

                try
                {
                    SqlConnection con = new SqlConnection(constr);
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    con.Open();
                    con.Close();

                    MessageBox.Show("Database Connected");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Not Connected");
                }
            }
            else //webservice
            {
                try
                {
                    spexws.Spex ws = new spexws.Spex();
                    ws.Url = urlWsTxt.Text;
                    if (ws.CheckConnection() == "ok")
                    {
                        MessageBox.Show("Webservice Connected");
                    }
                    else
                    {
                        MessageBox.Show("Webservice Not Connected");
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Webservice Not Connected: " +exc.Message);
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                dh.SetSysData("db_server", serverNameTxt.Text);
                dh.SetSysData("db_catalog", catalogTxt.Text);
                dh.SetSysData("db_uid", usernameTxt.Text);
                dh.SetSysData("db_password", passwordTxt.Text);
                string conn_using_ws = "0";
                if (tabControl1.SelectedIndex == 1)
                {
                    conn_using_ws = "1";
                }
                dh.SetSysData("conn_using_ws", conn_using_ws);
                dh.SetSysData("url_ws", urlWsTxt.Text);
                dh.SetSysData("timeout", tbTimeOut.Text);
                MessageBox.Show("Save Successfully");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void tbTimeOut_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbTimeOut_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void SettingDBForm_Load(object sender, EventArgs e)
        {

        }
    }
}