﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;
using System.Threading;
using System.Data.SqlClient;

namespace SpexMobile
{
    public partial class PartHomeForm : Form
    {
        bool pageActive = true;
        DatabaseHelper db = new DatabaseHelper();

        public PartHomeForm()
        {
            InitializeComponent();
            cekData();
        }

        public void ChkConPart(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChkConPart), new object[] { value });
                return;
            }
            if (pageActive && (value == "OFFLINE"))
            {
                OffLineInfoForm ph = new OffLineInfoForm();
                pageActive = false;
                this.Hide();
                ph.ShowDialog();
                this.Show();
                pageActive = true;
            }
        }
        void CheckConnectionThread()
        {
            while (true)
            {
                string v = "OFFLINE";
                if (db.IsConnect())
                {
                    v = "ONLINE";
                }
                ChkConPart(v);
                Thread.Sleep(2000);
            }
        }

        private void cekData()
        {
            binningBtn.Enabled = false;
            pickingBtn.Enabled = false;

            if (DeviceUser.PLANT_LINE_CD == "B")
            {
                binningBtn.Enabled = true;
            }

            if (DeviceUser.PLANT_LINE_CD == "C") 
            {
                pickingBtn.Enabled = true;            
            }
        }

        private void binningBtn_Click(object sender, EventArgs e)
        {
            PartStockForm ph = new PartStockForm();
            pageActive = false;
            this.Hide();
            ph.ShowDialog();
            this.Show();
            pageActive = true;
            cekData();
        }

        private void pickingBtn_Click(object sender, EventArgs e)
        {
            PickingListForm ph = new PickingListForm();
            pageActive = false;
            this.Hide();
            ph.ShowDialog();
            this.Show();
            pageActive = true;
            cekData();
        }

        private void PartHomeForm_Load(object sender, EventArgs e)
        {
            loginTxt.Text = DeviceUser.USER_LOGIN;
            TransportCdTxt.Text = DeviceUser.TRANSPORT_CD_DESC;
            LineCodeLbl.Text = "Line Code:" + DeviceUser.PLANT_LINE_CD;
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (db.IsConnect())
                {
                    pageActive = false;
                    db.Logout();
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Can not contact server for process. Check network connection");
                }

            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        }



    }
}