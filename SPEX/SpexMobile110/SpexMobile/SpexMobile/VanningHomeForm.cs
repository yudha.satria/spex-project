﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;
using System.Threading;
using System.Data.SqlClient;

namespace SpexMobile
{
    public partial class VanningHomeForm : Form
    {
        bool pageActive = true;
        DatabaseHelper db = new DatabaseHelper();
        public VanningHomeForm()
        {
            InitializeComponent();
            cekData();
            if (db.using_thread)
            {
                new Thread(CheckConnectionThread).Start();
            }
        }

        public void ChkConPart(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChkConPart), new object[] { value });
                return;
            }
            if (pageActive && (value == "OFFLINE"))
            {
                OffLineInfoForm ph = new OffLineInfoForm();
                pageActive = false;
                this.Hide();
                ph.ShowDialog();
                this.Show();
                pageActive = true;
            }
        }

        void CheckConnectionThread()
        {
            while (true)
            {
                string v = "OFFLINE";
                if (db.IsConnect())
                {
                    v = "ONLINE";
                }
                ChkConPart(v);
                Thread.Sleep(2000);
            }
        }

        private void cekData()
        {
            if (db.IsConnect())
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@MODE", "Text", "REOPEN"));
                param.Add(new DbParam("@PAGEVIEW", "Int", "1"));

                DataSet ds1 = db.GetDataSet("[spex].SP_DEVICE_GET_GRID_VANNING", param);
                DataTable o1 = ds1.Tables[0];
                btnReopen.Enabled = (o1.Rows.Count > 0);

                param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@MODE", "Text", "REMAIN"));
                param.Add(new DbParam("@PAGEVIEW", "Int", "1"));

                DataSet ds2 = db.GetDataSet("[spex].SP_DEVICE_GET_GRID_VANNING", param);
                DataTable o2 = ds2.Tables[0];
                btnRemain.Enabled = (o2.Rows.Count > 0);
            }
            else
            {
                MessageBox.Show("Can not contact server for process. Check network connection");
            }
        }

        private void PackingHomeForm_Load(object sender, EventArgs e)
        {
            loginTxt.Text = DeviceUser.USER_LOGIN;
            TransportCdTxt.Text = DeviceUser.TRANSPORT_CD_DESC;
            //LineCodeLbl.Text = "Line Code:" + DeviceUser.PLANT_LINE_CD;
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (db.IsConnect())
                {
                    db.Logout();
                    pageActive = false;
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Can not contact server for process. Check network connection");
                }

            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        }

        private void newProcessBtn_Click(object sender, EventArgs e)
        {
            DeviceUser.DEVICE_MODE = "NEW";
            
            VanningProcessForm ph = new VanningProcessForm();
            pageActive = false;
            this.Hide();
            ph.ShowDialog();
            this.Show();
            pageActive = true;
            cekData();
        }

        private void btnReopen_Click(object sender, EventArgs e)
        {
            GridVanning gc = new GridVanning("REOPEN");
            pageActive = false;
            this.Hide();
            gc.ShowDialog();
            this.Show();
            pageActive = true;
            cekData();
        }

        private void btnRemain_Click(object sender, EventArgs e)
        {
            GridVanning gc = new GridVanning("REMAIN");
            pageActive = false;
            this.Hide();
            gc.ShowDialog();
            this.Show();
            pageActive = true;
            cekData();
        }
    }
}