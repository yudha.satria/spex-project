﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;

namespace SpexMobile
{
    public partial class PrinterConfigForm : Form
    {
        DatabaseLocalHelper dh = new DatabaseLocalHelper();
        DatabaseHelper db = new DatabaseHelper();
        public PrinterConfigForm()
        {
            InitializeComponent();
            InitData();
        }
        private void InitData()
        {
            refreshCombo();
            cmbPrinter.SelectedValue = dh.GetSysData("printer");
        }
        private void refreshCombo()
        {
            DataTable o = db.GetDataTable("SELECT PRINTER_NAME ID,(PRINTER_NAME+' | '+STATUS) AS DESK FROM [spex].[TB_P_PRINTER]");
            cmbPrinter.DataSource = o;
            cmbPrinter.ValueMember = "ID";
            cmbPrinter.DisplayMember = "DESK";
            cmbPrinter.Refresh();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                dh.SetSysData("printer", cmbPrinter.SelectedValue.ToString());
                MessageBox.Show("Save Successfully");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void TestBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "TESTPRINT|" + cmbPrinter.SelectedValue.ToString();
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@Message", "Text", message));
                db.ExecuteProcedure("[spex].spQueueSend", param);
                MessageBox.Show("Success");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void btnRenew_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "RENEWPRINT";
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@Message", "Text", message));
                db.ExecuteProcedure("[spex].spQueueSend", param);
                MessageBox.Show("Success");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            refreshCombo();
        }
    }
}