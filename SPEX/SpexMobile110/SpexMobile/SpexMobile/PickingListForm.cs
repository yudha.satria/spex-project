﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;
using System.Threading;

namespace SpexMobile
{
    public partial class PickingListForm : Form
    {
        bool pageActive = true;
        DatabaseHelper db = new DatabaseHelper();


        public PickingListForm()
        {
            InitializeComponent();
            txtScanId.Text = "PICKING_LIST";
            if (db.using_thread)
            {
                new Thread(CheckConnectionThread).Start();
            }
            loginTxt.Text = DeviceUser.USER_LOGIN;
            TransportCdTxt.Text = DeviceUser.TRANSPORT_CD_DESC;
        }

        void CheckConnectionThread()
        {
            while (true)
            {
                string v = "OFFLINE";
                if (db.IsConnect())
                {
                    v = "ONLINE";
                }
                ChkConPart(v);
                Thread.Sleep(2000);
            }
        }

        private void label1_ParentChanged(object sender, EventArgs e)
        {

        }

        private void label2_ParentChanged(object sender, EventArgs e)
        {

        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
            MessageBox.Show("Manifest No telah berhasil di-receive");
        }

        private void tbPickingList_TextChanged(object sender, EventArgs e)
        {
            //string pickingListNo = tbPickingList.Text;
        }

        public void ChkConPart(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChkConPart), new object[] { value });
                return;
            }
            if (pageActive && (value == "OFFLINE"))
            {
                pageActive = false;
                OffLineInfoForm ph = new OffLineInfoForm();
                this.Hide();
                ph.ShowDialog();
                this.Show();
                pageActive = true;
            }
        }


        public void scan(object sender, KeyPressEventArgs e)
        { 
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtScanId.Text =="PICKING_LIST")
                {
                    getPickingList();
                }
                else if (txtScanId.Text == "PART_NO")
                {
                    getPartNo();
                }
                else if (txtScanId.Text == "KANBAN_ID")
                {
                    getKanbanId();
                }
            }        
        }



        public void getPickingList()
        {
                    try
                    {
                        List<DbParam> param = new List<DbParam>();
                        param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                        param.Add(new DbParam("@PICKING_LIST_NO", "Text", inputScan.Text));
                        param.Add(new DbParam("@PART_NO", "Text", ""));
                        param.Add(new DbParam("@KANBAN_ID", "Text", ""));
                        param.Add(new DbParam("@SCAN_METHOD", "Text", txtScanId.Text));

                        
                        DataTable o = db.GetDataTable("spex.SP_PICKING_LIST_SCAN", param);
                        if (o.Rows[0]["TEXT"].ToString() == "Success")
                        {
                            txtPickingListNo.Text = o.Rows[0]["MANIFEST_NO"].ToString();
                            inputZone.Text = o.Rows[0]["ZONE"].ToString();
                            txtScanId.Text = "PART_NO";
                            txtScan.Text = "PART NO";
                            clearScan();
                        }
                        else
                        {
                            MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                            clearScan();                            
                        }

                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Error: " + exc.Message);
                        clearScan();
                    }           
        }

        public void getPartNo() {
            try
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@PICKING_LIST_NO", "Text", txtPickingListNo.Text));
                param.Add(new DbParam("@PART_NO", "Text", inputScan.Text));
                param.Add(new DbParam("@KANBAN_ID", "Text", ""));
                param.Add(new DbParam("@SCAN_METHOD", "Text", txtScanId.Text));


                DataTable o = db.GetDataTable("spex.SP_PICKING_LIST_SCAN", param);
                if (o.Rows[0]["TEXT"].ToString() == "Success")
                {
                    inputPartNo.Text = o.Rows[0]["PART_NO"].ToString();
                    inputPartAddress.Text = o.Rows[0]["RACK_ADDRESS_CD"].ToString();
                    txtScanId.Text = "KANBAN_ID";
                    txtScan.Text = "KANBAN ID";
                    clearScan();
                }
                else
                {
                    clearScan();
                    MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
                clearScan();
            }                   
        }

        public void getKanbanId()
        {
            try
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@PICKING_LIST_NO", "Text", txtPickingListNo.Text));
                param.Add(new DbParam("@PART_NO", "Text", inputPartNo.Text));
                param.Add(new DbParam("@KANBAN_ID", "Text", inputScan.Text));
                param.Add(new DbParam("@SCAN_METHOD", "Text", txtScanId.Text));

                DataTable o = db.GetDataTable("spex.SP_PICKING_LIST_SCAN", param);
                if (o.Rows[0]["TEXT"].ToString() == "Success")
                {
                    inputKanbanId.Text = o.Rows[0]["KANBAN_ID"].ToString();
                    inputQty.Text = o.Rows[0]["TOTAL_QTY"].ToString();
                    txtScanId.Text = "PART_NO";
                    txtScan.Text = "PART NO";
                    resetData();
                }
                else
                {
                   clearScan();
                   MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        
        }

        public void resetData() {
            inputScan.Text = "";
            inputPartNo.Text = "";
            inputKanbanId.Text = "";
            inputPartAddress.Text = "";
        }

        public void clearScan() {
            inputScan.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
            if (confirmResult == DialogResult.Yes)
            {
                pageActive = false;
                this.DialogResult = DialogResult.OK;

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pageActive = false;
            this.DialogResult = DialogResult.OK;

        }


    }
}