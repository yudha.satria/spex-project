﻿namespace SpexMobile
{
    partial class ConfigHomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDatabase = new System.Windows.Forms.Button();
            this.btnPrinter = new System.Windows.Forms.Button();
            this.LogoutBtn = new System.Windows.Forms.Button();
            this.btnTaskMgr = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDatabase
            // 
            this.btnDatabase.Location = new System.Drawing.Point(36, 41);
            this.btnDatabase.Name = "btnDatabase";
            this.btnDatabase.Size = new System.Drawing.Size(170, 31);
            this.btnDatabase.TabIndex = 0;
            this.btnDatabase.Text = "Connection";
            this.btnDatabase.Click += new System.EventHandler(this.btnDatabase_Click);
            // 
            // btnPrinter
            // 
            this.btnPrinter.Location = new System.Drawing.Point(36, 78);
            this.btnPrinter.Name = "btnPrinter";
            this.btnPrinter.Size = new System.Drawing.Size(170, 31);
            this.btnPrinter.TabIndex = 1;
            this.btnPrinter.Text = "Printer";
            this.btnPrinter.Click += new System.EventHandler(this.btnPrinter_Click);
            // 
            // LogoutBtn
            // 
            this.LogoutBtn.Location = new System.Drawing.Point(36, 215);
            this.LogoutBtn.Name = "LogoutBtn";
            this.LogoutBtn.Size = new System.Drawing.Size(170, 31);
            this.LogoutBtn.TabIndex = 2;
            this.LogoutBtn.Text = "Logout";
            this.LogoutBtn.Click += new System.EventHandler(this.LogoutBtn_Click);
            // 
            // btnTaskMgr
            // 
            this.btnTaskMgr.Location = new System.Drawing.Point(36, 115);
            this.btnTaskMgr.Name = "btnTaskMgr";
            this.btnTaskMgr.Size = new System.Drawing.Size(170, 31);
            this.btnTaskMgr.TabIndex = 3;
            this.btnTaskMgr.Text = "Task Manager";
            this.btnTaskMgr.Click += new System.EventHandler(this.btnTaskMgr_Click);
            // 
            // ConfigHomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.btnTaskMgr);
            this.Controls.Add(this.LogoutBtn);
            this.Controls.Add(this.btnPrinter);
            this.Controls.Add(this.btnDatabase);
            this.Name = "ConfigHomeForm";
            this.Text = "ConfigHomeForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDatabase;
        private System.Windows.Forms.Button btnPrinter;
        private System.Windows.Forms.Button LogoutBtn;
        private System.Windows.Forms.Button btnTaskMgr;
    }
}