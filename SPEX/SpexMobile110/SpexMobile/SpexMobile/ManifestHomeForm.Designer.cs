﻿namespace SpexMobile
{
    partial class ManifestHomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.receivingBtn = new System.Windows.Forms.Button();
            this.btnCripple = new System.Windows.Forms.Button();
            this.btnReopen = new System.Windows.Forms.Button();
            this.btnRemaining = new System.Windows.Forms.Button();
            this.LineCodeLbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.loginTxt = new System.Windows.Forms.Label();
            this.TransportCdTxt = new System.Windows.Forms.Label();
            this.finishBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // receivingBtn
            // 
            this.receivingBtn.Location = new System.Drawing.Point(33, 36);
            this.receivingBtn.Name = "receivingBtn";
            this.receivingBtn.Size = new System.Drawing.Size(180, 25);
            this.receivingBtn.TabIndex = 0;
            this.receivingBtn.Text = "Receiving";
            this.receivingBtn.Click += new System.EventHandler(this.receivingBtn_Click);
            // 
            // btnCripple
            // 
            this.btnCripple.Location = new System.Drawing.Point(33, 157);
            this.btnCripple.Name = "btnCripple";
            this.btnCripple.Size = new System.Drawing.Size(180, 25);
            this.btnCripple.TabIndex = 1;
            this.btnCripple.Text = "Cripple Process";
            this.btnCripple.Click += new System.EventHandler(this.btnCripple_Click);
            // 
            // btnReopen
            // 
            this.btnReopen.Location = new System.Drawing.Point(33, 126);
            this.btnReopen.Name = "btnReopen";
            this.btnReopen.Size = new System.Drawing.Size(180, 25);
            this.btnReopen.TabIndex = 2;
            this.btnReopen.Text = "Reopen Process";
            this.btnReopen.Click += new System.EventHandler(this.btnReopen_Click);
            // 
            // btnRemaining
            // 
            this.btnRemaining.Location = new System.Drawing.Point(33, 95);
            this.btnRemaining.Name = "btnRemaining";
            this.btnRemaining.Size = new System.Drawing.Size(180, 25);
            this.btnRemaining.TabIndex = 3;
            this.btnRemaining.Text = "Remaining Process";
            this.btnRemaining.Click += new System.EventHandler(this.btnRemaining_Click);
            // 
            // LineCodeLbl
            // 
            this.LineCodeLbl.Location = new System.Drawing.Point(47, 10);
            this.LineCodeLbl.Name = "LineCodeLbl";
            this.LineCodeLbl.Size = new System.Drawing.Size(166, 20);
            this.LineCodeLbl.Text = "Line Code :  R - Receiving";
            this.LineCodeLbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 238);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.Text = "Mode : N/A";
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(141, 200);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(72, 20);
            this.logoutBtn.TabIndex = 6;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // loginTxt
            // 
            this.loginTxt.Location = new System.Drawing.Point(81, 238);
            this.loginTxt.Name = "loginTxt";
            this.loginTxt.Size = new System.Drawing.Size(66, 20);
            this.loginTxt.Text = "L";
            this.loginTxt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TransportCdTxt
            // 
            this.TransportCdTxt.Location = new System.Drawing.Point(153, 238);
            this.TransportCdTxt.Name = "TransportCdTxt";
            this.TransportCdTxt.Size = new System.Drawing.Size(60, 20);
            this.TransportCdTxt.Text = "label1";
            this.TransportCdTxt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // finishBtn
            // 
            this.finishBtn.Location = new System.Drawing.Point(33, 66);
            this.finishBtn.Name = "finishBtn";
            this.finishBtn.Size = new System.Drawing.Size(180, 25);
            this.finishBtn.TabIndex = 9;
            this.finishBtn.Text = "Finish Case";
            this.finishBtn.Click += new System.EventHandler(this.finishBtn_Click);
            // 
            // ManifestHomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.finishBtn);
            this.Controls.Add(this.TransportCdTxt);
            this.Controls.Add(this.loginTxt);
            this.Controls.Add(this.logoutBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LineCodeLbl);
            this.Controls.Add(this.btnRemaining);
            this.Controls.Add(this.btnReopen);
            this.Controls.Add(this.btnCripple);
            this.Controls.Add(this.receivingBtn);
            this.Name = "ManifestHomeForm";
            this.Text = "Home Manifest Receiving";
            this.Load += new System.EventHandler(this.ManifestHomeForm_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ManifestHomeForm_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button receivingBtn;
        private System.Windows.Forms.Button btnCripple;
        private System.Windows.Forms.Button btnReopen;
        private System.Windows.Forms.Button btnRemaining;
        private System.Windows.Forms.Label LineCodeLbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Label loginTxt;
        private System.Windows.Forms.Label TransportCdTxt;
        private System.Windows.Forms.Button finishBtn;
    }
}