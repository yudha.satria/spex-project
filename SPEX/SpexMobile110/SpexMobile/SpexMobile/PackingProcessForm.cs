﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;
using System.Threading;
using System.Data.SqlClient;

namespace SpexMobile
{
    public partial class PackingProcessForm : Form
    {
        bool pageActive = true;
        DatabaseHelper db = new DatabaseHelper();
        DatabaseLocalHelper dl = new DatabaseLocalHelper();
        public bool is_activate = false;
        public static string inputtext = "";
        public string last_status = "";

        public PackingProcessForm()
        {
            InitializeComponent();
            clearForm();
            if (db.using_thread)
            {
                new Thread(CheckConnectionThread).Start();
            }
        }

        public PackingProcessForm(string casetype, string caseno, string qty, string destination, string priv, string prepdate, string df, string laststatus, bool isRemain)
        {
            InitializeComponent();
            caseTypeLbl.Text = casetype;
            caseNoLbl.Text = caseno;
            qtyLbl.Text = qty;
            destinationLbl.Text = destination;
            privilageLbl.Text = priv;
            preparedDtTxt.Text = prepdate;
            handlingTypeLbl.Text = df;
            kanbanIdTxt.Text = "";
            instLbl.Text = "Please Scan Case No";
            
            if (DeviceUser.TRANSPORT_CD == "1") //air
            {
                transportationLbl.Text = "1 - AIR";
            }
            else if (DeviceUser.TRANSPORT_CD == "2") //sea
            {
                transportationLbl.Text = "2 - SEA";
            }
            modeLbl.Text = laststatus;
            last_status = laststatus;
            homeBtn.Visible = true;
            CrippleBtn.Visible = false;
            prevModeBtn.Visible = false;
            
            is_activate = (laststatus == "CRIP" || laststatus == "REOP");
            if (isRemain)
            {
                modeLbl.Text = DeviceUser.DEVICE_MODE + " " + DeviceUser.FILL_MODE; 
                if (DeviceUser.DEVICE_MODE == "OPEN" || DeviceUser.DEVICE_MODE == "ACTIVE")
                {
                    instLbl.Text = "Scan Kanban to Fill, Scan Case No to Close";
                    prepare_complete(true);
                }
                else if (DeviceUser.DEVICE_MODE == "PREP")
                {
                    instLbl.Text = "Please Scan Destination";
                }
                else if (DeviceUser.DEVICE_MODE == "FILL")
                {
                    if (DeviceUser.FILL_MODE == "KANBAN")
                    {
                        instLbl.Text = "Please Scan Part";
                        prepare_complete(false);
                    }
                    else
                    {
                        instLbl.Text = "Scan Kanban to Fill, Scan Case No to Close";
                        prepare_complete(true);
                    }
                }
                else
                {
                    instLbl.Text = "Error";
                }
            }
            if (db.using_thread)
            {
                new Thread(CheckConnectionThread).Start();
            }
        }

        public void ChkConPart(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChkConPart), new object[] { value });
                return;
            }
            if (pageActive && (value == "OFFLINE"))
            {
                pageActive = false;
                OffLineInfoForm ph = new OffLineInfoForm();
                this.Hide();
                ph.ShowDialog();
                this.Show();
                pageActive = true;
            }
        }

        void CheckConnectionThread()
        {
            while (true)
            {
                string v = "OFFLINE";
                if (db.IsConnect())
                {
                    v = "ONLINE";
                }
                ChkConPart(v);
                Thread.Sleep(2000);
            }
        }

        private void PackingProcessForm_Load(object sender, EventArgs e)
        {
            loginTxt.Text = DeviceUser.USER_LOGIN;
            TransportCdTxt.Text = DeviceUser.TRANSPORT_CD_DESC;
            //LineCodeLbl.Text = "Line Code:" + DeviceUser.PLANT_LINE_CD;
        }

        private void receivingBtn_Click(object sender, EventArgs e)
        {

        }

        private void clearForm()
        {
            DataTable tgl = db.GetDataTable("SELECT CONVERT(VARCHAR(10), GETDATE(), 120) + ' ' + CONVERT(VARCHAR(8), GETDATE(), 108) DT");
            caseTypeLbl.Text = "";
            kanbanIdTxt.Text = "";
            caseNoLbl.Text = "";
            destinationLbl.Text = "";
            transportationLbl.Text = "";
            privilageLbl.Text = "";
            handlingTypeLbl.Text = "";
            qtyLbl.Text = "";
            //preparedDtTxt.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); using server date
            preparedDtTxt.Text = tgl.Rows[0]["DT"].ToString();
            CrippleBtn.Visible = false;
            prevModeBtn.Visible = false;
            homeBtn.Visible = true;
            modeLbl.Text = DeviceUser.DEVICE_MODE;
            if (DeviceUser.TRANSPORT_CD == "1") //air
            {
                instLbl.Text = "Please Scan Kanban";
            }
            else if (DeviceUser.TRANSPORT_CD == "2") //sea
            {
                instLbl.Text = "Please Scan Case Type";
            }
            
        }

        private void homeBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (db.IsConnect())
                {
                    List<DbParam> param = new List<DbParam>();
                    param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                    param.Add(new DbParam("@PREPARED_DT", "Text", preparedDtTxt.Text));
                    param.Add(new DbParam("@CASE_NO", "Text", caseNoLbl.Text));
                    param.Add(new DbParam("@CASE_TYPE", "Text", caseTypeLbl.Text));
                    db.ExecuteProcedure("[spex].SP_DEVICE_PACKING_HOME_PROCESS", param);
                    pageActive = false;
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Can not contact server for process. Check network connection");
                }
            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void receiveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@MANIFEST_NO", "Text", inputTxt.Text));
                db.ExecuteProcedure("[spex].SP_DEVICE_MANIFEST_RECEIVING_PROCESS", param);
                clearForm();
                inputTxt.Text = "";
                MessageBox.Show("Success");
            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        }

        private void inputTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                // Start : Add by FID.Andri - 2018/08/14
                CheckPassword cp = new CheckPassword();
                // End : Add by FID.Andri - 2018/08/14
                if (is_activate)
                {
                    int l = inputTxt.Text.Length;
                    if (DeviceUser.SETTING_CASE_LABEL_CHECK_DIGIT == "Y")
                    {
                        int m = Int32.Parse(DeviceUser.SETTING_CASE_LABEL_MAX_STR);
                        if (l > m)
                        {
                            l = m;
                        }
                        inputTxt.Text = inputTxt.Text.Substring(0, l);
                    }

                    if (inputTxt.Text == caseNoLbl.Text)
                    {
                        List<DbParam> param = new List<DbParam>();
                        param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                        param.Add(new DbParam("@PREPARED_DT", "Text", preparedDtTxt.Text));
                        param.Add(new DbParam("@CASE_NO", "Text", caseNoLbl.Text));
                        param.Add(new DbParam("@CASE_TYPE", "Text", caseTypeLbl.Text.Substring(0, 2)));
                        param.Add(new DbParam("@LAST_STATUS", "Text", last_status));

                        DataTable o = db.GetDataTable("[spex].SP_DEVICE_PACKING_ACTIVATED_PROCESS", param);
                        if (o.Rows[0]["TEXT"].ToString() == "Success")
                        {
                            DeviceUser.DEVICE_MODE = o.Rows[0]["DEVICE_MODE"].ToString();
                            DeviceUser.FILL_MODE = o.Rows[0]["DEVICE_FILL_MODE"].ToString();
                            is_activate = false;
                            homeBtn.Visible = false;
                            modeLbl.Text = DeviceUser.DEVICE_MODE + " " + DeviceUser.FILL_MODE;
                            if (DeviceUser.DEVICE_MODE == "FILL" || DeviceUser.DEVICE_MODE == "OPEN" || DeviceUser.DEVICE_MODE == "ACTIVE")
                            {
                                instLbl.Text = "Scan Kanban to Fill, Scan Case No to Close";
                                prepare_complete(true);
                            }
                            else if (DeviceUser.DEVICE_MODE == "PREP")
                            {
                                instLbl.Text = "Please Scan Destination";
                            }
                        }
                        else
                        {
                            MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                        }
                        inputTxt.Text = "";
                    }
                    else
                    {
                        MessageBox.Show("CaseNo yang di scan berbeda dengan caseno yang dipilih");
                    }
                    
                }
                else
                {
                    if (DeviceUser.TRANSPORT_CD == "1") //air
                    {
                        if (DeviceUser.DEVICE_MODE == "NEW")
                        {
                            try
                            {
                                List<DbParam> param = new List<DbParam>();
                                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                                param.Add(new DbParam("@KANBAN_ID", "Text", inputTxt.Text));
                                param.Add(new DbParam("@PACKING_COMPANY", "Text", DeviceUser.PACKING_COMPANY));
                                param.Add(new DbParam("@COMPANY_PLANT_LINE_CODE", "Text", DeviceUser.COMPANY_PLANT_CD));
                                param.Add(new DbParam("@PLANT_LINE_CD", "Text", DeviceUser.PLANT_LINE_CD));
                                param.Add(new DbParam("@PREPARED_DT", "Text", preparedDtTxt.Text));
                                param.Add(new DbParam("@PRINTER", "Text", dl.GetSysData("printer")));

                                DataTable o = db.GetDataTable("[spex].SP_DEVICE_PACKING_SCAN_KANBAN_INIT_PROCESS", param);
                                if (o.Rows[0]["TEXT"].ToString() == "Success")
                                {
                                    kanbanIdTxt.Text = inputTxt.Text;
                                    transportationLbl.Text = "1 - AIR";
                                    inputTxt.Text = "";
                                    caseTypeLbl.Text = "##";
                                    caseNoLbl.Text = o.Rows[0]["CASE_NO"].ToString();
                                    destinationLbl.Text = o.Rows[0]["DESTINATION"].ToString();
                                    handlingTypeLbl.Text = o.Rows[0]["DG_FLAG"].ToString();
                                    privilageLbl.Text = o.Rows[0]["PREVILEGE"].ToString();
                                    homeBtn.Visible = false;
                                    DeviceUser.DEVICE_MODE = "OPEN";
                                    modeLbl.Text = DeviceUser.DEVICE_MODE;
                                    instLbl.Text = "Please Scan Kanban  Or Case No";
                                    prepare_complete(true);
                                }
                                else
                                {
                                    MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                                    inputTxt.Text = "";
                                    if (DeviceUser.SETTING_SHOW_ERR_CLS_PWD == "Y")
                                    {
                                        // Start : Add by FID.Andri - 2018/08/14
                                        cp.ShowDialog();
                                        // End : Add by FID.Andri - 2018/08/14
                                    }
                                }
                            }
                            catch (SqlException se)
                            {
                                string errorMessages = "";
                                for (int i = 0; i < se.Errors.Count; i++)
                                {
                                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                                        "Message: " + se.Errors[i].Message + "\n" +
                                        "Error Number: " + se.Errors[i].Number + "\n" +
                                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                                        "Source: " + se.Errors[i].Source + "\n" +
                                        "Procedure: " + se.Errors[i].Procedure + "\n");
                                }
                                MessageBox.Show("SQL Error: " + errorMessages);
                            }
                            catch (Exception exc)
                            {
                                MessageBox.Show("Error: " + exc.Message);
                            }
                        }

                        else if (DeviceUser.DEVICE_MODE == "OPEN" || DeviceUser.DEVICE_MODE == "ACTIVE" || (DeviceUser.DEVICE_MODE == "FILL" && DeviceUser.FILL_MODE == "PART"))
                        {
                            process_3();
                        }
                        else if (DeviceUser.DEVICE_MODE == "FILL" && DeviceUser.FILL_MODE == "KANBAN")
                        {
                            int l = inputTxt.Text.Length;
                            if (DeviceUser.SETTING_PART_LABEL_CHECK_DIGIT == "Y")
                            {
                                l = l - 1;
                            }
                            inputTxt.Text = inputTxt.Text.Substring(0, l).TrimEnd();
                            process_4_scan_part(inputTxt.Text);
                        }
                    }
                    else if (DeviceUser.TRANSPORT_CD == "2") //sea
                    {
                        if (DeviceUser.DEVICE_MODE == "NEW")
                        {
                            try
                            {
                                List<DbParam> param = new List<DbParam>();
                                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                                param.Add(new DbParam("@CASE_TYPE", "Text", inputTxt.Text));
                                param.Add(new DbParam("@PACKING_COMPANY", "Text", DeviceUser.PACKING_COMPANY));
                                param.Add(new DbParam("@COMPANY_PLANT_LINE_CODE", "Text", DeviceUser.COMPANY_PLANT_CD));
                                param.Add(new DbParam("@PLANT_LINE_CD", "Text", DeviceUser.PLANT_LINE_CD));
                                param.Add(new DbParam("@PREPARED_DT", "Text", preparedDtTxt.Text));

                                DataTable o = db.GetDataTable("[spex].SP_DEVICE_PACKING_SCAN_CASE_TYPE_PROCESS", param);
                                if (o.Rows[0]["TEXT"].ToString() == "Success")
                                {
                                    caseTypeLbl.Text = inputTxt.Text;
                                    DeviceUser.DEVICE_MODE = "PREP";
                                    modeLbl.Text = DeviceUser.DEVICE_MODE;
                                    homeBtn.Visible = false;
                                    inputTxt.Text = "";
                                    instLbl.Text = "Please Scan Destination";
                                }
                                else
                                {
                                    MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                                    inputTxt.Text = "";
                                }
                            }
                            catch (SqlException se)
                            {
                                string errorMessages = "";
                                for (int i = 0; i < se.Errors.Count; i++)
                                {
                                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                                        "Message: " + se.Errors[i].Message + "\n" +
                                        "Error Number: " + se.Errors[i].Number + "\n" +
                                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                                        "Source: " + se.Errors[i].Source + "\n" +
                                        "Procedure: " + se.Errors[i].Procedure + "\n");
                                }
                                MessageBox.Show("SQL Error: " + errorMessages);
                            }
                            catch (Exception exc)
                            {
                                MessageBox.Show("Error: " + exc.Message);
                            }
                        }
                        else if (DeviceUser.DEVICE_MODE == "PREP")
                        {
                            try
                            {
                                List<DbParam> param = new List<DbParam>();
                                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                                param.Add(new DbParam("@INPUT", "Text", inputTxt.Text));
                                param.Add(new DbParam("@PREPARED_DT", "Text", preparedDtTxt.Text));
                                param.Add(new DbParam("@PRINTER", "Text", dl.GetSysData("printer")));

                                DataTable o = db.GetDataTable("[spex].SP_DEVICE_PACKING_SCAN_DESTINATION_PROCESS", param);
                                if (o.Rows[0]["TEXT"].ToString() == "Success")
                                {
                                    destinationLbl.Text = inputTxt.Text.Substring(0, 7);
                                    transportationLbl.Text = "2 - SEA";
                                    caseNoLbl.Text = o.Rows[0]["CASE_NO"].ToString();
                                    DeviceUser.DEVICE_MODE = "OPEN";
                                    modeLbl.Text = DeviceUser.DEVICE_MODE;
                                    inputTxt.Text = "";
                                    instLbl.Text = "Please Scan Kanban  Or Case No";
                                    prepare_complete(true);
                                }
                                else
                                {
                                    MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                                    inputTxt.Text = "";
                                }
                            }
                            catch (SqlException se)
                            {
                                string errorMessages = "";
                                for (int i = 0; i < se.Errors.Count; i++)
                                {
                                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                                        "Message: " + se.Errors[i].Message + "\n" +
                                        "Error Number: " + se.Errors[i].Number + "\n" +
                                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                                        "Source: " + se.Errors[i].Source + "\n" +
                                        "Procedure: " + se.Errors[i].Procedure + "\n");
                                }
                                MessageBox.Show("SQL Error: " + errorMessages);
                            }
                            catch (Exception exc)
                            {
                                MessageBox.Show("Error: " + exc.Message);
                            }
                        }
                        else if (DeviceUser.DEVICE_MODE == "OPEN" || DeviceUser.DEVICE_MODE == "ACTIVE" || (DeviceUser.DEVICE_MODE == "FILL" && DeviceUser.FILL_MODE == "PART"))
                        {
                            process_3();
                        }
                        else if (DeviceUser.DEVICE_MODE == "FILL" && DeviceUser.FILL_MODE == "KANBAN")
                        {
                            int l = inputTxt.Text.Length;
                            if (DeviceUser.SETTING_PART_LABEL_CHECK_DIGIT == "Y")
                            {
                                l = l - 1;
                            }
                            inputTxt.Text = inputTxt.Text.Substring(0, l).TrimEnd();
                            process_4_scan_part(inputTxt.Text);
                        }
                    }
                }
            }
        }

        private void process_3()
        {
            if (DeviceUser.SETTING_CASE_LABEL_CHECK_DIGIT == "Y")
            {
                int l = inputTxt.Text.Length;
                int m = Int32.Parse(DeviceUser.SETTING_CASE_LABEL_MAX_STR);

                if (l > m)
                {
                    l = m;
                }

                if (inputTxt.Text.Substring(0, l) == caseNoLbl.Text)
                {
                    //finish
                    inputTxt.Text = caseNoLbl.Text;
                    process_finish();
                }
                else
                {
                    process_3_scan_kanban();
                }
            }
            else
            {
                if (inputTxt.Text == caseNoLbl.Text)
                {
                    //finish
                    process_finish();
                }
                else
                {
                    process_3_scan_kanban();
                }
            }
        }

        private void process_4_scan_part(string part_no)
        {
            // Start : Add by FID.Andri - 2018/08/14
            CheckPassword cp = new CheckPassword();
            // End : Add by FID.Andri - 2018/08/14

            try
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@PART_NO", "Text", part_no));
                param.Add(new DbParam("@PREPARED_DT", "Text", preparedDtTxt.Text));
                param.Add(new DbParam("@KANBAN_ID", "Text", kanbanIdTxt.Text));
                param.Add(new DbParam("@CASE_NO", "Text", caseNoLbl.Text));
                param.Add(new DbParam("@CASE_TYPE", "Text", caseTypeLbl.Text.Substring(0, 2)));

                DataTable o = db.GetDataTable("[spex].SP_DEVICE_PACKING_SCAN_PART_PROCESS", param);
                if (o.Rows[0]["TEXT"].ToString() == "Success")
                {
                    kanbanIdTxt.Text = "";//inputTxt.Text;
                    DeviceUser.DEVICE_MODE = "FILL";
                    DeviceUser.FILL_MODE = "PART";
                    modeLbl.Text = DeviceUser.DEVICE_MODE + " " + DeviceUser.FILL_MODE;
                    instLbl.Text = "Scan Kanban to Fill, Scan Case No to Close";

                    inputTxt.Text = "";
                    prepare_complete(true);
                }
                else
                {
                    MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                    inputTxt.Text = "";
                    if (DeviceUser.SETTING_SHOW_ERR_CLS_PWD == "Y")
                    {
                        // Start : Add by FID.Andri - 2018/08/14
                        cp.ShowDialog();
                        // End : Add by FID.Andri - 2018/08/14
                    }
                }
            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        }

        private void process_3_scan_kanban()
        {
            // Start : Add by FID.Andri - 2018/08/14
            CheckPassword cp = new CheckPassword();
            // End : Add by FID.Andri - 2018/08/14

            try
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@KANBAN_ID", "Text", inputTxt.Text));
                param.Add(new DbParam("@PREPARED_DT", "Text", preparedDtTxt.Text));
                param.Add(new DbParam("@DESTINATION", "Text", destinationLbl.Text));
                param.Add(new DbParam("@CASE_TYPE", "Text", caseTypeLbl.Text.Substring(0,2)));
                param.Add(new DbParam("@TRANSP_CD", "Text", DeviceUser.TRANSPORT_CD));
                param.Add(new DbParam("@CASE_NO", "Text", caseNoLbl.Text));
                param.Add(new DbParam("@PACKING_COMPANY", "Text", DeviceUser.PACKING_COMPANY));
                param.Add(new DbParam("@PRIV", "Text", privilageLbl.Text));

                DataTable o = db.GetDataTable("[spex].SP_DEVICE_PACKING_SCAN_KANBAN_PROCESS", param);
                if (o.Rows[0]["TEXT"].ToString() == "Success")
                {
                    privilageLbl.Text = o.Rows[0]["PRIVILEGE"].ToString();
                    handlingTypeLbl.Text = o.Rows[0]["DG_FLAG"].ToString();
                    DeviceUser.DEVICE_MODE = "FILL";

                    if (DeviceUser.PLANT_LINE_CD == "R") //automate scan part
                    {
                        kanbanIdTxt.Text = "";//inputTxt.Text;
                        DeviceUser.FILL_MODE = "PART";
                        modeLbl.Text = DeviceUser.DEVICE_MODE + " " + DeviceUser.FILL_MODE;
                        instLbl.Text = "Scan Kanban to Fill, Scan Case No to Close";
                        inputTxt.Text = "";
                        prepare_complete(true);
                        //process_4_scan_part(o.Rows[0]["PART_NO"].ToString());
                    }
                    else
                    {
                        kanbanIdTxt.Text = inputTxt.Text;
                        DeviceUser.FILL_MODE = "KANBAN";
                        modeLbl.Text = DeviceUser.DEVICE_MODE + " " + DeviceUser.FILL_MODE;
                        instLbl.Text = "Please Scan Part";
                        inputTxt.Text = "";
                        prepare_complete(false);
                    }
                }
                else
                {
                    MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                    inputTxt.Text = "";
                    if (DeviceUser.SETTING_SHOW_ERR_CLS_PWD == "Y")
                    {
                        // Start : Add by FID.Andri - 2018/08/14
                        cp.ShowDialog();
                        // End : Add by FID.Andri - 2018/08/14
                    }
                }
            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        }

        private void process_finish()
        {
            if (DeviceUser.TRANSPORT_CD == "1")
            {
                this.Hide();
                pageActive = false;
                DimensionForm f = new DimensionForm();
                f.PreparedDate = preparedDtTxt.Text;
                f.CaseNo = caseNoLbl.Text;
                f.CaseType = caseTypeLbl.Text.Substring(0, 2);
                f.ShowDialog();
                this.Show();
                pageActive = true;

                if (f.SaveSuccess)
                {
                    do_finish();
                }
                else
                {
                    inputTxt.Text = "";
                }
            }
            else if (DeviceUser.TRANSPORT_CD == "2")
            {
                var confirmResult = MessageBox.Show("Are you sure?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                if (confirmResult == DialogResult.Yes)
                {
                    do_finish();
                }
                else
                {
                    inputTxt.Text = "";
                }
            }
        }

        private void do_finish()
        {
            // Start : Add by FID.Andri - 2018/08/14
            CheckPassword cp = new CheckPassword();
            // End : Add by FID.Andri - 2018/08/14

            try
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@PREPARED_DT", "Text", preparedDtTxt.Text));
                param.Add(new DbParam("@CASE_NO", "Text", caseNoLbl.Text));
                param.Add(new DbParam("@CASE_TYPE", "Text", caseTypeLbl.Text.Substring(0, 2)));
                param.Add(new DbParam("@DF", "Text", handlingTypeLbl.Text));
                param.Add(new DbParam("@PACKING_COMPANY", "Text", DeviceUser.PACKING_COMPANY));

                DataTable o = db.GetDataTable("[spex].SP_DEVICE_PACKING_FINISH_PROCESS", param);
                if (o.Rows[0]["TEXT"].ToString() == "Success")
                {
                    inputTxt.Text = "";
                    DeviceUser.DEVICE_MODE = "NEW";
                    DeviceUser.FILL_MODE = "";
                    clearForm();
                    MessageBox.Show("Case Closed Successfully");
                }
                else
                {
                    MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                    inputTxt.Text = "";
                }

            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        }


        private void prepare_complete(bool isComplete)
        {
            CrippleBtn.Visible = isComplete;
            prevModeBtn.Visible = !isComplete && DeviceUser.PLANT_LINE_CD=="P";

            if (isComplete)//calculate qty
            {

                // Start : Add by FID.Muhajir - 2018/12/20
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@CASE_NO", "Text", caseNoLbl.Text));
                param.Add(new DbParam("@CASE_TYPE", "Text", caseTypeLbl.Text.Substring(0, 2)));
                param.Add(new DbParam("@PREPARED_DT", "Text", preparedDtTxt.Text));

                DataTable t = db.GetDataTable("[spex].SP_DEVICE_PACKING_CALCULATE_QTY", param);
                /*string q = @"select COUNT(1) J from [spex].[TB_R_PACKING_PART] 
                            WHERE CASE_NO='"+caseNoLbl.Text+"' AND CASE_TYPE='"+caseTypeLbl.Text.Substring(0,2)+"' AND PREPARED_DT='"+preparedDtTxt.Text+"'";
                            DataTable t = db.GetDataTable(q);*/
                // End : Add by FID.Muhajir - 2018/12/20

                if (t != null && t.Rows.Count > 0)
                {
                    qtyLbl.Text = t.Rows[0]["J"].ToString();
                }
            }
        }

        private void CrippleBtn_Click(object sender, EventArgs e)
        {
             var confirmResult = MessageBox.Show("Are you sure?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
             if (confirmResult == DialogResult.Yes)
             {
                 try
                 {
                     List<DbParam> param = new List<DbParam>();
                     param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                     param.Add(new DbParam("@PREPARED_DT", "Text", preparedDtTxt.Text));
                     param.Add(new DbParam("@CASE_NO", "Text", caseNoLbl.Text));
                     param.Add(new DbParam("@CASE_TYPE", "Text", caseTypeLbl.Text.Substring(0, 2)));

                     DataTable o = db.GetDataTable("[spex].SP_DEVICE_PACKING_CRIPPLE_PROCESS", param);
                     if (o.Rows[0]["TEXT"].ToString() == "Success")
                     {
                         inputTxt.Text = "";
                         DeviceUser.DEVICE_MODE = "NEW";
                         DeviceUser.FILL_MODE = "";
                         clearForm();
                         MessageBox.Show("Case Crippled Successfully");
                     }
                     else
                     {
                         MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                         inputTxt.Text = "";
                     }

                 }
                 catch (SqlException se)
                 {
                     string errorMessages = "";
                     for (int i = 0; i < se.Errors.Count; i++)
                     {
                         errorMessages = errorMessages + ("Index #" + i + "\n" +
                             "Message: " + se.Errors[i].Message + "\n" +
                             "Error Number: " + se.Errors[i].Number + "\n" +
                             "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                             "Source: " + se.Errors[i].Source + "\n" +
                             "Procedure: " + se.Errors[i].Procedure + "\n");
                     }
                     MessageBox.Show("SQL Error: " + errorMessages);
                 }
                 catch (Exception exc)
                 {
                     MessageBox.Show("Error: " + exc.Message);
                 }
             }
        }

        private void prevModeBtn_Click(object sender, EventArgs e)
        {
            if (DeviceUser.POSTION_CD == "OP")
            {
                pageActive = false;
                this.Hide();
                Login f = new Login();
                f.ShowDialog();
                this.Show();
                pageActive = true;
                if (f.SuccessLogin)
                {
                    process_prev_mode();
                }
            }
            else
            {
                process_prev_mode();
            }
        }

        private void process_prev_mode()
        {
            try
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@PREPARED_DT", "Text", preparedDtTxt.Text));
                param.Add(new DbParam("@CASE_NO", "Text", caseNoLbl.Text));
                param.Add(new DbParam("@CASE_TYPE", "Text", caseTypeLbl.Text.Substring(0,2)));
                param.Add(new DbParam("@KANBAN_ID", "Text", kanbanIdTxt.Text));

                DataTable o = db.GetDataTable("[spex].SP_DEVICE_PACKING_PREV_MODE_PROCESS", param);
                if (o.Rows[0]["TEXT"].ToString() == "Success")
                {
                    inputTxt.Text = "";
                    DeviceUser.DEVICE_MODE = o.Rows[0]["DEVICE_MODE"].ToString();
                    DeviceUser.FILL_MODE = o.Rows[0]["DEVICE_FILL_MODE"].ToString(); ;
                    modeLbl.Text = DeviceUser.DEVICE_MODE + " " + DeviceUser.FILL_MODE; ;
                    kanbanIdTxt.Text = "";
                    if (DeviceUser.DEVICE_MODE == "FILL" || DeviceUser.DEVICE_MODE == "OPEN")
                    {
                        instLbl.Text = "Scan Kanban to Fill, Scan Case No to Close";
                        prepare_complete(true);
                    }
                    MessageBox.Show("Prev Mode Success");
                }
                else
                {
                    MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                    inputTxt.Text = "";
                }

            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        }
    }
}