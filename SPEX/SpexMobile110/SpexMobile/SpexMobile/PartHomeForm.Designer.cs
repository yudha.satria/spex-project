﻿namespace SpexMobile
{
    partial class PartHomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LineCodeLbl = new System.Windows.Forms.Label();
            this.binningBtn = new System.Windows.Forms.Button();
            this.pickingBtn = new System.Windows.Forms.Button();
            this.TransportCdTxt = new System.Windows.Forms.Label();
            this.loginTxt = new System.Windows.Forms.Label();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LineCodeLbl
            // 
            this.LineCodeLbl.Location = new System.Drawing.Point(73, 14);
            this.LineCodeLbl.Name = "LineCodeLbl";
            this.LineCodeLbl.Size = new System.Drawing.Size(143, 20);
            this.LineCodeLbl.Text = "Line Code :  P - Packing";
            this.LineCodeLbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // binningBtn
            // 
            this.binningBtn.Location = new System.Drawing.Point(29, 41);
            this.binningBtn.Name = "binningBtn";
            this.binningBtn.Size = new System.Drawing.Size(180, 29);
            this.binningBtn.TabIndex = 2;
            this.binningBtn.Text = "Binning";
            this.binningBtn.Click += new System.EventHandler(this.binningBtn_Click);
            // 
            // pickingBtn
            // 
            this.pickingBtn.Location = new System.Drawing.Point(29, 80);
            this.pickingBtn.Name = "pickingBtn";
            this.pickingBtn.Size = new System.Drawing.Size(180, 29);
            this.pickingBtn.TabIndex = 3;
            this.pickingBtn.Text = "Picking";
            this.pickingBtn.Click += new System.EventHandler(this.pickingBtn_Click);
            // 
            // TransportCdTxt
            // 
            this.TransportCdTxt.Location = new System.Drawing.Point(164, 184);
            this.TransportCdTxt.Name = "TransportCdTxt";
            this.TransportCdTxt.Size = new System.Drawing.Size(60, 20);
            this.TransportCdTxt.Text = "label1";
            this.TransportCdTxt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // loginTxt
            // 
            this.loginTxt.Location = new System.Drawing.Point(92, 184);
            this.loginTxt.Name = "loginTxt";
            this.loginTxt.Size = new System.Drawing.Size(66, 20);
            this.loginTxt.Text = "L";
            this.loginTxt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(152, 146);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(72, 20);
            this.logoutBtn.TabIndex = 9;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 184);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.Text = "Mode : N/A";
            // 
            // PartHomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.TransportCdTxt);
            this.Controls.Add(this.loginTxt);
            this.Controls.Add(this.logoutBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pickingBtn);
            this.Controls.Add(this.binningBtn);
            this.Controls.Add(this.LineCodeLbl);
            this.Name = "PartHomeForm";
            this.Text = "Part Home Form";
            this.Load += new System.EventHandler(this.PartHomeForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LineCodeLbl;
        private System.Windows.Forms.Button binningBtn;
        private System.Windows.Forms.Button pickingBtn;
        private System.Windows.Forms.Label TransportCdTxt;
        private System.Windows.Forms.Label loginTxt;
        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Label label2;
    }
}