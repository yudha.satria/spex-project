﻿namespace SpexMobile
{
    partial class VanningProcessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.instLbl = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.barcodeLbl = new System.Windows.Forms.Label();
            this.vanningLbl = new System.Windows.Forms.Label();
            this.destinationLbl = new System.Windows.Forms.Label();
            this.sealnoLbl = new System.Windows.Forms.Label();
            this.tareLbl = new System.Windows.Forms.Label();
            this.containerLbl = new System.Windows.Forms.Label();
            this.caseQtyLbl = new System.Windows.Forms.Label();
            this.homeBtn = new System.Windows.Forms.Button();
            this.finishBtn = new System.Windows.Forms.Button();
            this.modeLbl = new System.Windows.Forms.Label();
            this.TransportCdTxt = new System.Windows.Forms.Label();
            this.loginTxt = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.packCompLbl = new System.Windows.Forms.Label();
            this.plantCodeLbl = new System.Windows.Forms.Label();
            this.ipLbl = new System.Windows.Forms.Label();
            this.lineCodeLbl = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // inputTxt
            // 
            this.inputTxt.Location = new System.Drawing.Point(108, 26);
            this.inputTxt.Name = "inputTxt";
            this.inputTxt.Size = new System.Drawing.Size(116, 21);
            this.inputTxt.TabIndex = 19;
            this.inputTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.inputTxt_KeyPress_1);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 20);
            this.label1.Text = "Input";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(92, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 20);
            this.label8.Text = ":";
            // 
            // instLbl
            // 
            this.instLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.instLbl.Location = new System.Drawing.Point(3, 9);
            this.instLbl.Name = "instLbl";
            this.instLbl.Size = new System.Drawing.Size(234, 13);
            this.instLbl.Text = "Please Scan Case Type";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(3, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 20);
            this.label3.Text = "Barcode";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(3, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 20);
            this.label2.Text = "Vanning Label";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(3, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 20);
            this.label4.Text = "Destination";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(3, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 20);
            this.label5.Text = "Seal No";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(3, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 20);
            this.label6.Text = "Container No";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label7.Location = new System.Drawing.Point(3, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 20);
            this.label7.Text = "Tare";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label9.Location = new System.Drawing.Point(3, 175);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 20);
            this.label9.Text = "Case Qty";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label10.Location = new System.Drawing.Point(92, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 20);
            this.label10.Text = ":";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label11.Location = new System.Drawing.Point(92, 75);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(10, 20);
            this.label11.Text = ":";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label12.Location = new System.Drawing.Point(92, 95);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 20);
            this.label12.Text = ":";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label13.Location = new System.Drawing.Point(92, 115);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(10, 20);
            this.label13.Text = ":";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label14.Location = new System.Drawing.Point(92, 135);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(10, 20);
            this.label14.Text = ":";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label15.Location = new System.Drawing.Point(92, 155);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(10, 20);
            this.label15.Text = ":";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label16.Location = new System.Drawing.Point(92, 175);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(10, 20);
            this.label16.Text = ":";
            // 
            // barcodeLbl
            // 
            this.barcodeLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.barcodeLbl.Location = new System.Drawing.Point(108, 55);
            this.barcodeLbl.Name = "barcodeLbl";
            this.barcodeLbl.Size = new System.Drawing.Size(116, 20);
            this.barcodeLbl.Text = "DUMMY";
            // 
            // vanningLbl
            // 
            this.vanningLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.vanningLbl.Location = new System.Drawing.Point(108, 75);
            this.vanningLbl.Name = "vanningLbl";
            this.vanningLbl.Size = new System.Drawing.Size(116, 20);
            this.vanningLbl.Text = "DUMMY";
            // 
            // destinationLbl
            // 
            this.destinationLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.destinationLbl.Location = new System.Drawing.Point(108, 95);
            this.destinationLbl.Name = "destinationLbl";
            this.destinationLbl.Size = new System.Drawing.Size(116, 20);
            this.destinationLbl.Text = "DUMMY";
            // 
            // sealnoLbl
            // 
            this.sealnoLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.sealnoLbl.Location = new System.Drawing.Point(108, 115);
            this.sealnoLbl.Name = "sealnoLbl";
            this.sealnoLbl.Size = new System.Drawing.Size(116, 20);
            this.sealnoLbl.Text = "DUMMY";
            // 
            // tareLbl
            // 
            this.tareLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.tareLbl.Location = new System.Drawing.Point(108, 155);
            this.tareLbl.Name = "tareLbl";
            this.tareLbl.Size = new System.Drawing.Size(116, 20);
            this.tareLbl.Text = "DUMMY";
            // 
            // containerLbl
            // 
            this.containerLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.containerLbl.Location = new System.Drawing.Point(108, 135);
            this.containerLbl.Name = "containerLbl";
            this.containerLbl.Size = new System.Drawing.Size(116, 20);
            this.containerLbl.Text = "DUMMY";
            // 
            // caseQtyLbl
            // 
            this.caseQtyLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.caseQtyLbl.Location = new System.Drawing.Point(108, 175);
            this.caseQtyLbl.Name = "caseQtyLbl";
            this.caseQtyLbl.Size = new System.Drawing.Size(116, 20);
            this.caseQtyLbl.Text = "DUMMY";
            // 
            // homeBtn
            // 
            this.homeBtn.Location = new System.Drawing.Point(4, 199);
            this.homeBtn.Name = "homeBtn";
            this.homeBtn.Size = new System.Drawing.Size(72, 20);
            this.homeBtn.TabIndex = 65;
            this.homeBtn.Text = "Home";
            this.homeBtn.Click += new System.EventHandler(this.homeBtn_Click);
            // 
            // finishBtn
            // 
            this.finishBtn.Location = new System.Drawing.Point(82, 199);
            this.finishBtn.Name = "finishBtn";
            this.finishBtn.Size = new System.Drawing.Size(72, 20);
            this.finishBtn.TabIndex = 66;
            this.finishBtn.Text = "Finish";
            this.finishBtn.Click += new System.EventHandler(this.finishBtn_Click);
            // 
            // modeLbl
            // 
            this.modeLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.modeLbl.Location = new System.Drawing.Point(43, 231);
            this.modeLbl.Name = "modeLbl";
            this.modeLbl.Size = new System.Drawing.Size(50, 20);
            this.modeLbl.Text = "New";
            // 
            // TransportCdTxt
            // 
            this.TransportCdTxt.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.TransportCdTxt.Location = new System.Drawing.Point(154, 231);
            this.TransportCdTxt.Name = "TransportCdTxt";
            this.TransportCdTxt.Size = new System.Drawing.Size(60, 20);
            this.TransportCdTxt.Text = "label1";
            this.TransportCdTxt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // loginTxt
            // 
            this.loginTxt.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.loginTxt.Location = new System.Drawing.Point(82, 231);
            this.loginTxt.Name = "loginTxt";
            this.loginTxt.Size = new System.Drawing.Size(66, 20);
            this.loginTxt.Text = "L";
            this.loginTxt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label17.Location = new System.Drawing.Point(4, 231);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 20);
            this.label17.Text = "Mode :";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label18.Location = new System.Drawing.Point(4, 248);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 20);
            this.label18.Text = "Pack Comp :";
            // 
            // packCompLbl
            // 
            this.packCompLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.packCompLbl.Location = new System.Drawing.Point(73, 248);
            this.packCompLbl.Name = "packCompLbl";
            this.packCompLbl.Size = new System.Drawing.Size(54, 20);
            this.packCompLbl.Text = "New";
            // 
            // plantCodeLbl
            // 
            this.plantCodeLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.plantCodeLbl.Location = new System.Drawing.Point(108, 248);
            this.plantCodeLbl.Name = "plantCodeLbl";
            this.plantCodeLbl.Size = new System.Drawing.Size(106, 20);
            this.plantCodeLbl.Text = "Plant Code: ";
            this.plantCodeLbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ipLbl
            // 
            this.ipLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.ipLbl.Location = new System.Drawing.Point(108, 268);
            this.ipLbl.Name = "ipLbl";
            this.ipLbl.Size = new System.Drawing.Size(106, 20);
            this.ipLbl.Text = "ip";
            this.ipLbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lineCodeLbl
            // 
            this.lineCodeLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lineCodeLbl.Location = new System.Drawing.Point(73, 268);
            this.lineCodeLbl.Name = "lineCodeLbl";
            this.lineCodeLbl.Size = new System.Drawing.Size(54, 20);
            this.lineCodeLbl.Text = "Vanning";
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label20.Location = new System.Drawing.Point(4, 268);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 20);
            this.label20.Text = "Line Code :";
            // 
            // VanningProcessForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.lineCodeLbl);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.ipLbl);
            this.Controls.Add(this.plantCodeLbl);
            this.Controls.Add(this.packCompLbl);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.modeLbl);
            this.Controls.Add(this.TransportCdTxt);
            this.Controls.Add(this.loginTxt);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.finishBtn);
            this.Controls.Add(this.homeBtn);
            this.Controls.Add(this.caseQtyLbl);
            this.Controls.Add(this.containerLbl);
            this.Controls.Add(this.tareLbl);
            this.Controls.Add(this.sealnoLbl);
            this.Controls.Add(this.destinationLbl);
            this.Controls.Add(this.vanningLbl);
            this.Controls.Add(this.barcodeLbl);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.inputTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.instLbl);
            this.Name = "VanningProcessForm";
            this.Text = "VanningProcessForm";
            this.Load += new System.EventHandler(this.VanningProcessForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox inputTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label instLbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label barcodeLbl;
        private System.Windows.Forms.Label vanningLbl;
        private System.Windows.Forms.Label destinationLbl;
        private System.Windows.Forms.Label sealnoLbl;
        private System.Windows.Forms.Label tareLbl;
        private System.Windows.Forms.Label containerLbl;
        private System.Windows.Forms.Label caseQtyLbl;
        private System.Windows.Forms.Button homeBtn;
        private System.Windows.Forms.Button finishBtn;
        private System.Windows.Forms.Label modeLbl;
        private System.Windows.Forms.Label TransportCdTxt;
        private System.Windows.Forms.Label loginTxt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label packCompLbl;
        private System.Windows.Forms.Label plantCodeLbl;
        private System.Windows.Forms.Label ipLbl;
        private System.Windows.Forms.Label lineCodeLbl;
        private System.Windows.Forms.Label label20;
    }
}