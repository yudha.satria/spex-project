﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;
using System.Threading;
using System.Data.SqlClient;

namespace SpexMobile
{
    public partial class PackingHomeForm : Form
    {
        bool pageActive = true;
        DatabaseHelper db = new DatabaseHelper();
        public PackingHomeForm()
        {
            InitializeComponent();
            cekData();
            //new Thread(CheckConnectionThread).Start();
        }

        public void ChkConPart(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChkConPart), new object[] { value });
                return;
            }
            if (pageActive && (value == "OFFLINE"))
            {
                OffLineInfoForm ph = new OffLineInfoForm();
                pageActive = false;
                this.Hide();
                ph.ShowDialog();
                this.Show();
                pageActive = true;
            }
        }
        void CheckConnectionThread()
        {
            while (true)
            {
                string v = "OFFLINE";
                if (db.IsConnect())
                {
                    v = "ONLINE";
                }
                ChkConPart(v);
                Thread.Sleep(2000);
            }
        }

        private void cekData()
        {
            if (db.IsConnect())
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@MODE", "Text", "CRIPPLE"));
                param.Add(new DbParam("@PAGEVIEW", "Int", "1"));

                DataSet ds = db.GetDataSet("[spex].SP_DEVICE_GET_GRID_CARGO", param);
                DataTable o = ds.Tables[0];
                btnCripple.Enabled = (o.Rows.Count > 0);

                param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@MODE", "Text", "REOPEN"));
                param.Add(new DbParam("@PAGEVIEW", "Int", "1"));

                DataSet ds1 = db.GetDataSet("[spex].SP_DEVICE_GET_GRID_CARGO", param);
                DataTable o1 = ds1.Tables[0];
                btnReopen.Enabled = (o1.Rows.Count > 0);

                param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@MODE", "Text", "REMAIN"));
                param.Add(new DbParam("@PAGEVIEW", "Int", "1"));

                DataSet ds2 = db.GetDataSet("[spex].SP_DEVICE_GET_GRID_CARGO", param);
                DataTable o2 = ds2.Tables[0];
                btnRemain.Enabled = (o2.Rows.Count > 0);
            }
            else
            {
                MessageBox.Show("Can not contact server for process. Check network connection");
            }
        }

        private void PackingHomeForm_Load(object sender, EventArgs e)
        {
            loginTxt.Text = DeviceUser.USER_LOGIN;
            TransportCdTxt.Text = DeviceUser.TRANSPORT_CD_DESC;
            //LineCodeLbl.Text = "Line Code:" + DeviceUser.PLANT_LINE_CD;
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (db.IsConnect())
                {
                    db.Logout();
                    pageActive = false;
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Can not contact server for process. Check network connection");
                }

            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        }

        private void newProcessBtn_Click(object sender, EventArgs e)
        {
            DeviceUser.DEVICE_MODE = "NEW";
            
            PackingProcessForm ph = new PackingProcessForm();
            pageActive = false;
            this.Hide();
            ph.ShowDialog();
            this.Show();
            pageActive = true;
            cekData();
        }

        private void btnCripple_Click(object sender, EventArgs e)
        {
            GridCargo gc = new GridCargo("CRIPPLE");
            pageActive = false;
            this.Hide();
            gc.ShowDialog();
            pageActive = true;
            this.Show();
            cekData();
        }

        private void btnReopen_Click(object sender, EventArgs e)
        {
            GridCargo gc = new GridCargo("REOPEN");
            pageActive = false;
            this.Hide();
            gc.ShowDialog();
            pageActive = true;
            this.Show();
            cekData();
        }

        private void btnRemain_Click(object sender, EventArgs e)
        {
            GridCargo gc = new GridCargo("REMAIN");
            pageActive = false;
            this.Hide();
            gc.ShowDialog();
            pageActive = true;
            this.Show();
            cekData();
        }

        private void PackingHomeForm_Activated(object sender, EventArgs e)
        {
        }

        private void PackingHomeForm_GotFocus(object sender, EventArgs e)
        {
        }

        private void bTest_Click(object sender, EventArgs e)
        {
            CheckPassword cp = new CheckPassword();

            string caption = "Error";
            string message = "Ada yang Tidak Sesuai datanya";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show(message, caption, buttons, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                DeviceUser.ERR_CLOSE_STS = false;
                if (DeviceUser.SETTING_SHOW_ERR_CLS_PWD == "Y")
                {
                    cp.ShowDialog();
                }
            }
        }

        private void PackingHomeForm_DoubleClick(object sender, EventArgs e)
        {
            bTest.Visible = true;
        }
    }
}