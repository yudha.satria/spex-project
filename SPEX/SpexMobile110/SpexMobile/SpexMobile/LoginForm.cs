﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Net.Json;
using System.Data.Common;
using System.Data.SqlClient;
using SpexMobile.AppCode;
using System.Diagnostics;
using System.Data.SqlServerCe;
using System.Xml;
using System.Data.SQLite;
using System.Reflection;
using System.Threading;

//using Finisar.SQLite;
//using System.Data.SqlLite;

namespace SpexMobile
{
    public partial class LoginForm : Form
    {
        DatabaseHelper db = new DatabaseHelper();
        bool pageActive = true;
        public LoginForm()
        {
            InitializeComponent();
            //CheckConnection();
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            versionLbl.Text = "V : " + version.ToString();
            username_txt.Focus();
            //new Thread(CheckConnectionThread).Start();
            
            //timer1.sta
            
        }
        
        public void ChkConPart(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChkConPart), new object[] { value });
                return;
            }
            if (pageActive)
            {
                statusLbl.Text = value;
                if (value == "ONLINE")
                {
                    statusLbl.ForeColor = Color.Green;
                }
                else
                {
                    statusLbl.ForeColor = Color.Red;
                }
            }
        }

        void CheckConnectionThread()
        {
            while(true)
            {
                string v = "OFFLINE";
                if (db.IsConnect())
                {
                    v = "ONLINE";
                }
                ChkConPart(v);
                Thread.Sleep(2000);
            }
        }

        private void CheckConnection()
        {
            if (db.IsConnect())
            {
                statusLbl.Text = "ONLINE";
                statusLbl.ForeColor = Color.Green;
            }
            else
            {
                statusLbl.Text = "OFFLINE";
                statusLbl.ForeColor = Color.Red;
            }
        }


        private void LoginBtn_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (username_txt.Text == passwordtxt.Text && username_txt.Text == "admin")
                {
                    pageActive = false;
                    ConfigHomeForm ph = new ConfigHomeForm();
                    this.Hide();
                    username_txt.Text = "";
                    passwordtxt.Text = "";
                    ph.ShowDialog();
                    this.Show();
                    pageActive = true;
                }
                else
                {
                    if (true || statusLbl.Text == "ONLINE")
                    {
                        var ip = (from a in Dns.GetHostEntry(Dns.GetHostName()).AddressList
                                  where a.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork
                                  select a).First().ToString();

                        List<DbParam> param = new List<DbParam>();
                        param.Add(new DbParam("@USER_LOGIN", "Text", username_txt.Text));
                        param.Add(new DbParam("@USER_PWD", "Text", passwordtxt.Text));
                        param.Add(new DbParam("@IP", "Text", ip));
                        param.Add(new DbParam("@VALIDASI", "Text", "1"));
                        DataTable o = db.GetDataTable("[spex].SP_DEVICE_USER_GET", param);

                        if (o.Rows.Count == 0)
                        {
                            MessageBox.Show("User Login Tidak Ditemukan");
                        }
                        else
                        {
                            if (o.Rows[0]["RESPONSE"].ToString().Trim() == "SUCCESS")
                            {
                                DeviceUser.USER_NAME = o.Rows[0]["USER_NAME"].ToString();
                                DeviceUser.COMPANY_PLANT_CD = o.Rows[0]["COMPANY_PLANT_CD"].ToString();
                                DeviceUser.DEVICE_NW_IDENT = o.Rows[0]["DEVICE_NW_IDENT"].ToString();
                                DeviceUser.LOGIN_STS = o.Rows[0]["LOGIN_STS"].ToString();
                                DeviceUser.PACKING_COMPANY = o.Rows[0]["PACKING_COMPANY"].ToString();
                                DeviceUser.PLANT_LINE_CD = o.Rows[0]["PLANT_LINE_CD"].ToString();
                                DeviceUser.TRANSPORT_CD = o.Rows[0]["TRANSPORT_CD"].ToString();
                                DeviceUser.USER_LOGIN = o.Rows[0]["USER_LOGIN"].ToString();
                                DeviceUser.USER_PASWD = o.Rows[0]["USER_PASWD"].ToString();
                                DeviceUser.POSTION_CD = o.Rows[0]["POSTION_CD"].ToString();
                                DeviceUser.IP = ip;

                                DeviceUser.SETTING_CASE_LABEL_CHECK_DIGIT = o.Rows[0]["CASE_LABEL_CHECK_DIGIT"].ToString();
                                DeviceUser.SETTING_CASE_LABEL_MAX_STR = o.Rows[0]["CASE_LABEL_MAX_STR"].ToString();
                                DeviceUser.SETTING_PART_LABEL_CHECK_DIGIT = o.Rows[0]["PART_LABEL_CHECK_DIGIT"].ToString();
                                DeviceUser.SETTING_PART_LABEL_MAX_STR = o.Rows[0]["PART_LABEL_MAX_STR"].ToString();
                                
                                // Add Get Setting for Show Error Close Password : by FID.Andri (2018-08-16)
                                DeviceUser.SETTING_SHOW_ERR_CLS_PWD = o.Rows[0]["SHOW_ERR_CLS_PWD"].ToString();

                                if (DeviceUser.TRANSPORT_CD == "1")
                                {
                                    DeviceUser.TRANSPORT_CD_DESC = "AIR";
                                }
                                else if (DeviceUser.TRANSPORT_CD == "2")
                                {
                                    DeviceUser.TRANSPORT_CD_DESC = "SEA";
                                }
                                else if (DeviceUser.TRANSPORT_CD == "3")
                                {
                                    DeviceUser.TRANSPORT_CD_DESC = "LAND";
                                }
                                else
                                {
                                    DeviceUser.TRANSPORT_CD_DESC = "ERR";
                                }

                                if (DeviceUser.PLANT_LINE_CD == "P")
                                {
                                    pageActive = false;
                                    PackingHomeForm ph = new PackingHomeForm();
                                    this.Hide();
                                    ph.ShowDialog();
                                    this.Show();
                                    username_txt.Focus();
                                    pageActive = true;
                                }
                                else if (DeviceUser.PLANT_LINE_CD == "R")
                                {
                                    pageActive = false;
                                    ManifestHomeForm ph = new ManifestHomeForm();
                                    this.Hide();
                                    ph.ShowDialog();
                                    this.Show();
                                    username_txt.Focus();
                                    pageActive = true;
                                }
                                else if (DeviceUser.PLANT_LINE_CD == "V")
                                {
                                    pageActive = false;
                                    VanningHomeForm ph = new VanningHomeForm();
                                    this.Hide();
                                    ph.ShowDialog();
                                    this.Show();
                                    username_txt.Focus();
                                    pageActive = true;
                                }
                                else if (DeviceUser.PLANT_LINE_CD == "C" || DeviceUser.PLANT_LINE_CD == "B")
                                {
                                    pageActive = false;
                                    PartHomeForm ph = new PartHomeForm();
                                    this.Hide();
                                    ph.ShowDialog();
                                    this.Show();
                                    username_txt.Focus();
                                    pageActive = true;
                                }
                                else
                                {
                                    MessageBox.Show("Undefined Plant Code");
                                }

                                username_txt.Text = "";
                                passwordtxt.Text = "";

                            }
                            else
                            {
                                MessageBox.Show(o.Rows[0]["RESPONSE"].ToString().Trim().Replace("Error|", ""));
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Can not contact server for process. Check network connection");
                    }
                
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error:" + exc.Message);
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {

            Process thisProcess = Process.GetCurrentProcess();
            thisProcess.Kill();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {
                
                var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                MessageBox.Show(version.ToString());
                MessageBox.Show(Environment.OSVersion.Platform.ToString());
                MessageBox.Show((Environment.OSVersion.Platform.ToString() == "Win32NT").ToString());
                DatabaseLocalHelper dl = new DatabaseLocalHelper();
                MessageBox.Show(dl.GetSysData("db_server"));

                String o = "";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load("Configuration.xml");
                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/Table/sys_config");
                string k = "", v = "";
                foreach (XmlNode node in nodeList)
                {
                    k = node.SelectSingleNode("sys_key").InnerText;
                    v = node.SelectSingleNode("sys_value").InnerText;

                    if (k == "db_server")
                    {
                        o = v;
                        break;
                    }
                }
                MessageBox.Show(o);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            return;
            //this.Hide();
            //GridCargo f = new GridCargo();
            //f.ShowDialog();
            //this.Show();
            

            //return;
            try
            {
                DeviceUser.USER_NAME = "Bobby";
                DeviceUser.COMPANY_PLANT_CD = "P";
                DeviceUser.DEVICE_NW_IDENT = "10.20.30.30";
                DeviceUser.LOGIN_STS = "1";
                DeviceUser.PACKING_COMPANY = "TMMIN";
                DeviceUser.PLANT_LINE_CD = "P";
                DeviceUser.TRANSPORT_CD = "1";
                DeviceUser.USER_LOGIN = "Bobby";
                DeviceUser.USER_PASWD = "dasd";

                if (DeviceUser.TRANSPORT_CD == "1")
                {
                    DeviceUser.TRANSPORT_CD_DESC = "AIR";
                }
                else if (DeviceUser.TRANSPORT_CD == "2")
                {
                    DeviceUser.TRANSPORT_CD_DESC = "SEA";
                }
                else if (DeviceUser.TRANSPORT_CD == "3")
                {
                    DeviceUser.TRANSPORT_CD_DESC = "LAND";
                }
                else
                {
                    DeviceUser.TRANSPORT_CD_DESC = "ERR";
                }

                if (DeviceUser.PLANT_LINE_CD == "P")
                {
TestForm ph = new TestForm();
                    this.Hide();
                    
                    ph.ShowDialog();
                    this.Show();
                }
                else if (DeviceUser.PLANT_LINE_CD == "R")
                {
                    this.Hide();
                    ManifestHomeForm ph = new ManifestHomeForm();
                    ph.ShowDialog();
                    this.Show();
                }
                
                
                
                /*
                string path;
                path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                path = path.Replace("file:\\","") + "\\SpexMobile.sdf";
                
                
                SqlCeConnection sql_con = new SqlCeConnection(@"Data Source="+path);

                SqlCeCommand sql_cmd = sql_con.CreateCommand();
                sql_con.Open();
                sql_cmd.CommandText = "SELECT ref_val FROM SYSCONFIG WHERE ref_key= 'account'";
                //sql_cmd.CommandText = "SELECT NAME FROM TEST";
                SqlCeDataReader reader = sql_cmd.ExecuteReader();
                while (reader.Read())
                {
                    MessageBox.Show(reader[0].ToString());
                }

                sql_con.Close();
                 * */
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            /** */
 
            /*
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("Configuration.xml");
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/Table/sys_config");
            string proID = "", proName = "", price = "";
            foreach (XmlNode node in nodeList)
            {
                proID = node.SelectSingleNode("sys_key").InnerText;
                proName = node.SelectSingleNode("sys_value").InnerText;

                if (proID == "db_server")
                {
                    node.SelectSingleNode("sys_value").InnerText = "bobby";
                }
                MessageBox.Show(proID + " | " + proName);
            }
            xmlDoc.Save("Configuration.xml");
            */
            /*
            XmlDocument doc = new XmlDocument();
            doc.Load("Configuration.xml");
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                string text = node.InnerText; //or loop through its children as well
                MessageBox.Show(text);
            }
            
            //SqlCeConnection sql_con = new SqlCeConnection(@"Data Source=|DataDirectory|\SpexMobile.sdf");
            SqlCeConnection sql_con = new SqlCeConnection(@"Data Source=MobileSpexDB.sdf");
            
            SqlCeCommand sql_cmd = sql_con.CreateCommand();
            sql_con.Open();
            //sql_cmd.CommandText = "SELECT ref_val FROM SYSCONFIG WHERE ref_key= 'account'";
            sql_cmd.CommandText = "SELECT NAME FROM TEST";
            SqlCeDataReader reader = sql_cmd.ExecuteReader();
            while (reader.Read())
            {
                MessageBox.Show(reader[0].ToString()); 
            }

            sql_con.Close();
            */
            /*
            if (name != null)
            {
                MessageBox.Show(name.ToString());
                return;
            }
            
            // acount table not exist, create table and insert 
            sql_cmd.CommandText = "CREATE TABLE SYSCONFIG (ref_key VARCHAR(100), ref_val VARCHAR(800))";
            sql_cmd.ExecuteNonQuery();
            sql_cmd.CommandText = "INSERT INTO SYSCONFIG (ref_key, ref_val) VALUES ('account', 'bobby')";
            sql_cmd.ExecuteNonQuery();
            sql_con.Close(); 
            */
            /*
            DeviceUser.USER_NAME = "Bobby";
            DeviceUser.COMPANY_PLANT_CD = "P";
            DeviceUser.DEVICE_NW_IDENT = "10.20.30.30";
            DeviceUser.LOGIN_STS = "1";
            DeviceUser.PACKING_COMPANY = "TMMIN";
            DeviceUser.PLANT_LINE_CD = "P";
            DeviceUser.TRANSPORT_CD = "1";
            DeviceUser.USER_LOGIN = "Bobby";
            DeviceUser.USER_PASWD = "dasd";

            if (DeviceUser.TRANSPORT_CD == "1")
            {
                DeviceUser.TRANSPORT_CD_DESC = "AIR";
            }
            else if (DeviceUser.TRANSPORT_CD == "2")
            {
                DeviceUser.TRANSPORT_CD_DESC = "SEA";
            }
            else if (DeviceUser.TRANSPORT_CD == "3")
            {
                DeviceUser.TRANSPORT_CD_DESC = "LAND";
            }
            else
            {
                DeviceUser.TRANSPORT_CD_DESC = "ERR";
            }

            if (DeviceUser.PLANT_LINE_CD == "P")
            {
                this.Hide();
                PackingHomeForm ph = new PackingHomeForm();
                ph.ShowDialog();
                this.Show();
            }
            else if (DeviceUser.PLANT_LINE_CD == "R")
            {
                this.Hide();
                ManifestHomeForm ph = new ManifestHomeForm();
                ph.ShowDialog();
                this.Show();
            }
             * 
             * */
        }

        private void LoginForm_DoubleClick(object sender, EventArgs e)
        {
            CheckConnection();
        }

        private void username_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                passwordtxt.Focus();
            }
        }

        private void passwordtxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                LoginBtn_Click_1(sender, e);
            }
        }

    }
}