﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;
using System.Threading;
using System.Data.SqlClient;

namespace SpexMobile
{
    public partial class ReceivingProcessForm : Form
    {
        bool pageActive = true;
        DatabaseHelper db = new DatabaseHelper();
        public ReceivingProcessForm()
        {
            InitializeComponent();
            clearForm();
            if (db.using_thread)
            {
                new Thread(CheckConnectionThread).Start();
            }
        }

        private void ReceivingProcessForm_Load(object sender, EventArgs e)
        {
            loginTxt.Text = DeviceUser.USER_LOGIN;
            TransportCdTxt.Text = DeviceUser.TRANSPORT_CD_DESC;
            //LineCodeLbl.Text = "Line Code:" + DeviceUser.PLANT_LINE_CD;
        }

        public void ChkConPart(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChkConPart), new object[] { value });
                return;
            }
            if (pageActive && (value == "OFFLINE"))
            {
                pageActive = false;
                OffLineInfoForm ph = new OffLineInfoForm();
                this.Hide();
                ph.ShowDialog();
                this.Show();
                pageActive = true;
            }
        }

        void CheckConnectionThread()
        {
            while (true)
            {
                string v = "OFFLINE";
                if (db.IsConnect())
                {
                    v = "ONLINE";
                }
                ChkConPart(v);
                Thread.Sleep(2000);
            }
        }

        private void receivingBtn_Click(object sender, EventArgs e)
        {

        }

        private void clearForm()
        {
           
            barcodeLbl.Text = "";
            manifestLbl.Text = "";
            supPlantCodelbl.Text = "";
            orderNoLbl.Text = "";
            recPlantDockCdLbl.Text = "";
            receiveBtn.Visible = false;
            //finishCaseBtn.Visible = false;
            instLbl.Text = "Instruction: Please Scan Manifest Barcode";
        }

        private void homeBtn_Click(object sender, EventArgs e)
        {
            if (barcodeLbl.Text.Length > 6)
            {

                var confirmResult = MessageBox.Show("Manifest No " + barcodeLbl.Text + " belum di receive apakah anda akan melanjutkan ke home ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button2);

                if (confirmResult == DialogResult.Yes)
                {
                    pageActive = false;
                    this.DialogResult = DialogResult.OK;
                }
            }
            else
            {
                pageActive = false;
                this.DialogResult = DialogResult.OK;
            }
            
        }

        private void receiveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@MANIFEST_NO", "Text", barcodeLbl.Text));
                DataTable o = db.GetDataTable("[spex].SP_DEVICE_MANIFEST_RECEIVING_PROCESS", param);
                if (o.Rows[0]["TEXT"].ToString() == "Success")
                {
                    MessageBox.Show("Manifest No " + barcodeLbl.Text + "  telah berhasil di-receive");
                    clearForm();
                    inputTxt.Text = "";
                    inputTxt.Focus();
                }
                else
                {
                    MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                }
            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        }

        private void inputTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)Keys.Return)
            {
                if (!receiveBtn.Visible)
                {
                    try
                    {
                        List<DbParam> param = new List<DbParam>();
                        param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                        param.Add(new DbParam("@MANIFEST_NO", "Text", inputTxt.Text));
                        DataTable o = db.GetDataTable("[spex].SP_DEVICE_MANIFEST_RECEIVING_GET", param);
                        if (o.Rows[0]["TEXT"].ToString() == "Success")
                        {
                            barcodeLbl.Text = inputTxt.Text;
                            manifestLbl.Text = inputTxt.Text;
                            orderNoLbl.Text = o.Rows[0]["TMMIN_ORDER_NO"].ToString();
                            recPlantDockCdLbl.Text = o.Rows[0]["RCV_PLANT_CD"].ToString().Trim() + o.Rows[0]["DOCK_CD"].ToString().Trim();
                            supPlantCodelbl.Text = o.Rows[0]["SUPPLIER_PLANT"].ToString();
                            receiveBtn.Visible = true;
                            finishCaseBtn.Visible = true;
                            instLbl.Text = "Instruction: Please Click Button Receive";
                        }
                        else
                        {
                            MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                            inputTxt.Text = "";
                        }
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Error: " + exc.Message);
                    }
                    
                }
                else if (inputTxt.Text.Trim() != barcodeLbl.Text.Trim())
                {
                    var confirmResult = MessageBox.Show("Manifest No " + barcodeLbl.Text + " belum di receive apakah anda akan melanjutkan scan manifest yang lain ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button2);

                    if (confirmResult == DialogResult.Yes)
                    {
                        clearForm();
                        inputTxt.Text = "";
                    }
                    else
                    {
                        inputTxt.Text = barcodeLbl.Text;
                    }
                }

            }
           
        }

        private void finishCaseBtn_Click(object sender, EventArgs e)
        {
            if (barcodeLbl.Text.Length > 6)
            {

                var confirmResult = MessageBox.Show("Manifest No " + barcodeLbl.Text + " belum di receive apakah anda akan melanjutkan finish case ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button2);

                if (confirmResult == DialogResult.Yes)
                {
                    DeviceUser.DEVICE_MODE = "NEW";

                    PackingProcessForm ph = new PackingProcessForm();
                    pageActive = false;
                    this.Hide();
                    ph.ShowDialog();
                    this.Show();
                    //pageActive = true;
                    this.DialogResult = DialogResult.OK;
                }
            }
            else
            {
                DeviceUser.DEVICE_MODE = "NEW";

                PackingProcessForm ph = new PackingProcessForm();
                pageActive = false;
                this.Hide();
                ph.ShowDialog();
                this.Show();
                //pageActive = true;
                this.DialogResult = DialogResult.OK;
            }
            
        }
    }
}