﻿namespace SpexMobile
{
    partial class VerifyVanningLabelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.inputTxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // backBtn
            // 
            this.backBtn.Location = new System.Drawing.Point(13, 93);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(72, 20);
            this.backBtn.TabIndex = 0;
            this.backBtn.Text = "Back";
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(13, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 20);
            this.label1.Text = "Please Scan Vanning Label";
            // 
            // inputTxt
            // 
            this.inputTxt.Location = new System.Drawing.Point(13, 61);
            this.inputTxt.Name = "inputTxt";
            this.inputTxt.Size = new System.Drawing.Size(149, 21);
            this.inputTxt.TabIndex = 2;
            this.inputTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.inputTxt_KeyPress);
            // 
            // VerifyVanningLabelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.inputTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.backBtn);
            this.Name = "VerifyVanningLabelForm";
            this.Text = "Verify Vanning Label";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button backBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox inputTxt;
    }
}