﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;
using System.Threading;
using System.Data.SqlClient;

namespace SpexMobile
{
    public partial class VanningProcessForm : Form
    {
        bool pageActive = true;
        DatabaseHelper db = new DatabaseHelper();
        public bool is_activate = false;
        public string LastStatus = "";
        public VanningProcessForm()
        {
            InitializeComponent();
            initData();
            if (db.using_thread)
            {
                new Thread(CheckConnectionThread).Start();
            }
        }
        public VanningProcessForm(string vanningLabel,string dest,string seal,string tare,string container,string qty,string laststatus,bool isRemain)
        {
            InitializeComponent();
            finishBtn.Visible = false;
            barcodeLbl.Text = vanningLabel;
            vanningLbl.Text = vanningLabel;
            destinationLbl.Text = dest;
            sealnoLbl.Text = seal;
            tareLbl.Text = tare;
            containerLbl.Text = container;
            caseQtyLbl.Text = qty;
            instLbl.Text = "Please Scan Vanning Label";
            homeBtn.Visible = false;
            finishBtn.Visible = true;// request pak admin dan elmas
            is_activate = true;
            modeLbl.Text = laststatus; //request elmas
            LastStatus = laststatus;

            if (isRemain) //request elmas2 10722
            {
                modeLbl.Text = "REMA";
            }
            if (db.using_thread)
            {
                new Thread(CheckConnectionThread).Start();
            }

            /*
             * is_activate = !isRemain;
            
            if (isRemain)
            {
                instLbl.Text = "Please Scan Case Label";
            }
             * 

            finishBtn.Visible = isRemain;*/
        }
        public void ChkConPart(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChkConPart), new object[] { value });
                return;
            }
            if (pageActive && (value == "OFFLINE"))
            {
                pageActive = false;
                OffLineInfoForm ph = new OffLineInfoForm();
                this.Hide();
                ph.ShowDialog();
                this.Show();
                pageActive = true;
            }
        }

        void CheckConnectionThread()
        {
            while (true)
            {
                string v = "OFFLINE";
                if (db.IsConnect())
                {
                    v = "ONLINE";
                }
                ChkConPart(v);
                Thread.Sleep(2000);
            }
        }


        private void initData()
        {
            finishBtn.Visible = false;
            barcodeLbl.Text = "";
            vanningLbl.Text = "";
            destinationLbl.Text = "";
            sealnoLbl.Text = "";
            tareLbl.Text = "";
            containerLbl.Text = "";
            caseQtyLbl.Text = "";
            instLbl.Text = "Please Scan Vanning Label";

        }

        private void VanningProcessForm_Load(object sender, EventArgs e)
        {
            loginTxt.Text = DeviceUser.USER_LOGIN;
            TransportCdTxt.Text = DeviceUser.TRANSPORT_CD_DESC;
            packCompLbl.Text = DeviceUser.PACKING_COMPANY;
            ipLbl.Text = DeviceUser.IP;
            plantCodeLbl.Text = "Plant Code: "+DeviceUser.COMPANY_PLANT_CD;
        }

        private void count_qty()
        {

            string q = @"select COUNT(DISTINCT CASE_NO) J FROM spex.TB_R_READYCARGO_CASE A WHERE VANNING_LABEL = '"+vanningLbl.Text+"'";

            DataTable t = db.GetDataTable(q);
            if (t != null && t.Rows.Count > 0)
            {
                caseQtyLbl.Text = t.Rows[0]["J"].ToString();
            }
            
        }

        private void inputTxt_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (is_activate)
                {
                    if (vanningLbl.Text == inputTxt.Text)
                    {
                        try
                        {
                            List<DbParam> param = new List<DbParam>();
                            param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                            param.Add(new DbParam("@VANNING_LABEL", "Text", inputTxt.Text));
                            param.Add(new DbParam("@PACKING_COMPANY", "Text", DeviceUser.PACKING_COMPANY));
                            param.Add(new DbParam("@LAST_STATUS", "Text", LastStatus));

                            DataTable o = db.GetDataTable("[spex].SP_DEVICE_VANNING_ACTIVATED_PROCESS", param);
                            if (o.Rows[0]["TEXT"].ToString() == "Success")
                            {
                                DeviceUser.DEVICE_MODE = o.Rows[0]["DEVICE_MODE"].ToString();
                                finishBtn.Visible = true;
                                homeBtn.Visible = false;
                                is_activate = false;
                                modeLbl.Text = DeviceUser.DEVICE_MODE;
                                instLbl.Text = "Please Scan Case Label";

                            }
                            else
                            {
                                MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));

                            }
                            inputTxt.Text = "";
                        }
                        catch (SqlException se)
                        {
                            string errorMessages = "";
                            for (int i = 0; i < se.Errors.Count; i++)
                            {
                                errorMessages = errorMessages + ("Index #" + i + "\n" +
                                    "Message: " + se.Errors[i].Message + "\n" +
                                    "Error Number: " + se.Errors[i].Number + "\n" +
                                    "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                                    "Source: " + se.Errors[i].Source + "\n" +
                                    "Procedure: " + se.Errors[i].Procedure + "\n");
                            }
                            MessageBox.Show("SQL Error: " + errorMessages);
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show("Error: " + exc.Message);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Vanning Label yang di scan berbeda dengan Vanning Label yang dipilih");
                    }
                    
                }
                else
                {
                    if (DeviceUser.DEVICE_MODE == "NEW")
                    {
                        try
                        {
                            List<DbParam> param = new List<DbParam>();
                            param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                            param.Add(new DbParam("@VANNING_LABEL", "Text", inputTxt.Text));
                            param.Add(new DbParam("@PACKING_COMPANY", "Text", DeviceUser.PACKING_COMPANY));

                            DataTable o = db.GetDataTable("[spex].SP_DEVICE_VANNING_SCAN_VANNING_LABEL", param);
                            if (o.Rows[0]["TEXT"].ToString() == "Success")
                            {
                                vanningLbl.Text = inputTxt.Text;
                                barcodeLbl.Text = inputTxt.Text;
                                inputTxt.Text = "";
                                destinationLbl.Text = o.Rows[0]["DESTINATION"].ToString();
                                sealnoLbl.Text = o.Rows[0]["SEALNO"].ToString();
                                tareLbl.Text = o.Rows[0]["TARE"].ToString();
                                containerLbl.Text = o.Rows[0]["CONTAINERNO"].ToString();

                                DeviceUser.TRANSPORT_CD = o.Rows[0]["TRANS_CD"].ToString();
                                if (DeviceUser.TRANSPORT_CD == "1")
                                {
                                    DeviceUser.TRANSPORT_CD_DESC = "AIR";
                                }
                                else if (DeviceUser.TRANSPORT_CD == "2")
                                {
                                    DeviceUser.TRANSPORT_CD_DESC = "SEA";
                                }
                                else if (DeviceUser.TRANSPORT_CD == "3")
                                {
                                    DeviceUser.TRANSPORT_CD_DESC = "LAND";
                                }
                                TransportCdTxt.Text = DeviceUser.TRANSPORT_CD_DESC;
                                //caseQtyLbl.Text = "0";
                                count_qty();
                                finishBtn.Visible = true;
                                homeBtn.Visible = false;
                                DeviceUser.DEVICE_MODE = "OPEN";
                                modeLbl.Text = DeviceUser.DEVICE_MODE;
                                instLbl.Text = "Please Scan Case Label";
                            }
                            else
                            {
                                MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                                inputTxt.Text = "";
                            }
                        }
                        catch (SqlException se)
                        {
                            string errorMessages = "";
                            for (int i = 0; i < se.Errors.Count; i++)
                            {
                                errorMessages = errorMessages + ("Index #" + i + "\n" +
                                    "Message: " + se.Errors[i].Message + "\n" +
                                    "Error Number: " + se.Errors[i].Number + "\n" +
                                    "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                                    "Source: " + se.Errors[i].Source + "\n" +
                                    "Procedure: " + se.Errors[i].Procedure + "\n");
                            }
                            MessageBox.Show("SQL Error: " + errorMessages);
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show("Error: " + exc.Message);
                        }
                    }
                    else if (DeviceUser.DEVICE_MODE == "OPEN" || DeviceUser.DEVICE_MODE == "FILL" || DeviceUser.DEVICE_MODE == "REMA")
                    {
                        try
                        {
                            int l = inputTxt.Text.Length;
                            if (DeviceUser.SETTING_CASE_LABEL_CHECK_DIGIT == "Y")
                            {
                                int m = Int32.Parse(DeviceUser.SETTING_CASE_LABEL_MAX_STR);

                                if (l > m)
                                {
                                    l = m;
                                }
                            }
                            inputTxt.Text = inputTxt.Text.Substring(0, l);
                            List<DbParam> param = new List<DbParam>();
                            param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));

                            param.Add(new DbParam("@VANNING_LABEL", "Text", vanningLbl.Text));
                            param.Add(new DbParam("@PACKING_COMPANY", "Text", DeviceUser.PACKING_COMPANY));
                            param.Add(new DbParam("@CASE_NO", "Text", inputTxt.Text));
                            param.Add(new DbParam("@TRANSPORT_CD", "Text", DeviceUser.TRANSPORT_CD));
                            param.Add(new DbParam("@DESTINATION", "Text", destinationLbl.Text));
                            param.Add(new DbParam("@CONTAINERNO", "Text", containerLbl.Text));

                            DataTable o = db.GetDataTable("[spex].SP_DEVICE_VANNING_SCAN_CASE_LABEL", param);
                            if (o.Rows[0]["TEXT"].ToString() == "Success")
                            {
                                count_qty();
                                barcodeLbl.Text = inputTxt.Text;
                                inputTxt.Text = "";
                                DeviceUser.DEVICE_MODE = "FILL";
                                modeLbl.Text = DeviceUser.DEVICE_MODE;
                                instLbl.Text = "Please Scan Case Label";
                            }
                            else
                            {
                                MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                                inputTxt.Text = "";
                            }
                        }
                        catch (SqlException se)
                        {
                            string errorMessages = "";
                            for (int i = 0; i < se.Errors.Count; i++)
                            {
                                errorMessages = errorMessages + ("Index #" + i + "\n" +
                                    "Message: " + se.Errors[i].Message + "\n" +
                                    "Error Number: " + se.Errors[i].Number + "\n" +
                                    "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                                    "Source: " + se.Errors[i].Source + "\n" +
                                    "Procedure: " + se.Errors[i].Procedure + "\n");
                            }
                            MessageBox.Show("SQL Error: " + errorMessages);
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show("Error: " + exc.Message);
                        }
                    }
                }
            }
        }

        private void homeBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (db.IsConnect())
                {
                    List<DbParam> param = new List<DbParam>();
                    param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                    param.Add(new DbParam("@VANNING_LABEL", "Text", vanningLbl.Text));
                    param.Add(new DbParam("@PACKING_COMPANY", "Text", DeviceUser.PACKING_COMPANY));

                    db.ExecuteProcedure("[spex].SP_DEVICE_VANNING_HOME_PROCESS", param);
                    pageActive = false;
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Can not contact server for process. Check network connection");
                }
            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void finishBtn_Click(object sender, EventArgs e)
        {
            if (caseQtyLbl.Text == "0")
            {
                var confirmResult = MessageBox.Show("Tidak ada case, anda yakin untuk finish?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                if (confirmResult == DialogResult.Yes)
                {
                    do_finish();
                }
            }
            else
            {
                var confirmResult = MessageBox.Show("Anda yakin untuk finish?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                if (confirmResult == DialogResult.Yes)
                {
                    do_finish();
                }
            }
        }

        private void do_finish()
        {
            try
            {
                List<DbParam> param = new List<DbParam>();
                param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                param.Add(new DbParam("@VANNING_LABEL", "Text", vanningLbl.Text));
                param.Add(new DbParam("@PACKING_COMPANY", "Text", DeviceUser.PACKING_COMPANY));

                DataTable o = db.GetDataTable("[spex].SP_DEVICE_VANNING_FINISH_PROCESS", param);
                if (o.Rows[0]["TEXT"].ToString() == "Success")
                {
                    inputTxt.Text = "";
                    DeviceUser.DEVICE_MODE = "NEW";
                    modeLbl.Text = DeviceUser.DEVICE_MODE;
                    homeBtn.Visible = true;
                    initData();
                    MessageBox.Show("Finish Successfully");
                }
                else
                {
                    MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                    inputTxt.Text = "";
                }
            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        }
    }
}