﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;

namespace SpexMobile
{
    public partial class VerifyVanningLabelForm : Form
    {
        DatabaseHelper db = new DatabaseHelper();
        public bool toGrid = false;
        public string VanningLabel;
        public string status;
        public VerifyVanningLabelForm()
        {
            InitializeComponent();
        }

        private void inputTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (VanningLabel == inputTxt.Text)
                {
                    //call procedure
                    try
                    {
                        List<DbParam> param = new List<DbParam>();
                        param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                        param.Add(new DbParam("@VANNING_LABEL", "Text", VanningLabel));
                        param.Add(new DbParam("@PACKING_COMPANY", "Text", DeviceUser.PACKING_COMPANY));

                        DataTable o = db.GetDataTable("[spex].SP_DEVICE_VANNING_ACTIVATED_PROCESS", param);
                        if (o.Rows[0]["TEXT"].ToString() == "Success")
                        {

                            DeviceUser.DEVICE_MODE = o.Rows[0]["DEVICE_MODE"].ToString();
                            string Destination = o.Rows[0]["DESTINATION"].ToString();
                            string SEALNO = o.Rows[0]["SEALNO"].ToString();
                            string TARE = o.Rows[0]["TARE"].ToString();
                            string CONTAINERNO = o.Rows[0]["CONTAINERNO"].ToString();
                            string QTY = o.Rows[0]["QTY"].ToString();

                            VanningProcessForm ph = new VanningProcessForm(VanningLabel, Destination, SEALNO, TARE, CONTAINERNO, QTY,status,false);
                            this.Hide();
                            ph.ShowDialog();
                            this.Show();
                            this.DialogResult = DialogResult.OK;
                            
                        }
                        else
                        {
                            MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                            inputTxt.Text = "";
                        }
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Error: " + exc.Message);
                    }
                    
                }
                else
                {
                    MessageBox.Show("Vanning Label yang di scan berbeda dengan Vanning Label yang dipilih");
                }
            }
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            toGrid = true;
            this.DialogResult = DialogResult.OK;
        }
    }
}