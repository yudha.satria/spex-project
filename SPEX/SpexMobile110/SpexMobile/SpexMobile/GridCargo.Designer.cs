﻿namespace SpexMobile
{
    partial class GridCargo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGrid1 = new System.Windows.Forms.DataGrid();
            this.homeBtn = new System.Windows.Forms.Button();
            this.infolbl = new System.Windows.Forms.Label();
            this.modeLbl = new System.Windows.Forms.Label();
            this.TransportCdTxt = new System.Windows.Forms.Label();
            this.loginTxt = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.titleLbl = new System.Windows.Forms.Label();
            this.refreshBtn = new System.Windows.Forms.Button();
            this.LastBtn = new System.Windows.Forms.LinkLabel();
            this.NextBtn = new System.Windows.Forms.LinkLabel();
            this.prevBtn = new System.Windows.Forms.LinkLabel();
            this.firstLbl = new System.Windows.Forms.LinkLabel();
            this.pageLbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dataGrid1
            // 
            this.dataGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dataGrid1.Location = new System.Drawing.Point(3, 48);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.Size = new System.Drawing.Size(271, 141);
            this.dataGrid1.TabIndex = 0;
            this.dataGrid1.Click += new System.EventHandler(this.dataGrid1_Click);
            // 
            // homeBtn
            // 
            this.homeBtn.Location = new System.Drawing.Point(25, 229);
            this.homeBtn.Name = "homeBtn";
            this.homeBtn.Size = new System.Drawing.Size(72, 20);
            this.homeBtn.TabIndex = 1;
            this.homeBtn.Text = "Home";
            this.homeBtn.Click += new System.EventHandler(this.homeBtn_Click);
            // 
            // infolbl
            // 
            this.infolbl.Location = new System.Drawing.Point(4, 69);
            this.infolbl.Name = "infolbl";
            this.infolbl.Size = new System.Drawing.Size(153, 20);
            this.infolbl.Text = "No data found";
            // 
            // modeLbl
            // 
            this.modeLbl.Location = new System.Drawing.Point(47, 263);
            this.modeLbl.Name = "modeLbl";
            this.modeLbl.Size = new System.Drawing.Size(50, 20);
            this.modeLbl.Text = "New";
            // 
            // TransportCdTxt
            // 
            this.TransportCdTxt.Location = new System.Drawing.Point(158, 263);
            this.TransportCdTxt.Name = "TransportCdTxt";
            this.TransportCdTxt.Size = new System.Drawing.Size(60, 20);
            this.TransportCdTxt.Text = "label1";
            this.TransportCdTxt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // loginTxt
            // 
            this.loginTxt.Location = new System.Drawing.Point(86, 263);
            this.loginTxt.Name = "loginTxt";
            this.loginTxt.Size = new System.Drawing.Size(66, 20);
            this.loginTxt.Text = "L";
            this.loginTxt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 263);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 20);
            this.label2.Text = "Mode :";
            // 
            // titleLbl
            // 
            this.titleLbl.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.titleLbl.Location = new System.Drawing.Point(4, 17);
            this.titleLbl.Name = "titleLbl";
            this.titleLbl.Size = new System.Drawing.Size(220, 20);
            this.titleLbl.Text = "label1";
            // 
            // refreshBtn
            // 
            this.refreshBtn.Location = new System.Drawing.Point(103, 229);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(72, 20);
            this.refreshBtn.TabIndex = 8;
            this.refreshBtn.Text = "Refresh";
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click_1);
            // 
            // LastBtn
            // 
            this.LastBtn.Location = new System.Drawing.Point(201, 194);
            this.LastBtn.Name = "LastBtn";
            this.LastBtn.Size = new System.Drawing.Size(30, 20);
            this.LastBtn.TabIndex = 18;
            this.LastBtn.Text = "Last";
            this.LastBtn.Click += new System.EventHandler(this.LastBtn_Click);
            // 
            // NextBtn
            // 
            this.NextBtn.Location = new System.Drawing.Point(170, 194);
            this.NextBtn.Name = "NextBtn";
            this.NextBtn.Size = new System.Drawing.Size(38, 20);
            this.NextBtn.TabIndex = 17;
            this.NextBtn.Text = "Next";
            this.NextBtn.Click += new System.EventHandler(this.NextBtn_Click);
            // 
            // prevBtn
            // 
            this.prevBtn.Location = new System.Drawing.Point(142, 194);
            this.prevBtn.Name = "prevBtn";
            this.prevBtn.Size = new System.Drawing.Size(33, 20);
            this.prevBtn.TabIndex = 16;
            this.prevBtn.Text = "Prev";
            this.prevBtn.Click += new System.EventHandler(this.prevBtn_Click);
            // 
            // firstLbl
            // 
            this.firstLbl.Location = new System.Drawing.Point(114, 194);
            this.firstLbl.Name = "firstLbl";
            this.firstLbl.Size = new System.Drawing.Size(30, 20);
            this.firstLbl.TabIndex = 15;
            this.firstLbl.Text = "First";
            this.firstLbl.Click += new System.EventHandler(this.firstLbl_Click_1);
            // 
            // pageLbl
            // 
            this.pageLbl.Location = new System.Drawing.Point(47, 194);
            this.pageLbl.Name = "pageLbl";
            this.pageLbl.Size = new System.Drawing.Size(61, 20);
            this.pageLbl.Text = "pageLbl";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 20);
            this.label1.Text = "Page :";
            // 
            // GridCargo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 307);
            this.Controls.Add(this.LastBtn);
            this.Controls.Add(this.NextBtn);
            this.Controls.Add(this.prevBtn);
            this.Controls.Add(this.firstLbl);
            this.Controls.Add(this.pageLbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.refreshBtn);
            this.Controls.Add(this.titleLbl);
            this.Controls.Add(this.modeLbl);
            this.Controls.Add(this.TransportCdTxt);
            this.Controls.Add(this.loginTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.infolbl);
            this.Controls.Add(this.homeBtn);
            this.Controls.Add(this.dataGrid1);
            this.Name = "GridCargo";
            this.Text = "GridCargo";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid dataGrid1;
        private System.Windows.Forms.Button homeBtn;
        private System.Windows.Forms.Label infolbl;
        private System.Windows.Forms.Label modeLbl;
        private System.Windows.Forms.Label TransportCdTxt;
        private System.Windows.Forms.Label loginTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label titleLbl;
        private System.Windows.Forms.Button refreshBtn;
        private System.Windows.Forms.LinkLabel LastBtn;
        private System.Windows.Forms.LinkLabel NextBtn;
        private System.Windows.Forms.LinkLabel prevBtn;
        private System.Windows.Forms.LinkLabel firstLbl;
        private System.Windows.Forms.Label pageLbl;
        private System.Windows.Forms.Label label1;
    }
}