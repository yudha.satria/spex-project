﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;
using System.Threading;
using System.Data.SqlClient;

namespace SpexMobile
{
    public partial class CheckPassword : Form
    {
        DatabaseHelper db = new DatabaseHelper();
        public CheckPassword()
        {
            InitializeComponent();
        }

        private void passwordtxt_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void bOK_Click(object sender, EventArgs e)
        {
            List<DbParam> param = new List<DbParam>();
            param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
            param.Add(new DbParam("@SHLH_PWD", "Text", passwordtxt.Text));
            DataTable o = db.GetDataTable("[spex].SP_CHECK_HEAD_PASSWORD", param);

            if (o.Rows[0]["TEXT"].ToString() == "SUCCESS")
            {
                DeviceUser.ERR_CLOSE_STS = true;
                this.Close();
            }
            else
            {
                MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                //MessageBox.Show("Password tidak sesuai"+"| Untuk User : "+DeviceUser.USER_LOGIN+"| SH Password : "+passwordtxt.Text);
            }
        }

        private void CheckPassword_DoubleClick(object sender, EventArgs e)
        {
            bClose.Visible = true;
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            try
            {
                if (db.IsConnect())
                {
                    //db.Logout();
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Can not contact server for process. Check network connection");
                }

            }
            catch (SqlException se)
            {
                string errorMessages = "";
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages = errorMessages + ("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "Error Number: " + se.Errors[i].Number + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                MessageBox.Show("SQL Error: " + errorMessages);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error: " + exc.Message);
            }
        }
    }
}