﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;
using System.Threading;

namespace SpexMobile
{
    public partial class OffLineInfoForm : Form
    {
        DatabaseHelper db = new DatabaseHelper();
        public OffLineInfoForm()
        {
            InitializeComponent();
            new Thread(CheckConnectionThread).Start();
        }

        public void ChkConPart(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChkConPart), new object[] { value });
                return;
            }
            if (value == "ONLINE")
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        void CheckConnectionThread()
        {
            while (true)
            {
                string v = "OFFLINE";
                if (db.IsConnect())
                {
                    v = "ONLINE";
                }
                ChkConPart(v);
                Thread.Sleep(2000);
            }
        }
    }
}