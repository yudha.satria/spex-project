﻿namespace SpexMobile
{
    partial class PartStockForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.inputScan = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.inputZone = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.inputRackAddress = new System.Windows.Forms.TextBox();
            this.inputKanbanId = new System.Windows.Forms.TextBox();
            this.inputPartNo = new System.Windows.Forms.TextBox();
            this.inputPcsKanban = new System.Windows.Forms.TextBox();
            this.homeBtn = new System.Windows.Forms.Button();
            this.TransportCdTxt = new System.Windows.Forms.Label();
            this.loginTxt = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 20);
            this.label1.Text = "Please Scan Kanban Id :";
            // 
            // inputScan
            // 
            this.inputScan.Location = new System.Drawing.Point(14, 32);
            this.inputScan.Name = "inputScan";
            this.inputScan.Size = new System.Drawing.Size(206, 21);
            this.inputScan.TabIndex = 1;
            this.inputScan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.scan);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.Text = "Kanban ID";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(14, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 20);
            this.label3.Text = "Part No";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(14, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 20);
            this.label4.Text = "Pcs / Kanban";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(14, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 20);
            this.label5.Text = "Zone";
            // 
            // inputZone
            // 
            this.inputZone.Enabled = false;
            this.inputZone.Location = new System.Drawing.Point(39, 170);
            this.inputZone.Name = "inputZone";
            this.inputZone.Size = new System.Drawing.Size(181, 21);
            this.inputZone.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(14, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 20);
            this.label6.Text = "Part Address";
            // 
            // inputRackAddress
            // 
            this.inputRackAddress.Enabled = false;
            this.inputRackAddress.Location = new System.Drawing.Point(39, 219);
            this.inputRackAddress.Name = "inputRackAddress";
            this.inputRackAddress.Size = new System.Drawing.Size(181, 21);
            this.inputRackAddress.TabIndex = 8;
            // 
            // inputKanbanId
            // 
            this.inputKanbanId.Enabled = false;
            this.inputKanbanId.Location = new System.Drawing.Point(102, 60);
            this.inputKanbanId.Name = "inputKanbanId";
            this.inputKanbanId.Size = new System.Drawing.Size(118, 21);
            this.inputKanbanId.TabIndex = 9;
            // 
            // inputPartNo
            // 
            this.inputPartNo.Enabled = false;
            this.inputPartNo.Location = new System.Drawing.Point(102, 93);
            this.inputPartNo.Name = "inputPartNo";
            this.inputPartNo.Size = new System.Drawing.Size(118, 21);
            this.inputPartNo.TabIndex = 10;
            // 
            // inputPcsKanban
            // 
            this.inputPcsKanban.Enabled = false;
            this.inputPcsKanban.Location = new System.Drawing.Point(101, 125);
            this.inputPcsKanban.Name = "inputPcsKanban";
            this.inputPcsKanban.Size = new System.Drawing.Size(119, 21);
            this.inputPcsKanban.TabIndex = 11;
            // 
            // homeBtn
            // 
            this.homeBtn.Location = new System.Drawing.Point(148, 249);
            this.homeBtn.Name = "homeBtn";
            this.homeBtn.Size = new System.Drawing.Size(72, 20);
            this.homeBtn.TabIndex = 36;
            this.homeBtn.Text = "Home";
            this.homeBtn.Click += new System.EventHandler(this.homeBtn_Click_1);
            // 
            // TransportCdTxt
            // 
            this.TransportCdTxt.Location = new System.Drawing.Point(160, 274);
            this.TransportCdTxt.Name = "TransportCdTxt";
            this.TransportCdTxt.Size = new System.Drawing.Size(60, 20);
            this.TransportCdTxt.Text = "label1";
            this.TransportCdTxt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // loginTxt
            // 
            this.loginTxt.Location = new System.Drawing.Point(88, 274);
            this.loginTxt.Name = "loginTxt";
            this.loginTxt.Size = new System.Drawing.Size(66, 20);
            this.loginTxt.Text = "L";
            this.loginTxt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(10, 274);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 20);
            this.label7.Text = "Mode : PRT";
            // 
            // PartStockForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.TransportCdTxt);
            this.Controls.Add(this.loginTxt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.homeBtn);
            this.Controls.Add(this.inputPcsKanban);
            this.Controls.Add(this.inputPartNo);
            this.Controls.Add(this.inputKanbanId);
            this.Controls.Add(this.inputRackAddress);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.inputZone);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.inputScan);
            this.Controls.Add(this.label1);
            this.KeyPreview = true;
            this.Name = "PartStockForm";
            this.Text = "PartStockForm";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PartStockForm_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox inputScan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox inputZone;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox inputRackAddress;
        private System.Windows.Forms.TextBox inputKanbanId;
        private System.Windows.Forms.TextBox inputPartNo;
        private System.Windows.Forms.TextBox inputPcsKanban;
        private System.Windows.Forms.Button homeBtn;
        private System.Windows.Forms.Label TransportCdTxt;
        private System.Windows.Forms.Label loginTxt;
        private System.Windows.Forms.Label label7;
    }
}