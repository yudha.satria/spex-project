﻿namespace SpexMobile
{
    partial class ReceivingProcessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.loginTxt = new System.Windows.Forms.Label();
            this.TransportCdTxt = new System.Windows.Forms.Label();
            this.instLbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.inputTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.barcodeLbl = new System.Windows.Forms.Label();
            this.manifestLbl = new System.Windows.Forms.Label();
            this.supPlantCodelbl = new System.Windows.Forms.Label();
            this.orderNoLbl = new System.Windows.Forms.Label();
            this.recPlantDockCdLbl = new System.Windows.Forms.Label();
            this.receiveBtn = new System.Windows.Forms.Button();
            this.homeBtn = new System.Windows.Forms.Button();
            this.finishCaseBtn = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 238);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.Text = "Mode : RCV";
            // 
            // loginTxt
            // 
            this.loginTxt.Location = new System.Drawing.Point(81, 238);
            this.loginTxt.Name = "loginTxt";
            this.loginTxt.Size = new System.Drawing.Size(66, 20);
            this.loginTxt.Text = "L";
            this.loginTxt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TransportCdTxt
            // 
            this.TransportCdTxt.Location = new System.Drawing.Point(153, 238);
            this.TransportCdTxt.Name = "TransportCdTxt";
            this.TransportCdTxt.Size = new System.Drawing.Size(60, 20);
            this.TransportCdTxt.Text = "label1";
            this.TransportCdTxt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // instLbl
            // 
            this.instLbl.Location = new System.Drawing.Point(4, 16);
            this.instLbl.Name = "instLbl";
            this.instLbl.Size = new System.Drawing.Size(221, 32);
            this.instLbl.Text = "Instruction: Please Scan Manifest Barcode";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 20);
            this.label1.Text = "Input";
            // 
            // inputTxt
            // 
            this.inputTxt.Location = new System.Drawing.Point(123, 60);
            this.inputTxt.Name = "inputTxt";
            this.inputTxt.Size = new System.Drawing.Size(102, 21);
            this.inputTxt.TabIndex = 15;
            this.inputTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.inputTxt_KeyPress);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(4, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 20);
            this.label3.Text = "Barcode";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(4, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 20);
            this.label4.Text = "Manifest No";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(4, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 20);
            this.label5.Text = "Supplier Plant Code";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(4, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 20);
            this.label6.Text = "Order No";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(4, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 32);
            this.label7.Text = "Receiving Plant And Dock Code";
            // 
            // barcodeLbl
            // 
            this.barcodeLbl.Location = new System.Drawing.Point(123, 94);
            this.barcodeLbl.Name = "barcodeLbl";
            this.barcodeLbl.Size = new System.Drawing.Size(100, 20);
            this.barcodeLbl.Text = "label8";
            // 
            // manifestLbl
            // 
            this.manifestLbl.Location = new System.Drawing.Point(123, 114);
            this.manifestLbl.Name = "manifestLbl";
            this.manifestLbl.Size = new System.Drawing.Size(100, 20);
            this.manifestLbl.Text = "label8";
            // 
            // supPlantCodelbl
            // 
            this.supPlantCodelbl.Location = new System.Drawing.Point(123, 134);
            this.supPlantCodelbl.Name = "supPlantCodelbl";
            this.supPlantCodelbl.Size = new System.Drawing.Size(100, 20);
            this.supPlantCodelbl.Text = "label8";
            // 
            // orderNoLbl
            // 
            this.orderNoLbl.Location = new System.Drawing.Point(123, 154);
            this.orderNoLbl.Name = "orderNoLbl";
            this.orderNoLbl.Size = new System.Drawing.Size(100, 20);
            this.orderNoLbl.Text = "label8";
            // 
            // recPlantDockCdLbl
            // 
            this.recPlantDockCdLbl.Location = new System.Drawing.Point(123, 174);
            this.recPlantDockCdLbl.Name = "recPlantDockCdLbl";
            this.recPlantDockCdLbl.Size = new System.Drawing.Size(100, 20);
            this.recPlantDockCdLbl.Text = "label8";
            // 
            // receiveBtn
            // 
            this.receiveBtn.Location = new System.Drawing.Point(4, 210);
            this.receiveBtn.Name = "receiveBtn";
            this.receiveBtn.Size = new System.Drawing.Size(72, 20);
            this.receiveBtn.TabIndex = 34;
            this.receiveBtn.Text = "Receive";
            this.receiveBtn.Click += new System.EventHandler(this.receiveBtn_Click);
            // 
            // homeBtn
            // 
            this.homeBtn.Location = new System.Drawing.Point(82, 210);
            this.homeBtn.Name = "homeBtn";
            this.homeBtn.Size = new System.Drawing.Size(72, 20);
            this.homeBtn.TabIndex = 35;
            this.homeBtn.Text = "Home";
            this.homeBtn.Click += new System.EventHandler(this.homeBtn_Click);
            // 
            // finishCaseBtn
            // 
            this.finishCaseBtn.Location = new System.Drawing.Point(160, 210);
            this.finishCaseBtn.Name = "finishCaseBtn";
            this.finishCaseBtn.Size = new System.Drawing.Size(77, 20);
            this.finishCaseBtn.TabIndex = 36;
            this.finishCaseBtn.Text = "Finish Case";
            this.finishCaseBtn.Click += new System.EventHandler(this.finishCaseBtn_Click);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(115, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 20);
            this.label8.Text = ":";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(115, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 20);
            this.label9.Text = ":";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(115, 114);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 20);
            this.label10.Text = ":";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(115, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(10, 20);
            this.label11.Text = ":";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(115, 154);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 20);
            this.label12.Text = ":";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(115, 174);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(10, 20);
            this.label13.Text = ":";
            // 
            // ReceivingProcessForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.finishCaseBtn);
            this.Controls.Add(this.homeBtn);
            this.Controls.Add(this.receiveBtn);
            this.Controls.Add(this.recPlantDockCdLbl);
            this.Controls.Add(this.orderNoLbl);
            this.Controls.Add(this.supPlantCodelbl);
            this.Controls.Add(this.manifestLbl);
            this.Controls.Add(this.barcodeLbl);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.inputTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.instLbl);
            this.Controls.Add(this.TransportCdTxt);
            this.Controls.Add(this.loginTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Name = "ReceivingProcessForm";
            this.Text = "Manifest Receiving";
            this.Load += new System.EventHandler(this.ReceivingProcessForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label loginTxt;
        private System.Windows.Forms.Label TransportCdTxt;
        private System.Windows.Forms.Label instLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox inputTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label barcodeLbl;
        private System.Windows.Forms.Label manifestLbl;
        private System.Windows.Forms.Label supPlantCodelbl;
        private System.Windows.Forms.Label orderNoLbl;
        private System.Windows.Forms.Label recPlantDockCdLbl;
        private System.Windows.Forms.Button receiveBtn;
        private System.Windows.Forms.Button homeBtn;
        private System.Windows.Forms.Button finishCaseBtn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}