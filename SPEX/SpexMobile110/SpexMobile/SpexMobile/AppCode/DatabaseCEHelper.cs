﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlServerCe;

namespace SpexMobile.AppCode
{
    class DatabaseCEHelper
    {
        public String GetSysData(String SysKey)
        {
            string o = "";
            SqlCeConnection sql_con = new SqlCeConnection(@"Data Source=" + GetPath());
            SqlCeCommand sql_cmd = sql_con.CreateCommand();
            sql_con.Open();
            sql_cmd.CommandText = "SELECT ref_val FROM SYSCONFIG WHERE ref_key= '" + SysKey + "'";

            SqlCeDataReader reader = sql_cmd.ExecuteReader();
            while (reader.Read())
            {
                o = reader[0].ToString();
            }

            sql_con.Close();

            return o;   
        }

        private string GetPath()
        {
            string path;
            path = System.IO.Path.GetDirectoryName(
            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = path.Replace("file:\\", "") + "\\SpexMobile.sdf";

            return path;
        }

        public void SetSysData(String SysKey, String SysVal)
        {

            if (SysKey == "printer" || SysKey == "conn_using_ws" || SysKey == "url_ws" || SysKey == "timeout")
            {
                ForceSetSysData(SysKey, SysVal);
            }
            else
            {
                SqlCeConnection sql_con = new SqlCeConnection(@"Data Source=" + GetPath());
                SqlCeCommand sql_cmd = sql_con.CreateCommand();
                sql_con.Open();
                sql_cmd.CommandText = "UPDATE SYSCONFIG SET ref_val='" + SysVal + "' WHERE ref_key= '" + SysKey + "'";

                sql_cmd.ExecuteScalar();

                sql_con.Close();
            }
        }
        public void ForceSetSysData(String SysKey, String SysVal)
        {
            if (Environment.OSVersion.Platform.ToString() == "Win32NT")
            {
            }
            else
            {
                SqlCeConnection sql_con = new SqlCeConnection(@"Data Source=" + GetPath());
                SqlCeCommand sql_cmd = sql_con.CreateCommand();
                sql_con.Open();
                sql_cmd.CommandText = "DELETE SYSCONFIG WHERE ref_key= '" + SysKey + "'";
                sql_cmd.ExecuteScalar();

                sql_cmd.CommandText = "INSERT INTO SYSCONFIG(ref_key,ref_val) VALUES ('" + SysKey + "','" + SysVal + "')";
                sql_cmd.ExecuteScalar();

                sql_con.Close();
            }
        }
    }
}
