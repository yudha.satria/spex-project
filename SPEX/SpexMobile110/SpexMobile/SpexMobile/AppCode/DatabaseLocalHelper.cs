﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
//using System.Data.SqlServerCe;
using System.Xml;
using System.IO;
using System.Reflection;

namespace SpexMobile.AppCode
{
    class DatabaseLocalHelper
    {

        public String GetSysData(String SysKey)
        {
            if ((Environment.OSVersion.Platform.ToString() == "Win32NT") )
            {
                String o = "";
                var fileName = Path.Combine(
                    Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)
                    , @"Configuration.xml");
                
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load("Configuration.xml");
                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/Table/sys_config");
                string k = "", v = "";
                foreach (XmlNode node in nodeList)
                {
                    k = node.SelectSingleNode("sys_key").InnerText;
                    v = node.SelectSingleNode("sys_value").InnerText;

                    if (k == SysKey)
                    {
                        o = v;
                        break;
                    }
                }
                return o;
            }
            else
            {
                DatabaseCEHelper ce = new DatabaseCEHelper();
                return ce.GetSysData(SysKey);
            }
             
        }

        public void SetSysData(String SysKey, String SysVal)
        {
            if (Environment.OSVersion.Platform.ToString() == "Win32NT")
            {
                String o = "";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load("Configuration.xml");
                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/Table/sys_config");
                string k = "";
                foreach (XmlNode node in nodeList)
                {
                    k = node.SelectSingleNode("sys_key").InnerText;

                    if (k == SysKey)
                    {
                        node.SelectSingleNode("sys_value").InnerText = SysVal;
                        break;
                    }
                }
                xmlDoc.Save("Configuration.xml");
            }
            else
            {
                DatabaseCEHelper ce = new DatabaseCEHelper();
                ce.SetSysData(SysKey,SysVal);
            }
             
        }

        

        
        
            
    }
}
