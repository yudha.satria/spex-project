﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using System.Net;

namespace SpexMobile.AppCode
{
    class DatabaseHelper
    {
        //string constr = "Data Source=DB1-DEV-SQL2016.toyota.co.id\\DB1DEVSQL2016;Initial Catalog=SPEX_DB;UID=SP3X_DB_DEV;Password=Sp3X_d8_dev;Persist Security Info=false;";
        //string constr = "Data Source=localhost;Initial Catalog=SPEX_DB_DEV;UID=sa;Password=sa;Persist Security Info=false;";
        //string constr = "Data Source=10.165.8.69;Initial Catalog=SPEX_DB_DEV;UID=dms;Password=dms;Persist Security Info=false;";
        //string constr = "Data Source=localhost;Initial Catalog=SPEX_DB_DEV;UID=sa;Password=sa;Persist Security Info=false;";
        
        
        // dicomment sementara error di device spexws.Spex ws = new spexws.Spex(); 
        string constr = "Data Source=DB1-QAS-SQL2016.toyota.co.id\\DB1QASSQL2016;Initial Catalog=SPEX_DB;UID=SP3X_DB_QAS;Password=Sp3X_db_qas;Persist Security Info=false;";
        public bool using_thread = false;
        public SqlConnection open()
        {
            DatabaseLocalHelper l = new DatabaseLocalHelper();
            constr = "Data Source=" + l.GetSysData("db_server") + ";Initial Catalog=" + l.GetSysData("db_catalog") + ";UID=" + l.GetSysData("db_uid") + ";Password=" + l.GetSysData("db_password") + ";Persist Security Info=false;";
            return new SqlConnection(constr);
        }

        public bool using_ws
        {
            get
            {
                return false;
                /*
                DatabaseLocalHelper l = new DatabaseLocalHelper();
                string is_ws = l.GetSysData("conn_using_ws");
                ws.Url = l.GetSysData("url_ws");
                return is_ws == "1";
                 * */
            }
        }

        public int timeout_db
        {
            get
            {
                DatabaseLocalHelper l = new DatabaseLocalHelper();
                string tm = l.GetSysData("timeout");
                if(!string.IsNullOrEmpty(tm))
                {
                    return Int32.Parse(tm);
                }
                else
                {
                    return 0;
                }
            }
        }

        public void close(SqlConnection con)
        {
            con.Close();
        }

        public DataTable GetDataTable(String Query)
        {
            if (IsConnect())
            {
                SqlConnection con = open();
                SqlCommand cmd = new SqlCommand(Query, con);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = timeout_db;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                con.Open();
                da.Fill(dt);
                close(con);
                return dt;
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Clear();
                dt.Columns.Add("RESPONSE");
                dt.Columns.Add("TEXT");
                DataRow _err = dt.NewRow();
                _err["RESPONSE"] = "Can not contact server for process. Check network connection";
                _err["TEXT"] = "Can not contact server for process. Check network connection";
                dt.Rows.Add(_err);
                return dt;
            }
        }

        public DataTable GetDataTable(String procName, List<DbParam> p)
        {
            if (IsConnect())
            {
                /*
                if (using_ws)
                {
                    
                    //ws.Url = l.GetSysData("url_ws");
                    DataSet o = ws.RetrieveDataPrc(procName, p.Select(x => x.Name).ToArray(), p.Select(x => x.Val).ToArray());
                    return o.Tables[0];
                }
                else
                {
                 * */
                    SqlConnection con = open();
                    SqlCommand cmd = new SqlCommand(procName, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = timeout_db;
                    foreach (DbParam par in p)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.Val);
                    }
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    con.Open();
                    da.Fill(dt);
                    con.Close();

                    return dt;
                //}
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Clear();
                dt.Columns.Add("RESPONSE");
                dt.Columns.Add("TEXT");
                DataRow _err = dt.NewRow();
                _err["RESPONSE"] = "Can not contact server for process. Check network connection";
                _err["TEXT"] = "Can not contact server for process. Check network connection";
                dt.Rows.Add(_err);
                return dt;
            }
        }

        public DataSet GetDataSet(String procName, List<DbParam> p)
        {
            /*
            if (using_ws)
            {
                return ws.RetrieveDataPrc(procName, p.Select(x => x.Name).ToArray(), p.Select(x => x.Val).ToArray());
            }
            else
            {*/
                SqlConnection con = open();
                SqlCommand cmd = new SqlCommand(procName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = timeout_db;
                foreach (DbParam par in p)
                {
                    cmd.Parameters.AddWithValue(par.Name, par.Val);
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                con.Open();
                da.Fill(dt);
                con.Close();

                return dt;
            //}
        }

        public void ExecuteProcedure(String procName, List<DbParam> p)
        {
            /*
            if (using_ws)
            {
                ws.ProcessDataPrc(procName, p.Select(x => x.Name).ToArray(), p.Select(x => x.Val).ToArray());
            }
            else
            {
             * */
                SqlConnection con = open();
                SqlCommand cmd = new SqlCommand(procName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (DbParam par in p)
                {
                    cmd.Parameters.AddWithValue(par.Name, par.Val);
                }
                con.Open();
                cmd.CommandTimeout = timeout_db;
                cmd.ExecuteScalar();
                con.Close();
            //}
        }

        public void ExecuteSql(String sql)
        {
            /*
            if (using_ws)
            {
                ws.ProcessDataStr(sql);
            }
            else
            {*/
                SqlConnection con = open();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                con.Open();
                cmd.CommandTimeout = timeout_db;
                cmd.ExecuteScalar();
                con.Close();
            //}
        }

        public bool IsConnectNetwork()
        {
            try
            {
                string hostName = Dns.GetHostName();
                IPHostEntry curHost = Dns.GetHostByName(hostName);
                return curHost.AddressList[0].ToString() != IPAddress.Loopback.ToString();
            }
            catch
            {
                return false;
            }
        } 

        public bool IsConnect()
        {
            bool ConnNetWork = IsConnectNetwork();
            if (ConnNetWork)
            {
                /*
                if (using_ws)
                {
                    try
                    {
                        return ws.CheckConnection() == "ok";
                    }
                    catch (Exception exc)
                    {
                        return false;
                    }
                }
                else
                {*/
                    SqlConnection con = open();
                    try
                    {

                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                        con.Open();
                        con.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
               // }
            }
            else
            {
                return false;
            }
        }

        public void Logout()
        {
            List<DbParam> param = new List<DbParam>();
            param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
            ExecuteProcedure("[spex].SP_DEVICE_USER_LOGOUT", param);

            DeviceUser.USER_LOGIN = "";
            DeviceUser.COMPANY_PLANT_CD = "";
            DeviceUser.DEVICE_NW_IDENT = "";
            DeviceUser.PACKING_COMPANY = "";
            DeviceUser.PLANT_LINE_CD = "";
            DeviceUser.TRANSPORT_CD = "";
            DeviceUser.TRANSPORT_CD_DESC = "";
            DeviceUser.USER_NAME = "";
            DeviceUser.USER_PASWD = "";
            DeviceUser.POSTION_CD = "";
            DeviceUser.IP = "";
            DeviceUser.SETTING_CASE_LABEL_CHECK_DIGIT = "";
            DeviceUser.SETTING_CASE_LABEL_MAX_STR = "";
            DeviceUser.SETTING_PART_LABEL_CHECK_DIGIT = "";
            DeviceUser.SETTING_PART_LABEL_MAX_STR = "";
        }
    }
}
