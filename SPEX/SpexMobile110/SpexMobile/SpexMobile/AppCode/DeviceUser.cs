﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SpexMobile.AppCode
{
    public static class DeviceUser
    {
        public static string USER_LOGIN { get; set; }
        public static string USER_PASWD { get; set; }
        public static string USER_NAME { get; set; }
        public static string PACKING_COMPANY { get; set; }
        public static string COMPANY_PLANT_CD { get; set; }
        public static string PLANT_LINE_CD { get; set; }
        public static string TRANSPORT_CD { get; set; }
        public static string TRANSPORT_CD_DESC { get; set; }
        public static string DEVICE_NW_IDENT { get; set; }
        public static string LOGIN_STS { get; set; }
        public static string POSTION_CD { get; set; }
        public static string DEVICE_MODE { get; set; }
        public static string FILL_MODE { get; set; }
        public static string IP { get; set; }
        public static string SETTING_PART_LABEL_CHECK_DIGIT { get; set; }
        public static string SETTING_PART_LABEL_MAX_STR { get; set; }
        public static string SETTING_CASE_LABEL_CHECK_DIGIT { get; set; }
        public static string SETTING_CASE_LABEL_MAX_STR { get; set; }
        // Add New Global Variable : by FID.Andri (2018-08-14)
        public static bool ERR_CLOSE_STS { get; set; }
        // Add New Global Variable : by FID.Andri (2018-08-16)
        public static string SETTING_SHOW_ERR_CLS_PWD { get; set; }
    }
}
