﻿namespace SpexMobile
{
    partial class VanningHomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newProcessBtn = new System.Windows.Forms.Button();
            this.btnReopen = new System.Windows.Forms.Button();
            this.btnRemain = new System.Windows.Forms.Button();
            this.LineCodeLbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.loginTxt = new System.Windows.Forms.Label();
            this.TransportCdTxt = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // newProcessBtn
            // 
            this.newProcessBtn.Location = new System.Drawing.Point(33, 36);
            this.newProcessBtn.Name = "newProcessBtn";
            this.newProcessBtn.Size = new System.Drawing.Size(180, 40);
            this.newProcessBtn.TabIndex = 0;
            this.newProcessBtn.Text = "New Process";
            this.newProcessBtn.Click += new System.EventHandler(this.newProcessBtn_Click);
            // 
            // btnReopen
            // 
            this.btnReopen.Location = new System.Drawing.Point(33, 136);
            this.btnReopen.Name = "btnReopen";
            this.btnReopen.Size = new System.Drawing.Size(180, 40);
            this.btnReopen.TabIndex = 2;
            this.btnReopen.Text = "Reopen";
            this.btnReopen.Click += new System.EventHandler(this.btnReopen_Click);
            // 
            // btnRemain
            // 
            this.btnRemain.Location = new System.Drawing.Point(33, 86);
            this.btnRemain.Name = "btnRemain";
            this.btnRemain.Size = new System.Drawing.Size(180, 40);
            this.btnRemain.TabIndex = 3;
            this.btnRemain.Text = "Remaining";
            this.btnRemain.Click += new System.EventHandler(this.btnRemain_Click);
            // 
            // LineCodeLbl
            // 
            this.LineCodeLbl.Location = new System.Drawing.Point(70, 10);
            this.LineCodeLbl.Name = "LineCodeLbl";
            this.LineCodeLbl.Size = new System.Drawing.Size(143, 20);
            this.LineCodeLbl.Text = "Line Code :  V - Vanning";
            this.LineCodeLbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 238);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.Text = "Mode : N/A";
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(141, 200);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(72, 20);
            this.logoutBtn.TabIndex = 6;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // loginTxt
            // 
            this.loginTxt.Location = new System.Drawing.Point(81, 238);
            this.loginTxt.Name = "loginTxt";
            this.loginTxt.Size = new System.Drawing.Size(66, 20);
            this.loginTxt.Text = "L";
            this.loginTxt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TransportCdTxt
            // 
            this.TransportCdTxt.Location = new System.Drawing.Point(153, 238);
            this.TransportCdTxt.Name = "TransportCdTxt";
            this.TransportCdTxt.Size = new System.Drawing.Size(60, 20);
            this.TransportCdTxt.Text = "label1";
            this.TransportCdTxt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // VanningHomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.TransportCdTxt);
            this.Controls.Add(this.loginTxt);
            this.Controls.Add(this.logoutBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LineCodeLbl);
            this.Controls.Add(this.btnRemain);
            this.Controls.Add(this.btnReopen);
            this.Controls.Add(this.newProcessBtn);
            this.Name = "VanningHomeForm";
            this.Text = "Home Vanning";
            this.Load += new System.EventHandler(this.PackingHomeForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button newProcessBtn;
        private System.Windows.Forms.Button btnReopen;
        private System.Windows.Forms.Button btnRemain;
        private System.Windows.Forms.Label LineCodeLbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Label loginTxt;
        private System.Windows.Forms.Label TransportCdTxt;
    }
}