﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SpexMobile.AppCode;

namespace SpexMobile
{
    public partial class VerifyCaseNoForm : Form
    {
        DatabaseHelper db = new DatabaseHelper();
        public bool toGrid = false;
        public string CaseNo;
        public string CaseType;
        public string prepDate;
        public string status;
        public VerifyCaseNoForm()
        {
            InitializeComponent();
        }

        private void inputTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (CaseNo == inputTxt.Text)
                {
                    //call procedure
                    try
                    {
                        List<DbParam> param = new List<DbParam>();
                        param.Add(new DbParam("@USER_LOGIN", "Text", DeviceUser.USER_LOGIN));
                        param.Add(new DbParam("@PREPARED_DT", "Text", prepDate));
                        param.Add(new DbParam("@CASE_NO", "Text", CaseNo));
                        param.Add(new DbParam("@CASE_TYPE", "Text", CaseType.Substring(0,2)));
                        param.Add(new DbParam("@LAST_STATUS", "Text", status));

                        DataTable o = db.GetDataTable("[spex].SP_DEVICE_PACKING_ACTIVATED_PROCESS", param);
                        if (o.Rows[0]["TEXT"].ToString() == "Success")
                        {

                            DeviceUser.DEVICE_MODE = o.Rows[0]["DEVICE_MODE"].ToString();
                            DeviceUser.FILL_MODE = o.Rows[0]["DEVICE_FILL_MODE"].ToString();
                            string Destination = o.Rows[0]["DESTINATION"].ToString();
                            string DF = o.Rows[0]["DF"].ToString();
                            string PRIV = o.Rows[0]["PRIV"].ToString();
                            string QTY = o.Rows[0]["QTY"].ToString();

                            PackingProcessForm ph = new PackingProcessForm(CaseType, CaseNo, QTY, Destination, PRIV, prepDate, DF,"",false);
                            this.Hide();
                            ph.ShowDialog();
                            this.Show();
                            this.DialogResult = DialogResult.OK;
                            
                        }
                        else
                        {
                            MessageBox.Show(o.Rows[0]["TEXT"].ToString().Replace("Error|", ""));
                            inputTxt.Text = "";
                        }
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Error: " + exc.Message);
                    }
                    
                }
                else
                {
                    MessageBox.Show("CaseNo yang di scan berbeda dengan caseno yang dipilih");
                }
            }
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            toGrid = true;
            this.DialogResult = DialogResult.OK;
        }
    }
}