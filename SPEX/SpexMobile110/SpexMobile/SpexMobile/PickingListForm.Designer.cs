﻿namespace SpexMobile
{
    partial class PickingListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtScan = new System.Windows.Forms.Label();
            this.inputScan = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPickingListNo = new System.Windows.Forms.Label();
            this.txtZone = new System.Windows.Forms.Label();
            this.txtPartNo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.inputZone = new System.Windows.Forms.TextBox();
            this.inputKanbanId = new System.Windows.Forms.TextBox();
            this.inputPartNo = new System.Windows.Forms.TextBox();
            this.inputPartAddress = new System.Windows.Forms.TextBox();
            this.inputQty = new System.Windows.Forms.TextBox();
            this.txtScanId = new System.Windows.Forms.Label();
            this.TransportCdTxt = new System.Windows.Forms.Label();
            this.loginTxt = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 20);
            this.label1.Text = "Scan";
            this.label1.ParentChanged += new System.EventHandler(this.label1_ParentChanged);
            // 
            // txtScan
            // 
            this.txtScan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtScan.Location = new System.Drawing.Point(60, 10);
            this.txtScan.Name = "txtScan";
            this.txtScan.Size = new System.Drawing.Size(167, 20);
            this.txtScan.Text = "Picking List No";
            this.txtScan.ParentChanged += new System.EventHandler(this.label2_ParentChanged);
            // 
            // inputScan
            // 
            this.inputScan.Location = new System.Drawing.Point(12, 33);
            this.inputScan.Name = "inputScan";
            this.inputScan.Size = new System.Drawing.Size(215, 21);
            this.inputScan.TabIndex = 3;
            this.inputScan.TextChanged += new System.EventHandler(this.tbPickingList_TextChanged);
            this.inputScan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.scan);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 20);
            this.label2.Text = "Picking List No :";
            // 
            // txtPickingListNo
            // 
            this.txtPickingListNo.Location = new System.Drawing.Point(118, 60);
            this.txtPickingListNo.Name = "txtPickingListNo";
            this.txtPickingListNo.Size = new System.Drawing.Size(109, 20);
            // 
            // txtZone
            // 
            this.txtZone.Location = new System.Drawing.Point(16, 93);
            this.txtZone.Name = "txtZone";
            this.txtZone.Size = new System.Drawing.Size(63, 18);
            this.txtZone.Text = "Zone";
            // 
            // txtPartNo
            // 
            this.txtPartNo.Location = new System.Drawing.Point(16, 121);
            this.txtPartNo.Name = "txtPartNo";
            this.txtPartNo.Size = new System.Drawing.Size(63, 18);
            this.txtPartNo.Text = "Part No";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 18);
            this.label3.Text = "Kanban Id";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 18);
            this.label4.Text = "Address";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 18);
            this.label5.Text = "Total Qty";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(155, 247);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 20);
            this.button1.TabIndex = 15;
            this.button1.Text = "Finish";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // inputZone
            // 
            this.inputZone.Enabled = false;
            this.inputZone.Location = new System.Drawing.Point(85, 93);
            this.inputZone.Name = "inputZone";
            this.inputZone.Size = new System.Drawing.Size(142, 21);
            this.inputZone.TabIndex = 16;
            // 
            // inputKanbanId
            // 
            this.inputKanbanId.Enabled = false;
            this.inputKanbanId.Location = new System.Drawing.Point(85, 147);
            this.inputKanbanId.Name = "inputKanbanId";
            this.inputKanbanId.Size = new System.Drawing.Size(142, 21);
            this.inputKanbanId.TabIndex = 17;
            // 
            // inputPartNo
            // 
            this.inputPartNo.Enabled = false;
            this.inputPartNo.Location = new System.Drawing.Point(85, 121);
            this.inputPartNo.Name = "inputPartNo";
            this.inputPartNo.Size = new System.Drawing.Size(142, 21);
            this.inputPartNo.TabIndex = 18;
            // 
            // inputPartAddress
            // 
            this.inputPartAddress.Enabled = false;
            this.inputPartAddress.Location = new System.Drawing.Point(85, 177);
            this.inputPartAddress.Name = "inputPartAddress";
            this.inputPartAddress.Size = new System.Drawing.Size(142, 21);
            this.inputPartAddress.TabIndex = 19;
            // 
            // inputQty
            // 
            this.inputQty.Enabled = false;
            this.inputQty.Location = new System.Drawing.Point(85, 209);
            this.inputQty.Name = "inputQty";
            this.inputQty.Size = new System.Drawing.Size(142, 21);
            this.inputQty.TabIndex = 20;
            // 
            // txtScanId
            // 
            this.txtScanId.Location = new System.Drawing.Point(217, 60);
            this.txtScanId.Name = "txtScanId";
            this.txtScanId.Size = new System.Drawing.Size(10, 20);
            this.txtScanId.Visible = false;
            // 
            // TransportCdTxt
            // 
            this.TransportCdTxt.Location = new System.Drawing.Point(165, 272);
            this.TransportCdTxt.Name = "TransportCdTxt";
            this.TransportCdTxt.Size = new System.Drawing.Size(60, 20);
            this.TransportCdTxt.Text = "label1";
            this.TransportCdTxt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // loginTxt
            // 
            this.loginTxt.Location = new System.Drawing.Point(93, 272);
            this.loginTxt.Name = "loginTxt";
            this.loginTxt.Size = new System.Drawing.Size(66, 20);
            this.loginTxt.Text = "L";
            this.loginTxt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(15, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 20);
            this.label6.Text = "Mode : PCK";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(77, 247);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(72, 20);
            this.button2.TabIndex = 30;
            this.button2.Text = "Home";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // PickingListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.TransportCdTxt);
            this.Controls.Add(this.loginTxt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtScanId);
            this.Controls.Add(this.inputQty);
            this.Controls.Add(this.inputPartAddress);
            this.Controls.Add(this.inputPartNo);
            this.Controls.Add(this.inputKanbanId);
            this.Controls.Add(this.inputZone);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPartNo);
            this.Controls.Add(this.txtZone);
            this.Controls.Add(this.txtPickingListNo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.inputScan);
            this.Controls.Add(this.txtScan);
            this.Controls.Add(this.label1);
            this.Name = "PickingListForm";
            this.Text = "PickingListForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label txtScan;
        private System.Windows.Forms.TextBox inputScan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txtZone;
        private System.Windows.Forms.Label txtPartNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox inputZone;
        private System.Windows.Forms.TextBox inputKanbanId;
        private System.Windows.Forms.TextBox inputPartNo;
        private System.Windows.Forms.TextBox inputPartAddress;
        private System.Windows.Forms.TextBox inputQty;
        private System.Windows.Forms.Label txtScanId;
        private System.Windows.Forms.Label txtPickingListNo;
        private System.Windows.Forms.Label TransportCdTxt;
        private System.Windows.Forms.Label loginTxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
    }
}